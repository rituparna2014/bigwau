@extends('Administrator.layouts.master')
@section('content')

<!-- Main content -->
<section class="content"> 
    <div class="row m-b-15">
        
        <div class="col-lg-8 col-md-8 col-sm-8">
            {{ Form::open(array('url' => 'administrator/warehouselocation/zones', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <label class="m-l-15">Rows</label>
            <div class="adddrop m-l-15 w-200">
                <select name="searchByRow" id="searchByRow" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option value="">All rows</option>
                    @foreach($rowList as $row)
                    <option {{$searchData['searchByRow']==$row['id']?'selected':''}} value="{{$row['id']}}">{{$row['name']}}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
            {{ Form::close() }}
        </div>
        
        <div class="col-lg-4 col-md-4 col-sm-4 text-right">    
        <div class=" col-lg-8 col-md-7 col-sm-6 col-xs-7 text-right">
        @if($canAdd == 1)<a href="javascript:void(0);" class="btn btn-sm btn-warning" onclick="showImport('warehouselocation/importzonedata');"><span class="Cicon"><i class="glyphicon glyphicon-import"></i></span> Import</a>   @endif
    </div>
    <div class="col-lg-4 col-md-5 col-sm-6 col-xs-5 text-right">
            @if($canAdd == 1)<a onclick="showAddEdit(0, 0, 'warehouselocation/zones/addedit')"  class="btn btn-info btn-sm pull-right"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Zone</a>@endif
        </div>
        </div>
    </div>                     
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <table id="example2"  class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Zone</th>
                                <th data-sort="rowName" class="{{$sort['rowName']['current']}} sortby">Row</th>
                                <th data-sort="createdOn" class="{{$sort['createdOn']['current']}} sortby">Created On</th>
                                <th width="15%">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($shipmentLocationData)
                            @foreach ($shipmentLocationData as $shipmentLocation)
                            @php 
                                $url = url('administrator/warehouselocation/changestatus/'.$shipmentLocation->id.'/'.$page); 
                                $deleteUrl = url('administrator/warehouselocation/deletelocation/'.$shipmentLocation->id.'/zone/'.$page);
                            @endphp
                            <tr>
                                <td>{{$shipmentLocation->name}}</td>
                                <td>{{$shipmentLocation->rowName}}</td>
                                <td>{{ \Carbon\Carbon::parse($shipmentLocation->createdOn)->format('m-d-Y H:i')}}</td>
                                
                                <td>
                                @if($canAdd == 1)
                                @if($shipmentLocation->status == 1)
                                <a data-toggle="confirmation" href="{{$url. '/0'}}" class="btn btn-success btnActive">Active</a>
                                @else 
                                <a data-toggle="confirmation" href="{{$url. '/1'}}" class="btn btn-danger btnActive">Inactive</a>
                                @endif
                                @endif
                                </td>
                                
                                <td>
                                    @if($canAdd == 1)<a class="text-green edit actionIcons" data-toggle="tooltip" title="Click to Edit" onclick="showAddEdit({{$shipmentLocation->id}}, {{$page}}, 'warehouselocation/zones/addedit');"><i class="fa fa-fw fa-edit"></i></a>@endif
                                    @if(!in_array($shipmentLocation->id, $inUseLocations))
                                        @if($canDelete == 1)<a class="actionIcons color-theme-2" href="{{$deleteUrl}}" data-toggle="confirmation"><i class="fa fa-fw fa-trash"></i></a>@endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="6">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $shipmentLocationData->firstItem() . ' - ' . $shipmentLocationData->lastItem() . ' of  ' . $shipmentLocationData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $shipmentLocationData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-export"></div>
    <!--modal close-->
</section>
<!-- /.content -->             
@endsection