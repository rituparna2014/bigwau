$(function () {

    baseUrl = $('#baseUrl').val();

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    var table = $('#example2').DataTable({
        'paging': false,
        'lengthChange': false,
        'searching': false,
        'ordering': false,
        columnDefs: [
            {orderable: false, targets: -1}
        ],
        'info': false,
        'autoWidth': false
    });
    
    $("#addeditFrm").validate();

    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        // other options
    });

    $('.sortby').on('click', function () {
        var sorttype = 'asc';
        if ($(this).hasClass('sorting_asc'))
            sorttype = 'desc';

        var sortby = $(this).attr('data-sort');

        $('#field').val(sortby);
        $('#type').val(sorttype);
        $('#frmsearch').submit();
    });


    $('.chk_all').on('ifChecked', function () {
        $('.checkbox').each(function () {
            $(this).attr('checked', 'checked');
            $(this).parent().addClass('checked');
        })

    });
    $('.chk_all').on('ifUnchecked', function () {
        $('.checkbox').each(function () {
            $(this).attr('checked', '');
            $(this).parent().removeClass('checked');
        })

    });
    $('.checkAllCharges').on('ifChecked', function () {

        var cbId = $(this).attr('data-id');
        $('.shipCb_' + cbId).attr('checked', 'checked');
        $('.shipCb_' + cbId).parent().addClass('checked');
    });
    $('.checkAllCharges').on('ifUnchecked', function () {

        var cbId = $(this).attr('data-id');
        $('.shipCb_' + cbId).attr('checked', '');
        $('.shipCb_' + cbId).parent().removeClass('checked');
    });
    $('.checkbox').on('ifUnchecked', function () {
        $('.chk_all').attr('checked', '');
        $('.chk_all').parent().removeClass('checked');
    });


    $('.updateStatusCommon').on('click', function (e) {
        e.preventDefault();
        var linkElem = $(this);
        var recordEditId = $(this).attr('data-edit');
        var baseUrl = $('#baseUrl').val();
        var recordStatus = $(this).attr('data-status');
        var ajaxMethod = $('#ajaxPage').val();
        var postStatus = 1;
        if (recordStatus == 1)
            postStatus = 0;

        $.ajax({
            type: "get",
            dataType: 'json',
            url: baseUrl + '/' + ajaxMethod + '/' + recordEditId + '/' + postStatus,
            data: {},
            success: function (msg) {
                //console.log(msg.updated);
                if (msg.updated)
                {
                    //console.log('i am here');
                    if (recordStatus == 1)
                    {
                        linkElem.removeClass('btn-success');
                        linkElem.addClass('btn-danger');
                        linkElem.attr('data-status', 0);
                        linkElem.text('Inactive');
                    }
                    else
                    {
                        linkElem.removeClass('btn-danger');
                        linkElem.addClass('btn-success');
                        linkElem.attr('data-status', 1);
                        linkElem.text('Active');
                    }
                }
            }
        });
    });

    $("#table-search").keyup(function (e) {
        $("#searchData").val($(this).val());
        if (e.keyCode == 13) {
            $('#frmsearch').submit();
        }
    });

    $("#rootCategory").change(function () {
        var rootCatVal = $(this).val();
        var baseUrl = $('#baseUrl').val();
        var ajaxMethod = $('#subcatPage').val();

        $.ajax({
            type: "get",
            dataType: 'json',
            url: baseUrl + '/' + ajaxMethod + '/' + rootCatVal,
            data: {},
            success: function (msg) {
                //console.log(msg.updated);
                if (msg.data)
                {
                    //console.log(msg.data);
                    $('#subCategory').html('<option value="">Select</option>');
                    $.each(msg.data, function (i, j) {
                        //console.log(j);
                        $('#subCategory').append('<option value="' + j.id + '">' + j.category + '</option>')
                    });

                }
            }
        });
    });
    
    $("#subCategory").change(function () {
        console.log('here');
        var rootCat = $(this);
        var rootCatVal = rootCat.val();
        var baseUrl = $('#baseUrl').val();

        $.ajax({
            type: "get",
            dataType: 'json',
            url: baseUrl + '/getsiteproduct/' + rootCatVal,
            data: {},
            success: function (msg) {
                //console.log(msg.updated);
                if (msg.data)
                {
                    //console.log(msg.data);
                    rootCat.parent().parent().find('#siteProduct').html('<option value="">Select</option>');
                    $.each(msg.data, function (i, j) {
                        //console.log(j);
                        rootCat.parent().parent().find('#siteProduct').append('<option value="' + j.id + '">' + j.product + '</option>');
                    });

                }
            }
        });
    });

    $('.editStatus').on('click', function () {
        var recordEdit = $(this).attr('data-id');
        var recordStatus = $(this).attr('data-status');
        $('#recordStatus option[value="' + recordStatus + '"]').attr("selected", true);
        //console.log(recordEdit);
        var formAction = $('#form-action').val();
        formAction = formAction + '/' + recordEdit;
        $('#editStatus-form').attr('action', formAction);
    });

    $('.checkbox-update').on('ifUnchecked', function () {
        //console.log($(this).attr('class'));
        $(this).parent().next().val('0');
    });
    $('.checkbox-update').on('ifChecked', function () {
        //console.log($(this).attr('class'));
        $(this).parent().next().val('1');
    });

    $(document).on('blur', '.numFormat', function () {
        if ($.isNumeric(this.value)) {
            var newVal = parseFloat(this.value);
            $(this).val(newVal.toFixed(2));
        } else {
            var newVal = 0;
            $(this).val(newVal.toFixed(2));
        }
    });
    $(document).on('blur', '.intFormat', function () {
        if ($.isNumeric(this.value)) {
            var newVal = parseInt(this.value);
            $(this).val(newVal);
        } else {
            //var newVal = 0;
            $(this).val('');
        }
    });
    $(document).on('keyup', '#searchUser', function () {
        var searchVal = $("#searchUser").val();
        console.log(searchVal);
        var baseUrl = $('#baseUrl').val();
        if (searchVal == '')
            searchVal = '-1';
        $.ajax({
            type: "get",
            url: baseUrl + '/searchuser' + '/' + searchVal,
            data: {},
            success: function (msg) {
                //console.log(msg.updated);
                if (msg)
                {
                    console.log(msg);
                    $("#showUser").html(msg);
                }
            }
        });
    });
    
    $(document).on('click','.editTrackingData',function(e){
        console.log('test');
        $(".text-data").hide();
        $(".text-edit").show();
    });
    
    $(document).on('click', 'ul.userData li', function () {
        var user = $(this).text();
        $("#searchUser").val(user);
        $("#userId").val($(this).attr('id'));
        $("#showUser").html('');
    });

    /* File Type validation */
    $(document).on('change', '#exampleInputFile', function () {
        $('.modal-body .row .alert-danger').remove();
        var fileName = $(this).val(); //get file name
        var findLastDot = fileName.lastIndexOf('.');
        var ext = fileName.substring(parseInt(findLastDot + 1), fileName.length); //get extension
        //check extension is valid or not
        if (ext != 'csv' && ext != 'xls' && ext != 'xlsx')
        {
            $("#browselabel").hide();
            $(this).val('');
            //show error message
            $('.modal-body .row').prepend('<div class="alert alert-danger">You can upload only CSV or Excel file</div>');

        }
    });

    //Collapse Box
    $('.collapse').on('shown.bs.collapse', function () {
        $('.accordion-toggle').find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
    }).on('hidden.bs.collapse', function () {
        $('.accordion-toggle').find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
    });

    $(function () {
        $('#reservation').daterangepicker();
    });

    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });




});

function getProduct(subCategory) {
    console.log('infunction');
}

function showAddEdit(id, page, urlSegment) {
    var url = urlSegment + '/' + id + '/' + page;
    $.ajax({
        method: "GET",
        url: baseUrl + '/' + url,
    }).done(function (response) {
        $('#modal-addEdit').html(response);
        $('#modal-addEdit').modal({backdrop: 'static', keyboard: false});
        $('#modal-addEdit').modal('show');
    });
}

function showComment(id, page, urlSegment) {
    var url = urlSegment + '/' + id + '/' + page;
    $.ajax({
        method: "GET",
        url: baseUrl + '/' + url,
    }).done(function (response) {
        $('#modal-comment').html(response);
        $('#modal-comment').modal({backdrop: 'static', keyboard: false});
        $('#modal-comment').modal('show');
    });
}

function showImport(urlSegment) {
    var url = urlSegment;
    //alert(urlSegment);
    $.ajax({
        method: "GET",
        url: baseUrl + '/' + url,
    }).done(function (response) {
        $('#modal-export').html(response);
        $('#modal-export').modal({backdrop: 'static', keyboard: false});
        $('#modal-export').modal('show');
    });
}

function isNumberKey(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
        if (charCode == 46)
            return false;
    if (charCode == 46)
        return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;

}

function deleteSelected() {
    if ($('.checkbox:checked').length == 0) {
        alert('Please select atleast one item!!');
    } else {
        $('#modal-default').modal({backdrop: 'static', keyboard: false});
        $('#modal-default').modal('show');
    }
}

function updateAction(action) {
    if ($('.checkbox:checked').length == 0) {
        alert('Please select atleast one item!!');
    } else {
        if(action == 'delete')
        {
            var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");

            if (r == true) {
                $("#action").val(action);
                $('#frmedit').submit();
            }
        }
        else if(action == 'update')
        {
            $("#action").val(action);
            $('#frmedit').submit();
        }
        
    }
}

/* THIS IS FOR LANDING PAGE AND STATIC PAGE SEARCH FUNCTIONALITY */
$(document).ready(function () {
    $("#table-search-button-pages").click(function () {
        $("#searchData").val($("#table-search-text").val());
        $('#frmsearch').submit();
    });
});

