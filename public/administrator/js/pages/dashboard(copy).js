/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  'use strict';

  // Make the dashboard widgets sortable Using jquery UI
  $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

  // jQuery UI sortable for the todo list
  $('.todo-list').sortable({
    placeholder         : 'sort-highlight',
    handle              : '.handle',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });

  // bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5();

  $('.daterange').daterangepicker({
    ranges   : {
      'Today'       : [moment(), moment()],
      'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month'  : [moment().startOf('month'), moment().endOf('month')],
      'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate  : moment()
  }, function (start, end) {
    window.alert('You chose: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  });

  /* jQueryKnob */
  $('.knob').knob();

  // jvectormap data
  var visitorsData = {
    US: 398, // USA
    SA: 400, // Saudi Arabia
    CA: 1000, // Canada
    DE: 500, // Germany
    FR: 760, // France
    CN: 300, // China
    AU: 700, // Australia
    BR: 600, // Brazil
    IN: 800, // India
    GB: 320, // Great Britain
    RU: 3000 // Russia
  };
  // World map by jvectormap
  $('#world-map').vectorMap({
    map              : 'world_mill_en',
    backgroundColor  : 'transparent',
    regionStyle      : {
      initial: {
        fill            : '#e4e4e4',
        'fill-opacity'  : 1,
        stroke          : 'none',
        'stroke-width'  : 0,
        'stroke-opacity': 1
      }
    },
    series           : {
      regions: [
        {
          values           : visitorsData,
          scale            : ['#92c1dc', '#ebf4f9'],
          normalizeFunction: 'polynomial'
        }
      ]
    },
    onRegionLabelShow: function (e, el, code) {
      if (typeof visitorsData[code] != 'undefined')
        el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
    }
  });

  // Sparkline charts
  var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
  $('#sparkline-1').sparkline(myvalues, {
    type     : 'line',
    lineColor: '#92c1dc',
    fillColor: '#ebf4f9',
    height   : '50',
    width    : '80'
  });
  myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
  $('#sparkline-2').sparkline(myvalues, {
    type     : 'line',
    lineColor: '#92c1dc',
    fillColor: '#ebf4f9',
    height   : '50',
    width    : '80'
  });
  myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
  $('#sparkline-3').sparkline(myvalues, {
    type     : 'line',
    lineColor: '#92c1dc',
    fillColor: '#ebf4f9',
    height   : '50',
    width    : '80'
  });

  // The Calender
  $('#calendar').datepicker();

  // SLIMSCROLL FOR CHAT WIDGET
  $('#chat-box').slimScroll({
    height: '250px'
  });
  

  // Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
    donut.redraw();
    line.redraw();
  });

  /* The todo list plugin */
  $('.todo-list').todoList({
    onCheck  : function () {
      window.console.log($(this), 'The element has been checked');
    },
    onUnCheck: function () {
      window.console.log($(this), 'The element has been unchecked');
    }
  });

  overviewCharts('revenue-chart');
  overviewBarchart('Order');
  customerDounoughtChart();
  countryDataBarChart();

  $('ul.Overview li.overview-tab').click(function(){
    var liId = $(this).find('a').attr('href');
    overviewCharts(liId.substr(1));
  });

  $('ul.service-overview li.service-overview-tab').click(function(){
    var liId = $(this).find('a').attr('href');
    overviewBarchart(liId.substr(1));
  });


});

/* dynamic date config */
var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  var d = new Date();
function getMonths(year)
{
  var monthsThisYear = [];
  for(var i = 0;i<12;i++)
  {
    monthsThisYear.push(MONTHS[i]);
    if(i==d.getMonth())
    {
      break;
    }
  }

  return monthsThisYear; 
}
/* End */


/* Revenue line graph config */
    
    var config = {
      type: 'line',
      data: {
        labels: getMonths(d.getFullYear()),
        datasets: [{
          label: 'Total sales : '+d.getFullYear(),
          backgroundColor: window.chartColors.red,
          borderColor: window.chartColors.blue,
          data: [10, 45, 21, 35, 15, 45, 50],
          fill: false,
        }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'This year revenue'
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Month'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Value'
            }
          }]
        }
      }
    };

/* config end */    

/* dynamic overview line charts */

  function overviewCharts(selectedLi) {

    var ctx = document.getElementById(selectedLi+'-data').getContext('2d');
    if(selectedLi == 'revenue-chart')
    {
      config.options.title.text = "This year revenue";
      config.data.datasets = [];
      config.data.datasets.push({
        label: 'Total shipments : '+d.getFullYear(),
        backgroundColor: window.chartColors.red,
        borderColor: window.chartColors.blue,
        data: [10, 45, 21, 35, 15, 45, 50],
        fill: false,
      });
    }
    else if(selectedLi == 'shipment-chart')
    {
      config.options.title.text = "This year shipments";
      config.data.datasets = [];
      config.data.datasets.push({
        label: 'Total shipments : '+d.getFullYear(),
        backgroundColor: window.chartColors.red,
        borderColor: window.chartColors.blue,
        data: [1000, 955, 920, 935, 945, 910, 900],
        fill: false,
      });
    }
    else if(selectedLi == 'customer-chart')
    {
      config.options.title.text = "This year new customers";
      config.data.datasets = [];
      config.data.datasets.push({
        label: 'Total new customers : '+d.getFullYear(),
        backgroundColor: window.chartColors.red,
        borderColor: window.chartColors.blue,
        data: [1000, 1200, 1300, 1155, 945, 910, 900],
        fill: false,
      });
    }
    window.myLine = new Chart(ctx, config);
  }

  /* line chart end */

  /* Doughnut chart start */

  var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };

    var dounoughtConfig = {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [
            10,
            15,
            8,
            20,
            7,
            9,
            5,
            12,
            5,
            12
          ],
          backgroundColor: [
            "#a41b08",
            "#f66955",
            "#537b25",
            "#00a65a",
            "#6475e1",
            "#f39c11",
            "#303e3f",
            "#00c0ef",
            "#9761ad",
            "#3c8dbc",
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'France',
          'USA',
          'Kenya',
          'Nigeria',
          'Georgia',
          'Canada',
          'Australia',
          'UK',
          'France',
          'Others'

        ]
      },
      options: {
        responsive: true,
        legend: {
          position: 'top',
        },
        title: {
          display: false,
          text: 'Chart.js Doughnut Chart'
        },
        animation: {
          animateScale: true,
          animateRotate: true
        }
      }
    };

    function customerDounoughtChart() {
      var ctx = document.getElementById('countrywise-customer').getContext('2d');
      window.myDoughnut = new Chart(ctx, dounoughtConfig);
    };

  /* Doughnut chart end */  

  /* barchart area for destination country start */

  var color = Chart.helpers.color;
    var barChartData = {
      labels: ['USA', 'Nigeria', 'Ghana', 'Kenya', 'Australia','Canada', 'UK','France','Georgia','Others'],
      datasets: [{
        label: 'Air Shipment',
        backgroundColor: '#4b9ece',
        borderColor: '#4b9ece',
        borderWidth: 1,
        data: [
          100,90,95,45,40,89,75,79,97,25,85
        ]
      }, {
        label: 'Sea Shipment',
        backgroundColor: '#76d1da',
        borderColor: '#76d1da',
        borderWidth: 1,
        data: [
          90,35,40,85,65,65,49,102,47,78,35
        ]
      }]

    };

    function countryDataBarChart() {
      var ctx = document.getElementById('order-by-country').getContext('2d');
      window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
          responsive: true,
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'Orders Completed'
          }
        }
      });

    };

  /* barchart area end */


  /* barchart area for destination country start */

  var color = Chart.helpers.color;
    var barChartOverviewData = {
      labels: getMonths(d.getFullYear()),
      datasets: [{
        label: 'Procurement',
        backgroundColor: '#60b7b1',
        borderColor: '#60b7b1',
        borderWidth: 1,
        data: [
          100,90,95,45,40,89,75,79,97,25,85
        ]
      }, {
        label: 'Fill & ship',
        backgroundColor: '#bcf0ff',
        borderColor: '#bcf0ff',
        borderWidth: 1,
        data: [
          90,35,40,85,65,65,49,102,47,78,35
        ]
      }, {
        label: 'Auto',
        backgroundColor: '#ffdb88',
        borderColor: '#ffdb88',
        borderWidth: 1,
        data: [
          90,35,40,85,65,65,49,102,47,78,35
        ]
      }]

    };

    function overviewBarchart(tabId) {
      var ctx = document.getElementById(tabId+'-overview').getContext('2d');
      if(tabId == 'Order')
      {
        window.myBar = new Chart(ctx, {
          type: 'bar',
          data: barChartOverviewData,
          options: {
            responsive: true,
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Orders Completed'
            }
          }
        });
      }
      else if(tabId == 'Shipment')
      {
        window.myBar = new Chart(ctx, {
          type: 'bar',
          data: barChartOverviewData,
          options: {
            responsive: true,
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Shipments Completed'
            }
          }
        });
      }
      else if(tabId == 'Revenue')
      {
        window.myBar = new Chart(ctx, {
          type: 'bar',
          data: barChartOverviewData,
          options: {
            responsive: true,
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Revenues Completed'
            }
          }
        });
      }

    };

  /* barchart area end */


    


