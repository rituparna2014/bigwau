$(function () {
    /*CKEDITOR.replace('promoText')
     $('.textarea').wysihtml5()
     
     CKEDITOR.replace('promoDetailed')
     $('.textarea').wysihtml5()
     
     CKEDITOR.replace('promoCar')
     $('.textarea').wysihtml5()
     
     CKEDITOR.replace('promoTotal')
     $('.textarea').wysihtml5()*/

    CKEDITOR.replace('editor1', {
        allowedContent: true,
//        toolbar: [
//            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
//            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
//            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
//        ]
    });

    CKEDITOR.replace('editor2', {
        allowedContent: true,
//        toolbar: [
//            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
//            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
//            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
//        ]
    });

    CKEDITOR.replace('editor3', {
        allowedContent: true,
//        toolbar: [
//            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
//            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
//            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
//        ]
    });


    CKEDITOR.replace('editor4', {
        allowedContent: true,
//        toolbar: [
//            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
//            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
//            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
//        ]
    });
})

//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
})

//tooltip
//$('[data-toggle="tooltip"]').tooltip(); 


//show notes
$(".showOnlyNote").hide();
$("#showNote").click(function () {
    $(".showOnlyNote").toggle();
})

$(".noShow").click(function () {
    $(".showOnlyNote").hide();
})







//Date range picker
$(function () {
    $('#startDate').daterangepicker();
    $('#endDate').daterangepicker();
    $('#resDate').daterangepicker();
    $('#purDate').daterangepicker();
})

/*$(document).ready(function () {
 var brand = document.getElementById('logo-id');
 brand.className = 'attachment_upload';
 brand.onchange = function () {
 document.getElementById('fakeUploadLogo').value = this.value.substring(12);
 };
 
 // Source: http://stackoverflow.com/a/4459419/6396981
 function readURL(input) {
 if (input.files && input.files[0]) {
 var reader = new FileReader();
 
 reader.onload = function (e) {
 $('.img-preview').attr('src', e.target.result);
 $('.closeImg').show();
 };
 reader.readAsDataURL(input.files[0]);
 }
 }
 $("#logo-id").change(function () {
 readURL(this);
 });
 });*/

$(document).ready(function () {
    if ($("#checkedCondition1").val() == 1) {
        $("#conditions_1").css('display', 'block');
    }
    if ($("#checkedCondition5").val() == 5) {
        $("#conditions_5").css('display', 'block');
    }
    if ($("#checkedCondition4").val() == 4) {
        $("#conditions_4").css('display', 'block');
    }
    if ($("#checkedCondition2").val() == 2) {
        $("#conditions_2").css('display', 'block');
    }
    if ($("#checkedCondition6").val() == 6) {
        $("#conditions_6").css('display', 'block');
    }
    if ($("#checkedCondition7").val() == 7) {
        $("#conditions_7").css('display', 'block');
    }
    if ($("#checkedCondition8").val() == 8) {
        $("#conditions_8").css('display', 'block');
    }
    if ($("#checkedCondition9").val() == 9) {
        $("#conditions_9").css('display', 'block');
    }
    if ($("#checkedCondition10").val() == 10) {
        $("#conditions_10").css('display', 'block');
    }
});

$(document).ready(function () {

    // disabled the next button on conditions tab
    if ($("#isSetCondition").val() == 1) {
        $("#conditionNextButton").css("display", "block");
    }

    //var selectedradio = $('input[name=condition_tab]:checked').val();
    $('input[name=condition_tab]').on('ifChecked', function (event) {

        var selectedradio = $(this).val();

        if (selectedradio == 1) {
            //$("#conditions_4").css('display', 'none');
            $("#conditions_1").css('display', 'block');
            //$("#conditions_2").css('display', 'none');
            //$("#conditions_3").css('display', 'none');
            //$("#conditions_5").css('display', 'none');
            //$("#conditions_7").css('display', 'none');
            //$("#conditions_6").css('display', 'none');
            //$("#conditions_8").css('display', 'none');
            //$("#conditions_9").css('display', 'none');
            //$("#formCondition1").css('display', 'none');
            //$("#conditions_10").css('display', 'none');
        }


        if (selectedradio == 5) {
            //$("#conditions_4").css('display', 'none');
            //$("#conditions_1").css('display', 'none');
            //$("#conditions_2").css('display', 'none');
            //$("#conditions_3").css('display', 'none');
            $("#conditions_5").css('display', 'block');
            //$("#conditions_7").css('display', 'none');
            //$("#conditions_6").css('display', 'none');
            //$("#conditions_8").css('display', 'none');
            //$("#conditions_9").css('display', 'none');
            //$("#formCondition1").css('display', 'none');
            //$("#conditions_10").css('display', 'none');
        }

        if (selectedradio == 2) {
            $("#conditions_2").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if (selectedradio == 4) {
            $("#conditions_4").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }

        if (selectedradio == 6) {
            $("#conditions_6").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }

        if (selectedradio == 7) {
            $("#conditions_7").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }

        if (selectedradio == 9) {
            $("#conditions_9").css('display', 'block');
            /*$("#conditions_8").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }

        if (selectedradio == 8) {
            $("#conditions_8").css('display', 'block');
            /*$("#conditions_7").css('display', 'none');
             $("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }

        if (selectedradio == 10) {
            /*$("#conditions_8").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');*/
            $("#conditions_10").css('display', 'block');
        }


    });
    // Bonus tab selected
    var selectedradio2 = $('input[name=bonus_tab]:checked').val();
    if (selectedradio2 == 1) {
        $("#bonus_1").css('display', 'block');
        $("#bonus_2").css('display', 'none');
        $("#bonus_3").css('display', 'none');
        $("#bonus_4").css('display', 'none');
        $("#bonus_5").css('display', 'none');
        $("#formCondition1").css('display', 'block');
    }
    if (selectedradio2 == 2) {
        $("#bonus_2").css('display', 'block');
        $("#bonus_1").css('display', 'none');
        $("#bonus_3").css('display', 'none');
        $("#bonus_4").css('display', 'none');
        $("#bonus_5").css('display', 'none');
        $("#formCondition1").css('display', 'none');
    }


});


$(document).ready(function () {
    if ($("#isCouponSet").val() == 10000) {
        $("#couponCode").removeAttr("readonly");
    }
    $('.createCoupon').on('ifChecked', function () {
        $(this).closest('.form-group').find('input[type=text]').removeAttr("readonly");
    });
    $('.createCoupon').on('ifUnchecked', function () {
        $(this).closest('.form-group').find('input[type=text]').attr("readonly", true);
    });

    //alert($('input[name=condition_tab]:checked').val()); // get the value of selected checkbox's value
    //Newly Added for conditions
    $('input:checkbox[name=condition_tab]').on('ifUnchecked', function () {
        var thisOfferId = $("#thisOfferId").val();
        if (thisOfferId) {
            deleteSingleRowCondition(thisOfferId, $(this).val());
        }
        if ($(this).val() == '1') {
            $("#conditions_1").css('display', 'none');

        }

        if ($(this).val() == '5') {
            $("#conditions_5").css('display', 'none');
        }

        if ($(this).val() == '2') {
            $("#conditions_2").css('display', 'none');
        }

        if ($(this).val() == '4') {
            $("#conditions_4").css('display', 'none');
        }

        if ($(this).val() == '6') {
            $("#conditions_6").css('display', 'none');
        }

        if ($(this).val() == '7') {
            $("#conditions_7").css('display', 'none');
        }

        if ($(this).val() == '8') {
            $("#conditions_8").css('display', 'none');
        }

        if ($(this).val() == '9') {
            $("#conditions_9").css('display', 'none');
        }

        if ($(this).val() == '10') {
            $("#conditions_10").css('display', 'none');
        }

    });

    $('input:checkbox[name=condition_tab]').on('ifChanged', function () {
        //console.log($(this).val());
        $("#condition_tab_no").val($(this).val());
        //$("input:radio[name=bonus_tab]").iCheck('uncheck');
        //$("#condition_tab_no_2").val($(this).val());
        if ($(this).val() == '1') {
            $("#conditions_1").css('display', 'block');
            /*$("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'block');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '2') {
            $("#conditions_2").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '3') {
            $("#conditions_3").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '4') {
            $("#conditions_4").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '5') {
            $("#conditions_5").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '6') {
            $("#conditions_6").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '7') {
            $("#conditions_7").css('display', 'block');
            /*$("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '8') {
            $("#conditions_8").css('display', 'block');
            /*$("#conditions_7").css('display', 'none');
             $("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#conditions_9").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '9') {
            $("#conditions_9").css('display', 'block');
            /*$("#conditions_8").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#formCondition1").css('display', 'none');
             $("#conditions_10").css('display', 'none');*/
        }
        if ($(this).val() == '10') {
            /*$("#conditions_9").css('display', 'none');
             $("#conditions_8").css('display', 'none');
             $("#conditions_7").css('display', 'none');
             $("#conditions_1").css('display', 'none');
             $("#conditions_2").css('display', 'none');
             $("#conditions_3").css('display', 'none');
             $("#conditions_4").css('display', 'none');
             $("#conditions_5").css('display', 'none');
             $("#conditions_6").css('display', 'none');
             $("#formCondition1").css('display', 'none');*/
            $("#conditions_10").css('display', 'block');
        }
    });

    $('input:radio[name=bonus_tab]').on('ifChanged', function () {
        $("input:radio[name=condition_tab]").iCheck('uncheck');
        if ($(this).val() == '1') {
            $("#bonus_1").css('display', 'block');
            $("#bonus_2").css('display', 'none');
            $("#bonus_3").css('display', 'none');
            $("#bonus_4").css('display', 'none');
            $("#bonus_5").css('display', 'none');
            $("#formCondition1").css('display', 'block');
        }
        if ($(this).val() == '2') {
            $("#bonus_2").css('display', 'block');
            $("#bonus_1").css('display', 'none');
            $("#bonus_3").css('display', 'none');
            $("#bonus_4").css('display', 'none');
            $("#bonus_5").css('display', 'none');
            $("#formCondition1").css('display', 'none');
        }
        if ($(this).val() == '3') {
            $("#bonus_3").css('display', 'block');
            $("#bonus_1").css('display', 'none');
            $("#bonus_2").css('display', 'none');
            $("#bonus_4").css('display', 'none');
            $("#bonus_5").css('display', 'none');
            $("#formCondition1").css('display', 'none');
        }
        if ($(this).val() == '4') {
            $("#bonus_4").css('display', 'block');
            $("#bonus_1").css('display', 'none');
            $("#bonus_2").css('display', 'none');
            $("#bonus_3").css('display', 'none');
            $("#bonus_5").css('display', 'none');
            $("#formCondition1").css('display', 'none');
        }
        if ($(this).val() == '5') {
            $("#bonus_5").css('display', 'block');
            $("#bonus_1").css('display', 'none');
            $("#bonus_2").css('display', 'none');
            $("#bonus_3").css('display', 'none');
            $("#bonus_4").css('display', 'none');
            $("#formCondition1").css('display', 'none');
        }
    });



    $('#condition_step2_chk').on('ifChecked', function (event) {
        $("#condition_step2_input").prop("disabled", false);
    });
    $('#condition_step2_chk').on('ifUnchecked', function (event) {
        $("#condition_step2_input").prop("disabled", true);
    });

    if ($("#fixedOrPer").val() == '1') {
        $("#f_point").prop("disabled", false);
        $("#a_point").prop("disabled", true);
        $("#per_point").prop("disabled", true);
        $("#of_points").prop("disabled", true);
        $("#bonus_apply_prod_set").iCheck('disable');
    } else {
        $("#f_point").prop("disabled", true);
        $("#a_point").prop("disabled", false);
        $("#per_point").prop("disabled", false);
        $("#of_points").prop("disabled", false);
        $("#bonus_apply_prod_set").iCheck('enable');
    }

    $('input:radio[name=bonus_point]').on('ifChanged', function () {
        if ($(this).val() == '1') {
            $("#f_point").prop("disabled", false);
            $("#a_point").prop("disabled", true);
            $("#per_point").prop("disabled", true);
            $("#of_points").prop("disabled", true);
            $("#bonus_apply_prod_set").iCheck('disable');
        }
        if ($(this).val() == '2') {
            $("#f_point").prop("disabled", true);
            $("#a_point").prop("disabled", false);
            $("#per_point").prop("disabled", false);
            $("#of_points").prop("disabled", false);
            $("#bonus_apply_prod_set").iCheck('enable');
        }
    });


    $('#bonus_selected_ship_chk').on('ifChecked', function () {
        $("#selected_shipping").prop("disabled", false);
    });
    $('#bonus_selected_ship_chk').on('ifUnchecked', function () {
        $("#selected_shipping").prop("disabled", true);
    });



    $("#condition1_save").click(function () {
        $("#formCondition1").submit();
        return false;
    });

    $("#condition_step2_save").click(function () {
        $("#formCondition2").submit();
        return false;
    });

    $("#condition_step3_save").click(function () {
        $("#formCondition2").submit();
        return false;
    });

    $("#condition_step4_save").click(function () {
        $("#formCondition2").submit();
        return false;
    });

    $("#condition_step5_save").click(function () {
        $("#formCondition2").submit();
        return false;
    });

    $("#bonus_1_btn_save").click(function () {
        $("#formBonus1").submit();
        return false;
    });

    $("#bonus_step1_update").click(function () {
        $("#formBonus1").submit();
        return false;
    });

    $("#bonus_step2_2_update").click(function () {
        $("#formBonus1").submit();
        return false;
    });

    $("#bonus_step3_save").click(function () {
        $("#formBonus1").submit();
        return false;
    });

    $("#bonus_step3_update").click(function () {
        $("#formBonus1").submit();
        return false;
    });

    $("#bonus_step4_save").click(function () {
        $("#formBonus1").submit();
        return false;
    });

    $("#bonus_step4_update").click(function () {
        $("#formBonus1").submit();
        return false;
    });

    $("#bonus_step5_update").click(function () {
        $("#formBonus1").submit();
        return false;
    });

    $("#offer_step1_update").click(function () {
        $("#formOffer").submit();
        return false;
    });

    $("#promo_step1_update").click(function () {
        $("#formPromo").submit();
        return false;
    });

    $("#step1_condition_save").click(function () {
        $("#formCondition1").submit();
        return false;
    });


    $('.collapse').on('shown.bs.collapse', function () {
        $('.accordion-toggle').find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
    }).on('hidden.bs.collapse', function () {
        $('.accordion-toggle').find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
    });


    var accordian = getUrlParameter('mode');
    var isSetCondition = $("#isSetCondition").val();
    var isSetBonus = $("#isSetBonus").val();
    var isSetPromo = $("#isSetPromo").val();
    var isSetOffer = $("#isSetOffer").val();
    if (accordian == "bonus") {
        if (isSetCondition == 0) {
            window.location = "?mode=conditions";
        }
        $('#collapseOne').removeClass('in');
        $('#collapseTwo').addClass('in');
        $('#collapseThree').removeClass('in');
        $('#collapseFour').removeClass('in');
        $('.accordion-toggle').find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
        $('#collapseOneMain').addClass('collapsed');
        $('#collapseTwoMain').removeClass('collapsed');
        $('#showNote').addClass('collapsed');
        $('#collapseFourMain').addClass('collapsed');

    }
    if (accordian == "conditions") {
        $('#collapseOne').addClass('in');
        $('#collapseTwo').removeClass('in');
        $('#collapseThree').removeClass('in');
        $('#collapseFour').removeClass('in');
    }
    if (accordian == "promo") {
        if (isSetBonus == 0) {
            window.location = "?mode=bonus";
        }
        $('#collapseOne').removeClass('in');
        $('#collapseTwo').removeClass('in');
        $('#collapseThree').addClass('in');
        $('#collapseFour').removeClass('in');
        $(".showOnlyNote").toggle();
        $('#collapseOneMain').addClass('collapsed');
        $('#collapseTwoMain').addClass('collapsed');
        $('#showNote').removeClass('collapsed');
        $('#collapseFourMain').addClass('collapsed');
    }
    if (accordian == "offer") {
        if (isSetPromo == 0) {
            window.location = "?mode=promo";
        }
        $('#collapseOne').removeClass('in');
        $('#collapseTwo').removeClass('in');
        $('#collapseThree').removeClass('in');
        $('#collapseFour').addClass('in');
        $('#collapseOneMain').addClass('collapsed');
        $('#collapseTwoMain').addClass('collapsed');
        $('#showNote').addClass('collapsed');
        $('#collapseFourMain').removeClass('collapsed');
    }

});

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
;

function previous_func() {
    var accordion = getUrlParameter('mode');
    if (accordion == '') {
        return false;
    } else if (accordion == 'conditions') {
        return false;
    } else if (accordion == 'bonus') {
        window.location = "?mode=conditions";
    } else if (accordion == 'promo') {
        window.location = "?mode=bonus";
    } else if (accordion == 'offer') {
        window.location = "?mode=promo";
    }
}



// Newly Created function for Submit buttons under Conditions section

function submit_condition_2() {
    if ($("#discountTotalAmountMinimum").val() == '' || $("#discountTotalAmountMinimum").val() == '0.00' || $("#discountTotalAmountMinimum").val() == '0') {
        $("#discountTotalAmountMinimumAlert").css('display', 'block');
        $("#discountTotalAmountMinimumAlert").css('color', 'red');
        $("#discountTotalAmountMinimumAlert").addClass('error');
        $("#discountTotalAmountMinimumAlert").text('Enter value');
        return false;
    }

    if ($("#discountTotalAmountMaximum").val() == '' || $("#discountTotalAmountMaximum").val() == '0.00' || $("#discountTotalAmountMaximum").val() == '0') {
        $("#discountTotalAmountMaximumAlert").css('display', 'block');
        $("#discountTotalAmountMaximumAlert").css('color', 'red');
        $("#discountTotalAmountMaximumAlert").addClass('error');
        $("#discountTotalAmountMaximumAlert").text('Enter value');
        return false;
    }

    if (parseFloat($("#discountTotalAmountMinimum").val()) >= parseFloat($("#discountTotalAmountMaximum").val())) {
        $("#discountTotalAmountMinimumAlert").css('display', 'none');
        $("#discountTotalAmountMaximumAlert").css('display', 'block');
        $("#discountTotalAmountMaximumAlert").css('color', 'red');
        $("#discountTotalAmountMaximumAlert").addClass('error');
        $("#discountTotalAmountMaximumAlert").text('Min value is greater than or equal the Max value');
        return false;
    }


    if ($("#discountTotalAmountMinimum").val() != '' && $("#discountTotalAmountMaximum").val() != '') {
        $('<input />').attr('type', 'hidden')
                .attr('name', "condition_tab_no")
                .attr('value', '2')
                .appendTo('#discountTotalAmountShipping');
        $("#discountTotalAmountShipping").submit();
    }
}

function submit_condition_4() {

    $('<input />').attr('type', 'hidden')
            .attr('name', "condition_tab_no")
            .attr('value', '4')
            .appendTo('#customNotPurchasedWithin');
    $("#customNotPurchasedWithin").submit();

}

function submit_condition_6() {
    if ($("#store").val() == '') {
        $("#customerPurchasedStoreAlert").css('display', 'block');
        $("#customerPurchasedStoreAlert").css('color', 'red');
        $("#customerPurchasedStoreAlert").addClass('error');
        $("#customerPurchasedStoreAlert").text('Select a store');
        return false;
    } else {
        $('<input />').attr('type', 'hidden')
                .attr('name', "condition_tab_no")
                .attr('value', '6')
                .appendTo('#CustomerPurchasedStore');
        $("#CustomerPurchasedStore").submit();
    }
}


function submit_condition_7() {
    if ($("#weightFrom_DiscountTotalWeightShipping").val() == '' || $("#weightFrom_DiscountTotalWeightShipping").val() == '0.00' || $("#weightFrom_DiscountTotalWeightShipping").val() == '0') {
        $("#weightFrom_DiscountTotalWeightShippingAlert").css('display', 'block');
        $("#weightFrom_DiscountTotalWeightShippingAlert").css('color', 'red');
        $("#weightFrom_DiscountTotalWeightShippingAlert").addClass('error');
        $("#weightFrom_DiscountTotalWeightShippingAlert").text('Enter value');
        return false;
    }

    if ($("#weightTo_DiscountTotalWeightShipping").val() == '' || $("#weightTo_DiscountTotalWeightShipping").val() == '0.00' || $("#weightTo_DiscountTotalWeightShipping").val() == '0') {
        $("#weightTo_DiscountTotalWeightShippingAlert").css('display', 'block');
        $("#weightTo_DiscountTotalWeightShippingAlert").css('color', 'red');
        $("#weightTo_DiscountTotalWeightShippingAlert").addClass('error');
        $("#weightTo_DiscountTotalWeightShippingAlert").text('Enter value');
        return false;
    }

    if (parseFloat($("#weightFrom_DiscountTotalWeightShipping").val()) >= parseFloat($("#weightTo_DiscountTotalWeightShipping").val())) {
        $("#weightFrom_DiscountTotalWeightShippingAlert").css('display', 'none');
        $("#weightTo_DiscountTotalWeightShippingAlert").css('display', 'none');
        $("#weightTo_DiscountTotalWeightShippingAlert").css('display', 'block');
        $("#weightTo_DiscountTotalWeightShippingAlert").css('color', 'red');
        $("#weightTo_DiscountTotalWeightShippingAlert").addClass('error');
        $("#weightTo_DiscountTotalWeightShippingAlert").text('Weight from is greater than or equal the Weight to');
        return false;
    }


    if ($("#weightFrom_DiscountTotalWeightShipping").val() != '' && $("#weightTo_DiscountTotalWeightShipping").val() != '') {
        $('<input />').attr('type', 'hidden')
                .attr('name', "condition_tab_no")
                .attr('value', '7')
                .appendTo('#DiscountTotalWeightShipping');
        $("#DiscountTotalWeightShipping").submit();
    }
}

function submit_condition_8() {
    if ($("#shippingMethod").val() == '') {
        $("#shippingMethodAlert").css('display', 'block');
        $("#shippingMethodAlert").css('color', 'red');
        $("#shippingMethodAlert").addClass('error');
        $("#shippingMethodAlert").text('Select a shipping method');
        return false;
    } else {
        $('<input />').attr('type', 'hidden')
                .attr('name', "condition_tab_no")
                .attr('value', '8')
                .appendTo('#SpecificShippingMethod');
        $("#SpecificShippingMethod").submit();
    }
}

function submit_condition_9() {
    if ($("#durationspecificcustomer").val() == '') {
        $("#durationspecificcustomerAlert").css('display', 'block');
        $("#durationspecificcustomerAlert").css('color', 'red');
        $("#durationspecificcustomerAlert").addClass('error');
        $("#durationspecificcustomerAlert").text('Enter value');
        return false;
    }

    if ($("#weightFromspecificcustomer").val() == '' || $("#weightFromspecificcustomer").val() == '0.00' || $("#weightFromspecificcustomer").val() == '0') {
        $("#weightFromspecificcustomerAlert").css('display', 'block');
        $("#weightFromspecificcustomerAlert").css('color', 'red');
        $("#weightFromspecificcustomerAlert").addClass('error');
        $("#weightFromspecificcustomerAlert").text('Enter value');
        return false;
    }

    if ($("#weightTospecificcustomer").val() == '' || $("#weightTospecificcustomer").val() == '0.00' || $("#weightTospecificcustomer").val() == '0') {
        $("#weightTospecificcustomerAlert").css('display', 'block');
        $("#weightTospecificcustomerAlert").css('color', 'red');
        $("#weightTospecificcustomerAlert").addClass('error');
        $("#weightTospecificcustomerAlert").text('Enter value');
        return false;
    }

    if (parseFloat($("#weightFromspecificcustomer").val()) >= parseFloat($("#weightTospecificcustomer").val())) {
        $("#weightFromspecificcustomerAlert").css('display', 'none');
        $("#weightTospecificcustomerAlert").css('display', 'none');
        $("#weightTospecificcustomerAlert").css('display', 'block');
        $("#weightTospecificcustomerAlert").css('color', 'red');
        $("#weightTospecificcustomerAlert").addClass('error');
        $("#weightTospecificcustomerAlert").text('Weight from is greater than or equal the Weight to');
        return false;
    }


    if ($("#durationspecificcustomer").val() != '' && $("#weightFromspecificcustomer").val() != '' && $("#weightTospecificcustomer").val() != '') {
        $('<input />').attr('type', 'hidden')
                .attr('name', "condition_tab_no")
                .attr('value', '9')
                .appendTo('#SpecificCustomer');
        $("#SpecificCustomer").submit();
    }
}

function submit_condition_10() {
    $('<input />').attr('type', 'hidden')
            .attr('name', "condition_tab_no")
            .attr('value', '10')
            .appendTo('#registerApp');
    $("#registerApp").submit();
}



function next_func() {
    var _token = $("input[name='_token']").val();
    var base_path_offers = $("input[name='base_path_offers']").val();
    var offerId = $("#thisOfferId").val();
    $.ajax({
        url: base_path_offers + "checkNumberOfCondition",
        type: 'POST',
        data: {_token: _token, offerId: offerId},
        success: function (data) {
            if (data.success != 0) {
                window.location.href = '?mode=bonus';
            }
            // Do Nothing
            if ($.isEmptyObject(data.error)) {
            } else {
                printErrorMsg(data.error);
            }
        }
    });

    //window.location.href='?mode=bonus';
}

/*
 function next_func() {
 
 var selectedradio = $('input[name=condition_tab]:checked').val();
 if (selectedradio == 1) {
 window.location.href='?mode=bonus';
 }
 
 if (selectedradio == 2) {
 //$("#discountTotalAmountShipping").validate();
 if ($("#discountTotalAmountMinimum").val() == '') {
 $("#discountTotalAmountMinimumAlert").css('display', 'block');
 $("#discountTotalAmountMinimumAlert").css('color', 'red');
 $("#discountTotalAmountMinimumAlert").addClass('error');
 $("#discountTotalAmountMinimumAlert").text('Enter value');
 return false;
 }
 
 if ($("#discountTotalAmountMaximum").val() == '') {
 $("#discountTotalAmountMaximumAlert").css('display', 'block');
 $("#discountTotalAmountMaximumAlert").css('color', 'red');
 $("#discountTotalAmountMaximumAlert").addClass('error');
 $("#discountTotalAmountMaximumAlert").text('Enter value');
 return false;
 }
 if ($("#discountTotalAmountMinimum").val() != '' && $("#discountTotalAmountMaximum").val() != '') {
 $('<input />').attr('type', 'hidden')
 .attr('name', "condition_tab_no")
 .attr('value', $("#condition_tab_no").val())
 .appendTo('#discountTotalAmountShipping');
 $("#discountTotalAmountShipping").submit();
 }
 }
 
 
 if (selectedradio == 4) {
 $('<input />').attr('type', 'hidden')
 .attr('name', "condition_tab_no")
 .attr('value', $("#condition_tab_no").val())
 .appendTo('#customNotPurchasedWithin');
 $("#customNotPurchasedWithin").submit();
 }
 
 if (selectedradio == 5) {
 window.location.href='?mode=bonus';
 }
 
 if (selectedradio == 6) {
 if ($("#store").val() == '') {
 $("#customerPurchasedStoreAlert").css('display', 'block');
 $("#customerPurchasedStoreAlert").css('color', 'red');
 $("#customerPurchasedStoreAlert").addClass('error');
 $("#customerPurchasedStoreAlert").text('Select a store');
 return false;
 } else {
 $('<input />').attr('type', 'hidden')
 .attr('name', "condition_tab_no")
 .attr('value', $("#condition_tab_no").val())
 .appendTo('#CustomerPurchasedStore');
 $("#CustomerPurchasedStore").submit();
 }
 }
 
 
 if (selectedradio == 7) {
 if ($("#weightFrom_DiscountTotalWeightShipping").val() == '') {
 $("#weightFrom_DiscountTotalWeightShippingAlert").css('display', 'block');
 $("#weightFrom_DiscountTotalWeightShippingAlert").css('color', 'red');
 $("#weightFrom_DiscountTotalWeightShippingAlert").addClass('error');
 $("#weightFrom_DiscountTotalWeightShippingAlert").text('Enter value');
 return false;
 }
 
 if ($("#weightTo_DiscountTotalWeightShipping").val() == '') {
 $("#weightTo_DiscountTotalWeightShippingAlert").css('display', 'block');
 $("#weightTo_DiscountTotalWeightShippingAlert").css('color', 'red');
 $("#weightTo_DiscountTotalWeightShippingAlert").addClass('error');
 $("#weightTo_DiscountTotalWeightShippingAlert").text('Enter value');
 return false;
 }
 if ($("#weightFrom_DiscountTotalWeightShipping").val() != '' && $("#weightTo_DiscountTotalWeightShipping").val() != '') {
 $('<input />').attr('type', 'hidden')
 .attr('name', "condition_tab_no")
 .attr('value', $("#condition_tab_no").val())
 .appendTo('#DiscountTotalWeightShipping');
 $("#DiscountTotalWeightShipping").submit();
 }
 }
 
 
 
 
 if (selectedradio == 9) {
 if ($("#durationspecificcustomer").val() == '') {
 $("#durationspecificcustomerAlert").css('display', 'block');
 $("#durationspecificcustomerAlert").css('color', 'red');
 $("#durationspecificcustomerAlert").addClass('error');
 $("#durationspecificcustomerAlert").text('Enter value');
 return false;
 }
 
 if ($("#weightFromspecificcustomer").val() == '') {
 $("#weightFromspecificcustomerAlert").css('display', 'block');
 $("#weightFromspecificcustomerAlert").css('color', 'red');
 $("#weightFromspecificcustomerAlert").addClass('error');
 $("#weightFromspecificcustomerAlert").text('Enter value');
 return false;
 }
 
 if ($("#weightTospecificcustomer").val() == '') {
 $("#weightTospecificcustomerAlert").css('display', 'block');
 $("#weightTospecificcustomerAlert").css('color', 'red');
 $("#weightTospecificcustomerAlert").addClass('error');
 $("#weightTospecificcustomerAlert").text('Enter value');
 return false;
 }
 
 if ($("#durationspecificcustomer").val() != '' && $("#weightFromspecificcustomer").val() != '' && $("#weightTospecificcustomer").val() != '') {
 $('<input />').attr('type', 'hidden')
 .attr('name', "condition_tab_no")
 .attr('value', $("#condition_tab_no").val())
 .appendTo('#SpecificCustomer');
 $("#SpecificCustomer").submit();
 }
 }
 
 
 if (selectedradio == 8) {
 if ($("#shippingMethod").val() == '') {
 $("#shippingMethodAlert").css('display', 'block');
 $("#shippingMethodAlert").css('color', 'red');
 $("#shippingMethodAlert").addClass('error');
 $("#shippingMethodAlert").text('Select a shipping method');
 return false;
 } else {
 $('<input />').attr('type', 'hidden')
 .attr('name', "condition_tab_no")
 .attr('value', $("#condition_tab_no").val())
 .appendTo('#SpecificShippingMethod');
 $("#SpecificShippingMethod").submit();
 }
 }
 
 if (selectedradio == 10) {
 $('<input />').attr('type', 'hidden')
 .attr('name', "condition_tab_no")
 .attr('value', $("#condition_tab_no").val())
 .appendTo('#registerApp');
 $("#registerApp").submit();
 }
 }*/

function next_func_bonus() {

    var selectedradio2 = $('input[name=bonus_tab]:checked').val();

    var cc = $("#couponCode").val();
    cc = cc.trim();



    //var bonusTabNo = $("#bonusTabNo").val();
    //alert(selectedradio2);
    if (selectedradio2 == 1 || selectedradio2 == undefined) {
        selectedradio2 = 1;
        if ($("#perProductDiscount").val() == '') {
            $("#perProductDiscountAlert").css('display', 'block');
            $("#perProductDiscountAlert").css('color', 'red');
            $("#perProductDiscountAlert").addClass('error');
            $("#perProductDiscountAlert").text('Enter value');
            return false;
        }

        if ($("#discountNotExceed").val() == '') {
            $("#discountNotExceedAlert").css('display', 'block');
            $("#discountNotExceedAlert").css('color', 'red');
            $("#discountNotExceedAlert").addClass('error');
            $("#discountNotExceedAlert").text('Enter value');
            return false;
        }

        if (cc == '') {
            $("#couponCodeErrorText").css('display', 'block');
            $("#couponCodeErrorText").css('color', 'red');
            $("#couponCodeErrorText").addClass('error');
            $("#couponCodeErrorText").text('Enter coupon code');

            return false;
        }


        if ($("#perProductDiscount").val() != '' && $("#discountNotExceed").val() != '' && cc != '') {
            $('<input />').attr('type', 'hidden')
                    .attr('name', "bonus_tab_no")
                    .attr('value', selectedradio2)
                    .appendTo('#bonusGiveDiscount');
            $("#bonusGiveDiscount").submit();
        }


    }
    if (selectedradio2 == 2) {
        var selectedCheckbox = $('input[name=bonus_point]:checked').val();

        var cc2 = $("#couponCode2").val();
        cc2 = cc2.trim();



        if (selectedCheckbox == 1) {
            if ($("#f_point").val() == '') {
                $("#bonusPointsFixedAmountAlert").css('display', 'block');
                $("#bonusPointsFixedAmountAlert").css('color', 'red');
                $("#bonusPointsFixedAmountAlert").addClass('error');
                $("#bonusPointsFixedAmountAlert").text('Enter value');
                return false;
            } else if (cc2 == '') {
                $("#couponCodeErrorText2").css('display', 'block');
                $("#couponCodeErrorText2").css('color', 'red');
                $("#couponCodeErrorText2").addClass('error');
                $("#couponCodeErrorText2").text('Enter coupon code');

                return false;
            }

            else {
                $('<input />').attr('type', 'hidden')
                        .attr('name', "bonus_tab_no")
                        .attr('value', selectedradio2)
                        .appendTo('#bonusGivePoint');
                $("#bonusGivePoint").submit();
            }

        }
        if (selectedCheckbox == 2) {
            if ($("#a_point").val() == '') {
                $("#AmountPerSubtotalAlert").css('display', 'block');
                $("#AmountPerSubtotalAlert").css('color', 'red');
                $("#AmountPerSubtotalAlert").addClass('error');
                $("#AmountPerSubtotalAlert").text('Enter value');
                return false;
            }
            if ($("#per_point").val() == '') {
                $("#bonusPerAlert").css('display', 'block');
                $("#bonusPerAlert").css('color', 'red');
                $("#bonusPerAlert").addClass('error');
                $("#bonusPerAlert").text('Enter value');
                return false;
            }
            if (cc2 == '') {
                $("#couponCodeErrorText2").css('display', 'block');
                $("#couponCodeErrorText2").css('color', 'red');
                $("#couponCodeErrorText2").addClass('error');
                $("#couponCodeErrorText2").text('Enter coupon code');

                return false;
            }

            if ($("#a_point").val() != '' && $("#per_point").val() != '' && cc2 != '') {
                $('<input />').attr('type', 'hidden')
                        .attr('name', "bonus_tab_no")
                        .attr('value', selectedradio2)
                        .appendTo('#bonusGivePoint');
                $("#bonusGivePoint").submit();
            }
        }
    }
}

function next_func_promo() {
    $("#PromoForm").submit();
}
function next_func_offer() {
    $("#OfferForm").submit();
}
/*$(document).ready(function() {
 $('#conditionstep3').select2();
 });*/

$('.select2').select2();

$(document).ready(function () {

    // Discount Type

    if ($("#AbsOrPer").val() == 'Percent') {
        $("#usdOrPercentage").addClass('fa-percent');
        $("#usdOrPercentage").removeClass('fa-usd');
        $("#discountNotExceedMainDiv").css("display", "block");
    }

    $("#discountType").on('change', function () {
        if (this.value == 'Absolute') {
            $("#usdOrPercentage").removeClass('fa-percent');
            $("#usdOrPercentage").addClass('fa-usd');
            $("#discountNotExceedMainDiv").css("display", "none");
        } else {
            $("#usdOrPercentage").addClass('fa-percent');
            $("#usdOrPercentage").removeClass('fa-usd');
            $("#discountNotExceedMainDiv").css("display", "block");
        }
    });

    if ($("#discountType").val() == 'Absolute') {
        $("#discountNotExceedMainDiv").css("display", "none");
    } else {
        $("#discountNotExceedMainDiv").css("display", "block");
    }



    $('#condition1_Addproduct').on('change', function () {
        /*if($("#condition1_Addproduct").val() != ''){
         $("#condition1AddProductButton").css("display", "block");
         } else {
         $("#condition1AddProductButton").css("display", "none");
         }*/
        // calling Ajax
        var _token = $("input[name='_token']").val();
        var base_path_offers = $("input[name='base_path_offers']").val();
        $.ajax({
            url: base_path_offers + "getProductsByCategory",
            type: 'POST',
            data: {_token: _token, catId: $(this).val()},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    console.log(data);
                    if (data.length == 0) {
                        $('#showProducts1Label').text('There are no available products under this category.');
                    } else {
                        /*$('#showProducts1Label').text('');
                         var i;
                         var html = '<table id="example2" class="table table-bordered table-hover"><tbody>';
                         for (i = 0; i < data.length; ++i) {
                         //console.log(data[i].productName);
                         if (i % 2 == 0) {
                         html += '</tr><tr>';
                         }
                         html += "<td><input type=\"checkbox\" class=\"flat-red\" onclick=\"checkcond1checkbox();\" name='Selectedproducts1[]' value=" + data[i].id + "> " + data[i].productName + "</td>";
                         }
                         html += '</tbody></table>';*/
                        $('#showProducts1Label').text('');
                        $('#showProducts1Label').append(data);
                    }
                } else {
                    printErrorMsg(data.error);
                }
            }
        });

    });
    $('#countrySelected').on('change', function () {
        var _token = $("input[name='_token']").val();
        var base_path_offers = $("input[name='base_path_offers']").val();

        $.ajax({
            url: base_path_offers + "getStates",
            type: 'POST',
            data: {_token: _token, countryId: $(this).val()},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    //console.log(data);
                    $('#stateSelected').find('option:gt(0)').remove();
                    $.each(data, function (key, entry) {
                        $('#stateSelected').append($('<option></option>').attr('value', entry.id).text(entry.name));
                    });


                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    });

    $('#stateSelected').on('change', function () {
        var _token = $("input[name='_token']").val();
        var base_path_offers = $("input[name='base_path_offers']").val();
        var countrySelectedId = $("#countrySelected").val();

        $.ajax({
            url: base_path_offers + "getCities",
            type: 'POST',
            data: {_token: _token, stateId: $(this).val(), country: countrySelectedId},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    //console.log(data);
                    $('#citySelected').find('option:gt(0)').remove();
                    $.each(data, function (key, entry) {
                        $('#citySelected').append($('<option></option>').attr('value', entry.id).text(entry.name));
                    });


                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    });
});


//Promo Image upload validation
(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

$(function () {
    $('#logo-id').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
        success: function () {
            //alert('Success');


        },
        error: function () {
            alert('Error! File type not supported');
            readURL('error');
            $('#loaded').css('display', 'none');
            $('#defaultLoaded').css('display', 'block');
        }
    });



});

function readURL(input) {
    if (input != 'error') {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#loaded').css('display', 'block');
                $('#loaded').attr('src', e.target.result);
                $('#defaultLoaded').css('display', 'none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $('#loaded').css('display', 'none');
        $('#defaultLoaded').css('display', 'block');
    }
}


$("#logo-id").change(function () {
    //alert(this.files[0]);
    readURL(this);
});

$(document).on("click", "#modal-add-product-main1", function () {
    var dataId = $(this).data('id');
    $(".setno").val(dataId);
    $(".setnocat").val(dataId);
    $("#howru").val($(".setnocat").val());

});

$(document).on("click", "#modal-add-category-main1", function () {
    var dataId = $(this).data('id');
    $(".setnocat").val(dataId);
    $(".setno").val(dataId);

});

$("#modal-add-product-main1").on("hidden.bs.modal", function () {
    // put your default event here
    $("#howru").val($("#howru").val());
    alert($("#howru").val());
});


$(function () {
    $('input[type="checkbox"].prodcheckboxes, input[type="radio"].prodcheckboxes')
            .iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            })
            .on('ifChecked', function (event) {
                //alert(this.value);
                if ($('input[type="checkbox"].prodcheckboxes').filter(":checked").length < 1) {
                    $("#condition1AddProductButton").css("display", "none");
                } else {
                    $("#condition1AddProductButton").css("display", "block");
                }
            })
            .on('ifUnchecked', function (event) {
                //alert(this.value);
                if ($('input[type="checkbox"].prodcheckboxes').filter(":checked").length < 1) {
                    $("#condition1AddProductButton").css("display", "none");
                } else {
                    $("#condition1AddProductButton").css("display", "block");
                }
            });
});

function deleteSingleRowCondition(offerId, rowId) {
    var _token = $("input[name='_token']").val();
    var base_path_offers = $("input[name='base_path_offers']").val();
    $.ajax({
        url: base_path_offers + "deletesinglerowcondition",
        type: 'POST',
        data: {_token: _token, offerId: offerId, rowId: rowId},
        success: function (data) {
            // Do Nothing
            if ($.isEmptyObject(data.error)) {
            } else {
                printErrorMsg(data.error);
            }
        }
    });
}
