$(document).ready(function() {
        $("#password_submit").click(function(e){
            e.preventDefault();


            var _token = $("input[name='_token']").val();
            var old_password = $("input[name='old_password']").val();
            var new_password = $("input[name='new_password']").val();
            var confirm_password = $("input[name='confirm_password']").val();


            $.ajax({
                url: "myProfile/1",
                type:'PUT',
                data: {_token:_token, old_password:old_password, new_password:new_password, confirm_password:confirm_password},
                success: function(data) {
                    if($.isEmptyObject(data.error)){
                        //alert(data.success);
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").css('display','block');
                        $(".print-success-msg").find("ul").append('<li>'+data.success+'</li>');
                        setTimeout(function() {$('#modal-add').modal('hide');}, 3000);
                        setTimeout(function() {$(".print-error-msg").css('display','none'); $(".print-success-msg").css('display','none');}, 3000);
                    }else{
                        printErrorMsg(data.error);
                    }
                }
            });


        }); 


        function printErrorMsg (msg) { //alert(msg);
            $(".print-error-msg").find("ul").html('');
            $(".print-success-msg").css('display','none');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
    });