$(function () {
    /*CKEDITOR.replace('editor2')
     $('.textarea').wysihtml5();*/

    CKEDITOR.replace('editor2', {
        allowedContent: true,
    });
});


function insertAtCaret(areaId, text) {
    if (CKEDITOR.instances[areaId].mode == 'wysiwyg') {
        CKEDITOR.instances[areaId].insertHtml(text);
    } else {
        var caretPos = jQuery('textarea.cke_source')[0].selectionStart;
        var textAreaTxt = jQuery('textarea.cke_source').val();
        jQuery('textarea.cke_source').val(textAreaTxt.substring(0, caretPos) + text + textAreaTxt.substring(caretPos));
    }
}

$(document).ready(function () {
    $("#templateType").on('change', function () {
        var base_path_loadata = $("input[name='base_path_loadata']").val();
        var token_csrf = $("input[name=_token]").val();
        var type = $("#templateType").val();

        if (type != '') {

            $.ajax({
                url: base_path_loadata,
                method: "POST",
                data: {_token: token_csrf, type: type},
                dataType: "text",
                success: function (data)
                {
                    if (data != '')
                    {
                        var options = data;
                        $('#templateKey').children('option:not(:first)').remove();
                        $("#templateKey").append(options);
                    }
                    else
                    {
                        console.log('no data');
                    }
                }
            });
        } else {
            $('#templateKey').children('option:not(:first)').remove();
        }
    });
});


$(document).ready(function () {
// Static page image validation
    (function ($) {
        $.fn.checkFileType = function (options) {
            var defaults = {
                allowedExtensions: [],
                success: function () {
                },
                error: function () {
                }
            };
            options = $.extend(defaults, options);

            return this.each(function () {

                $(this).on('change', function () {
                    var value = $(this).val(),
                            file = value.toLowerCase(),
                            extension = file.substring(file.lastIndexOf('.') + 1);

                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                        options.error();
                        $(this).focus();
                    } else {
                        options.success();

                    }

                });

            });
        };

    })(jQuery);

    $(function () {
        $('#logo-id').checkFileType({
            allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
            success: function () {
                //alert('Success');
                //$("#regBannerSubmit").css('display', 'block');


            },
            error: function () {
                alert('Error! File type not supported');
                readURL('error');
                $('#loaded').css('display', 'none');
                $('#defaultLoaded').css('display', 'block');
                //$("#regBannerSubmit").css('display', 'none');
            }
        });



    });
});
function readURL(input) {
    if (input != 'error') {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#loaded').css('display', 'block');
                $('#loaded').attr('src', e.target.result);
                $('#defaultLoaded').css('display', 'none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $('#loaded').css('display', 'none');
        $('#defaultLoaded').css('display', 'block');
    }
}

$(document).ready(function () {
    $("#logo-id").change(function () {
        //alert(this.files[0]);
        readURL(this);
    });
});