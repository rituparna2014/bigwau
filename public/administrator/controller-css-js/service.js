$(function () {
    var category_id = $("#category_id").val();
    var action_url = $("#action_url").val();
    var subcat_id = $("#subcat_id").val();
    if(action_url == 'Edit'){
        getSubcategories(category_id, subcat_id);
    }
    
    $("#addeditFrm").validate();
    
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    
});

function getSubcategories(catid,subcat_id){
	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});
  	$.ajax({
            url: baseUrl + "/subcategorylist",
            type: 'POST',
            data: {catid: catid,subcat_id:subcat_id},
            success: function (response) {
                $("#subCategory").html(response);
            }
    });
}

//Update on 03-04-2019

function changeStatus(userId, page, status)
{
    var baseUrl = $('#baseUrl').val();
    
    $.ajax({
            url: baseUrl+"/users/changestatus/"+ userId + "/" + page + "/" + status,
            type: 'POST',
            success: function (data) {
                console.log(data);
                if(data.status == 1)
                {
                    $('.showmsg p').text(data.message);
                    $('.showmsg').show();
                    window.location.reload();
                }else{
                    $('.errorsg p').text(data.message);
                    $('.errorsg').show();
                    //window.location.reload();
                }
                // $('.showmsg').text();                            
                       
                // window.location.reload();
            }
        });
}
