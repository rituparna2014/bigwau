//Promo Image upload validation
(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

$(function () {
    $('#logo-id').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
        success: function () {
            //alert('Success');


        },
        error: function () {
            alert('Error! File type not supported');
            readURL('error');
            $('#loaded').css('display', 'none');
            $('#storeIcon').css('display', 'block');
        }
    });

    CKEDITOR.replace('editor2', {
        allowedContent: true,
    });

});

function readURL(input) {
    if (input != 'error') {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#loaded').css('display', 'block');
                $('#loaded').attr('src', e.target.result);
                $('#storeIcon').css('display', 'none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $('#loaded').css('display', 'none');
        $('#storeIcon').css('display', 'block');
    }
}


$("#logo-id").change(function () {
    //alert(this.files[0]);
    readURL(this);
});

function changeCategory(storeType)
{
    $.ajax({
            type: "get",
            dataType: 'json',
            url: baseUrl + '/managestores/getcategory',
            data: {'storeType': storeType},
            success: function (msg) {
                //console.log(msg);
                if (msg)
                {
                    //console.log(msg.data);
                    $('#storeCategory').html('<option value="">Select</option>');
                    $.each(msg, function (i, j) {
                        console.log(j);
                        $('#storeCategory').append('<option value="' + j.id + '">' + j.categoryName + '</option>')
                    });

                }
            }
        });
}

