$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    
    $('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $('.addressBook').on('ifChecked', function (event) {
        $('#addressBook').val($(this).val());
    });

    $('#searchByDate').daterangepicker();

    $("#file-upload").change(function () {
        readURL(this);
    });

    /*Form Validation */
    $("#changeaddressFrm").validate();
    $("#addEditDeliveryFrm").validate();
    /*Form Validation */

    $("#userUnit").keypress(function(e){
        if(e.keyCode == 13)
        {
            userVerification();
        }
    });

    $('.add-more-modal').click(function () {
        if ($("#addEditDeliveryFrm").valid()) {
            var numPackage = $(".deliveryItem").length;
            $('#packageDiv').append('<div id="modal_row_'+numPackage+'" class="deliveryItem">'+$('#modal_first_row').html()+'</div>');
            var storeId = $("#modal_first_row #storeId").val();
            $("#modal_row_"+numPackage+" #storeId").val(storeId);
//            var deliveryCompany = $("#modal_first_row #deliveryCompanyId").val();
//            var deliveredOn = $("#modal_first_row #delivered").val();
//            var trackingNumber = $("#modal_first_row #trackingNumber").val();         
//            $("#modal_row_"+numPackage+" #deliveryCompanyId").val(deliveryCompany);
//            $("#modal_row_"+numPackage+" #delivered").val(deliveredOn);
//            $("#modal_row_"+numPackage+" #trackingNumber").val(trackingNumber);
        }
    });

    //Custom radio button show daterange
    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });


    $("body").on("click",".addButt", function () {
        var table = $(this).parent().prev();
        var firstCategory = table.children('tbody').children("tr").first().find('.packagestore').val();
        table.find('tbody').append('<tr id="test">' + table.children('tfoot').find("#blank_items").html() + '</tr>');
        //alert($(this).parent().prev().children('tbody').children("tr").last().html());
        $(this).parent().prev().children('tbody').children("tr").last().find(".storeId").val(firstCategory);
    });


    $('#addMore').on('click', function () {
        var storeId = $("#tb tr:eq(1) #storeId").val();
        var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
        data.find("input").val('');
        data.find("#storeId").val(storeId);
//        data.find(".itemReceived").prop("disabled", false);
//        data.find(".itemReceived").html('<span style="font-size:10px">Enter Details</span>');
    });


    $("body").on("click", ".remove", function (event) {
        var trIndex = $(this).closest("tr").index();
        if (trIndex > 1) {
            $(this).closest("tr").remove();
        } else {
            alert("Sorry!! Can't remove first row!");
        }
    });



    $("body").on("click", ".removeItem", function (event) {
        $(this).parent().parent().remove();
    });


    $("body").on("click", ".savePackaging", function (event) {
        var link = $(this);
        var updateId = $(this).attr('data-id');
        $(".cb_" + updateId).attr("checked", "checked");
        var data = $("#frmpkgdetails").serializeArray();
        data.push({name: "action", value: 'update'});
        data.push({name: "fromSource", value: "ajax"});
        var url = $("#frmpkgdetails").attr("action");

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            success: function (response) {
                if (response.message == 'success')
                {
                    window.location.reload();
//                    $(link).closest("tr:has(input)").each(function () {
//                        $('.value_field input', this).each(function () {
//                            $(this).parent().hide();
//                            $(this).parent().prev().text($(this).val()).show();
//                        });
//
//                    });
                }
            }
        });

        $(this).hide();
        $(this).prev().show();

    });
    
    $("body").on("click",".paymentStatusUpdate",function (event){
    var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
    if(r)
    {
        $("#updatepayment").submit();
    }
    return false;
});

    $("body").on("click", ".removePackaging", function (event) {
        var link = $(this);
        var deleteId = $(this).attr('data-id');
        //console.log(updateId);
        $(".cb_" + deleteId).attr("checked", "checked");
        var data = $("#frmpkgdetails").serializeArray();
        data.push({name: "action", value: 'delete'});
        data.push({name: "fromSource", value: "ajax"});
        var url = $("#frmpkgdetails").attr("action");

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            success: function (response) {
                if (response.message == 'success')
                {
                    $("#packagingrow_" + deleteId).remove();
                }
            }
        });

    });

    $(document).on('click', '.deletecharge', function () {
        var id = $(this).attr('data-id');
        var shipmentId = $(this).attr('data-shipmentid');
        var deliveryId = $(this).attr('data-deliveryid');
        $.ajax({
            url: baseUrl + "/shipments/deleteothercharge/" + id,
            type: 'GET',
            success: function (response) {
                if (response == 1) {
                    $('#othercharge' + id).remove();
                    $(".deliveryShipingCostBox-"+deliveryId).load(baseUrl + "/shipments/deliveryshippingcost/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    //$("#totalShipmentChrg").load(baseUrl + "/shipments/gettotalcharge/" + shipmentId);
                }
            }
        });
    });
    
    $(document).on('click', '.deletenewcharge', function () {
        var id = $(this).attr('data-id');
        var shipmentId = $(this).attr('data-shipmentid');
        var deliveryId = $(this).attr('data-deliveryid');
        $.ajax({
            url: baseUrl + "/shipments/deleteotherchargenewdelivery/" + id,
            type: 'GET',
            success: function (response) {
                if (response == 1) {
                    $('#othercharge' + id).remove();
//                    $(".deliveryShipingCostBox-"+deliveryId).load(baseUrl + "/shipments/deliveryshippingcost/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
//                    });
                    //$("#totalShipmentChrg").load(baseUrl + "/shipments/gettotalcharge/" + shipmentId);
                }
            }
        });
    });

    $(document).on('click', '.printShippingLabel', function () {
        var action = $("#frmshippinglabel").attr("action");
        var data = $("#frmshippinglabel").serialize();
        $.ajax({
            url: action,
            data: data,
            type: 'POST',
            success: function (response) {
                $("#modal-addEdit").html(response);
            }

        });
        return false;
    });

    $('#addresscountryId').on('change', function () {
        var countryId = $('#addresscountryId').val();
        $('#addressstateId').find('option:gt(0)').remove();
        $('#addresscityId').find('option:gt(0)').remove();

        if (countryId != '') {
            var url = baseUrl + "/users/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addressstateId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

            var url = baseUrl + "/users/getcitylistbycountry/" + countryId;
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    $('#addressstateId').on('change', function () {
        var stateId = $('#addressstateId').val();
        $('#addresscityId').find('option:gt(0)').remove();
        if (stateId != '') {
            var url = baseUrl + "/users/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });
        }
    });

    $('#addressBookId').on('change', function () {
        $('#changeaddressFrm')[0].reset();
        if ($(this).val() != 'new') {
            var id = $(this).val();
            $.getJSON(baseUrl + "/shipments/getaddressbookdetails/" + id, function (data) {
                $('#userAddressBookId').val(id);
                $('#title').val(data.title);
                $('#firstName').val(data.firstName);
                $('#lastName').val(data.lastName);
                $('#email').val(data.email);
                $('#address').val(data.address);
                $('#alternateAddress').val(data.alternateAddress);
                $('#zipcode').val(data.zipcode);
                $('#zipcode').val(data.zipcode);
                $('#phone').val(data.phone);
                $('#alternatePhone').val(data.alternatePhone);
                $('#addresscountryId').val(data.countryId);

                var url = baseUrl + "/users/getstatelist/" + data.countryId;
                // Populate dropdown with list of provinces
                $.getJSON(url, function (response) {
                    $.each(response, function (key, entry) {
                        if (entry.id == data.stateId)
                            $('#addressstateId').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $('#addressstateId').append($('<option></option>').attr('value', entry.id).text(entry.name));

                    })
                });

                var url = baseUrl + "/users/getcitylistbycountry/" + data.countryId;
                $.getJSON(url, function (response) {
                    $.each(response, function (key, entry) {
                        if (data.cityId != null && entry.id == data.cityId)
                            $('#addresscityId').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));

                    })
                });
            });

            $('#changeaddressFrm input').attr('readonly', 'readonly');
            $('#changeaddressFrm select').not('#addressBookId').attr('readonly', 'readonly').attr("style", "pointer-events: none;");

        } else {
            $('#changeaddressFrm input').removeAttr('readonly');
            $('#changeaddressFrm select').removeAttr('readonly').removeAttr("style");
        }
    });

    $('#shipmentMessage').on('change', function () {
        var messageId = $(this).val();
        $('#notify-customer').val(messageId);
    });

    $('#hideFromCustomer').on('change', function () {
        var hideFromCustomer = $(this).val();
        var shipmentId = $('#shipmentId').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/shipments/updateshipment/" + shipmentId,
            type: 'POST',
            data: {hideFromCustomer: hideFromCustomer, field: 'hideFromCustomer'},
            success: function (response) {
                if (response == '1')
                    alert("Information saved successfully!!");
            }
        });
    });
    
    $("body").on("change",".storeId",function () {
        var rootCat = $(this);
        var rootCatVal = rootCat.val();
        if(rootCatVal != '0')
        {
            
            var baseUrl = $('#baseUrl').val();
            //rootCat.parent().parent().find('.siteProduct').html('<option value="">Select</option>');
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + "/shipments/getcategorylist/" + rootCatVal,
                data: {},
                success: function (msg) {
                    //console.log(msg.updated);
                    if (msg)
                    {
                        //$('.siteCategoryId').append($('<option></option>').attr('value', entry.id).text(entry.categoryName));
                        //console.log(msg.data);
                        rootCat.parent().parent().find('.siteCategoryId').html('<option value="0">Not Listed</option>');
                        rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
                        rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
                        //rootCat.parent().parent().find('.subCategory').append('<option value="0">Not Listed</option>');
                        
                        $.each(msg, function (i, j) {
                            //console.log(j);
                            rootCat.parent().parent().find('.siteCategoryId').append('<option value="' + j.id + '">' + j.categoryName + '</option>')
                        });

                    }

                }
            });
        }
        else
        {
            rootCat.parent().parent().find('.siteCategoryId').html('<option value="0">Not Listed</option>');
            rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
            rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
        }
    });
    
    $("body").on("change",".packagestore",function () {
        var rootCat = $(this);
        var rootCatVal = rootCat.val();
        if(rootCatVal != '0')
        {
            
            var baseUrl = $('#baseUrl').val();
            //rootCat.parent().parent().find('.siteProduct').html('<option value="">Select</option>');
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + "/shipments/getcategorylist/" + rootCatVal,
                data: {},
                success: function (msg) {
                    //console.log(msg.updated);
                    if (msg)
                    {
                        //$('.siteCategoryId').append($('<option></option>').attr('value', entry.id).text(entry.categoryName));
                        //console.log(msg.data);
                        rootCat.parent().parent().parent().find('.packagecategory').html('<option value="0">Not Listed</option>');
                        rootCat.parent().parent().parent().find('.packagesubcategory').html('<option value="0">Not Listed</option>');
                        rootCat.parent().parent().parent().find('.packageproduct').html('<option value="0">Not Listed</option>');
                        //rootCat.parent().parent().find('.subCategory').append('<option value="0">Not Listed</option>');
                        
                        $.each(msg, function (i, j) {
                            //console.log(j);
                            rootCat.parent().parent().parent().find('.packagecategory').append('<option value="' + j.id + '">' + j.categoryName + '</option>')
                        });

                    }

                }
            });
        }
        else
        {
            rootCat.parent().parent().find('.siteCategoryId').html('<option value="0">Not Listed</option>');
            rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
            rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
        }
    });
    
    $("body").on("change",".siteCategoryId",function () {
        var rootCat = $(this);
        var rootCatVal = rootCat.val();
        if(rootCatVal != '0')
        {
            var baseUrl = $('#baseUrl').val();
            var ajaxMethod = $('#subcatPage').val();
            //rootCat.parent().parent().find('.siteProduct').html('<option value="">Select</option>');
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + '/' + ajaxMethod + '/' + rootCatVal,
                data: {},
                success: function (msg) {
                    //console.log(msg.updated);
                    if (msg.data)
                    {
                        //console.log(msg.data);
                        rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
                        //rootCat.parent().parent().find('.subCategory').append('<option value="0">Not Listed</option>');
                        
                        $.each(msg.data, function (i, j) {
                            //console.log(j);
                            rootCat.parent().parent().find('.subCategory').append('<option value="' + j.id + '">' + j.category + '</option>')
                        });

                    }

                }
            });
        }
        else
        {
            rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
            rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
        }
    });
    
    $("body").on("change",".subCategory",function () {
        var rootCat = $(this);
        var rootCatVal = rootCat.val();
        var baseUrl = $('#baseUrl').val();
        if(rootCatVal !='0')
        {
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + '/getsiteproduct/' + rootCatVal,
                data: {},
                success: function (msg) {
                    //console.log(msg.updated);
                    if (msg.data)
                    {
                        //console.log(msg.data);
                        rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
                        //rootCat.parent().parent().find('.siteProduct').append('<option value="0">Not Listed</option>');
                        $.each(msg.data, function (i, j) {
                            //console.log(j);
                            rootCat.parent().parent().find('.siteProduct').append('<option value="' + j.id + '">' + j.product + '</option>');
                        });

                    }
                }
            });
        }
        else
        {
            rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
        }
    });
    
    $("body").on("change",".siteProduct",function() {
        var productVal = $(this).val();
        if(productVal !='')
        {
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + '/getsiteproductweight/' + productVal,
                data: {},
                success: function (msg) {
                    if(msg!='0')
                    {
                        var weightOfItem = 0;
                        var totalItem = 0;
                        if($('#totalWeight').html()!=undefined)
                        {
                            weightOfItem = parseInt($('#totalWeight').html())+msg;
                        }
                        else
                            weightOfItem = msg;
                        $('.siteProduct').each(function(){
                            if($(this).val()>0)
                            {
                                totalItem++;
                            }
                        });
                        
                        $("#foundProductDetails").html('<span style="text-color:#e01d44">We found weigth of '+totalItem+' item(s) is: <span id="totalWeight">'+weightOfItem+'</span>lbs <span data-toggle="tooltip" title="Enter dimension of all items together" class="toolTip"><i class="fa fa-question-circle"></i></span></span>');
                    }
                }
            });
        }
    });


});

/* Format Decimal Fields */
$("body").on("blur", ".format", function (event) {
    if ($.isNumeric(this.value)) {
        var newVal = parseFloat(this.value);
        $(this).val(newVal.toFixed(2));
    } else {
        var newVal = 0;
        $(this).val(newVal.toFixed(2));
    }
});

/* Item Edit - Calculate Sub Total per Item / Package on Quantity Change   */
$(document).on('blur', '.quantity', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().prev().find('.productval').val();
    var shipping = $(this).parent().parent().next().find('.productshipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().next().find('.producttotal').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$(document).on('blur', '.quantityOther', function () {

    var qty = $(this).val();
    var value = $(this).parent().parent().prev().find('.productvalOther').val();
    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        $(this).parent().parent().parent().find('.producttotal').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

/*  Item Edit - Calculate Sub Total per Item / Package on Value Change  */
$(document).on('blur', '.productval', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().next().find('.quantity').val();
    var shipping = $(this).parent().parent().next().next().find('.productshipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().next().next().find('.producttotal').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);

    }
});

/*  Item Edit - Calculate Sub Total per Item / Package on Value Change  */
$(document).on('blur', '.productvalOther', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().next().find('.quantityOther').val();

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        $(this).parent().parent().parent().find('.producttotal').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);

    }
});

$(document).on('blur', '.productshipping', function () {
    var shipping = $(this).val();
    var qty = $(this).parent().parent().prev().find('.quantity').val();
    var value = $(this).parent().parent().prev().prev().find('.productval').val();
    if (shipping == '')
        shipping = 0;


    if ($.isNumeric(shipping)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().find('.producttotal').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});


/*  Item Add - Calculate Sub Total per Item / Package on Quantity Change  */
$(document).on('blur', '.qty', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().find('.product-val').val();
    var shipping = $(this).parent().parent().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;
    
    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        //producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

/* Item Add -  Calculate Sub Total per Item / Package  on Value Change */
$(document).on('blur', '.product-val', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().find('.qty').val();
    var shipping = $(this).parent().parent().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(value)) {
        var producttotal = parseFloat(qty * value);
        //producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

/*  Item Add - Calculate Sub Total per Item / Package on Quantity Change  */
$(document).on('blur', '.qtyAddDelivery', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().parent().find('.product-val-addDelivery').val();
    /*var shipping = $(this).parent().parent().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;*/
    
    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        //producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().parent().find('.product-total-addDelivery').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

/* Item Add -  Calculate Sub Total per Item / Package  on Value Change */
$(document).on('blur', '.product-val-addDelivery', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().parent().find('.qtyAddDelivery').val();
    /*var shipping = $(this).parent().parent().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;*/

    if ($.isNumeric(value)) {
        var producttotal = parseFloat(qty * value);
        //producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().parent().find('.product-total-addDelivery').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$(document).on('blur', '.product-shipping', function () {
    var shipping = $(this).val();
    var qty = $(this).parent().parent().find('.qty').val();
    var value = $(this).parent().parent().find('.product-val').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(shipping)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.product-total').val((producttotal.toFixed(2)));
    } else {
        $(this).val(0);
    }
});

/* Add New Delivery Item */
$("body").on("click", ".addDeliveryItem", function (event) {
    $(this).attr('disabled', true);
    var err = false;
    var inputArray = [];
    var shipmentId = $('#shipmentId').val();

    $(this).closest("tr:has(input)").each(function () {

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else {
                    if ($(this).hasClass('qty') && $(this).val() < 1)
                    {
                        err = true;
                        alert("Enter value greater than 0 for " + $(this).attr('data-name'));
                    } else if ($(this).hasClass('product-val-min'))
                    {
                        var maxVal = $(this).closest("tr:has(input)").find('.product-val').val();
                        if (parseFloat($(this).val()) > parseFloat(maxVal))
                        {
                            err = true;
                            alert("Product Minimum value can not be greater than product maximum value");
                        }
                    } else if ($(this).hasClass('product-val'))
                    {
                        var minVal = $(this).closest("tr:has(input)").find('.product-val-min').val();
                        if (parseFloat($(this).val()) < parseFloat(minVal))
                        {
                            err = true;
                            alert("Product Maximum value can not be lower than product minimum value");
                        }
                    }
                    if (!err)
                    {
                        inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                    }
                }
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/shipments/adddeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                alert('Data saved successfully');
                $(this).find('.editPackage').attr('disabled','disabled');
                $("#packageList").load(baseUrl + "/shipments/getpackagedetails/" + response);
                $("#infoBoxDetail").load(baseUrl + "/shipments/getshipmentdetails/" + shipmentId);
            }
        });
    }

});

/* Show Edit Delivery Item Form */
$("body").on("click", ".editPackage", function (event) {
    $(this).closest("tr").each(function () {

        $('.packagecategory', this).each(function () {
            var categoryId = $(this).val();
            var category = $(this);
            var subcategoryId = $(this).parent().parent().next().find('.packagesubcategoryId').val();
            var productId = $(this).parent().parent().next().next().find('.packageproductId').val();

            // Populate dropdown with list of subcategories
            $(this).parent().parent().next().find('.packagesubcategory').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/shipments/getsubcategorylist/" + categoryId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (subcategoryId == entry.id)
                            $(category).parent().parent().next().find('.packagesubcategory').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.category));
                        else
                            $(category).parent().parent().next().find('.packagesubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
                    });
                }
            });

            // Populate dropdown with list of products
            $(this).parent().parent().next().next().find('.packageproduct').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/shipments/getproductlist/" + subcategoryId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (productId == entry.id)
                            $(category).parent().parent().next().next().find('.packageproduct').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.productName));
                        else
                            $(category).parent().parent().next().next().find('.packageproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
                    });
                }
            });
        });

        $('.value_label', this).each(function () {
            $(this).hide();
        });
        $('.value_field', this).each(function () {
            $(this).show();
        });
    });
    //$(this).hide();
    $(this).next().show();
});


$("body").on("change", ".packagecategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().parent().next().find('.packagesubcategory').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/shipments/getsubcategorylist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().parent().next().find('.packagesubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
            });
        }
    });

    $(this).parent().parent().next().next().find('.packageproduct').find('option:gt(0)').remove();
});

$("body").on("change", ".packagesubcategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().parent().next().find('.packageproduct').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/shipments/getproductlist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().parent().next().find('.packageproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
            });
        }
    });
});

/* Save Edit Delivery Item Items */
$("body").on("click", ".savePackage", function (event) {
    $(this).attr('disabled', true);
    var inputArray = [];
    var link = $(this);
    var err = false;
    var shipmentId = $('#shipmentId').val();

    $(this).closest("tr:has(input)").each(function () {
        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    {
                        if($(this).hasClass('quantityOther') && $(this).val()<1)
                        {
                            err = true;
                            alert("Enter value greater than 0 for "+$(this).attr('data-name'));
                        }
                        else if($(this).hasClass('productvalOtherMin'))
                        {
                            var maxVal = $(this).closest("tr:has(input)").find('.productvalOther').val();
                            if(parseFloat($(this).val())>parseFloat(maxVal))
                            {
                                err = true;
                                alert("Product Minimum value can not be greater than product maximum value");
                            }
                        }
                        else if($(this).hasClass('productvalOther'))
                        {
                            var minVal = $(this).closest("tr:has(input)").find('.productvalOtherMin').val();
                            if(parseFloat($(this).val())<parseFloat(minVal))
                            {
                                err = true;
                                alert("Product Maximum value can not be lower than product minimum value");
                            }
                        }
                        if(!err)
                        {
                            inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                        }
                    }
                    
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/shipments/editdeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                alert('Data saved successfully');
                $("#packageList").load(baseUrl + "/shipments/getpackagedetails/" + response);
                $("#infoBoxDetail").load(baseUrl + "/shipments/getshipmentdetails/" + shipmentId);
            }
        });
        $(this).hide();
        $(this).prev().show();
    }
});

function validateDeliveryForm() {

    var error = false;
     $('#addEditDeliveryFrm input').each(function () {
        if ($(this)[0].hasAttribute("required")) {
            if ($(this).val() == '') {
                error = true;
                var fieldNameWithStar = $(this).prev("label").text();
                var fieldName = fieldNameWithStar.substr(0,fieldNameWithStar.length-1);
                alert(fieldName + " field is required.");
            }
            else if($(this).hasClass('qtyAddDelivery') && $(this).val()<1)
            {
                error = true;
                alert("Enter value greater than 0 for quantity");
            }
        }
    });
    $('#addEditDeliveryFrm select').each(function () {
        if ($(this)[0].hasAttribute("required")) {
            if ($(this).val() == '') {
                error = true;
                var fieldNameWithStar = $(this).prev("label").text();
                var fieldName = fieldNameWithStar.substr(0,fieldNameWithStar.length-1);
                alert(fieldName + " field is required.");
            }
        }
    });
    
    $('.deliveryItem').each(function() {
        var minVal = $(this).find('.product-minval-addDelivery').val();
        var maxVal = $(this).find('.product-val-addDelivery').val();

        if(parseFloat(minVal) > parseFloat(maxVal))
        {
            error = true;
            alert("Minimum price of item can not be higher than maximum value");
        }
    });
        
    
    if(error)
        return false;
    else
        return true;
}



/* Delete Delivery Item */
$("body").on("click", ".removePackage", function (event) {
    var id = $(this).attr('data-id');
    var shipmentId = $('#shipmentId').val();

    $.ajax({
        url: baseUrl + "/shipments/deletedeliveryitem/" + id,
        type: 'GET',
        success: function (response) {
            if (response == 1) {
                $('#package' + id).remove();
                $("#infoBoxDetail").load(baseUrl + "/shipments/getshipmentdetails/" + shipmentId);
            }

        }
    });
});

$("body").on("change", ".itemcategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(category).parent().next().find('.itemsubcategory').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/shipments/getsubcategorylist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $(category).parent().next().find('.itemsubcategory').find('option:gt(0)').remove();
            $.each(response, function (key, entry) {
                $(category).parent().next().find('.itemsubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
            });
        }
    });

    $(this).parent().next().next().find('.itemproduct').find('option:gt(0)').remove();
});

$("body").on("change", ".itemsubcategory", function (event) {

    var subcategoryId = $(this).val();
    var subcategory = $(this);

    // Populate dropdown with list of subcategories
    $(subcategory).parent().next().find('.itemproduct').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/shipments/getproductlist/" + subcategoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $(subcategory).parent().next().find('.itemproduct').find('option:gt(0)').remove();
            $.each(response, function (key, entry) {
                $(subcategory).parent().next().find('.itemproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
            });
        }
    });
});

/* Start packaging validation */
$(document).on('click', '#packagenow', function () {
    var shipmentId = $("#shipmentId").val();
    var deliveryCount = $("#deliveryCount").val();
    var addeddata = $("#labeldata").val();
    var deliveryTobeScaned = $("#deliveryTobeScaned").val();
    var deliveryTobeScanedArr = deliveryTobeScaned.split("^");
    addeddata = addeddata.trim();
    var allData = new Array();
    addeddata.split("\n").forEach(function (item) {
        allData.push(item);
    });

    var match = 1;
    var deliveryScaned = 0;
    if (allData.length > 0)
    {
        for (var i = 0; i < allData.length; i++)
        {
            if (i == 0)
            {
                if (allData[i] != shipmentId)
                {
                    match = 0;
                    break;
                }
            } 
            else
            {
                deliveryScaned++;
                var ifDelivery = allData[i].indexOf('-')
                if (ifDelivery != '-1')
                {
                    var delivId = allData[i].substr(parseInt(ifDelivery) + 1);
                    allData[i] = allData[i].substr(0, ifDelivery);
                    if (allData[i] != shipmentId || (deliveryTobeScanedArr.indexOf(delivId)=="-1"))
                    {
                        match = 0;
                        break;
                    }
                } 
                else
                {
                    match = 0;
                    break;
                }


            }

        }
        if (match != 0 && deliveryScaned != deliveryCount)
        {
            match = 0;
        }
        //console.log(match);
        if (match == 0)
            alert("Wrong data! Please recheck");
        else
        {
            $("#packageShipment").val(shipmentId);
            var action = $("#frmstartpkg").attr('action');
            var data = $("#frmstartpkg").serialize();
            $.ajax({
                url: action,
                data: data,
                dataType: 'JSON',
                type: 'post',
                success: function (response) {
                    if (response.message == 'success')
                    {
                        $('#modal-addEdit').modal('hide');
                        $("#addPackaging").removeAttr("disabled");
                        window.location.reload();
                    } else
                    {
                        alert("Something went wrong");
                    }
                }
            });
        }
    } else
        alert("Wrong data! Please recheck");

    return false;
});
/* End */

$(document).on('click', '#addPackaging', function () {
    var error = 0;
    $("#frmaddpkg input[type='text']").each(function(){
      if($(this).val()=='' || $(this).val()=='0.00')
      {
          error = 1;
      }
    });
    if(error == 0)
    {
        var action = $("#frmaddpkg").attr("action");
        var data = $("#frmaddpkg").serialize();
        var shipmentId = $(this).attr('data-shipment');
        var page = $(this).attr('data-page');
        var baseUrl = $('#baseUrl').val();
        $.ajax({
            url: action,
            data: data,
            dataType: 'JSON',
            type: 'post',
            success: function (response) {
                if (response.message == 'success')
                {
                    //$("#packaginTab").load(baseUrl + "/shipments/getpackagingdata/" + shipmentId + "/" + page);
                    window.location.reload();
                }
            }
        });
    }
    else
    {
        alert('Enter packaging details');
    }

    return false;


});

var baseUrl = $('#baseUrl').val();

function userVerification() {
    var userUnit = $('#userUnit').val();

    if (userUnit != '') {
        $.ajax({
            url: baseUrl + "/shipments/verifyuser/" + userUnit,
            type: 'GET',
            success: function (response) {
                if (response == 0) {
                    $('#user-verification').removeClass('text-success');
                    $('#user-verification').addClass('text-danger');
                    $('#user-verification').html('<b>This is an Invalid Customer.</b>');
                    $("#submitBtn").prop("disabled", true);
                    if($(".disableblock").length==0)
                        $("#shipmentDeliveryDetails").addClass("disableblock");
                    $("#customerSettingsBlock").hide();
                } else {
                    
                    $("#warehouseId").html("<option value=''>Select</option>");
                    $.ajax({
                    url: baseUrl + "/shipments/getuserwarehouse/" + userUnit,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                            if(response.data.length>0)
                            {
                                $('#warehouse-location').removeClass('text-danger');
                                $('#warehouse-location').html('');
                                $.each(response.data, function (i, j) {
                                    $("#warehouseId").append('<option value="' + j.id + '">' + j.name +' ('+ j.cityName +')</option>');
                                });
                                $("#shipmentDeliveryRest").removeClass("disableblock");
                                $("#submitBtn").prop("disabled", false);
                                $("#customerSettingsBlock").show();
                                $.each(response.userSettings, function (i, j) {
                                    if(j.fieldType == 'special_instruction'){
                                        if(j.fieldValue != '' || j.fieldValue != 'null')
                                            $('#'+j.fieldType).text(j.fieldValue);
                                        else
                                            $('#'+j.fieldType).text('N/A');
                                    } else {
                                        if(j.fieldValue == 1)
                                            $('#'+j.fieldType).html("<strong class='text-red'>Yes</strong>");
                                         else
                                            $('#'+j.fieldType).html("<strong>No</strong>");
                                    }
                                });
                            }
                            else
                            {
                                $("#shipmentDeliveryRest").addClass("disableblock");
                                $("#submitBtn").prop("disabled", true);
                                $('#warehouse-location').addClass('text-danger');
                                $('#warehouse-location').html('<b>Shipping address is not set for this user.</b>');
                                $("#customerSettingsBlock").hide();
                            }
                        }
                    });
            
//                    $("#submitBtn").prop("disabled", false);
                    $("#shipmentDeliveryDetails").removeClass("disableblock");
                    $('#user-verification').removeClass('text-danger');
                    $('#user-verification').addClass('text-success');
                    $('#user-verification').html('<b>This is a valid Customer.</b>');
                    $('#modal-verify').html(response);
                    $('#modal-verify').modal('show');
                }
            }
        });
    } else {
        $("#addEditFrm input:not('#userUnit')").prop("disabled", true);
        $("#addEditFrm select").prop("disabled", true);
        $('#user-verification').removeClass('text-success');
        $('#user-verification').removeClass('text-danger');
        $('#user-verification').html('');
    }
}

function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");
        if (r == true) {
            $('#checkedval').val(checkedIds);
            $('#frmCheckedItem').attr('action', baseUrl + '/shipments/deleteall');
            $('#frmCheckedItem').submit();
        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}

function savepacked(shipmentid) {
    
    var r = confirm("You will not be allowed to do any changes in packaging after this. This action cannot be reversed back!!");
    if(r) {
        $.ajax({
        url: baseUrl + "/shipments/updatepackedstatus/" + shipmentid,
        type: 'GET',
        success: function (response) {
                if(response == '1')
                {
                    window.location.reload();
                }
            }
        });
    }
}

function changeStatus() {
    var selected = [];
    var shipmentstatus = [];
    $('#checkboxes input:checked').each(function () {
        shipmentstatus.push($(this).attr('data-attr'));
        selected.push($(this).attr('value'));
    });
    if (checkForDuplicates(shipmentstatus) == 1) {
        var checkedIds = selected.join('^');
        if (checkedIds != '') {
            var r = confirm("Status of all the selected items will be modified. This action cannot be reversed back!!");
            if (r == true) {
                showAddEdit(checkedIds, 0, 'shipments/bulkchangestatus');
            }
        }
    } else {
        alert('Please select shipments having same status!!');
    }
}

function scheduleNotification() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        showAddEdit(checkedIds, 0, 'shipments/schedulenotification');
    } else {
        alert("Select atleast one field");
        return false;
    }
}

function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
    return 1;
}

function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    } else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}
function exportallse_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        } else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();

                $.ajax({
                    url: "shipments/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function getWarehouseZoneList(rowId) {
    $('#warehouseZoneId').find('option:gt(0)').remove();
    if (rowId != '') {
        var url = baseUrl + "/shipments/getwarehousezonelist/" + rowId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#warehouseZoneId').append($('<option></option>').attr('value', entry.id).text(entry.name));
            })
        });

    }
}

function warehouseChanged(warehouseId)
{
    $('#storeId').find('option:gt(0)').remove();
    if (warehouseId != '') {
        var url = baseUrl + "/shipments/getstorelist/" + warehouseId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#storeId').append($('<option></option>').attr('value', entry.id).text(entry.storeName));
            })
        });

    }

}

function storeChanged(storeId)
{
    $('.siteCategoryId').find('option:gt(0)').remove();
    if (storeId != '') {
        var url = baseUrl + "/shipments/getcategorylist/" + storeId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('.siteCategoryId').append($('<option></option>').attr('value', entry.id).text(entry.categoryName));
            })
        });

    }

}

function updateShipmentType(shipmenttype, shipmentid) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/shipments/updateshipment/" + shipmentid,
        type: 'POST',
        data: {shipmentType: shipmenttype, field: 'shipmentType'},
        success: function (response) {
            if (response == '1') {
                if (shipmenttype == 'air')
                    var txt = 'Air';
                else
                    var txt = 'Sea';
                $('#shipmentTypeTxt').text(txt);

            }
        }
    });

}

$("body").on("click", ".deletelocation", function (event) {
    var warehouseLocationId = $(this).attr('data-warehouseid');
    var shipmentId = $('#shipmentId').val();
    $(this).confirmation({
        onConfirm: function () {
            var url = baseUrl + "/shipments/deletelocation/" + warehouseLocationId + "/" + shipmentId;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    if (response == 1) {
                        $("#locationTrack").load(baseUrl + "/shipments/getwarehouselocations/" + shipmentId);
                        alert('Warehouse location deleted successfully.');
                    }
                }
            });
        },
    });
});

$("body").on("change","#warehouseId",function() {
   var selectedWarehouse = $("#warehouseId option:selected").text(); 
   r = confirm("You have selected the warehouse "+selectedWarehouse+" for this shipment. You want to continue with that?");
   if(!r)
   {
       $("#warehouseId option:selected").prop("selected", false)
   }
   else{
    warehouseChanged($("#warehouseId option:selected").val());
   }

});

(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

$(function () {
    $('#file-upload').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
        success: function () {
            //alert('Success');


        },
        error: function () {
            alert('Error! File type not supported');
            readURL('error');
            //$('#loaded').css('display', 'none');
            //$('#defaultLoaded').css('display', 'block');
        }
    });

});


function uploadSnapshot(shipmentId,packageId,deliveryId) {
    var allowedSnapCount = $("#deliverySnapAllowed-"+deliveryId).val();
    $.ajax({
        url: baseUrl + "/shipments/getsnapshots/" + shipmentId+"/"+deliveryId+"/"+packageId,
        type: 'get',
        success: function (response) {
            if(response!='')
            {
                $(".images").html('');
                $(".images").append(response);
                $('#defaultLoaded').css('display', 'none');
            }
            $("#file-upload").attr("data-uploadallowed",allowedSnapCount);
            $('#modal-upload #packageId').val(packageId);
            $('#modal-upload #deliveryId').val(deliveryId);
            $('#modal-upload').modal('show');
        }
    });
    
}

function showDeliveryDetails(packageId) {
    var modalContent = $(".deliveryDetaiils-"+packageId).html();
    $("#modal-delivery-details .modal-body").html(modalContent);
    $("#modal-delivery-details").modal('show');
}

function getShippingCharge(shipmentId,deliveryId,shippingId) {
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/shipments/methodwiseshippingcharge",
            type: 'POST',
            data: {shipmentId: shipmentId, deliveryId: deliveryId, shippingMethodId: shippingId},
            success: function (response) {
                $(".deliveryShipingCost-"+deliveryId).html(response);
            }
        });
    
}
function addNewCharge(shipmentId,deliveryId) {
    var otherChargeId = $('#otherChargeId'+deliveryId).val();
    var notes = $('#chargeNotes'+deliveryId).val();

    if (otherChargeId != '') {
        $('#otherChrgBtn').attr('disabled', true);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/shipments/addshipmentcharge/" + shipmentId+"/"+deliveryId,
            type: 'POST',
            data: {otherChargeId: otherChargeId, notes: notes, type: 'other'},
            success: function (response) {
                if (response == '1') {
                    $('#otherChrgBtn').attr('disabled', false);
                    $('.oCharge').val('');
                    $("#otherChargeList_"+deliveryId).load(baseUrl + "/shipments/getothercharges/other/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    $(".deliveryShipingCostBox-"+deliveryId).load(baseUrl + "/shipments/deliveryshippingcost/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    //$("#totalShipmentChrg").load(baseUrl + "/shipments/gettotalcharge/" + shipmentId);
                }
            }
        });
    } else {
        alert("Select a charge to continue!!");
        return false;
    }
}

function addNewChargeNewDelivery(shipmentId) {
    var otherChargeId = $('#otherChargeIdNewdeliv').val();
    var notes = $('#chargeNotesNewdeliv').val();
    if (otherChargeId != '') {
        $('#otherChrgBtn').attr('disabled', true);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/shipments/addshipmentnewdeliverycharge/" + shipmentId,
            type: 'POST',
            data: {otherChargeId: otherChargeId, notes: notes, type: 'other'},
            success: function (response) {
                if (response == '1') {
                    $('#otherChrgBtn').attr('disabled', false);
                    $('.oCharge').val('');
                    $("#otherChargeList").load(baseUrl + "/shipments/getotherchargesnewdelivery/other/" + shipmentId, function (responseTxt, statusTxt, xhr) {
                    });
//                    $(".deliveryShipingCostBox-"+deliveryId).load(baseUrl + "/shipments/deliveryshippingcost/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
//                    });
                    //$("#totalShipmentChrg").load(baseUrl + "/shipments/gettotalcharge/" + shipmentId);
                }
            }
        });
    } else {
        alert("Select a charge to continue!!");
        return false;
    }
}

function saveComment(shipmentId)
{
    var message = $('#addcomment').val();
    if (message != '') {
        $.ajax({
            url: baseUrl + "/shipments/addcomment/" + shipmentId,
            type: 'POST',
            data: {comment: message},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response != '')
                {
                    var url = response["noteId"] + ", " + response["shipmentId"] + ", 'shipments/shownotes'";
                    $('#showNote').prepend('<div class="row"><div class="col-sm-12">By: ' + response["firstname"] + ' ' + response["lastname"] + ', ' + response["email"] + ',' + response["sentOn"] + ', <a onclick="showAddEdit(' + url + ')"><strong>Comment Details</strong></a>. </div> </div>');
                } else {
                    $('#showmsg').html('<div class="danger">Notes not saved</div>')
                }
            }
        });
    } else {
        $('#showmsg').html('<div class="success">Please enter a message above</div>');
        $('#addcomment').focus();
    }

}

function addNewManualCharge(shipmentId,deliveryId) {
    var otherChargeName = $('#otherChargeName'+deliveryId).val();
    var otherChargeAmount = $('#otherChargeAmount'+deliveryId).val();
    var notes = $('#chargeNotes'+deliveryId).val();

    if (otherChargeName != '' && otherChargeAmount != '') {
        $('#manualChrgBtn').attr('disabled', true);
        
        $.ajax({
            url: baseUrl + "/shipments/addshipmentcharge/" + shipmentId+"/"+deliveryId,
            type: 'POST',
            data: {otherChargeName: otherChargeName, otherChargeAmount: otherChargeAmount, notes: notes, type: 'manual'},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function (response) {
                if (response == '1') {
                    $('#manualChrgBtn').attr('disabled', false);
                    $('.mCharge').val('');
                    $("#manualChargeList_"+deliveryId).load(baseUrl + "/shipments/getothercharges/manual/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    $(".deliveryShipingCostBox-"+deliveryId).load(baseUrl + "/shipments/deliveryshippingcost/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    $("#totalShipmentChrg").load(baseUrl + "/shipments/gettotalcharge/" + shipmentId);
                }
            }
        });
    } else {
        alert("Enter charge name & amount to continue!!");
        return false;
    }
}

function addNewManualChargeNewDelivery(shipmentId) {
    var otherChargeName = $('#otherChargeNameNewdeliv').val();
    var otherChargeAmount = $('#otherChargeAmountNewdeliv').val();
    var notes = $('#otherNotesNewdeliv').val();

    if (otherChargeName != '' && otherChargeAmount != '') {
        $('#manualChrgBtn').attr('disabled', true);
        
        $.ajax({
            url: baseUrl + "/shipments/addshipmentnewdeliverycharge/" + shipmentId,
            type: 'POST',
            data: {otherChargeName: otherChargeName, otherChargeAmount: otherChargeAmount, notes: notes, type: 'manual'},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function (response) {
                if (response == '1') {
                    $('#manualChrgBtn').attr('disabled', false);
                    $('.mCharge').val('');
                    $("#manualChargeList").load(baseUrl + "/shipments/getotherchargesnewdelivery/manual/" + shipmentId, function (responseTxt, statusTxt, xhr) {
                    });
//                    $(".deliveryShipingCostBox-"+deliveryId).load(baseUrl + "/shipments/deliveryshippingcost/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
//                    });
//                    $("#totalShipmentChrg").load(baseUrl + "/shipments/gettotalcharge/" + shipmentId);
                }
            }
        });
    } else {
        alert("Enter charge name & amount to continue!!");
        return false;
    }
}


function updateStorageCharge(shipmentId) {
    if (shipmentId != '') {
        $('#storageChrgBtn').attr('disabled', true);
        var storageCharge = $('#storageCharge').val();

        $.ajax({
            url: baseUrl + "/shipments/updateshipment/" + shipmentId,
            type: 'POST',
            data: {storageCharge: storageCharge, field: 'storageCharge'},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function (response) {
                if (response == '1') {
                    $('#storageChrgBtn').attr('disabled', false);
                    $("#storageChargeTxt").load(baseUrl + "/shipments/updatestoragecharge/" + shipmentId, function (responseTxt, statusTxt, xhr) {
                    });
                    $("#totalShipmentChrg").load(baseUrl + "/shipments/gettotalcharge/" + shipmentId);

                }
            }
        });
    }
}

function updateDelivery(shipmentId, deliveryId)
{
    //var storeId = $('#deliveryStore').val();
    //var companyId = $('#deliveryCompany').val();
    //var trackingId = $('#deliveryTracking').val();
    //var trackingId2 = $('#deliveryTracking2').val();
    var deliveryLength = $('#deliveryBox'+deliveryId+' .deliveryLength').val();
    var deliveryWidth = $('#deliveryBox'+deliveryId+' .deliveryWidth').val();
    var deliveryHeight = $('#deliveryBox'+deliveryId+' .deliveryHeight').val();
    var deliveryWeight = $('#deliveryBox'+deliveryId+' .deliveryWeight').val();
    //var deliveredOn = $('#deliveredOn').val();
    //var received = $('#received').val();

    $.ajax({
        url: baseUrl + "/shipments/updatedelivery/" + shipmentId + "/" + deliveryId,
        type: 'POST',
        data: { deliveryLength: deliveryLength, deliveryWidth: deliveryWidth, deliveryHeight: deliveryHeight, deliveryWeight: deliveryWeight},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            alert("Delivery information saved successfully.")
            //$("#infoBoxDetail").load(baseUrl + "/shipments/getshipmentdetails/" + shipmentId);
        }
    });


}

function updatePaymentStatus(shipmentId) {
    var paymentStatus = $('#paymentStatus').val();
    $.ajax({
        url: baseUrl + "/shipments/updatepaymentstatus/" + shipmentId,
        type: 'POST',
        data: {paymentStatus: paymentStatus},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if(response == 1){
                if(paymentStatus == 3 || paymentStatus == 4 || paymentStatus == 5){
                     $('#paymentStatus').attr('disabled','disabled');
                }
                alert("Payment status updated successfully.")
                $("#infoBoxDetail").load(baseUrl + "/shipments/getshipmentdetails/" + shipmentId);
            }
        }
    });
}

function updatePayment(shipmentId,type) {
    
    var paymentStatus = $('#paymentInvoiceStatus').val();
    r = confirm("Are you sure? This action cannot be reversed back!!");
    if(r)
    {
        $.ajax({
            url: baseUrl + "/shipments/updatepaymentstatus/" + shipmentId+'/'+type,
            type: 'POST',
            data: {paymentStatus: paymentStatus},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if(response == 1){
                    $('#paymenyInvoiceStatus').parent().html('<button type="button" class="btn btn-default custumBtnWtIcn printBtns">'+paymentStatus+'</button>');
                    //$('.updatePayment').hide();
                    window.location.reload();
                }
            }
        });
    }
    
}

function generateInvoice(shipmentId, type)
{
    
    $.ajax({
            url: baseUrl + "/shipments/generateInvoice/" + shipmentId+'/'+type,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if(response == 1){
                    
                    window.location.reload();
                }
            }
        });
}

$('#toCountry').on('change', function () {
    var _token = $("input[name='_token']").val();
    var base_path_offers = $("input[name='base_path_offers']").val();

    $.ajax({
        url: base_path_offers + "getStates",
        type: 'POST',
        data: {_token: _token, countryId: $(this).val()},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                //console.log(data);
                $('#toState').find('option:gt(0)').remove();
                $.each(data, function (key, entry) {
                    $('#toState').append($('<option></option>').attr('value', entry.id).text(entry.name));
                });


            } else {
                printErrorMsg(data.error);
            }
        }
    });
});

$('#fromState').on('change', function () {

    var countrySelectedId = $("#countrySelected").val();

    $.ajax({
        url: base_path_offers + "getCities",
        type: 'POST',
        data: {_token: _token, stateId: $(this).val(), country: countrySelectedId},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                //console.log(data);
                $('#fromCity').find('option:gt(0)').remove();
                $.each(data, function (key, entry) {
                    $('#fromCity').append($('<option></option>').attr('value', entry.id).text(entry.name));
                });


            } else {
                printErrorMsg(data.error);
            }
        }
    });
});

$('#toState').on('change', function () {
    var _token = $("input[name='_token']").val();
    var base_path_offers = $("input[name='base_path_offers']").val();
    var countrySelectedId = $("#countrySelected").val();

    $.ajax({
        url: base_path_offers + "getCities",
        type: 'POST',
        data: {_token: _token, stateId: $(this).val(), country: countrySelectedId},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                //console.log(data);
                $('#toCity').find('option:gt(0)').remove();
                $.each(data, function (key, entry) {
                    $('#toCity').append($('<option></option>').attr('value', entry.id).text(entry.name));
                });


            } else {
                printErrorMsg(data.error);
            }
        }
    });
});

$(document).ready(function () {
    $("#piecesPlaceholder").text($('#Pieces').val());
    var fromCountryDefault = $("#fromCountry").val();
    var fromStateDefault = $("#fromState").val();
    var toCountryDefault = $("#toCountry").val();
    var toStateDefault = $("#toState option:selected").val();
    var _token = $("input[name='_token']").val();
    var base_path_offers = $("input[name='base_path_offers']").val();


    // generate states for default selected from country
    if (fromCountryDefault != undefined) {
        $.ajax({
            url: base_path_offers + "getStates",
            type: 'POST',
            data: {_token: _token, countryId: fromCountryDefault},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    //console.log(data);
                    $('#fromState').find('option:gt(0)').remove();
                    $.each(data, function (key, entry) {
                        if ($("#fromStateIDDefault").val() == entry.id) {
                            var sel = "selected";
                        } else {
                            var sel = '';
                        }
                        $('#fromState').append($('<option ' + sel + '></option>').attr('value', entry.id).text(entry.name));
                    });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }


    // generate cities for default selected from states/countries
    if ($("#fromStateIDDefault").val() != undefined && $("#fromCountryIDDefault").val() != undefined) {
        $.ajax({
            url: base_path_offers + "getCities",
            type: 'POST',
            data: {_token: _token, stateId: $("#fromStateIDDefault").val(), country: $("#fromCountryIDDefault").val()},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    //console.log(data);
                    $('#fromCity').find('option:gt(0)').remove();
                    $.each(data, function (key, entry) {
                        if ($("#fromCityIDDefault").val() == entry.id) {
                            var sel = "selected";
                        } else {
                            var sel = '';
                        }
                        $('#fromCity').append($('<option ' + sel + '></option>').attr('value', entry.id).text(entry.name));
                    });


                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }


    // generate states for default selected to country
    if (toCountryDefault != undefined) {
        $.ajax({
            url: base_path_offers + "getStates",
            type: 'POST',
            data: {_token: _token, countryId: toCountryDefault},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    //console.log(data);
                    $('#toState').find('option:gt(0)').remove();
                    $.each(data, function (key, entry) {
                        if ($("#toStateIDDefault").val() == entry.id) {
                            var sel = "selected";
                        } else {
                            var sel = '';
                        }
                        $('#toState').append($('<option ' + sel + '></option>').attr('value', entry.id).text(entry.name));
                    });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }

    // generate cities for default selected to states/countries
    if ($("#toStateIDDefault").val() != undefined && $("#toCountryIDDefault").val() != undefined) {
        $.ajax({
            url: base_path_offers + "getCities",
            type: 'POST',
            data: {_token: _token, stateId: $("#toStateIDDefault").val(), country: $("#toCountryIDDefault").val()},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    //console.log(data);
                    $('#toCity').find('option:gt(0)').remove();
                    $.each(data, function (key, entry) {
                        if ($("#toCityIDDefault").val() == entry.id) {
                            var sel = "selected";
                        } else {
                            var sel = '';
                        }
                        $('#toCity').append($('<option ' + sel + '></option>').attr('value', entry.id).text(entry.name));
                    });


                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }
});

function readURL(input) {
    if (input != 'error') {
        var allowedFile = $("#file-upload").attr("data-uploadallowed");
        if(input.files.length > allowedFile )
        {
            alert("Maximum 5 images allowed per delivery");
        }
        else
        {
            var allowedSnapCount = allowedFile-input.files.length;
            $("#file-upload").attr("data-uploadallowed",allowedSnapCount);
            
            if (input.files) {
                //var uploadedImage = $(".images div.col-lg-3").length;
                //console.log(uploadedImage);
                for(i=0;i<input.files.length;i++)
                {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        //$('#loaded').css('display', 'block');
                        //$('#loaded').attr('src', e.target.result);
                        $(".images").append('<div class="col-lg-3"><img id="loaded" style="height:100px;max-width:100%; margin-bottom:10px;" src="'+e.target.result+'" alt="" /></div>')
                        $('#defaultLoaded').css('display', 'none');
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
    } else {
        $('#loaded').css('display', 'none');
        $('#defaultLoaded').css('display', 'block');
    }
}

function checkSelectRecord() {
    //var selected = [];
    if ($("input[name=checkboxselected]:checked").length > 0) {
        //selected.push($(this).attr('name'));
        $('#modal-export-selected').modal('show');
    } else {
        alert('Please select record');
    }
}

function exportallse_submit() {

    if ($('#exportAllse input[type=checkbox]:checked').length) {
        var selectedField = [];
        $('#exportAllse input:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });


        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        } else {


            $(document).ready(function () {

                var _token = $("input[name='_token']").val();
                var base_path_selected_export = $("input[name='base_path_selected_export']").val();

                //alert(base_path_selected_export);

                $.ajax({
                    headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                     },
                    url: base_path_selected_export,
                    type: 'POST',
                    data: {selected: selected, selectedField: selectedField, _token: _token},
                    success: function (data) {
                        //alert(data.path);
                        window.location = data.path;
                        if ($.isEmptyObject(data.error)) {
                            //alert(data);
                        } else {
                            //alert(data.error);
                        }
                    }
                });


            });


        }
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export').modal('show');
        }, 1000);
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
        //return false;
    }

}

function dhl_ci_submit() {
    var base_path_dhlci = $("input[name='base_path_dhlci']").val();
    var data = $('#dhlcipost').serialize();
    var _token = $("input[name='_token']").val();
    var descr = $('input[name^=descr]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var ccc = $('input[name^=ccc]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var manufCountry = $('select[name^=manufacturerCountry]').map(function () {
        return this.value;
    }).get();

    var qty = $('input[name^=qty]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var price = $('input[name^=price]').map(function (idx, elem) {
        return $(elem).val();
    }).get();


    $.ajax({
        url: base_path_dhlci,
        type: 'POST',
        data: {data: data, _token: _token, descr: descr, ccc: ccc, manufCountry: manufCountry, qty: qty, price: price, toCountry: $("#toCountry option:selected").text(), fromCountry: $("#fromCountry option:selected").text(), fromState: $("#fromState option:selected").text(), toState: $("#toState option:selected").text(), fromCity: $("#fromCity option:selected").text(), toCity: $("#toCity option:selected").text()},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                // success
                $("#modal-addEdit .modal-content").html(data);

            } else {
                printErrorMsg(data.error);
            }
        }
    });


}

function deleteDelivery(deliveryId) {
    if (deliveryId != '') {
        var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
        if (r)
        {
            $.ajax({
                url: baseUrl + "/shipments/deletedelivery/" + deliveryId,
                type: 'GET',
                success: function (response) {
                    if (response == 1) {
                        $('#deliveryBox' + deliveryId).remove();
                        alert('Shipment delivery deleted successfully.');
                    }
                }
            });

        }
    }
}

function updateDeliveryStage(deliveryId) {
    var url = $("#updatestageform_"+deliveryId).attr('action');
    var showDiv = false;

        $("#updatestageform_"+deliveryId+" input[type='text']:not([disabled]").each(function(){
            if($(this).attr("name") == "status[in_transit]")
              {
                showDiv = true;
                //showAddEdit(deliveryId, 1, 'shipments/assignDispatchCompany');
                return false;
              }
              else{
                showDiv = false;
              }
        });
    if(showDiv == true)
    {
        
       showAddEdit(deliveryId, 1, 'shipments/assignDispatchCompany');
        
    }
    if(showDiv == false){
       $.ajax({
            url: url,
            data : $("#updatestageform_"+deliveryId).serialize(),
            type: 'POST',
            success: function (response) {
                if (response == 1) {
                    $("#updatestageform_"+deliveryId+" input[type='text']").each(function(){

                       if($(this).val()!="")
                       {
                           $(this).attr("disabled",true);
                       }
                    });
                    alert('Stages data updated successfully.');
                }
            }
        });
     
    }
        
    return false;
}

function assignDispatchCompany(deliveryId)
{
    $('#assignCompany').val($('#shippingMethod').val());
    $('#modal-addEdit').modal('hide');
    var url = $("#updatestageform_"+deliveryId).attr('action');

     $.ajax({
            url: url,
            data : $("#updatestageform_"+deliveryId).serialize(),
            type: 'POST',
            success: function (response) {
                    if (response == 1) {
                         
                        $("#updatestageform_"+deliveryId+" input[type='text']").each(function(){

                           if($(this).val()!="")
                           {
                               $(this).attr("disabled",true);
                           }
                        });
                        alert('Stages data updated successfully.');
                    }
                }
            }); 



}


function dhl_submit() {
    var base_path_dhlci = $("input[name='base_path_dhlci']").val();
    var data = $('#dhlpost').serialize();
    var _token = $("input[name='_token']").val();

    var length = $('input[name^=dhllenght]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var width = $('input[name^=dhlwidth]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var height = $('input[name^=dhlheight]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var weight = $('input[name^=dhlweight]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var cweight = $('input[name^=dhlcweight]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var contents = $('textarea[name^=dhlcontent]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var shipmentId = $("#shipmentId").val();


    $.ajax({
        url: base_path_dhlci,
        type: 'POST',
        data: {data: data, _token: _token, shipmentId: shipmentId, length: length, width: width, height: height, weight: weight, cweight: cweight, contents: contents, toCountry: $("#toCountry option:selected").text(), fromCountry: $("#fromCountry option:selected").text(), fromState: $("#fromState option:selected").text(), toState: $("#toState option:selected").text(), fromCity: $("#fromCity option:selected").text(), toCity: $("#toCity option:selected").text()},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                // success
                $("#modal-addEdit .modal-content").html(data);

            } else {
                printErrorMsg(data.error);
            }
        }
    });


}

function checkValid(form) {
    var totalTrackingField = 0;
    var trackingEntered = 0;
    var error = false
//    $(".trackingEnteredVal").each(function(){
//        totalTrackingField++;
//       var trackingEnterVal = $(this).val();
//       if(trackingEnterVal == '1')
//       {
//           trackingEntered++;
//       }
//    });
//    //console.log(totalTrackingField+' '+trackingEntered);
//    if(totalTrackingField == trackingEntered)
//    {
//        return true;
//    }
//    else
//    {
//        $("#trackingError").show();
//        return false;
//        
//    }
    $('.dash_custom_shipment_tble table tbody tr').each(function() {
        
        var minVal = $(this).find('.product-min-val').val();
        var maxVal = $(this).find('.product-val').val();

        if(parseFloat(minVal) > parseFloat(maxVal))
        {
            error = true;
        }
    });
    
    if(error)
        alert("Minimum price of item can not be higher than maximum value");
    
    if(error)
        return false;
    else
        return true;
    
}

// $("body").on("click", "#addEditLocation", function (event) {
//     $(this).attr('disabled', true);
//     var shipmentId = $('#shipmentId').val();
//     $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });    

//     $.ajax({
//         url: $('#addeditLocationFrm').attr('action'),
//         type: 'POST',
//         data: $('#addeditLocationFrm').serialize(),
//         success: function (response) {
//             if (response == 1) {
//                 $("#locationTrack").load(baseUrl + "/shipments/getwarehouselocations/" + shipmentId);
//                 $("#modal-addEdit").modal('hide');
//                 alert("Warehouse location details saved successfully.");
//             }
//         }
//     });
// });

function addeditlocation()
{
    var shipmentId = $('#shipmentId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });    

    $.ajax({
        url: $('#addeditLocationFrm').attr('action'),
        type: 'POST',
        data: $('#addeditLocationFrm').serialize(),
        success: function (response) {
            if (response == 1) {
                $("#locationTrack").load(baseUrl + "/shipments/getwarehouselocations/" + shipmentId);
                $("#modal-addEdit").modal('hide');
                alert("Warehouse location details saved successfully.");
            }
        }
    }); 
}

$("body").on("click", "#changeAddressBtn", function (event) {
    $(this).attr('disabled', true);
    var shipmentId = $('#shipmentId').val();
    var userId = $('#userId').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: $('#changeaddressFrm').attr('action'),
        type: 'POST',
        data: $('#changeaddressFrm').serialize(),
        success: function (response) {
            if (response == '1') {
                $(".shipList").load(baseUrl + "/shipments/getaddressdetails/" + shipmentId);
                $("#modal-addEdit").modal('hide');
                alert("Address details saved successfully.");

            }
        }
    });
});

$('body').on('change','.itemReceived',function(e){
    var itemReceived = $(this).val();
    if(itemReceived == 'Y') {
        $("#modal-deliveryDetails").modal({backdrop: 'static', keyboard: false});
        //$("#selectedReceivedDroop").val($(this));
        window.selectedReceivedDroop = $(this);
    }
});

$('body').on('click','.itemReceived',function(e){
    
    $("#modal-deliveryDetails").modal({backdrop: 'static', keyboard: false});
    //$("#selectedReceivedDroop").val($(this));
    window.selectedReceivedDroop = $(this);

});

$('body').on('click','#saveDeliveryDetails',function(e){
    var deliveryCompany = $("#deliveryCompanyId").val();
    var delivered = $("#delivered").val();
    var trackingNumber = $("#trackingNumber").val();
    var note = $("#note").val();
    var html =  '<input type="hidden" name="shipment[deliveryCompanyId][]" value="'+deliveryCompany+'" />'+
                '<input type="hidden" name="shipment[deliveredOn][]" value="'+delivered+'" />'+
                '<input type="hidden" name="shipment[tracking][]" value="'+trackingNumber+'" />'+
                '<input type="hidden" name="shipment[note][]" value="'+note+'" />';
    window.selectedReceivedDroop.prop("disabled", true); 
    window.selectedReceivedDroop.html('<span style="font-size:10px">Details added</span>'); 
    window.selectedReceivedDroop.next('.shipmentDetails').html(html);
    window.selectedReceivedDroop.parent().find('.trackingEnteredVal').val('1');
    $('#modal-deliveryDetails').modal('hide');
    $("#deliveryCompanyId").val('');
    $("#delivered").val('');
    $("#trackingNumber").val('');
    $("#note").val('');
});

$('body').on('click','#saveDeliveryDetailsEdit',function(e){
    var deliveryCompany = $("#deliveryCompanyId").val();
    var delivered = $("#delivered").val();
    var trackingNumber = $("#trackingNumber").val();
    var note = $("#note").val();
    var html =  '<input type="hidden" name="deliveryCompanyId" value="'+deliveryCompany+'" />'+
                '<input type="hidden" name="deliveredOn" value="'+delivered+'" />'+
                '<input type="hidden" name="tracking" value="'+trackingNumber+'" />'+
                '<input type="hidden" name="note" value="'+note+'" />';
    window.selectedReceivedDroop.prop("disabled", true);    
    window.selectedReceivedDroop.next('.shipmentDetails').html(html);
    $('#modal-deliveryDetails').modal('hide');
    $("#deliveryCompanyId").val('');
    $("#delivered").val('');
    $("#trackingNumber").val('');
    $("#note").val('');
});



function exportselectedExport_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one record");
        } else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();
                var base_path_selected_export = $("input[name='base_path_selected_export']").val();
                $.ajax({
                    url: base_path_selected_export,
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function getLocationDetails(listIds) {    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl+"/shipments/getwerehouselocations",
        type: 'POST',
        data: {listIds : listIds},
        success: function (response) {
            if(response)
            {
                var totalArray = JSON.parse(response);

                var array = totalArray.returnLocationArr;
                
                for (var key in array) {
                    var value = array[key];
                    //console.log(key, value);
                    $("#location-"+key).html(value);
                }

                var delievryArray = totalArray.returnDeliveryArr;

                for (var key in delievryArray) {
                    var value = delievryArray[key];
                    $("#delivery-"+key).html(value);
                }

                var dispatchArray = totalArray.returndispatchCompany;

                for (var key in dispatchArray) {
                    var value = dispatchArray[key];
                    $("#dispatch-"+key).html(value);
                }
                
            }
        }
    });
}

function loadDeliveryDetails(shipmentId,shipmentType) {
    $("#wait").show();
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl+"/shipments/getdeliverydetails",
        type: 'POST',
        data: {shipmentId : shipmentId,shipmentType:shipmentType},
        success: function (response) {
            $("#wait").hide();
            if(response)
            {
                $(".deliveryDtlsColps").html(response);
                $("#loadDeliveryDetails").text('refresh delivery details');
            }
        }
    });
}