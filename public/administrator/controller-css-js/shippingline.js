
(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

$(function () {
    $('#file-upload').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'gif', 'png', 'csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx'],
        success: function () {
            var file = $('#file-upload')[0].files[0].name;
            $('#file-text').text(file);
        },
        error: function () {
            $(this).val('');
            alert('Error! File type not supported');
        }
    });

    $("#addeditFrm").validate();
});


