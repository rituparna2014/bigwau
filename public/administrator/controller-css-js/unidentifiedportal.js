$(function () {
    $('.datepicker').datepicker({
       format: 'yyyy-mm-dd',
       autoclose: true
   });
   $("#addeditFrm").validate();
   $("#itemResolved").validate();
   $(".attachment_upload").change(function () {
        //alert(this.files[0]);
        var field = $(this);
        var input = $(this)[0];
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                console.log(field.parent().html());
                field.parent().parent().parent().parent().find('.loaded').css('display', 'block');
                field.parent().parent().parent().parent().find('.loaded').attr('src', e.target.result);
                field.parent().parent().parent().parent().find('.storeIcon').css('display', 'none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    });
});

function readURL(input) {
    if (input != 'error') {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                console.lof(input.parent().html());
                input.parent().parent().parent().parent().find('.loaded').css('display', 'block');
                input.parent().parent().parent().parent().find('.loaded').attr('src', e.target.result);
                $('#storeIcon').css('display', 'none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $('#loaded').css('display', 'none');
        $('#storeIcon').css('display', 'block');
    }
}

function getWarehouseZoneList(rowId) {
    $('#warehouseZoneId').find('option:gt(0)').remove();
    if (rowId != '') {
        var url = baseUrl + "/shipments/getwarehousezonelist/" + rowId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#warehouseZoneId').append($('<option></option>').attr('value', entry.id).text(entry.name));
            })
        });

    }
}

function updateStatus(status) {
    //$("#wait").show();
    var checkedId = $("#unidentifiedStatus").attr('data-item');
    var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
    if(r)
    {
        if(status == 'resolved')
        {
            $('#modal-item-resolved').modal('show');
        }
//        $.ajaxSetup({
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            }
//        });
//
//        $.ajax({
//            url: baseUrl + "/unidentifiedportal/updatestatus",
//            type: 'POST',
//            data: {shipmentId: checkedId, status: status},
//            success: function (response) {
//                $("#wait").hide();
//                if (response == 1) {
//                     window.location.reload();
//                }
//            }
//        });
    }

}