$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.addressBook').on('ifChecked', function (event) {
        $('#addressBook').val($(this).val());
    });

    $('#searchByDate').daterangepicker();

    //Custom radio button show daterange
    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });


    var baseUrl = $('#baseUrl').val();

  
});

$("body").on("blur", ".format", function (event) {
    if ($.isNumeric(this.value)) {
        var newVal = parseFloat(this.value);
        $(this).val(newVal.toFixed(2));
    } else {
        var newVal = 0;
        $(this).val(newVal.toFixed(2));
    }
});




$("body").on("click", ".removeItem", function (event) {
    $(this).parent().parent().remove();
});

$("body").on("click", ".saveItem", function (event) {
    $(this).attr('disabled', true);
    var inputArray = [];
    var link = $(this);
    var err = false;
    var procurementId = $('#procurementId').val();

    $(this).closest("tr:has(input)").each(function () {

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                {
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                }
            } else{
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                 
            }
        });

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                {
                     if($(this).attr('data-name')!= 'Location'){
                        inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                     }
                }
            } else
                {
                    if($(this).attr('data-name')!= 'Location'){
                        inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                     }
                }
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/autopickup/adddeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                $("#itemList").load(baseUrl + "/autopickup/getitemdetails/" + procurementId);
                //$("#infoBoxDetail").load(baseUrl + "/autopickup/getprocurementdetails/" + procurementId);
            }
        });

        $(this).hide();
        $(this).prev().show();
    }

});

$("body").on("click", ".editItem", function (event) {
    $(this).closest("tr").each(function () {

        $('.carmake', this).each(function () {
            var makeId = $(this).val();
            var make = $(this);
            var modelId = $(this).parent().parent().next().find('.automodelId').val();

            // Populate dropdown with list of subcategories
            $(this).parent().parent().next().find('.carmodel').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/autopickup/getmodellist/" + makeId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (modelId == entry.id)
                            $(make).parent().parent().next().find('.carmodel').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $(make).parent().parent().next().find('.carmodel').append($('<option></option>').attr('value', entry.id).text(entry.name));
                    });
                }
            });

        });

        $('.value_label', this).each(function () {
            $(this).hide();
        });
        $('.value_field', this).each(function () {
            $(this).show();
        });
    });
    $(this).hide();
    $(this).next().show();
});

$("body").on("click",".paymentStatusUpdate",function (event){
    var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
    if(r)
    {
        $("#updatepayment").submit();
    }
    return false;
});

$("body").on("click","#createShipmentbtn",function (event){
    var href = $(this).attr('data-href');
    var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
    if(r)
    {
        $.ajax({
                url: href,
                type: 'GET',
                success: function (response) {
                    window.location.reload();
                }
            });
    }
    return false;
});

$("body").on("click", ".updateItem", function (event) {
    $(this).attr('disabled', true);
    var inputArray = [];
    var link = $(this);
    var err = false;
    var procurementId = $('#procurementId').val();

    $(this).closest("tr:has(input)").each(function () {

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                {
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                }
            } else{
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                 
            }
        });

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                {
                     if($(this).attr('data-name')!= 'Location'){
                        inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                     }
                }
            } else
                {
                    if($(this).attr('data-name')!= 'Location'){
                        inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                     }
                }
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/autopickup/editdeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                $("#itemList").load(baseUrl + "/autopickup/getitemdetails/" + procurementId);
                //$("#infoBoxDetail").load(baseUrl + "/autopickup/getprocurementdetails/" + procurementId);
            }
        });

        $(this).hide();
        $(this).prev().show();
    }

});

$("body").on("click", ".deleteItem", function (event) {
    var id = $(this).attr('data-id');
    var procurementId = $('#procurementId').val();

    $.ajax({
        url: baseUrl + "/autopickup/deletedeliveryitem/" + id,
        type: 'GET',
        success: function (response) {
            if (response == 1) {
                $("#itemList").load(baseUrl + "/autopickup/getitemdetails/" + procurementId);
                //$("#infoBoxDetail").load(baseUrl + "/autopickup/getprocurementdetails/" + procurementId);
                
            }
        }
    });
});

$("body").on("change", ".carmake", function (event) {
   
    var makeId = $(this).val();
    var make = $(this);

    // Populate dropdown with list of subcategories
    $('.carmodel').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/autopickup/getmodellist/" + makeId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $('.carmodel').append($('<option></option>').attr('value', entry.id).text(entry.name));
            });
        }
    });

    //$(this).parent().next().next().find('.itemproduct').find('option:gt(0)').remove();
});




$("body").on("click", ".receiveItem", function (event) {
    $(this).attr('disabled', true);
    var procurementId = $('#procurementId').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: $('#receivePkgFrm').attr('action'),
        type: 'POST',
        data: $('#receivePkgFrm').serialize(),
        success: function (response) {
            if (response == 1) {
                $("#itemList").load(baseUrl + "/autopickup/getitemdetails/" + procurementId);
                $("#modal-addEdit").modal('hide');
                $("#procurementStatus").val('');
                checkProcurementStatus();
            }
        }
    });
});



function updateProcurementStatus(status) {
    var selected = [];
    var procurementstatus = [];
    var procurementId = $('#procurementId').val();
    var checkedId = $("#procurementStatus").attr('data-item');
    //alert(checkedId);

    if (status == '') {
        alert("Please select status to continue.");
        return false;
    }

    $('#itemList input:checked').each(function () {
        procurementstatus.push($(this).attr('data-attr'));
        selected.push($(this).attr('value'));
    });
    
    if (checkForDuplicates(procurementstatus) == 1) {

        if (procurementstatus[0] == 'submitted') {
            var statusArr = ["orderplaced", "unavailable"];
            if (jQuery.inArray(status, statusArr) < 0) {
                alert("Please update status to Order Placed or Unavailable");
                $("#procurementStatus").val('');
                return false;
            }
        } else if (procurementstatus[0] == 'orderplaced') {
            var statusArr = ["pickedup"];

            if (jQuery.inArray(status, statusArr) < 0) {
                alert("Please change status to Picked Up state first");
                $("#procurementStatus").val('');
                return false;
            }
        } else if (procurementstatus[0] == 'pickedup') {
            var statusArr = ["received"];

            if (jQuery.inArray(status, statusArr) < 0) {
                alert("Please specify if Package has been received");
                $("#procurementStatus").val('');
                return false;
            }
        } else if (procurementstatus[0] == 'unavailable' || procurementstatus[0] == 'received') {
            alert("Status cannot be further updated");
            $("#procurementStatus").val('');
            return false;
        }
        var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");

        if (r == true) {
            
            if (status != 'received') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: baseUrl + "/autopickup/updateprocurementitemstatus",
                    type: 'POST',
                    data: {procurementItemId: checkedId, status: status},
                    success: function (response) {
                        if (response == 1) {
                             $("#itemList").load(baseUrl + "/autopickup/getitemdetails/" + procurementId);
                             $("#procurementStatus").val('');
                             checkProcurementStatus();
                        }
                    }
                });
            } else {
                showAddEdit(checkedId, status, 'autopickup/receiveitem');
            }


        }
    } else {
        alert('Please select procurements having same status!!');
    }
}

$("body").on("click","#viewDeliveryNotes",function(e){
    $("#modal-view-deliverynotes").modal();
});

$("body").on("click", ".receiveItemNotes", function (event) {
    var checkedId = $("#procurementStatus").attr('data-item');
    var status = 'received';
    var itemNote = $("#itemReceivedNotes").val();
    console.log(itemNote);
    var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
    if(r)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/autopickup/updateprocurementitemstatus",
            type: 'POST',
            data: {procurementItemId: checkedId, status: status,deliveryNotes:itemNote},
            success: function (response) {
                if (response == 1) {
                     window.location.reload();
                }
            }
        });
    }
    
});

function updateStatus(status) {
    $("#wait").show();
    var checkedId = $("#procurementStatus").attr('data-item');
    console.log(status);
    if(status == 'received')
    {
        $("#modal-received-notes").modal();
    }
    else
    {
        var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
        if(r)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: baseUrl + "/autopickup/updateprocurementitemstatus",
                type: 'POST',
                data: {procurementItemId: checkedId, status: status},
                success: function (response) {
                    $("#wait").hide();
                    if (response == 1) {
                         window.location.reload();
                    }
                }
            });
        }
    }

}

function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
    return 1;
}


function setDefault(defaultType) {
    if ($('#addressBook').val() != '') {
        $('#isDefaultType').val(defaultType);
        $('#saveaddressbook').submit();
    } else {
        alert('Select an address');
        return false;
    }
}

function saveComment(shipmentId)
{
    var message = $('#addcomment').val();
    $.ajax({
        url: baseUrl + "/procurement/addcomment/" + shipmentId,
        type: 'POST',
        data: {comment: message},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response == '1')
            {
                $('#showmsg').html('<div class="success">Notes saved successfully</div>');
            } else {
                $('#showmsg').html('<div class="danger">Notes not saved</div>')
            }
        }
    });
}


function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}
function exportallse_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {

            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one shipment");
        }
        else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        //console.log(data);
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function checkProcurementStatus() {
    var procurementId = $('#shipmentId').val();
    
    
    $.ajax({
        url: baseUrl + "/procurement/checkprocurementstatus/" + procurementId,
        type: 'GET',
        success: function (response) {
            if (response == 0) {
                $('#createShipmentbtn').removeAttr('disabled');
            } else {
                $('#createShipmentbtn').attr('disabled', 'disabled');
            }
        }
    });

}


function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");

        if (r == true) {
            $('#checkedval').val(checkedIds);
            $('#frmCheckedItem').attr('action', baseUrl + "/procurementfees/deleteall");
            $('#frmCheckedItem').submit();
        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}

function getModelList() {
    
}

