 $(function () {  
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
        $('ul.progressbar li a').click(function () {
                $('ul.progressbar li').removeClass('active');
                $(this).parent().addClass('active');
            });
        //Colleps Box
        //Collapse Box
        $('.collapse').on('shown.bs.collapse', function () {
            $('.accordion-toggle').find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
        }).on('hidden.bs.collapse', function () {
            $('.accordion-toggle').find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
        });
        //Date range picker
        $(function () {
            $('#reservation').daterangepicker();
        })
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('.datepicker').datepicker({
            autoclose: true
        });
        //Custom radio button show daterange
        $('.customRange').on('ifChecked', function () {
            document.getElementById('customDaterange').style.visibility = 'visible';
        });
        $('.customRange').on('ifUnchecked', function () {
            document.getElementById('customDaterange').style.visibility = 'hidden';
        });
        //Tooltip	
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    $('#pickupCountry').on('change', function () {

        var countryId = $('#pickupCountry').val();

        $('#pickupState').find('option:gt(0)').remove();

        if (countryId != '') {
            var url = baseUrl + "/schedulepickup/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#pickupState').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    $('#pickupState').on('change', function () {

        var stateId = $('#pickupState').val();

        $('#pickupCity').find('option:gt(0)').remove();

        if (stateId != '') {
            var url = baseUrl + "/schedulepickup/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                if(data.length>0){
                    $('#cityText').remove();
                    $('#cityName').html("<select class='form-control input-lg customSelect2' id='pickupCity' name='searchSchedule[pickupCity]'><option value=''>All</option></select>");
                    $.each(data, function (key, entry) {
                        $('#pickupCity').append($('<option></option>').attr('value', entry.id).text(entry.name));
                    })
                }
                else{
                    $('#cityName').html('<input class="form-control input-lg" placeholder="" type="text" name="searchSchedule[pickupCity]">');
                }
                
            });

        }
    });

    $('#scheduleTocountry').on('change', function () {

        var countryId = $('#scheduleTocountry').val();

        $('#scheduleToState').find('option:gt(0)').remove();

        if (countryId != '') {
            var url = baseUrl + "/schedulepickup/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#scheduleToState').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });
        }
    });

    $('#scheduleToState').on('change', function () {

        var stateId = $('#scheduleToState').val();

        $('#scheduleToCity').find('option:gt(0)').remove();

        if (stateId != '') {
            var url = baseUrl + "/schedulepickup/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                if(data.length>0){
                    $('#cityToText').remove();
                    $('#cityToName').html("<select class='form-control input-lg customSelect2' id='scheduleToCity' name='searchSchedule[scheduleToCity]'><option value=''>All</option></select>");
                    $.each(data, function (key, entry) {
                        $('#scheduleToCity').append($('<option></option>').attr('value', entry.id).text(entry.name));
                    })
                }
                else{
                    $('#cityToName').html('<input class="form-control input-lg" placeholder="" type="text" name="searchSchedule[scheduleToCity]">');
                }
            });

        }
    });
});

function setDefault(defaultType) {
    if ($('#addressBook').val() != '') {
        $('#isDefaultType').val(defaultType);
        $('#saveaddressbook').submit();
    } else {
        alert('Select an address');
        return false;
    }
}


function scheduleNotification() {
            if ($('.checkbox:checked').length == 0) {
                alert('Please select atleast one item!!');
            } else {

                
                    var selected = [];
                        $('#checkboxes input:checked').each(function () {
                            selected.push($(this).attr('value'));
                        });

              
                   $('#notificationEmail').val(selected.join(','));     
              
                $('#modal-email').modal('show');
            }
        }
function changeStatus(scheduleId) {                
                value = $("#statusSelect").val();
                if(value == 'S'){                  
                        $('#modal-update-status').modal();
                        $('#schedule_block').show();
                }else{
                    $('#schedule_block').hide();
                }
            }
 
            

