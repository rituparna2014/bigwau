$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.addressBook').on('ifChecked', function (event) {
        $('#addressBook').val($(this).val());
    });

    $('#searchByDate').daterangepicker();

    $("#file-upload").change(function () {
        readURL(this);
    });

    $("#changeaddressFrm").validate();


    //Custom radio button show daterange
    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });

   
    $('.add-more-modal').click(function () {
        $('#modal_first_row').after($('#modal_first_row').html());
    });

    $('.addButt').click(function () {
        var table = $(this).parent().prev();
        table.find('tbody').append('<tr>' + $('#blank_items').html() + '</tr>');
    });

    $(document).on('blur', '.qty', function () {
        var qty = $(this).val();
        var value = $(this).parent().parent().find('.product-val').val();
        if ($.isNumeric(qty))
            $(this).parent().parent().find('.product-total').val(parseFloat(qty * value).toFixed(2));
        else
            $(this).val(0);

    });
    $(document).on('blur', '.product-val', function () {
        var value = $(this).val();
        var qty = $(this).parent().parent().find('.qty').val();
        if ($.isNumeric(value))
            $(this).parent().parent().find('.product-total').val(parseFloat(qty * value).toFixed(2));
    });

    $('#addMore').on('click', function () {
        var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
        data.find("input").val('');
    });

    $('.format').blur(function () {
        if ($.isNumeric(this.value)) {
            var newVal = parseFloat(this.value);
            $(this).val(newVal.toFixed(2));
        } else {
            var newVal = 0;
            $(this).val(newVal.toFixed(2));
        }
    });

    $("body").on("click", ".remove", function (event) {
        var trIndex = $(this).closest("tr").index();
        if (trIndex > 1) {
            $(this).closest("tr").remove();
        } else {
            alert("Sorry!! Can't remove first row!");
        }
    });

    $("body").on("click", ".addDeliveryItem", function (event) {
        $(this).attr('disabled', true);
        $(this).closest("tr:has(input)").each(function () {
            var inputArray = [];

            $('input', this).each(function () {
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            });
            $('select', this).each(function () {
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: baseUrl + "/shipments/adddeliveryitem",
                type: 'POST',
                data: {data: inputArray},
                success: function (response) {
                    window.location.href = window.location.href;
                }
            });
        });

    });

   
   });
   
$("body").on("change", ".carmake", function (event) {
   
    var makeId = $(this).val();
    var make = $(this);

    // Populate dropdown with list of subcategories
    $('.carmodel').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/autopickup/getmodellist/" + makeId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $('.carmodel').append($('<option></option>').attr('value', entry.id).text(entry.name));
            });
        }
    });

    //$(this).parent().next().next().find('.itemproduct').find('option:gt(0)').remove();
});

$("body").on("change",".autoImages",function(event) {
    var imageField = $(this);
    var formData = new FormData();
    var file = $(this)[0].files[0];
    formData.append('carImage',file);
    formData.append('carImageType',$(this).attr('data-imagetype'));
    var shipmentId = $("#shipmentId").val();
    $("#wait").show();
    $.ajax({
        url: baseUrl + "/autoshipment/uploadimages/"+shipmentId,
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (response) {
            $("#wait").hide();
            if(response.imageId!="-1")
            {
                //$(".autothumbnail").html("<img src='"+response+"'>");
                imageField.parent().find(".autothumbnail").html('<img src="'+response.imagePath+'" onclick="showAddEdit('+response.imageId+',1,\'autoshipment/showwarehouseimage\')">');
                imageField.parent().append('<a class="actionIcons color-theme-2"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i>Remove Image</a>');
                imageField.remove();
            }
            else
            {
                alert("Something went wrong! Please try again");
            }
        }
    });
});

$("body").on("click",".paymentStatusUpdate",function (event){
    var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
    if(r)
    {
        $("#updatepayment").submit();
    }
    return false;
});

$("body").on("click",".remove-autoimage",function(event) {
   var imageId = $(this).attr('data-id');
   $.ajax({
        url: baseUrl + "/autoshipment/deletewarehouseimage/" + imageId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if(response.return == 'success')
                window.location.reload();
            else
                alert("Something went wrong! Please try again");
        }
    });
});

$("body").on("click","#viewDeliveryNotes",function(e){
    $("#modal-view-deliverynotes").modal();
});

$(document).on('click', '#addPackaging', function () {

    var action = $("#frmaddpkg").attr("action");
    var data = $("#frmaddpkg").serialize();
    var shipmentId = $(this).attr('data-shipment');
    var page = $(this).attr('data-page');
    var baseUrl = $('#baseUrl').val();
    $.ajax({
        url: action,
        data: data,
        dataType: 'JSON',
        type: 'post',
        success: function (response) {
            if (response.message == 'success')
            {
                $("#packaginTab").load(baseUrl + "/shipments/getpackagingdata/" + shipmentId + "/" + page);
            }
        }
    });

    return false;


});

var baseUrl = $('#baseUrl').val();

function userVerification() {
    var userUnit = $('#userUnit').val();
    
    if (userUnit != '') {
        $.ajax({
            url: baseUrl + "/autoshipment/verifyuser/" + userUnit,
            type: 'GET',
            success: function (response) {
                if (response == 0) {
                    $('#user-verification').removeClass('text-success');
                    $('#user-verification').addClass('text-danger');
                    $('#user-verification').html('<b>This is an Invalid Customer.</b>');
                } else {
                    $("#submitBtn").prop("disabled", false);

                    $('#user-verification').removeClass('text-danger');
                    $('#user-verification').addClass('text-success');
                    $('#user-verification').html('<b>This is a valid Customer.</b>');
                    $('#modal-verify').html(response);
                    $('#modal-verify').modal('show');
                }
            }
        });
    } else {
        $("#addEditFrm input:not('#userUnit')").prop("disabled", true);
        $("#addEditFrm select").prop("disabled", true);

        $('#user-verification').removeClass('text-success');
        $('#user-verification').removeClass('text-danger');
        $('#user-verification').html('');
    }
}

function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");
        if (r == true) {
            $('#checkedval').val(checkedIds);
            $('#frmCheckedItem').attr('action', baseUrl + '/autoshipment/deleteSelected');
            $('#frmCheckedItem').submit();
        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}

function changeStatus() {
    var selected = [];
    var shipmentstatus = [];
    $('#checkboxes input:checked').each(function () {
        shipmentstatus.push($(this).attr('data-attr'));
        selected.push($(this).attr('value'));
    });
    if (checkForDuplicates(shipmentstatus) == 1) {
        var checkedIds = selected.join('^');
        if (checkedIds != '') {
            var r = confirm("Status of all the selected items will be modified. This action cannot be reversed back!!");
            if (r == true) {
                showAddEdit(checkedIds, 0, 'shipments/bulkchangestatus');
            }
        }
    } else {
        alert('Please select shipments having same status!!');
    }
}

function scheduleNotification() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        showAddEdit(checkedIds, 0, 'shipments/schedulenotification');
    } else {
        alert("Select atleast one field");
        return false;
    }
}

function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
    return 1;
}

function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    } else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}
function exportallse_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        } else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();

                $.ajax({
                    url: "shipments/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function getWarehouseZoneList(rowId) {
    $('#warehouseZoneId').find('option:gt(0)').remove();
    if (rowId != '') {
        var url = baseUrl + "/shipments/getwarehousezonelist/" + rowId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#warehouseZoneId').append($('<option></option>').attr('value', entry.id).text(entry.name));
            })
        });

    }
}

function updateShipmentType(shipmenttype, shipmentid) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/shipments/updateshipment/" + shipmentid,
        type: 'POST',
        data: {shipmentType: shipmenttype, field: 'shipmentType'},
        success: function (response) {
            if (response == '1') {
                if (shipmenttype == 'air')
                    var txt = 'Air';
                else
                    var txt = 'Sea';
                $('#shipmentTypeTxt').text(txt);

            }
        }
    });

}

function updateStatus(status) {
    $("#wait").show();
    var checkedId = $("#shipmentStatus").attr('data-item');
    var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
    if(r)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/autoshipment/updatestatus",
            type: 'POST',
            data: {shipmentId: checkedId, status: status},
            success: function (response) {
                $("#wait").hide();
                if (response == 1) {
                     window.location.reload();
                }
            }
        });
    }

}

$("body").on("click", ".deletelocation", function (event) {
    var warehouseLocationId = $(this).attr('data-warehouseid');
    var shipmentId = $('#shipmentId').val();
    $(this).confirmation({
        onConfirm: function () {
            var url = baseUrl + "/shipments/deletelocation/" + warehouseLocationId + "/" + shipmentId;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    if (response == 1) {
                        $("#locationTrack").load(baseUrl + "/shipments/getwarehouselocations/" + shipmentId);
                        alert('Warehouse location deleted successfully.');
                    }
                }
            });
        },
    });
});

(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

 function changestate(stateId) {
    
    var carStateId = stateId;
   
    
    if (carStateId != '') {
            var url = baseUrl + "/autoshipment/getCities/" + carStateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {                
                    $('#carCity').find('option:gt(0)').remove();
                $.each(data, function (key, entry) {
                    $('#carCity').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });
        }
    
}

function changeMake(makeId)
{
    var carMakeId = makeId;
   
    $('#makeModel').find('option:gt(0)').remove();
    if (carMakeId != '') {
            var url = baseUrl + "/autoshipment/getCarMake/" + carMakeId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {                
                    
                $.each(data, function (key, entry) {
                    $('#makeModel').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });
        }
}

$('#fromCountry').on('change', function () {
    var _token = $("input[name='_token']").val();
    var base_path_offers = $("input[name='base_path_offers']").val();

    $.ajax({
        url: base_path_offers + "getStates",
        type: 'POST',
        data: {_token: _token, countryId: $(this).val()},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                //console.log(data);
                $('#fromState').find('option:gt(0)').remove();
                $.each(data, function (key, entry) {
                    $('#fromState').append($('<option></option>').attr('value', entry.id).text(entry.name));
                });


            } else {
                printErrorMsg(data.error);
            }
        }
    });
});








