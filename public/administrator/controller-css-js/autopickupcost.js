$(function () {

    $('.format').blur(function () {
        if ($.isNumeric(this.value)) {
            var newVal = parseFloat(this.value);
            $(this).val(newVal.toFixed(2));
        } else {
            var newVal = 0;
            $(this).val(newVal.toFixed(2));
        }
    });
    $('.setCityValue').on('ifChecked', function () {
        var costToSet = $("#costToSet").val();
        //console.log('test');
        if (costToSet != '' && costToSet != '0.00')
        {
            //var confirm = window.confirm("This cost will be added to all cities of this state!!");
            //if(confirm == true)
            $("#frmedit input[type=text]").val(costToSet);
        } else
        {
            alert("Please enter cost");
            $('#costToSet').focus();
        }
    });

    $('#costToSet').change(function () {
        var cbChecked = $(".setCityValue").parent().hasClass("checked");
        if (cbChecked)
        {
            var costToSet = $("#costToSet").val();
            console.log(costToSet);
            if (costToSet != '' && costToSet != '0.00')
            {
                var confirm = window.confirm("This cost will be added to all cities of this state!!");
                if (confirm == true)
                    $("#frmedit input[type=text]").val(costToSet);
            } else
            {
                alert("Please enter cost first");
            }
        }

    });


});