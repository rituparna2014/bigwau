/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {
    
    //$('.addMoreRow').on('click', function () {
    $("body").on("click",".addMoreRow",function(event){
        $("#wait").show();

        var err = false;
        
        if($("#quickShipoutForm").valid())
        {
            

            $('#consolidatedtr tr').each(function () {              
                $('input, select', this).each(function () {

                if ($(this)[0].hasAttribute("required")) {
                    
                    if ($(this).val() == '') {
                        err = true;
                        alert($(this).attr('data-name') + " field is required.");
                        $(this).focus();
                        return false;

                    }else
                    {
                        err = false;
                    }

                }

            }); 
            });
            
            if(err == false)
            {  

                   var formData = $("#quickShipoutForm").serialize();
                    var formUrl = $("#quickShipoutForm").attr('action');
                    console.log(formUrl);
                    $.ajax({
                        method: "POST",
                        url: formUrl,
                        data : formData,
                    }).done(function (response) {
                        if(response == '1')
                        {    
                            var storeId = $("#tb tr:eq(1) #storeId").val();
                            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
                            data.find("input").val('');
                            data.find("#storeId").val(storeId);
                            data.find(".notify-customer").attr("disabled",false);
                            data.find(".notify-customer").attr("data-unit","");
                            data.find(".notify-customer").removeClass("btn-info");
                            data.find(".notify-customer").addClass("btn-success");
                            $("#wait").hide();
                        }
                        else {
                            alert('Something went wrong. Please reload the page');
                        }
                        
                    });


            }
         
        }
        else
        {
            $("#wait").hide();
        }
    });




    $("body").on("change",".eachItem input,select",function(event) {
        //$(this).find(".notify-customer").attr("disabled",false);
        $(this).parent().parent().find(".notify-customer").attr("disabled",false);
        $(this).parent().parent().find(".notify-customer").removeClass("btn-info");
        $(this).parent().parent().find(".notify-customer").addClass("btn-success");
    });
    
   $(".userUnit").on("blur", function (event) {
       var userUnit = $(this).val();
       showAddEdit(userUnit, "0", "consolidated/getusersettings");
       if(userUnit!="")
       {
            $(this).parent().parent().find(".notify-customer").attr("disabled",false);
            $(this).parent().parent().find(".notify-customer").attr("data-unit",userUnit);
       }
       else
       {
           $(this).parent().parent().find(".notify-customer").attr("disabled",true);
           $(this).parent().parent().find(".notify-customer").attr("data-unit","");
       }
   });
   
   $("body").on("click", ".notify-customer", function (event) {
       
        $("#wait").show();
        $(this).attr("disabled",true);
        var input = $(this);
        var userUnit = $(this).attr("data-unit");
        var storeName = $(this).parent().parent().find(".storeId option:selected").text();
        var productName = $(this).parent().parent().find(".siteProduct option:selected").text();
        var subcategoryName = $(this).parent().parent().find(".subCategory option:selected").text();
        var trackingNumber = $(this).parent().parent().find(".trackingNumber").val();
        var itemQty = $(this).parent().parent().find(".qty").val();
        var quickshipoutId = $("#quickshipoutId").val();
        var addedItem = $(this).parent().parent().find(".addedItem").val();
        var shipmentType = $(this).parent().parent().find(".shipmentType option:selected").text();
        var dataNotification = $(this).attr("data-notification");
        if(userUnit == "" || storeName == "" || trackingNumber == "" || itemQty == "")
        {
            alert("Please enter all values correctly");
        }
        else
        {
            $.ajax({
                method: "POST",
                url: baseUrl + '/consolidated/quick-shipout/notifycustomer',
                data : {
                        unit:userUnit,store:storeName,tracking:trackingNumber,itemQty:itemQty,productName:productName,subcategoryName:subcategoryName,
                        quickshipoutId:quickshipoutId,addedItem:addedItem,dataNotification:dataNotification,shipmentType:shipmentType
                       }
            }).done(function (response) {
                $("#wait").hide();
                if(response == "1")
                {
                    input.removeClass("btn-success");
                    input.addClass("btn-info");
                    //alert('Email sent successfully');
                }
                else if(response == "2") {
                    $(this).attr("disabled",false);
                    alert("Unable to send notification. Please try again");
                }
                else if(response == "3") {
                    $(this).attr("disabled",false);
                    alert("User not found. Please recheck Unit Number");
                }

            });
        }
   });
});


function checkShipoutValid(e)
{

    return false;
}

function saveandnotify(shipment)
{
  
    $("#wait").show();
        if($("#quickShipoutForm").valid())
        {
            
            var formData = $("#quickShipoutForm").serialize();
            var formUrl = $("#quickShipoutForm").attr('action');
            $.ajax({
                method: "POST",
                url: formUrl,
                data : formData + "&submit="+shipment,
                success: function (data) {
                    //console.log(data.path);
                  window.location = data.path;
                }
            });
        }

}


