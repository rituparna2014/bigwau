$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.addressBook').on('ifChecked', function (event) {
        $('#addressBook').val($(this).val());
    });

    $('#searchByDate').daterangepicker();

    //Custom radio button show daterange
    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });
    
     $("#receivePkgFrm").validate();

    var baseUrl = $('#baseUrl').val();

    /* Get State List For Search */
    $('#destinationCountry').on('change', function () {
        if ($('#destinationCountry').val() != '') {

            var countryId = $('#destinationCountry').val();
            var stateId = $('#destinationState').attr('data-value');

            $('#destinationState').find('option:gt(0)').remove();
            if (countryId != '') {
                var url = baseUrl + "/othervehicle/getstatelist/" + countryId;
                // Populate dropdown with list of provinces
                $.getJSON(url, function (data) {
                    $.each(data, function (key, entry) {
                        if (stateId == entry.id)
                            $('#destinationState').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $('#destinationState').append($('<option></option>').attr('value', entry.id).text(entry.name));
                    })
                });

            }
        }

    });

    /* Get City List For Search */
    $('#destinationState').on('change', function () {

        if ($('#destinationState').val() != '') {
            var stateId = $('#destinationState').val();
            var cityId = $('#destinationCity').attr('data-value');

            $('#destinationCity').find('option:gt(0)').remove();

            if (stateId != '') {
                var url = baseUrl + "/othervehicle/getcitylist/" + stateId;
                // Populate dropdown with list of provinces
                $.getJSON(url, function (data) {
                    $.each(data, function (key, entry) {
                        if (cityId == entry.id)
                            $('#destinationCity').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $('#destinationCity').append($('<option></option>').attr('value', entry.id).text(entry.name));
                    })


                });

            }
        }

    });

    $('.addButt').click(function () {
        var table = $(this).parent().prev();
        table.find('tbody').append('<tr>' + $('#blank_items').html() + '</tr>');
    });
    
    $('#addressBookId').on('change', function () {
        $('#changeaddressFrm')[0].reset();
        if ($(this).val() != 'new') {
            var id = $(this).val();
            $.getJSON(baseUrl + "/othervehicle/getaddressbookdetails/" + id, function (data) {
                $('#userAddressBookId').val(id);
                $('#title').val(data.title);
                $('#firstName').val(data.firstName);
                $('#lastName').val(data.lastName);
                $('#email').val(data.email);
                $('#address').val(data.address);
                $('#alternateAddress').val(data.alternateAddress);
                $('#zipcode').val(data.zipcode);
                $('#zipcode').val(data.zipcode);
                $('#phone').val(data.phone);
                $('#alternatePhone').val(data.alternatePhone);
                $('#addresscountryId').val(data.countryId);

                var url = baseUrl + "/othervehicle/getstatelist/" + data.countryId;
                // Populate dropdown with list of provinces
                $.getJSON(url, function (response) {
                    $.each(response, function (key, entry) {
                        if (entry.id == data.stateId)
                            $('#addressstateId').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $('#addressstateId').append($('<option></option>').attr('value', entry.id).text(entry.name));

                    })
                });

                var url = baseUrl + "/othervehicle/getcitylistbycountry/" + data.countryId;
                $.getJSON(url, function (response) {
                    $.each(response, function (key, entry) {
                        if (data.cityId != null && entry.id == data.cityId)
                            $('#addresscityId').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));

                    })
                });
            });

            $('#changeaddressFrm input').attr('readonly', 'readonly');
            $('#changeaddressFrm select').not('#addressBookId').attr('readonly', 'readonly').attr("style", "pointer-events: none;");

        } else {
            $('#changeaddressFrm input').removeAttr('readonly');
            $('#changeaddressFrm select').removeAttr('readonly').removeAttr("style");
        }
    });
    
    $('#addresscountryId').on('change', function () {
        var countryId = $('#addresscountryId').val();
        $('#addressstateId').find('option:gt(0)').remove();
        $('#addresscityId').find('option:gt(0)').remove();

        if (countryId != '') {
            var url = baseUrl + "/othervehicle/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addressstateId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

            var url = baseUrl + "/othervehicle/getcitylistbycountry/" + countryId;
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    $('#addressstateId').on('change', function () {
        var stateId = $('#addressstateId').val();
        $('#addresscityId').find('option:gt(0)').remove();
        if (stateId != '') {
            var url = baseUrl + "/othervehicle/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });
        }
    });

});

$("body").on("blur", ".format", function (event) {
    if ($.isNumeric(this.value)) {
        var newVal = parseFloat(this.value);
        $(this).val(newVal.toFixed(2));
    } else {
        var newVal = 0;
        $(this).val(newVal.toFixed(2));
    }
});

$("body").on("click", "#changeAddressBtn", function (event) {
    $(this).attr('disabled', true);
    var shipmentId = $('#shipmentId').val();
    var userId = $('#userId').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: $('#changeaddressFrm').attr('action'),
        type: 'POST',
        data: $('#changeaddressFrm').serialize(),
        success: function (response) {
            if (response == 1) {
                $(".shipList").load(baseUrl + "/othervehicle/getaddressdetails/" + shipmentId);
                $("#modal-addEdit").modal('hide');
                alert("Address details saved successfully.");

            }
        }
    });
});


$("body").on("click", ".removeItem", function (event) {
    $(this).parent().parent().remove();
});

$("body").on("click", ".saveItem", function (event) {
    $(this).attr('disabled', true);
    var inputArray = [];
    var link = $(this);
    var err = false;
    var procurementId = $('#shipmentId').val();

    $(this).closest("tr:has(input)").each(function () {

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                {
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                }
            } else {
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});

            }
        });

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                {
                    if ($(this).attr('data-name') != 'Location') {
                        inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                    }
                }
            } else
            {
                if ($(this).attr('data-name') != 'Location') {
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                }
            }
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/othervehicle/adddeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                $("#itemList").load(baseUrl + "/othervehicle/getpackagedetails/" + procurementId);
            }
        });

        $(this).hide();
        $(this).prev().show();
    }

});

$("body").on("click", ".editItem", function (event) {

    $(this).closest("tr").each(function () {

        $('.partsmake', this).each(function () {
            var makeId = $(this).val();
            var make = $(this);
            var modelId = $(this).parent().parent().next().find('.automodelId').val();

            // Populate dropdown with list of subcategories
            $(this).parent().parent().next().find('.partsmodel').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/othervehicle/getmodellist/" + makeId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (modelId == entry.id)
                            $(make).parent().parent().next().find('.partsmodel').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $(make).parent().parent().next().find('.partsmodel').append($('<option></option>').attr('value', entry.id).text(entry.name));
                    });
                }
            });

        });

        $('.value_label', this).each(function () {
            $(this).hide();
        });
        $('.value_field', this).each(function () {
            $(this).show();
        });
    });
    $(this).hide();
    $(this).next().show();
});

$("body").on("click", ".updateItem", function (event) {
    $(this).attr('disabled', true);
    var inputArray = [];
    var link = $(this);
    var err = false;
    var procurementId = $('#shipmentId').val();
   
    $(this).closest("tr:has(input)").each(function () {

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                {
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                }
            } else {
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});

            }
        });

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                {
                    if ($(this).attr('data-name') != 'Location') {
                        inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                    }
                }
            } else
            {
                if ($(this).attr('data-name') != 'Location') {
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                }
            }
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/othervehicle/editdeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                $("#itemList").load(baseUrl + "/othervehicle/getpackagedetails/" + procurementId);
            }
        });

        $(this).hide();
        $(this).prev().show();
    }

});

$("body").on("click", ".deleteItem", function (event) {
    var id = $(this).attr('data-id');
    var procurementId = $('#shipmentId').val();

    $.ajax({
        url: baseUrl + "/othervehicle/deletedeliveryitem/" + id,
        type: 'GET',
        success: function (response) {
            if (response == 1) {
                $("#itemList").load(baseUrl + "/othervehicle/getpackagedetails/" + procurementId);
            }
        }
    });
});

function changeMake(makeId, itemId) {

    // var makeId = $(this).val();
    // var make = $(this);

    // Populate dropdown with list of subcategories
    $('#itemModel' + itemId).find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/othervehicle/getmodellist/" + makeId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $('#itemModel' + itemId).append($('<option></option>').attr('value', entry.id).text(entry.name));
            });
        }
    });

    //$(this).parent().next().next().find('.itemproduct').find('option:gt(0)').remove();
}

$("body").on("change", ".partsmake", function (event) {

    var makeId = $(this).val();
    var make = $(this);

    // Populate dropdown with list of subcategories
    $('.partsmodel').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/othervehicle/getmodellist/" + makeId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $('.partsmodel').append($('<option></option>').attr('value', entry.id).text(entry.name));
            });
        }
    });

    //$(this).parent().next().next().find('.itemproduct').find('option:gt(0)').remove();
});


$("body").on("click", ".receivePackage", function (event) {
    $(this).attr('disabled', true);
    var procurementId = $('#shipmentId').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: $('#receivePkgFrm').attr('action'),
        type: 'POST',
        data: $('#receivePkgFrm').serialize(),
        success: function (response) {
            if (response == 1) {
                $("#itemList").load(baseUrl + "/othervehicle/getpackagedetails/" + procurementId);
                $("#modal-addEdit").modal('hide');
                $("#procurementStatus").val('');
                checkProcurementStatus();
            }
        }
    });
});



function updateProcurementStatus(status) {
    var selected = [];
    var procurementstatus = [];
    var procurementId = $('#shipmentId').val();

    if (status == '') {
        alert("Please select status to continue.");
        return false;
    }

    $('#itemList input:checked').each(function () {
        procurementstatus.push($(this).attr('data-attr'));
        selected.push($(this).attr('value'));
    });

    if (checkForDuplicates(procurementstatus) == 1) {

        if (procurementstatus[0] == 'submitted') {
            var statusArr = ["orderplaced", "unavailable"];
            if (jQuery.inArray(status, statusArr) < 0) {
                alert("Please update status to Order Placed or Unavailable");
                $("#procurementStatus").val('');
                return false;
            }
        } else if (procurementstatus[0] == 'orderplaced') {
            var statusArr = ["pickedup"];

            if (jQuery.inArray(status, statusArr) < 0) {
                alert("Please change status to Picked Up state first");
                $("#procurementStatus").val('');
                return false;
            }
        } else if (procurementstatus[0] == 'pickedup') {
            var statusArr = ["received"];

            if (jQuery.inArray(status, statusArr) < 0) {
                alert("Please specify if Package has been received");
                $("#procurementStatus").val('');
                return false;
            }
        } else if (procurementstatus[0] == 'unavailable' || procurementstatus[0] == 'received') {
            alert("Status cannot be further updated");
            $("#procurementStatus").val('');
            return false;
        }

        var checkedIds = selected.join('^');
        if (checkedIds != '') {
            var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");

            if (r == true) {
                $('#checkedval').val(checkedIds);
                if (status != 'received') {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: baseUrl + "/othervehicle/updateprocurementitemstatus",
                        type: 'POST',
                        data: {procurementItemIds: checkedIds, status: status},
                        success: function (response) {
                            if (response == 1) {
                                $("#itemList").load(baseUrl + "/othervehicle/getpackagedetails/" + procurementId);
                                $("#procurementStatus").val('');
                                checkProcurementStatus();
                            }
                        }
                    });
                } else {
                    showAddEdit(checkedIds, status, 'othervehicle/receivepackage');
                }


            }
        } else {
            alert("Select atleast one field");
            return false;
        }
    } else {
        alert('Please select procurements having same status!!');
    }
}

function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
    return 1;
}


function setDefault(defaultType) {
    if ($('#addressBook').val() != '') {
        $('#isDefaultType').val(defaultType);
        $('#saveaddressbook').submit();
    } else {
        alert('Select an address');
        return false;
    }
}

function saveComment(shipmentId)
{
    var message = $('#addcomment').val();
    $.ajax({
        url: baseUrl + "/othervehicle/addcomment/" + shipmentId,
        type: 'POST',
        data: {comment: message},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response == '1')
            {
                $('#showmsg').html('<div class="success">Notes saved successfully</div>');
            } else {
                $('#showmsg').html('<div class="danger">Notes not saved</div>')
            }
        }
    });
}


function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}
function exportallse_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {

            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one shipment");
        }
        else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        //console.log(data);
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function checkProcurementStatus() {
    var procurementId = $('#shipmentId').val();


    $.ajax({
        url: baseUrl + "/othervehicle/checkprocurementstatus/" + procurementId,
        type: 'GET',
        success: function (response) {
            if (response == 0) {
                $('#createShipmentbtn').removeAttr('disabled');
            } else {
                $('#createShipmentbtn').attr('disabled', 'disabled');
            }
        }
    });

}


function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");

        if (r == true) {
            $('#checkedval').val(checkedIds);
            $('#frmCheckedItem').attr('action', baseUrl + "/procurementfees/deleteall");
            $('#frmCheckedItem').submit();
        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}

$(document).on('blur', '.qty', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().prev().find('.product-val').val();
    var shipping = $(this).parent().parent().next().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().next().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$(document).on('blur', '.product-val', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().next().find('.qty').val();
    var shipping = $(this).parent().parent().next().next().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().next().next().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$(document).on('blur', '.product-shipping', function () {
    var shipping = $(this).val();
    var value = $(this).parent().parent().prev().prev().find('.product-val').val();
    var qty = $(this).parent().parent().prev().find('.qty').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});


$(document).on('blur', '.quantity', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().find('.productval').val();
    var shipping = $(this).parent().parent().find('.productshipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.producttotal').val((producttotal.toFixed(2)));
    } else {
        $(this).val(0);
    }
});
;
$(document).on('blur', '.productval', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().find('.quantity').val();
    var shipping = $(this).parent().parent().find('.productshipping').val();
    if (shipping == '')
        shipping = 0;
    if ($.isNumeric(value)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.producttotal').val((producttotal.toFixed(2)));
    } else {
        ;
        $(this).val(0);
    }
});

$(document).on('blur', '.productshipping', function () {
    var shipping = $(this).val();
    var qty = $(this).parent().parent().find('.quantity').val();
    var value = $(this).parent().parent().find('.productval').val();
    if (shipping == '')
        shipping = 0;
    if ($.isNumeric(shipping)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.producttotal').val((producttotal.toFixed(2)));
    } else {
        $(this).val(0);
    }
});

function calculateCost() {
    var shipmentId = $('#shipmentId').val();

    $.ajax({
        url: baseUrl + "/othervehicle/getprocurementcostdetails/" + shipmentId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                data: {weightCalculated: response.weightCalculated, totalWeight: response.totalWeight, totalQuantity: response.totalQuantity, totalItems: response.totalItems},
                url: baseUrl + "/othervehicle/procurementweightdetails/" + shipmentId,
            }).done(function (response) {
                $('#modal-addEdit').html(response);
                $('#modal-addEdit').modal({backdrop: 'static', keyboard: false});
                $('#modal-addEdit').modal('show');
            });
        }
    });


}

//Payment details Section Start

$("body").on("click", ".saveProcurementWeight", function (event) {
    var shipmentId = $('#shipmentId').val();

    if ($('#totalWeight').val() != '') {
        $(this).attr('disabled', true);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: $('#weightdetailsFrm').attr('action'),
            type: 'POST',
            data: $('#weightdetailsFrm').serialize(),
            success: function (response) {
                $("#modal-addEdit").modal('hide');
                $("#paymentDetails").load(baseUrl + "/othervehicle/paymentdetails/" + shipmentId);
            }
        });
    } else {
        alert("Please enter total weight to continue");
        return false;
    }
});



function sendCustomerInvoice(procurementId) {
    alert();
    $.ajax({
        url: baseUrl + "/othervehicle/sendcustomerinvoice/" + procurementId,
        type: 'GET',
        success: function (response) {
            window.location.href = window.location.href;
        }
    });
}

function notifyCustomer() {
    var shipmentId = $('#shipmentId').val();
    var messageId = $('#shipmentMessage').val();

    if (messageId != '')
        showAddEdit(messageId, shipmentId, 'othervehicle/notifycustomer');
    else
        alert("Please select a message to continue.");
}

function createShipment() {
    var shipmentId = $('#shipmentId').val();
    $.ajax({
        url: baseUrl + "/othervehicle/createshipment/" + shipmentId,
        type: 'GET',
        success: function (response) {
            window.location.href = window.location.href;
        }
    });

}

