/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {
    
    //$('.addMoreRow').on('click', function () {
    $("body").on("click",".addMoreRow",function(event){
        $("#wait").show();

        var err = false;
        
        if($("#quickShipoutForm").valid())
        {   
            $('#consolidatedtr tr').each(function () {              
                $('input, select', this).each(function () {

                if ($(this)[0].hasAttribute("required")) {
                    
                    if ($(this).val() == '') {
                        err = true;
                        alert($(this).attr('data-name') + " field is required.");
                        $(this).focus();
                        return false;

                    }else
                    {
                        err = false;
                    }

                }

            }); 
            });
            
            if(err == false)
            {  

                   var formData = $("#quickShipoutForm").serialize();
                    var formUrl = $("#quickShipoutForm").attr('action');
                    console.log(formUrl);
                    $.ajax({
                        method: "POST",
                        url: formUrl,
                        data : formData,
                    }).done(function (response) {
                        if(response == '1')
                        {    
                            //var storeId = $("#tb tr:eq(1) #storeId").val();
                            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
                            data.find("input").val('');
                            data.find(".notify-customer").attr("disabled",false);
                            data.find(".notify-customer").removeClass("btn-info");
                            data.find(".notify-customer").addClass("btn-success");
                            $("#wait").hide();
                        }
                        else {
                            alert('Something went wrong. Please reload the page');
                        }
                        
                    });


            }
         
        }
        else
        {
            $("#wait").hide();
        }
    });




    $("body").on("change",".eachItem input,select",function(event) {
        //$(this).find(".notify-customer").attr("disabled",false);
        $(this).parent().parent().find(".notify-customer").attr("disabled",false);
        $(this).parent().parent().find(".notify-customer").removeClass("btn-info");
        $(this).parent().parent().find(".notify-customer").addClass("btn-success");
    });
  
   $("body").on("click", ".notify-customer", function (event) {       
        $("#wait").show();
        $(this).attr("disabled",true);
        var input = $(this);
        var orderNumber = $(this).parent().parent().find(".orderNumber").val();
        var noofBoxes = $(this).parent().parent().find(".noofboxes").val();
        var totalboxes = $(this).parent().parent().find(".totalboxes").val();
        var itemQty = $(this).parent().parent().find(".qty").val();
        var quickshipoutId = $("#quickshipoutId").val();
        var addedItem = $(this).parent().parent().find(".addedItem").val();
        var shipmentType = $(this).parent().parent().find(".shipmentType option:selected").text();
        var dataNotification = $(this).attr("data-notification");
        if(quickshipoutId == "" || shipmentType == "" || orderNumber == "" || itemQty == "")
        {
            alert("Please enter all values correctly");
        }
        else
        {
            $.ajax({
                method: "POST",
                url: baseUrl + '/consolidated/quick-shipout-ocean/notifycustomer',
                data : {
                        orderNumber:orderNumber, noofBoxes:noofBoxes, totalboxes:totalboxes, itemQty:itemQty, quickshipoutId:quickshipoutId, addedItem:addedItem, dataNotification:dataNotification,shipmentType:shipmentType
                       }
            }).done(function (response) {
                $("#wait").hide();
                if(response == "1")
                {
                    input.removeClass("btn-success");
                    input.addClass("btn-info");
                }
                else if(response == "2") {
                    $(this).attr("disabled",false);
                    alert("Unable to send notification. Please try again");
                }
                else if(response == "3") {
                    $(this).attr("disabled",false);
                    alert("User not found. Please recheck Unit Number");
                }

            });
        }
   });
});


function checkShipoutValid(e)
{

    return false;
}

function saveandnotify(shipment)
{
  
    $("#wait").show();
        if($("#quickShipoutForm").valid())
        {
            
            var formData = $("#quickShipoutForm").serialize();
            var formUrl = $("#quickShipoutForm").attr('action');
            $.ajax({
                method: "POST",
                url: formUrl,
                data : formData + "&submit="+shipment,
                success: function (data) {
                    //console.log(data.path);
                  window.location = data.path;
                }
            });
        }

}


