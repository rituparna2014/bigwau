$(document).ready(function () {
    $(".copytoclip").click(function () {
        var c = $(this).attr('id');
        var copyText = $("#imageId" + c);
        copyText.select();
        document.execCommand("copy");
        $("#imageId" + c).css("background", "yellow");
        $("#imageId" + c).animate({backgroundColor: 'white'}, 3000);
    });
});


$(document).ready(function () {

    $(document).off('click', '#btn-more').on('click', '#btn-more', function () {
        var id = $(this).data('id');
        var token_csrf = $("#csrfToken").val();
        var base_path_loadata = $("input[name='base_path_loadata']").val();
        $("#btn-more").html("Loading....");
        $.ajax({
            url: base_path_loadata,
            method: "POST",
            data: {id: id, _token: token_csrf},
            dataType: "text",
            success: function (data)
            {
                if (data != '')
                {
                    $('#remove-row').remove();
                    $('#load-data').append(data);
                }
                else
                {
                    $('#btn-more').html("No Data");
                }
            }
        });
    });
});

$(function () {
    $(".selectable").selectable({
        selected: function () {

            var selectedItemList = $("#selected-item-list").empty();
            $(".selectable img").each(function (index) {
                if ($(this).hasClass("ui-selected")) {
                    selectedItemList.append(($(this).attr('id')) + ", ");
                }
            });
        },
        unselected: function () {
            var selectedItemList = $("#selected-item-list").empty();
            $(".selectable img").each(function (index) {
                if ($(this).hasClass("ui-selected")) {
                    selectedItemList.append(($(this).attr('id')) + ", ");
                }
            });
        }

    });
});

$(".ui-widget-content").click(function () {

    if ($(this).hasClass("ui-selected")) {
        $(this).removeClass('added');
    }
    else {
        $(this).addClass('added');
    }
});


function insertAtCaret(editorName) {
    var valuesreceived = $("#selected-item-list").text();
    var vlenght = valuesreceived.length;
    if (vlenght > 0) {
        var token_csrf = $("#csrfToken").val();
        var base_path_loaddataSelected = $("input[name='base_path_loaddataSelected']").val();
        var areaId = editorName;

        $.ajax({
            url: base_path_loaddataSelected,
            method: "POST",
            data: {valuesreceived: valuesreceived, _token: token_csrf},
            dataType: "text",
            success: function (data)
            {
                if (CKEDITOR.instances[areaId].mode == 'wysiwyg') {
                    CKEDITOR.instances[areaId].insertHtml(data);
                } else {
                    var caretPos = jQuery('textarea.cke_source')[0].selectionStart;
                    var textAreaTxt = jQuery('textarea.cke_source').val();
                    jQuery('textarea.cke_source').val(textAreaTxt.substring(0, caretPos) + data + textAreaTxt.substring(caretPos));
                }
                $('#modal-addEdit').modal('hide');

            }
        });
    }
}



(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);


function readURL(input) {
    if (input != 'error') {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#loaded').css('display', 'block');
                $('#loaded').attr('src', e.target.result);
                $('#storeIcon').css('display', 'none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $('#loaded').css('display', 'none');
        $('#storeIcon').css('display', 'block');
    }
}


$("#logo-id").change(function () {
    //alert(this.files[0]);
    readURL(this);
});


$(function () {
    $('#logo-id').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
        success: function () {
            //alert('Success');
            $("#submitMediaImage").css('display', 'block');

        },
        error: function () {
            alert('Error! File type not supported');
            readURL('error');
            $('#loaded').css('display', 'none');
            $('#storeIcon').css('display', 'block');
            $("#submitMediaImage").css('display', 'none');


        }
    });



});



