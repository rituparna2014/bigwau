$(document).ready(function(){
  var newsleterId = $("#newsleterId").val();
  
  if(newsleterId != 0 && newsleterId != ''){
    var subscribersCheck = $('#subscriberEmailselect').val();
    var sellerCheck = $('#sellerEmailselect').val();
    var buyerCheck = $('#buyerEmailselect').val();

    $('#example-enableCaseInsensitiveFiltering-subs').val(JSON.parse(subscribersCheck));
    $('#example-enableCaseInsensitiveFiltering-sellers').val(JSON.parse(sellerCheck));
    $('#example-enableCaseInsensitiveFiltering-buyers').val(JSON.parse(buyerCheck));
  }
  

  $('#example-enableCaseInsensitiveFiltering-subs').multiselect({
      enableCaseInsensitiveFiltering: true
  });

  $('#example-enableCaseInsensitiveFiltering-sellers').multiselect({
      enableCaseInsensitiveFiltering: true
  });

  $('#example-enableCaseInsensitiveFiltering-buyers').multiselect({
      enableCaseInsensitiveFiltering: true
  });

	$("#addeditFrm").validate();
  
    $('#scheduleDate').datepicker({
  		multidate: true,
  		format: 'yyyy-mm-dd',
	});
	$('#scheduleTime').datetimepicker({
        format: 'LT'
    });
    /*CKEDITOR.replace('editor2', {
        allowedContent: true,
	});*/
	/*Datepicker only opening for yearly,weekly,monthly and specific dates*/
	$("#scheduleNewsletterType").change(function(){
		$('#scheduleDate').val('');
		$('#scheduleDate').val('');
		
		if($(this).val() != 4){
    		$("#scheduleDatediv").css('display','block');
    		$("#scheduleTimediv").css('display','none');
    		$("#scheduleDate").attr('required','true');
    		$("#scheduleTime").removeAttr('required');
    		$('#scheduleRepeatDiv').css('display','block');
    		$("#scheduleRepeat").attr('required','true');
    	}else if($(this).val() == 4){
    		$("#scheduleDatediv").css('display','none');
    		$("#scheduleTimediv").css('display','block');
    		$("#scheduleTime").attr('required','true');
    		$("#scheduleDate").removeAttr('required');
    		$('#scheduleRepeatDiv').css('display','none');
    		$("#scheduleRepeat").removeAttr('required');
    	}
    });
    $('.clicktoSend').on('ifChecked', function () {
        var checkedVal = $(this).val();
        var baseUrl = $("#baseUrl").val();
        $.ajax({
            type: "get",
            dataType: 'json',
            url: baseUrl + '/' + getValue,
            data: {'checkedVal':checkedVal},
            success: function (msg) {
                //console.log(msg.updated);
                
            }
        });
    });

    /*Multiple Auto complete in newsletter page for sellers , buyers, subscribers*/

    //var itemsSubs = [ 'France', 'Italy', 'Malta', 'England','Australia', 'Spain', 'Scotland' ];
    var subsVal = $("#scubscriberData").val();
    var itemsSubs = $.parseJSON(subsVal); //convert to javascript array

    var sellersVal = $("#sellerData").val();
    var itemsSellers = $.parseJSON(sellersVal); //convert to javascript array

    var buyersVal = $("#buyerData").val();
    var itemsBuyers = $.parseJSON(buyersVal); //convert to javascript array
    
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#searchSubs" ).autocomplete({
        minLength: 0,
        source: function( request, response ) {
          response( $.ui.autocomplete.filter(
            itemsSubs, extractLast( request.term ) ) );
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          $(this).autocomplete("search", "");
          return false;
        }
      }).click(function() {
        $(this).autocomplete("search", "");
    });

    $( "#searchSellers" ).autocomplete({
        minLength: 0,
        source: function( request, response ) {
          response( $.ui.autocomplete.filter(
            itemsSellers, extractLast( request.term ) ) );
        },
        select: function( event, ui ) {
       
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      }).click(function() {

        $(this).autocomplete("search", "");
    });

      $( "#searchBuyers" ).autocomplete({
        minLength: 0,
        source: function( request, response ) {
          response( $.ui.autocomplete.filter(
            itemsBuyers, extractLast( request.term ) ) );
        },
        select: function( event, ui ) {
       
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      }).click(function() {
        $(this).autocomplete("search", "");
    });
});