
/* Header Search */
function changeSearch(keyword)
{
   var key =  keyword;
   $('#searchType').val(key);
   
   if(key == 'orderId')
   {
       $('#searchKey').attr('placeholder', "Please provide order ID");
       $('#selectSearch').html('Order ID <span class="caret skybg m-l-10"></span>');
   }
   else if(key == 'customerID')
   {
        $('#searchKey').attr('placeholder', "Please provide customer unit");
        $('#selectSearch').html('Customer ID<span class="caret skybg m-l-10"></span>');
   }
   else if(key == 'trackingID')
   {
        $('#searchKey').attr('placeholder', "Please provide tracking id from stores");
        $('#selectSearch').html('Tracking ID<span class="caret skybg m-l-10"></span>');
   }
   else{
       $('#searchKey').attr('placeholder', "Please provide shipment ID");
       $('#selectSearch').html('Shipment ID <span class="caret skybg m-l-10"></span>');
   }
}

function searchBySelection(){
    
    var searchKey = $('#searchKey').val();
    searchKey = searchKey.trim();
    var searchType = $('#searchType').val();
    if(searchKey !='' && searchType!='')
    {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        $.ajax({
            url: baseUrl + "/dashboard/searchbyselection/" + searchKey+"/"+searchType,
            type: 'POST',
            success: function (response) {
              console.log(response);
              if(response.indexOf('shipment')!= '-1'){
                  shipmentId = response.substring((response.indexOf('_')+1));
                  window.location = baseUrl + '/shipments/addedit/'+shipmentId+'/1';
              }
              else if(response == 2){
                 window.location = baseUrl + '/orders'; 
              }
              else if(response == 3){
                 window.location = baseUrl + '/users/index'; 
              }
              else if(response == '-1'){
                  alert('No record found');
              }
            }
        });
    }
    else
    {
        if(searchKey == '')
            alert('Please select option of what you like to search');
        else if(searchType == '')
            alert('Please enter search value');
    }
    
    
}
/* Header Search */