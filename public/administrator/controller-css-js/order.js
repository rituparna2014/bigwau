$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('#searchByDate').daterangepicker();

});

function changeStatus() {

    if ($("input[name=checkboxselected]:checked").length > 0) {
        var selected = [];
        var orderstatus = [];
        $('#checkboxes input:checked').each(function () {
            orderstatus.push($(this).attr('data-attr'));
            selected.push($(this).attr('value'));
        });
        //console.log(orderstatus);
        if (checkForDuplicates(orderstatus) == 1) {
            var checkedIds = selected.join('^');
            if (checkedIds != '') {
                var r = confirm("Status of all the selected items will be modified. This action cannot be reversed back!!");
                if (r == true) {
                    showAddEdit(checkedIds, 0, 'orders/bulkchangestatus');
                }
            }
        } else {
            alert('Please select orders having same status!!');
        }

    } else {
        alert('Please select atleast one record');
    }
}

function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
    return 1;
}

function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");
        if (r == true) {
            $.ajax({
                url: baseUrl +"/orders/delete/"+checkedIds+"/1",
                type: 'GET',
                success: function (data) {
                    $(".wrapper").html(data);
                }
            });
        }
    } else {
        alert("Select atleast one record");
        return false;
    }

}

function checkSelectRecord() {
    //var selected = [];
    if ($("input[name=checkboxselected]:checked").length > 0) {
        //selected.push($(this).attr('name'));
        $('#modal-export-selected').modal('show');
    } else {
        alert('Please select atleast one record');
    }
}

function exportallse_submit() {

    if ($('#exportAllse input[type=checkbox]:checked').length) {
        var selectedField = [];
        $('#exportAllse input:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });


        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        } else {


            $(document).ready(function () {

                var _token = $("input[name='_token']").val();
                var base_path_selected_export = $("input[name='base_path_selected_export']").val();

                $.ajax({
                    /*headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                     },*/
                    url: base_path_selected_export,
                    type: 'POST',
                    data: {selected: selected, selectedField: selectedField, _token: _token},
                    success: function (data) {
                        //alert(data.path);
                        window.location = data.path;
                        if ($.isEmptyObject(data.error)) {
                            //alert(data);
                        } else {
                            //alert(data.error);
                        }
                    }
                });


            });


        }
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export').modal('show');
        }, 1000);
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
        //return false;
    }


}

function viewSnapshot(shipmentId,packageId,deliveryId) { 
    $.ajax({
        url: baseUrl + "/orders/getsnapshots/" + shipmentId+"/"+deliveryId+"/"+packageId,
        type: 'get',
        success: function (response) {
            $(".images").html('No snapshot has been uploaded.');
            if(response!='')
            {
                $(".images").html('');
                $(".images").append(response);
                $('#defaultLoaded').css('display', 'none');
            }
            $('#modal-upload').modal('show');
        }
    });
    
}

function getLocationDetails(listIds) {
    //console.log(listIds);
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl+"/orders/getwerehouselocations",
        type: 'POST',
        data: {listIds : listIds},
        success: function (response) {
            if(response)
            {
                var array = JSON.parse(response);
                //console.log(array);
                for (var key in array) {
                    var value = array[key];
                    for(var ids in value) {
                        //console.log(key, value[ids]);
                        $("#location-"+key+"-"+value[ids]['id']).html(value[ids]['location']);
                    }
                    
                    //$("#location-"+key).html(value);
                }
                $(".warehouse-location").each(function(i,j){
                    if($(this).text()=="Loading ...") {
                        $(this).text("N/A");
                    }
                });
            }
        }
    });
}