-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: shoptomydoor
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stmd_activity_log`
--

DROP TABLE IF EXISTS `stmd_activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` int(11) DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_log_log_name_index` (`log_name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_activity_log`
--

LOCK TABLES `stmd_activity_log` WRITE;
/*!40000 ALTER TABLE `stmd_activity_log` DISABLE KEYS */;
INSERT INTO `stmd_activity_log` VALUES (1,'default','created',1,'App\\Model\\Shipmentwarehousemessage',1,'App\\Model\\UserAdmin','[]','2018-06-22 13:01:55','2018-06-22 13:01:55'),(2,'othercharges','created',1,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":1,\"shipmentId\":13,\"otherChargeId\":75,\"otherChargeName\":\"Secured Packing Box (18x18x12)\",\"otherChargeAmount\":\"4.99\",\"notes\":\"Test\"}}','2018-06-22 13:38:51','2018-06-22 13:38:51'),(3,'othercharges','created',2,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":2,\"shipmentId\":13,\"otherChargeId\":75,\"otherChargeName\":\"Secured Packing Box (18x18x12)\",\"otherChargeAmount\":\"4.99\",\"notes\":\"Test\"}}','2018-06-22 13:38:52','2018-06-22 13:38:52'),(4,'othercharges','deleted',2,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":2,\"shipmentId\":13,\"otherChargeId\":75,\"otherChargeName\":\"Secured Packing Box (18x18x12)\",\"otherChargeAmount\":\"4.99\",\"notes\":\"Test\"}}','2018-06-22 13:38:54','2018-06-22 13:38:54'),(5,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-25 07:08:37','2018-06-25 07:08:37'),(6,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-25 07:08:49','2018-06-25 07:08:49'),(7,'shipment','created',15,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-25 07:40:46','2018-06-25 07:40:46'),(8,'warehouseLocation','created',5,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"shipmentId\":15,\"warehouseRowId\":31,\"warehouseZoneId\":78}}','2018-06-25 07:40:46','2018-06-25 07:40:46'),(9,'shipment','updated',15,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-25 07:40:46','2018-06-25 07:40:46'),(10,'warehouseLocation','created',6,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":6,\"shipmentId\":15,\"warehouseRowId\":31,\"warehouseZoneId\":80}}','2018-06-26 06:04:23','2018-06-26 06:04:23'),(11,'shipment','updated',15,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-26 12:59:11','2018-06-26 12:59:11'),(12,'shipment','updated',15,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-26 12:59:11','2018-06-26 12:59:11'),(13,'shipment','updated',15,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-26 13:07:29','2018-06-26 13:07:29'),(14,'othercharges','created',2,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":2,\"shipmentId\":15,\"otherChargeId\":44,\"otherChargeName\":\"Fragile Packaging\",\"otherChargeAmount\":\"4.99\",\"notes\":\"Test\"}}','2018-06-26 13:07:46','2018-06-26 13:07:46'),(15,'othercharges','deleted',2,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":2,\"shipmentId\":15,\"otherChargeId\":44,\"otherChargeName\":\"Fragile Packaging\",\"otherChargeAmount\":\"4.99\",\"notes\":\"Test\"}}','2018-06-26 13:08:05','2018-06-26 13:08:05'),(16,'othercharges','created',3,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":3,\"shipmentId\":15,\"otherChargeId\":40,\"otherChargeName\":\"TV Crating\",\"otherChargeAmount\":\"29.99\",\"notes\":\"Test\"}}','2018-06-26 13:08:20','2018-06-26 13:08:20'),(17,'othercharges','deleted',3,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":3,\"shipmentId\":15,\"otherChargeId\":40,\"otherChargeName\":\"TV Crating\",\"otherChargeAmount\":\"29.99\",\"notes\":\"Test\"}}','2018-06-26 13:08:57','2018-06-26 13:08:57'),(18,'othercharges','created',4,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":4,\"shipmentId\":15,\"otherChargeId\":58,\"otherChargeName\":\"Liquid Packaging\",\"otherChargeAmount\":\"4.99\",\"notes\":\"Test\"}}','2018-06-26 13:13:15','2018-06-26 13:13:15'),(19,'othercharges','created',5,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"shipmentId\":15,\"otherChargeId\":0,\"otherChargeName\":\"Test\",\"otherChargeAmount\":\"200.00\",\"notes\":null}}','2018-06-26 13:39:03','2018-06-26 13:39:03'),(20,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:42:15','2018-06-27 07:42:15'),(21,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:42:31','2018-06-27 07:42:31'),(22,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:42:33','2018-06-27 07:42:33'),(23,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:43:00','2018-06-27 07:43:00'),(24,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:43:01','2018-06-27 07:43:01'),(25,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:43:07','2018-06-27 07:43:07'),(26,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:43:14','2018-06-27 07:43:14'),(27,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:43:41','2018-06-27 07:43:41'),(28,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:45:01','2018-06-27 07:45:01'),(29,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:45:14','2018-06-27 07:45:14'),(30,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:45:14','2018-06-27 07:45:14'),(31,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-27 07:45:15','2018-06-27 07:45:15'),(32,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:45:15','2018-06-27 07:45:15'),(33,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:45:15','2018-06-27 07:45:15'),(34,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:45:35','2018-06-27 07:45:35'),(35,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:45:44','2018-06-27 07:45:44'),(36,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:45:46','2018-06-27 07:45:46'),(37,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:45:54','2018-06-27 07:45:54'),(38,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:47:37','2018-06-27 07:47:37'),(39,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:48:05','2018-06-27 07:48:05'),(40,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:48:28','2018-06-27 07:48:28'),(41,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:48:52','2018-06-27 07:48:52'),(42,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:50:59','2018-06-27 07:50:59'),(43,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:57:05','2018-06-27 07:57:05'),(44,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:57:19','2018-06-27 07:57:19'),(45,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 07:57:27','2018-06-27 07:57:27'),(46,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 08:00:07','2018-06-27 08:00:07'),(47,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 08:00:26','2018-06-27 08:00:26'),(48,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 08:00:56','2018-06-27 08:00:56'),(49,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 08:10:04','2018-06-27 08:10:04'),(50,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 08:10:44','2018-06-27 08:10:44'),(51,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 08:10:54','2018-06-27 08:10:54'),(52,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-06-27 08:10:54','2018-06-27 08:10:54'),(53,'shipment','updated',15,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-06-28 06:03:20','2018-06-28 06:03:20'),(54,'shipment','updated',15,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":15,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-07-06 12:58:48','2018-07-06 12:58:48'),(55,'shipment','updated',7,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":7,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":7,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-07-06 12:58:48','2018-07-06 12:58:48'),(56,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"sea\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-07-20 16:10:09','2018-07-20 16:10:09'),(57,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"sea\",\"storageCharge\":\"26.98\"}}','2018-07-20 16:10:20','2018-07-20 16:10:20'),(58,'warehouseLocation','created',8,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":8,\"shipmentId\":14,\"warehouseRowId\":31,\"warehouseZoneId\":78}}','2018-07-21 19:24:30','2018-07-21 19:24:30'),(59,'warehouseLocation','created',7,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":7,\"shipmentId\":14,\"warehouseRowId\":31,\"warehouseZoneId\":78}}','2018-07-21 19:24:30','2018-07-21 19:24:30'),(60,'warehouseLocation','updated',7,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":7,\"shipmentId\":14,\"warehouseRowId\":31,\"warehouseZoneId\":79},\"old\":{\"id\":7,\"shipmentId\":14,\"warehouseRowId\":31,\"warehouseZoneId\":78}}','2018-07-21 19:24:54','2018-07-21 19:24:54'),(61,'shipment','updated',12,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"1.25\"},\"old\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-07-21 19:31:32','2018-07-21 19:31:32'),(62,'shipment','updated',12,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"1.25\"},\"old\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"1.25\"}}','2018-07-21 19:33:47','2018-07-21 19:33:47'),(63,'shipment','updated',5,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"5.44\"},\"old\":{\"id\":5,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-07-24 09:36:46','2018-07-24 09:36:46'),(64,'shipment','updated',5,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"5.44\"},\"old\":{\"id\":5,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"5.44\"}}','2018-07-24 09:37:16','2018-07-24 09:37:16'),(65,'shipment','updated',5,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"5.44\"},\"old\":{\"id\":5,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"5.44\"}}','2018-07-24 09:37:33','2018-07-24 09:37:33'),(66,'shipment','updated',5,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"5.44\"},\"old\":{\"id\":5,\"toCountry\":230,\"toState\":89,\"toCity\":null,\"toName\":\"Ritu Mitra\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"2342423424\",\"toEmail\":\"rituparna.mitra@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"5.44\"}}','2018-07-24 09:38:32','2018-07-24 09:38:32'),(67,'shipment','updated',14,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":14,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null},\"old\":{\"id\":14,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":null}}','2018-07-24 16:30:30','2018-07-24 16:30:30'),(68,'shipment','updated',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"},\"old\":{\"id\":13,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"26.98\"}}','2018-07-24 16:30:30','2018-07-24 16:30:30'),(69,'shipment','updated',12,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"1.25\"},\"old\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"1.25\"}}','2018-07-24 16:30:30','2018-07-24 16:30:30'),(70,'shipment','updated',12,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"1.25\"},\"old\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"air\",\"storageCharge\":\"1.25\"}}','2018-07-24 16:43:02','2018-07-24 16:43:02'),(71,'shipment','created',1,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":1,\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-08-02 12:07:42','2018-08-02 12:07:42'),(72,'warehouseLocation','created',1,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":1,\"shipmentId\":1,\"warehouseRowId\":31,\"warehouseZoneId\":79}}','2018-08-02 12:07:42','2018-08-02 12:07:42'),(73,'shipment','created',1,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":1,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-11 06:01:04','2018-09-11 06:01:04'),(74,'shipment','created',2,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":2,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-11 06:01:06','2018-09-11 06:01:06'),(75,'shipment','created',3,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":3,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-11 07:39:57','2018-09-11 07:39:57'),(76,'shipment','created',4,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":4,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-11 07:40:05','2018-09-11 07:40:05'),(77,'shipment','created',5,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"toCountry\":163,\"toState\":5,\"toCity\":378,\"toName\":\"Nduka Udeh\",\"toCompany\":null,\"toAddress\":\"12011 Westbrae Parkway\",\"toPhone\":\"7135301160\",\"toEmail\":\"n.udeh@shoptomydoor.com\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-11 09:25:20','2018-09-11 09:25:20'),(78,'shipment','created',6,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":6,\"toCountry\":163,\"toState\":15,\"toCity\":638,\"toName\":\"Paulami Sarkar\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"6767\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-12 13:32:02','2018-09-12 13:32:02'),(79,'shipment','created',7,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":7,\"toCountry\":163,\"toState\":5,\"toCity\":378,\"toName\":\"Nduka Udeh\",\"toCompany\":null,\"toAddress\":\"12011 Westbrae Parkway\",\"toPhone\":\"7135301160\",\"toEmail\":\"n.udeh@shoptomydoor.com\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-13 06:13:03','2018-09-13 06:13:03'),(80,'default','created',1,'App\\Model\\Shipmentwarehousemessage',1,'App\\Model\\UserAdmin','[]','2018-09-13 06:31:26','2018-09-13 06:31:26'),(81,'shipment','created',8,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":8,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-13 07:58:22','2018-09-13 07:58:22'),(82,'shipment','created',9,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":9,\"toCountry\":230,\"toState\":90,\"toCity\":239,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-13 08:25:31','2018-09-13 08:25:31'),(83,'shipment','created',10,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":10,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-13 09:04:10','2018-09-13 09:04:10'),(84,'shipment','created',11,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":11,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-13 09:15:38','2018-09-13 09:15:38'),(85,'shipment','created',12,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":12,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-14 08:37:12','2018-09-14 08:37:12'),(86,'othercharges','created',1,'App\\Model\\Shipmentothercharges',NULL,NULL,'{\"attributes\":{\"id\":1,\"shipmentId\":1,\"otherChargeId\":0,\"otherChargeName\":\"Delivery Snap Shot Charge\",\"otherChargeAmount\":\"6.99\",\"notes\":null}}','2018-09-14 14:13:38','2018-09-14 14:13:38'),(87,'othercharges','created',2,'App\\Model\\Shipmentothercharges',NULL,NULL,'{\"attributes\":{\"id\":2,\"shipmentId\":1,\"otherChargeId\":0,\"otherChargeName\":\"Delivery Recount Charge\",\"otherChargeAmount\":\"6.99\",\"notes\":null}}','2018-09-14 14:14:08','2018-09-14 14:14:08'),(88,'othercharges','created',3,'App\\Model\\Shipmentothercharges',NULL,NULL,'{\"attributes\":{\"id\":3,\"shipmentId\":1,\"otherChargeId\":0,\"otherChargeName\":\"Delivery Reweigh Charge\",\"otherChargeAmount\":\"6.99\",\"notes\":null}}','2018-09-14 14:14:18','2018-09-14 14:14:18'),(89,'othercharges','created',4,'App\\Model\\Shipmentothercharges',NULL,NULL,'{\"attributes\":{\"id\":4,\"shipmentId\":2,\"otherChargeId\":0,\"otherChargeName\":\"Delivery Snap Shot Charge\",\"otherChargeAmount\":\"6.99\",\"notes\":null}}','2018-09-14 14:14:43','2018-09-14 14:14:43'),(90,'shipment','updated',11,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":11,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":11,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:12:33','2018-09-17 14:12:33'),(91,'shipment','updated',9,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":9,\"toCountry\":230,\"toState\":90,\"toCity\":239,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":9,\"toCountry\":230,\"toState\":90,\"toCity\":239,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:12:33','2018-09-17 14:12:33'),(92,'shipment','updated',6,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":6,\"toCountry\":163,\"toState\":15,\"toCity\":638,\"toName\":\"Paulami Sarkar\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"6767\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":6,\"toCountry\":163,\"toState\":15,\"toCity\":638,\"toName\":\"Paulami Sarkar\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"6767\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:12:33','2018-09-17 14:12:33'),(93,'shipment','updated',4,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":4,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":4,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:12:33','2018-09-17 14:12:33'),(94,'shipment','updated',3,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":3,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":3,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:12:33','2018-09-17 14:12:33'),(95,'shipment','updated',11,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":11,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":11,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:14:23','2018-09-17 14:14:23'),(96,'shipment','updated',9,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":9,\"toCountry\":230,\"toState\":90,\"toCity\":239,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":9,\"toCountry\":230,\"toState\":90,\"toCity\":239,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:14:23','2018-09-17 14:14:23'),(97,'shipment','updated',6,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":6,\"toCountry\":163,\"toState\":15,\"toCity\":638,\"toName\":\"Paulami Sarkar\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"6767\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":6,\"toCountry\":163,\"toState\":15,\"toCity\":638,\"toName\":\"Paulami Sarkar\",\"toCompany\":null,\"toAddress\":\"Address 1\",\"toPhone\":\"6767\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:14:23','2018-09-17 14:14:23'),(98,'shipment','updated',4,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":4,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":4,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:14:23','2018-09-17 14:14:23'),(99,'shipment','updated',3,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":3,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":3,\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toName\":\"somnath pahari\",\"toCompany\":null,\"toAddress\":\"211 South Meadows Court\",\"toPhone\":\"713-530-1160\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-17 14:14:23','2018-09-17 14:14:23'),(100,'shipment','created',13,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":13,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-19 08:47:24','2018-09-19 08:47:24'),(101,'shipment','created',14,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":14,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-19 08:50:52','2018-09-19 08:50:52'),(102,'warehouseLocation','created',1,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":1,\"shipmentId\":14,\"warehouseRowId\":33,\"warehouseZoneId\":177}}','2018-09-19 08:50:52','2018-09-19 08:50:52'),(103,'shipment','created',15,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":15,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-19 09:09:36','2018-09-19 09:09:36'),(104,'warehouseLocation','created',2,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":2,\"shipmentId\":15,\"warehouseRowId\":33,\"warehouseZoneId\":177}}','2018-09-19 09:09:36','2018-09-19 09:09:36'),(105,'shipment','created',16,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":16,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-19 09:10:27','2018-09-19 09:10:27'),(106,'warehouseLocation','created',3,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":3,\"shipmentId\":16,\"warehouseRowId\":33,\"warehouseZoneId\":177}}','2018-09-19 09:10:27','2018-09-19 09:10:27'),(107,'shipment','created',17,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":17,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-19 09:10:57','2018-09-19 09:10:57'),(108,'warehouseLocation','created',4,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":4,\"shipmentId\":17,\"warehouseRowId\":33,\"warehouseZoneId\":177}}','2018-09-19 09:10:57','2018-09-19 09:10:57'),(109,'shipment','created',18,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":18,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-20 02:03:08','2018-09-20 02:03:08'),(110,'shipment','created',19,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":19,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-20 04:38:40','2018-09-20 04:38:40'),(111,'warehouseLocation','created',5,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"shipmentId\":19,\"warehouseRowId\":35,\"warehouseZoneId\":304}}','2018-09-20 04:38:41','2018-09-20 04:38:41'),(112,'shipment','created',20,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-20 04:39:18','2018-09-20 04:39:18'),(113,'warehouseLocation','created',6,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":6,\"shipmentId\":20,\"warehouseRowId\":35,\"warehouseZoneId\":304}}','2018-09-20 04:39:18','2018-09-20 04:39:18'),(114,'shipment','updated',20,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"},\"old\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-20 04:39:19','2018-09-20 04:39:19'),(115,'default','created',2,'App\\Model\\Shipmentwarehousemessage',1,'App\\Model\\UserAdmin','[]','2018-09-20 04:56:56','2018-09-20 04:56:56'),(116,'warehouseLocation','created',7,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":7,\"shipmentId\":20,\"warehouseRowId\":36,\"warehouseZoneId\":349}}','2018-09-20 04:58:13','2018-09-20 04:58:13'),(117,'warehouseLocation','created',8,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":8,\"shipmentId\":20,\"warehouseRowId\":36,\"warehouseZoneId\":349}}','2018-09-20 04:58:13','2018-09-20 04:58:13'),(118,'warehouseLocation','deleted',8,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":8,\"shipmentId\":20,\"warehouseRowId\":36,\"warehouseZoneId\":349}}','2018-09-20 04:58:25','2018-09-20 04:58:25'),(119,'othercharges','created',5,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":5,\"shipmentId\":20,\"otherChargeId\":58,\"otherChargeName\":\"Liquid Packaging\",\"otherChargeAmount\":\"4.99\",\"notes\":\"this needs lequid package\"}}','2018-09-20 05:00:40','2018-09-20 05:00:40'),(120,'shipment','updated',20,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"},\"old\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"}}','2018-09-20 05:00:40','2018-09-20 05:00:40'),(121,'othercharges','created',6,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":6,\"shipmentId\":20,\"otherChargeId\":54,\"otherChargeName\":\"Delivery Count (20 to 50 items)\",\"otherChargeAmount\":\"2.99\",\"notes\":\"ttt\"}}','2018-09-20 05:38:21','2018-09-20 05:38:21'),(122,'shipment','updated',20,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"},\"old\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"}}','2018-09-20 05:38:21','2018-09-20 05:38:21'),(123,'othercharges','created',7,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":7,\"shipmentId\":20,\"otherChargeId\":55,\"otherChargeName\":\"Delivery Count (50 to 100 items)\",\"otherChargeAmount\":\"4.99\",\"notes\":\"hhh\"}}','2018-09-20 05:40:52','2018-09-20 05:40:52'),(124,'shipment','updated',20,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"},\"old\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"}}','2018-09-20 05:40:52','2018-09-20 05:40:52'),(125,'shipment','created',21,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-20 05:48:37','2018-09-20 05:48:37'),(126,'warehouseLocation','created',9,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":9,\"shipmentId\":21,\"warehouseRowId\":34,\"warehouseZoneId\":225}}','2018-09-20 05:48:37','2018-09-20 05:48:37'),(127,'shipment','updated',21,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"},\"old\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-20 05:48:37','2018-09-20 05:48:37'),(128,'othercharges','created',8,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":8,\"shipmentId\":21,\"otherChargeId\":40,\"otherChargeName\":\"TV Crating\",\"otherChargeAmount\":\"29.99\",\"notes\":\"tv to pack\"}}','2018-09-20 05:50:24','2018-09-20 05:50:24'),(129,'shipment','updated',21,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"},\"old\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"}}','2018-09-20 05:50:24','2018-09-20 05:50:24'),(130,'othercharges','created',9,'App\\Model\\Shipmentothercharges',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":9,\"shipmentId\":21,\"otherChargeId\":40,\"otherChargeName\":\"TV Crating\",\"otherChargeAmount\":\"29.99\",\"notes\":\"tv to pack\"}}','2018-09-20 05:50:55','2018-09-20 05:50:55'),(131,'shipment','updated',21,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"},\"old\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"}}','2018-09-20 05:50:55','2018-09-20 05:50:55'),(132,'shipment','updated',21,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"},\"old\":{\"id\":21,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"}}','2018-09-20 07:58:58','2018-09-20 07:58:58'),(133,'shipment','updated',20,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"},\"old\":{\"id\":20,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"}}','2018-09-20 07:59:34','2018-09-20 07:59:34'),(134,'shipment','updated',19,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":19,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null},\"old\":{\"id\":19,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-20 07:59:34','2018-09-20 07:59:34'),(135,'shipment','updated',11,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":11,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null},\"old\":{\"id\":11,\"toCountry\":230,\"toState\":90,\"toCity\":588,\"toName\":\"Joy Karmakar\",\"toCompany\":null,\"toAddress\":\"SDF Building\",\"toPhone\":\"9831723608\",\"toEmail\":\"karmakar.joy@gmail.com\",\"shipmentType\":\"autopart\",\"storageCharge\":null}}','2018-09-20 08:48:47','2018-09-20 08:48:47'),(136,'shipment','created',22,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":22,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-21 06:58:06','2018-09-21 06:58:06'),(137,'shipment','created',23,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":23,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-24 05:39:09','2018-09-24 05:39:09'),(138,'warehouseLocation','created',10,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":10,\"shipmentId\":23,\"warehouseRowId\":31,\"warehouseZoneId\":79}}','2018-09-24 05:39:10','2018-09-24 05:39:10'),(139,'shipment','created',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-24 05:47:24','2018-09-24 05:47:24'),(140,'warehouseLocation','created',11,'App\\Model\\Shipmentwarehouselocation',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":11,\"shipmentId\":24,\"warehouseRowId\":31,\"warehouseZoneId\":78}}','2018-09-24 05:47:25','2018-09-24 05:47:25'),(141,'shipment','updated',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"2.00\"},\"old\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":null}}','2018-09-24 05:47:25','2018-09-24 05:47:25'),(142,'shipment','updated',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"},\"old\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"2.00\"}}','2018-09-25 04:04:21','2018-09-25 04:04:21'),(143,'shipment','updated',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"},\"old\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"}}','2018-09-25 04:04:47','2018-09-25 04:04:47'),(144,'shipment','updated',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"},\"old\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"11.24\"}}','2018-09-25 04:09:29','2018-09-25 04:09:29'),(145,'shipment','updated',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"0.10\"},\"old\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"10.00\"}}','2018-09-25 04:16:28','2018-09-25 04:16:28'),(146,'shipment','updated',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"0.10\"},\"old\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"0.10\"}}','2018-09-25 07:22:29','2018-09-25 07:22:29'),(147,'shipment','updated',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"0.10\"},\"old\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"0.10\"}}','2018-09-25 07:24:20','2018-09-25 07:24:20'),(148,'shipment','updated',24,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"0.10\"},\"old\":{\"id\":24,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"othershipment\",\"storageCharge\":\"0.10\"}}','2018-09-25 07:25:10','2018-09-25 07:25:10'),(149,'shipment','created',25,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":25,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-26 05:24:16','2018-09-26 05:24:16'),(150,'shipment','updated',25,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":25,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null},\"old\":{\"id\":25,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-26 05:24:17','2018-09-26 05:24:17'),(151,'shipment','created',26,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":26,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-26 06:37:51','2018-09-26 06:37:51'),(152,'shipment','created',27,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":27,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-26 06:39:30','2018-09-26 06:39:30'),(153,'shipment','created',28,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":28,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-26 06:41:50','2018-09-26 06:41:50'),(154,'shipment','created',29,'App\\Model\\Shipment',1,'App\\Model\\UserAdmin','{\"attributes\":{\"id\":29,\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toName\":\"Tathagata sur\",\"toCompany\":null,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toPhone\":\"+91+90+92+9208961186006\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"shipmentType\":\"shopforme\",\"storageCharge\":null}}','2018-09-26 06:45:41','2018-09-26 06:45:41');
/*!40000 ALTER TABLE `stmd_activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_address_book`
--

DROP TABLE IF EXISTS `stmd_address_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_address_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0',
  `isDefaultShipping` enum('1','0') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `isDefaultBilling` enum('1','0') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `title` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `firstName` varchar(128) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `lastName` varchar(128) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `email` varchar(128) CHARACTER SET latin1 NOT NULL,
  `address` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `alternateAddress` varchar(255) DEFAULT NULL,
  `cityId` int(11) DEFAULT NULL,
  `countyId` int(11) DEFAULT NULL,
  `stateId` int(11) DEFAULT NULL,
  `countryId` int(11) NOT NULL,
  `zipcode` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `phone` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `alternatePhone` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `deletedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_address_book`
--

LOCK TABLES `stmd_address_book` WRITE;
/*!40000 ALTER TABLE `stmd_address_book` DISABLE KEYS */;
INSERT INTO `stmd_address_book` VALUES (1,1,'0','0','Mr.','John','Doe','john.doe@gmail.com','','3454',NULL,NULL,NULL,12,'345435345','345435345345','3435345345345','2018-06-08 10:03:18',1,'0',NULL,1),(2,1,'0','0','Mr.','John','Doe','john.doe@gmail.com','Test Address','3454',NULL,NULL,NULL,20,'345435345','345435345345','3435345345345','2018-06-08 10:03:18',1,'1',NULL,1),(4,1,'0','0','Mr.','Ritu','Mitra','ritu.mitra@indusnet.co.in','Address 1','Test Adddress',NULL,NULL,185,3,'83424234','345345345','34535353535','2018-06-08 10:03:18',1,'0',NULL,NULL),(5,2,'0','0','Ms.','ANUMITA','BANERJEE','ANUMITADAS@GMAIL.COM','B. M.1ST LANE',NULL,378,NULL,5,163,'700009','9836888522','9836888522','2018-06-08 10:03:18',1,'0',NULL,NULL),(6,3,'0','0','Mr.','somnath','pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,639,NULL,2,163,'77479','713-530-1160',NULL,'2018-06-08 10:03:18',1,'0',NULL,NULL),(7,7,'1','0','Mr.','Nduka','Udeh','n.udeh@shoptomydoor.com','12011 Westbrae Parkway',NULL,378,NULL,5,163,'77031','7135301160','8323722668','2018-08-31 17:08:20',0,'0',NULL,NULL),(8,3,'0','0','Mr.','Paulami','Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,638,NULL,15,163,'67676','6767',NULL,'2018-09-12 06:50:15',0,'0',NULL,NULL),(9,13,'1','1','Mr.','Joy','Karmakar','karmakar.joy@gmail.com','SDF Building',NULL,783,NULL,90,230,'852000','9831723608',NULL,'2018-09-13 06:48:27',0,'0',NULL,NULL),(10,14,'1','1','Mr.','Arun','Roy','joy.karmakar@indusnet.co.in','Las Vegas',NULL,380,NULL,90,230,'852000','9831593608',NULL,'2018-09-13 09:43:56',0,'0',NULL,NULL),(11,1,'0','0','mr','Tathagata','sur','sur.tatha@gmail.com','10 a','roy',0,NULL,NULL,0,'BIDHAN NAGAR','08961186006',NULL,'2018-09-13 12:35:17',0,'0',NULL,NULL),(12,1,'0','0','ms','tathagata','sur','sur.tatha@gmail.com','10a','roypara',0,NULL,0,0,'700091','8961186006','8961186007','2018-09-13 12:40:58',0,'0',NULL,NULL),(13,1,'0','0','mr','af','asas','sss@gmail.com','saas','saas',0,NULL,0,0,'sss','8961186006',NULL,'2018-09-13 12:48:48',0,'0',NULL,NULL),(14,1,'1','0','mr','sss','sss','ssss@gmail.com','sss','sss',0,NULL,2,2,'70050','+91+911111111111','+91+9188888','2018-09-14 08:20:43',0,'0',NULL,NULL),(15,1,'0','1','mr','Tathagata','sur','sur.tatha@gmail.com','10 a','sss',11,NULL,2,2,'700091','+918961186006','+91sssss','2018-09-14 08:14:36',0,'0',NULL,NULL),(16,1,'0','0','mr','ff','fff','ff@gmail.com','ff','ff',0,NULL,2,2,'700050','+918961186006',NULL,'2018-09-14 09:24:25',0,'0',NULL,NULL),(17,1,'0','0','mr','Tatha','sur','sur.tathagata@gmail.com','10 a roy para','asssssssssssssssss',0,NULL,2,2,'700091','+9108961186006',NULL,'2018-09-14 09:31:48',0,'0',NULL,NULL),(18,3,'1','1','mr','Tathagata','sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j',53,NULL,4,163,'700091','+91+90+92+9208961186006','+921234567899','2018-09-17 10:28:34',0,'0',NULL,NULL),(19,18,'1','1','mr','Mithun','Sen','mithun.sen@indusnet.co.in','273, Test address',NULL,771,NULL,4,163,'73738','+921234567890',NULL,'2018-09-18 12:53:23',0,'0',NULL,NULL),(20,18,'0','0','mr','Mithun','Sen','mithun.sen@indusnet.co.in','748, saltlake city',NULL,642,NULL,7,163,'8488','+928989898989','+915555555555','2018-09-18 13:10:14',0,'0',NULL,NULL),(21,7,'0','1','Ms.','Oby','Gerald','oby.g@sclogisticsng.com','5a banana close',NULL,720,NULL,2,163,'23400','17135301160',NULL,'2018-09-18 13:51:04',0,'0',NULL,NULL);
/*!40000 ALTER TABLE `stmd_address_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_admincmspages`
--

DROP TABLE IF EXISTS `stmd_admincmspages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_admincmspages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminCmsPageTitle` varchar(100) NOT NULL,
  `adminCmsPageSlug` varchar(100) NOT NULL,
  `adminCmsPageContent` longtext NOT NULL,
  `adminCmsPageMetaTitle` varchar(255) NOT NULL,
  `adminCmsPageMetaKeyword` varchar(255) NOT NULL,
  `adminCmsPageMetaDescription` text NOT NULL,
  `uploaded_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') NOT NULL,
  `deleted` enum('0','1') NOT NULL,
  `flag` enum('safe','trash') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_admincmspages`
--

LOCK TABLES `stmd_admincmspages` WRITE;
/*!40000 ALTER TABLE `stmd_admincmspages` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_admincmspages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminmenudefaultpermission`
--

DROP TABLE IF EXISTS `stmd_adminmenudefaultpermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminmenudefaultpermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminMenuId` int(11) NOT NULL,
  `userTypeId` int(11) NOT NULL,
  `permissionView` int(1) NOT NULL DEFAULT '0',
  `permissionAdd` int(1) NOT NULL DEFAULT '0',
  `permissionEdit` int(1) NOT NULL DEFAULT '0',
  `permissionDelete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `adminMenuId` (`adminMenuId`),
  KEY `userTypeId` (`userTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=404 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminmenudefaultpermission`
--

LOCK TABLES `stmd_adminmenudefaultpermission` WRITE;
/*!40000 ALTER TABLE `stmd_adminmenudefaultpermission` DISABLE KEYS */;
INSERT INTO `stmd_adminmenudefaultpermission` VALUES (1,1,1,1,1,0,1),(2,2,1,1,1,1,1),(3,3,1,1,1,1,1),(4,4,1,0,0,0,0),(5,5,1,0,0,0,0),(6,6,1,0,0,0,0),(7,7,1,0,0,0,0),(8,8,1,0,0,0,0),(9,9,1,0,0,0,0),(10,10,1,0,0,0,0),(11,11,1,0,0,0,0),(12,12,1,0,0,0,0),(13,13,1,1,1,1,1),(14,14,1,1,1,1,1),(15,15,1,1,1,1,1),(16,16,1,1,1,1,1),(17,17,1,1,1,1,1),(18,18,1,1,1,1,1),(19,19,1,1,1,1,1),(20,20,1,1,1,1,1),(21,21,1,1,1,1,1),(22,22,1,1,1,1,1),(23,23,1,1,1,1,1),(24,24,1,1,1,1,1),(25,25,1,0,0,0,0),(26,26,1,0,0,0,0),(27,27,1,0,0,0,0),(28,28,1,0,0,0,0),(29,29,1,0,0,0,0),(30,30,1,0,0,0,0),(31,31,1,0,0,0,0),(32,32,1,0,0,0,0),(33,33,1,0,0,0,0),(34,34,1,0,0,0,0),(35,35,1,0,0,0,0),(36,36,1,0,0,0,0),(37,37,1,0,0,0,0),(38,38,1,0,0,0,0),(39,39,1,1,1,1,1),(40,40,1,0,0,0,0),(41,41,1,0,0,0,0),(42,42,1,0,0,0,0),(43,43,1,0,0,0,0),(44,44,1,0,0,0,0),(45,45,1,0,0,0,0),(46,46,1,0,0,0,0),(47,47,1,0,0,0,0),(48,48,1,0,0,0,0),(49,49,1,0,0,0,0),(50,50,1,0,0,0,0),(51,51,1,0,0,0,0),(52,52,1,0,0,0,0),(53,53,1,0,0,0,0),(54,54,1,0,0,0,0),(55,55,1,0,0,0,0),(56,56,1,0,0,0,0),(57,57,1,0,0,0,0),(58,58,1,0,0,0,0),(59,59,1,0,0,0,0),(60,60,1,0,0,0,0),(61,61,1,0,0,0,0),(62,62,1,0,0,0,0),(63,63,1,0,0,0,0),(64,64,1,0,0,0,0),(65,65,1,0,0,0,0),(66,66,1,0,0,0,0),(67,67,1,0,0,0,0),(68,68,1,0,0,0,0),(69,69,1,0,0,0,0),(70,70,1,0,0,0,0),(71,71,1,0,0,0,0),(72,72,1,0,0,0,0),(73,73,1,0,0,0,0),(74,74,1,0,0,0,0),(75,75,1,0,0,0,0),(76,76,1,0,0,0,0),(77,77,1,0,0,0,0),(78,78,1,0,0,0,0),(79,79,1,0,0,0,0),(80,80,1,0,0,0,0),(81,81,1,0,0,0,0),(82,82,1,0,0,0,0),(83,83,1,0,0,0,0),(84,84,1,0,0,0,0),(85,85,1,0,0,0,0),(86,86,1,0,0,0,0),(87,87,1,0,0,0,0),(88,88,1,0,0,0,0),(89,89,1,0,0,0,0),(90,90,1,0,0,0,0),(91,91,1,0,0,0,0),(92,92,1,0,0,0,0),(93,93,1,1,1,1,1),(94,1,2,1,1,1,1),(95,2,2,1,1,1,1),(96,3,2,1,1,1,1),(97,4,2,1,1,1,1),(98,5,2,0,0,0,0),(99,6,2,0,0,0,0),(100,7,2,0,0,0,0),(101,8,2,0,0,0,0),(102,9,2,1,1,1,1),(103,10,2,0,0,0,0),(104,11,2,1,1,1,1),(105,12,2,1,1,1,1),(106,13,2,1,1,1,1),(107,14,2,1,1,1,1),(108,15,2,1,0,0,0),(109,16,2,0,0,0,0),(110,17,2,0,0,0,0),(111,18,2,0,0,0,0),(112,19,2,0,0,0,0),(113,20,2,0,0,0,0),(114,21,2,0,0,0,0),(115,22,2,0,0,0,0),(116,23,2,0,0,0,0),(117,24,2,1,1,1,1),(118,25,2,1,1,1,1),(119,26,2,1,1,1,1),(120,27,2,0,0,0,0),(121,28,2,0,0,0,0),(122,29,2,0,0,0,0),(123,30,2,0,0,0,0),(124,31,2,0,0,0,0),(125,32,2,0,0,0,0),(126,33,2,0,0,0,0),(127,34,2,1,1,1,1),(128,35,2,1,1,0,1),(129,36,2,0,0,0,0),(130,37,2,0,0,0,0),(131,38,2,0,0,0,0),(132,39,2,0,0,0,0),(133,40,2,0,0,0,0),(134,41,2,0,0,0,0),(135,42,2,0,0,0,0),(136,43,2,0,0,0,0),(137,44,2,0,0,0,0),(138,45,2,0,0,0,0),(139,46,2,0,0,0,0),(140,47,2,0,0,0,0),(141,48,2,0,0,0,0),(142,49,2,0,0,0,0),(143,50,2,0,0,0,0),(144,51,2,0,0,0,0),(145,52,2,0,0,0,0),(146,53,2,0,0,0,0),(147,54,2,0,0,0,0),(148,55,2,0,0,0,0),(149,56,2,0,0,0,0),(150,57,2,0,0,0,0),(151,58,2,0,0,0,0),(152,59,2,0,0,0,0),(153,60,2,0,0,0,0),(154,61,2,0,0,0,0),(155,62,2,0,0,0,0),(156,63,2,0,0,0,0),(157,64,2,0,0,0,0),(158,65,2,0,0,0,0),(159,66,2,0,0,0,0),(160,67,2,0,0,0,0),(161,68,2,0,0,0,0),(162,69,2,0,0,0,0),(163,70,2,0,0,0,0),(164,71,2,0,0,0,0),(165,72,2,0,0,0,0),(166,73,2,0,0,0,0),(167,74,2,0,0,0,0),(168,75,2,0,0,0,0),(169,76,2,0,0,0,0),(170,77,2,0,0,0,0),(171,78,2,0,0,0,0),(172,79,2,0,0,0,0),(173,80,2,0,0,0,0),(174,81,2,0,0,0,0),(175,82,2,0,0,0,0),(176,83,2,0,0,0,0),(177,84,2,0,0,0,0),(178,85,2,0,0,0,0),(179,86,2,0,0,0,0),(180,87,2,0,0,0,0),(181,88,2,0,0,0,0),(182,89,2,0,0,0,0),(183,90,2,0,0,0,0),(184,91,2,0,0,0,0),(185,92,2,0,0,0,0),(186,93,2,1,1,1,1),(187,1,3,1,1,1,1),(188,2,3,1,1,1,1),(189,3,3,1,1,1,1),(190,4,3,1,1,1,1),(191,5,3,0,0,0,0),(192,6,3,0,0,0,0),(193,7,3,0,0,0,0),(194,8,3,0,0,0,0),(195,9,3,1,1,1,1),(196,10,3,1,1,1,1),(197,11,3,1,1,1,1),(198,12,3,1,1,1,1),(199,13,3,0,0,0,0),(200,14,3,0,0,0,0),(201,15,3,0,0,0,0),(202,16,3,1,1,1,1),(203,17,3,1,0,0,0),(204,18,3,0,0,0,0),(205,19,3,0,0,0,0),(206,20,3,0,0,0,0),(207,21,3,0,0,0,0),(208,22,3,0,0,0,0),(209,23,3,0,0,0,0),(210,24,3,0,0,0,0),(211,25,3,0,0,0,0),(212,26,3,0,0,0,0),(213,27,3,0,0,0,0),(214,28,3,0,0,0,0),(215,29,3,0,0,0,0),(216,30,3,0,0,0,0),(217,31,3,0,0,0,0),(218,32,3,0,0,0,0),(219,33,3,0,0,0,0),(220,34,3,1,1,1,1),(221,35,3,0,0,0,0),(222,36,3,0,0,0,0),(223,37,3,0,0,0,0),(224,38,3,0,0,0,0),(225,39,3,0,0,0,0),(226,40,3,0,0,0,0),(227,41,3,0,0,0,0),(228,42,3,0,0,0,0),(229,43,3,0,0,0,0),(230,44,3,0,0,0,0),(231,45,3,0,0,0,0),(232,46,3,0,0,0,0),(233,47,3,0,0,0,0),(234,48,3,0,0,0,0),(235,49,3,0,0,0,0),(236,50,3,0,0,0,0),(237,51,3,0,0,0,0),(238,52,3,0,0,0,0),(239,53,3,0,0,0,0),(240,54,3,0,0,0,0),(241,55,3,0,0,0,0),(242,56,3,0,0,0,0),(243,57,3,0,0,0,0),(244,58,3,0,0,0,0),(245,59,3,0,0,0,0),(246,60,3,0,0,0,0),(247,61,3,0,0,0,0),(248,62,3,0,0,0,0),(249,63,3,0,0,0,0),(250,64,3,0,0,0,0),(251,65,3,0,0,0,0),(252,66,3,0,0,0,0),(253,67,3,0,0,0,0),(254,68,3,0,0,0,0),(255,69,3,0,0,0,0),(256,70,3,0,0,0,0),(257,71,3,0,0,0,0),(258,72,3,0,0,0,0),(259,73,3,0,0,0,0),(260,74,3,0,0,0,0),(261,75,3,0,0,0,0),(262,76,3,0,0,0,0),(263,77,3,0,0,0,0),(264,78,3,0,0,0,0),(265,79,3,0,0,0,0),(266,80,3,0,0,0,0),(267,81,3,0,0,0,0),(268,82,3,0,0,0,0),(269,83,3,0,0,0,0),(270,84,3,0,0,0,0),(271,85,3,0,0,0,0),(272,86,3,0,0,0,0),(273,87,3,0,0,0,0),(274,88,3,0,0,0,0),(275,89,3,0,0,0,0),(276,90,3,1,1,1,1),(277,91,3,0,0,1,1),(278,92,3,0,0,0,0),(279,93,3,1,1,1,1),(280,1,4,1,0,0,0),(281,2,4,1,1,1,1),(282,3,4,1,1,1,1),(283,4,4,0,0,0,0),(284,5,4,0,0,0,0),(285,6,4,0,0,0,0),(286,7,4,0,0,0,0),(287,8,4,0,0,0,0),(288,9,4,1,1,1,1),(289,10,4,0,0,0,0),(290,11,4,0,0,0,0),(291,12,4,0,0,0,0),(292,13,4,1,0,0,0),(293,14,4,0,0,0,0),(294,15,4,0,0,0,0),(295,16,4,1,1,1,1),(296,17,4,0,0,0,0),(297,18,4,0,0,0,0),(298,19,4,0,0,0,0),(299,20,4,0,0,0,0),(300,21,4,0,0,0,0),(301,22,4,0,0,0,0),(302,23,4,0,0,0,0),(303,24,4,0,0,0,0),(304,25,4,0,0,0,0),(305,26,4,0,0,0,0),(306,27,4,0,0,0,0),(307,28,4,0,0,0,0),(308,29,4,0,0,0,0),(309,30,4,0,0,0,0),(310,31,4,0,0,0,0),(311,32,4,0,0,0,0),(312,33,4,0,0,0,0),(313,34,4,1,1,1,1),(314,35,4,0,0,0,0),(315,36,4,0,0,0,0),(316,37,4,0,0,0,0),(317,38,4,0,0,0,0),(318,39,4,0,0,0,0),(319,40,4,0,0,0,0),(320,41,4,1,1,1,1),(321,42,4,0,0,0,0),(322,43,4,0,0,0,0),(323,44,4,0,0,0,0),(324,45,4,0,0,0,0),(325,46,4,0,0,0,0),(326,47,4,0,0,0,0),(327,48,4,0,0,0,0),(328,49,4,0,0,0,0),(329,50,4,0,0,0,0),(330,51,4,0,0,0,0),(331,52,4,0,0,0,0),(332,53,4,0,0,0,0),(333,54,4,0,0,0,0),(334,55,4,0,0,0,0),(335,56,4,0,0,0,0),(336,57,4,0,0,0,0),(337,58,4,0,0,0,0),(338,59,4,0,0,0,0),(339,60,4,0,0,0,0),(340,61,4,0,0,0,0),(341,62,4,0,0,0,0),(342,63,4,0,0,0,0),(343,64,4,0,0,0,0),(344,65,4,0,0,0,0),(345,66,4,0,0,0,0),(346,67,4,0,0,0,0),(347,68,4,0,0,0,0),(348,69,4,0,0,0,0),(349,70,4,0,0,0,0),(350,71,4,0,0,0,0),(351,72,4,0,0,0,0),(352,73,4,0,0,0,0),(353,74,4,0,0,0,0),(354,75,4,0,0,0,0),(355,76,4,0,0,0,0),(356,77,4,0,0,0,0),(357,78,4,0,0,0,0),(358,79,4,0,0,0,0),(359,80,4,0,0,0,0),(360,81,4,0,0,0,0),(361,82,4,0,0,0,0),(362,83,4,0,0,0,0),(363,84,4,1,1,1,1),(364,85,4,0,0,0,0),(365,86,4,0,0,0,0),(366,87,4,0,0,0,0),(367,88,4,0,0,0,0),(368,89,4,0,0,0,0),(369,90,4,0,0,0,0),(370,91,4,0,0,0,0),(371,92,4,0,0,0,0),(372,93,4,0,0,0,0),(373,94,4,0,0,0,0),(374,94,2,1,1,0,0),(375,95,2,1,1,1,1),(376,96,1,1,1,1,1),(377,99,1,1,1,1,1),(378,100,1,1,1,1,1),(379,100,2,1,1,1,1),(380,100,3,1,1,1,1),(381,100,4,1,1,1,1),(382,104,1,1,1,1,1),(383,107,1,1,1,1,1),(384,108,1,1,1,1,1),(385,109,1,1,1,1,1),(386,110,1,1,1,1,1),(388,0,0,1,1,1,1),(389,0,0,1,1,1,1),(390,0,0,1,1,1,1),(391,0,0,1,1,1,1),(392,0,0,0,0,0,0),(393,0,0,1,1,1,1),(394,0,0,1,1,1,1),(395,0,0,1,1,1,1),(396,0,0,0,0,0,0),(397,0,0,1,1,1,1),(398,0,0,1,1,1,1),(399,0,0,1,1,1,1),(400,0,0,1,1,1,1),(401,0,0,1,1,1,1),(402,0,0,0,0,0,0),(403,0,0,1,1,1,1);
/*!40000 ALTER TABLE `stmd_adminmenudefaultpermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminmenumaster`
--

DROP TABLE IF EXISTS `stmd_adminmenumaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminmenumaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminMenuName` varchar(100) NOT NULL,
  `adminMenuSlug` varchar(100) NOT NULL,
  `adminMenuParentId` int(11) NOT NULL,
  `adminMenuGroupName` varchar(100) NOT NULL,
  `adminMenuOrder` varchar(100) NOT NULL,
  `adminMenuIcon` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminmenumaster`
--

LOCK TABLES `stmd_adminmenumaster` WRITE;
/*!40000 ALTER TABLE `stmd_adminmenumaster` DISABLE KEYS */;
INSERT INTO `stmd_adminmenumaster` VALUES (1,'Dashboard','dashboard',0,'Main','1','fa-dashboard','1'),(2,'Content','#',0,'Main','2','fa-desktop','1'),(3,'Settings','#',0,'Main','3','fa-gear','1'),(4,'Users','#',0,'Main','4','fa-users','1'),(5,'Shipments','#',0,'Main','5','fa-truck','1'),(6,'Orders','#',0,'Main','6','fa-shopping-bag','1'),(7,'Procurement','#',0,'Main','7','fa-pie-chart','1'),(8,'Fill & Ship','#',0,'Main','8','fa-dropbox','1'),(9,'Auto','#',0,'Main','9','fa-car','1'),(10,'Reports','#',0,'Main','10','fa-file','1'),(11,'Message Board','#',0,'Main','11','fa-envelope','1'),(12,'Tools','#',0,'Main','12','fa-wrench','1'),(13,'Home Page Content','#',2,'Content','1','fa-circle-o','1'),(14,'Banners','homepagebanner',13,'Home Page Content','1','fa-circle-o','1'),(15,'Block Content','blockcontent',13,'Home Page Content','2','fa-circle-o','1'),(16,'Site Content','#',2,'Content','2','fa-circle-o','1'),(17,'Static Pages','pagelist/staticpage',16,'Site Content','1','fa-circle-o','1'),(18,'Landing Pages','pagelist/landingpage',16,'Site Content','2','fa-circle-o','1'),(19,'Registration Banners','registrationbanner',16,'Site Content','3','fa-circle-o','1'),(20,'Email Templates','emailtemplate',16,'Site Content','4','fa-circle-o','1'),(21,'SMS Templates','smstemplate',16,'Site Content','5','fa-circle-o','1'),(22,'Split Shipment Requests','splitshipmentrequests',11,'Message Board','3','fa-circle-o','1'),(23,'Warehouse Banners','warehousebanner',16,'Site Content','6','fa-circle-o','1'),(24,'Testimonials','testimonial',2,'Content','4','fa-circle-o','1'),(25,'Site Settings','#',3,'Settings','1','fa-circle-o','1'),(26,'General Configuration','generalsettings',25,'Site Settings','1','fa-circle-o','1'),(27,'Countries','country',25,'Site Settings','2','fa-circle-o','1'),(28,'States','state',25,'Site Settings','3','fa-circle-o','1'),(29,'Cities','city',25,'Site Settings','4','fa-circle-o','1'),(30,'Destination Zones','zone',25,'Site Settings','5','fa-circle-o','1'),(31,'Locations','location',25,'Site Settings','6','fa-circle-o','1'),(32,'Currencies','currency',25,'Site Settings','7','fa-circle-o','1'),(33,'Social Media','socialmedia',25,'Site Settings','8','fa-circle-o','1'),(34,'Shipping Settings','#',3,'Settings','2','fa-circle-o','1'),(35,'General Configuration','shippingsettings',34,'Shipping Settings','1','fa-circle-o','1'),(36,'Shipping Methods','shippingmethods',34,'Shipping Settings','2','fa-circle-o','1'),(37,'Service Types','servicetype',34,'Shipping Settings','3','fa-circle-o','1'),(38,'Update Type','updatetype',34,'Shipping Settings','4','fa-circle-o','1'),(39,'Warehouse','warehouse',34,'Shipping Settings','5','fa-circle-o','1'),(40,'Site Categories','settingscategories',34,'Shipping Settings','6','fa-circle-o','1'),(41,'Site Products','settingsproducts',34,'Shipping Settings','7','fa-circle-o','1'),(42,'Destination Ports','destinationport',34,'Shipping Settings','8','fa-circle-o','1'),(43,'Shipping Charges Settings','#',3,'Settings','3','fa-circle-o','1'),(44,'Shipping Charges','shippingcharges',43,'Shipping Charges Settings','1','fa-circle-o','1'),(46,'Other Shipment Charges','othershipmentcharges',43,'Shipping Charges Settings','3','fa-circle-o','1'),(47,'Packaging Settings','packagingcharges',43,'Shipping Charges Settings','4','fa-circle-o','1'),(51,'Tax System','taxsettings',43,'Shipping Charges Settings','8','fa-circle-o','1'),(52,'Payment Settings','paymentmethod',3,'Settings','4','fa-circle-o','1'),(53,'Rewards Settings','#',3,'Settings','5','fa-circle-o','0'),(54,'Discounts','#',53,'Rewards Settings','1','fa-circle-o','0'),(55,'Coupons','#',53,'Rewards Settings','2','fa-circle-o','0'),(56,'Offers Settings','offers',3,'Settings','6','fa-circle-o','1'),(57,'Admin','#',4,'Users','1','fa-circle-o','1'),(58,'Admin Roles','adminrole',57,'Admin','1','fa-circle-o','1'),(59,'Admin Users','adminusers',57,'Admin','2','fa-circle-o','1'),(60,'Users','users',4,'Users','2','fa-circle-o','1'),(61,'Fund User E-Wallet','fundwallet',4,'Users','3','fa-circle-o','1'),(62,'Manage Shipments','shipments',5,'Shipments','1','fa-circle-o','1'),(63,'Manage Drivers','driver',5,'Shipments','2','fa-circle-o','1'),(64,'Schedule A Pick-up','schedulepickup',5,'Shipments','3','fa-circle-o','1'),(65,'Track Shipment','shipmenttracking',5,'Shipments','4','fa-circle-o','1'),(66,'Consolidated Tracking','consolidatedtracking',5,'Shipments','5','fa-circle-o','1'),(67,'Manage Delivery Companies','deliverycompany',5,'Shipments','6','fa-circle-o','1'),(68,'Manage Dispatch Companies','dispatchcompany',5,'Shipments','7','fa-circle-o','1'),(69,'Shipment Locations - Rows','warehouselocation/rows',5,'Shipments','8','fa-circle-o','1'),(70,'Shipment Locations - Zones','warehouselocation/zones',5,'Shipments','9','fa-circle-o','1'),(71,'Manage Orders','orders',6,'Orders','1','fa-circle-o','1'),(72,'E-Wallet','ewallet',6,'Orders','2','fa-circle-o','1'),(73,'Procurement Services','#',7,'Procurement','1','fa-circle-o','1'),(74,'Procurement Fees','procurementfees',7,'Procurement','2','fa-circle-o','1'),(75,'Fill & Ship Request Shipments','#',8,'Fill & Ship','1','fa-circle-o','1'),(76,'Fill & Ship Boxes','fillshipboxes',8,'Fill & Ship','2','fa-circle-o','1'),(77,'Fill & Ship Options','fillshipsettings',8,'Fill & Ship','3','fa-circle-o','1'),(78,'Make Settings','automake',9,'Auto','1','fa-circle-o','1'),(79,'Model Settings','automodel',9,'Auto','2','fa-circle-o','1'),(80,'Shipping Cost','shippingcost',9,'Auto','3','fa-circle-o','1'),(84,'Auto Shipments','#',9,'Auto','8','fa-circle-o','1'),(87,'Accounting Info','#',10,'Reports','1','fa-circle-o','1'),(88,'Store History','#',10,'Reports','2','fa-circle-o','1'),(89,'Warehouse Tracking','#',10,'Reports','3','fa-circle-o','1'),(90,'Insurance','#',10,'Reports','4','fa-circle-o','1'),(91,'Contact Us Reasons','#',11,'Message Board','1','fa-circle-o','1'),(92,'Contact Us Requests','contactus',11,'Message Board','2','fa-circle-o','1'),(93,'Database Backup / Restore','#',12,'Tools','1','fa-circle-o','1'),(94,'Fund User Point','fundpoint',4,'Users','4','fa-circle-o','1'),(95,'Manage Stores','managestores',34,'Store Settings','9','fa-circle-o','1'),(96,'Menu Management','menu',2,'Content','3','fa-circle-o','1'),(97,'Warehouse Messages','#',11,'Message Board','3','fa-circle-o','1'),(99,'Pick-up Cost ','autopickupcost',9,'Auto','4','fa-circle-o','1'),(100,'Location Types','locationtype',25,'Site Settings','6','fa-circle-o','1'),(101,'Buy a car','autopickup',84,'Auto','1','fa-circle-o','1'),(102,'Manage Auto Shipments','autoshipment',84,'Auto','2','fa-circle-o','1'),(103,'Auto Parts','autoparts',73,'Procurement','2','fa-circle-o','1'),(104,'Media Library','media',16,'Site Content','7','fa-circle-o','1'),(105,'Shop For Me','procurement',73,'Procurement','1','fa-circle-o','1'),(107,'Newsletter Management','newsletter',16,'Site Content','7','fa-circle-o','1'),(108,'FAQ','faq',16,'Site Content','8','fa-circle-o','1'),(109,'Partner','partner',16,'Site Content','9','fa-circle-o','1'),(110,'Reward Management','rewardsettings',16,'Site Content','10','fa-circle-o','1');
/*!40000 ALTER TABLE `stmd_adminmenumaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminmenumaster_copy`
--

DROP TABLE IF EXISTS `stmd_adminmenumaster_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminmenumaster_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminMenuName` varchar(100) NOT NULL,
  `adminMenuSlug` varchar(100) NOT NULL,
  `adminMenuParentId` int(11) NOT NULL,
  `adminMenuGroupName` varchar(100) NOT NULL,
  `adminMenuOrder` varchar(100) NOT NULL,
  `adminMenuIcon` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminmenumaster_copy`
--

LOCK TABLES `stmd_adminmenumaster_copy` WRITE;
/*!40000 ALTER TABLE `stmd_adminmenumaster_copy` DISABLE KEYS */;
INSERT INTO `stmd_adminmenumaster_copy` VALUES (1,'Dashboard','dashboard',0,'Main','1','fa-dashboard','1'),(2,'Content','#',0,'Main','2','fa-desktop','1'),(3,'Settings','#',0,'Main','3','fa-gear','1'),(4,'Users','#',0,'Main','4','fa-users','1'),(5,'Shipments','#',0,'Main','5','fa-truck','1'),(6,'Orders','#',0,'Main','6','fa-shopping-bag','1'),(7,'Procurement','#',0,'Main','7','fa-pie-chart','1'),(8,'Fill & Ship','#',0,'Main','8','fa-dropbox','1'),(9,'Auto','#',0,'Main','9','fa-car','1'),(10,'Reports','#',0,'Main','10','fa-file','1'),(11,'Message Board','#',0,'Main','11','fa-envelope','1'),(12,'Tools','#',0,'Main','12','fa-wrench','1'),(13,'Home Page Content','#',2,'Content','1','fa-circle-o','1'),(14,'Banners','homepagebanner',13,'Home Page Content','1','fa-circle-o','1'),(15,'Block Content','blockcontent',13,'Home Page Content','2','fa-circle-o','1'),(16,'Site Content','#',2,'Content','2','fa-circle-o','1'),(17,'Static Pages','pagelist/staticpage',16,'Site Content','1','fa-circle-o','1'),(18,'Landing Pages','pagelist/landingpage',16,'Site Content','2','fa-circle-o','1'),(19,'Registration Banners','registrationbanner',16,'Site Content','3','fa-circle-o','1'),(20,'Email Templates','emailtemplate',16,'Site Content','4','fa-circle-o','1'),(21,'SMS Templates','smstemplate',16,'Site Content','5','fa-circle-o','1'),(22,'Split Shipment Requests','splitshipmentrequests',11,'Message Board','3','fa-circle-o','1'),(23,'Warehouse Banners','warehousebanner',16,'Site Content','6','fa-circle-o','1'),(24,'Testimonials','testimonial',2,'Content','4','fa-circle-o','1'),(25,'Site Settings','#',3,'Settings','1','fa-circle-o','1'),(26,'General Configuration','generalsettings',25,'Site Settings','1','fa-circle-o','1'),(27,'Countries','country',25,'Site Settings','2','fa-circle-o','1'),(28,'States','state',25,'Site Settings','3','fa-circle-o','1'),(29,'Cities','city',25,'Site Settings','4','fa-circle-o','1'),(30,'Destination Zones','zone',25,'Site Settings','5','fa-circle-o','1'),(31,'Locations','location',25,'Site Settings','6','fa-circle-o','1'),(32,'Currencies','currency',25,'Site Settings','7','fa-circle-o','1'),(33,'Social Media','socialmedia',25,'Site Settings','8','fa-circle-o','1'),(34,'Shipping Settings','#',3,'Settings','2','fa-circle-o','1'),(35,'General Configuration','shippingsettings',34,'Shipping Settings','1','fa-circle-o','1'),(36,'Shipping Methods','shippingmethods',34,'Shipping Settings','2','fa-circle-o','1'),(37,'Service Types','servicetype',34,'Shipping Settings','3','fa-circle-o','1'),(38,'Update Type','updatetype',34,'Shipping Settings','4','fa-circle-o','1'),(39,'Warehouse','warehouse',34,'Shipping Settings','5','fa-circle-o','1'),(40,'Site Categories','settingscategories',34,'Shipping Settings','6','fa-circle-o','1'),(41,'Site Products','settingsproducts',34,'Shipping Settings','7','fa-circle-o','1'),(42,'Destination Ports','destinationport',34,'Shipping Settings','8','fa-circle-o','1'),(43,'Shipping Charges Settings','#',3,'Settings','3','fa-circle-o','1'),(44,'Shipping Charges','shippingcharges',43,'Shipping Charges Settings','1','fa-circle-o','1'),(46,'Other Shipment Charges','othershipmentcharges',43,'Shipping Charges Settings','3','fa-circle-o','1'),(47,'Packaging Settings','packagingcharges',43,'Shipping Charges Settings','4','fa-circle-o','1'),(51,'Tax System','taxsettings',43,'Shipping Charges Settings','8','fa-circle-o','1'),(52,'Payment Settings','paymentmethod',3,'Settings','4','fa-circle-o','1'),(53,'Rewards Settings','#',3,'Settings','5','fa-circle-o','0'),(54,'Discounts','#',53,'Rewards Settings','1','fa-circle-o','0'),(55,'Coupons','#',53,'Rewards Settings','2','fa-circle-o','0'),(56,'Offers Settings','offers',3,'Settings','6','fa-circle-o','1'),(57,'Admin','#',4,'Users','1','fa-circle-o','1'),(58,'Admin Roles','adminrole',57,'Admin','1','fa-circle-o','1'),(59,'Admin Users','adminusers',57,'Admin','2','fa-circle-o','1'),(60,'Users','users',4,'Users','2','fa-circle-o','1'),(61,'Fund User E-Wallet','fundwallet',4,'Users','3','fa-circle-o','1'),(62,'Manage Shipments','shipments',5,'Shipments','1','fa-circle-o','1'),(63,'Manage Drivers','driver',5,'Shipments','2','fa-circle-o','1'),(64,'Schedule A Pick-up','schedulepickup',5,'Shipments','3','fa-circle-o','1'),(65,'Track Shipment','shipmenttracking',5,'Shipments','4','fa-circle-o','1'),(66,'Consolidated Tracking','consolidatedtracking',5,'Shipments','5','fa-circle-o','1'),(67,'Manage Delivery Companies','deliverycompany',5,'Shipments','6','fa-circle-o','1'),(68,'Manage Dispatch Companies','dispatchcompany',5,'Shipments','7','fa-circle-o','1'),(69,'Shipment Locations - Rows','warehouselocation/rows',5,'Shipments','8','fa-circle-o','1'),(70,'Shipment Locations - Zones','warehouselocation/zones',5,'Shipments','9','fa-circle-o','1'),(71,'Manage Orders','orders',6,'Orders','1','fa-circle-o','1'),(72,'E-Wallet','ewallet',6,'Orders','2','fa-circle-o','1'),(73,'Procurement Services','#',7,'Procurement','1','fa-circle-o','1'),(74,'Procurement Fees','procurementfees',7,'Procurement','2','fa-circle-o','1'),(75,'Fill & Ship Request Shipments','#',8,'Fill & Ship','1','fa-circle-o','1'),(76,'Fill & Ship Boxes','fillshipboxes',8,'Fill & Ship','2','fa-circle-o','1'),(77,'Fill & Ship Options','fillshipsettings',8,'Fill & Ship','3','fa-circle-o','1'),(78,'Make Settings','automake',9,'Auto','1','fa-circle-o','1'),(79,'Model Settings','automodel',9,'Auto','2','fa-circle-o','1'),(80,'Shipping Cost','shippingcost',9,'Auto','3','fa-circle-o','1'),(81,'Shipping Lines','shippingline',9,'Auto','5','fa-circle-o','1'),(82,'Shipping & Clearing Quote','#',9,'Auto','6','fa-circle-o','1'),(83,'Car Pickup Quote','#',9,'Auto','7','fa-circle-o','0'),(84,'Auto Shipments','#',9,'Auto','8','fa-circle-o','1'),(85,'Sailing Schedule','#',9,'Auto','9','fa-circle-o','1'),(86,'Personal Items Clearing Charge','personalcharge',9,'Auto','10','fa-circle-o','1'),(87,'Accounting Info','#',10,'Reports','1','fa-circle-o','1'),(88,'Store History','#',10,'Reports','2','fa-circle-o','1'),(89,'Warehouse Tracking','#',10,'Reports','3','fa-circle-o','1'),(90,'Insurance','#',10,'Reports','4','fa-circle-o','1'),(91,'Contact Us Reasons','#',11,'Message Board','1','fa-circle-o','1'),(92,'Contact Us Requests','contactus',11,'Message Board','2','fa-circle-o','1'),(93,'Database Backup / Restore','#',12,'Tools','1','fa-circle-o','1'),(94,'Fund User Point','fundpoint',4,'Users','4','fa-circle-o','1'),(95,'Manage Stores','managestores',34,'Store Settings','9','fa-circle-o','1'),(96,'Menu Management','menu',2,'Content','3','fa-circle-o','1'),(97,'Warehouse Messages','#',11,'Message Board','3','fa-circle-o','1'),(99,'Pick-up Cost ','autopickupcost',9,'Auto','4','fa-circle-o','1'),(100,'Location Types','locationtype',25,'Site Settings','6','fa-circle-o','1'),(101,'Buy a car','autopickup',84,'Auto','1','fa-circle-o','1'),(102,'Manage Auto Shipments','autoshipment',84,'Auto','2','fa-circle-o','1'),(103,'Auto Parts','autoparts',73,'Procurement','2','fa-circle-o','1'),(104,'Media Library','media',16,'Site Content','7','fa-circle-o','1'),(105,'Shop For Me','procurement',73,'Procurement','1','fa-circle-o','1'),(106,'Other Vehicles','othervehicle',84,'Procurement','1','fa-circle-o','0'),(107,'Newsletter Management','newsletter',16,'Site Content','7','fa-circle-o','1'),(108,'FAQ','faq',16,'Site Content','8','fa-circle-o','1'),(109,'Partner','partner',16,'Site Content','9','fa-circle-o','1'),(110,'Reward Management','rewardsettings',16,'Site Content','10','fa-circle-o','1');
/*!40000 ALTER TABLE `stmd_adminmenumaster_copy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminmenuuserpermissions`
--

DROP TABLE IF EXISTS `stmd_adminmenuuserpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminmenuuserpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminMenuId` int(11) NOT NULL,
  `adminUserId` int(11) NOT NULL,
  `permissionView` int(1) NOT NULL DEFAULT '0',
  `permissionAdd` int(1) NOT NULL DEFAULT '0',
  `permissionEdit` int(1) NOT NULL DEFAULT '0',
  `permissionDelete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `stmd_adminmenuuserpermissions_ibfk_1` (`adminUserId`),
  KEY `adminMenuId` (`adminMenuId`),
  CONSTRAINT `stmd_adminmenuuserpermissions_ibfk_1` FOREIGN KEY (`adminUserId`) REFERENCES `stmd_adminusers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=775 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminmenuuserpermissions`
--

LOCK TABLES `stmd_adminmenuuserpermissions` WRITE;
/*!40000 ALTER TABLE `stmd_adminmenuuserpermissions` DISABLE KEYS */;
INSERT INTO `stmd_adminmenuuserpermissions` VALUES (1,1,2,1,1,0,1),(2,2,2,1,1,1,1),(3,3,2,1,1,1,1),(4,4,2,1,1,1,1),(5,5,2,0,0,0,0),(6,6,2,0,0,0,0),(7,7,2,0,0,0,0),(8,8,2,0,0,0,0),(9,9,2,1,1,1,1),(10,10,2,0,0,0,0),(11,11,2,1,1,1,1),(12,12,2,1,1,1,1),(13,13,2,1,1,1,1),(14,14,2,1,1,1,1),(15,15,2,1,0,0,0),(16,16,2,0,0,0,0),(17,17,2,0,0,0,0),(18,18,2,0,0,0,0),(19,19,2,0,0,0,0),(20,20,2,0,0,0,0),(21,21,2,0,0,0,0),(22,22,2,0,0,0,0),(23,23,2,0,0,0,0),(24,24,2,0,0,0,0),(25,25,2,1,1,1,1),(26,26,2,1,1,1,1),(27,27,2,0,0,0,0),(28,28,2,0,0,0,0),(29,29,2,0,0,0,0),(30,30,2,0,0,0,0),(31,31,2,0,0,0,0),(32,32,2,0,0,0,0),(33,33,2,0,0,0,0),(34,34,2,1,1,1,1),(35,35,2,1,1,0,1),(36,36,2,0,0,0,0),(37,37,2,0,0,0,0),(38,38,2,0,0,0,0),(39,39,2,0,0,0,0),(40,40,2,0,0,0,0),(41,41,2,0,0,0,0),(42,42,2,0,0,0,0),(43,43,2,0,0,0,0),(44,44,2,0,0,0,0),(45,45,2,0,0,0,0),(46,46,2,0,0,0,0),(47,47,2,0,0,0,0),(48,48,2,0,0,0,0),(49,49,2,0,0,0,0),(50,50,2,0,0,0,0),(51,51,2,0,0,0,0),(52,52,2,0,0,0,0),(53,53,2,0,0,0,0),(54,54,2,0,0,0,0),(55,55,2,0,0,0,0),(56,56,2,0,0,0,0),(57,57,2,0,0,0,0),(58,58,2,0,0,0,0),(59,59,2,0,0,0,0),(60,60,2,0,0,0,0),(61,61,2,0,0,0,0),(62,62,2,0,0,0,0),(63,63,2,0,0,0,0),(64,64,2,0,0,0,0),(65,65,2,0,0,0,0),(66,66,2,0,0,0,0),(67,67,2,0,0,0,0),(68,68,2,0,0,0,0),(69,69,2,0,0,0,0),(70,70,2,0,0,0,0),(71,71,2,0,0,0,0),(72,72,2,0,0,0,0),(73,73,2,0,0,0,0),(74,74,2,0,0,0,0),(75,75,2,0,0,0,0),(76,76,2,0,0,0,0),(77,77,2,0,0,0,0),(78,78,2,0,0,0,0),(79,79,2,0,0,0,0),(80,80,2,0,0,0,0),(81,81,2,0,0,0,0),(82,82,2,0,0,0,0),(83,83,2,0,0,0,0),(84,84,2,0,0,0,0),(85,85,2,0,0,0,0),(86,86,2,0,0,0,0),(87,87,2,0,0,0,0),(88,88,2,0,0,0,0),(89,89,2,0,0,0,0),(90,90,2,0,0,0,0),(91,91,2,0,0,0,0),(92,92,2,0,0,0,0),(93,93,2,1,1,0,0),(94,1,1,1,1,1,1),(95,2,1,1,1,1,1),(96,3,1,1,1,1,1),(97,4,1,1,1,1,1),(98,5,1,1,1,1,1),(99,6,1,1,1,1,1),(100,7,1,1,1,1,1),(101,8,1,1,1,1,1),(102,9,1,1,1,1,1),(103,10,1,1,1,1,1),(104,11,1,1,1,1,1),(105,12,1,1,1,1,1),(106,13,1,1,1,1,1),(107,14,1,1,1,1,1),(108,15,1,1,1,1,1),(109,16,1,1,1,1,1),(110,17,1,1,1,1,1),(111,18,1,1,1,1,1),(112,19,1,1,1,1,1),(113,20,1,1,1,1,1),(114,21,1,1,1,1,1),(115,22,1,1,1,1,1),(116,23,1,1,1,1,1),(117,24,1,1,1,1,1),(118,25,1,1,1,1,1),(119,26,1,1,1,1,1),(120,27,1,1,1,1,1),(121,28,1,1,1,1,1),(122,29,1,1,1,1,1),(123,30,1,1,1,1,1),(124,31,1,1,1,1,1),(125,32,1,1,1,1,1),(126,33,1,1,1,1,1),(127,34,1,1,1,1,1),(128,35,1,1,1,1,1),(129,36,1,1,1,1,1),(130,37,1,1,1,1,1),(131,38,1,1,1,1,1),(132,39,1,1,1,1,1),(133,40,1,1,1,1,1),(134,41,1,1,1,1,1),(135,42,1,1,1,1,1),(136,43,1,1,1,1,1),(137,44,1,1,1,1,1),(138,45,1,0,0,0,0),(139,46,1,1,1,1,1),(140,47,1,1,1,1,1),(141,48,1,0,0,0,0),(142,49,1,0,0,0,0),(143,50,1,0,0,0,0),(144,51,1,1,1,1,1),(145,52,1,1,1,1,1),(146,53,1,1,1,1,1),(147,54,1,1,1,1,1),(148,55,1,1,1,1,1),(149,56,1,1,1,1,1),(150,57,1,1,1,1,1),(151,58,1,1,1,1,1),(152,59,1,1,1,1,1),(153,60,1,1,1,1,1),(154,61,1,1,1,1,1),(155,62,1,1,1,1,1),(156,63,1,1,1,1,1),(157,64,1,1,1,1,1),(158,65,1,1,1,1,1),(159,66,1,1,1,1,1),(160,67,1,1,1,1,1),(161,68,1,1,1,1,1),(162,69,1,1,1,1,1),(163,70,1,1,1,1,1),(164,71,1,1,1,1,1),(165,72,1,1,1,1,1),(166,73,1,1,1,1,1),(167,74,1,1,1,1,1),(168,75,1,1,1,1,1),(169,76,1,1,1,1,1),(170,77,1,1,1,1,1),(171,78,1,1,1,1,1),(172,79,1,1,1,1,1),(173,80,1,1,1,1,1),(174,81,1,1,1,1,1),(175,82,1,1,1,1,1),(176,83,1,1,1,1,1),(177,84,1,1,1,1,1),(178,85,1,1,1,1,1),(179,86,1,1,1,1,1),(180,87,1,1,1,1,1),(181,88,1,1,1,1,1),(182,89,1,1,1,1,1),(183,90,1,1,1,1,1),(184,91,1,1,1,1,1),(185,92,1,1,1,1,1),(186,93,1,1,1,1,1),(187,1,3,1,1,1,1),(188,2,3,1,1,1,1),(189,3,3,0,0,0,0),(190,4,3,0,0,0,0),(191,5,3,0,0,0,0),(192,6,3,0,0,0,0),(193,7,3,0,0,0,0),(194,8,3,0,0,0,0),(195,9,3,0,0,0,0),(196,10,3,0,0,0,0),(197,11,3,0,0,0,0),(198,12,3,1,1,1,1),(199,13,3,1,1,1,1),(200,14,3,1,1,1,1),(201,15,3,1,0,0,0),(202,16,3,0,0,0,0),(203,17,3,0,0,0,0),(204,18,3,0,0,0,0),(205,19,3,0,0,0,0),(206,20,3,0,0,0,0),(207,21,3,0,0,0,0),(208,22,3,0,0,0,0),(209,23,3,0,0,0,0),(210,24,3,0,0,0,0),(211,25,3,1,1,1,1),(212,26,3,1,1,1,1),(213,27,3,0,0,0,0),(214,28,3,0,0,0,0),(215,29,3,0,0,0,0),(216,30,3,0,0,0,0),(217,31,3,0,0,0,0),(218,32,3,0,0,0,0),(219,33,3,0,0,0,0),(220,34,3,0,0,0,0),(221,35,3,1,1,0,1),(222,36,3,0,0,0,0),(223,37,3,0,0,0,0),(224,38,3,0,0,0,0),(225,39,3,0,0,0,0),(226,40,3,0,0,0,0),(227,41,3,0,0,0,0),(228,42,3,0,0,0,0),(229,43,3,0,0,0,0),(230,44,3,0,0,0,0),(231,45,3,0,0,0,0),(232,46,3,0,0,0,0),(233,47,3,0,0,0,0),(234,48,3,0,0,0,0),(235,49,3,0,0,0,0),(236,50,3,0,0,0,0),(237,51,3,0,0,0,0),(238,52,3,0,0,0,0),(239,53,3,0,0,0,0),(240,54,3,0,0,0,0),(241,55,3,0,0,0,0),(242,56,3,0,0,0,0),(243,57,3,0,0,0,0),(244,58,3,0,0,0,0),(245,59,3,0,0,0,0),(246,60,3,0,0,0,0),(247,61,3,0,0,0,0),(248,62,3,0,0,0,0),(249,63,3,0,0,0,0),(250,64,3,0,0,0,0),(251,65,3,0,0,0,0),(252,66,3,0,0,0,0),(253,67,3,0,0,0,0),(254,68,3,0,0,0,0),(255,69,3,0,0,0,0),(256,70,3,0,0,0,0),(257,71,3,0,0,0,0),(258,72,3,0,0,0,0),(259,73,3,0,0,0,0),(260,74,3,0,0,0,0),(261,75,3,0,0,0,0),(262,76,3,0,0,0,0),(263,77,3,0,0,0,0),(264,78,3,0,0,0,0),(265,79,3,0,0,0,0),(266,80,3,0,0,0,0),(267,81,3,0,0,0,0),(268,82,3,0,0,0,0),(269,83,3,0,0,0,0),(270,84,3,0,0,0,0),(271,85,3,0,0,0,0),(272,86,3,0,0,0,0),(273,87,3,0,0,0,0),(274,88,3,0,0,0,0),(275,89,3,0,0,0,0),(276,90,3,0,0,0,0),(277,91,3,0,0,0,0),(278,92,3,0,0,0,0),(279,93,3,1,1,0,0),(280,1,6,1,1,1,1),(281,2,6,1,1,1,1),(282,3,6,0,0,0,0),(283,4,6,0,0,0,0),(284,5,6,0,0,0,0),(285,6,6,0,0,0,0),(286,7,6,0,0,0,0),(287,8,6,0,0,0,0),(288,9,6,0,0,0,0),(289,10,6,0,0,0,0),(290,11,6,0,0,0,0),(291,12,6,1,1,1,1),(292,13,6,0,0,0,0),(293,14,6,0,0,0,0),(294,15,6,0,0,0,0),(295,16,6,0,0,0,0),(296,17,6,0,0,0,0),(297,18,6,0,0,0,0),(298,19,6,0,0,0,0),(299,20,6,0,0,0,0),(300,21,6,0,0,0,0),(301,22,6,0,0,0,0),(302,23,6,0,0,0,0),(303,24,6,0,0,0,0),(304,25,6,0,0,0,0),(305,26,6,0,0,0,0),(306,27,6,0,0,0,0),(307,28,6,0,0,0,0),(308,29,6,0,0,0,0),(309,30,6,0,0,0,0),(310,31,6,0,0,0,0),(311,32,6,0,0,0,0),(312,33,6,0,0,0,0),(313,34,6,0,0,0,0),(314,35,6,0,0,0,0),(315,36,6,0,0,0,0),(316,37,6,0,0,0,0),(317,38,6,0,0,0,0),(318,39,6,0,0,0,0),(319,40,6,0,0,0,0),(320,41,6,0,0,0,0),(321,42,6,0,0,0,0),(322,43,6,0,0,0,0),(323,44,6,0,0,0,0),(324,45,6,0,0,0,0),(325,46,6,0,0,0,0),(326,47,6,0,0,0,0),(327,48,6,0,0,0,0),(328,49,6,0,0,0,0),(329,50,6,0,0,0,0),(330,51,6,0,0,0,0),(331,52,6,0,0,0,0),(332,53,6,0,0,0,0),(333,54,6,0,0,0,0),(334,55,6,0,0,0,0),(335,56,6,0,0,0,0),(336,57,6,0,0,0,0),(337,58,6,0,0,0,0),(338,59,6,0,0,0,0),(339,60,6,0,0,0,0),(340,61,6,0,0,0,0),(341,62,6,0,0,0,0),(342,63,6,0,0,0,0),(343,64,6,0,0,0,0),(344,65,6,0,0,0,0),(345,66,6,0,0,0,0),(346,67,6,0,0,0,0),(347,68,6,0,0,0,0),(348,69,6,0,0,0,0),(349,70,6,0,0,0,0),(350,71,6,0,0,0,0),(351,72,6,0,0,0,0),(352,73,6,0,0,0,0),(353,74,6,0,0,0,0),(354,75,6,0,0,0,0),(355,76,6,0,0,0,0),(356,77,6,0,0,0,0),(357,78,6,0,0,0,0),(358,79,6,0,0,0,0),(359,80,6,0,0,0,0),(360,81,6,0,0,0,0),(361,82,6,0,0,0,0),(362,83,6,0,0,0,0),(363,84,6,0,0,0,0),(364,85,6,0,0,0,0),(365,86,6,0,0,0,0),(366,87,6,0,0,0,0),(367,88,6,0,0,0,0),(368,89,6,0,0,0,0),(369,90,6,1,1,1,1),(370,91,6,0,0,0,0),(371,92,6,0,0,0,0),(372,93,6,1,1,1,1),(373,1,7,1,1,1,1),(374,2,7,1,1,1,1),(375,3,7,0,0,0,0),(376,4,7,0,0,0,0),(377,5,7,0,0,0,0),(378,6,7,0,0,0,0),(379,7,7,0,0,0,0),(380,8,7,0,0,0,0),(381,9,7,0,0,0,0),(382,10,7,0,0,0,0),(383,11,7,0,0,0,0),(384,12,7,1,1,1,1),(385,13,7,1,1,1,1),(386,14,7,1,1,1,1),(387,15,7,1,0,0,0),(388,16,7,0,0,0,0),(389,17,7,0,0,0,0),(390,18,7,0,0,0,0),(391,19,7,0,0,0,0),(392,20,7,0,0,0,0),(393,21,7,0,0,0,0),(394,22,7,0,0,0,0),(395,23,7,0,0,0,0),(396,24,7,0,0,0,0),(397,25,7,1,1,1,1),(398,26,7,1,1,1,1),(399,27,7,0,0,0,0),(400,28,7,0,0,0,0),(401,29,7,0,0,0,0),(402,30,7,0,0,0,0),(403,31,7,0,0,0,0),(404,32,7,0,0,0,0),(405,33,7,0,0,0,0),(406,34,7,0,0,0,0),(407,35,7,1,1,0,1),(408,36,7,0,0,0,0),(409,37,7,0,0,0,0),(410,38,7,0,0,0,0),(411,39,7,0,0,0,0),(412,40,7,0,0,0,0),(413,41,7,0,0,0,0),(414,42,7,0,0,0,0),(415,43,7,0,0,0,0),(416,44,7,0,0,0,0),(417,45,7,0,0,0,0),(418,46,7,0,0,0,0),(419,47,7,0,0,0,0),(420,48,7,0,0,0,0),(421,49,7,0,0,0,0),(422,50,7,0,0,0,0),(423,51,7,0,0,0,0),(424,52,7,0,0,0,0),(425,53,7,0,0,0,0),(426,54,7,0,0,0,0),(427,55,7,0,0,0,0),(428,56,7,0,0,0,0),(429,57,7,0,0,0,0),(430,58,7,0,0,0,0),(431,59,7,0,0,0,0),(432,60,7,0,0,0,0),(433,61,7,0,0,0,0),(434,62,7,0,0,0,0),(435,63,7,0,0,0,0),(436,64,7,0,0,0,0),(437,65,7,0,0,0,0),(438,66,7,0,0,0,0),(439,67,7,0,0,0,0),(440,68,7,0,0,0,0),(441,69,7,0,0,0,0),(442,70,7,0,0,0,0),(443,71,7,0,0,0,0),(444,72,7,0,0,0,0),(445,73,7,0,0,0,0),(446,74,7,0,0,0,0),(447,75,7,0,0,0,0),(448,76,7,0,0,0,0),(449,77,7,0,0,0,0),(450,78,7,0,0,0,0),(451,79,7,0,0,0,0),(452,80,7,0,0,0,0),(453,81,7,0,0,0,0),(454,82,7,0,0,0,0),(455,83,7,0,0,0,0),(456,84,7,0,0,0,0),(457,85,7,0,0,0,0),(458,86,7,0,0,0,0),(459,87,7,0,0,0,0),(460,88,7,0,0,0,0),(461,89,7,0,0,0,0),(462,90,7,0,0,0,0),(463,91,7,0,0,0,0),(464,92,7,0,0,0,0),(465,93,7,1,1,0,0),(466,1,4,1,1,1,1),(467,2,4,1,1,1,1),(468,3,4,0,0,0,0),(469,4,4,0,0,0,0),(470,5,4,0,0,0,0),(471,6,4,0,0,0,0),(472,7,4,0,0,0,0),(473,8,4,0,0,0,0),(474,9,4,0,0,0,0),(475,10,4,0,0,0,0),(476,11,4,0,0,0,0),(477,12,4,1,1,1,1),(478,13,4,1,1,1,1),(479,14,4,1,1,1,1),(480,15,4,1,0,0,0),(481,16,4,0,0,0,0),(482,17,4,0,0,0,0),(483,18,4,0,0,0,0),(484,19,4,0,0,0,0),(485,20,4,0,0,0,0),(486,21,4,0,0,0,0),(487,22,4,0,0,0,0),(488,23,4,0,0,0,0),(489,24,4,0,0,0,0),(490,25,4,1,1,1,1),(491,26,4,1,1,1,1),(492,27,4,0,0,0,0),(493,28,4,0,0,0,0),(494,29,4,0,0,0,0),(495,30,4,0,0,0,0),(496,31,4,0,0,0,0),(497,32,4,0,0,0,0),(498,33,4,0,0,0,0),(499,34,4,0,0,0,0),(500,35,4,1,1,0,1),(501,36,4,0,0,0,0),(502,37,4,0,0,0,0),(503,38,4,0,0,0,0),(504,39,4,0,0,0,0),(505,40,4,0,0,0,0),(506,41,4,0,0,0,0),(507,42,4,0,0,0,0),(508,43,4,0,0,0,0),(509,44,4,0,0,0,0),(510,45,4,0,0,0,0),(511,46,4,0,0,0,0),(512,47,4,0,0,0,0),(513,48,4,0,0,0,0),(514,49,4,0,0,0,0),(515,50,4,0,0,0,0),(516,51,4,0,0,0,0),(517,52,4,0,0,0,0),(518,53,4,0,0,0,0),(519,54,4,0,0,0,0),(520,55,4,0,0,0,0),(521,56,4,0,0,0,0),(522,57,4,0,0,0,0),(523,58,4,0,0,0,0),(524,59,4,0,0,0,0),(525,60,4,0,0,0,0),(526,61,4,0,0,0,0),(527,62,4,0,0,0,0),(528,63,4,0,0,0,0),(529,64,4,0,0,0,0),(530,65,4,0,0,0,0),(531,66,4,0,0,0,0),(532,67,4,0,0,0,0),(533,68,4,0,0,0,0),(534,69,4,0,0,0,0),(535,70,4,0,0,0,0),(536,71,4,0,0,0,0),(537,72,4,0,0,0,0),(538,73,4,0,0,0,0),(539,74,4,0,0,0,0),(540,75,4,0,0,0,0),(541,76,4,0,0,0,0),(542,77,4,0,0,0,0),(543,78,4,0,0,0,0),(544,79,4,0,0,0,0),(545,80,4,0,0,0,0),(546,81,4,0,0,0,0),(547,82,4,0,0,0,0),(548,83,4,0,0,0,0),(549,84,4,0,0,0,0),(550,85,4,0,0,0,0),(551,86,4,0,0,0,0),(552,87,4,0,0,0,0),(553,88,4,0,0,0,0),(554,89,4,0,0,0,0),(555,90,4,0,0,0,0),(556,91,4,0,0,0,0),(557,92,4,0,0,0,0),(558,93,4,1,1,0,0),(559,1,5,1,1,1,1),(560,2,5,1,1,1,1),(561,3,5,0,0,0,0),(562,4,5,0,0,0,0),(563,5,5,0,0,0,0),(564,6,5,0,0,0,0),(565,7,5,0,0,0,0),(566,8,5,0,0,0,0),(567,9,5,0,0,0,0),(568,10,5,0,0,0,0),(569,11,5,0,0,0,0),(570,12,5,1,1,1,1),(571,13,5,1,1,1,1),(572,14,5,1,1,1,1),(573,15,5,1,0,0,0),(574,16,5,0,0,0,0),(575,17,5,0,0,0,0),(576,18,5,0,0,0,0),(577,19,5,0,0,0,0),(578,20,5,0,0,0,0),(579,21,5,0,0,0,0),(580,22,5,0,0,0,0),(581,23,5,0,0,0,0),(582,24,5,0,0,0,0),(583,25,5,1,1,1,1),(584,26,5,1,1,1,1),(585,27,5,0,0,0,0),(586,28,5,0,0,0,0),(587,29,5,0,0,0,0),(588,30,5,0,0,0,0),(589,31,5,0,0,0,0),(590,32,5,0,0,0,0),(591,33,5,0,0,0,0),(592,34,5,0,0,0,0),(593,35,5,1,1,0,1),(594,36,5,0,0,0,0),(595,37,5,0,0,0,0),(596,38,5,0,0,0,0),(597,39,5,0,0,0,0),(598,40,5,0,0,0,0),(599,41,5,0,0,0,0),(600,42,5,0,0,0,0),(601,43,5,0,0,0,0),(602,44,5,0,0,0,0),(603,45,5,0,0,0,0),(604,46,5,0,0,0,0),(605,47,5,0,0,0,0),(606,48,5,0,0,0,0),(607,49,5,0,0,0,0),(608,50,5,0,0,0,0),(609,51,5,0,0,0,0),(610,52,5,0,0,0,0),(611,53,5,0,0,0,0),(612,54,5,0,0,0,0),(613,55,5,0,0,0,0),(614,56,5,0,0,0,0),(615,57,5,0,0,0,0),(616,58,5,0,0,0,0),(617,59,5,0,0,0,0),(618,60,5,0,0,0,0),(619,61,5,0,0,0,0),(620,62,5,0,0,0,0),(621,63,5,0,0,0,0),(622,64,5,0,0,0,0),(623,65,5,0,0,0,0),(624,66,5,0,0,0,0),(625,67,5,0,0,0,0),(626,68,5,0,0,0,0),(627,69,5,0,0,0,0),(628,70,5,0,0,0,0),(629,71,5,0,0,0,0),(630,72,5,0,0,0,0),(631,73,5,0,0,0,0),(632,74,5,0,0,0,0),(633,75,5,0,0,0,0),(634,76,5,0,0,0,0),(635,77,5,0,0,0,0),(636,78,5,0,0,0,0),(637,79,5,0,0,0,0),(638,80,5,0,0,0,0),(639,81,5,0,0,0,0),(640,82,5,0,0,0,0),(641,83,5,0,0,0,0),(642,84,5,0,0,0,0),(643,85,5,0,0,0,0),(644,86,5,0,0,0,0),(645,87,5,0,0,0,0),(646,88,5,0,0,0,0),(647,89,5,0,0,0,0),(648,90,5,0,0,0,0),(649,91,5,0,0,0,0),(650,92,5,0,0,0,0),(651,93,5,1,1,0,0),(652,1,8,1,1,1,1),(653,2,8,1,1,1,1),(654,3,8,1,1,1,1),(655,4,8,0,0,0,0),(656,5,8,0,0,0,0),(657,6,8,0,0,0,0),(658,7,8,0,0,0,0),(659,8,8,0,0,0,0),(660,9,8,1,1,1,1),(661,10,8,0,0,0,0),(662,11,8,0,0,0,0),(663,12,8,0,0,0,0),(664,13,8,1,1,1,1),(665,14,8,1,1,1,1),(666,15,8,1,1,1,1),(667,16,8,0,0,0,0),(668,17,8,0,0,0,0),(669,18,8,0,0,0,0),(670,19,8,0,0,0,0),(671,20,8,0,0,0,0),(672,21,8,0,0,0,0),(673,22,8,0,0,0,0),(674,23,8,0,0,0,0),(675,24,8,0,0,0,0),(676,25,8,0,0,0,0),(677,26,8,0,0,0,0),(678,27,8,0,0,0,0),(679,28,8,0,0,0,0),(680,29,8,0,0,0,0),(681,30,8,0,0,0,0),(682,31,8,0,0,0,0),(683,32,8,0,0,0,0),(684,33,8,0,0,0,0),(685,34,8,1,1,1,1),(686,35,8,0,0,0,0),(687,36,8,0,0,0,0),(688,37,8,0,0,0,0),(689,38,8,0,0,0,0),(690,39,8,0,0,0,0),(691,40,8,0,0,0,0),(692,41,8,1,1,1,1),(693,42,8,0,0,0,0),(694,43,8,0,0,0,0),(695,44,8,0,0,0,0),(696,45,8,0,0,0,0),(697,46,8,0,0,0,0),(698,47,8,0,0,0,0),(699,48,8,0,0,0,0),(700,49,8,0,0,0,0),(701,50,8,0,0,0,0),(702,51,8,0,0,0,0),(703,52,8,0,0,0,0),(704,53,8,0,0,0,0),(705,54,8,0,0,0,0),(706,55,8,0,0,0,0),(707,56,8,0,0,0,0),(708,57,8,0,0,0,0),(709,58,8,0,0,0,0),(710,59,8,0,0,0,0),(711,60,8,0,0,0,0),(712,61,8,0,0,0,0),(713,62,8,0,0,0,0),(714,63,8,0,0,0,0),(715,64,8,0,0,0,0),(716,65,8,0,0,0,0),(717,66,8,0,0,0,0),(718,67,8,0,0,0,0),(719,68,8,0,0,0,0),(720,69,8,0,0,0,0),(721,70,8,0,0,0,0),(722,71,8,0,0,0,0),(723,72,8,0,0,0,0),(724,73,8,0,0,0,0),(725,74,8,0,0,0,0),(726,75,8,0,0,0,0),(727,76,8,0,0,0,0),(728,77,8,0,0,0,0),(729,78,8,0,0,0,0),(730,79,8,0,0,0,0),(731,80,8,0,0,0,0),(732,81,8,0,0,0,0),(733,82,8,0,0,0,0),(734,83,8,0,0,0,0),(735,84,8,1,1,1,1),(736,85,8,0,0,0,0),(737,86,8,0,0,0,0),(738,87,8,0,0,0,0),(739,88,8,0,0,0,0),(740,89,8,0,0,0,0),(741,90,8,0,0,0,0),(742,91,8,0,0,0,0),(743,92,8,0,0,0,0),(744,93,8,0,0,0,0),(745,94,1,1,1,1,1),(747,94,8,1,1,1,1),(748,95,1,1,1,1,1),(749,96,1,1,1,1,1),(750,99,1,1,1,1,1),(751,100,2,1,1,1,1),(752,100,1,1,1,1,1),(753,100,3,1,1,1,1),(754,100,6,0,0,0,0),(755,100,7,1,1,1,1),(756,100,4,1,1,1,1),(757,100,5,1,1,1,1),(758,100,8,0,0,0,0),(759,101,1,1,1,1,1),(760,102,1,1,1,1,1),(761,103,1,1,1,1,1),(762,104,1,1,1,1,1),(763,105,1,1,1,1,1),(764,106,1,1,1,1,1),(765,107,1,1,1,1,1),(766,108,1,1,1,1,1),(767,109,1,1,1,1,1),(768,110,1,1,1,1,1),(769,94,2,1,1,0,0),(770,95,2,1,1,0,0),(771,96,2,1,1,0,0),(772,97,2,1,1,0,0),(773,98,2,0,0,0,0),(774,99,2,1,1,0,0);
/*!40000 ALTER TABLE `stmd_adminmenuuserpermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminnotification`
--

DROP TABLE IF EXISTS `stmd_adminnotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminnotification` (
  `notifyId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` text NOT NULL,
  `sendUserId` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  `date` datetime NOT NULL,
  PRIMARY KEY (`notifyId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminnotification`
--

LOCK TABLES `stmd_adminnotification` WRITE;
/*!40000 ALTER TABLE `stmd_adminnotification` DISABLE KEYS */;
INSERT INTO `stmd_adminnotification` VALUES (1,56,'<p>sxsxsx</p>',1,'1','2018-05-14 10:26:17'),(2,62,'<p>xssxxsxsxsxsd</p>',1,'1','2018-05-14 10:29:34'),(3,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(4,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(5,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(6,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(7,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(8,61,'ddededwdewd\r\nwdwedewd',1,'1','2018-05-14 11:52:51'),(9,61,'ddededwdewd\r\nwdwedewd',1,'1','2018-05-14 11:52:51'),(10,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(11,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(12,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(13,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(14,61,'<p><strong>diffrentLorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(15,61,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type <s>and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elec</s>tronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and <em>more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</em></p>',1,'1','2018-05-14 11:52:51'),(16,57,'sxsaxax',1,'1','2018-05-14 13:03:03'),(17,57,'sxsaxax',1,'1','2018-05-14 13:03:17'),(18,6,'<p>Hello there!</p>',1,'1','2018-05-22 05:46:59'),(19,2,'<p>xzcxc</p>',1,'1','2018-05-22 10:43:33'),(20,8,'<p>Test</p>\r\n\r\n<p>&nbsp;</p>',1,'1','2018-05-23 13:30:00'),(21,5,'<p>Hello Sir!</p>',1,'1','2018-05-24 11:05:43'),(22,8,'<p>dvdvdfvdfv</p>',1,'1','2018-06-04 05:08:56'),(23,3,'<p>&lt;p&gt;&lt;img class=&quot;ui-widget-content&quot; src=&quot;http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889718_images-8.jpg&quot; /&gt;&lt;/p&gt;</p>',1,'1','2018-07-19 09:22:47'),(24,6,'<p>Test Email</p>',1,'1','2018-09-13 05:51:23');
/*!40000 ALTER TABLE `stmd_adminnotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminuserlogs`
--

DROP TABLE IF EXISTS `stmd_adminuserlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminuserlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminUserId` int(11) NOT NULL,
  `adminUserInTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `adminUserOutTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `adminUserId` (`adminUserId`),
  CONSTRAINT `stmd_adminuserlogs_ibfk_1` FOREIGN KEY (`adminUserId`) REFERENCES `stmd_adminusers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=443 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminuserlogs`
--

LOCK TABLES `stmd_adminuserlogs` WRITE;
/*!40000 ALTER TABLE `stmd_adminuserlogs` DISABLE KEYS */;
INSERT INTO `stmd_adminuserlogs` VALUES (1,1,'2018-05-31 13:24:01',NULL),(2,1,'2018-06-01 06:04:18',NULL),(3,1,'2018-06-01 06:42:51',NULL),(4,1,'2018-06-01 07:26:35',NULL),(5,1,'2018-06-01 11:36:52',NULL),(6,1,'2018-06-01 12:22:30',NULL),(7,1,'2018-06-01 12:31:32',NULL),(8,1,'2018-06-01 12:32:45',NULL),(9,1,'2018-06-01 17:14:29',NULL),(10,1,'2018-06-04 05:08:27',NULL),(11,1,'2018-06-04 05:17:14',NULL),(12,1,'2018-06-04 05:19:21',NULL),(13,1,'2018-06-04 06:03:41',NULL),(14,1,'2018-06-04 07:29:06',NULL),(15,1,'2018-06-04 09:46:24',NULL),(16,1,'2018-06-04 10:16:00',NULL),(17,1,'2018-06-04 10:18:17',NULL),(18,1,'2018-06-04 13:55:57',NULL),(19,1,'2018-06-05 04:50:28',NULL),(20,1,'2018-06-05 07:01:17',NULL),(21,1,'2018-06-05 07:11:33',NULL),(22,1,'2018-06-05 10:23:49',NULL),(23,1,'2018-06-05 11:35:03',NULL),(24,1,'2018-06-05 11:43:41',NULL),(25,1,'2018-06-05 13:07:38',NULL),(26,1,'2018-06-05 15:25:58',NULL),(27,1,'2018-06-05 15:37:19',NULL),(28,1,'2018-06-06 05:08:08',NULL),(29,1,'2018-06-06 05:20:59',NULL),(30,1,'2018-06-06 06:43:24',NULL),(31,1,'2018-06-06 06:44:19',NULL),(32,1,'2018-06-06 06:45:45',NULL),(33,1,'2018-06-06 06:46:55',NULL),(34,1,'2018-06-06 06:51:57',NULL),(35,1,'2018-06-06 07:07:11',NULL),(36,1,'2018-06-06 07:08:00',NULL),(37,1,'2018-06-06 07:09:03',NULL),(38,1,'2018-06-06 08:36:15',NULL),(39,1,'2018-06-06 09:16:09',NULL),(40,1,'2018-06-06 09:48:21',NULL),(41,1,'2018-06-06 10:38:05',NULL),(42,1,'2018-06-07 05:17:37',NULL),(43,1,'2018-06-07 07:23:29',NULL),(44,1,'2018-06-07 07:38:54',NULL),(45,1,'2018-06-07 07:41:21',NULL),(46,1,'2018-06-07 07:52:26',NULL),(47,1,'2018-06-07 07:53:46',NULL),(48,1,'2018-06-07 07:56:37',NULL),(49,1,'2018-06-07 08:02:40',NULL),(50,1,'2018-06-07 09:26:20',NULL),(51,1,'2018-06-07 10:07:54',NULL),(52,1,'2018-06-07 11:10:16',NULL),(53,1,'2018-06-07 12:49:00',NULL),(54,1,'2018-06-07 13:53:48',NULL),(55,1,'2018-06-07 14:06:41',NULL),(56,1,'2018-06-08 05:58:55',NULL),(57,1,'2018-06-08 06:03:19',NULL),(58,1,'2018-06-08 06:05:07',NULL),(59,1,'2018-06-08 06:06:35',NULL),(60,1,'2018-06-08 06:10:33',NULL),(61,1,'2018-06-08 06:36:22',NULL),(62,1,'2018-06-08 06:58:40',NULL),(63,1,'2018-06-08 07:51:08',NULL),(64,1,'2018-06-08 07:56:12',NULL),(65,1,'2018-06-08 08:02:01',NULL),(66,1,'2018-06-08 08:21:40',NULL),(67,1,'2018-06-08 08:24:31',NULL),(68,1,'2018-06-08 09:06:02',NULL),(69,1,'2018-06-08 09:06:31',NULL),(70,1,'2018-06-08 09:30:31',NULL),(71,1,'2018-06-08 09:34:08',NULL),(72,1,'2018-06-08 09:52:55',NULL),(73,1,'2018-06-08 10:01:50',NULL),(74,1,'2018-06-08 10:11:57',NULL),(75,1,'2018-06-08 10:50:15',NULL),(76,1,'2018-06-08 14:14:03',NULL),(77,1,'2018-06-11 05:02:12',NULL),(78,1,'2018-06-11 05:15:55',NULL),(79,1,'2018-06-11 05:33:16',NULL),(80,1,'2018-06-11 06:11:27',NULL),(81,1,'2018-06-11 06:13:45',NULL),(82,1,'2018-06-11 07:39:11',NULL),(83,1,'2018-06-11 10:23:35',NULL),(84,1,'2018-06-12 04:37:58',NULL),(85,1,'2018-06-12 05:57:33',NULL),(86,1,'2018-06-12 06:11:29',NULL),(87,1,'2018-06-12 07:59:39',NULL),(88,1,'2018-06-12 09:43:42',NULL),(89,1,'2018-06-12 11:13:32',NULL),(90,1,'2018-06-12 11:28:36',NULL),(91,1,'2018-06-12 13:07:51',NULL),(92,1,'2018-06-13 05:37:08',NULL),(93,1,'2018-06-13 06:33:42',NULL),(94,1,'2018-06-13 15:07:51',NULL),(95,1,'2018-06-14 06:11:02',NULL),(96,1,'2018-06-14 06:14:10',NULL),(97,1,'2018-06-14 07:23:10',NULL),(98,1,'2018-06-14 08:17:46',NULL),(99,1,'2018-06-14 13:02:25',NULL),(100,1,'2018-06-14 13:22:44',NULL),(101,1,'2018-06-15 08:02:44',NULL),(102,1,'2018-06-15 10:27:38',NULL),(103,1,'2018-06-15 10:29:14',NULL),(104,1,'2018-06-15 10:36:15',NULL),(105,1,'2018-06-15 15:22:04',NULL),(106,1,'2018-06-15 15:58:14',NULL),(107,1,'2018-06-18 06:20:50',NULL),(108,1,'2018-06-18 07:12:34',NULL),(109,1,'2018-06-18 08:12:28',NULL),(110,1,'2018-06-18 09:58:39',NULL),(111,1,'2018-06-18 09:59:24',NULL),(112,1,'2018-06-18 10:00:16',NULL),(113,1,'2018-06-18 12:06:05',NULL),(114,1,'2018-06-18 12:22:52',NULL),(115,1,'2018-06-19 05:37:50',NULL),(116,1,'2018-06-19 08:17:12',NULL),(117,1,'2018-06-19 09:31:03',NULL),(118,1,'2018-06-19 10:31:02',NULL),(119,1,'2018-06-19 12:19:25',NULL),(120,1,'2018-06-20 04:56:51',NULL),(121,1,'2018-06-20 05:13:55',NULL),(122,1,'2018-06-20 06:33:28',NULL),(123,1,'2018-06-20 07:25:37',NULL),(124,1,'2018-06-20 07:41:27',NULL),(125,1,'2018-06-20 08:23:47',NULL),(126,1,'2018-06-20 09:23:29',NULL),(127,1,'2018-06-20 09:42:00',NULL),(128,1,'2018-06-20 09:42:52',NULL),(129,1,'2018-06-20 10:46:01',NULL),(130,1,'2018-06-20 11:18:32',NULL),(131,1,'2018-06-20 12:07:31',NULL),(132,1,'2018-06-21 07:04:54',NULL),(133,1,'2018-06-21 10:25:44',NULL),(134,1,'2018-06-21 13:11:08',NULL),(135,1,'2018-06-21 14:15:29',NULL),(136,1,'2018-06-21 15:01:19',NULL),(137,1,'2018-06-22 05:44:19',NULL),(138,1,'2018-06-22 06:42:38',NULL),(139,1,'2018-06-22 07:30:55',NULL),(140,1,'2018-06-22 11:09:45',NULL),(141,1,'2018-06-22 11:39:22',NULL),(142,1,'2018-06-22 12:13:20',NULL),(143,1,'2018-06-22 12:59:38',NULL),(144,1,'2018-06-22 13:36:29',NULL),(145,1,'2018-06-22 14:02:49',NULL),(146,1,'2018-06-22 14:04:46',NULL),(147,1,'2018-06-22 15:14:13',NULL),(148,1,'2018-06-25 06:20:46',NULL),(149,1,'2018-06-25 07:16:08',NULL),(150,1,'2018-06-25 07:27:25',NULL),(151,1,'2018-06-25 11:42:12',NULL),(152,1,'2018-06-25 12:37:45',NULL),(153,1,'2018-06-26 05:11:08',NULL),(154,1,'2018-06-26 06:16:21',NULL),(155,1,'2018-06-26 06:22:43',NULL),(156,1,'2018-06-26 11:36:34',NULL),(157,1,'2018-06-26 11:41:39',NULL),(158,1,'2018-06-26 11:45:54',NULL),(159,1,'2018-06-26 11:46:25',NULL),(160,1,'2018-06-26 12:42:49',NULL),(161,1,'2018-06-26 12:42:58',NULL),(162,1,'2018-06-26 13:07:00',NULL),(163,1,'2018-06-26 14:21:21',NULL),(164,1,'2018-06-26 14:51:23',NULL),(165,1,'2018-06-26 15:36:34',NULL),(166,1,'2018-06-26 15:44:29',NULL),(167,1,'2018-06-27 04:34:12',NULL),(168,1,'2018-06-27 04:58:10',NULL),(169,1,'2018-06-27 04:59:29',NULL),(170,1,'2018-06-27 05:06:20',NULL),(171,1,'2018-06-27 05:21:58',NULL),(172,1,'2018-06-27 07:37:18',NULL),(173,1,'2018-06-27 07:44:06',NULL),(174,1,'2018-06-27 10:41:59',NULL),(175,1,'2018-06-27 11:16:14',NULL),(176,1,'2018-06-27 12:22:11',NULL),(177,1,'2018-06-27 12:23:47',NULL),(178,1,'2018-06-27 12:58:31',NULL),(179,1,'2018-06-28 04:25:17',NULL),(180,1,'2018-06-28 05:11:55',NULL),(181,1,'2018-06-28 05:45:56',NULL),(182,1,'2018-06-28 05:46:05',NULL),(183,1,'2018-06-28 05:48:53',NULL),(184,1,'2018-06-28 05:56:19',NULL),(185,1,'2018-06-28 06:04:32',NULL),(186,1,'2018-06-28 07:28:35',NULL),(187,1,'2018-06-28 07:55:41',NULL),(188,1,'2018-06-28 12:00:41',NULL),(189,1,'2018-06-28 13:33:17',NULL),(190,1,'2018-06-28 14:22:43',NULL),(191,1,'2018-06-29 08:35:02',NULL),(192,1,'2018-06-29 11:03:53',NULL),(193,1,'2018-07-02 04:44:10',NULL),(194,1,'2018-07-02 07:31:35',NULL),(195,1,'2018-07-02 10:08:46',NULL),(196,1,'2018-07-02 11:17:10',NULL),(197,1,'2018-07-03 05:40:57',NULL),(198,1,'2018-07-03 11:03:30',NULL),(199,1,'2018-07-03 13:24:35',NULL),(200,1,'2018-07-03 14:11:15',NULL),(201,1,'2018-07-04 07:25:53',NULL),(202,1,'2018-07-04 08:33:21',NULL),(203,1,'2018-07-04 09:07:34',NULL),(204,1,'2018-07-04 09:29:43',NULL),(205,1,'2018-07-04 10:03:40',NULL),(206,1,'2018-07-04 14:29:56',NULL),(207,1,'2018-07-05 07:37:05',NULL),(208,1,'2018-07-05 07:40:26',NULL),(209,1,'2018-07-05 10:42:12',NULL),(210,1,'2018-07-06 06:24:23',NULL),(211,1,'2018-07-06 10:08:11',NULL),(212,1,'2018-07-06 10:08:46',NULL),(213,1,'2018-07-06 10:55:09',NULL),(214,1,'2018-07-06 12:18:26',NULL),(215,1,'2018-07-06 12:37:27',NULL),(216,1,'2018-07-06 12:50:29',NULL),(217,1,'2018-07-06 12:52:49',NULL),(218,1,'2018-07-06 13:29:20',NULL),(219,1,'2018-07-06 13:33:42',NULL),(220,1,'2018-07-06 13:39:56',NULL),(221,1,'2018-07-06 14:03:57',NULL),(222,1,'2018-07-06 14:20:07',NULL),(223,1,'2018-07-06 14:25:11',NULL),(224,1,'2018-07-09 05:44:35',NULL),(225,1,'2018-07-09 05:48:46',NULL),(226,1,'2018-07-09 05:49:43',NULL),(227,1,'2018-07-09 05:52:36',NULL),(228,1,'2018-07-09 05:54:37',NULL),(229,1,'2018-07-09 05:57:42',NULL),(230,1,'2018-07-09 05:58:38',NULL),(231,1,'2018-07-09 06:09:18',NULL),(232,1,'2018-07-09 06:14:24',NULL),(233,1,'2018-07-09 06:19:23',NULL),(234,1,'2018-07-09 06:20:44',NULL),(235,1,'2018-07-09 06:21:32',NULL),(236,1,'2018-07-09 06:32:31',NULL),(237,1,'2018-07-09 06:47:24',NULL),(238,1,'2018-07-09 06:57:19',NULL),(239,1,'2018-07-09 06:58:20',NULL),(240,1,'2018-07-09 07:12:14',NULL),(241,1,'2018-07-09 07:20:07',NULL),(242,1,'2018-07-09 08:45:08',NULL),(243,1,'2018-07-09 10:15:12',NULL),(244,1,'2018-07-10 05:40:58',NULL),(245,1,'2018-07-10 06:41:13',NULL),(246,1,'2018-07-10 06:42:08',NULL),(247,1,'2018-07-10 06:55:18',NULL),(248,1,'2018-07-10 06:56:35',NULL),(249,1,'2018-07-10 13:57:40',NULL),(250,1,'2018-07-11 05:10:40',NULL),(251,1,'2018-07-11 06:44:27',NULL),(252,1,'2018-07-11 08:35:46',NULL),(253,1,'2018-07-11 09:40:41',NULL),(254,1,'2018-07-11 10:10:43',NULL),(255,1,'2018-07-11 11:11:56',NULL),(256,1,'2018-07-12 06:38:58',NULL),(257,1,'2018-07-12 13:39:30',NULL),(258,1,'2018-07-13 03:18:36',NULL),(259,1,'2018-07-13 05:55:20',NULL),(260,1,'2018-07-13 06:31:59',NULL),(261,1,'2018-07-13 06:43:36',NULL),(262,1,'2018-07-13 06:44:17',NULL),(263,1,'2018-07-13 06:47:49',NULL),(264,1,'2018-07-13 06:56:28',NULL),(265,1,'2018-07-13 07:10:34',NULL),(266,1,'2018-07-13 09:16:26',NULL),(267,1,'2018-07-13 09:23:53',NULL),(268,1,'2018-07-13 11:23:30',NULL),(269,1,'2018-07-15 13:35:14',NULL),(270,1,'2018-07-16 05:59:56',NULL),(271,1,'2018-07-16 06:45:31',NULL),(272,1,'2018-07-16 09:33:08',NULL),(273,1,'2018-07-16 09:47:42',NULL),(274,1,'2018-07-16 12:11:35',NULL),(275,1,'2018-07-17 05:34:24',NULL),(276,1,'2018-07-17 10:12:40',NULL),(277,1,'2018-07-17 12:05:39',NULL),(278,1,'2018-07-17 13:38:30',NULL),(279,1,'2018-07-18 04:38:06',NULL),(280,1,'2018-07-18 06:56:57',NULL),(281,1,'2018-07-18 13:20:56',NULL),(282,1,'2018-07-18 13:39:21',NULL),(283,1,'2018-07-19 04:40:53',NULL),(284,1,'2018-07-19 05:29:28',NULL),(285,1,'2018-07-19 06:17:24',NULL),(286,1,'2018-07-19 08:21:03',NULL),(287,1,'2018-07-19 14:56:06',NULL),(288,1,'2018-07-20 06:33:22',NULL),(289,1,'2018-07-20 07:35:05',NULL),(290,1,'2018-07-20 07:53:44',NULL),(291,1,'2018-07-20 09:09:25',NULL),(292,1,'2018-07-20 09:26:39',NULL),(293,1,'2018-07-20 10:02:08',NULL),(294,1,'2018-07-20 10:10:21',NULL),(295,1,'2018-07-20 10:43:49',NULL),(296,1,'2018-07-20 13:06:05',NULL),(297,1,'2018-07-20 14:37:01',NULL),(298,1,'2018-07-20 16:07:26',NULL),(299,1,'2018-07-21 17:05:26',NULL),(300,1,'2018-07-21 19:22:08',NULL),(301,1,'2018-07-23 06:27:20',NULL),(302,1,'2018-07-23 08:41:38',NULL),(303,1,'2018-07-23 08:50:06',NULL),(304,1,'2018-07-23 09:01:04',NULL),(305,1,'2018-07-23 11:53:38',NULL),(306,1,'2018-07-23 13:42:40',NULL),(307,1,'2018-07-23 14:20:56',NULL),(308,1,'2018-07-23 14:55:53',NULL),(309,1,'2018-07-24 05:29:23',NULL),(310,1,'2018-07-24 05:51:45',NULL),(311,1,'2018-07-24 06:00:16',NULL),(312,1,'2018-07-24 07:15:47',NULL),(313,1,'2018-07-24 09:01:06',NULL),(314,1,'2018-07-24 09:34:26',NULL),(315,1,'2018-07-24 10:05:14',NULL),(316,1,'2018-07-24 10:28:19',NULL),(317,1,'2018-07-24 11:31:00',NULL),(318,1,'2018-07-24 11:40:09',NULL),(319,1,'2018-07-24 13:50:47',NULL),(320,1,'2018-07-24 14:16:54',NULL),(321,1,'2018-07-24 15:16:30',NULL),(322,1,'2018-07-25 04:54:06',NULL),(323,1,'2018-07-25 07:34:48',NULL),(324,1,'2018-07-25 07:40:08',NULL),(325,1,'2018-07-25 07:57:40',NULL),(326,1,'2018-07-25 08:14:40',NULL),(327,1,'2018-07-25 10:32:45',NULL),(328,1,'2018-07-25 10:39:30',NULL),(329,1,'2018-07-25 10:39:33',NULL),(330,1,'2018-07-25 11:27:01',NULL),(331,1,'2018-07-26 06:10:27',NULL),(332,1,'2018-07-26 12:14:35',NULL),(333,1,'2018-07-26 13:39:44',NULL),(334,1,'2018-07-27 05:32:51',NULL),(335,1,'2018-07-27 07:11:14',NULL),(336,1,'2018-07-27 08:01:21',NULL),(337,1,'2018-07-27 08:57:13',NULL),(338,1,'2018-07-27 10:42:32',NULL),(339,1,'2018-07-27 13:02:42',NULL),(340,1,'2018-07-27 13:04:36',NULL),(341,1,'2018-07-30 06:20:23',NULL),(342,1,'2018-07-30 06:49:59',NULL),(343,1,'2018-07-30 07:17:30',NULL),(344,1,'2018-07-30 07:28:39',NULL),(345,1,'2018-07-30 07:29:39',NULL),(346,1,'2018-07-30 10:33:20',NULL),(347,1,'2018-07-30 12:18:55',NULL),(348,1,'2018-07-31 07:38:12',NULL),(349,1,'2018-07-31 07:48:22',NULL),(350,1,'2018-07-31 07:51:48',NULL),(351,1,'2018-07-31 09:54:38',NULL),(352,1,'2018-07-31 10:43:42',NULL),(353,1,'2018-07-31 11:24:10',NULL),(354,1,'2018-07-31 12:54:38',NULL),(355,1,'2018-07-31 15:06:24',NULL),(356,1,'2018-08-01 09:09:08',NULL),(357,1,'2018-08-01 11:31:26',NULL),(358,1,'2018-08-02 11:36:04',NULL),(359,1,'2018-08-02 11:38:19',NULL),(360,1,'2018-08-02 12:06:03',NULL),(361,1,'2018-08-03 05:21:57',NULL),(362,1,'2018-08-03 06:05:21',NULL),(363,1,'2018-08-03 07:19:21',NULL),(364,1,'2018-08-03 07:31:16',NULL),(365,1,'2018-08-03 10:15:12',NULL),(366,1,'2018-08-03 10:46:09',NULL),(367,1,'2018-08-06 06:57:39',NULL),(368,1,'2018-08-06 07:04:25',NULL),(369,1,'2018-08-06 10:43:22',NULL),(370,1,'2018-08-07 05:58:14',NULL),(371,1,'2018-08-07 07:37:12',NULL),(372,1,'2018-08-07 10:01:39',NULL),(373,1,'2018-08-07 13:35:45',NULL),(374,1,'2018-08-09 06:00:16',NULL),(375,1,'2018-08-09 06:34:51',NULL),(376,1,'2018-08-09 08:24:12',NULL),(377,1,'2018-08-09 09:21:00',NULL),(378,1,'2018-08-09 09:28:11',NULL),(379,1,'2018-08-09 10:11:29',NULL),(380,1,'2018-08-10 04:56:44',NULL),(381,1,'2018-08-10 08:05:53',NULL),(382,1,'2018-08-10 13:22:09',NULL),(383,1,'2018-08-14 17:18:26',NULL),(384,1,'2018-08-17 11:28:41',NULL),(385,1,'2018-08-23 08:59:33',NULL),(386,1,'2018-08-24 04:41:05',NULL),(387,1,'2018-08-24 05:43:25',NULL),(388,1,'2018-08-24 09:25:14',NULL),(389,1,'2018-08-24 12:36:38',NULL),(390,1,'2018-08-27 10:08:55',NULL),(391,1,'2018-08-29 09:52:52',NULL),(392,1,'2018-08-29 09:53:02',NULL),(393,1,'2018-08-30 09:20:25',NULL),(394,1,'2018-09-03 09:27:48',NULL),(395,1,'2018-09-03 11:48:27',NULL),(396,1,'2018-09-04 07:17:18',NULL),(397,1,'2018-09-05 04:44:13',NULL),(398,1,'2018-09-05 10:50:49',NULL),(399,1,'2018-09-05 12:46:49',NULL),(400,1,'2018-09-06 09:56:42',NULL),(401,1,'2018-09-07 10:08:53',NULL),(402,1,'2018-09-10 06:37:12',NULL),(403,1,'2018-09-10 13:34:21',NULL),(404,1,'2018-09-11 05:09:20',NULL),(405,1,'2018-09-11 05:14:56',NULL),(406,1,'2018-09-11 05:15:52',NULL),(407,1,'2018-09-11 06:00:25',NULL),(408,1,'2018-09-11 06:25:17',NULL),(409,1,'2018-09-11 08:53:20',NULL),(410,1,'2018-09-11 10:49:37',NULL),(411,1,'2018-09-11 15:44:25',NULL),(412,1,'2018-09-12 07:00:48',NULL),(413,1,'2018-09-12 07:18:34',NULL),(414,1,'2018-09-12 10:08:23',NULL),(415,1,'2018-09-12 11:53:06',NULL),(416,1,'2018-09-12 13:26:17',NULL),(417,1,'2018-09-13 04:44:39',NULL),(418,1,'2018-09-13 04:45:05',NULL),(419,1,'2018-09-13 05:04:57',NULL),(420,1,'2018-09-13 05:30:08',NULL),(421,1,'2018-09-13 06:27:37',NULL),(422,1,'2018-09-13 08:02:09',NULL),(423,1,'2018-09-13 08:31:47',NULL),(424,1,'2018-09-13 11:27:59',NULL),(425,1,'2018-09-14 08:33:45',NULL),(426,1,'2018-09-14 08:45:09',NULL),(427,1,'2018-09-14 12:38:32',NULL),(428,1,'2018-09-14 13:24:14',NULL),(429,1,'2018-09-14 14:35:50',NULL),(430,1,'2018-09-17 06:14:03',NULL),(431,1,'2018-09-17 14:04:14',NULL),(432,1,'2018-09-18 12:43:26',NULL),(433,1,'2018-09-19 06:26:03',NULL),(434,1,'2018-09-19 03:50:58',NULL),(435,1,'2018-09-20 00:00:29',NULL),(436,1,'2018-09-20 00:41:40',NULL),(437,1,'2018-09-20 04:09:09',NULL),(438,1,'2018-09-20 23:46:12',NULL),(439,1,'2018-09-24 01:22:13',NULL),(440,1,'2018-09-25 00:35:04',NULL),(441,1,'2018-09-25 23:51:25',NULL),(442,1,'2018-09-26 00:49:51',NULL);
/*!40000 ALTER TABLE `stmd_adminuserlogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminusers`
--

DROP TABLE IF EXISTS `stmd_adminusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `contactno` varchar(255) NOT NULL,
  `company` varchar(200) NOT NULL,
  `website` varchar(200) DEFAULT NULL,
  `title` varchar(6) NOT NULL,
  `userType` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `passwordLastChanged` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userType` (`userType`),
  CONSTRAINT `stmd_adminusers_ibfk_1` FOREIGN KEY (`userType`) REFERENCES `stmd_adminusertype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminusers`
--

LOCK TABLES `stmd_adminusers` WRITE;
/*!40000 ALTER TABLE `stmd_adminusers` DISABLE KEYS */;
INSERT INTO `stmd_adminusers` VALUES (1,'Ndukaq','Udeh','admin@stmd.com','$2y$10$BHSK.qc69hE2jG5MQGvAe.cnAL4i.lQt8M/dIKVHtwk6UZR8jPhZe','nsW8ccTokDkznBuAR4re0X3Ro429B40GkD5MNf2362Rc9W11nqgBYdDVWjHu','9863586955','STMD','www.shoptomydoor.com','Mr.',1,'2017-08-04 07:09:12',1,NULL,1,'2018-05-30 00:00:00',0,'1','0',NULL),(2,'Niladri','Banerjee','niladrib.banerjee@indusnet.co.in','$2y$10$BbKxvQ7xQdAVkyw1VEZTFeOx0osVqT5O9Kvi4EW/oq10nBRW4A6mu','QWkWEhCplDDvKhKuiHe5rwTm41g0RV2qqCYfnaGQbFA5cyBomZscPHzJpVFR','9830522010','INT','','',2,'2017-08-04 07:09:12',1,'2018-05-07 10:29:00',0,'2018-05-30 00:00:00',0,'1','0','2018-06-04 07:14:12'),(3,'Oboyo','Adeyki','oboyo@yahoo.ng','$2y$10$c/XIO3qJu1kpSNG4TBotSe19IueWpQO21v2i9nd39iAA7UzCa1CbC','$2y$10$TVnwmqaYpagzNKHI6NpCQ.qsGK/v7AMeR1AfpDYduVYPn4whEYrtu','7895520100','GoPro',NULL,'Mr.',2,'2018-05-11 14:48:04',1,NULL,1,NULL,NULL,'1','0',NULL),(4,'Demo12','Demo','demo@gmail.com','$2y$10$c2hsqrYIl38HqlMxvYnSF.M3zPow6qtU4FuNbvX3SeJ/q2q6nLvKS','$2y$10$ovnE.yBgKf2i7JU2hGF8D.vnQyN2ULsWEZYupmAXHvgm0qgdLHt.u','1234569870','demo',NULL,'Mr.',2,'2018-05-16 06:54:33',1,NULL,1,NULL,NULL,'1','0',NULL),(5,'John','Deo','joy.karmakar@indusnet.co.in','$2y$10$hlRdQQdFc84J7EHNkZmn2.2rAVvXpAOAZ.gjkRmZF8knbHCzZVxGq','$2y$10$aIc9JNXmb0FbOAj/w1LWuuoTwh1DYd0FNAA4gJID.ks1vfbJmYXDS','979878998','STMD',NULL,'Mr.',2,'2018-05-16 07:05:43',1,NULL,1,NULL,NULL,'1','0',NULL),(6,'Demo','WareHouseAdmin','demowarehouse@gmail.com','$2y$10$RFX1aYaO0v6020Chl0zslOpM0iHzdzcc5YiTbOHhcX7nURe5ytdfy','YUD2cL3FPdX9xMVsSqhu9dgZgJASANASEASkJ3y3Fz6Vfn8mHgMlp2g6esNQ','9863586950','demo warehouse company',NULL,'Mr.',3,'2018-05-21 08:35:25',1,NULL,NULL,NULL,NULL,'1','0',NULL),(7,'dfdf\'s1','sfsdf2','admin@stmd2.com','$2y$10$kWBlfOkE7Z1dZH1HL/4WE.Q3R0FEMWObrXUeqZF7yPxhmIQioT.zS','$2y$10$q3TZufISAOvGsyZInO/w2.umZZki1YTB7ccqBbee4QKfSADyWUZQq','67567567','sdfds',NULL,'Mr.',2,'2018-05-21 08:52:08',1,NULL,1,NULL,NULL,'1','0',NULL),(8,'Okoyo','Abudin','xyzcompany@gmail.com','$2y$10$JzGbGg8klsfElWnTvjTIGutwLybNxq3qmjwGWCMO8hyAQAKpA82sy','$2y$10$AH8DGYDHuBJCaBxZHiBeLeMpftLm0oxCPKQgfZNOIG6CWTCJs3QC2','1234567890','XYZ demo company',NULL,'Mr.',4,'2018-05-22 11:11:36',1,'2018-07-06 06:28:40',1,NULL,NULL,'1','0','2018-05-24 12:19:48');
/*!40000 ALTER TABLE `stmd_adminusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminusertype`
--

DROP TABLE IF EXISTS `stmd_adminusertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminusertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userTypeName` varchar(255) NOT NULL,
  `userTypeSlag` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminusertype`
--

LOCK TABLES `stmd_adminusertype` WRITE;
/*!40000 ALTER TABLE `stmd_adminusertype` DISABLE KEYS */;
INSERT INTO `stmd_adminusertype` VALUES (1,'Super Admin','superadmin','1'),(2,'Sub Admin','subadmin','1'),(3,'Warehouse Personnel','warehouseadmin','1'),(4,'Accountant','accountant','1');
/*!40000 ALTER TABLE `stmd_adminusertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_adminwarehousemapping`
--

DROP TABLE IF EXISTS `stmd_adminwarehousemapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_adminwarehousemapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminUserId` int(11) NOT NULL,
  `warehouseId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `adminUserId` (`adminUserId`,`warehouseId`),
  KEY `stmd_adminwarehousemapping_ibfk_2` (`warehouseId`),
  CONSTRAINT `stmd_adminwarehousemapping_ibfk_1` FOREIGN KEY (`adminUserId`) REFERENCES `stmd_adminusers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_adminwarehousemapping_ibfk_2` FOREIGN KEY (`warehouseId`) REFERENCES `stmd_warehousemaster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_adminwarehousemapping`
--

LOCK TABLES `stmd_adminwarehousemapping` WRITE;
/*!40000 ALTER TABLE `stmd_adminwarehousemapping` DISABLE KEYS */;
INSERT INTO `stmd_adminwarehousemapping` VALUES (1,1,1),(24,3,1),(25,3,2),(18,4,2),(19,4,3),(16,5,2),(17,5,3),(12,6,1),(13,6,3),(22,7,3),(23,7,9),(27,8,3);
/*!40000 ALTER TABLE `stmd_adminwarehousemapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_authentication_log`
--

DROP TABLE IF EXISTS `stmd_authentication_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_authentication_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `deviceType` enum('I','A') NOT NULL DEFAULT 'I' COMMENT 'A=Android, I=IOS',
  `deviceId` varchar(255) NOT NULL,
  `loggedDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `stmd_authentication_log_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_authentication_log`
--

LOCK TABLES `stmd_authentication_log` WRITE;
/*!40000 ALTER TABLE `stmd_authentication_log` DISABLE KEYS */;
INSERT INTO `stmd_authentication_log` VALUES (1,3,'A','8ad2081afec76c98','2018-09-14 10:44:16'),(2,18,'A','8ad2081afec76c98','2018-09-18 12:03:54');
/*!40000 ALTER TABLE `stmd_authentication_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_make`
--

DROP TABLE IF EXISTS `stmd_auto_make`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_make` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `orders` int(4) NOT NULL,
  `type` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 = Car, 1= Auto Parts, 2=Other vehicles',
  `status` enum('0','1') NOT NULL,
  `deleted` enum('0','1') NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_make`
--

LOCK TABLES `stmd_auto_make` WRITE;
/*!40000 ALTER TABLE `stmd_auto_make` DISABLE KEYS */;
INSERT INTO `stmd_auto_make` VALUES (4,'Acuraa',1,'0','0','0',1,'2018-09-13 07:12:33',NULL,NULL),(5,'Aston Martin',2,'0','0','0',1,'2018-06-26 12:30:33',NULL,NULL),(6,'Audi',0,'0','0','0',NULL,NULL,NULL,NULL),(7,'Bently',0,'0','0','0',NULL,NULL,NULL,NULL),(8,'BMW',0,'0','0','0',NULL,NULL,NULL,NULL),(9,'Bugatti',0,'0','0','0',NULL,NULL,NULL,NULL),(10,'Buick',0,'0','0','0',NULL,NULL,NULL,NULL),(11,'Cadillac',0,'0','0','0',NULL,NULL,NULL,NULL),(12,'Chevrolet',0,'0','0','0',NULL,NULL,NULL,NULL),(13,'Chrysler',10,'0','0','0',1,'2018-09-13 06:21:26',NULL,NULL),(14,'Dodge',0,'0','0','0',NULL,NULL,NULL,NULL),(15,'Fiat',0,'0','0','0',NULL,NULL,NULL,NULL),(16,'Ford',0,'0','0','0',NULL,NULL,NULL,NULL),(17,'GMC',0,'0','0','0',NULL,NULL,NULL,NULL),(18,'Honda',0,'0','0','0',NULL,NULL,NULL,NULL),(19,'Hummer',0,'0','0','0',NULL,NULL,NULL,NULL),(20,'Hyundai',0,'0','0','0',NULL,NULL,NULL,NULL),(21,'Infiniti',0,'0','0','0',NULL,NULL,NULL,NULL),(22,'International',0,'0','0','0',NULL,NULL,NULL,NULL),(23,'Isuzu',0,'0','0','0',NULL,NULL,NULL,NULL),(24,'Jaguar',0,'0','0','0',NULL,NULL,NULL,NULL),(25,'Jeep',0,'0','0','0',NULL,NULL,NULL,NULL),(26,'Kia',0,'0','0','0',NULL,NULL,NULL,NULL),(27,'Lamborghini',0,'0','0','0',NULL,NULL,NULL,NULL),(28,'Land Rover',0,'0','0','0',NULL,NULL,NULL,NULL),(29,'Lexus',0,'0','0','0',NULL,NULL,NULL,NULL),(30,'Lincoln',0,'0','0','0',NULL,NULL,NULL,NULL),(31,'Maserati',0,'0','0','0',NULL,NULL,NULL,NULL),(32,'Maybach',0,'0','0','0',NULL,NULL,NULL,NULL),(33,'Mazda',0,'0','0','0',NULL,NULL,NULL,NULL),(34,'Mclaren',0,'0','0','0',NULL,NULL,NULL,NULL),(35,'Mercedes-Benz',0,'0','0','0',NULL,NULL,NULL,NULL),(36,'Mercury',0,'0','0','0',NULL,NULL,NULL,NULL),(37,'Mini',0,'0','0','0',NULL,NULL,NULL,NULL),(38,'Mitsubishi',0,'0','0','0',NULL,NULL,NULL,NULL),(39,'Nissan',0,'0','0','0',NULL,NULL,NULL,NULL),(40,'Pontiac',0,'0','0','0',NULL,NULL,NULL,NULL),(41,'Porsche',0,'0','0','0',NULL,NULL,NULL,NULL),(42,'Rolls-Royce',0,'0','0','0',NULL,NULL,NULL,NULL),(43,'Saab',0,'0','0','0',NULL,NULL,NULL,NULL),(44,'Saturn',0,'0','0','0',NULL,NULL,NULL,NULL),(45,'Scion',0,'0','0','0',NULL,NULL,NULL,NULL),(46,'Subaru',0,'0','0','0',NULL,NULL,NULL,NULL),(47,'Suzuki',0,'0','0','0',NULL,NULL,NULL,NULL),(48,'Toyota',0,'0','0','0',NULL,NULL,NULL,NULL),(49,'Volkswagen',0,'0','0','0',NULL,NULL,NULL,NULL),(50,'Volvo',0,'0','0','0',NULL,NULL,NULL,NULL),(51,'CCS',3,'0','0','1',1,'2018-06-08 18:15:39',1,'2018-06-08 18:17:34'),(52,'HH1',12,'0','0','1',1,'2018-06-11 05:03:16',1,'2018-06-11 05:04:38'),(53,'Aston Martin Parts',0,'1','1','0',NULL,NULL,NULL,NULL),(54,'Audi Parts',0,'1','1','0',NULL,NULL,NULL,NULL),(55,'BMW Parts',0,'1','1','0',NULL,NULL,NULL,NULL),(56,'Lexus',0,'2','0','0',NULL,NULL,NULL,NULL),(57,'Lincoln',0,'2','0','0',NULL,NULL,NULL,NULL),(58,'Maserati',0,'2','0','0',NULL,NULL,NULL,NULL),(59,'Maybach',0,'2','0','0',NULL,NULL,NULL,NULL),(60,'Mazda',0,'2','0','0',NULL,NULL,NULL,NULL),(61,'Mclaren',0,'2','0','0',NULL,NULL,NULL,NULL),(62,'Mercedes-Benz',0,'2','0','0',NULL,NULL,NULL,NULL),(63,'Mercury',0,'2','0','0',NULL,NULL,NULL,NULL),(64,'Mini',0,'2','0','0',NULL,NULL,NULL,NULL),(65,'Mitsubishi',0,'2','0','0',NULL,NULL,NULL,NULL),(66,'Nissan',0,'2','0','0',NULL,NULL,NULL,NULL),(67,'Pontiac',0,'2','0','0',NULL,NULL,NULL,NULL),(68,'Porsche',0,'2','0','0',NULL,NULL,NULL,NULL),(69,'Rolls-Royce',0,'2','0','0',NULL,NULL,NULL,NULL),(70,'Saab',0,'2','0','0',NULL,NULL,NULL,NULL),(71,'Isuzu demo',0,'2','0','1',1,'2018-09-12 10:14:40',1,'2018-09-12 10:15:09'),(72,'Test',11,'0','0','0',NULL,NULL,NULL,NULL),(73,'Tests',12,'0','0','0',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_auto_make` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_model`
--

DROP TABLE IF EXISTS `stmd_auto_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `makeId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `insurance` decimal(12,2) NOT NULL,
  `makeType` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 = Car, 1= Auto Parts, 2=Other vehicles',
  `deleted` enum('0','1') NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_make` (`makeId`),
  CONSTRAINT `fk_make` FOREIGN KEY (`makeId`) REFERENCES `stmd_auto_make` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_model`
--

LOCK TABLES `stmd_auto_model` WRITE;
/*!40000 ALTER TABLE `stmd_auto_model` DISABLE KEYS */;
INSERT INTO `stmd_auto_model` VALUES (1,48,'Rav 4',1.00,'0','0',NULL,NULL,NULL,NULL),(2,4,'CL',1.00,'0','0',NULL,NULL,NULL,NULL),(3,4,'ILX',1.00,'0','0',NULL,NULL,NULL,NULL),(4,4,'MDX',1.00,'0','0',NULL,NULL,NULL,NULL),(5,4,'NSX',1.00,'0','0',NULL,NULL,NULL,NULL),(6,4,'RDX',1.00,'0','0',NULL,NULL,NULL,NULL),(7,4,'RL',1.00,'0','0',NULL,NULL,NULL,NULL),(8,4,'RLX',1.00,'0','0',NULL,NULL,NULL,NULL),(9,4,'RSX',1.00,'0','0',NULL,NULL,NULL,NULL),(10,4,'TL',1.00,'0','0',NULL,NULL,NULL,NULL),(11,4,'TLX',1.00,'0','0',NULL,NULL,NULL,NULL),(12,4,'TSX',1.00,'0','0',NULL,NULL,NULL,NULL),(13,4,'ZDX',1.00,'0','0',NULL,NULL,NULL,NULL),(14,21,'EX35',1.00,'0','0',NULL,NULL,NULL,NULL),(15,21,'EX37',1.00,'0','0',NULL,NULL,NULL,NULL),(16,21,'FX35',1.00,'0','0',NULL,NULL,NULL,NULL),(17,21,'FX45',1.00,'0','0',NULL,NULL,NULL,NULL),(18,21,'FX50',1.00,'0','0',NULL,NULL,NULL,NULL),(19,21,'G35',1.00,'0','0',NULL,NULL,NULL,NULL),(20,21,'QX56',1.00,'0','0',NULL,NULL,NULL,NULL),(21,6,'A3',1.00,'0','0',NULL,NULL,NULL,NULL),(22,6,'A4',1.00,'0','0',NULL,NULL,NULL,NULL),(23,6,'A5',1.00,'0','0',NULL,NULL,NULL,NULL),(24,6,'A6',1.00,'0','0',NULL,NULL,NULL,NULL),(25,6,'A7',1.00,'0','0',NULL,NULL,NULL,NULL),(26,6,'A8',1.00,'0','0',NULL,NULL,NULL,NULL),(27,6,'Q3',1.00,'0','0',NULL,NULL,NULL,NULL),(28,6,'Q5',1.00,'0','0',NULL,NULL,NULL,NULL),(29,6,'Q7',1.00,'0','0',NULL,NULL,NULL,NULL),(30,6,'R8',1.00,'0','0',NULL,NULL,NULL,NULL),(31,6,'RS 4',1.00,'0','0',NULL,NULL,NULL,NULL),(32,6,'RS 5',1.00,'0','0',NULL,NULL,NULL,NULL),(33,6,'RS 6',1.00,'0','0',NULL,NULL,NULL,NULL),(34,6,'RS 7',1.00,'0','0',NULL,NULL,NULL,NULL),(35,6,'S3',1.00,'0','0',NULL,NULL,NULL,NULL),(36,6,'S4',1.00,'0','0',NULL,NULL,NULL,NULL),(37,6,'S5',1.00,'0','0',NULL,NULL,NULL,NULL),(38,6,'S6',1.00,'0','0',NULL,NULL,NULL,NULL),(39,6,'S7',1.00,'0','0',NULL,NULL,NULL,NULL),(40,6,'S8',1.00,'0','0',NULL,NULL,NULL,NULL),(41,8,'1 Series',1.00,'0','0',NULL,NULL,NULL,NULL),(42,8,'2 Series',1.00,'0','0',NULL,NULL,NULL,NULL),(43,8,'3 Series',1.00,'0','0',NULL,NULL,NULL,NULL),(44,8,'4 Series',1.00,'0','0',NULL,NULL,NULL,NULL),(45,8,'5 Series',1.00,'0','0',NULL,NULL,NULL,NULL),(46,8,'6 Series',1.00,'0','0',NULL,NULL,NULL,NULL),(47,8,'7 Series',1.00,'0','0',NULL,NULL,NULL,NULL),(48,8,'M Series',1.00,'0','0',NULL,NULL,NULL,NULL),(49,8,'X Series',1.00,'0','0',NULL,NULL,NULL,NULL),(50,8,'Z Series',1.00,'0','0',NULL,NULL,NULL,NULL),(51,48,'4Runner',1.00,'0','0',NULL,NULL,NULL,NULL),(52,48,'Avalon',1.00,'0','0',NULL,NULL,NULL,NULL),(53,48,'Camry',1.00,'0','0',NULL,NULL,NULL,NULL),(54,48,'Corolla',1.00,'0','0',NULL,NULL,NULL,NULL),(55,48,'FJ Cruiser',1.00,'0','0',NULL,NULL,NULL,NULL),(56,48,'Highlander',1.00,'0','0',NULL,NULL,NULL,NULL),(57,48,'Land Cruiser',1.00,'0','0',NULL,NULL,NULL,NULL),(58,48,'Matrix',1.00,'0','0',NULL,NULL,NULL,NULL),(59,48,'Prius',1.00,'0','0',NULL,NULL,NULL,NULL),(60,48,'Sequoia',1.00,'0','0',NULL,NULL,NULL,NULL),(61,48,'Sienna',1.00,'0','0',NULL,NULL,NULL,NULL),(62,48,'Tacoma',1.00,'0','0',NULL,NULL,NULL,NULL),(63,48,'Tundra',1.00,'0','0',NULL,NULL,NULL,NULL),(64,48,'Venza',1.00,'0','0',NULL,NULL,NULL,NULL),(65,48,'Yaris',1.00,'0','0',NULL,NULL,NULL,NULL),(66,18,'Accord',1.00,'0','0',NULL,NULL,NULL,NULL),(67,18,'Accord Crosstour',1.00,'0','0',NULL,NULL,NULL,NULL),(68,18,'Civic',1.00,'0','0',NULL,NULL,NULL,NULL),(69,18,'Element',1.00,'0','0',NULL,NULL,NULL,NULL),(70,18,'Odyssey',1.00,'0','0',NULL,NULL,NULL,NULL),(71,18,'Passport',1.00,'0','0',NULL,NULL,NULL,NULL),(72,18,'Pilot',1.00,'0','0',NULL,NULL,NULL,NULL),(73,18,'Ridgeline',1.00,'0','0',NULL,NULL,NULL,NULL),(74,10,'Enclave',1.00,'0','0',NULL,NULL,NULL,NULL),(75,10,'Lacrosse',1.00,'0','0',NULL,NULL,NULL,NULL),(76,16,'Crown Victoria',1.00,'0','0',NULL,NULL,NULL,NULL),(77,16,'E150 - Econoline',1.00,'0','0',NULL,NULL,NULL,NULL),(78,16,'E250 - Econoline',1.00,'0','0',NULL,NULL,NULL,NULL),(79,16,'E350 - Econoline',1.00,'0','0',NULL,NULL,NULL,NULL),(80,16,'Edge',1.00,'0','0',NULL,NULL,NULL,NULL),(81,16,'Escape',1.00,'0','0',NULL,NULL,NULL,NULL),(82,16,'Escort',1.00,'0','0',NULL,NULL,NULL,NULL),(83,16,'Excursion',1.00,'0','0',NULL,NULL,NULL,NULL),(84,16,'Expedition',1.00,'0','0',NULL,NULL,NULL,NULL),(85,16,'Explorer',1.00,'0','0',NULL,NULL,NULL,NULL),(86,16,'F150 Truck',1.00,'0','0',NULL,NULL,NULL,NULL),(87,16,'F250 Truck',1.00,'0','0',NULL,NULL,NULL,NULL),(88,16,'F350 Truck',1.00,'0','0',NULL,NULL,NULL,NULL),(89,16,'F450 Truck',1.00,'0','0',NULL,NULL,NULL,NULL),(90,16,'Focus',1.00,'0','0',NULL,NULL,NULL,NULL),(91,16,'Fusion',1.00,'0','0',NULL,NULL,NULL,NULL),(92,16,'Mustang',1.00,'0','0',NULL,NULL,NULL,NULL),(93,16,'Ranger',1.00,'0','0',NULL,NULL,NULL,NULL),(94,16,'Taurus',1.00,'0','0',NULL,NULL,NULL,NULL),(95,16,'Transit Connect',1.00,'0','0',NULL,NULL,NULL,NULL),(96,16,'Transit 150',1.00,'0','0',NULL,NULL,NULL,NULL),(97,16,'Transit 250',1.00,'0','0',NULL,NULL,NULL,NULL),(98,16,'Transit 350',1.00,'0','0',NULL,NULL,NULL,NULL),(99,28,'Discovery',1.00,'0','0',NULL,NULL,NULL,NULL),(100,28,'Freelander',1.00,'0','0',NULL,NULL,NULL,NULL),(101,28,'LR2',1.00,'0','0',NULL,NULL,NULL,NULL),(102,28,'LR3',1.00,'0','0',NULL,NULL,NULL,NULL),(103,28,'LR4',1.00,'0','0',NULL,NULL,NULL,NULL),(104,28,'Range Rover',1.00,'0','0',NULL,NULL,NULL,NULL),(105,28,'Range Rover Evoque',1.00,'0','0',NULL,NULL,NULL,NULL),(106,29,'ES',1.00,'0','0',NULL,NULL,NULL,NULL),(107,29,'GS',1.00,'0','0',NULL,NULL,NULL,NULL),(108,29,'GX 460',1.00,'0','0',NULL,NULL,NULL,NULL),(109,29,'GX 470',1.00,'0','0',NULL,NULL,NULL,NULL),(110,29,'IS',1.00,'0','0',NULL,NULL,NULL,NULL),(111,29,'LS',1.00,'0','0',NULL,NULL,NULL,NULL),(112,29,'LX 470',1.00,'0','0',NULL,NULL,NULL,NULL),(113,29,'LX 570',1.00,'0','0',NULL,NULL,NULL,NULL),(114,29,'RX',1.00,'0','0',NULL,NULL,NULL,NULL),(115,29,'SC',1.00,'0','0',NULL,NULL,NULL,NULL),(116,46,'Forester',1.00,'0','0',NULL,NULL,NULL,NULL),(117,9,'Demo Model',12.10,'0','1',1,'2018-06-12 12:58:34',1,'2018-06-12 13:17:14'),(118,9,'DD12',21.00,'0','1',1,'2018-06-12 13:22:42',1,'2018-06-12 13:22:49'),(119,19,'SQ1',11.00,'0','1',1,'2018-06-12 08:04:10',1,'2018-06-12 08:04:20'),(120,19,'demo1',12.00,'0','1',1,'2018-06-26 12:31:33',1,'2018-06-26 12:31:37'),(121,53,'Brake Pad Set',0.00,'1','0',NULL,NULL,NULL,NULL),(122,53,'Oxygen Sensor',0.00,'1','0',NULL,NULL,NULL,NULL),(123,53,'Power Steering Rack',0.00,'1','0',NULL,NULL,NULL,NULL),(124,53,'Aston Martin DB7 Oxygen Sensor',0.00,'1','0',NULL,NULL,NULL,NULL),(125,54,'Audi 100 A/C Compressor',0.00,'1','0',NULL,NULL,NULL,NULL),(126,54,'Audi 100 Catalytic Converter',0.00,'1','0',NULL,NULL,NULL,NULL),(127,55,'BMW 325 Drive Axle',0.00,'1','0',NULL,NULL,NULL,NULL),(128,55,'BMW 518 Power Steering Gear Box',0.00,'1','0',NULL,NULL,NULL,NULL),(129,53,'ES610 Tyre',101.00,'0','0',1,'2018-09-12 10:17:19',NULL,NULL),(130,4,'Model1',5.00,'0','0',1,'2018-09-13 07:13:49',NULL,NULL),(131,73,'Model22',10.00,'0','0',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_auto_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_personal_items_charges`
--

DROP TABLE IF EXISTS `stmd_auto_personal_items_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_personal_items_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelId` int(11) NOT NULL,
  `charge` decimal(12,2) NOT NULL DEFAULT '0.00',
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stmd_auto_personal_items_charges_ibfk_1` (`modelId`),
  CONSTRAINT `stmd_auto_personal_items_charges_ibfk_1` FOREIGN KEY (`modelId`) REFERENCES `stmd_auto_model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_personal_items_charges`
--

LOCK TABLES `stmd_auto_personal_items_charges` WRITE;
/*!40000 ALTER TABLE `stmd_auto_personal_items_charges` DISABLE KEYS */;
INSERT INTO `stmd_auto_personal_items_charges` VALUES (1,2,2300.00,'0',1,'2018-06-29 08:38:29',1,'2018-06-29 08:43:17',NULL,NULL),(2,129,14.00,'0',1,'2018-09-12 10:59:57',1,'2018-09-12 11:00:18',NULL,NULL);
/*!40000 ALTER TABLE `stmd_auto_personal_items_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_pickup_requests`
--

DROP TABLE IF EXISTS `stmd_auto_pickup_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_pickup_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `websiteId` int(11) NOT NULL,
  `makeId` int(11) NOT NULL,
  `modelId` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `carPurchaseUrl` varchar(255) NOT NULL,
  `carPrice` decimal(12,2) NOT NULL,
  `stateId` int(11) NOT NULL,
  `cityId` int(11) NOT NULL,
  `milage` varchar(255) DEFAULT NULL,
  `vinNumber` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `carImage` varchar(255) DEFAULT NULL,
  `processingFee` decimal(12,2) DEFAULT NULL,
  `pickupCost` decimal(12,2) DEFAULT NULL,
  `shippingCost` decimal(12,2) DEFAULT NULL,
  `totalCost` decimal(12,2) DEFAULT NULL,
  `postedOn` datetime NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=Submitted,1=Picked Up',
  `paymentStatus` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=Unpaid,1=Paid',
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `websiteId` (`websiteId`),
  KEY `makeId` (`makeId`),
  KEY `modelId` (`modelId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_pickup_requests`
--

LOCK TABLES `stmd_auto_pickup_requests` WRITE;
/*!40000 ALTER TABLE `stmd_auto_pickup_requests` DISABLE KEYS */;
INSERT INTO `stmd_auto_pickup_requests` VALUES (1,3,1,48,1,2015,'https://www.carwale.com/toyota-cars/yaris/colours/silver-metallic-6269/',2000.00,2,6,'17.1 kmpl','J MT\n','Silver Metallic','quarter.png',0.00,0.00,0.00,0.00,'2018-07-26 00:00:00','1','0',1,'2018-07-02 13:07:44');
/*!40000 ALTER TABLE `stmd_auto_pickup_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_pickupcost`
--

DROP TABLE IF EXISTS `stmd_auto_pickupcost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_pickupcost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cityId` int(11) NOT NULL,
  `warehouseId` int(11) NOT NULL,
  `pickupCost` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_fk` (`cityId`),
  KEY `warehouse_fk` (`warehouseId`),
  CONSTRAINT `city_fk` FOREIGN KEY (`cityId`) REFERENCES `stmd_cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `warehouse_fk` FOREIGN KEY (`warehouseId`) REFERENCES `stmd_warehousemaster` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_pickupcost`
--

LOCK TABLES `stmd_auto_pickupcost` WRITE;
/*!40000 ALTER TABLE `stmd_auto_pickupcost` DISABLE KEYS */;
INSERT INTO `stmd_auto_pickupcost` VALUES (2,228,1,15.55),(3,244,1,15.55),(4,277,1,15.55),(5,82,1,15.55),(6,375,1,15.55),(7,154,1,15.55),(8,584,1,991.00),(9,170,1,991.00),(10,660,1,15.55),(11,188,1,15.55),(12,204,1,15.55),(13,220,1,34.00),(14,236,1,15.55),(15,252,1,15.55),(16,268,1,15.55),(17,74,1,15.55),(18,285,1,15.55),(19,90,1,49.99),(20,314,1,15.55),(21,106,1,15.55),(22,330,1,15.55),(23,122,1,15.55),(24,346,1,15.55),(25,138,1,15.55),(26,372,1,15.55),(27,151,1,15.55),(28,580,1,15.55),(29,167,1,991.00),(30,627,1,15.55),(31,183,1,991.00),(32,201,1,15.55),(33,217,1,34.00),(34,233,1,15.55),(35,249,1,15.55),(36,265,1,15.55),(37,71,1,15.55),(38,282,1,15.55),(39,87,1,49.99),(40,311,1,15.55),(41,103,1,49.99),(42,327,1,15.55),(43,119,1,15.55),(44,343,1,15.55),(45,135,1,15.55),(46,148,1,15.55),(47,575,1,49.99),(48,164,1,15.55),(49,621,1,15.55),(50,180,1,991.00),(51,198,1,15.55),(52,214,1,34.00),(53,230,1,15.55),(54,246,1,15.55),(55,262,1,15.55),(56,68,1,15.55),(57,279,1,15.55),(58,84,1,15.55),(59,308,1,15.55),(60,100,1,49.99),(61,324,1,15.55),(62,116,1,15.55),(63,340,1,15.55),(64,132,1,15.55),(65,369,1,15.55),(66,568,1,34.00),(67,161,1,15.55),(68,614,1,49.99),(69,177,1,991.00),(70,781,1,49.99),(71,195,1,15.55),(72,211,1,34.00),(73,227,1,34.00),(74,243,1,15.55),(75,259,1,15.55),(76,65,1,15.55),(77,276,1,15.55),(78,81,1,15.55),(79,292,1,15.55),(80,97,1,49.99),(81,321,1,15.55),(82,113,1,15.55),(83,337,1,15.55),(84,129,1,15.55),(85,365,1,15.55),(86,145,1,15.55),(87,563,1,15.55),(88,158,1,15.55),(89,590,1,34.00),(90,174,1,991.00),(91,667,1,15.55),(92,192,1,15.55),(93,208,1,15.55),(94,224,1,34.00),(95,240,1,15.55),(96,256,1,15.55),(97,13,1,49.99),(98,273,1,15.55),(99,78,1,15.55),(100,289,1,15.55),(101,94,1,49.99),(102,318,1,15.55),(103,110,1,15.55),(104,334,1,15.55),(105,126,1,15.55),(106,358,1,991.00),(107,142,1,15.55),(108,376,1,15.55),(109,155,1,15.55),(110,585,1,34.00),(111,171,1,991.00),(112,663,1,49.99),(113,189,1,49.99),(114,205,1,15.55),(115,221,1,34.00),(116,237,1,15.55),(117,253,1,15.55),(118,269,1,15.55),(119,75,1,15.55),(120,286,1,15.55),(121,91,1,49.99),(122,315,1,15.55),(123,107,1,15.55),(124,331,1,15.55),(125,123,1,15.55),(126,347,1,15.55),(127,139,1,15.55),(128,373,1,15.55),(129,152,1,15.55),(130,581,1,15.55),(131,168,1,991.00),(132,633,1,15.55),(133,184,1,991.00),(134,202,1,15.55),(135,218,1,34.00),(136,234,1,15.55),(137,250,1,15.55),(138,266,1,15.55),(139,72,1,15.55),(140,283,1,15.55),(141,88,1,49.99),(142,312,1,15.55),(143,104,1,15.55),(144,328,1,15.55),(145,120,1,15.55),(146,344,1,15.55),(147,136,1,15.55),(148,149,1,15.55),(149,578,1,15.55),(150,165,1,991.00),(151,622,1,15.55),(152,181,1,991.00),(153,199,1,15.55),(154,215,1,34.00),(155,231,1,15.55),(156,247,1,15.55),(157,263,1,15.55),(158,69,1,15.55),(159,280,1,15.55),(160,85,1,15.55),(161,309,1,15.55),(162,101,1,49.99),(163,325,1,15.55),(164,117,1,15.55),(165,341,1,15.55),(166,133,1,15.55),(167,370,1,34.00),(168,573,1,49.99),(169,162,1,15.55),(170,619,1,15.55),(171,178,1,991.00),(172,196,1,15.55),(173,212,1,34.00),(174,260,1,15.55),(175,66,1,15.55),(176,303,1,15.55),(177,98,1,49.99),(178,322,1,15.55),(179,114,1,15.55),(180,338,1,15.55),(181,130,1,15.55),(182,367,1,15.55),(183,146,1,15.55),(184,565,1,15.55),(185,159,1,15.55),(186,591,1,49.99),(187,175,1,991.00),(188,777,1,15.55),(189,193,1,15.55),(190,209,1,34.00),(191,225,1,34.00),(192,241,1,15.55),(193,257,1,15.55),(194,62,1,16.99),(195,274,1,15.55),(196,79,1,15.55),(197,290,1,15.55),(198,95,1,49.99),(199,319,1,15.55),(200,111,1,15.55),(201,335,1,15.55),(202,127,1,15.55),(203,359,1,15.55),(204,143,1,15.55),(205,522,1,15.55),(206,156,1,15.55),(207,588,1,15.55),(208,172,1,991.00),(209,664,1,15.55),(210,190,1,15.55),(211,206,1,15.55),(212,222,1,34.00),(213,238,1,15.55),(214,254,1,15.55),(215,270,1,15.55),(216,76,1,15.55),(217,287,1,15.55),(218,92,1,49.99),(219,316,1,15.55),(220,108,1,15.55),(221,332,1,15.55),(222,124,1,15.55),(223,355,1,991.00),(224,140,1,15.55),(225,374,1,15.55),(226,153,1,15.55),(227,583,1,49.99),(228,169,1,991.00),(229,647,1,991.00),(230,187,1,34.00),(231,203,1,15.55),(232,219,1,34.00),(233,235,1,15.55),(234,251,1,15.55),(235,267,1,15.55),(236,73,1,15.55),(237,284,1,15.55),(238,89,1,49.99),(239,313,1,15.55),(240,105,1,15.55),(241,329,1,15.55),(242,121,1,15.55),(243,345,1,15.55),(244,137,1,15.55),(245,371,1,15.55),(246,150,1,15.55),(247,579,1,15.55),(248,166,1,991.00),(249,625,1,49.99),(250,182,1,991.00),(251,200,1,15.55),(252,216,1,34.00),(253,232,1,15.55),(254,248,1,15.55),(255,264,1,15.55),(256,70,1,15.55),(257,281,1,15.55),(258,86,1,49.99),(259,310,1,15.55),(260,102,1,49.99),(261,326,1,15.55),(262,118,1,15.55),(263,342,1,15.55),(264,134,1,15.55),(265,147,1,15.55),(266,574,1,991.00),(267,163,1,15.55),(268,620,1,15.55),(269,179,1,991.00),(270,197,1,15.55),(271,213,1,34.00),(272,229,1,15.55),(273,245,1,15.55),(274,261,1,15.55),(275,67,1,15.55),(276,278,1,15.55),(277,83,1,15.55),(278,304,1,991.00),(279,99,1,49.99),(280,323,1,15.55),(281,115,1,15.55),(282,339,1,15.55),(283,131,1,15.55),(284,368,1,15.55),(285,566,1,15.55),(286,160,1,15.55),(287,612,1,15.55),(288,176,1,991.00),(289,778,1,15.55),(290,194,1,15.55),(291,210,1,34.00),(292,226,1,34.00),(293,242,1,15.55),(294,258,1,15.55),(295,64,1,49.99),(296,275,1,15.55),(297,80,1,15.55),(298,291,1,15.55),(299,96,1,49.99),(300,320,1,15.55),(301,112,1,15.55),(302,336,1,15.55),(303,128,1,15.55),(304,360,1,15.55),(305,144,1,15.55),(306,523,1,15.55),(307,157,1,15.55),(308,589,1,15.55),(309,173,1,991.00),(310,666,1,15.55),(311,191,1,15.55),(312,207,1,15.55),(313,223,1,34.00),(314,239,1,15.55),(315,255,1,15.55),(316,271,1,15.55),(317,77,1,15.55),(318,288,1,15.55),(319,93,1,49.99),(320,317,1,15.55),(321,109,1,15.55),(322,333,1,15.55),(323,125,1,15.55),(324,356,1,990.00),(325,141,1,15.55),(326,826,1,56.99),(327,827,1,56.99),(328,828,1,56.99),(329,165,2,50.00),(330,166,2,50.00),(331,167,2,50.00),(332,168,2,50.00),(333,169,2,50.00),(334,170,2,50.00),(335,171,2,50.00),(336,172,2,50.00),(337,173,2,50.00),(338,174,2,50.00),(339,175,2,50.00),(340,176,2,50.00),(341,177,2,50.00),(342,178,2,50.00),(343,179,2,50.00),(344,180,2,50.00),(345,181,2,50.00),(346,182,2,50.00),(347,183,2,50.00),(348,184,2,50.00),(349,304,2,50.00),(350,355,2,50.00),(351,358,2,50.00),(352,574,2,50.00),(353,584,2,50.00),(354,647,2,50.00),(355,373,2,50.00),(356,580,2,50.00),(357,165,3,200.00),(358,166,3,200.00),(359,167,3,200.00),(360,168,3,200.00),(361,169,3,200.00),(362,170,3,200.00),(363,171,3,200.00),(364,172,3,200.00),(365,173,3,200.00),(366,174,3,200.00),(367,175,3,200.00),(368,176,3,200.00),(369,177,3,200.00),(370,178,3,200.00),(371,179,3,200.00),(372,180,3,200.00),(373,181,3,200.00),(374,182,3,200.00),(375,183,3,200.00),(376,184,3,200.00),(377,304,3,200.00),(378,355,3,200.00),(379,358,3,200.00),(380,574,3,200.00),(381,584,3,200.00),(382,647,3,200.00);
/*!40000 ALTER TABLE `stmd_auto_pickupcost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipment_images`
--

DROP TABLE IF EXISTS `stmd_auto_shipment_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipment_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autoShipmentId` int(11) NOT NULL DEFAULT '0',
  `imageType` enum('carpicture','cartitle') NOT NULL DEFAULT 'carpicture',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `createdByType` enum('user','admin') NOT NULL DEFAULT 'user',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipment_images`
--

LOCK TABLES `stmd_auto_shipment_images` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipment_images` DISABLE KEYS */;
INSERT INTO `stmd_auto_shipment_images` VALUES (1,1,'carpicture','1535631578_car-vehicle-luxury-112460.jpg','1','2018-08-30 12:20:02',3,'user','2018-08-30 12:20:02',0,'0','2018-08-30 12:20:02',0),(2,1,'cartitle','1535631578_download.jpeg','1','2018-08-30 12:20:02',3,'user','2018-08-30 12:20:02',0,'0','2018-08-30 12:20:02',0),(3,2,'carpicture','1535633202_car-vehicle-luxury-112460.jpg','1','2018-08-30 12:47:50',3,'user','2018-08-30 12:47:50',0,'0','2018-08-30 12:47:50',0),(4,2,'cartitle','1535633202_download.jpeg','1','2018-08-30 12:47:50',3,'user','2018-08-30 12:47:50',0,'0','2018-08-30 12:47:50',0),(5,3,'carpicture','1536647034_acura-cl-2001-2003.jpg','1','2018-09-11 07:07:35',1,'admin','2018-09-11 07:07:36',0,'0','2018-09-11 07:07:36',0),(6,4,'carpicture','1536650325_acura-cl-2001-2003.jpg','1','2018-09-11 07:19:32',3,'user','2018-09-11 07:19:32',0,'0','2018-09-11 07:19:32',0),(7,4,'cartitle','1536650325_car-vehicle-luxury-112460.jpg','1','2018-09-11 07:19:32',3,'user','2018-09-11 07:19:32',0,'0','2018-09-11 07:19:32',0),(8,6,'carpicture','1536753542_maxresdefault.jpg','1','2018-09-12 12:02:37',1,'admin','2018-09-12 12:02:37',0,'0','2018-09-12 12:02:37',0),(9,8,'carpicture','1536756422_downloadsnapdeal.png','1','2018-09-12 12:47:10',3,'user','2018-09-12 12:47:10',0,'0','2018-09-12 12:47:10',0),(10,8,'cartitle','1536756422_image_1522933273.png','1','2018-09-12 12:47:10',3,'user','2018-09-12 12:47:10',0,'0','2018-09-12 12:47:10',0),(11,9,'carpicture','1536759635_images.jpeg','1','2018-09-12 13:42:27',3,'user','2018-09-12 13:42:27',0,'0','2018-09-12 13:42:27',0),(12,9,'cartitle','1536759635_images (1).jpeg','1','2018-09-12 13:42:27',3,'user','2018-09-12 13:42:27',0,'0','2018-09-12 13:42:27',0);
/*!40000 ALTER TABLE `stmd_auto_shipment_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipment_images_bak`
--

DROP TABLE IF EXISTS `stmd_auto_shipment_images_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipment_images_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autoShipmentId` int(11) NOT NULL DEFAULT '0',
  `itemId` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipment_images_bak`
--

LOCK TABLES `stmd_auto_shipment_images_bak` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipment_images_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_auto_shipment_images_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipment_issues_log`
--

DROP TABLE IF EXISTS `stmd_auto_shipment_issues_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipment_issues_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `procurementId` int(11) NOT NULL COMMENT 'Procurement Id/Shipment Id',
  `message` text NOT NULL,
  `sentBy` int(11) NOT NULL,
  `sentOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipment_issues_log`
--

LOCK TABLES `stmd_auto_shipment_issues_log` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipment_issues_log` DISABLE KEYS */;
INSERT INTO `stmd_auto_shipment_issues_log` VALUES (1,3,23,'Sorry for delays',1,'2018-09-12 11:30:02'),(2,3,6,'Hello',1,'2018-09-12 12:16:45'),(3,3,9,'dcsdscsdcdscs demo shipment # 9',1,'2018-09-12 13:50:34');
/*!40000 ALTER TABLE `stmd_auto_shipment_issues_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipment_items`
--

DROP TABLE IF EXISTS `stmd_auto_shipment_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipment_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL DEFAULT '0',
  `makeId` int(11) NOT NULL,
  `modelId` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `vin` varchar(64) NOT NULL DEFAULT '',
  `millege` varchar(255) NOT NULL,
  `carValue` decimal(12,2) NOT NULL,
  `insurance` char(1) NOT NULL DEFAULT 'N',
  `title` varchar(64) NOT NULL DEFAULT '',
  `condition` varchar(255) NOT NULL DEFAULT '',
  `carColor` varchar(16) NOT NULL,
  `carWebsiteId` int(11) NOT NULL,
  `websiteUrl` varchar(255) DEFAULT NULL,
  `hasPersonalItems` enum('Y','N') NOT NULL DEFAULT 'N',
  `personalItems` text NOT NULL,
  `deliveryMethod` varchar(64) NOT NULL DEFAULT '',
  `deliveryCompany` int(11) NOT NULL,
  `delivered` datetime NOT NULL,
  `quote` varchar(64) NOT NULL DEFAULT '',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipment_items`
--

LOCK TABLES `stmd_auto_shipment_items` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipment_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_auto_shipment_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipment_status_log`
--

DROP TABLE IF EXISTS `stmd_auto_shipment_status_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipment_status_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autoshipmentId` int(11) NOT NULL,
  `oldStatus` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `updatedOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `autoshipmentId` (`autoshipmentId`),
  CONSTRAINT `stmd_auto_shipment_status_log_ibfk_1` FOREIGN KEY (`autoshipmentId`) REFERENCES `stmd_auto_shipments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipment_status_log`
--

LOCK TABLES `stmd_auto_shipment_status_log` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipment_status_log` DISABLE KEYS */;
INSERT INTO `stmd_auto_shipment_status_log` VALUES (1,1,'none','submitted','2018-08-30 12:20:02'),(2,2,'none','submitted','2018-08-30 12:47:50'),(3,3,'none','submitted','2018-09-11 07:00:33'),(4,3,'submitted','orderplaced','2018-09-11 07:05:53'),(5,3,'orderplaced','pickedup','2018-09-11 07:06:01'),(6,3,'pickedup','received','2018-09-11 07:06:17'),(7,4,'none','submitted','2018-09-11 07:19:32'),(12,6,'none','submitted','2018-09-12 11:59:22'),(13,6,'submitted','orderplaced','2018-09-12 12:02:08'),(14,6,'orderplaced','pickedup','2018-09-12 12:02:16'),(15,6,'pickedup','received','2018-09-12 12:02:21'),(16,6,'received','customverification','2018-09-12 12:24:15'),(17,6,'customverification','sailed','2018-09-12 12:29:37'),(18,6,'sailed','customclearing','2018-09-12 12:30:03'),(19,6,'customclearing','availpickup','2018-09-12 12:30:09'),(20,6,'availpickup','completed','2018-09-12 12:30:48'),(22,8,'none','submitted','2018-09-12 12:47:10'),(23,8,'submitted','orderplaced','2018-09-12 12:49:59'),(24,8,'orderplaced','pickedup','2018-09-12 12:50:42'),(25,8,'pickedup','received','2018-09-12 12:51:35'),(26,8,'received','customverification','2018-09-12 12:52:20'),(27,8,'customverification','sailed','2018-09-12 12:52:52'),(28,8,'sailed','customclearing','2018-09-12 12:52:58'),(29,8,'customclearing','availpickup','2018-09-12 12:53:06'),(30,8,'availpickup','completed','2018-09-12 12:54:21'),(31,9,'none','submitted','2018-09-12 13:42:27'),(32,9,'submitted','orderplaced','2018-09-12 13:46:51'),(33,9,'orderplaced','pickedup','2018-09-12 13:50:56'),(34,9,'pickedup','received','2018-09-12 13:51:06'),(35,9,'received','customverification','2018-09-12 13:51:11'),(36,9,'customverification','sailed','2018-09-12 13:51:14'),(37,9,'sailed','customclearing','2018-09-12 13:51:18'),(38,9,'customclearing','availpickup','2018-09-12 13:51:22'),(39,9,'availpickup','completed','2018-09-12 13:51:25');
/*!40000 ALTER TABLE `stmd_auto_shipment_status_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipments`
--

DROP TABLE IF EXISTS `stmd_auto_shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0',
  `shipmentStatus` enum('submitted','orderplaced','pickedup','customverification','received','sailed','customclearing','availpickup','completed') NOT NULL DEFAULT 'submitted',
  `paymentStatus` enum('paid','unpaid') NOT NULL DEFAULT 'unpaid',
  `paymentMethodId` int(11) DEFAULT NULL,
  `paymentReceivedOn` datetime DEFAULT NULL,
  `pickupName` varchar(100) NOT NULL,
  `pickupAddress` varchar(100) NOT NULL,
  `pickupCountry` int(11) NOT NULL,
  `pickupState` int(11) NOT NULL,
  `pickupCity` int(11) NOT NULL,
  `pickupPhone` varchar(32) NOT NULL,
  `pickupEmail` varchar(100) NOT NULL,
  `pickupLocationType` enum('home','dealer','auction') DEFAULT 'home',
  `pickupDate` date NOT NULL,
  `warehouseId` int(11) NOT NULL DEFAULT '0',
  `recceiverName` varchar(100) NOT NULL DEFAULT '',
  `receiverAddress` varchar(100) NOT NULL DEFAULT '',
  `destinationCountry` int(11) NOT NULL,
  `destinationState` int(11) NOT NULL,
  `destinationCity` int(11) NOT NULL,
  `receiverPhone` varchar(100) NOT NULL DEFAULT '',
  `receiverEmail` varchar(64) NOT NULL DEFAULT '',
  `makeId` int(11) NOT NULL,
  `modelId` int(11) NOT NULL,
  `year` int(4) NOT NULL,
  `carCondition` enum('drivable','not_drivable') NOT NULL DEFAULT 'not_drivable',
  `color` varchar(50) DEFAULT NULL,
  `websiteLink` text,
  `milage` varchar(10) DEFAULT NULL,
  `vinNumber` int(11) DEFAULT NULL,
  `itemPrice` decimal(12,2) NOT NULL,
  `pickupCost` decimal(12,2) NOT NULL,
  `insuranceCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `clearingItemsCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `taxCost` decimal(12,2) DEFAULT NULL,
  `discountAmount` decimal(12,2) DEFAULT NULL,
  `couponcodeApplied` varchar(255) DEFAULT NULL,
  `totalCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `shipmentCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `isCurrencyChanged` enum('Y','N') NOT NULL DEFAULT 'N',
  `defaultCurrencyCode` varchar(4) DEFAULT NULL,
  `paidCurrencyCode` varchar(4) DEFAULT NULL,
  `exchangeRate` decimal(12,2) DEFAULT NULL,
  `trackingUrl` varchar(64) NOT NULL DEFAULT '',
  `deliveryNotes` text,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `createdByType` enum('user','admin') NOT NULL DEFAULT 'user',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `receiverCountry` (`destinationCountry`),
  KEY `receiverState` (`destinationState`),
  KEY `receiverState_2` (`destinationState`),
  KEY `receiverCity` (`destinationCity`),
  KEY `warehouseId` (`warehouseId`),
  KEY `makeId` (`makeId`),
  KEY `modelId` (`modelId`),
  CONSTRAINT `stmd_auto_shipments_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_auto_shipments_ibfk_2` FOREIGN KEY (`makeId`) REFERENCES `stmd_auto_make` (`id`),
  CONSTRAINT `stmd_auto_shipments_ibfk_3` FOREIGN KEY (`modelId`) REFERENCES `stmd_auto_model` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipments`
--

LOCK TABLES `stmd_auto_shipments` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipments` DISABLE KEYS */;
INSERT INTO `stmd_auto_shipments` VALUES (1,3,'submitted','unpaid',6,NULL,'John Doe','550 E. Van Buren Street, Phoenix',230,91,356,'123456','johndoe@gmail.com','home','2018-08-31',1,'Chris Martin','1234 Bauchu Road, Yelwa',163,2,639,'123456456','chrismartin@gmail.com',4,2,2018,'drivable','blue','www.cargurus.com/acura','25',123456,7800.00,15.55,0.00,0.00,1.17,0.00,NULL,40.20,23.48,'N','USD','USD',1.00,'',NULL,'2018-08-30 12:20:02',3,'user','2018-08-30 12:20:02',0,'0','2018-08-30 12:20:02',0),(2,3,'submitted','unpaid',5,NULL,'John Doe','550 E. Van Buren Street, Phoenix',230,91,356,'123456','johndoe@gmail.com','home','2018-08-31',1,'Chris Martin','1234 Bauchu Road, Yelwa',163,2,639,'123456456','chrismartin@gmail.com',4,2,2018,'drivable','black','www.cargurus.com/acura','25',1234456,7800.00,15.55,0.00,0.00,1.17,0.00,NULL,40.20,23.48,'N','USD','USD',1.00,'',NULL,'2018-08-30 12:47:50',3,'user','2018-08-30 12:47:50',0,'0','2018-08-30 12:47:50',0),(3,3,'received','paid',1,NULL,'','12011 Westbrae Parkway #B',230,120,256,'','','home','2018-09-30',1,'somnath pahari','211 South Meadows Court',163,2,639,'713-530-1160','somnath.pahari@indusnet.co.in',4,2,2017,'drivable','Silver','https://www.cargurus.com/Cars/inventorylisting/viewDetailsFilterViewInventoryListing.action?sourceContext=carGurusHomePageModel&entitySelectingHelper.selectedEntity=d191&zip=85001#listing=210629583','160692',19,5995.00,15.55,586.31,0.00,20.00,10.00,'nigeria10',645.34,23.48,'N','USD','USD',1.00,'',NULL,'2018-09-11 07:07:35',1,'admin','2018-09-11 07:07:36',0,'0','2018-09-11 07:07:36',0),(4,3,'submitted','paid',7,NULL,'John Doe','2540 E 6th St',230,91,356,'8596321475','johndoe@gmail.com','home','2018-09-16',1,'John Mile','Landmark Towers , 5B Water Corporation Road , Oniru Estate, Victoria Island',163,2,639,'56984123','johnmile@gmail.com',4,2,2017,'drivable','silver','www.acura.com/CL','13560',7856321,5995.00,15.55,0.00,0.00,0.00,0.00,NULL,39.03,23.48,'N','USD','USD',1.00,'',NULL,'2018-09-11 07:19:32',3,'user','2018-09-11 07:19:32',0,'0','2018-09-11 07:19:32',0),(6,3,'completed','paid',2,'2018-09-12 12:01:56','','12011 Westbrae Parkway #B',230,120,256,'','','home','2018-09-30',1,'Paulami Sarkar','Address 1',163,15,638,'6767','paulami.sarkar@indusnet.co.in',4,2,2017,'drivable','Red','pp23.com','11',11,25000.00,49.99,0.00,0.00,0.00,0.00,NULL,73.47,23.48,'N','USD','USD',1.00,'',NULL,'2018-09-12 12:02:37',1,'admin','2018-09-12 12:02:37',0,'0','2018-09-12 12:02:37',0),(8,3,'completed','paid',2,'2018-09-12 12:49:40','Mr. Test Alver','Address 1, Test Adddress',230,80,13,'2423424','testuser@example.com','home','2018-09-30',1,'Alder demo','Address 1',163,5,378,'242342450','testuser@example.com',4,2,2017,'drivable','black','direct.com','12',2147483647,1000.00,49.99,0.00,0.00,0.00,0.00,NULL,73.47,23.48,'N','USD','USD',1.00,'',NULL,'2018-09-12 12:47:10',3,'user','2018-09-12 12:47:10',0,'0','2018-09-12 12:47:10',0),(9,3,'completed','paid',2,'2018-09-12 13:46:12','Mr. Test Alver','Address 1, Test Adddress',230,80,13,'2423424','testuser@example.com','dealer','2018-10-30',1,'Alder demo','Address 1',163,5,378,'242342466565','testuser@example2.com',4,2,2016,'not_drivable','black','direct.com','15',0,100000.00,49.99,0.00,0.00,0.00,0.00,NULL,73.47,23.48,'N','USD','USD',1.00,'',NULL,'2018-09-12 13:42:27',3,'user','2018-09-12 13:42:27',0,'0','2018-09-12 13:42:27',0);
/*!40000 ALTER TABLE `stmd_auto_shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipments_bak`
--

DROP TABLE IF EXISTS `stmd_auto_shipments_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipments_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0',
  `pickupName` varchar(100) NOT NULL,
  `pickupStateId` int(11) NOT NULL,
  `pickupCityId` int(11) NOT NULL,
  `pickupEmail` varchar(100) NOT NULL,
  `pickupLocationType` int(11) DEFAULT NULL,
  `postedOn` datetime NOT NULL,
  `shipmentStatus` enum('0','1','2','3','4','5','6') NOT NULL DEFAULT '1' COMMENT '0 = Shipment Submiited, 1= In Warehouse, 2= In Transit, 3= Customs clearing. 4=In destination warehouse, 5=Out for delivery, 6= Delivered',
  `warehouseId` int(11) DEFAULT NULL,
  `recceiverName` varchar(100) NOT NULL DEFAULT '',
  `receiverAddress` varchar(100) NOT NULL DEFAULT '',
  `destinationCountry` int(11) DEFAULT NULL,
  `destinationState` int(11) DEFAULT NULL,
  `destinationCity` int(11) DEFAULT NULL,
  `receiverPhone` varchar(100) NOT NULL DEFAULT '',
  `receiverEmail` varchar(64) NOT NULL DEFAULT '',
  `whoClear` enum('0','1','2') NOT NULL COMMENT '0 = Not Decided, 1 = ShopToMyDoor, 2 = MyAgent',
  `quote` varchar(64) NOT NULL DEFAULT '',
  `deliveryLocation` enum('0','1','2') DEFAULT '0' COMMENT '0=NA, 1=ShopToMyDoor, 2=MyLocation',
  `insuranceCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `clearingItemsCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `totalCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `shipmentCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `shippingOption` varchar(64) NOT NULL DEFAULT '',
  `destinationPort` varchar(64) NOT NULL DEFAULT '',
  `passportNumber` varchar(64) NOT NULL DEFAULT '',
  `billOfLaden` varchar(64) NOT NULL DEFAULT '',
  `shippingLine` varchar(64) NOT NULL DEFAULT '',
  `ship` varchar(64) NOT NULL DEFAULT '',
  `trackingUrl` varchar(64) NOT NULL DEFAULT '',
  `stage1` datetime NOT NULL,
  `stage2` datetime NOT NULL,
  `stage3` datetime NOT NULL,
  `stage4` datetime NOT NULL,
  `stage5` datetime NOT NULL,
  `stage6` datetime NOT NULL,
  `issues` varchar(255) NOT NULL DEFAULT '',
  `billOfLadenFile` varchar(64) NOT NULL DEFAULT '',
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'User ID who processed payment status on shipment page',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `receiverCountry` (`destinationCountry`),
  KEY `receiverState` (`destinationState`),
  KEY `receiverState_2` (`destinationState`),
  KEY `receiverCity` (`destinationCity`),
  KEY `warehouseId` (`warehouseId`),
  KEY `stmd_autoshipment_ibfk_8` (`pickupStateId`),
  KEY `stmd_autoshipment_ibfk_9` (`pickupCityId`),
  CONSTRAINT `stmd_auto_shipments_bak_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_autoshipment_ibfk_4` FOREIGN KEY (`destinationCountry`) REFERENCES `stmd_countries` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_autoshipment_ibfk_5` FOREIGN KEY (`destinationState`) REFERENCES `stmd_states` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_autoshipment_ibfk_6` FOREIGN KEY (`destinationCity`) REFERENCES `stmd_cities` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_autoshipment_ibfk_7` FOREIGN KEY (`warehouseId`) REFERENCES `stmd_warehousemaster` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_autoshipment_ibfk_8` FOREIGN KEY (`pickupStateId`) REFERENCES `stmd_states` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_autoshipment_ibfk_9` FOREIGN KEY (`pickupCityId`) REFERENCES `stmd_cities` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipments_bak`
--

LOCK TABLES `stmd_auto_shipments_bak` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipments_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_auto_shipments_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipping_cost`
--

DROP TABLE IF EXISTS `stmd_auto_shipping_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipping_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zoneId` int(11) NOT NULL,
  `pzoneId` int(11) NOT NULL,
  `makeId` int(11) NOT NULL,
  `modelId` int(11) NOT NULL,
  `shippingCharge` decimal(12,2) NOT NULL DEFAULT '0.00',
  `clearingPortHandlingCharge` decimal(12,2) NOT NULL,
  `insurancePercent` decimal(12,2) NOT NULL,
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `makeId` (`makeId`),
  KEY `modelId` (`modelId`),
  KEY `zoneId` (`zoneId`),
  KEY `pzoneId` (`pzoneId`),
  CONSTRAINT `stmd_auto_shipping_cost_ibfk_1` FOREIGN KEY (`makeId`) REFERENCES `stmd_auto_make` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_auto_shipping_cost_ibfk_2` FOREIGN KEY (`modelId`) REFERENCES `stmd_auto_model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_auto_shipping_cost_ibfk_3` FOREIGN KEY (`zoneId`) REFERENCES `stmd_zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `stmd_auto_shipping_cost_ibfk_4` FOREIGN KEY (`pzoneId`) REFERENCES `stmd_zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipping_cost`
--

LOCK TABLES `stmd_auto_shipping_cost` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipping_cost` DISABLE KEYS */;
INSERT INTO `stmd_auto_shipping_cost` VALUES (1,1,4,4,2,10.99,12.49,9.78,'0',1,'2018-07-26 17:55:19',NULL,NULL,1,'2018-07-26 20:19:25'),(2,1,7,4,2,78.00,98.00,9.99,'0',1,'2018-07-26 17:55:46',NULL,NULL,1,'2018-07-26 20:23:11'),(3,1,8,6,21,45.00,65.00,9.99,'0',1,'2018-07-26 17:58:16',NULL,NULL,1,'2018-07-26 20:23:11'),(4,43,7,4,2,10.54,10.78,9.75,'0',1,'2018-07-26 17:58:45',NULL,NULL,1,'2018-07-26 20:23:11'),(5,1,39,8,49,76.55,45.55,5.55,'0',1,'2018-07-26 20:41:08',NULL,NULL,NULL,NULL),(6,4,48,53,129,10.00,15.00,9.99,'0',1,'2018-09-12 10:20:13',NULL,NULL,NULL,NULL),(7,4,48,54,125,10.00,1.00,1.00,'0',1,'2018-09-12 10:21:20',NULL,NULL,NULL,NULL),(8,4,48,8,43,10.00,15.00,9.99,'0',1,'2018-09-12 10:22:47',NULL,NULL,NULL,NULL),(9,4,48,55,128,8.00,9.00,9.99,'0',1,'2018-09-12 10:23:30',NULL,NULL,NULL,NULL),(10,4,48,16,76,10.00,10.00,9.99,'0',1,'2018-09-12 10:25:40',NULL,NULL,NULL,NULL),(11,43,41,54,125,1.00,1.00,9.99,'0',1,'2018-09-12 10:26:34',NULL,NULL,NULL,NULL),(12,4,1,4,2,10.00,10.00,9.99,'0',1,'2018-09-13 05:05:51',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_auto_shipping_cost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_shipping_lines`
--

DROP TABLE IF EXISTS `stmd_auto_shipping_lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_shipping_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `line` varchar(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `fileName` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `displayOrder` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_shipping_lines`
--

LOCK TABLES `stmd_auto_shipping_lines` WRITE;
/*!40000 ALTER TABLE `stmd_auto_shipping_lines` DISABLE KEYS */;
INSERT INTO `stmd_auto_shipping_lines` VALUES (1,'Shipping Line','1530262008_noimage.png',1,1,'2018-06-29 08:46:48',NULL,NULL,NULL,NULL),(2,'My Shipping demo Line1','1536749143_downloadsnapdeal.png',2,1,'2018-09-12 10:45:13',1,'2018-09-12 10:45:43',NULL,NULL);
/*!40000 ALTER TABLE `stmd_auto_shipping_lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_website_makeModel_mapping`
--

DROP TABLE IF EXISTS `stmd_auto_website_makeModel_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_website_makeModel_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `websiteId` int(11) NOT NULL,
  `makeId` int(11) NOT NULL,
  `modelId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_website_makeModel_mapping`
--

LOCK TABLES `stmd_auto_website_makeModel_mapping` WRITE;
/*!40000 ALTER TABLE `stmd_auto_website_makeModel_mapping` DISABLE KEYS */;
INSERT INTO `stmd_auto_website_makeModel_mapping` VALUES (1,1,10,74),(2,1,10,75),(3,1,16,78),(4,1,16,79),(5,1,4,2);
/*!40000 ALTER TABLE `stmd_auto_website_makeModel_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_auto_websites`
--

DROP TABLE IF EXISTS `stmd_auto_websites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_auto_websites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedBy` int(11) NOT NULL,
  `deletedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_auto_websites`
--

LOCK TABLES `stmd_auto_websites` WRITE;
/*!40000 ALTER TABLE `stmd_auto_websites` DISABLE KEYS */;
INSERT INTO `stmd_auto_websites` VALUES (1,'Cargurus.com','Cargurus','1531480069_ship_ribbon.png','1','0',0,'2018-07-13 11:07:49',1,'2018-07-13 11:07:49',NULL,NULL);
/*!40000 ALTER TABLE `stmd_auto_websites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_banners`
--

DROP TABLE IF EXISTS `stmd_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('1','2','3','4','5','6') CHARACTER SET latin1 NOT NULL COMMENT '1 for homepage banner, 2 for registration banner, 3 for warehouse banner, 4 for fillnship banner, 5 for shopforme banner, 6 for auto banner',
  `bannerType` enum('image','video') CHARACTER SET latin1 NOT NULL DEFAULT 'image',
  `imagePath` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'used to store both banner image and video preview image',
  `videoPath` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `displayOrder` int(3) NOT NULL,
  `status` enum('1','0') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `isDefault` enum('1','0') NOT NULL DEFAULT '0' COMMENT 'Used to set default image for registration page',
  `bannerContent` longtext,
  `pageLink` text,
  `slug` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_banners`
--

LOCK TABLES `stmd_banners` WRITE;
/*!40000 ALTER TABLE `stmd_banners` DISABLE KEYS */;
INSERT INTO `stmd_banners` VALUES (21,'1','image','1532078856_banner-1.jpg','',1,'1','0','&lt;div class=&quot;col caption-content&quot;&gt;\r\n				&lt;h1 class=&quot;font-weight-bold text-white mb-md-4&quot;&gt;Introducing Fill &amp; Ship&lt;/h1&gt;\r\n				&lt;ul class=&quot;list-ui mb-md-5&quot;&gt;\r\n					&lt;li&gt;Know your shipping cost before shopping&lt;/li&gt;\r\n					&lt;li&gt;Easily compare costs for up to 18 different boxes&lt;/li&gt;\r\n					&lt;li&gt;See in real time space left as items are load into your box&lt;/li&gt;\r\n					&lt;li&gt;Get refunded for unfilled spaces&lt;/li&gt;\r\n				&lt;/ul&gt;\r\n				&lt;a href=&quot;#&quot; class=&quot;btn btn-lg btn-secondary&quot;&gt;\r\n					&lt;i class=&quot;fa fa-play mr-2&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;Check Out Our Intro Video&lt;/a&gt;\r\n			&lt;/div&gt;',NULL,''),(22,'1','video','1532078878_597938858.jpg','https://youtu.be/PCwL3-hkKrg',2,'1','0',NULL,NULL,''),(23,'2','image','1532097610_banner-1.jpg',NULL,1,'1','1',NULL,NULL,'id23'),(24,'2','image','1527082397_7f1617af6217d4e9a977732a8e24a8d5.jpg',NULL,3,'1','',NULL,NULL,'id24'),(27,'3','image','1527242758_7f1617af6217d4e9a977732a8e24a8d5.jpg',NULL,1,'1','0',NULL,NULL,''),(28,'3','image','1527242767_30f6d22adf83742fc5506a28c4de5b64.jpg',NULL,2,'1','0',NULL,NULL,'');
/*!40000 ALTER TABLE `stmd_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_blockcontents`
--

DROP TABLE IF EXISTS `stmd_blockcontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_blockcontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `blockTitle` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `contentTitle` varchar(255) DEFAULT NULL,
  `contentSubTitle` varchar(255) DEFAULT NULL,
  `contentImage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_blockcontents`
--

LOCK TABLES `stmd_blockcontents` WRITE;
/*!40000 ALTER TABLE `stmd_blockcontents` DISABLE KEYS */;
INSERT INTO `stmd_blockcontents` VALUES (1,'why_shoptomydoor','Why Shoptomydoor','<h3 class=\\\"text-uppercase text-primary font-weight-bold\\\">100% Money Back</h3>\r\n\r\n<p class=\\\"m-0\\\">Our key promise to you is simple.</p>\r\n\r\n<p class=\\\"mb-md-4\\\">Anything happens to your items, we compensate you 100% for:</p>\r\n\r\n<ul class=\\\"list-ui\\\">\r\n	<li>Total cost of your items</li>\r\n	<li>Total shipping cost for your items</li>\r\n</ul>\r\n\r\n<p><span class=\\\"text-uppercase text-primary mb-3 mb-md-5 h5 d-block\\\">So <strong>relax</strong>, <strong>shop</strong>, <strong>save</strong> and enjoy <strong>guaranteed</strong> delivery.</span> <a class=\\\"btn btn-lg btn-secondary\\\" routerlink=\\\"/\\\">Learn More About Guaranteed Delivery</a></p>','1','100% Money Back',NULL,'1531304190_money-back-guarantee.png'),(2,'why_shoptomydoor','Why Shoptomydoor','<h3 class=\\\"text-uppercase text-primary font-weight-bold\\\">Special offer</h3>\r\n						<p class=\\\"m-0\\\">Hurry Up !!</p>\r\n						<p class=\\\"mb-md-4\\\">Check special offer on selected products</p>\r\n						<ul class=\\\"list-ui\\\">\r\n							<li>Total cost of your items</li>\r\n							<li>Total shipping cost for your items</li>\r\n						</ul>\r\n						<span class=\\\"text-uppercase text-primary mb-3 mb-md-5 h5 d-block\\\">So\r\n							<strong>relax</strong>,\r\n							<strong>shop</strong>,\r\n							<strong>save</strong> and enjoy\r\n							<strong>guaranteed</strong> delivery.</span>\r\n						<a routerLink=\\\"/\\\" class=\\\"btn btn-lg btn-secondary\\\">Learn More About Guaranteed Delivery</a>','1','Special offer',NULL,'1531304521_Shelf_Wobblers-Red-7.jpg'),(3,'how_it_works','How it Works','<h4 class=\\\"text-primary\\\">Signup Immediately</h4>\r\n							<p>Your assigned address allows you to\r\n								<a routerLink=\\\"/\\\" class=\\\"text-secondary text-underline\\\">shop in the US</a> (UK and China for some customers) using the following simple method.</p>\r\n							<ul class=\\\"list-ui\\\">\r\n								<li>Go to any store of your choice, shop and pay with your visa, mastercard or Paypal.</li>\r\n								<li>Send all purchases to your assigned address. I.e Your assigned address is your shipping address. </li>\r\n								<li>Shop as much as you want. You have 21 days free storage to shop in as many stores as you want, and we combine all\r\n									in your warehouse.\r\n								</li>\r\n								<li>We alert you via email and SMS once any item is received for you.</li>\r\n							</ul>\r\n							<div class=\\\"text-center\\\">\r\n								<a routerLink=\\\"/\\\" class=\\\"btn btn-secondary btn-lg\\\">Learn More</a>\r\n							</div>','1','Sign Up test','Its Free test',NULL),(4,'how_it_works','How it Works','<h4 class=\\\"text-primary\\\">Shop Online Immediately</h4>\r\n							<p>Your assigned address allows you to\r\n								<a routerLink=\\\"/\\\" class=\\\"text-secondary text-underline\\\">shop in the US</a> (UK and China for some customers) using the following simple method.</p>\r\n							<ul class=\\\"list-ui\\\">\r\n								<li>Go to any store of your choice, shop and pay with your visa, mastercard or Paypal.</li>\r\n								<li>Send all purchases to your assigned address. I.e Your assigned address is your shipping address. </li>\r\n								<li>Shop as much as you want. You have 21 days free storage to shop in as many stores as you want, and we combine all\r\n									in your warehouse.\r\n								</li>\r\n								<li>We alert you via email and SMS once any item is received for you.</li>\r\n							</ul>\r\n							<div class=\\\"text-center\\\">\r\n								<a routerLink=\\\"/\\\" class=\\\"btn btn-secondary btn-lg\\\">Learn More</a>\r\n							</div>','1','Shop Online','Immediately',NULL),(5,'how_it_works','How it Works','<h4 class=\\\"text-primary\\\">We deliver to your door step</h4>\r\n							<p>Your assigned address allows you to\r\n								<a routerLink=\\\"/\\\" class=\\\"text-secondary text-underline\\\">shop in the US</a> (UK and China for some customers) using the following simple method.</p>\r\n							<ul class=\\\"list-ui\\\">\r\n								<li>Go to any store of your choice, shop and pay with your visa, mastercard or Paypal.</li>\r\n								<li>Send all purchases to your assigned address. I.e Your assigned address is your shipping address. </li>\r\n								<li>Shop as much as you want. You have 21 days free storage to shop in as many stores as you want, and we combine all\r\n									in your warehouse.\r\n								</li>\r\n								<li>We alert you via email and SMS once any item is received for you.</li>\r\n							</ul>\r\n							<div class=\\\"text-center\\\">\r\n								<a routerLink=\\\"/\\\" class=\\\"btn btn-secondary btn-lg\\\">Learn More</a>\r\n							</div>','1','We Deliver','to Your Door',NULL),(6,'service','Service','<h3 class=\\\"text-primary\\\">Air Shipping Features</h3>\r\n						<p>Your assigned address allows you to\r\n							<a routerLink=\\\"/\\\" class=\\\"text-secondary text-underline\\\">shop in the US</a> (UK and China for some customers) using the following simple method.</p>\r\n						<ul class=\\\"list-ui\\\">\r\n							<li>Go to any store of your choice, shop and pay with your visa, mastercard or Paypal.</li>\r\n							<li>Send all purchases to your assigned address. I.e Your assigned address is your shipping address. </li>\r\n							<li>Shop as much as you want. You have 21 days free storage to shop in as many stores as you want, and we combine all\r\n								in your warehouse.\r\n							</li>\r\n							<li>We alert you via email and SMS once any item is received for you.</li>\r\n						</ul>\r\n						<div class=\\\"text-center\\\">\r\n							<a routerLink=\\\"/\\\" class=\\\"btn btn-secondary btn-lg\\\">Learn More</a>\r\n						</div>','1','Air Shipping',NULL,NULL),(7,'service','Service','<h3 class=\\\"text-primary\\\">Ocen Shipping Features</h3>\r\n						<p>Your assigned address allows you to\r\n							<a routerLink=\\\"/\\\" class=\\\"text-secondary text-underline\\\">shop in the US</a> (UK and China for some customers) using the following simple method.</p>\r\n						<ul class=\\\"list-ui\\\">\r\n							<li>Go to any store of your choice, shop and pay with your visa, mastercard or Paypal.</li>\r\n							<li>Send all purchases to your assigned address. I.e Your assigned address is your shipping address. </li>\r\n							<li>Shop as much as you want. You have 21 days free storage to shop in as many stores as you want, and we combine all\r\n								in your warehouse.\r\n							</li>\r\n							<li>We alert you via email and SMS once any item is received for you.</li>\r\n						</ul>\r\n						<div class=\\\"text-center\\\">\r\n							<a routerLink=\\\"/\\\" class=\\\"btn btn-secondary btn-lg\\\">Learn More</a>\r\n						</div>','1','Ocean Shipping',NULL,NULL),(8,'service','Service','<h4 class=\\\"text-primary\\\">Car Purchase Features</h4>\r\n						<p>Your assigned address allows you to\r\n							<a routerLink=\\\"/\\\" class=\\\"text-secondary text-underline\\\">shop in the US</a> (UK and China for some customers) using the following simple method.</p>\r\n						<ul class=\\\"list-ui\\\">\r\n							<li>Go to any store of your choice, shop and pay with your visa, mastercard or Paypal.</li>\r\n							<li>Send all purchases to your assigned address. I.e Your assigned address is your shipping address. </li>\r\n							<li>Shop as much as you want. You have 21 days free storage to shop in as many stores as you want, and we combine all\r\n								in your warehouse.\r\n							</li>\r\n							<li>We alert you via email and SMS once any item is received for you.</li>\r\n						</ul>\r\n						<div class=\\\"text-center\\\">\r\n							<a routerLink=\\\"/\\\" class=\\\"btn btn-secondary btn-lg\\\">Learn More</a>\r\n						</div>','1','Car Purchase',NULL,NULL),(9,'service','Service','<h4 class=\\\"text-primary\\\">Procurement Features</h4>\r\n						<p>Your assigned address allows you to\r\n							<a routerLink=\\\"/\\\" class=\\\"text-secondary text-underline\\\">shop in the US</a> (UK and China for some customers) using the following simple method.</p>\r\n						<ul class=\\\"list-ui\\\">\r\n							<li>Go to any store of your choice, shop and pay with your visa, mastercard or Paypal.</li>\r\n							<li>Send all purchases to your assigned address. I.e Your assigned address is your shipping address. </li>\r\n							<li>Shop as much as you want. You have 21 days free storage to shop in as many stores as you want, and we combine all\r\n								in your warehouse.\r\n							</li>\r\n							<li>We alert you via email and SMS once any item is received for you.</li>\r\n						</ul>\r\n						<div class=\\\"text-center\\\">\r\n							<a routerLink=\\\"/\\\" class=\\\"btn btn-secondary btn-lg\\\">Learn More</a>\r\n						</div>','1','Procurement',NULL,NULL),(10,'value_added_service','Value Added Services','<div class=\\\"row no-gutters value-added-inn\\\">\r\n			<div class=\\\"col-md-3 col-sm-4 col-6 item mb-3\\\">\r\n				<a routerLink=\\\"/\\\">\r\n					<svg-icon src=\\\"assets/imgs/icons/free-warehouse.svg\\\" [svgStyle]=\\\"{ \\\'height.px\\\':50}\\\"></svg-icon>\r\n					<span class=\\\"mt-3\\\">Free Warehouse Address (USA, UK and China)</span>\r\n				</a>\r\n			</div>\r\n			<div class=\\\"col-md-3 col-sm-4 col-6 item mb-3\\\">\r\n				<a routerLink=\\\"/\\\">\r\n					<svg-icon src=\\\"assets/imgs/icons/free-storage.svg\\\" [svgStyle]=\\\"{ \\\'height.px\\\':50}\\\"></svg-icon>\r\n					<span class=\\\"mt-3\\\">21 Days Free of Charge  Storage Period</span>\r\n				</a>\r\n			</div>\r\n			<div class=\\\"col-md-3 col-sm-4 col-6 item mb-3\\\">\r\n				<a routerLink=\\\"/\\\">\r\n					<svg-icon src=\\\"assets/imgs/icons/free-repackaging.svg\\\" [svgStyle]=\\\"{ \\\'height.px\\\':50}\\\"></svg-icon>\r\n					<span class=\\\"mt-3\\\">Free Repackaging and Consolidation</span>\r\n				</a>\r\n			</div>\r\n			<div class=\\\"col-md-3 col-sm-4 col-6 item mb-3\\\">\r\n				<a routerLink=\\\"/\\\">\r\n					<svg-icon src=\\\"assets/imgs/icons/customer-care.svg\\\" [svgStyle]=\\\"{ \\\'height.px\\\':50}\\\"></svg-icon>\r\n					<span class=\\\"mt-3\\\">Responsive 24/7 Customer Care</span>\r\n				</a>\r\n			</div>\r\n			<div class=\\\"col-md-3 col-sm-4 col-6 item mb-3\\\">\r\n				<a routerLink=\\\"/\\\">\r\n					<svg-icon src=\\\"assets/imgs/icons/online-tracking.svg\\\" [svgStyle]=\\\"{ \\\'height.px\\\':50}\\\"></svg-icon>\r\n					<span class=\\\"mt-3\\\">Online 24/7 Tracking With Regular Notification</span>\r\n				</a>\r\n			</div>\r\n			<div class=\\\"col-md-3 col-sm-4 col-6 item mb-3\\\">\r\n				<a routerLink=\\\"/\\\">\r\n					<svg-icon src=\\\"assets/imgs/icons/free-insurance.svg\\\" [svgStyle]=\\\"{ \\\'height.px\\\':50}\\\"></svg-icon>\r\n					<span class=\\\"mt-3\\\">Free Insurance Coverage Money</span>\r\n				</a>\r\n			</div>\r\n			<div class=\\\"col-md-3 col-sm-4 col-6 item mb-3\\\">\r\n				<a routerLink=\\\"/\\\">\r\n					<svg-icon src=\\\"assets/imgs/icons/personal-shopper.svg\\\" [svgStyle]=\\\"{ \\\'height.px\\\':50}\\\"></svg-icon>\r\n					<span class=\\\"mt-3\\\">Personal Shopper Service</span>\r\n				</a>\r\n			</div>\r\n			<div class=\\\"col-md-3 col-sm-4 col-6 item mb-3\\\">\r\n				<a routerLink=\\\"/\\\">\r\n					<svg-icon src=\\\"assets/imgs/icons/discount-on-shipping.svg\\\" [svgStyle]=\\\"{ \\\'height.px\\\':50}\\\"></svg-icon>\r\n					<span class=\\\"mt-3\\\">Up to 70% discount on Shipping</span>\r\n				</a>\r\n			</div>\r\n		</div>\r\n		<div class=\\\"row\\\">\r\n			<div class=\\\"col text-center mt-sm-4\\\">\r\n				<a routerLink=\\\"/\\\" class=\\\"btn btn-lg btn-secondary\\\">Join Now!</a>\r\n			</div>\r\n		</div>','1','Value Added Services',NULL,NULL),(11,'shop_for_me','Shop For Me','<h4 class=\\\"text-white mb-4\\\">Was your card declined by the Store? Our personal shopper service provides you an easy option to shop from stores that\r\n				may decline your card.</h4>\r\n			<ul class=\\\"list-ui text-left mb-4 d-inline-block\\\">\r\n				<li>Log in to your warehouse and click on Personal Shopper.</li>\r\n				<li>Use one of the available options to tell us what you want to order.</li>\r\n				<li>Pay us using your Visa, MasterCard or PayPal.</li>\r\n				<li>We buy, ship and deliver to your warehouse.</li>\r\n				<li>Pay and ship it out to your door like any other purchase.</li>\r\n			</ul>\r\n			<div class=\\\"row\\\">\r\n				<div class=\\\"col\\\">\r\n					<a routerLink=\\\"/join\\\" class=\\\"btn btn-lg btn-secondary\\\">Know More</a>\r\n				</div>\r\n			</div>','1',NULL,NULL,'1531390598_shop-for-me.jpg');
/*!40000 ALTER TABLE `stmd_blockcontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_cities`
--

DROP TABLE IF EXISTS `stmd_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `stateCode` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `countryCode` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `countryCode` (`countryCode`),
  KEY `stateCode` (`stateCode`),
  CONSTRAINT `stmd_cities_ibfk_1` FOREIGN KEY (`countryCode`) REFERENCES `stmd_countries` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=829 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_cities`
--

LOCK TABLES `stmd_cities` WRITE;
/*!40000 ALTER TABLE `stmd_cities` DISABLE KEYS */;
INSERT INTO `stmd_cities` VALUES (2,'Umuahia','AB','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(3,'Mushin','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(4,'Ikeja','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(5,'Agege','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(6,'Isolo','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(7,'Shomolu','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(8,'Surulere','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(9,'Apapa','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(10,'Ikoyi','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(11,'Victoria Island','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(12,'Lekki','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(13,'Houston','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(19,'Gariki','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(20,'Wuse 1','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(21,'Wuse 2','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(22,'Asokoro','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(23,'Maitama','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(24,'Yola','AD','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(25,'Uyo','AK','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(26,'Awka','AN','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(27,'Bauchi','BA','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(28,'Yenagoa','BY','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(29,'Makurdi','BN','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(30,'Maiduguri','BO','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(31,'Calabar','CR','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(33,'Abakaliki','EB','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(34,'Benin City','ED','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(35,'Ado-Ekiti','EK','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(36,'Enugu','EN','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(37,'Gombe','GO','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(38,'Owerri','IM','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(39,'Dutse','JG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(40,'Kaduna','KD','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(41,'Kano','KN','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(42,'Katsina','KT','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(43,'Birnin Kebbi','KB','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(44,'Lokoja','KO','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(45,'Ilorin','KW','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(46,'Lafia','NA','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(47,'Minna','NG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(48,'Abeokuta','OG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(49,'Akure','ON','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(50,'Oshogbo','OS','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(51,'Ibadan','OY','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(52,'Jos','PL','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(53,'Port Harcourt','RV','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(54,'Sokoto','SO','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(55,'Jalingo','TR','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(56,'Damaturu','YO','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(57,'Gusau','ZA','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(58,'Warri','DT','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(61,'Belize (BZE - Airport)','BZE','BZ','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(62,'Little Rock','AR','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(63,'London','LON','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(64,'Dallas','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(65,'New Orleans','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(66,'Cincinnati','OH','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(67,'Baton Rouge','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(68,'Shreveport','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(69,'Metairie','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(70,'Lafayette','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(71,'Lake Charles','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(72,'Kenner','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(73,'Bossier City','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(74,'Monroe','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(75,'Alexandria','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(76,'Marrero','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(77,'New Iberia','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(78,'Houma','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(79,'Laplace','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(80,'Slidell','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(81,'Central','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(82,'Terry Town','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(83,'Opelousas','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(84,'Ruston','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(85,'Hammond','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(86,'Corpus Christi','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(87,'Austin','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(88,'El Paso','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(89,'San Antonio','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(90,'Fort Worth','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(91,'Arlington','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(92,'Plano','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(93,'Garland','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(94,'Lubbock','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(95,'Laredo','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(96,'Irving','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(97,'Amarillo','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(98,'Brownsville','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(99,'Galveston','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(100,'Waco','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(101,'Lufkin','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(102,'Tyler','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(103,'Hildalgo','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(104,'D.C.','DC','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(105,'Seattle','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(106,'Spokane','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(107,'Tacoma','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(108,'Vancouver','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(109,'Bellevue','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(110,'Everetti','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(111,'Federal Way','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(112,'Kent','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(113,'Yakima','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(114,'Bellingham','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(115,'Lakewood','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(116,'Kennewick','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(117,'Shoreline','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(118,'Renton','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(119,'Redmond','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(120,'Kirkland','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(121,'Olympia','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(122,'Auburn','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(123,'Edmonds','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(124,'Bremerton','WA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(125,'Baltimore','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(126,'Columbia','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(127,'Silver Springs','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(128,'Dundalk','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(129,'Wheaton-Glenment','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(130,'Ellicott City','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(131,'German Town','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(132,'Bethesda','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(133,'Frederick','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(134,'Gaithersburg','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(135,'Towson','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(136,'Bowie','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(137,'Aspen Hill','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(138,'Rockville','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(139,'Potomac','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(140,'Catonsville','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(141,'Bel Air South','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(142,'Essex','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(143,'Glen Burnie','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(144,'North Bethesda','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(145,'Minneapolis','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(146,'St. Paul','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(147,'Duluth','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(148,'Rochester','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(149,'Bloomington','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(150,'Brooklyn Park','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(151,'Plymouth','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(152,'Eagan','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(153,'Coon Rapids','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(154,'Burnsville','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(155,'St. Cloud','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(156,'Eden Prairie','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(157,'Minnetonka','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(158,'Maple Grove','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(159,'Edina','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(160,'Woodbury','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(161,'Apple Valley','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(162,'Blaine','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(163,'St. Louis Park','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(164,'Lakeville','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(165,'Los Angeles','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(166,'San Diego','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(167,'San Jose','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(168,'San Fransisco','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(169,'Long Beach','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(170,'Fresno','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(171,'Sacramento','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(172,'Oakland','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(173,'Santa Ana','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(174,'Anaheim','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(175,'Riverside','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(176,'Bakersfield','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(177,'Stockton','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(178,'Fremoni','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(179,'Glendale','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(180,'Hunington Beach','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(181,'Modesto','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(182,'San Bernardino','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(183,'Chula Vista','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(184,'Oxnard','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(185,'Johannesburg ','GAU','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(186,'Onitsha','AN','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(187,'Orlando','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(188,'Alexandria','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(189,'Stafford','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(190,'Virginia Beach','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(191,'Norfolk','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(192,'Chesapeake','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(193,'Richmond','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(194,'Newport News','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(195,'Hampton','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(196,'Portsmouth','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(197,'Roanoake','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(198,'Suffolk','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(199,'Lynchburg','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(200,'Danville','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(201,'Charlotteville','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(202,'Harrisonburg','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(203,'Manassas','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(204,'Petersburg','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(205,'Salem','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(206,'Winchester','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(207,'Staunton','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(208,'Hopewell','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(209,'Jacksonville','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(210,'Miami','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(211,'Tampa','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(212,'St. Pettersburg','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(213,'Hialeah','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(214,'Tallahassee','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(215,'Fort Lauderdale','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(216,'Port Saint Lucie','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(217,'Pembroke Pines','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(218,'Cape Coral','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(219,'Hollywood','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(220,'Gainsville','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(221,'Miramar','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(222,'Coral Springs','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(223,'Clearwater','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(224,'Miami Gardens','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(225,'Palm Bay','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(226,'Panama City','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(227,'Sarasota','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(228,'Nashville','TN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(229,'Gloucester Township','NJ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(230,'Detroit','MI','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(231,'Lansing','MI','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(232,'Atlanta','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(233,'Augusta','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(234,'Savannah','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(235,'Columbus','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(236,'Macon','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(237,'Athens','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(238,'Gainsville','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(239,'Albany','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(240,'Dalton','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(241,'Warner Robins','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(242,'Valdosta','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(243,'Brunswick','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(244,'Rome','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(245,'Hineville','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(246,'Statesboro','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(247,'LaGrange','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(248,'Dublin','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(249,'Mileedgeville','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(250,'Waycross','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(251,'Douglas','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(252,'Albany','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(253,'Binghamton','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(254,'Buffalo','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(255,'Ithaca','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(256,'New York','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(257,'Rochester','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(258,'Niagara Falls','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(259,'Syracuse','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(260,'Yonkers','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(261,'Utica','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(262,'Schenectady','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(263,'Mount Vernon','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(264,'Auburn','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(265,'Jamestown','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(266,'New Rochelle','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(267,'North Tonawanda','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(268,'Poughkeepsie','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(269,'Rome','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(270,'Troy','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(271,'Whilte Plains','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(272,'Paris','PR','FR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(273,'Philadelphia','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(274,'Pittsburg','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(275,'Allentown','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(276,'Erie','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(277,'Reading','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(278,'Scranton','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(279,'Bethlehem','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(280,'Lancaster','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(281,'Altoona','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(282,'Wikes-Barre','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(283,'York','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(284,'State College','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(285,'Upper Darby','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(286,'Bensalem','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(287,'Lower Merion','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(288,'Abington Township','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(289,'Bristol','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(290,'Levittown','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(291,'Millcreek Township','PA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(292,'Columbus','OH','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(293,'Kumasi','AS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(294,'Sunyani','BA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(295,'Cape Coast','CT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(296,'Koforidua','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(297,'Accra','GA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(298,'Tamale','NR','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(299,'Bolgatanga','UE','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(300,'Wa','UW','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(301,'Ho','VT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(302,'Takoradi','WS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(303,'Jefferson','MO','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(304,'Hollywood','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(305,'Port of Spain(AirPort)','PS','TT','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(307,'Liverpool Central','LIV','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(308,'Oklahoma City','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(309,'Tulsa','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(310,'Norman','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(311,'Broken Arrow','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(312,'Midwest City','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(313,'Moore','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(314,'Enid','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(315,'Stillwater','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(316,'Muskogee','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(317,'Bartlesville','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(318,'Shawnee','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(319,'Owasso','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(320,'Ponca City','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(321,'Ardmore','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(322,'Duncan','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(323,'Yukon','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(324,'Del City','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(325,'Sapulpa','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(326,'Lawton','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(327,'Edmond','OK','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(328,'Jackson','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(329,'Gulpori','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(330,'Hattiesburg','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(331,'Southhaven','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(332,'Biloxi','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(333,'Meridian','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(334,'Tupelo','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(335,'Greenville','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(336,'Olive Branch','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(337,'Clinton','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(338,'Vicksburg','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(339,'Horn Lake','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(340,'Pearl','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(341,'Starkville','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(342,'Columbus','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(343,'Pascagoula','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(344,'Brandon','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(345,'Ridgeland','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(346,'Laural','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(347,'Clarksdale','MS','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(348,'Guangzhou','GD','CN','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(350,'Frankfurt','HE','DE','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(351,'Paderborn','NRW','DE','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(352,'Freetown','FR','SL','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(353,'UK (Various Locations)','LIV','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(354,'United Kingdom','UK','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(355,'Inglewood','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(356,'Tuscon','AZ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(358,'Lancaster','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(359,'Mankato','MN','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(360,'Darien','CT','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(361,'Valencia','VA','ES','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(362,'Lubango','HU','AO','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(365,'Kearney','MO','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(367,'Raleigh','NC','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(368,'Dayton','OH','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(369,'Grafton','OH','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(370,'Daytona Beach','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(371,'Chicago','IL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(372,'Providence','RI','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(373,'Fort Collins','CO','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(374,'Ahualoa','HI','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(375,'Broadwater','NE','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(376,'Marietta','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(378,'Aba','AB','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(379,'Bonny','RV','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(380,'Eket','AK','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(381,'Nnewi','AN','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(382,'Zaria','KD','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(435,'Obuasi','AS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(436,'Mampong','AS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(437,'Konongo','AS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(438,'Agogo','AS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(439,'Bekwai','AS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(440,'Techiman','BA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(441,'Berekum','BA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(442,'Winneba','CT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(443,'Elmina','CT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(444,'Dunkwa','CT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(445,'SaltPond','CT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(446,'Nkawkaw','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(447,'Akim - Oda','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(448,'Suhum','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(449,'Nsawam','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(450,'Somanya','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(451,'Aburi','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(452,'Akropong','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(453,'Teshie','GA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(454,'Tema','GA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(455,'Nungua','GA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(456,'Yendi','NR','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(457,'Navorongo','UE','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(458,'Hohoe','VT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(459,'Sekondi ','WS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(460,'Prestea','WS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(461,'Tarkwa','WS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(462,'Axim','WS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(463,'Bibiani','WS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(464,'Ejisu','AS','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(465,'Dormaa-ahenkro','BA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(466,'Goaso','BA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(467,'Nkoranza','BA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(468,'Tepa','BA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(469,'Swedru','CT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(470,'Akosombo','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(471,'Achimota','GA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(472,'Legon','GA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(473,'Juapong','VT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(474,'Labadi','VT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(479,'Port Elizabeth','ES','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(480,'Alexandria','ES','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(481,'Aberdeen','ES','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(482,'St Francis Bay','ES','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(483,'Welkom','FS','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(484,'Edenburg','FS','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(485,'Bloemfontein','FS','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(486,'Winburg','FS','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(487,'Soweto','GAU','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(488,'Tembisa','GAU','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(489,'Mamelodi','GAU','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(490,'Pretoria','GAU','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(491,'Tsakane','GAU','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(492,'Impumelelo','GAU','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(493,'Amajuba','KN','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(494,'Sisonke','KN','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(495,'Ugu','KN','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(496,'Uthukela','KN','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(497,'Pietermaritzburg','KN','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(498,'Ethekwini','KN','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(499,'Durban','KN','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(500,'Lephalale','LP','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(501,'Ellisras ','LP','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(502,'Mookgophong','LP','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(503,'Polokwane','LP','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(504,'Standerton','MP','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(505,'White River','MP','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(506,'Nelspruit','MP','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(507,'Volksrust','MP','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(508,'Rustenburg','NW','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(509,'Lichtenburg','NW','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(510,'Mafikeng','NW','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(511,'Madibeng','NW','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(512,'Springbok','NR','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(513,'Fraserburg','NR','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(514,'Barkly West','NR','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(515,'Kimberley','NR','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(516,'Cape Town','WS','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(517,'Durban Ville','WS','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(518,'Worcester','WS','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(519,'Elandsfontein','WS','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(520,'Oshodi','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(521,'La Spezia','','IT','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(522,'Annapolis','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(523,'Breese','IL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(524,'marl','HE','DE','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(528,'Camp Bastion','HM','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(529,'Camp Leatherneck','HM','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(530,'FOB Dwyer','HM','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(531,'Herat City','HR','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(532,'Shindad Airbase','HR','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(533,'Camp Blackhorse','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(534,'Camp Eggers','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(535,'Camp Julien','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(536,'Camp Phonenix','KB','AF','1','0',0,'0000-00-00 00:00:00',1,'2018-05-11 08:23:39',NULL,NULL),(537,'Camp Warehouse','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(538,'Camp Dubs','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(539,'Camp Invicta','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(540,'Camp Kaia','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(541,'Camp Morehead','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(542,'Camp Souter','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(543,'Kabul','KB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(544,'FOB Camp Wilson','KD','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(545,'Kandahar Airbase','KD','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(546,'FOB Salerno','KH','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(547,'FOB Shank','LG','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(548,'FOB Fenty','NG','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(549,'Jalalabad Airbase','NG','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(550,'Tarin - Kowt','OG','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(551,'Camp Marmal','BK','AF','1','0',0,'0000-00-00 00:00:00',1,'2018-05-11 08:24:43',NULL,NULL),(552,'FOB Sharana','PK','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(553,'Mazaari-I-Shariff','BK','AF','1','0',0,'0000-00-00 00:00:00',1,'2018-05-16 06:57:38',NULL,NULL),(554,'Bargram Airbase','PW','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(555,'Camp Albert','PW','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(556,'Camp Blackjack','PW','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(557,'Camp Bulldog','PW','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(558,'Camp Civillian','PW','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(559,'Camp Cunningham','PW','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(560,'FOB Wolverine','ZB','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(561,'Camp Spann','BK','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(562,'Isaf Camp','HR','AF','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(563,'Plymouth','MI','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(565,'Cleveland','OH','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(566,'Elizabeth','NJ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(567,'San Ferando','PA','PH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(568,'Ocala','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(569,'Domodedovo','Texas','KE','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(571,' Morvant','TG','TT','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(572,'Domodedovo','MS','RU','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(573,'Pasadena','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(574,'San Clemente','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(575,'Marshall','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(576,'Luanda','HU','AO','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(577,'Luanda','LU','AO','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(578,'Newark','NJ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(579,'Milwaukee','WI','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(580,'Aurora','CO','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(581,'LAUREL ','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(582,'Lagos Port - Apapa','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(583,'Woodlands','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(584,'Belmont','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(585,'Pensacola ','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(586,'Milnerton','ES','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(587,'Milnerton','ML','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(588,' Bronx','NY','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(589,'Irvington','NJ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(590,'Weston','FL','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(591,'Burleson','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(592,'Akowonjo','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(593,'Ajegunle','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(594,'Amuwo Odofin','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(595,'Badagry','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(596,'Epe','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(597,'Ifako Ijaiye','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(598,'Ikorodu','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(599,'Ojota','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(600,'Ebutte Metta','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(601,'Ojo','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(602,'Abule Egba','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(603,'Bariga','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(604,'Idimu Egbe','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(605,'Ejigbo','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(606,'Iba','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(607,'Igando/Ikotun','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(608,'Orile','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(609,'Ijaniki','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(610,'Yaba','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(611,'Magodo','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(612,'Waxhaw','NC','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(613,'Utako','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(614,'Houston (Walk In Customers Only)','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(616,'Isolo Pick Up Center','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(618,'Houston (DO)','TX','UM','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(619,'Plainfield','NJ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(620,'Abita Springs','LA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(621,'Kenilworth','NJ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(622,'East Brunswick','NJ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(623,'Ketu','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(624,'Ijegun','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(625,'Orange','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(626,'Ijebu Ode','OG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(627,'Hyattsville','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(628,'Ngor -Okpala','IM','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(629,'Keffi','NA','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(630,'<script></script>','HE','DE','1','0',0,'0000-00-00 00:00:00',1,'2018-07-24 06:24:41',NULL,NULL),(631,'Ilesha','OS','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(632,'Sango Otta','OG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(633,'Cheverly','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(634,'Gwawalada ','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(635,'Kubwa ','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(636,'Sapele','DT','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(637,'Mowe','OG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(638,'Akoko-Edo','ED','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(639,' Lagos Island','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(640,'Krobo','ES','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(641,'Ipaja','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(642,'Ikot Epene','AK','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(643,'Zuru','KB','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(644,'King Williams Town','ES','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(645,'Nairobi','','KE','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(646,'Awoyaya','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(647,'Corona','CA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(648,'London','ENG','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(649,'Liverpool','ENG','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(650,'Leeds','ENG','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(651,'Manchester','ENG','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(652,'Shefielled','ENG','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(653,'Lisburn','NTHLN','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(654,'Glasglow','SCOT','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(655,'Aberdeen','SCOT','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(656,'Cardiff','WHL','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(657,'Swansea','WHL','GB','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(658,'Gwarinpa','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(659,'Gbagada','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(660,'Snellville','GA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(661,'Ifo','OG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(662,'Festac','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(663,'College Station','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(664,'Arlington','VA','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(665,'Bacoor','CA','PH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(666,'Union City','NJ','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(667,' Charlotte','NC','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(668,'Isolo Pick Up Center','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(669,'Aba','AB','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(670,'Umuahia','AB','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(672,'Asokoro','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(673,'Gariki','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(674,'Gwarinpa','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(675,'Gwawalada ','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(676,'Kubwa ','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(677,'Maitama','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(678,'Utako','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(679,'Wuse 1','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(680,'Wuse 2','ABJ','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(681,'Yola','AD','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(682,'Eket','AK','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(683,'Ikot Epene','AK','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(684,'Uyo','AK','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(685,'Awka','AN','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(686,'Nnewi','AN','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(687,'Onitsha','AN','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(688,'Bauchi','BA','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(689,'Yenagoa','BY','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(690,'Makurdi','BN','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(691,'Maiduguri','BO','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(692,'Calabar','CR','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(693,'Asaba','DT','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(694,'Asaba','DT','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(695,'Sapele','DT','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(696,'Warri','DT','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(698,'Abakaliki','EB','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(700,'Benin City','ED','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(701,'Akoko-Edo','ED','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(702,'Ado-Ekiti','EK','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(703,'Enugu','EN','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(705,'Gombe','GO','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(706,'Ngor -Okpala','IM','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(707,'Owerri','IM','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(708,'Dutse','JG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(710,'Kaduna','KD','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(711,'Zaria','KD','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(712,'Kano','KN','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(713,'Katsina','KT','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(714,'Birnin Kebbi','KB','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(715,'Zuru','KB','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(716,'Lokoja','KO','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(718,' Lagos Island','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(719,'Abule Egba','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(720,'Agege','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(721,'Ajegunle','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(722,'Akowonjo','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(723,'Amuwo Odofin','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(724,'Apapa','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(725,'Awoyaya','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(726,'Badagry','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(727,'Bariga','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(728,'Ebutte Metta','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(729,'Ejigbo','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(730,'Epe','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(731,'Festac','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(732,'Gbagada','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(733,'Iba','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(734,'Idimu Egbe','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(735,'Ifako Ijaiye','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(736,'Igando/Ikotun','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(737,'Ijaniki','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(738,'Ijegun','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(739,'Ikeja','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(740,'Ikorodu','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(741,'Ikoyi','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(742,'Ikoyi Pick Up Center','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(743,'Ipaja','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(744,'Isolo','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(745,'Ketu','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(746,'Lagos Port - Apapa','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(747,'Lekki','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(748,'Magodo','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(749,'Mushin','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(750,'Ojo','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(751,'Ojota','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(752,'Orile','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(753,'Oshodi','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(754,'Shomolu','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(755,'Surulere','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(756,'Victoria Island','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(757,'Yaba','LG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(758,'Keffi','NA','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(759,'Lafia','NA','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(760,'Minna','NG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(761,'Abeokuta','OG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(762,'Ifo','OG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(763,'Ijebu Ode','OG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(764,'Mowe','OG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(765,'Sango Otta','OG','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(766,'Akure','ON','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(767,'Oshogbo','OS','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(768,'Ilesha','OS','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(769,'Ibadan','OY','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(770,'Jos','PL','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(771,'Port Harcourt','RV','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(772,'Bonny','RV','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(773,'Sokoto','SO','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(774,'Jalingo','TR','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(775,'Damaturu','YO','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(776,'Gusau','ZA','NR','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(777,'Waldorf','MD','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(778,'Hudson','WI','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(779,'Klerksdorp','NW','ZA','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(780,'Logokoma','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(781,'Terrell','TX','US','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(782,'Pangasinan','CA','PH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(783,'Abak','AK','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(784,'Lagos Office','LG','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(786,'Accra','ACC','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(787,'Douala AirPort (DLA)','DLA','CM','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(788,'Brazzaville AirPort (BZV)','BZV','CG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(790,'Sunyani','BRA','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(791,'Cape Coast','CEN','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(792,'Koforidua','EST','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(793,'Tamale','NRT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(796,'Kumasi','ASH','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(797,'Bolgatanga','UET','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(798,'Wa','UWT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(799,'Ho','VLT','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(800,'Takoradi','WST','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(801,'Abuja','ABJ','NG','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(802,'Accra Office','ACC','GH','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(803,'Test','HM','AF','1','0',1,'2018-05-11 08:24:25',NULL,NULL,NULL,NULL),(804,'ee','BK','AF','0','1',1,'2018-05-16 06:57:07',1,NULL,1,NULL),(826,'Kolkata','WB','YY','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(827,'Burdawan','WB','YY','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(828,'Test','WB','YY','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_contactus`
--

DROP TABLE IF EXISTS `stmd_contactus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_contactus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `alternateAddress` varchar(255) DEFAULT NULL,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `zip` int(11) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `status` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT '1 for submitted, 2 for viewes,3 for replied  ',
  `postedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `repliedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_contactus`
--

LOCK TABLES `stmd_contactus` WRITE;
/*!40000 ALTER TABLE `stmd_contactus` DISABLE KEYS */;
INSERT INTO `stmd_contactus` VALUES (1,'somnath pahari','INTY','test address','test alternate',76,153,649,NULL,'123456','testtinguser@gmail.com','7894654','google','test sub','test message','3','2018-07-30 16:19:49','2018-09-13 08:41:33'),(2,'prasenjit banik','INTT','test address',NULL,230,92,174,NULL,'123456','prasenjit@gmail.com','123456',NULL,'Confuse about how to place an order','Please guide me how to order. I am getting confused','2','2018-07-30 16:47:16',NULL),(6,'prasenjit banik','INTT','test address',NULL,230,92,174,NULL,'123456','prasenjit@gmail.com','123456',NULL,'Confuse about how to place an order','Please guide me how to order. I am getting confused','2','2018-07-30 16:55:07',NULL),(7,'Abinash Mahapatra','test','test address',NULL,230,98,471,NULL,'123456','abinash@gmail.com',NULL,NULL,'help me out with the application','help me out with the application help me out with the application','2','2018-07-30 16:57:11','2018-09-13 06:37:06');
/*!40000 ALTER TABLE `stmd_contactus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_counties`
--

DROP TABLE IF EXISTS `stmd_counties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_counties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stateId` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `stateId` (`stateId`),
  CONSTRAINT `stmd_counties_ibfk_1` FOREIGN KEY (`stateId`) REFERENCES `stmd_states` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_counties`
--

LOCK TABLES `stmd_counties` WRITE;
/*!40000 ALTER TABLE `stmd_counties` DISABLE KEYS */;
INSERT INTO `stmd_counties` VALUES (1,258,'Test','1','0',1,'2018-05-11 07:56:49',NULL,NULL,NULL,NULL),(2,6,'Test','1','0',1,'2018-05-11 08:00:08',NULL,NULL,NULL,NULL),(3,198,'Test','1','0',1,'2018-05-11 08:12:04',1,'2018-07-24 06:00:14',NULL,NULL),(4,198,'Test2','1','0',1,'2018-05-11 08:14:08',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_counties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_countries`
--

DROP TABLE IF EXISTS `stmd_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `codeA3` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `codeN3` int(4) NOT NULL DEFAULT '0',
  `region` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` enum('1','0') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `priority` int(5) NOT NULL DEFAULT '0',
  `modifiedOn` datetime DEFAULT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_countries`
--

LOCK TABLES `stmd_countries` WRITE;
/*!40000 ALTER TABLE `stmd_countries` DISABLE KEYS */;
INSERT INTO `stmd_countries` VALUES (1,'AA','AND',20,'EU','Andorras','0',0,'2018-09-18 13:47:59',1),(2,'AE','ARE',784,'AS','United Arab Emirates','0',0,'2018-09-18 13:47:49',1),(3,'AF','AFG',4,'AS','Afghanistan','0',0,'2018-09-18 13:47:55',1),(4,'AG','ATG',28,'LA','Antigua and Barbuda','1',0,NULL,NULL),(5,'AI','AIA',660,'LA','Anguilla','1',0,NULL,NULL),(6,'AL','ALB',8,'EU','Albania','1',0,NULL,NULL),(7,'AM','ARM',51,'AS','Armenia','1',0,NULL,NULL),(8,'AN','ANT',530,'LA','Netherlands Antilles','1',0,NULL,NULL),(9,'AO','AGO',24,'AF','Angola','1',0,NULL,NULL),(10,'AQ','ATA',10,'AN','Antarctica','1',0,NULL,NULL),(11,'AR','ARG',32,'LA','Argentina','1',0,NULL,NULL),(12,'AS','ASM',16,'AU','American Samoa','1',0,NULL,NULL),(13,'AT','AUT',40,'EU','Austria','1',0,NULL,NULL),(14,'AU','AUS',36,'AU','Australia','1',0,NULL,NULL),(15,'AW','ABW',533,'LA','Aruba','1',0,NULL,NULL),(16,'AX','ALA',248,'EU','Aland Islands','1',0,NULL,NULL),(17,'AZ','AZE',31,'AS','Azerbaijan','1',0,NULL,NULL),(18,'BA','BIH',70,'EU','Bosnia and Herzegovina','1',0,NULL,NULL),(19,'BB','BRB',52,'LA','Barbados','1',0,NULL,NULL),(20,'BD','BGD',50,'AS','Bangladesh','1',0,NULL,NULL),(21,'BE','BEL',56,'EU','Belgium','1',0,NULL,NULL),(22,'BF','BFA',854,'AF','Burkina Faso','1',0,NULL,NULL),(23,'BG','BGR',100,'EU','Bulgaria','1',0,NULL,NULL),(24,'BH','BHR',48,'AS','Bahrain','1',0,NULL,NULL),(25,'BI','BDI',108,'AF','Burundi','1',0,NULL,NULL),(26,'BJ','BEN',204,'AF','Benin','1',0,NULL,NULL),(27,'BL','BLM',652,'NA','St. Barthelemy','1',0,'2018-07-13 07:16:30',1),(28,'BM','BMU',60,'LA','Bermuda','1',0,NULL,NULL),(29,'BN','BRN',96,'AS','Brunei Darussalam','1',0,NULL,NULL),(30,'BO','BOL',68,'LA','Bolivia','1',0,NULL,NULL),(31,'BR','BRA',76,'LA','Brazil','1',0,NULL,NULL),(32,'BS','BHS',44,'LA','Bahamas','1',0,NULL,NULL),(33,'BT','BTN',64,'AS','Bhutan','1',0,NULL,NULL),(34,'BV','BVT',74,'AN','Bouvet Island','1',0,NULL,NULL),(35,'BW','BWA',72,'AF','Botswana','1',0,NULL,NULL),(36,'BY','BLR',112,'EU','Belarus','0',0,NULL,NULL),(37,'BZ','BLZ',84,'LA','Belize','0',0,NULL,NULL),(38,'CA','CAN',124,'NA','Canada','1',0,NULL,NULL),(39,'CC','CCK',166,'AU','Cocos (Keeling) Islands','1',0,NULL,NULL),(40,'CD','COD',180,'AF','Democratic Republic of the Congo','1',0,NULL,NULL),(41,'CF','CAF',140,'AF','Central African Republic','0',0,NULL,NULL),(42,'CG','COG',178,'AF','Congo','1',0,NULL,NULL),(43,'CH','CHE',756,'EU','Switzerland','1',0,NULL,NULL),(44,'CI','CIV',384,'AF','Cote D\'ivoire','0',0,NULL,NULL),(45,'CK','COK',184,'AU','Cook Islands','1',0,NULL,NULL),(46,'CL','CHL',152,'LA','Chile','1',0,NULL,NULL),(47,'CM','CMR',120,'AF','Cameroon','1',0,NULL,NULL),(48,'CN','CHN',156,'AS','China','1',0,NULL,NULL),(49,'CO','COL',170,'LA','Colombia','1',0,NULL,NULL),(50,'CR','CRI',188,'LA','Costa Rica','1',0,NULL,NULL),(51,'CU','CUB',192,'LA','Cuba','0',0,NULL,NULL),(52,'CV','CPV',132,'AF','Cape Verde','1',0,NULL,NULL),(53,'CX','CXR',162,'AU','Christmas Island','1',0,NULL,NULL),(54,'CY','CYP',196,'EU','Cyprus','1',0,NULL,NULL),(55,'CZ','CZE',203,'EU','Czech Republic','1',0,NULL,NULL),(56,'DE','DEU',276,'EU','Germany','1',0,NULL,NULL),(57,'DJ','DJI',262,'AF','Djibouti','1',0,NULL,NULL),(58,'DK','DNK',208,'EU','Denmark','1',0,NULL,NULL),(59,'DM','DMA',212,'LA','Dominica','1',0,NULL,NULL),(60,'DO','DOM',214,'LA','Dominican Republic','1',0,NULL,NULL),(61,'DZ','DZA',12,'AF','Algeria','1',0,NULL,NULL),(62,'EC','ECU',218,'LA','Ecuador','1',0,NULL,NULL),(63,'EE','EST',233,'EU','Estonia','1',0,NULL,NULL),(64,'EG','EGY',818,'AF','Egypt','1',0,NULL,NULL),(65,'EH','ESH',732,'AF','Western Sahara','1',0,NULL,NULL),(66,'ER','ERI',232,'AF','Eritrea','1',0,NULL,NULL),(67,'ES','ESP',724,'EU','Spain','1',0,NULL,NULL),(68,'ET','ETH',231,'AF','Ethiopia','1',0,NULL,NULL),(69,'FI','FIN',246,'EU','Finland','1',0,NULL,NULL),(70,'FJ','FJI',242,'AU','Fiji','1',0,NULL,NULL),(71,'FK','FLK',238,'LA','Falkland Islands (Malvinas)','1',0,NULL,NULL),(72,'FM','FSM',583,'AU','Micronesia','1',0,NULL,NULL),(73,'FO','FRO',234,'EU','Faroe Islands','1',0,NULL,NULL),(74,'FR','FRA',250,'EU','France','1',0,NULL,NULL),(75,'GA','GAB',266,'AF','Gabon','1',0,NULL,NULL),(76,'GB','GBR',826,'EU','United Kingdom','1',1,NULL,NULL),(77,'GD','GRD',308,'LA','Grenada','1',0,NULL,NULL),(78,'GE','GEO',268,'AS','Georgia','1',0,NULL,NULL),(79,'GF','GUF',254,'LA','French Guiana','1',0,NULL,NULL),(80,'GG','GGY',831,'EU','Guernsey','1',0,NULL,NULL),(81,'GH','GHA',288,'AF','Ghana','1',0,NULL,NULL),(82,'GI','GIB',292,'EU','Gibraltar','1',0,NULL,NULL),(83,'GL','GRL',304,'NA','Greenland','1',0,NULL,NULL),(84,'GM','GMB',270,'AF','Gambia','1',0,NULL,NULL),(85,'GN','GIN',324,'AF','Guinea','1',0,NULL,NULL),(86,'GP','GLP',312,'LA','Guadeloupe','1',0,NULL,NULL),(87,'GQ','GNQ',226,'AF','Equatorial Guinea','1',0,NULL,NULL),(88,'GR','GRC',300,'EU','Greece','1',0,NULL,NULL),(89,'GS','SGS',239,'AN','South Georgia and the South Sandwich Islands','1',0,NULL,NULL),(90,'GT','GTM',320,'LA','Guatemala','1',0,NULL,NULL),(91,'GU','GUM',316,'AU','Guam','1',0,NULL,NULL),(92,'GW','GNB',624,'AF','Guinea-Bissau','1',0,NULL,NULL),(93,'GY','GUY',328,'LA','Guyana','1',0,NULL,NULL),(94,'HK','HKG',344,'AS','Hong Kong','1',0,NULL,NULL),(95,'HM','HMD',334,'AU','Heard and McDonald Islands','1',0,NULL,NULL),(96,'HN','HND',340,'LA','Honduras','1',0,NULL,NULL),(97,'HR','HRV',191,'EU','Croatia','1',0,NULL,NULL),(98,'HT','HTI',332,'LA','Haiti','1',0,NULL,NULL),(99,'HU','HUN',348,'EU','Hungary','1',0,NULL,NULL),(100,'ID','IDN',360,'AS','Indonesia','0',0,NULL,NULL),(101,'IE','IRL',372,'EU','Ireland','1',0,NULL,NULL),(102,'IL','ISR',376,'AS','Israel','1',0,NULL,NULL),(103,'IM','IMN',833,'EU','Isle of Man','1',0,NULL,NULL),(104,'IN','IND',356,'AS','India','1',0,NULL,NULL),(105,'IO','IOT',86,'AS','British Indian Ocean Territory','1',0,NULL,NULL),(106,'IQ','IRQ',368,'AS','Iraq','0',0,NULL,NULL),(107,'IR','IRN',364,'AS','Islamic Republic of Iran','1',0,NULL,NULL),(108,'IS','ISL',352,'EU','Iceland','1',0,NULL,NULL),(109,'IT','ITA',380,'EU','Italy','1',0,NULL,NULL),(110,'JE','JEY',832,'EU','Jersey','1',0,NULL,NULL),(111,'JM','JAM',388,'LA','Jamaica','1',0,NULL,NULL),(112,'JO','JOR',400,'AS','Jordan','1',0,NULL,NULL),(113,'JP','JPN',392,'AS','Japan','1',0,NULL,NULL),(114,'KE','KEN',404,'AF','Kenya','1',0,NULL,NULL),(115,'KG','KGZ',417,'AS','Kyrgyzstan','0',0,NULL,NULL),(116,'KH','KHM',116,'AS','Cambodia','1',0,NULL,NULL),(117,'KI','KIR',296,'AU','Kiribati','1',0,NULL,NULL),(118,'KM','COM',174,'AF','Comoros','1',0,NULL,NULL),(119,'KN','KNA',659,'LA','St. Kitts and Nevis','1',0,NULL,NULL),(120,'KP','PRK',408,'AS','South Korea','1',0,NULL,NULL),(121,'KR','KOR',410,'AS','Korea, Republic of','0',0,NULL,NULL),(122,'KW','KWT',414,'AS','Kuwait','1',0,NULL,NULL),(123,'KY','CYM',136,'LA','Cayman Islands','1',0,NULL,NULL),(124,'KZ','KAZ',398,'AS','Kazakhstan','1',0,NULL,NULL),(125,'LA','LAO',418,'AS','Laos','1',0,NULL,NULL),(126,'LB','LBN',422,'AS','Lebanon','1',0,NULL,NULL),(127,'LC','LCA',662,'LA','St. Lucia','1',0,NULL,NULL),(128,'LI','LIE',438,'EU','Liechtenstein','1',0,NULL,NULL),(129,'LK','LKA',144,'AS','Sri Lanka','1',0,NULL,NULL),(130,'LR','LBR',430,'AF','Liberia','1',0,NULL,NULL),(131,'LS','LSO',426,'AF','Lesotho','1',0,NULL,NULL),(132,'LT','LTU',440,'EU','Lithuania','1',0,NULL,NULL),(133,'LU','LUX',442,'EU','Luxembourg','1',0,NULL,NULL),(134,'LV','LVA',428,'EU','Latvia','1',0,NULL,NULL),(135,'LY','LBY',434,'AF','Libyan Arab Jamahiriya','1',0,NULL,NULL),(136,'MA','MAR',504,'AF','Morocco','1',0,NULL,NULL),(137,'MC','MCO',492,'EU','Monaco','1',0,NULL,NULL),(138,'MD','MDA',498,'EU','Moldova, Republic of','1',0,NULL,NULL),(139,'ME','MNE',499,'EU','Montenegro','1',0,NULL,NULL),(140,'MF','MAF',663,'NA','St. Martin','0',0,'2018-09-18 13:48:36',1),(141,'MG','MDG',450,'AF','Madagascar','1',0,NULL,NULL),(142,'MH','MHL',584,'AU','Marshall Islands','1',0,NULL,NULL),(143,'MK','MKD',807,'EU','Macedonia','1',0,NULL,NULL),(144,'ML','MLI',466,'AF','Mali','0',0,NULL,NULL),(145,'MM','MMR',104,'AS','Myanmar','0',0,NULL,NULL),(146,'MN','MNG',496,'AS','Mongolia','1',0,NULL,NULL),(147,'MO','MAC',446,'AS','Macau','1',0,NULL,NULL),(148,'MP','MNP',580,'AU','Northern Mariana Islands','1',0,NULL,NULL),(149,'MQ','MTQ',474,'LA','Martinique','1',0,NULL,NULL),(150,'MR','MRT',478,'AF','Mauritania','1',0,NULL,NULL),(151,'MS','MSR',500,'LA','Montserrat','1',0,NULL,NULL),(152,'MT','MLT',470,'EU','Malta','1',0,NULL,NULL),(153,'MU','MUS',480,'AF','Mauritius','1',0,NULL,NULL),(154,'MV','MDV',462,'AS','Maldives','1',0,NULL,NULL),(155,'MW','MWI',454,'AF','Malawi','1',0,NULL,NULL),(156,'MX','MEX',484,'LA','Mexico','1',0,NULL,NULL),(157,'MY','MYS',458,'AS','Malaysia','1',0,NULL,NULL),(158,'MZ','MOZ',508,'AF','Mozambique','1',0,NULL,NULL),(159,'NA','NAM',516,'AF','Namibia','1',0,NULL,NULL),(160,'NC','NCL',540,'AU','New Caledonia','1',0,NULL,NULL),(161,'NE','NER',562,'AF','Niger','1',0,NULL,NULL),(162,'NF','NFK',574,'AU','Norfolk Island','1',0,NULL,NULL),(163,'NG','NGA',566,'AF','Nigeria','1',2,NULL,NULL),(164,'NI','NIC',558,'LA','Nicaragua','1',0,NULL,NULL),(165,'NL','NLD',528,'EU','Netherlands','1',0,NULL,NULL),(166,'NO','NOR',578,'EU','Norway','1',0,NULL,NULL),(167,'NP','NPL',524,'AS','Nepal','1',0,NULL,NULL),(168,'NR','NRU',520,'AU','Nigeria (DO)','0',0,NULL,NULL),(169,'NU','NIU',570,'AU','Niue','1',0,NULL,NULL),(170,'NZ','NZL',554,'AU','New Zealand','1',0,NULL,NULL),(171,'OM','OMN',512,'AS','Oman','1',0,NULL,NULL),(172,'PA','PAN',591,'LA','Panama','1',0,NULL,NULL),(173,'PE','PER',604,'LA','Peru','1',0,NULL,NULL),(174,'PF','PYF',258,'AU','French Polynesia','1',0,NULL,NULL),(175,'PG','PNG',598,'AS','Papua New Guinea','1',0,NULL,NULL),(176,'PH','PHL',608,'AS','Philippines','1',0,NULL,NULL),(177,'PK','PAK',586,'AS','Pakistan','1',0,NULL,NULL),(178,'PL','POL',616,'EU','Poland','1',0,NULL,NULL),(179,'PM','SPM',666,'NA','St. Pierre and Miquelon','0',0,'2018-09-18 13:48:31',1),(180,'PN','PCN',612,'AU','Pitcairn','1',0,NULL,NULL),(181,'PR','PRI',630,'LA','Puerto Rico','1',0,NULL,NULL),(182,'PS','PSE',275,'AS','Palestinian Territory','0',0,NULL,NULL),(183,'PT','PRT',620,'EU','Portugal','1',0,NULL,NULL),(184,'PW','PLW',585,'AU','Palau','1',0,NULL,NULL),(185,'PY','PRY',600,'LA','Paraguay','1',0,NULL,NULL),(186,'QA','QAT',634,'AS','Qatar','1',0,NULL,NULL),(187,'RE','REU',638,'AF','Reunion','1',0,NULL,NULL),(188,'RO','ROU',642,'EU','Romania','1',0,NULL,NULL),(189,'RS','SRB',688,'EU','Serbia','1',0,NULL,NULL),(190,'RU','RUS',643,'EU','Russian Federation','1',0,NULL,NULL),(191,'RW','RWA',646,'AF','Rwanda','1',0,NULL,NULL),(192,'SA','SAU',682,'AS','Saudi Arabia','1',0,NULL,NULL),(193,'SB','SLB',90,'AU','Solomon Islands','1',0,NULL,NULL),(194,'SC','SYC',690,'AF','Seychelles','1',0,NULL,NULL),(195,'SD','SDN',736,'AF','Sudan','0',0,NULL,NULL),(196,'SE','SWE',752,'EU','Sweden','1',0,NULL,NULL),(197,'SG','SGP',702,'AS','Singapore','1',0,NULL,NULL),(198,'SH','SHN',654,'AF','St. Helena','1',0,NULL,NULL),(199,'SI','SVN',705,'EU','Slovenia','1',0,NULL,NULL),(200,'SJ','SJM',744,'EU','Svalbard and Jan Mayen Islands','1',0,NULL,NULL),(201,'SK','SVK',703,'EU','Slovakia','1',0,NULL,NULL),(202,'SL','SLE',694,'AF','Sierra Leone','1',0,NULL,NULL),(203,'SM','SMR',674,'EU','San Marino','1',0,NULL,NULL),(204,'SN','SEN',686,'AF','Senegal','1',0,NULL,NULL),(205,'SO','SOM',706,'AF','Somalia','0',0,NULL,NULL),(206,'SR','SUR',740,'LA','Suriname','1',0,NULL,NULL),(207,'ST','STP',678,'AF','Sao Tome and Principe','1',0,NULL,NULL),(208,'SV','SLV',222,'LA','El Salvador','1',0,NULL,NULL),(209,'SY','SYR',760,'AS','Syrian Arab Republic','0',0,NULL,NULL),(210,'SZ','SWZ',748,'AF','Swaziland','1',0,NULL,NULL),(211,'TC','TCA',796,'LA','Turks and Caicos Islands','1',0,NULL,NULL),(212,'TD','TCD',148,'AF','Chad','1',0,NULL,NULL),(213,'TF','ATF',260,'AN','French Southern Territories','1',0,NULL,NULL),(214,'TG','TGO',768,'AF','Togo','1',0,NULL,NULL),(215,'TH','THA',764,'AS','Thailand','1',0,NULL,NULL),(216,'TJ','TJK',762,'AS','Tajikistan','1',0,NULL,NULL),(217,'TK','TKL',772,'AU','Tokelau','1',0,NULL,NULL),(218,'TL','TLS',626,'AS','Timor-Leste','1',0,NULL,NULL),(219,'TM','TKM',795,'AS','Turkmenistan','1',0,NULL,NULL),(220,'TN','TUN',788,'AF','Tunisia','1',0,NULL,NULL),(221,'TO','TON',776,'AU','Tonga','1',0,NULL,NULL),(222,'TR','TUR',792,'EU','Turkey','1',0,NULL,NULL),(223,'TT','TTO',780,'LA','Trinidad and Tobago','1',0,NULL,NULL),(224,'TV','TUV',798,'AU','Tuvalu','1',0,NULL,NULL),(225,'TW','TWN',158,'AS','Taiwan','0',0,NULL,NULL),(226,'TZ','TZA',834,'AF','Tanzania, United Republic of','1',0,NULL,NULL),(227,'UA','UKR',804,'EU','Ukraine','0',0,NULL,NULL),(228,'UG','UGA',800,'AF','Uganda','1',0,NULL,NULL),(229,'UM','UMI',581,'AU','United States (DO)','1',0,NULL,NULL),(230,'US','USA',840,'NA','United States','1',3,NULL,NULL),(231,'UY','URY',858,'LA','Uruguay','1',0,NULL,NULL),(232,'UZ','UZB',860,'AS','Uzbekistan','1',0,NULL,NULL),(233,'VA','VAT',336,'EU','Vatican City State','1',0,NULL,NULL),(234,'VC','VCT',670,'LA','St. Vincent and the Grenadines','1',0,NULL,NULL),(235,'VE','VEN',862,'LA','Venezuela','0',0,NULL,NULL),(236,'VG','VGB',92,'LA','British Virgin Islands','1',0,NULL,NULL),(237,'VI','VIR',850,'LA','United States Virgin Islands','1',0,NULL,NULL),(238,'VN','VNM',704,'AS','Vietnam','1',0,NULL,NULL),(239,'VU','VUT',548,'AU','Vanuatu','1',0,NULL,NULL),(240,'WF','WLF',876,'AU','Wallis And Futuna Islands','1',0,NULL,NULL),(241,'WS','WSM',882,'AU','Samoa','1',0,NULL,NULL),(242,'YE','YEM',887,'AS','Yemen','0',0,NULL,NULL),(243,'YT','MYT',175,'AF','Mayotte','0',0,'2018-09-18 13:48:17',1),(244,'ZA','ZAF',710,'AF','South Africa','1',0,NULL,NULL),(245,'ZM','ZMB',894,'AF','Zambia','0',0,'2018-09-18 13:48:12',1),(246,'ZW','ZWE',716,'AF','Zimbabwe','0',0,NULL,NULL),(261,'YY','AND',356,'AS','India','1',0,NULL,NULL);
/*!40000 ALTER TABLE `stmd_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_countryphones`
--

DROP TABLE IF EXISTS `stmd_countryphones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_countryphones` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(64) NOT NULL DEFAULT '',
  `countries_iso_code` varchar(2) NOT NULL,
  `countries_isd_code` varchar(7) DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  PRIMARY KEY (`countries_id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_countryphones`
--

LOCK TABLES `stmd_countryphones` WRITE;
/*!40000 ALTER TABLE `stmd_countryphones` DISABLE KEYS */;
INSERT INTO `stmd_countryphones` VALUES (1,'Afghanistan','AF','93',0),(2,'Albania','AL','355',0),(3,'Algeria','DZ','213',0),(4,'American Samoa','AS','1-684',0),(5,'Andorra','AD','376',0),(6,'Angola','AO','244',0),(7,'Anguilla','AI','1-264',0),(8,'Antarctica','AQ','672',0),(9,'Antigua and Barbuda','AG','1-268',0),(10,'Argentina','AR','54',0),(11,'Armenia','AM','374',0),(12,'Aruba','AW','297',0),(13,'Australia','AU','61',0),(14,'Austria','AT','43',0),(15,'Azerbaijan','AZ','994',0),(16,'Bahamas','BS','1-242',0),(17,'Bahrain','BH','973',0),(18,'Bangladesh','BD','880',0),(19,'Barbados','BB','1-246',0),(20,'Belarus','BY','375',0),(21,'Belgium','BE','32',0),(22,'Belize','BZ','501',0),(23,'Benin','BJ','229',0),(24,'Bermuda','BM','1-441',0),(25,'Bhutan','BT','975',0),(26,'Bolivia','BO','591',0),(27,'Bosnia and Herzegowina','BA','387',0),(28,'Botswana','BW','267',0),(29,'Bouvet Island','BV','47',0),(30,'Brazil','BR','55',0),(31,'British Indian Ocean Territory','IO','246',0),(32,'Brunei Darussalam','BN','673',0),(33,'Bulgaria','BG','359',0),(34,'Burkina Faso','BF','226',0),(35,'Burundi','BI','257',0),(36,'Cambodia','KH','855',0),(37,'Cameroon','CM','237',0),(38,'Canada','CA','1',0),(39,'Cape Verde','CV','238',0),(40,'Cayman Islands','KY','1-345',0),(41,'Central African Republic','CF','236',0),(42,'Chad','TD','235',0),(43,'Chile','CL','56',0),(44,'China','CN','86',0),(45,'Christmas Island','CX','61',0),(46,'Cocos (Keeling) Islands','CC','61',0),(47,'Colombia','CO','57',0),(48,'Comoros','KM','269',0),(49,'Congo Democratic Republic of','CG','242',0),(50,'Cook Islands','CK','682',0),(51,'Costa Rica','CR','506',0),(52,'Cote D\'Ivoire','CI','225',0),(53,'Croatia','HR','385',0),(54,'Cuba','CU','53',0),(55,'Cyprus','CY','357',0),(56,'Czech Republic','CZ','420',0),(57,'Denmark','DK','45',0),(58,'Djibouti','DJ','253',0),(59,'Dominica','DM','1-767',0),(60,'Dominican Republic','DO','1-809',0),(61,'Timor-Leste','TL','670',0),(62,'Ecuador','EC','593',0),(63,'Egypt','EG','20',0),(64,'El Salvador','SV','503',0),(65,'Equatorial Guinea','GQ','240',0),(66,'Eritrea','ER','291',0),(67,'Estonia','EE','372',0),(68,'Ethiopia','ET','251',0),(69,'Falkland Islands (Malvinas)','FK','500',0),(70,'Faroe Islands','FO','298',0),(71,'Fiji','FJ','679',0),(72,'Finland','FI','358',0),(73,'France','FR','33',0),(75,'French Guiana','GF','594',0),(76,'French Polynesia','PF','689',0),(77,'French Southern Territories','TF',NULL,0),(78,'Gabon','GA','241',0),(79,'Gambia','GM','220',0),(80,'Georgia','GE','995',0),(81,'Germany','DE','49',0),(82,'Ghana','GH','233',0),(83,'Gibraltar','GI','350',0),(84,'Greece','GR','30',0),(85,'Greenland','GL','299',0),(86,'Grenada','GD','1-473',0),(87,'Guadeloupe','GP','590',0),(88,'Guam','GU','1-671',0),(89,'Guatemala','GT','502',0),(90,'Guinea','GN','224',0),(91,'Guinea-bissau','GW','245',0),(92,'Guyana','GY','592',0),(93,'Haiti','HT','509',0),(94,'Heard Island and McDonald Islands','HM','011',0),(95,'Honduras','HN','504',0),(96,'Hong Kong','HK','852',0),(97,'Hungary','HU','36',0),(98,'Iceland','IS','354',0),(99,'India','IN','91',0),(100,'Indonesia','ID','62',0),(101,'Iran (Islamic Republic of)','IR','98',0),(102,'Iraq','IQ','964',0),(103,'Ireland','IE','353',0),(104,'Israel','IL','972',0),(105,'Italy','IT','39',0),(106,'Jamaica','JM','1-876',0),(107,'Japan','JP','81',0),(108,'Jordan','JO','962',0),(109,'Kazakhstan','KZ','7',0),(110,'Kenya','KE','254',0),(111,'Kiribati','KI','686',0),(112,'Korea, Democratic People\'s Republic of','KP','850',0),(113,'South Korea','KR','82',0),(114,'Kuwait','KW','965',0),(115,'Kyrgyzstan','KG','996',0),(116,'Lao People\'s Democratic Republic','LA','856',0),(117,'Latvia','LV','371',0),(118,'Lebanon','LB','961',0),(119,'Lesotho','LS','266',0),(120,'Liberia','LR','231',0),(121,'Libya','LY','218',0),(122,'Liechtenstein','LI','423',0),(123,'Lithuania','LT','370',0),(124,'Luxembourg','LU','352',0),(125,'Macao','MO','853',0),(126,'Macedonia, The Former Yugoslav Republic of','MK','389',0),(127,'Madagascar','MG','261',0),(128,'Malawi','MW','265',0),(129,'Malaysia','MY','60',0),(130,'Maldives','MV','960',0),(131,'Mali','ML','223',0),(132,'Malta','MT','356',0),(133,'Marshall Islands','MH','692',0),(134,'Martinique','MQ','596',0),(135,'Mauritania','MR','222',0),(136,'Mauritius','MU','230',0),(137,'Mayotte','YT','262',0),(138,'Mexico','MX','52',0),(139,'Micronesia, Federated States of','FM','691',0),(140,'Moldova','MD','373',0),(141,'Monaco','MC','377',0),(142,'Mongolia','MN','976',0),(143,'Montserrat','MS','1-664',0),(144,'Morocco','MA','212',0),(145,'Mozambique','MZ','258',0),(146,'Myanmar','MM','95',0),(147,'Namibia','NA','264',0),(148,'Nauru','NR','674',0),(149,'Nepal','NP','977',0),(150,'Netherlands','NL','31',0),(151,'Netherlands Antilles','AN','599',0),(152,'New Caledonia','NC','687	',0),(153,'New Zealand','NZ','64',0),(154,'Nicaragua','NI','505',0),(155,'Niger','NE','227',0),(156,'Nigeria','NG','234',0),(157,'Niue','NU','683',0),(158,'Norfolk Island','NF','672',0),(159,'Northern Mariana Islands','MP','1-670',0),(160,'Norway','NO','47',0),(161,'Oman','OM','968',0),(162,'Pakistan','PK','92',0),(163,'Palau','PW','680',0),(164,'Panama','PA','507',0),(165,'Papua New Guinea','PG','675',0),(166,'Paraguay','PY','595',0),(167,'Peru','PE','51',0),(168,'Philippines','PH','63',0),(169,'Pitcairn','PN','64',0),(170,'Poland','PL','48',0),(171,'Portugal','PT','351',0),(172,'Puerto Rico','PR','1-787',0),(173,'Qatar','QA','974',0),(174,'Reunion','RE','262',0),(175,'Romania','RO','40',0),(176,'Russian Federation','RU','7',0),(177,'Rwanda','RW','250',0),(178,'Saint Kitts and Nevis','KN','1-869',0),(179,'Saint Lucia','LC','1-758',0),(180,'Saint Vincent and the Grenadines','VC','1-784',0),(181,'Samoa','WS','685',0),(182,'San Marino','SM','378',0),(183,'Sao Tome and Principe','ST','239',0),(184,'Saudi Arabia','SA','966',0),(185,'Senegal','SN','221',0),(186,'Seychelles','SC','248',0),(187,'Sierra Leone','SL','232',0),(188,'Singapore','SG','65',0),(189,'Slovakia (Slovak Republic)','SK','421',0),(190,'Slovenia','SI','386',0),(191,'Solomon Islands','SB','677',0),(192,'Somalia','SO','252',0),(193,'South Africa','ZA','27',0),(194,'South Georgia and the South Sandwich Islands','GS','500',0),(195,'Spain','ES','34',0),(196,'Sri Lanka','LK','94',0),(197,'Saint Helena, Ascension and Tristan da Cunha','SH','290',0),(198,'St. Pierre and Miquelon','PM','508',0),(199,'Sudan','SD','249',0),(200,'Suriname','SR','597',0),(201,'Svalbard and Jan Mayen Islands','SJ','47',0),(202,'Swaziland','SZ','268',0),(203,'Sweden','SE','46',0),(204,'Switzerland','CH','41',0),(205,'Syrian Arab Republic','SY','963',0),(206,'Taiwan','TW','886',0),(207,'Tajikistan','TJ','992',0),(208,'Tanzania, United Republic of','TZ','255',0),(209,'Thailand','TH','66',0),(210,'Togo','TG','228',0),(211,'Tokelau','TK','690',0),(212,'Tonga','TO','676',0),(213,'Trinidad and Tobago','TT','1-868',0),(214,'Tunisia','TN','216',0),(215,'Turkey','TR','90',0),(216,'Turkmenistan','TM','993',0),(217,'Turks and Caicos Islands','TC','1-649',0),(218,'Tuvalu','TV','688',0),(219,'Uganda','UG','256',0),(220,'Ukraine','UA','380',0),(221,'United Arab Emirates','AE','971',0),(222,'United Kingdom','GB','44',0),(223,'United States','US','1',0),(224,'United States Minor Outlying Islands','UM','246',0),(225,'Uruguay','UY','598',0),(226,'Uzbekistan','UZ','998',0),(227,'Vanuatu','VU','678',0),(228,'Vatican City State (Holy See)','VA','379',0),(229,'Venezuela','VE','58',0),(230,'Vietnam','VN','84',0),(231,'Virgin Islands (British)','VG','1-284',0),(232,'Virgin Islands (U.S.)','VI','1-340',0),(233,'Wallis and Futuna Islands','WF','681',0),(234,'Western Sahara','EH','212',0),(235,'Yemen','YE','967',0),(236,'Serbia','RS','381',0),(238,'Zambia','ZM','260',0),(239,'Zimbabwe','ZW','263',0),(240,'Aaland Islands','AX','358',0),(241,'Palestine','PS','970',0),(242,'Montenegro','ME','382',0),(243,'Guernsey','GG','44-1481',0),(244,'Isle of Man','IM','44-1624',0),(245,'Jersey','JE','44-1534',0),(247,'Curaçao','CW','599',0),(248,'Ivory Coast','CI','225',0),(249,'Kosovo','XK','383',0);
/*!40000 ALTER TABLE `stmd_countryphones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_coupon_log`
--

DROP TABLE IF EXISTS `stmd_coupon_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_coupon_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `totalAmount` double(12,2) NOT NULL,
  `discountAmount` double(12,2) DEFAULT NULL,
  `discountPoint` int(11) DEFAULT NULL,
  `conditions` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_coupon_log`
--

LOCK TABLES `stmd_coupon_log` WRITE;
/*!40000 ALTER TABLE `stmd_coupon_log` DISABLE KEYS */;
INSERT INTO `stmd_coupon_log` VALUES (1,'nigeria10',3,'2018-09-11 07:00:33',7049.99,10.00,NULL,'[\"customer_of_a_specific_shipping_location\"]');
/*!40000 ALTER TABLE `stmd_coupon_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_currencies`
--

DROP TABLE IF EXISTS `stmd_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currencyId` int(11) NOT NULL,
  `format` tinyint(4) NOT NULL,
  `symbol` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `exchangeRate` double(10,6) DEFAULT NULL,
  `isDefault` enum('1','0') NOT NULL DEFAULT '0',
  `status` enum('1','0') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `deleted` enum('1','0') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `currencyId` (`currencyId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_currencies`
--

LOCK TABLES `stmd_currencies` WRITE;
/*!40000 ALTER TABLE `stmd_currencies` DISABLE KEYS */;
INSERT INTO `stmd_currencies` VALUES (1,162,0,'$',1.000000,'1','1','0',0,'2018-05-17 00:00:00',1,'2018-08-06 06:58:11',1,'2018-05-17 00:00:00'),(2,144,2,NULL,578.503789,'0','1','0',1,'2018-05-17 12:13:55',1,'2018-08-06 06:58:11',NULL,NULL),(3,71,0,NULL,72.715018,'0','1','0',1,'2018-05-22 12:48:49',1,'2018-08-06 06:58:11',NULL,NULL);
/*!40000 ALTER TABLE `stmd_currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_default_currencies`
--

DROP TABLE IF EXISTS `stmd_default_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_default_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(3) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `codeInt` int(3) NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `symbol` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `code_int` (`codeInt`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_default_currencies`
--

LOCK TABLES `stmd_default_currencies` WRITE;
/*!40000 ALTER TABLE `stmd_default_currencies` DISABLE KEYS */;
INSERT INTO `stmd_default_currencies` VALUES (1,'ADP',20,'Andorran Pesseta',''),(2,'AED',784,'UAE Dirtham',''),(3,'AFA',4,'Afghani',''),(4,'ALL',8,'Lek',''),(5,'AMD',51,'Armenian Dram',''),(6,'ANG',532,'Netherlands Antillian Guilder','&#402;'),(7,'AON',24,'Kwanza',''),(8,'ARS',32,'Argentine Peso','&#8369;'),(9,'ATS',40,'Austrian Shilling',''),(10,'AUD',36,'Austrilian Dollar','$'),(11,'AWG',533,'Aruban Guilder','&#402;'),(12,'AZM',31,'Azerbaijanian Manat',''),(13,'BAM',977,'Convertible Marks',''),(14,'BBD',52,'Barbados Dollar','$'),(15,'BDT',50,'Bangladesh Taka',''),(16,'BEF',56,'Belgium Franc','&#8355;'),(17,'BGL',100,'Bulgarian Lev',''),(18,'BHD',48,'Belgium Franc',''),(19,'BIF',108,'Burundi Franc',''),(20,'BMD',60,'Bermudian Dollar','$'),(21,'BND',96,'Brunei Dollar','$'),(22,'BOB',68,'Boliviano',''),(23,'BRL',986,'Brazilian Real',''),(24,'BSD',986,'Brazilian Real','$'),(25,'BTN',64,'Ngultrum',''),(26,'BWP',72,'Botswana Pula',''),(27,'BYR',974,'Belarussian Rouble',''),(28,'BZD',84,'Belize Dollar','$'),(29,'CAD',124,'Canadian Dollar','$'),(30,'CDF',976,'Franc Congolais',''),(31,'CHF',756,'Swiss Franc',''),(32,'CLP',152,'Chilean Peso','&#8369;'),(33,'CNY',156,'Chinese Renminbi Yuan','&#20803;'),(34,'COP',170,'Colombian Peso','&#8369;'),(35,'CRC',188,'Cost Rican Colon','&#8353;'),(36,'CUP',192,'Cuban Peso','&#8369;'),(37,'CVE',132,'Cape Verde Escudo',''),(38,'CYP',196,'Cypprus Pound','&#163;'),(39,'CZK',203,'Czech Koruna',''),(40,'DEM',276,'Deutsche Mark',''),(41,'DJF',262,'Djibouti Franc',''),(42,'DKK',208,'Danish Krone',''),(43,'DOP',214,'Dominican Peso','&#8369;'),(44,'DZD',12,'Algerian Dinar',''),(45,'EEK',233,'Kroon',''),(46,'EGP',818,'Egyptian Pound','&#163;'),(47,'ERN',232,'Nakfa',''),(48,'ETB',230,'Ethiopian Birr',''),(49,'EUR',978,'EURO','&#8364;'),(50,'FIM',246,'Finnish Markka',''),(51,'FJD',242,'Fiji Dollar','$'),(52,'FKP',238,'Falkland Islands Pound','&#163;'),(53,'FRF',250,'French Franc','&#8355;'),(54,'GBP',826,'Pound Sterling','&#163;'),(55,'GEL',981,'Lari',''),(56,'GHC',288,'Ghana Cedi',''),(57,'GIP',292,'Gilbraltar Pound','&#163;'),(58,'GMD',270,'Dalasi',''),(59,'GNF',324,'Guinea Franc',''),(60,'GTQ',320,'Quetzal',''),(61,'GWP',624,'Guinea-Bissau Peso',''),(62,'GYD',328,'Guyana Dollar','$'),(63,'HKD',344,'Hong Kong Dollar','$'),(64,'HNL',340,'Lempira',''),(65,'HRK',191,'Croatian Kuna',''),(66,'HTG',332,'Gourde',''),(67,'HUF',348,'Hungary Forint',''),(68,'IDR',360,'Rupiah',''),(69,'IEP',372,'Irish Punt','&#163;'),(70,'ILS',376,'New Israeli Sheqel','&#8362;'),(71,'INR',356,'Indian Ruppe','&#8360;'),(72,'IQD',368,'Iraqi Dinar',''),(73,'IRR',364,'Iranian Rial','&#65020;'),(74,'ISK',352,'Iceland Krona',''),(75,'ITL',380,'Italian Lira','&#8356;'),(76,'JMD',388,'Jamaican Dollar','$'),(77,'JOD',400,'Jordanian Dinar',''),(78,'JPY',392,'Japanese Yen','&#165;'),(79,'KES',404,'Kenyan Shilling',''),(80,'KGS',417,'Son',''),(81,'SHR',116,'Riel',''),(82,'KMF',174,'Comoro Franc',''),(83,'KPW',408,'North Korean Won','&#8361;'),(84,'KRW',410,'South Korean Won','&#8361;'),(85,'KWD',414,'Kuwaiti Dinar',''),(86,'KYD',136,'Cayman Islands Dollar','$'),(87,'KZT',398,'Tenge',''),(88,'LAK',418,'Kip','&#8365;'),(89,'LBP',422,'Lebanese Pound','&#163;'),(90,'LKR',144,'Sri Lanka Rupee','&#3065;'),(91,'LRD',430,'Liberian Dollar','$'),(92,'LSL',426,'Loti',''),(93,'LTL',440,'Lithuanian Litus',''),(94,'LUF',442,'Luxembourg Franc','&#8355;'),(95,'LVL',428,'Latvian Lats',''),(96,'LYD',434,'Libyan Dinar',''),(97,'MAD',504,'Moroccan Dirtham',''),(98,'MDL',498,'Moldovan Leu',''),(99,'MGF',450,'Malagasy Franc',''),(100,'MKD',807,'Denar',''),(101,'MMK',104,'Kyat',''),(102,'MNT',496,'Tugrik','&#8366;'),(103,'MOP',446,'Pataca',''),(104,'MRO',478,'Ouguiya',''),(105,'MTL',470,'Maltese Lira','&#8356;'),(106,'MUR',480,'Mauritius Rupee','&#8360;'),(107,'MVR',462,'Rufiyaa',''),(108,'MWK',454,'Kwacha',''),(109,'MXN',484,'Mexican Peso','$'),(110,'MYR',458,'Malasian Ringgit',''),(111,'MZM',508,'Mozambique Metical',''),(112,'NAD',516,'Namibia Dollar','$'),(113,'NGN',566,'Naira','&#8358;'),(114,'NIO',558,'Cordoba Oro',''),(115,'NLG',528,'Dutch Guilder','&#402;'),(116,'NOK',578,'Norwegian Krone',''),(117,'NPR',524,'Nepalese Rupee','&#8360;'),(118,'NZD',554,'New Zealand Dollar','$'),(119,'OMR',512,'Rial Omani','&#65020;'),(120,'PAB',590,'Balboa',''),(121,'PEN',604,'Nuevo Sol',''),(122,'PGK',598,'Kina',''),(123,'PHP',608,'Philippine Peso','&#8369;'),(124,'PKR',586,'Pakistan Rupee','&#8360;'),(125,'PLN',985,'Zloty',''),(126,'PTE',620,'Portuguese Escudo',''),(127,'PYG',600,'Guarani',''),(128,'QAR',634,'Qatari Rial','&#65020;'),(129,'ROL',642,'Leu',''),(130,'RSD',941,'Serbian Dinar',''),(131,'RUB',643,'Russian Ruble',''),(132,'RUR',810,'Russian Ruble',''),(133,'RWF',646,'Rwanda Franc',''),(134,'SAR',682,'Saudi Riyal','&#65020;'),(135,'SBD',90,'Solomon Islands Dollar','$'),(136,'SCR',690,'Seychelles Rupee','&#8360;'),(137,'SDD',736,'Sudanese Dinar',''),(138,'SEK',752,'Swedish Krona',''),(139,'SGD',702,'Singapore Dollar','$'),(140,'SHP',654,'Saint Helena Pound','&#163;'),(141,'SIT',705,'Tolar',''),(142,'SKK',703,'Slovak Koruna',''),(143,'SLL',694,'Leone',''),(144,'SOS',706,'Somali Shilling',''),(145,'SRG',740,'Suriname Guilder',''),(146,'STD',678,'Dobra',''),(147,'SVC',222,'El Salvador Colon','&#8353;'),(148,'SYP',760,'Syrian Pound','&#163;'),(149,'SZL',748,'Lilangeni',''),(150,'THB',764,'Thailand Baht','&#3647;'),(151,'TJS',972,'Somoni',''),(152,'TMM',795,'Manat',''),(153,'TND',788,'Tunisian Dinar',''),(154,'TOP',776,'Pa\'anga',''),(155,'TPE',626,'Timor Escudo',''),(156,'TRL',792,'Turkish Lira','&#8356;'),(157,'TTD',780,'Trinidad and Tobago Dollar','$'),(158,'TWD',901,'New Taiwan Dollar','$'),(159,'TZS',834,'Tanzanian Shilling',''),(160,'UAH',980,'Hryvnia',''),(161,'UGX',800,'Uganda Shilling',''),(162,'USD',840,'US Dollar','$'),(163,'UYU',858,'Peso Uruguayo',''),(164,'UZS',860,'Tanzanian Shilling',''),(165,'VEB',862,'Bolivar',''),(166,'VND',704,'Dong','&#8363;'),(167,'VUV',548,'Vatu',''),(168,'WST',882,'Tala',''),(169,'XAF',950,'CFA Franc BEAC',''),(170,'XCD',951,'East Caribbean Dollar','$'),(171,'XOF',952,'CFA Franc BCEAO',''),(172,'XPF',953,'CFP Franc',''),(173,'YER',886,'Yemeni Rial','&#65020;'),(174,'YUM',891,'Yugoslavian Dinar',''),(175,'ZAR',710,'South Africa Rand','&#82;'),(176,'ZMK',894,'Kwacha',''),(177,'ZWD',716,'Zimbabwe Dollar','$');
/*!40000 ALTER TABLE `stmd_default_currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_delivery_companies`
--

DROP TABLE IF EXISTS `stmd_delivery_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_delivery_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('1','0') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_delivery_companies`
--

LOCK TABLES `stmd_delivery_companies` WRITE;
/*!40000 ALTER TABLE `stmd_delivery_companies` DISABLE KEYS */;
INSERT INTO `stmd_delivery_companies` VALUES (1,'Shoptomydoor','1',0,'2018-05-25 13:23:45',1,'2018-05-25 14:35:20'),(2,'Fedex','1',0,'2018-05-25 13:23:45',NULL,NULL),(3,'UPS','1',0,'2018-05-25 13:23:45',NULL,NULL),(4,'DHL','1',0,'2018-05-25 13:23:45',NULL,NULL),(5,'USPS','1',0,'2018-05-25 13:23:45',NULL,NULL),(6,'TNT','1',0,'2018-05-25 13:23:45',NULL,NULL),(7,'EMS','1',0,'2018-05-25 13:23:45',NULL,NULL),(8,'Others','1',0,'2018-05-25 13:23:45',NULL,NULL),(9,'Red Star','1',0,'2018-05-25 13:23:45',NULL,NULL),(10,'ABC','1',0,'2018-05-25 13:23:45',1,'2018-09-18 13:19:39'),(11,'Partner - Abuja','1',0,'2018-05-25 13:23:45',NULL,NULL),(12,'Partner - Port Harcourt','1',0,'2018-05-25 13:23:45',NULL,NULL),(13,'Partner - Benin','1',0,'2018-05-25 13:23:45',NULL,NULL),(14,'Partner - Ibadan','1',0,'2018-05-25 13:23:45',NULL,NULL),(15,'Partner - Uyo','1',0,'2018-05-25 13:23:45',NULL,NULL),(16,'Partner - Kaduna','1',0,'2018-05-25 13:23:45',NULL,NULL),(17,'Amazon','1',0,'2018-05-25 13:23:45',NULL,NULL);
/*!40000 ALTER TABLE `stmd_delivery_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_destinationports`
--

DROP TABLE IF EXISTS `stmd_destinationports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_destinationports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `port` varchar(255) NOT NULL DEFAULT '',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_destinationports`
--

LOCK TABLES `stmd_destinationports` WRITE;
/*!40000 ALTER TABLE `stmd_destinationports` DISABLE KEYS */;
INSERT INTO `stmd_destinationports` VALUES (1,'Lagos - Tin Can','1','2018-05-17 06:26:33',0,'2018-05-17 06:26:33',0,'0','2018-05-17 06:26:33',0),(2,'Test456','1','2018-05-17 07:00:32',0,'2018-05-17 07:01:33',0,'1','2018-05-17 07:01:58',1),(3,'Test','1','2018-05-17 07:15:00',0,'2018-05-17 07:15:00',0,'0','2018-05-17 07:15:00',0);
/*!40000 ALTER TABLE `stmd_destinationports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_dispatch_companies`
--

DROP TABLE IF EXISTS `stmd_dispatch_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_dispatch_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('1','0') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_dispatch_companies`
--

LOCK TABLES `stmd_dispatch_companies` WRITE;
/*!40000 ALTER TABLE `stmd_dispatch_companies` DISABLE KEYS */;
INSERT INTO `stmd_dispatch_companies` VALUES (1,'Fedex','1',1,'2018-05-25 14:42:52',NULL,NULL);
/*!40000 ALTER TABLE `stmd_dispatch_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_documents`
--

DROP TABLE IF EXISTS `stmd_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `filename` text NOT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `createdOn` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_documents`
--

LOCK TABLES `stmd_documents` WRITE;
/*!40000 ALTER TABLE `stmd_documents` DISABLE KEYS */;
INSERT INTO `stmd_documents` VALUES (1,3,'1536644692_images (2).jpeg','Test 1 with Image','2018-09-11 05:44:52',3,'1','2018-09-11 05:45:13',3);
/*!40000 ALTER TABLE `stmd_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_drivers`
--

DROP TABLE IF EXISTS `stmd_drivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_drivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driverName` varchar(100) NOT NULL,
  `driverPhone` varchar(50) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `contactEmail` varchar(100) NOT NULL,
  `status` enum('0','1') DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL,
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_drivers`
--

LOCK TABLES `stmd_drivers` WRITE;
/*!40000 ALTER TABLE `stmd_drivers` DISABLE KEYS */;
INSERT INTO `stmd_drivers` VALUES (1,'Test','5645645645','dfsdfwertert','test@stmd.com','1','0','2018-05-28 11:16:36',1,1,'2018-05-28 11:20:33',0,'2018-05-28 13:04:05'),(2,'John Doe','6545645646','AC Transit','john@stmd.com','1','0','2018-05-28 11:21:14',1,0,'2018-05-28 11:21:14',0,'2018-05-28 11:21:14');
/*!40000 ALTER TABLE `stmd_drivers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_emailtemplatekeys`
--

DROP TABLE IF EXISTS `stmd_emailtemplatekeys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_emailtemplatekeys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `keyname` varchar(255) NOT NULL,
  `templateType` enum('G','O','S','W','T') NOT NULL COMMENT 'G=General, O=Order Status, S=Order Status (single), W=Shipment (notify), Storage Notification (single)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_emailtemplatekeys`
--

LOCK TABLES `stmd_emailtemplatekeys` WRITE;
/*!40000 ALTER TABLE `stmd_emailtemplatekeys` DISABLE KEYS */;
INSERT INTO `stmd_emailtemplatekeys` VALUES (1,'Registration Activation','registration_active','G'),(2,'Forgot Password for Customers','forget_password_customers','G'),(3,'Forgot Password for Admins','forget_password_admin','G'),(4,'Change Password for Customers','change_password_customers','G'),(5,'Change Password for Admins','change_password_admin','G'),(6,'Notification for Admins','notification_admin','G'),(7,'Create Admins','creation_admin','G'),(8,'Create Customers','creation_customers','G'),(9,'Successful Reset Password','successful_reset_password','G'),(12,'Order Status Key 1','Order_Status_Key1','O'),(13,'Order Status Key 2','Order_Status_Key2','O'),(14,'Order Status (single) Key 1','Order_Single_Key1','S'),(15,'Order Status (single) Key 2','Order_Single_Key2','S'),(18,'Shipment Key1','Shipment_Key1','W'),(19,'Shipment Key2','Shipment_Key2','W'),(20,'Storage Notification Key1','Storage_Notification_Key1','T'),(21,'Storage Notification Key2','Storage_Notification_Key2','T'),(22,'Sending Coupon Code','send_couponcode','G'),(24,'Referral Invitation','referral_invitation','G'),(25,'Contact Us','contact_us','G'),(26,'Wrong Inventory','wronginventory_admin','G'),(27,'File a Claim','File_a_claim','G'),(28,'Resend OTP','resend_otp','G'),(29,'Successfull Account Activation','successfull_activation','G');
/*!40000 ALTER TABLE `stmd_emailtemplatekeys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_emailtemplates`
--

DROP TABLE IF EXISTS `stmd_emailtemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_emailtemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateType` enum('G','O','S','T','W','N') NOT NULL DEFAULT 'G' COMMENT 'G=General,O=Order/Shipment Notification,S=Order Status (single),T=Storage Notification (single), N=Newsletter',
  `templateKey` varchar(255) NOT NULL,
  `templateSubject` varchar(255) NOT NULL,
  `templateBody` text,
  `fromName` varchar(255) NOT NULL,
  `fromEmail` varchar(255) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(10) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(10) NOT NULL DEFAULT '0',
  `deletedBy` int(10) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0:not deleted,1:deleted',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1:active,0:inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_emailtemplates`
--

LOCK TABLES `stmd_emailtemplates` WRITE;
/*!40000 ALTER TABLE `stmd_emailtemplates` DISABLE KEYS */;
INSERT INTO `stmd_emailtemplates` VALUES (1,'G','registration_active','STMD :  Active your account','<p>Kindly activate your account</p>\r\n\r\n<p>[ACTIVATION_LINK]</p>','Admin','admin@stmd.com','2018-05-11 07:34:57',1,'2018-05-11 07:42:55',1,0,'0','1'),(2,'G','change_password_customers','sadsad','<p>sfaf&lt;p&gt;&lt;img class=&quot;ui-widget-content&quot; src=&quot;http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889718_images-8.jpg&quot; /&gt;&lt;/p&gt;</p>','sad','admin@stmd.com','2018-05-22 07:50:53',1,'2018-07-19 09:19:19',1,0,'0','1'),(3,'G','change_password_admin','Change Password','<p>Dear [NAME],</p>\r\n\r\n<p>This is your new password: [PASSWORD]</p>','STMD','admin@stmd.com','2018-05-31 12:37:52',1,'2018-06-01 06:46:42',1,0,'0','1'),(4,'G','notification_admin','Notification from STMD','<p>Dear [NAME],</p>\r\n\r\n<p>Please read the below notification</p>\r\n\r\n[NOTIFICATION]','STMD','admin@stmd.com','2018-05-31 12:37:52',1,'2018-06-01 06:46:42',1,0,'0','1'),(5,'G','creation_admin','Account Creation','<p>Dear [NAME],</p>\r\n\r\n<p>Kindly login to the URL http://10.0.14.103/shoptomydoordev/administrator/login with the following email and password:</p>\r\n\r\n<p>Email: [EMAIL]</p>\r\n<p>Password: [PASSWORD]</p>','STMD','admin@stmd.com','2018-05-31 12:37:52',1,'2018-06-01 06:46:42',1,0,'0','1'),(6,'G','forget_password_admin','Forgot Password','<p>You are receiving this email because we received a password reset request for your account.</p>\r\n\r\n<a href=\"http://localhost/stmd/administrator/password/reset/[TOKEN]\">RESET YOUR PASSWORD</a>\r\n                                ','STMD','admin@stmd.com','2018-05-31 12:37:52',1,'2018-06-01 06:46:42',1,0,'0','1'),(7,'G','successful_reset_password','You have successfully reset your password','<p>Dear [NAME],</p>\r\n\r\n<p>You have successfully reset your password. Your new password is: [PASSWORD]</p>','STMD','admin@stmd.com','2018-06-04 07:12:13',1,'2018-06-04 07:12:13',1,0,'0','1'),(8,'G','e-wallet','E-Wallet','Fresh funds in your wallet','admin','admin@stmd.com','2018-06-01 12:16:30',0,'2018-06-01 12:16:30',0,0,'0','1'),(9,'O','SHIPPED_OUT_OF_UK','Shipped Out of UK','Order #{{order}} leaves UK this weekend. For details, track with order number at Shoptomydoor.com/services.php?mode=track_shipment','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(10,'O','SHIPPED_OUT_OF_CHINA','Shipped Out of China','Order #{{order}} leaves China this weekend. For details, track with order number at Shoptomydoor.com/services.php?mode=track_shipment','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(11,'O','ARRIVED_LAGOS_AIR','Arrived Lagos Airport','Order #{{order}} is at Lagos Airport been cleared. For details, track with order number at Shoptomydoor.com/services.php?mode=track_shipment','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(12,'O','ARRIVED_LAGOS_SEA','Arrived Lagos Airport','Order #{{order}} is at Lagos Seaport been cleared. For details, track with your order number at Shoptomydoor.com/services.php?mode=track_shipment','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(13,'O','ARRIVED_LAGOS_WAREHOUSE','Arrived Lagos Warehouse','Order #{{order}} is at Lagos Warehouse, to be delivered to you in 2 to 4 business days. For details, track with order number at Shoptomydoor.com','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(14,'O','PICKUP_LAGOS','Lagos Office Pick Up','Order #{{order}} is at Lagos Office and can be picked up now. Please come with a valid ID when coming to pick up','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(15,'O','PICKUP_PARTNER','Partner Pick Up','Order #{{order}} is at your Pick Up Center for collection. Please come with a valid ID when coming to pick up','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(16,'O','INTRANSIT_FROM','In Transit from USA/UK/China','Order #{{order}} is in transit. For more details, track with your order number at Shoptomydoor.com/services.php?mode=track_shipment','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(17,'O','DISPATCHED_FROM_LAGOS','Dispatched From Lagos','Order #{{order}} is dispatched and will be delivered in 2 to 4 business days.  For more info, go to Shoptomydoor.com, and track with your order number','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 09:34:24',0,0,'0','1'),(18,'O','','Delayed Shipment','<p>Order #{{order}} is temporarily delayed due to unforeseen issues. For full details, go to Shoptomydoor.com, and track with your order number</p>','Admin','admin@stmd.com','2018-06-06 09:34:24',1,'2018-06-06 11:10:16',1,0,'0','1'),(19,'G','SHIPPED_OUT_OF_USA','Shipped Out of USA','Order #{{order}} leaves US this weekend. For details, track with order number at Shoptomydoor.com/services.php?mode=track_shipment','Admin','admin@stmd.com','2018-06-29 13:03:54',1,'2018-06-29 13:03:54',0,0,'0','1'),(20,'T','successful_reset_password','demo','<p>&lt;p&gt;&lt;img class=&quot;ui-widget-content&quot; src=&quot;http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889718_images-8.jpg&quot; /&gt;&lt;/p&gt;</p>','Somnath Pahari','ssasded@xasx.com','2018-07-19 09:18:27',1,'2018-07-19 09:18:27',0,0,'0','1'),(21,'W','registration_active','sadsad','<p>sadasd</p>','sad','anumitadas@gmail.com','2018-07-19 10:24:18',1,'2018-07-19 10:24:18',0,0,'0','1'),(22,'W','notification_admin','<script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>','<pre id=\"line1\">\r\n<span class=\"comment\">          &lt;script src=&quot;https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js&quot;&gt;&lt;/script&gt;</span></pre>','trtertert','erterter@exmm.com','2018-07-20 14:26:38',1,'2018-07-20 14:26:38',0,0,'0','1'),(23,'G','send_couponcode','STMD Coupon Code','<p>Dear [NAME],</p>\r\n\r\n<p>Use the coupon code in ShopToMyDoor website to get more benefits.</p>\r\n\r\n<p>[COUPONCODE]</p>','Admin','admin@stmd.com','2018-07-27 05:38:25',1,'2018-07-27 11:20:34',1,0,'0','1'),(24,'G','referral_invitation','Join this site','test body','Admin','admin@stmd.com','2018-07-25 15:05:12',0,'2018-07-25 15:05:12',0,0,'0','1'),(25,'G','contact_us','Thank you for getting in touch!','We appreciate you contacting us. One of our representative will get back to you shortly.\r\n\r\nHave a great day!','Shoptomydoor','donotreply@shoptomydoor.com','2018-07-30 11:12:08',0,'2018-07-30 11:12:08',0,0,'0','1'),(26,'G','wronginventory_admin','Wrong Inventory','<p>Hello Admin,</p>\r\n\r\n<p>A customer has the following issues with Shipment Number [SHIPMENTID] and Delivery [DELIVERYID].</p>\r\n\r\n<p>[MESSAGE]</p>\r\n\r\nThanks','Admin','admin@stmd.com','2018-09-06 07:20:51',0,'2018-09-06 07:20:51',0,0,'0','1'),(27,'G','File_a_claim','File a Claim','<p>Dear Administrator,</p>\r\n\r\n<p>User&nbsp;[NAME] ([EMAIL]) has filed a claim. Please see the below details:</p>\r\n\r\n<p><br />\r\n[NOTIFICATION]</p>','Admin','admin@stmd.com','2018-09-10 13:35:24',1,'2018-09-10 13:35:24',0,0,'0','1'),(28,'G','resend_otp','Resent OTP','<p>We&#39;ve resent OTP. Please check below the new OTP;</p>\r\n\r\n<p>[OTP]&nbsp;</p>','Admin','admin@stmd.com','2018-09-13 11:30:17',1,'2018-09-13 11:30:17',0,0,'0','1'),(29,'G','successfull_activation','STMD :  Account is activated','<p>Hi, [NAME]</p>\r\n<p>Thanks for registering with us, Your account is activated</p>\r\n<p>Enjoy shopping</p>','Admin','admin@stmd.com','2018-09-13 13:27:53',1,'2018-09-13 13:27:53',0,0,'0','1');
/*!40000 ALTER TABLE `stmd_emailtemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_ewallet_transactions`
--

DROP TABLE IF EXISTS `stmd_ewallet_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_ewallet_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ewalletId` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `userType` enum('user','admin') NOT NULL DEFAULT 'user',
  `userId` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `transactionType` enum('credit','debit') NOT NULL DEFAULT 'debit',
  `transactionOn` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gcid` (`ewalletId`),
  KEY `recipient` (`userId`),
  CONSTRAINT `stmd_ewallet_transactions_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_ewallet_transactions`
--

LOCK TABLES `stmd_ewallet_transactions` WRITE;
/*!40000 ALTER TABLE `stmd_ewallet_transactions` DISABLE KEYS */;
INSERT INTO `stmd_ewallet_transactions` VALUES (1,'5','user',3,4217.39,'debit','2018-09-11'),(2,'5','user',3,39.03,'debit','2018-09-11'),(3,'5','user',3,577.24,'debit','2018-09-11'),(4,'5','user',3,1648.56,'debit','2018-09-11'),(5,'6','admin',1,190.00,'credit','2018-09-13');
/*!40000 ALTER TABLE `stmd_ewallet_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_ewallets`
--

DROP TABLE IF EXISTS `stmd_ewallets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_ewallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ewalletId` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `purchaser` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `recipient` int(11) NOT NULL,
  `sendVia` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'E',
  `recipientEmail` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `message` text CHARACTER SET latin1 NOT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `debit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `status` enum('0','1','2','3','4','5') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT '0 = Pending, 1= Active, 2=Blocked, 3=Disabled, 4=Expired, 5=Used',
  `addDate` date DEFAULT NULL,
  `blockDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `gcid` (`ewalletId`),
  KEY `recipient` (`recipient`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_ewallets`
--

LOCK TABLES `stmd_ewallets` WRITE;
/*!40000 ALTER TABLE `stmd_ewallets` DISABLE KEYS */;
INSERT INTO `stmd_ewallets` VALUES (5,'A7567D5DAD4A3411','Shoptomydoor',3,'E','somnath.pahari@indusnet.co.in','<p>Fresh funds in your wallet</p>',30517.78,139.03,'1','2018-06-28',NULL),(6,'BA781DACD2F03C83','Shoptomydoor',6,'E','virat@gmail.com','',190.00,0.00,'1','0000-00-00',NULL);
/*!40000 ALTER TABLE `stmd_ewallets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_faqs`
--

DROP TABLE IF EXISTS `stmd_faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_faqs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `metaTitle` varchar(255) DEFAULT NULL,
  `metaKeywords` varchar(255) DEFAULT NULL,
  `metaDescription` text,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `displayOrder` bigint(20) DEFAULT NULL,
  `createdBy` bigint(20) NOT NULL,
  `modifiedBy` bigint(20) DEFAULT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_faqs`
--

LOCK TABLES `stmd_faqs` WRITE;
/*!40000 ALTER TABLE `stmd_faqs` DISABLE KEYS */;
INSERT INTO `stmd_faqs` VALUES (5,'How can I change my shipping address??','<p>By default, the last used shipping address will be saved into your Sample Store account. When you are checking out your order, the default shipping address will be displayed and you have the option to amend it if you need to.</p>',NULL,NULL,NULL,'Active','0',2,1,1,'2017-05-30 05:02:18','2018-07-31 12:58:55',NULL,NULL),(6,'How can I use my remaining Account Credits?','We are in the process of removing the option to pay for your orders by Account Credits. If you have remaining credits in your account, it will be used to pay for your next checkout. If there are insufficient credits, the system will direct you automatically to pay the balance via Paypal.',NULL,NULL,NULL,'Active','0',1,1,1,'2017-05-30 05:02:25','2018-09-11 05:41:31',NULL,NULL),(24,'Lorem Ipsum','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',NULL,NULL,NULL,'Active','1',3,1,1,'2018-07-31 17:06:23','2018-07-31 12:18:46',1,'2018-07-31 17:48:45'),(25,'Lorem Ipsum','<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>',NULL,NULL,NULL,'Active','1',4,1,1,'2018-07-31 17:49:21','2018-07-31 13:09:16',1,'2018-07-31 13:09:16');
/*!40000 ALTER TABLE `stmd_faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_fillshipboxes`
--

DROP TABLE IF EXISTS `stmd_fillshipboxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_fillshipboxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `length` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `clearingAndDuty` decimal(12,2) NOT NULL,
  `warehousing` decimal(12,2) NOT NULL,
  `description` text,
  `customerNotes` text,
  `uploadedImage` text,
  `orderby` int(11) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_fillshipboxes`
--

LOCK TABLES `stmd_fillshipboxes` WRITE;
/*!40000 ALTER TABLE `stmd_fillshipboxes` DISABLE KEYS */;
INSERT INTO `stmd_fillshipboxes` VALUES (1,'NoName1',6,6,0,0.00,0.00,'<p>&nbsp;</p>\r\n\r\n<p><img class=\"ui-widget-content\" src=\"http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889718_images-8.jpg\" />&lt;p&gt;&lt;img class=&quot;ui-widget-content&quot; src=&quot;http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889735_Rotating_earth_(large).gif&quot; /&gt;&lt;/p&gt;</p>\r\n\r\n<p>&nbsp;</p>','<p>&nbsp;</p>\r\n\r\n<p><img class=\"ui-widget-content\" src=\"http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889735_Rotating_earth_(large).gif\" /></p>\r\n\r\n<p><img class=\"ui-widget-content\" src=\"http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889718_images-8.jpg\" /></p>\r\n\r\n<p><img class=\"ui-widget-content\" src=\"http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889706_images.jpeg\" />&lt;p&gt;&lt;img class=&quot;ui-widget-content&quot; src=&quot;http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889718_images-8.jpg&quot; /&gt;&lt;/p&gt;</p>\r\n\r\n<p>&nbsp;</p>','1528979046_image.png',0,'0',1,'2018-07-19 09:27:04',NULL,NULL),(2,'Cell phone/Shoe Pack',12,6,6,5.00,3.99,'<p>Preferred pack for shipping cell phones (4 or less) and can be combined with a few other small items. Hence this pack can take 3 cell phones and a watch box, or similar combination of items. Need to ship a pair or two of mid size ladies shoes (without the shoe box), then this is your perfect pack. As always, we use a solid 200 pound weight limited box to ensure your items are delivered in perfect condition.</p>','<p>Maximum allowable weight is 3 pounds. If shipping by sea, there is no limit on weight.</p>','1528979163_image.jpeg',1,'0',1,'2018-06-14 17:56:03',NULL,NULL),(3,'Tablet Pack',12,12,6,10.00,4.99,'<p>The right pack to ship your tablets. Ship just one tablet, or add up to 2 extra phones to make one tablet and 2 phones in this pack. Can also be used to ship as much as 8 small/mid size phones (screens less than 6 inches). Can also be used to ship tiny laptops of less than 12 inch screen. Need to send shoes, this can pack as much as 5 pairs of female shoes (without the shoe box) or as much as 3 pairs of male shoes (average size legs without the shoe boxes). Mix and match different small items to achieve great economy in shipping with this box.</p>','<p>Maximum allowable weight is 6 pounds. If shipping by sea, there is no limit on weight.</p>','1528979222_image.jpeg',2,'0',1,'2018-06-14 17:57:02',NULL,NULL),(4,'Laptop Mega',20,16,6,20.00,5.99,'<p>Designed to ship large size laptops of 17 inch screen and above while ensuring maximum protection during transit. Can fit 10 to 13 female shoes (without the show boxes), or as many as 7 male shoes (without the shoe boxes).</p>','<p>Maximum allowable weight is 14 pounds. If shipping by sea, there is no limit on weight.</p>','1528979599_image (1).jpeg',4,'0',1,'2018-06-14 18:03:19',NULL,NULL),(5,'Personal Micro Pack',12,12,12,15.00,5.99,'<p>Can be used to ship from 6 to 8 ladies shoes (without the shoe boxes), and well over 25 male shirts. Ideal for everyday personal items for just one person. Box is a high quality box designed to protect all your items during transit.</p>','<p>Maximum allowable weight is 12 pounds. If shipping by sea, there is no limit on weight.</p>','1528979644_image.jpeg',5,'0',1,'2018-06-14 18:04:04',NULL,NULL),(6,'Personal Mini Pack',18,12,12,20.00,5.99,'<p>Designed to fit as much as 5 laptops of less than 16 inch sceen each. Can ship from 16 to 20 ladies shoes (with the shoe boxes removed) or as much as 7 to 9 males shoes (with the shoe boxes removed). This box can also hold well over 40 t-shirts, making it ideal to combine lots of various items. Box is a high quality box designed to protect all your items during transit.</p>','<p>Maximum allowable weight is 19 pounds. If shipping by sea, there is no limit on weight.</p>','1528979673_image.jpeg',6,'0',1,'2018-06-14 18:04:33',NULL,NULL),(7,'Personal Standard Pack',24,12,12,25.00,6.99,'<p>This box can hold as much as 5 standard laptops of less than 16 inch screen width, and still have space to add up to 3 extra males shoes (without the shoe boxes). For ladies, pack as much as 20 shoes into this box (without the shoe boxes). This is ideal size to meet all your personal shopping needs. Box is a high quality box designed to protect all your items during transit.</p>','<p>Maximum allowable weight is 25 pounds. If shipping by sea, there is no limit on weight.</p>','1528979703_image.jpeg',7,'0',1,'2018-06-14 18:05:03',NULL,NULL),(8,'Personal Mega Pack',18,18,12,30.00,7.99,'<p>This box is ideal for all your mega personal shopping with capacity to take well over upto 5 standard laptops (less than 16 inch screen) as well as a few shoes and a few cell phones. In all, this box can hold well over 30 ladies shoes (without the shoe boxes), and as much as 15 to 20 male shoes (without the shoe boxes). Buy all types of electronics and lets load it in this box for you. Box is a high quality box designed to protect all your items during transit.</p>','<p>Maximum allowable weight is 28 pounds. If shipping by sea, there is no limit on weight.</p>','1528979734_image.jpeg',8,'0',1,'2018-06-14 18:05:34',NULL,NULL),(9,'Family Micro Pack',24,18,12,30.00,8.99,'<p>Doing a small shopping for everyone in the family. Then this pack is right fro you. Fit clothes, shoes, electronics and many things your family needs in this box. Box is made of solid material to ensure items are delivered safely to you always.</p>','<p>Maximum allowable weight is 37 pounds. If shipping by sea, there is no limit on weight.</p>','1528979760_image.jpeg',9,'0',1,'2018-06-14 18:06:00',NULL,NULL),(10,'NoName2',24,24,12,35.00,8.99,'<p>Shop for the whole family. Shoes, clothes, small electronics can all fit this box. Box is made of solid material to ensure items are delivered safely to you always.</p>','<p>Maximum allowable weight is 50 pounds. If shipping by sea, there is no limit on weight.</p>','1528979793_image.jpeg',10,'0',1,'2018-06-14 18:06:33',NULL,NULL),(11,'Laptop Standard',16,16,6,15.00,5.99,'<p>The perfect box to ship your laptop, while ensuring that it is well protected to its destination. This box will fit all laptops with screen size less than 16 inches. Combine other small size electronics, such as tablets and phones to save more on shipping when using this box. Also ideal to ship 8 to 10 female shoes (without the shoe boxes) or upto 4 male shoes (without the shoe boxes).</p>','<p>Maximum allowable weight is 11 pounds. If shipping by sea, there is no limit on weight.</p>','1528979406_image.jpeg',3,'0',1,'2018-06-14 18:00:06',NULL,NULL),(12,'Family Standard Pack',24,24,18,35.00,8.99,'<p>Shop for the whole family. Shoes, clothes, small electronics can all fit this box. This pack will take care of the holiday shopping needs to most families. Box is made of solid material to ensure items are delivered safely to you always.</p>','<p>Maximum allowable weight is 75 pounds. If shipping by sea, there is no limit on weight.</p>','1528979818_image.jpeg',11,'0',1,'2018-06-14 18:06:58',NULL,NULL),(13,'Family Mega Pack',24,24,24,45.00,9.99,'<p>Shop for a large family or just want to do big shopping this period, then this is the right box. Shoes, clothes, small electronics can all fit this box. This pack will take care of the holiday shopping needs to large families. Box is made of solid material to ensure items are delivered safely to you always.</p>','<p>Maximum allowable weight is 99 pounds. If shipping by sea, there is no limit on weight.</p>','1528979838_image.jpeg',12,'0',1,'2018-06-14 18:07:18',NULL,NULL),(14,'Business Micro Pack',36,24,24,50.00,14.99,'<p>Ideal for the small business owner buying items to resell to customers. This is a box that will take well over 120 ladies shoes (without the shoe boxes) and will contains well over 200 t-shirts. Hence this is idea for the business owner, lowering your cost per item to a few or below $1 in most cases.</p>','<p>Maximum allowable weight is 149 pounds. If shipping by sea, there is no limit on weight.</p>','1528979876_image.jpeg',13,'0',1,'2018-06-14 18:07:56',NULL,NULL),(15,'Business Mini Pack',30,30,30,60.00,19.99,'<p>Ideal for the small business owner buying items to resell to customers. This is a box that will take well over 150 ladies shoes (without the shoe boxes) and will contains well over 280 t-shirts. Hence this is idea for the business owner, lowering your cost per item to a few or below $1 in most cases.</p>','<p>Maximum allowable weight is 194 pounds. If shipping by sea, there is no limit on weight.</p>','1528979904_image.jpeg',14,'0',1,'2018-06-14 18:08:24',NULL,NULL),(16,'Business Standard Pack',36,36,36,60.00,24.99,'<p>Ideal for the business owner buying items to resell to customers. This is a box that can take over 250 ladies shoes (without the shoe boxes) and will contains well over 400 t-shirts. Hence this is idea for the business owner, lowering your cost per item to a few or below $1 in most cases.</p>','<p>Maximum allowable weight is 336 pounds. If shipping by sea, there is no limit on weight.</p>','1528979939_image.jpeg',15,'0',1,'2018-06-14 18:08:59',NULL,NULL),(17,'Business Mega Pack',48,40,36,100.00,39.99,'<p>Ideal for the business owner buying items to resell to customers. This is a box that can take over 400 ladies shoes (without the shoe boxes) and will contains well over 700 t-shirts. Hence this is idea for the business owner, lowering your cost per item to a few or below $1 in most cases.</p>','<p>Maximum allowable weight is 497 pounds. If shipping by sea, there is no limit on weight.</p>','1528979962_image.jpeg',16,'0',1,'2018-06-14 18:09:22',NULL,NULL),(18,'Business Pallet',48,40,60,150.00,59.99,'<p>Ideal for the businesses that have large items to ship.</p>','<p>Maximum allowable weight is 829 pounds. If shipping by sea, there is no limit on weight.</p>','1528979986_image.jpeg',17,'0',1,'2018-06-14 18:09:46',NULL,NULL);
/*!40000 ALTER TABLE `stmd_fillshipboxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_fundpoints`
--

DROP TABLE IF EXISTS `stmd_fundpoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_fundpoints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `point` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_fundpoints`
--

LOCK TABLES `stmd_fundpoints` WRITE;
/*!40000 ALTER TABLE `stmd_fundpoints` DISABLE KEYS */;
INSERT INTO `stmd_fundpoints` VALUES (1,3,200),(2,6,0);
/*!40000 ALTER TABLE `stmd_fundpoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_fundpointtransactions`
--

DROP TABLE IF EXISTS `stmd_fundpointtransactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_fundpointtransactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `senderId` int(11) DEFAULT NULL,
  `point` int(10) NOT NULL,
  `reason` varchar(255) CHARACTER SET utf8 NOT NULL,
  `type` enum('A','R','E') NOT NULL DEFAULT 'A' COMMENT 'A=added, R=redeemed, E=expired',
  `expiredDate` datetime DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_fundpointtransactions`
--

LOCK TABLES `stmd_fundpointtransactions` WRITE;
/*!40000 ALTER TABLE `stmd_fundpointtransactions` DISABLE KEYS */;
INSERT INTO `stmd_fundpointtransactions` VALUES (1,3,1,0,'Registration','A',NULL,'2018-09-11 05:49:56'),(2,3,1,200,'Added by Administrator','A','2018-09-15 00:00:00','2018-09-11 05:50:10'),(3,6,1,0,'Registration','A',NULL,'2018-09-13 05:04:53');
/*!40000 ALTER TABLE `stmd_fundpointtransactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_fundpointtransactions_Sept_7`
--

DROP TABLE IF EXISTS `stmd_fundpointtransactions_Sept_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_fundpointtransactions_Sept_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `senderId` int(11) DEFAULT NULL,
  `point` int(10) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_fundpointtransactions_Sept_7`
--

LOCK TABLES `stmd_fundpointtransactions_Sept_7` WRITE;
/*!40000 ALTER TABLE `stmd_fundpointtransactions_Sept_7` DISABLE KEYS */;
INSERT INTO `stmd_fundpointtransactions_Sept_7` VALUES (1,1,1,1,'0000-00-00 00:00:00'),(2,1,1,401,'0000-00-00 00:00:00'),(3,1,1,800,'2018-05-21 07:45:06'),(4,1,1,1,'2018-05-21 07:53:58'),(5,1,1,3400,'2018-05-21 13:38:14'),(6,1,1,1,'2018-05-21 13:38:21'),(7,1,1,200,'2018-05-22 05:58:14'),(8,1,1,200,'2018-05-22 09:29:25'),(9,1,1,1,'2018-05-22 11:05:24'),(10,1,1,2000,'2018-05-22 11:06:09'),(11,1,1,1,'2018-05-22 11:09:19'),(12,1,1,200,'2018-05-22 11:10:13'),(13,1,1,1,'2018-05-23 07:20:49'),(14,3,1,100,'2018-06-26 12:27:28'),(15,3,1,1000,'2018-06-27 05:00:16'),(16,3,1,100,'2018-06-27 05:01:52'),(17,3,1,100,'2018-06-27 05:02:05'),(18,3,1,1,'2018-06-27 05:03:30'),(19,3,1,1,'2018-06-27 05:21:32'),(20,3,1,2,'2018-06-27 05:21:41'),(21,3,1,1,'2018-06-27 05:22:13'),(22,3,1,1,'2018-09-03 09:28:06');
/*!40000 ALTER TABLE `stmd_fundpointtransactions_Sept_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_fundwallet`
--

DROP TABLE IF EXISTS `stmd_fundwallet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_fundwallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_fundwallet`
--

LOCK TABLES `stmd_fundwallet` WRITE;
/*!40000 ALTER TABLE `stmd_fundwallet` DISABLE KEYS */;
INSERT INTO `stmd_fundwallet` VALUES (1,1,497.11),(3,2,5.00);
/*!40000 ALTER TABLE `stmd_fundwallet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_fundwallettransction`
--

DROP TABLE IF EXISTS `stmd_fundwallettransction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_fundwallettransction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `senderId` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_fundwallettransction`
--

LOCK TABLES `stmd_fundwallettransction` WRITE;
/*!40000 ALTER TABLE `stmd_fundwallettransction` DISABLE KEYS */;
INSERT INTO `stmd_fundwallettransction` VALUES (1,1,1,1.00,'2018-05-17 07:27:34'),(2,1,1,2.00,'2018-05-21 06:05:18'),(3,1,1,3.00,'2018-05-22 05:57:57');
/*!40000 ALTER TABLE `stmd_fundwallettransction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_generalsettings`
--

DROP TABLE IF EXISTS `stmd_generalsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_generalsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupName` varchar(100) NOT NULL,
  `subGroupName` varchar(100) NOT NULL,
  `settingsTitle` varchar(100) NOT NULL,
  `settingsKey` varchar(100) NOT NULL,
  `settingsValue` varchar(100) DEFAULT NULL,
  `oldValue` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_generalsettings`
--

LOCK TABLES `stmd_generalsettings` WRITE;
/*!40000 ALTER TABLE `stmd_generalsettings` DISABLE KEYS */;
INSERT INTO `stmd_generalsettings` VALUES (1,'siteSettings','General Parameters','Weight Symbol','weight_symbol','lbs','lbs'),(2,'siteSettings','General Parameters','Weight','weight_value','2.33','2.33'),(3,'siteSettings','General Parameters','Length Symbol','length_symbol','meter','meter'),(4,'siteSettings','General Parameters','Length ','length_value','3','3'),(5,'siteSettings','Payment options','CC Information','cc_info','0','0'),(6,'siteSettings','Payment options','Display CVV2','dispaly_cvv2','1','1'),(7,'siteSettings','Payment options','Authorization Period','auth_period','36','36'),(8,'paymentSettings','Paypal','Test/Live mode','mode','Live','Live'),(9,'paymentSettings','Paypal','Currency','currency','USD','USD'),(10,'paymentSettings','Paypal','Order prefix','order_prefix','',''),(11,'paymentSettings','Paypal','Type','payment_type','email','email'),(12,'paymentSettings','Paypal','Email ','payment_email','contact@shoptomydoor.com','contact@shoptomydoor.com'),(13,'paymentSettings','Paypal','API access username','payment_api_user','nduka_api1.americanairseacargo.com','nduka_api1.americanairseacargo.com'),(14,'paymentSettings','Paypal','API access password','payment_api_pass','1111111111','1111111111'),(15,'paymentSettings','Paypal','Use PayPal authentication method','payment_auth','',''),(16,'paymentSettings','Paypal','API signature','payment_api_sign','AFcWxV21C7fd0v3bYYYRCpSSRl31AQjoG0ip8WmHbZ7.lONkZjDzvI-P','AFcWxV21C7fd0v3bYYYRCpSSRl31AQjoG0ip8WmHbZ7.lONkZjDzvI-P'),(17,'paymentSettings','Authorize.Net:AIM','Account type ','account_type','cnp_ecommerce','cnp_ecommerce'),(18,'paymentSettings','Authorize.Net:AIM','API Login ID','api_login','2x23Maq34DUn','2x23Maq34DUn'),(19,'paymentSettings','Authorize.Net:AIM','Transaction key','api_transaction_key','85apR3qGd2DW88km','85apR3qGd2DW88km'),(20,'paymentSettings','Authorize.Net:AIM','MD5 hash value','api_hash_val','46787','46787'),(21,'paymentSettings','Authorize.Net:AIM','Test/Live mode','mode','Live','Live'),(22,'paymentSettings','Authorize.Net:AIM','Order prefix','order_prefix','123','123'),(23,'shipmentSettings','General shipping parameters','Minimum Insurance Cost([CURRENCY])','min_insurance_cost','10.52','10.52'),(24,'shipmentSettings','General shipping parameters','Chargeable Weight Factor','weight_factor','139','3123'),(25,'shipmentSettings','General shipping parameters','Charge Per Unit Weight ','unit_weight_charge','0.1','0.1'),(26,'shipmentSettings','General shipping parameters','Maximum Storage Days','max_storage_days','14','14'),(27,'shipmentSettings','General shipping parameters','Chargeable Weight Factor(Kg/Cm) ','charge_weight_factor','5000','5000'),(29,'shipmentSettings','General shipping parameters','Recount Cost','recount_cost','4.99','4.99'),(30,'shipmentSettings','General shipping parameters','Acceptable Package Error','acceptable_package_error','5','5'),(31,'shipmentSettings','General shipping parameters','Houston Pick Up Cost','houston_pickup_cost','5','5'),(32,'shipmentSettings','General shipping parameters','Split Shipment Cost','split_shipment_cost','5.99','5.99'),(33,'shipmentSettings','General shipping parameters','Return Item Cost','return_item_cost','5.99','5.99'),(34,'shipmentSettings','General shipping parameters','Item Snap Shot','item_snap_shot','2.99','2.99'),(35,'shipmentSettings','General shipping parameters','Auto Shipments Insurance Value(%)','auto_shipment_insurance','0','0'),(38,'shipmentSettings','Re-weight Cost','Re-weight Cost','reweight_cost','7.99','7.99'),(39,'currencySettings','','Online service','currency_exchange_service','google_currency_api','google_currency_api'),(40,'currencySettings','','Auto Update','auto_update','1','1'),(41,'currencySettings','','Time Hours','update_scheduled_time','13:30','13:30'),(42,'ewalletSettings','generalSettings','Minimum GC value','min_gc_val','50','50'),(43,'ewalletSettings','generalSettings','Maximum GC value (0 if unlimited)','max_gc_val','2000','2000'),(44,'ewalletSettings','generalSettings','E-Wallet blocking period (minutes)','wallet_block_period','60','60'),(45,'ewalletSettings','generalSettings','Enable posting E-Wallet by mail','enable_posting','1','0'),(46,'ewalletSettings','generalSettings','Print E-Wallet on separate pages','print_wallet_separate','1','1'),(47,'ewalletSettings','generalSettings','Show expired E-Wallet','show_expired_wallet','1','0'),(48,'FillandShip','Fill and Ship','Excess Volume Handling Charge','excess_volume_handling_charge','0','0'),(49,'FillandShip','Fill and Ship','Excess Volume Charge %','excess_volume_charge_prc','10','10'),(50,'FillandShip','Fill and Ship','E-Wallet Transfer Discount %','ewallet_transfer_discount_prc','10','10'),(51,'FillandShip','Fill and Ship','Box Full Threshold %','box_full_threshold_prc','90','90'),(52,'FillandShip','Fill and Ship','Box Overflow Threshold %','box_overflow_threshold_prc','3','3'),(53,'FillandShip','Fill and Ship','Fill and Ship Storage days','fill_and_ship_storage_days','8','8'),(54,'FillandShip','Fill and Ship','Extra Charge Product ID','fs_extra_charge_productid','0','0'),(55,'siteSettings','General Parameters','Contact Email','contact_email','contact@shoptomydoor.com','contact@shoptomydoor.com'),(56,'siteSettings','General Parameters','Contact Number','contact_number','[\"US^+1 888315902\",\"NG^+234 7008000\"]','[\"US^+1 888315902\",\"NG^+234 7008000\"]'),(57,'siteSettings','General Parameters','countryCode','countryCode','NG','US'),(58,'shipmentSettings','General shipping parameters','First Delivery Inventory charge','first_delivery_charge','0.69','0.69'),(59,'shipmentSettings','General shipping parameters','Other Delivery Inventory charge','other_delivery_charge','0.69','0.69'),(60,'shipmentSettings','General shipping parameters','Delivery Photo Shot','delivery_snap_shot','6.99','6.99'),(61,'siteSettings','General Parameters','Point Expiry Period','point_expiry_period','4','4'),(62,'siteSettings','General Parameters','1 point = [CURRENCY]','perpoint_cost','100','100');
/*!40000 ALTER TABLE `stmd_generalsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_invoices`
--

DROP TABLE IF EXISTS `stmd_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceUniqueId` varchar(255) NOT NULL,
  `shipmentId` int(11) DEFAULT NULL,
  `procurementId` int(11) DEFAULT NULL,
  `type` enum('buycarforme','shopforme','autopart','othervehicle','autoshipment','shopformeshipment','autopartshipment','othervehicleshipment','othershipment') NOT NULL,
  `invoiceType` enum('invoice','receipt') NOT NULL DEFAULT 'invoice',
  `extraCostCharged` enum('Y','N') NOT NULL DEFAULT 'N',
  `userUnit` varchar(255) NOT NULL,
  `userFullName` varchar(255) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userContactNumber` varchar(255) DEFAULT NULL,
  `billingName` varchar(255) NOT NULL,
  `billingEmail` varchar(255) NOT NULL,
  `billingAddress` varchar(255) NOT NULL,
  `billingAlternateAddress` varchar(255) DEFAULT NULL,
  `billingCity` varchar(255) DEFAULT NULL,
  `billingState` varchar(255) DEFAULT NULL,
  `billingCountry` varchar(255) NOT NULL,
  `billingZipcode` varchar(255) NOT NULL,
  `billingPhone` varchar(255) NOT NULL,
  `billingAlternatePhone` varchar(255) DEFAULT NULL,
  `totalBillingAmount` decimal(12,2) NOT NULL,
  `invoiceParticulars` longtext NOT NULL,
  `paymentMethodId` int(11) DEFAULT NULL,
  `paymentStatus` enum('unpaid','paid','failed') NOT NULL DEFAULT 'unpaid',
  `createdOn` datetime NOT NULL,
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  KEY `procurementId` (`procurementId`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_invoices`
--

LOCK TABLES `stmd_invoices` WRITE;
/*!40000 ALTER TABLE `stmd_invoices` DISABLE KEYS */;
INSERT INTO `stmd_invoices` VALUES (1,'INVNG5-2-20180711',NULL,2,'shopforme','invoice','N','NG5','John Doe','johndoe@example.com','6456456456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'','Lagos','Nigeria','77479','713-530-1160',NULL,0.00,'{\"shipment\":{\"totalItemCost\":\"25951.00\",\"totalProcessingFee\":\"100.00\",\"urgentPurchaseCost\":\"10.00\",\"totalProcurementCost\":\"1000.00\",\"totalWeight\":\"20.00\",\"totalQuantity\":null},\"warehouse\":{\"fromAddress\":\"Test Address\",\"fromZipCode\":\"5435345345\",\"fromCountry\":\"United States\",\"fromState\":\"Louisiana\",\"fromCity\":\"Alexandria\"},\"shippingaddress\":{\"toCountry\":\"United States\",\"toState\":\"Louisiana\",\"toCity\":\"Alexandria\",\"toAddress\":\"24\\/5 P.O Street\",\"toZipCode\":\"3423424\",\"toName\":\"Jane Austan\",\"toEmail\":\"janeaustan.example.com\",\"toPhone\":\"0994993424234\"},\"shippingcharges\":[],\"packages\":[{\"id\":1,\"storeName\":null,\"categoryName\":\"Tablets & Phones\",\"subcategoryName\":\"Tablets\",\"productName\":\"Ipads\\/tablets\",\"itemName\":\"Test\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/test\",\"options\":\"Red\",\"itemPrice\":\"2000.00\",\"itemQuantity\":2,\"itemShippingCost\":\"200.00\",\"itemTotalCost\":\"4200.00\",\"trackingNumber\":1},{\"id\":2,\"storeName\":null,\"categoryName\":\"Baby\",\"subcategoryName\":\"Crib\",\"productName\":\"Shoes - Boys (No box)\",\"itemName\":\"Crib\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/crib\",\"options\":\"Steel\",\"itemPrice\":\"200.00\",\"itemQuantity\":2,\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"410.00\",\"trackingNumber\":2},{\"id\":5,\"storeName\":null,\"categoryName\":\"Electronics\",\"subcategoryName\":null,\"productName\":null,\"itemName\":\"Cannon\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/cannon\",\"options\":null,\"itemPrice\":\"1000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"100.00\",\"itemTotalCost\":\"1100.00\",\"trackingNumber\":34234},{\"id\":10,\"storeName\":null,\"categoryName\":\"Tablets & Phones\",\"subcategoryName\":\"Tablets\",\"productName\":\"Ipads\\/tablets\",\"itemName\":\"Test\",\"websiteUrl\":\"www.flipkart.com\",\"options\":\"Red\",\"itemPrice\":\"10000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"10001.00\",\"trackingNumber\":null}]}',NULL,'unpaid','2018-07-11 11:21:25','0',NULL,NULL),(2,'INVNG5-2-20180711',NULL,2,'shopforme','invoice','N','NG5','John Doe','johndoe@example.com','6456456456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'','Lagos','Nigeria','77479','713-530-1160',NULL,0.00,'{\"shipment\":{\"totalItemCost\":\"25951.00\",\"totalProcessingFee\":\"100.00\",\"urgentPurchaseCost\":\"10.00\",\"totalProcurementCost\":\"1000.00\",\"totalWeight\":\"40.00\",\"totalQuantity\":null},\"warehouse\":{\"fromAddress\":\"Test Address\",\"fromZipCode\":\"5435345345\",\"fromCountry\":\"United States\",\"fromState\":\"Louisiana\",\"fromCity\":\"Alexandria\"},\"shippingaddress\":{\"toCountry\":\"United States\",\"toState\":\"Louisiana\",\"toCity\":\"Alexandria\",\"toAddress\":\"24\\/5 P.O Street\",\"toZipCode\":\"3423424\",\"toName\":\"Jane Austan\",\"toEmail\":\"janeaustan.example.com\",\"toPhone\":\"0994993424234\"},\"shippingcharges\":[],\"packages\":[{\"id\":1,\"storeName\":null,\"categoryName\":\"Tablets & Phones\",\"subcategoryName\":\"Tablets\",\"productName\":\"Ipads\\/tablets\",\"itemName\":\"Test\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/test\",\"options\":\"Red\",\"itemPrice\":\"2000.00\",\"itemQuantity\":2,\"itemShippingCost\":\"200.00\",\"itemTotalCost\":\"4200.00\",\"trackingNumber\":1},{\"id\":2,\"storeName\":null,\"categoryName\":\"Baby\",\"subcategoryName\":\"Crib\",\"productName\":\"Shoes - Boys (No box)\",\"itemName\":\"Crib\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/crib\",\"options\":\"Steel\",\"itemPrice\":\"200.00\",\"itemQuantity\":2,\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"410.00\",\"trackingNumber\":2},{\"id\":5,\"storeName\":null,\"categoryName\":\"Electronics\",\"subcategoryName\":null,\"productName\":null,\"itemName\":\"Cannon\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/cannon\",\"options\":null,\"itemPrice\":\"1000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"100.00\",\"itemTotalCost\":\"1100.00\",\"trackingNumber\":34234},{\"id\":10,\"storeName\":null,\"categoryName\":\"Tablets & Phones\",\"subcategoryName\":\"Tablets\",\"productName\":\"Ipads\\/tablets\",\"itemName\":\"Test\",\"websiteUrl\":\"www.flipkart.com\",\"options\":\"Red\",\"itemPrice\":\"10000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"10001.00\",\"trackingNumber\":null}]}',NULL,'unpaid','2018-07-11 11:30:52','0',NULL,NULL),(3,'INVNG5-2-20180727',NULL,2,'shopforme','invoice','N','NG5','John Doe','johndoe@example.com','6456456456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'','Lagos','Nigeria','77479','713-530-1160',NULL,0.00,'{\"shipment\":{\"totalItemCost\":\"17731.00\",\"totalProcessingFee\":\"1063.86\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"18794.86\",\"totalWeight\":\"6000.00\",\"totalQuantity\":null},\"warehouse\":{\"fromAddress\":\"Test Address\",\"fromZipCode\":\"5435345345\",\"fromCountry\":\"United States\",\"fromState\":\"Louisiana\",\"fromCity\":\"Alexandria\"},\"shippingaddress\":{\"toCountry\":\"United States\",\"toState\":\"Louisiana\",\"toCity\":\"Alexandria\",\"toAddress\":\"24\\/5 P.O Street\",\"toZipCode\":\"3423424\",\"toName\":\"Jane Austan\",\"toEmail\":\"janeaustan.example.com\",\"toPhone\":\"0994993424234\"},\"shippingcharges\":[],\"packages\":[{\"id\":1,\"storeName\":null,\"categoryName\":\"Tablets & Phones\",\"subcategoryName\":\"Tablets\",\"productName\":\"Ipads\\/tablets\",\"itemName\":\"Test\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/test\",\"options\":\"Red\",\"itemPrice\":\"2000.00\",\"itemQuantity\":2,\"itemShippingCost\":\"200.00\",\"itemTotalCost\":\"4200.00\",\"trackingNumber\":\"1\"},{\"id\":2,\"storeName\":null,\"categoryName\":\"Baby\",\"subcategoryName\":\"Crib\",\"productName\":\"Shoes - Boys (No box)\",\"itemName\":\"Crib\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/crib\",\"options\":\"Steel\",\"itemPrice\":\"200.00\",\"itemQuantity\":2,\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"410.00\",\"trackingNumber\":\"2\"},{\"id\":5,\"storeName\":null,\"categoryName\":\"Electronics\",\"subcategoryName\":null,\"productName\":null,\"itemName\":\"Cannon\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/cannon\",\"options\":null,\"itemPrice\":\"1000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"100.00\",\"itemTotalCost\":\"1100.00\",\"trackingNumber\":\"34234\"},{\"id\":10,\"storeName\":null,\"categoryName\":\"Tablets & Phones\",\"subcategoryName\":\"Tablets\",\"productName\":\"Ipads\\/tablets\",\"itemName\":\"Test\",\"websiteUrl\":\"www.flipkart.com\",\"options\":\"Red\",\"itemPrice\":\"10000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"10001.00\",\"trackingNumber\":\"1216569459\"},{\"id\":11,\"storeName\":null,\"categoryName\":\"Tablets & Phones\",\"subcategoryName\":\"Phones\",\"productName\":\"Cell Phones\",\"itemName\":\"Phone\",\"websiteUrl\":\"htpp:\\/\\/amazon.com\\/test\",\"options\":\"Black\",\"itemPrice\":\"1000.00\",\"itemQuantity\":2,\"itemShippingCost\":\"20.00\",\"itemTotalCost\":\"2020.00\",\"trackingNumber\":null}]}',NULL,'unpaid','2018-07-27 13:13:08','0',NULL,NULL),(4,'INVNG5-3-20180830',NULL,3,'buycarforme','invoice','N','NG5','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,' Lagos Island','Lagos','Nigeria','77479','713-530-1160',NULL,6481.20,'{\"shipment\":{\"totalItemCost\":\"6000\",\"totalProcessingFee\":\"420\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"15.55\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":6420,\"insuranceCost\":\"0.00\",\"taxCost\":22.17,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"4\",\"paymentMethodName\":\"GTBank: # : Online Trf  : Act# : 0121339039\",\"paymentMethodKey\":\"bank_online_transfer\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"}}',4,'unpaid','2018-08-30 12:13:44','0',NULL,NULL),(5,'INVNG5-4-20180830',NULL,4,'shopforme','invoice','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,NULL,'2','163','77479','713-530-1160',NULL,5144.28,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"4200.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"4200.00\",\"totalTax\":24.99,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":5144.28,\"totalWeight\":3,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"id\":4,\"itemName\":\"Ipad\",\"websiteUrl\":\"http:\\/\\/staging.multirecruit.com\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"8 GB\",\"itemPrice\":\"4000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"200.00\",\"itemTotalCost\":\"4200.00\"}],\"payment\":{\"paymentMethodId\":4,\"paymentMethodName\":\"GTBank: # : Online Trf  : Act# : 0121339039\"},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-09-15\",\"isDutyCharged\":\"Y\",\"shippingCost\":55.37,\"totalClearingDuty\":444.34,\"totalShippingCost\":499.71}}',4,'unpaid','2018-08-30 12:17:15','0',NULL,NULL),(6,'INVNG5-5-20180830',NULL,5,'shopforme','invoice','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,NULL,'2','163','77479','713-530-1160',NULL,5144.28,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"4200.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"4200.00\",\"totalTax\":24.99,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":5144.28,\"totalWeight\":3,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"id\":5,\"itemName\":\"Ipad\",\"websiteUrl\":\"http:\\/\\/staging.multirecruit.com\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"8 GB\",\"itemPrice\":\"4000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"200.00\",\"itemTotalCost\":\"4200.00\"}],\"payment\":{\"paymentMethodId\":4,\"paymentMethodName\":\"GTBank: # : Online Trf  : Act# : 0121339039\"},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-09-15\",\"isDutyCharged\":\"Y\",\"shippingCost\":55.37,\"totalClearingDuty\":444.34,\"totalShippingCost\":499.71}}',4,'unpaid','2018-08-30 12:19:19','0',NULL,NULL),(7,'INVNG5-1-20180830',1,NULL,'autoshipment','invoice','N','NG5','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,' Lagos Island','Lagos','Nigeria','77479','713-530-1160',NULL,40.20,'{\"shipment\":{\"totalItemCost\":\"7800\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"15.55\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":\"0.00\",\"insuranceCost\":\"0.00\",\"taxCost\":1.17,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"6\",\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\",\"paymentMethodKey\":\"bank_account_pay\",\"poNumber\":\"\",\"companyName\":\"\",\"buyerName\":\"\",\"position\":\"\",\"cardNumber\":\"\",\"cardType\":\"\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Lagos\",\"toCity\":\"Lagos Island\",\"toAddress\":\"1234 Bauchu Road, Yelwa\",\"toZipCode\":\"\",\"toName\":\"Chris Martin\",\"toEmail\":\"chrismartin@gmail.com\",\"toPhone\":\"123456456\"}}',NULL,'unpaid','2018-08-30 12:20:02','0',NULL,NULL),(8,'INVNG5-6-20180830',NULL,6,'shopforme','invoice','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,NULL,'2','163','77479','713-530-1160',NULL,5144.28,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"4200.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"4200.00\",\"totalTax\":24.99,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":5144.28,\"totalWeight\":3,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":null,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"id\":6,\"itemName\":\"Ipad\",\"websiteUrl\":\"http:\\/\\/staging.multirecruit.com\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"8 GB\",\"itemPrice\":\"4000.00\",\"itemQuantity\":1,\"itemShippingCost\":\"200.00\",\"itemTotalCost\":\"4200.00\"}],\"payment\":{\"paymentMethodId\":4,\"paymentMethodName\":\"GTBank: # : Online Trf  : Act# : 0121339039\"},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-09-15\",\"isDutyCharged\":\"Y\",\"shippingCost\":55.37,\"totalClearingDuty\":444.34,\"totalShippingCost\":499.71}}',4,'unpaid','2018-08-30 12:21:01','0',NULL,NULL),(9,'INVNG5-7-20180830',NULL,7,'buycarforme','invoice','N','NG5','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,' Lagos Island','Lagos','Nigeria','77479','713-530-1160',NULL,7447.35,'{\"shipment\":{\"totalItemCost\":\"6900\",\"totalProcessingFee\":\"483\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"15.55\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":7383,\"insuranceCost\":\"0.00\",\"taxCost\":25.32,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"4\",\"paymentMethodName\":\"GTBank: # : Online Trf  : Act# : 0121339039\",\"paymentMethodKey\":\"bank_online_transfer\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"}}',4,'unpaid','2018-08-30 12:35:08','0',NULL,NULL),(10,'INVNG5-8-20180830',NULL,8,'buycarforme','invoice','N','NG5','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,' Lagos Island','Lagos','Nigeria','77479','713-530-1160',NULL,7769.40,'{\"shipment\":{\"totalItemCost\":\"7200\",\"totalProcessingFee\":\"504\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"15.55\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":7704,\"insuranceCost\":\"0.00\",\"taxCost\":26.37,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"4\",\"paymentMethodName\":\"GTBank: # : Online Trf  : Act# : 0121339039\",\"paymentMethodKey\":\"bank_online_transfer\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Lagos\",\"toCity\":\" Lagos Island\",\"toAddress\":\"211 South Meadows Court\",\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"}}',4,'unpaid','2018-08-30 12:38:30','0',NULL,NULL),(11,'INVNG5-2-20180830',2,NULL,'autoshipment','invoice','N','NG5','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,' Lagos Island','Lagos','Nigeria','77479','713-530-1160',NULL,40.20,'{\"shipment\":{\"totalItemCost\":\"7800\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"15.55\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":\"0.00\",\"insuranceCost\":\"0.00\",\"taxCost\":1.17,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"5\",\"paymentMethodName\":\"GTBank : Pay at Bank : Act# : 0121339039\",\"paymentMethodKey\":\"bank_pay_at_bank\",\"poNumber\":\"\",\"companyName\":\"\",\"buyerName\":\"\",\"position\":\"\",\"cardNumber\":\"\",\"cardType\":\"\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Lagos\",\"toCity\":\"Lagos Island\",\"toAddress\":\"1234 Bauchu Road, Yelwa\",\"toZipCode\":\"\",\"toName\":\"Chris Martin\",\"toEmail\":\"chrismartin@gmail.com\",\"toPhone\":\"123456456\"}}',NULL,'unpaid','2018-08-30 12:47:50','0',NULL,NULL),(12,'INVNG5-9-20180830',NULL,9,'autopart','invoice','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Somnath Pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,2015.09,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"2000.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":2000,\"totalTax\":1.9679999999999997,\"isInsuranceCharged\":\"Y\",\"totalInsurance\":0,\"totalCost\":2015.088,\"totalWeight\":3.5,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"procurementId\":9,\"itemName\":\"Car radio\",\"websiteUrl\":\"http:\\/\\/www.flipcart.com\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"38\",\"siteProductId\":\"48\",\"itemDescription\":\"Test Description\",\"year\":\"2018\",\"itemMake\":\"WO\",\"itemModel\":\"Q7\",\"itemPrice\":\"200.00\",\"itemQuantity\":10,\"itemShippingCost\":\"0.00\",\"itemTotalCost\":\"2000.00\"}],\"payment\":{\"paymentMethodId\":8,\"paymentMethodName\":\"Wire Transfer\",\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null}}',8,'paid','2018-08-30 13:04:35','0',NULL,NULL),(13,'RECNG5-10-20180911',NULL,10,'shopforme','receipt','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,4217.39,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"3761.49\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"3761.49\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":4217.389999999999,\"totalWeight\":3,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"id\":4,\"itemName\":\"Apple iPad(6th Gen) Tablet (9.7 inch, 32GB, Wi-Fi)\",\"websiteUrl\":\"https:\\/\\/www.amazon.in\\/Apple-iPad-Tablet-Wi-Fi-Space\\/dp\\/B07C4YKR3J\\/ref=sr_1_1?s=computers&ie=UTF8&qid=1536644836&sr=1-1&keywords=ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Space Grey\",\"itemPrice\":\"3661.49\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"100.00\",\"itemTotalCost\":\"3761.49\"}],\"payment\":{\"paymentMethodId\":7,\"paymentMethodName\":\"E-Wallet\",\"ewalletId\":5},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-09-27\",\"isDutyCharged\":\"Y\",\"shippingCost\":55.37,\"totalClearingDuty\":400.53,\"totalShippingCost\":455.9}}',7,'paid','2018-09-11 05:59:51','0',NULL,NULL),(14,'INVNG5-11-20180911',NULL,11,'shopforme','invoice','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,59.99,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"59.99\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"59.99\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":\"59.99\",\"totalWeight\":0,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"id\":5,\"itemName\":\"Echo Dot (2nd Generation) - Smart speaker with Alexa\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/dp\\/B01DFKC2SO\\/ref=ods_gw_ha_d_bt_propoff_090418?pf_rd_p=5f8214f2-ab41-4000-9a4e-298f4e650978&pf_rd_r=PN8RJ2RCDZ5JXF420N3V\",\"storeId\":\"1\",\"siteCategoryId\":null,\"siteSubCategoryId\":null,\"siteProductId\":null,\"options\":\"Black\",\"itemPrice\":\"49.99\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"59.99\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'unpaid','2018-09-11 06:43:38','0',NULL,NULL),(15,'RECNG5-12-20180911',NULL,12,'shopforme','receipt','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,59.99,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"59.99\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"59.99\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":\"59.99\",\"totalWeight\":0,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"id\":6,\"itemName\":\"Echo Dot (2nd Generation) - Smart speaker with Alexa\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/dp\\/B01DFKC2SO\\/ref=ods_gw_ha_d_bt_propoff_090418?pf_rd_p=5f8214f2-ab41-4000-9a4e-298f4e650978&pf_rd_r=PN8RJ2RCDZ5JXF420N3V\",\"storeId\":\"1\",\"siteCategoryId\":null,\"siteSubCategoryId\":null,\"siteProductId\":null,\"options\":\"Black\",\"itemPrice\":\"49.99\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"59.99\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'paid','2018-09-11 06:50:10','0',NULL,NULL),(16,'RECPNG5-13-20180911',3,13,'buycarforme','receipt','N','NG5','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,' Lagos Island','Lagos','Nigeria','77479','713-530-1160',NULL,7049.99,'{\"shipment\":{\"totalItemCost\":\"5995\",\"totalProcessingFee\":\"419.65\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"15.55\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":6414.65,\"insuranceCost\":586.31,\"taxCost\":20,\"discount\":\"10.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"1\",\"paymentMethodName\":\"Credit \\/ Debit Cards\",\"paymentMethodKey\":\"credit_debit_card\",\"cardNumber\":\"4111111111111111\",\"cartType\":\"VISA\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Lagos\",\"toCity\":\" Lagos Island\",\"toAddress\":\"211 South Meadows Court\",\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"}}',1,'paid','2018-09-11 07:00:33','0',NULL,NULL),(17,'RECNG5-17-20180911',NULL,17,'shopforme','receipt','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,129.98,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"109.98\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"109.98\",\"totalTax\":20,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":129.98000000000002,\"totalWeight\":0,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"id\":11,\"itemName\":\"Echo Dot (2nd Generation)\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/dp\\/B01DFKC2SO\\/ref=ods_gw_ha_d_bt_propoff_090418?pf_rd_p=5f8214f2-ab41-4000-9a4e-298f4e650978&pf_rd_r=PN8RJ2RCDZ5JXF420N3V\",\"storeId\":\"1\",\"siteCategoryId\":null,\"siteSubCategoryId\":null,\"siteProductId\":null,\"options\":\"Black\",\"itemPrice\":\"49.99\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"109.98\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'paid','2018-09-11 07:09:18','0',NULL,NULL),(18,'INVNG5-4-20180911',4,NULL,'autoshipment','invoice','N','NG5','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. somnath pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,' Lagos Island','Lagos','Nigeria','77479','713-530-1160',NULL,39.03,'{\"shipment\":{\"totalItemCost\":\"5995\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"15.55\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":\"0.00\",\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"7\",\"paymentMethodName\":\"E-Wallet\",\"paymentMethodKey\":\"ewallet\",\"poNumber\":\"\",\"companyName\":\"\",\"buyerName\":\"\",\"position\":\"\",\"cardNumber\":\"\",\"cardType\":\"\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Lagos\",\"toCity\":\"Lagos Island\",\"toAddress\":\"Landmark Towers , 5B Water Corporation Road , Oniru Estate, Victoria Island\",\"toZipCode\":\"\",\"toName\":\"John Mile\",\"toEmail\":\"johnmile@gmail.com\",\"toPhone\":\"56984123\"}}',7,'paid','2018-09-11 07:19:32','0',NULL,NULL),(19,'INVNG5-19-20180911',NULL,19,'autopart','invoice','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Somnath Pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,1648.56,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"464.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":464,\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":1648.5599999999997,\"totalWeight\":116,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"procurementId\":19,\"itemName\":\"Tires\",\"websiteUrl\":\"https:\\/\\/www.carid.com\\/auto-parts.html\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"53\",\"siteProductId\":\"68\",\"itemDescription\":\"NT421Q Tires by Nitto\\u00ae. Season: All Season. Type: Performance, Truck \\/ SUV. This NT421Q tire is Nitto\\u2019s premier all-season tire built specifically for your crossover or SUV. From style to function, this tire has been created with the...\\nIntelligent Maintenance Indicators\",\"year\":\"2017\",\"itemMake\":\"BMW\",\"itemModel\":\"Q7\",\"itemPrice\":\"116.00\",\"itemQuantity\":4,\"itemShippingCost\":\"0.00\",\"itemTotalCost\":\"464.00\"}],\"payment\":{\"paymentMethodId\":7,\"paymentMethodName\":\"E-Wallet\",\"ewalletId\":5},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-09-27\",\"isDutyCharged\":\"Y\",\"shippingCost\":615.03,\"totalClearingDuty\":523.18,\"totalShippingCost\":1138.21}}',7,'paid','2018-09-11 07:37:50','0',NULL,NULL),(20,'INVNG5-20-20180911',NULL,20,'autopart','invoice','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Somnath Pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,362.23,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"232.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":232,\"totalTax\":16.985999999999997,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":362.226,\"totalWeight\":116,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"procurementId\":20,\"itemName\":\"Tires\",\"websiteUrl\":\"https:\\/\\/www.carid.com\\/auto-parts.html\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"53\",\"siteProductId\":\"68\",\"itemDescription\":\"Test\",\"year\":\"2017\",\"itemMake\":\"BMW\",\"itemModel\":\"Q7\",\"itemPrice\":\"116.00\",\"itemQuantity\":2,\"itemShippingCost\":\"0.00\",\"itemTotalCost\":\"232.00\"}],\"payment\":{\"paymentMethodId\":8,\"paymentMethodName\":\"Wire Transfer\",\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":192,\"shipping\":\"Standard Shipping (Sea)\",\"estimateDeliveryDate\":\"2018-11-20\",\"isDutyCharged\":\"N\",\"shippingCost\":113.24,\"totalClearingDuty\":0,\"totalShippingCost\":113.24}}',8,'paid','2018-09-11 07:51:48','0',NULL,NULL),(21,'INVNG-21-20180911',NULL,21,'shopforme','invoice','N','NG','Mr. Nduka Udeh','n.udeh@aascargo.com','123456','Nduka Udeh','n.udeh@shoptomydoor.com','12011 Westbrae Parkway',NULL,'378','5','163','77031','7135301160',NULL,248.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"228.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"228.00\",\"totalTax\":20,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":248,\"totalWeight\":6,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":5,\"toCity\":378,\"toAddress\":\"12011 Westbrae Parkway\",\"toAlternateAddress\":null,\"toZipCode\":\"77031\",\"toName\":\"Nduka Udeh\",\"toEmail\":\"n.udeh@shoptomydoor.com\",\"toPhone\":\"7135301160\"},\"packages\":[{\"id\":15,\"itemName\":\"Fire HD 10 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue\\/dp\\/B01M6YJEAH\\/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Red\",\"itemPrice\":\"100.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"110.00\"},{\"id\":16,\"itemName\":\"Fire 7 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/dp\\/B01IO618J8\\/ref=fs_ods_tab_an\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Red\",\"itemPrice\":\"49.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"20.00\",\"itemTotalCost\":\"118.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'unpaid','2018-09-11 09:14:47','0',NULL,NULL),(22,'INVNG-22-20180911',NULL,22,'shopforme','invoice','N','NG','Mr. Nduka Udeh','n.udeh@aascargo.com','123456','Nduka Udeh','n.udeh@shoptomydoor.com','12011 Westbrae Parkway',NULL,'378','5','163','77031','7135301160',NULL,94.00,'{\"shipment\":{\"urgent\":\"Y\",\"totalItemCost\":\"69.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"5.00\",\"totalProcurementCost\":\"74.00\",\"totalTax\":20,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":94,\"totalWeight\":3,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":5,\"toCity\":378,\"toAddress\":\"12011 Westbrae Parkway\",\"toAlternateAddress\":null,\"toZipCode\":\"77031\",\"toName\":\"Nduka Udeh\",\"toEmail\":\"n.udeh@shoptomydoor.com\",\"toPhone\":\"7135301160\"},\"packages\":[{\"id\":17,\"itemName\":\"Fire 7 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/dp\\/B01IO618J8\\/ref=fs_ods_tab_an\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Red\",\"itemPrice\":\"49.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"20.00\",\"itemTotalCost\":\"69.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'unpaid','2018-09-11 09:21:54','0',NULL,NULL),(23,'INVNG-22-20180911',NULL,22,'shopforme','receipt','N','NG','Mr. Nduka Udeh','n.udeh@aascargo.com','123456','Nduka Udeh','n.udeh@shoptomydoor.com','12011 Westbrae Parkway',NULL,'378','5','163','77031','7135301160',NULL,94.00,'{\"shipment\":{\"urgent\":\"Y\",\"totalItemCost\":\"69.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"5.00\",\"totalProcurementCost\":\"74.00\",\"totalTax\":20,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":94,\"totalWeight\":3,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":5,\"toCity\":378,\"toAddress\":\"12011 Westbrae Parkway\",\"toAlternateAddress\":null,\"toZipCode\":\"77031\",\"toName\":\"Nduka Udeh\",\"toEmail\":\"n.udeh@shoptomydoor.com\",\"toPhone\":\"7135301160\"},\"packages\":[{\"id\":17,\"itemName\":\"Fire 7 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/dp\\/B01IO618J8\\/ref=fs_ods_tab_an\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Red\",\"itemPrice\":\"49.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"20.00\",\"itemTotalCost\":\"69.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'paid','2018-09-11 09:21:54','0',NULL,NULL),(24,'INVNG6-23-20180912',NULL,23,'buycarforme','invoice','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,71081.48,'{\"shipment\":{\"totalItemCost\":\"60000\",\"totalProcessingFee\":\"4200\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"990\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":64200,\"insuranceCost\":5868,\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',2,'unpaid','2018-09-12 11:26:11','1',1,'2018-09-12 11:33:40'),(25,'RECPNG6-23-20180912',NULL,23,'buycarforme','receipt','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,71081.48,'{\"shipment\":{\"totalItemCost\":\"60000\",\"totalProcessingFee\":\"4200\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"990\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":64200,\"insuranceCost\":5868,\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Arizona\",\"fromCity\":\"Tuscon\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',2,'unpaid','2018-09-12 11:33:40','0',NULL,NULL),(26,'INVNG6-24-20180912',NULL,24,'buycarforme','invoice','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,53403.39,'{\"shipment\":{\"totalItemCost\":\"45667\",\"totalProcessingFee\":\"3196.69\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":48863.69,\"insuranceCost\":4466.23,\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',2,'unpaid','2018-09-12 11:49:05','1',1,'2018-09-12 11:49:36'),(27,'RECPNG6-24-20180912',NULL,24,'buycarforme','receipt','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,53403.39,'{\"shipment\":{\"totalItemCost\":\"45667\",\"totalProcessingFee\":\"3196.69\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":48863.69,\"insuranceCost\":4466.23,\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',2,'unpaid','2018-09-12 11:49:36','0',NULL,NULL),(28,'INVNG6-25-20180912',NULL,25,'buycarforme','invoice','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,85954.64,'{\"shipment\":{\"totalItemCost\":\"80000\",\"totalProcessingFee\":\"5600\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":85600,\"insuranceCost\":\"0.00\",\"taxCost\":281.17,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"6\",\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\",\"paymentMethodKey\":\"bank_account_pay\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',6,'unpaid','2018-09-12 11:55:56','1',1,'2018-09-12 11:56:32'),(29,'RECPNG6-25-20180912',NULL,25,'buycarforme','receipt','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,85954.64,'{\"shipment\":{\"totalItemCost\":\"80000\",\"totalProcessingFee\":\"5600\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":85600,\"insuranceCost\":\"0.00\",\"taxCost\":281.17,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"6\",\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\",\"paymentMethodKey\":\"bank_account_pay\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',6,'unpaid','2018-09-12 11:56:32','0',NULL,NULL),(30,'INVNG6-26-20180912',NULL,26,'buycarforme','invoice','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,26823.47,'{\"shipment\":{\"totalItemCost\":\"25000\",\"totalProcessingFee\":\"1750\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":26750,\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',2,'unpaid','2018-09-12 11:59:18','0',NULL,NULL),(31,'INVNG6-27-20180912',6,27,'buycarforme','invoice','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,26823.47,'{\"shipment\":{\"totalItemCost\":\"25000\",\"totalProcessingFee\":\"1750\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":26750,\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',2,'unpaid','2018-09-12 11:59:22','1',1,'2018-09-12 12:01:56'),(32,'RECPNG6-27-20180912',6,27,'buycarforme','receipt','N','NG6','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,26823.47,'{\"shipment\":{\"totalItemCost\":\"25000\",\"totalProcessingFee\":\"1750\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":26750,\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',2,'unpaid','2018-09-12 12:01:56','0',NULL,NULL),(33,'INVNG8-8-20180912',8,NULL,'autoshipment','invoice','N','NG8','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,73.47,'{\"shipment\":{\"totalItemCost\":\"1000.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":\"0.00\",\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\",\"poNumber\":\"\",\"companyName\":\"\",\"buyerName\":\"\",\"position\":\"\",\"cardNumber\":\"\",\"cardType\":\"\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Abia\",\"toCity\":\"Aba\",\"toAddress\":\"Address 1\",\"toZipCode\":\"\",\"toName\":\"Alder demo\",\"toEmail\":\"testuser@example.com\",\"toPhone\":\"242342450\"}}',2,'unpaid','2018-09-12 12:47:10','1',1,'2018-09-12 12:49:40'),(34,'RECPNG8-8-20180912',8,NULL,'autoshipment','receipt','N','NG8','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,73.47,'{\"shipment\":{\"totalItemCost\":\"1000.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":\"0.00\",\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\",\"poNumber\":\"\",\"companyName\":\"\",\"buyerName\":\"\",\"position\":\"\",\"cardNumber\":\"\",\"cardType\":\"\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Abia\",\"toCity\":\"Aba\",\"toAddress\":\"Address 1\",\"toZipCode\":\"\",\"toName\":\"Alder demo\",\"toEmail\":\"testuser@example.com\",\"toPhone\":\"242342450\"}}',2,'unpaid','2018-09-12 12:49:40','0',NULL,NULL),(35,'INVNG8-28-20180912',NULL,28,'autopart','invoice','N','NG8','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Somnath Pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,516.20,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"300.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":300,\"totalTax\":28.2,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":516.2,\"totalWeight\":200,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":15,\"toCity\":638,\"toAddress\":\"Address 1\",\"toAlternateAddress\":null,\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"},\"packages\":[{\"procurementId\":28,\"itemName\":\"Custom HeadLights\",\"websiteUrl\":\"https:\\/\\/www.carid.com\\/auto-parts.html\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"55\",\"siteProductId\":\"70\",\"itemDescription\":\"Custom HeadLights\",\"year\":\"2017\",\"itemMake\":\"BMW\",\"itemModel\":\"Q7\",\"itemPrice\":\"100.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"100.00\",\"itemTotalCost\":\"300.00\"}],\"payment\":{\"paymentMethodId\":8,\"paymentMethodName\":\"Wire Transfer\",\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":192,\"shipping\":\"Standard Shipping (Sea)\",\"estimateDeliveryDate\":\"2018-11-21\",\"isDutyCharged\":\"N\",\"shippingCost\":188,\"totalClearingDuty\":0,\"totalShippingCost\":188}}',8,'unpaid','2018-09-12 13:25:45','0',NULL,NULL),(36,'INVNG8-28-20180912',NULL,28,'autopart','receipt','N','NG8','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Somnath Pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,516.20,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"300.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":300,\"totalTax\":28.2,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":516.2,\"totalWeight\":200,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":15,\"toCity\":638,\"toAddress\":\"Address 1\",\"toAlternateAddress\":null,\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"},\"packages\":[{\"procurementId\":28,\"itemName\":\"Custom HeadLights\",\"websiteUrl\":\"https:\\/\\/www.carid.com\\/auto-parts.html\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"55\",\"siteProductId\":\"70\",\"itemDescription\":\"Custom HeadLights\",\"year\":\"2017\",\"itemMake\":\"BMW\",\"itemModel\":\"Q7\",\"itemPrice\":\"100.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"100.00\",\"itemTotalCost\":\"300.00\"}],\"payment\":{\"paymentMethodId\":8,\"paymentMethodName\":\"Wire Transfer\",\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":192,\"shipping\":\"Standard Shipping (Sea)\",\"estimateDeliveryDate\":\"2018-11-21\",\"isDutyCharged\":\"N\",\"shippingCost\":188,\"totalClearingDuty\":0,\"totalShippingCost\":188}}',8,'paid','2018-09-12 13:25:45','0',NULL,NULL),(37,'INVNG9-29-20180912',NULL,29,'buycarforme','invoice','N','NG9','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,26823.47,'{\"shipment\":{\"totalItemCost\":\"25000\",\"totalProcessingFee\":\"1750\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":26750,\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Edo\",\"toCity\":\"Akoko-Edo\",\"toAddress\":\"Address 1\",\"toZipCode\":\"67676\",\"toName\":\"Paulami Sarkar\",\"toEmail\":\"paulami.sarkar@indusnet.co.in\",\"toPhone\":\"6767\"}}',2,'unpaid','2018-09-12 13:36:59','0',NULL,NULL),(38,'INVNG9-9-20180912',9,NULL,'autoshipment','invoice','N','NG9','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,73.47,'{\"shipment\":{\"totalItemCost\":\"100000.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":\"0.00\",\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\",\"poNumber\":\"\",\"companyName\":\"\",\"buyerName\":\"\",\"position\":\"\",\"cardNumber\":\"\",\"cardType\":\"\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Abia\",\"toCity\":\"Aba\",\"toAddress\":\"Address 1\",\"toZipCode\":\"\",\"toName\":\"Alder demo\",\"toEmail\":\"testuser@example2.com\",\"toPhone\":\"242342466565\"}}',2,'unpaid','2018-09-12 13:42:27','1',1,'2018-09-12 13:46:12'),(39,'RECPNG9-9-20180912',9,NULL,'autoshipment','receipt','N','NG9','Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Mr. Paulami Sarkar','paulami.sarkar@indusnet.co.in','Address 1',NULL,'Akoko-Edo','Edo','Nigeria','67676','6767',NULL,73.47,'{\"shipment\":{\"totalItemCost\":\"100000.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"pickupCost\":\"49.99\",\"shippingCost\":\"23.48\",\"totalProcurementCost\":\"0.00\",\"insuranceCost\":\"0.00\",\"taxCost\":0,\"discount\":\"0.00\",\"point\":null,\"currencySymbol\":\"$\",\"currencyCode\":\"USD\",\"exchangeRate\":1},\"payment\":{\"paymentMethodId\":\"2\",\"paymentMethodName\":\"Check\\/Cash In Office (US Only)\",\"paymentMethodKey\":\"check_cash_payment\",\"poNumber\":\"\",\"companyName\":\"\",\"buyerName\":\"\",\"position\":\"\",\"cardNumber\":\"\",\"cardType\":\"\"},\"warehouse\":{\"fromCountry\":\"United States\",\"fromState\":\"Texas\",\"fromCity\":\"Houston\"},\"shippingaddress\":{\"toCountry\":\"Nigeria\",\"toState\":\"Abia\",\"toCity\":\"Aba\",\"toAddress\":\"Address 1\",\"toZipCode\":\"\",\"toName\":\"Alder demo\",\"toEmail\":\"testuser@example2.com\",\"toPhone\":\"242342466565\"}}',2,'unpaid','2018-09-12 13:46:12','0',NULL,NULL),(40,'INVNG-21-20180911',NULL,21,'shopforme','receipt','N','NG','Mr. Nduka Udeh','n.udeh@aascargo.com','123456','Nduka Udeh','n.udeh@shoptomydoor.com','12011 Westbrae Parkway',NULL,'378','5','163','77031','7135301160',NULL,248.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"228.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"228.00\",\"totalTax\":20,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":248,\"totalWeight\":6,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":5,\"toCity\":378,\"toAddress\":\"12011 Westbrae Parkway\",\"toAlternateAddress\":null,\"toZipCode\":\"77031\",\"toName\":\"Nduka Udeh\",\"toEmail\":\"n.udeh@shoptomydoor.com\",\"toPhone\":\"7135301160\"},\"packages\":[{\"id\":15,\"itemName\":\"Fire HD 10 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue\\/dp\\/B01M6YJEAH\\/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Red\",\"itemPrice\":\"100.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"110.00\"},{\"id\":16,\"itemName\":\"Fire 7 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/dp\\/B01IO618J8\\/ref=fs_ods_tab_an\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Red\",\"itemPrice\":\"49.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"20.00\",\"itemTotalCost\":\"118.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'paid','2018-09-11 09:14:47','0',NULL,NULL),(41,'INVNG5-20-20180911',NULL,20,'autopart','receipt','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Somnath Pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,362.23,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"232.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":232,\"totalTax\":16.985999999999997,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":362.226,\"totalWeight\":116,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"procurementId\":20,\"itemName\":\"Tires\",\"websiteUrl\":\"https:\\/\\/www.carid.com\\/auto-parts.html\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"53\",\"siteProductId\":\"68\",\"itemDescription\":\"Test\",\"year\":\"2017\",\"itemMake\":\"BMW\",\"itemModel\":\"Q7\",\"itemPrice\":\"116.00\",\"itemQuantity\":2,\"itemShippingCost\":\"0.00\",\"itemTotalCost\":\"232.00\"}],\"payment\":{\"paymentMethodId\":8,\"paymentMethodName\":\"Wire Transfer\",\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":192,\"shipping\":\"Standard Shipping (Sea)\",\"estimateDeliveryDate\":\"2018-11-20\",\"isDutyCharged\":\"N\",\"shippingCost\":113.24,\"totalClearingDuty\":0,\"totalShippingCost\":113.24}}',8,'paid','2018-09-11 07:51:48','0',NULL,NULL),(42,'INVUS2-30-20180913',NULL,30,'shopforme','invoice','N','US2','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608','Joy Karmakar','karmakar.joy@gmail.com','SDF Building',NULL,'783','90','230','852000','9831723608',NULL,218.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"218.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"218.00\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":\"218.00\",\"totalWeight\":0,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":230,\"toState\":90,\"toCity\":\"588\",\"toAddress\":\"SDF Building\",\"toAlternateAddress\":null,\"toZipCode\":\"852000\",\"toName\":\"Joy Karmakar\",\"toEmail\":\"karmakar.joy@gmail.com\",\"toPhone\":\"9831723608\"},\"packages\":[{\"id\":25,\"itemName\":\"Fire HD 10 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue\\/dp\\/B01M6YJEAH\\/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":null,\"siteProductId\":null,\"options\":\"Red\",\"itemPrice\":\"149.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"159.00\"},{\"id\":26,\"itemName\":\"Fire 7 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/Apple-iPad-WiFi-2018-Model\\/dp\\/B07D5S5LHF\\/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":null,\"siteProductId\":null,\"options\":\"Red\",\"itemPrice\":\"49.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"59.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'unpaid','2018-09-13 07:55:16','0',NULL,NULL),(43,'INVUS2-30-20180913',NULL,30,'shopforme','receipt','N','US2','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608','Joy Karmakar','karmakar.joy@gmail.com','SDF Building',NULL,'783','90','230','852000','9831723608',NULL,218.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"218.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"218.00\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":\"218.00\",\"totalWeight\":0,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":230,\"toState\":90,\"toCity\":\"588\",\"toAddress\":\"SDF Building\",\"toAlternateAddress\":null,\"toZipCode\":\"852000\",\"toName\":\"Joy Karmakar\",\"toEmail\":\"karmakar.joy@gmail.com\",\"toPhone\":\"9831723608\"},\"packages\":[{\"id\":25,\"itemName\":\"Fire HD 10 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue\\/dp\\/B01M6YJEAH\\/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":null,\"siteProductId\":null,\"options\":\"Red\",\"itemPrice\":\"149.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"159.00\"},{\"id\":26,\"itemName\":\"Fire 7 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/Apple-iPad-WiFi-2018-Model\\/dp\\/B07D5S5LHF\\/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":null,\"siteProductId\":null,\"options\":\"Red\",\"itemPrice\":\"49.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"59.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'paid','2018-09-13 07:55:16','0',NULL,NULL),(44,'INVUS2-33-20180913',NULL,33,'autopart','invoice','N','US2','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608','Joy Karmakar','karmakar.joy@gmail.com','SDF Building',NULL,'588','90','230','852000','9831723608',NULL,179.00,'{\"shipment\":{\"urgent\":\"Y\",\"totalItemCost\":\"159.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":159,\"totalTax\":20,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":179,\"totalWeight\":3.5,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":230,\"toState\":90,\"toCity\":\"522\",\"toAddress\":\"SDF Building\",\"toAlternateAddress\":null,\"toZipCode\":\"852000\",\"toName\":\"Joy Karmakar\",\"toEmail\":\"karmakar.joy@gmail.com\",\"toPhone\":\"9831723608\"},\"packages\":[{\"procurementId\":33,\"itemName\":\"GOOACC Nylon Bumper\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue\\/dp\\/B01M6YJEAH\\/ref=sr_1_1_sspa?ie=UTF8\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"38\",\"siteProductId\":\"48\",\"itemDescription\":\"GOOACC Nylon Bumper\",\"year\":\"2011\",\"itemMake\":\"Audi\",\"itemModel\":\"Model\",\"itemPrice\":\"149.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"159.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'unpaid','2018-09-13 08:17:10','0',NULL,NULL),(45,'INVUS2-34-20180913',NULL,34,'autopart','invoice','N','US2','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608','Joy Karmakar','karmakar.joy@gmail.com','SDF Building',NULL,'275','90','230','852000','9831723608',NULL,480.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"460.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":460,\"totalTax\":20,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":480,\"totalWeight\":109,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":230,\"toState\":90,\"toCity\":\"239\",\"toAddress\":\"SDF Building\",\"toAlternateAddress\":null,\"toZipCode\":\"852000\",\"toName\":\"Joy Karmakar\",\"toEmail\":\"karmakar.joy@gmail.com\",\"toPhone\":\"9831723608\"},\"packages\":[{\"procurementId\":34,\"itemName\":\"Car Tire\",\"websiteUrl\":\"https:\\/\\/www.carid.com\\/tires\\/all-season\\/\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"53\",\"siteProductId\":\"67\",\"itemDescription\":\"TERRAIN GRIPPER A\\/T G Tires by AMP\\u00ae. Season: All Season. Type: Truck \\/ SUV, All Terrain \\/ Off Road \\/ Mud. AMP Tires is proud to introduce the latest addition to its line of off-road light truck tires - the all\\u2013new terrain Gripper A\\/T more details on - https:\\/\\/www.carid.com\\/tires\\/all-season\\/\",\"year\":\"2017\",\"itemMake\":\"Audi\",\"itemModel\":\"Q7\",\"itemPrice\":\"115.00\",\"itemQuantity\":\"4\",\"itemShippingCost\":\"0.00\",\"itemTotalCost\":\"460.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'paid','2018-09-13 08:23:21','0',NULL,NULL),(46,'INVUS2-35-20180913',NULL,35,'shopforme','invoice','N','US2','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608','Joy Karmakar','karmakar.joy@gmail.com','SDF Building',NULL,'588','90','230','852000','9831723608',NULL,369.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"369.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"369.00\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":\"369.00\",\"totalWeight\":6,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":230,\"toState\":90,\"toCity\":\"588\",\"toAddress\":\"SDF Building\",\"toAlternateAddress\":null,\"toZipCode\":\"852000\",\"toName\":\"Joy Karmakar\",\"toEmail\":\"karmakar.joy@gmail.com\",\"toPhone\":\"9831723608\"},\"packages\":[{\"id\":31,\"itemName\":\"Fire HD 10 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue\\/dp\\/B01M6YJEAH\\/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"RED\",\"itemPrice\":\"149.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"159.00\"},{\"id\":32,\"itemName\":\"Fire 7 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/Apple-iPad-WiFi-2018-Model\\/dp\\/B07D5S5LHF\\/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Red\",\"itemPrice\":\"200.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"210.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'unpaid','2018-09-13 08:53:41','0',NULL,NULL),(47,'INVUS2-35-20180913',NULL,35,'shopforme','receipt','N','US2','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608','Joy Karmakar','karmakar.joy@gmail.com','SDF Building',NULL,'588','90','230','852000','9831723608',NULL,369.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"369.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"369.00\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":\"369.00\",\"totalWeight\":6,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":230,\"toState\":90,\"toCity\":\"588\",\"toAddress\":\"SDF Building\",\"toAlternateAddress\":null,\"toZipCode\":\"852000\",\"toName\":\"Joy Karmakar\",\"toEmail\":\"karmakar.joy@gmail.com\",\"toPhone\":\"9831723608\"},\"packages\":[{\"id\":31,\"itemName\":\"Fire HD 10 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue\\/dp\\/B01M6YJEAH\\/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"RED\",\"itemPrice\":\"149.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"159.00\"},{\"id\":32,\"itemName\":\"Fire 7 Tablet\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/Apple-iPad-WiFi-2018-Model\\/dp\\/B07D5S5LHF\\/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"Red\",\"itemPrice\":\"200.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"210.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'paid','2018-09-13 08:53:41','0',NULL,NULL),(48,'INVUS2-36-20180913',NULL,36,'autopart','invoice','N','US2','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608','Joy Karmakar','karmakar.joy@gmail.com','SDF Building',NULL,'667','90','230','852000','9831723608',NULL,75.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"55.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":55,\"totalTax\":20,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":75,\"totalWeight\":109,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":230,\"toState\":90,\"toCity\":\"588\",\"toAddress\":\"SDF Building\",\"toAlternateAddress\":null,\"toZipCode\":\"852000\",\"toName\":\"Joy Karmakar\",\"toEmail\":\"karmakar.joy@gmail.com\",\"toPhone\":\"9831723608\"},\"packages\":[{\"procurementId\":36,\"itemName\":\"terrain-gripper\",\"websiteUrl\":\"https:\\/\\/www.carid.com\\/amp-tires\\/terrain-gripper-at-g-84753193.html\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"53\",\"siteProductId\":\"67\",\"itemDescription\":\"terrain-gripper\",\"year\":\"2014\",\"itemMake\":\"Make\",\"itemModel\":\"Model\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"5.00\",\"itemTotalCost\":\"55.00\"}],\"payment\":{\"paymentMethodId\":1,\"paymentMethodName\":\"Credit \\/ Debit Cards\"}}',1,'paid','2018-09-13 09:11:04','0',NULL,NULL),(49,'INVNG15-38-20180920',NULL,38,'shopforme','invoice','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,353.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"353.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"353.00\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":353,\"totalWeight\":7,\"totalQuantity\":4},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":35,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"red\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"3\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"151.00\"},{\"id\":36,\"itemName\":\"iphone 7plus\",\"websiteUrl\":\"amazon.com\\/iphone\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"40\",\"siteProductId\":\"16\",\"options\":\"grey\",\"itemPrice\":\"75.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"5.00\",\"itemTotalCost\":\"155.00\"},{\"id\":37,\"itemName\":\"rayban sunglass\",\"websiteUrl\":\"amazon.com\\/sunglass\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"2\",\"options\":\"blue\",\"itemPrice\":\"10.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"21.00\"},{\"id\":38,\"itemName\":\"levis jeans\",\"websiteUrl\":\"amazon.com\\/jeans\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"5\",\"options\":\"light blue\",\"itemPrice\":\"8.00\",\"itemQuantity\":\"3\",\"itemShippingCost\":\"2.00\",\"itemTotalCost\":\"26.00\"}],\"payment\":{\"paymentMethodId\":6,\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\"}}',6,'unpaid','2018-09-20 12:56:55','0',NULL,NULL),(50,'INVNG15-39-20180920',NULL,39,'shopforme','invoice','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,353.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"353.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"353.00\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":353,\"totalWeight\":7,\"totalQuantity\":4},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":39,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"red\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"3\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"151.00\"},{\"id\":40,\"itemName\":\"iphone 7plus\",\"websiteUrl\":\"amazon.com\\/iphone\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"40\",\"siteProductId\":\"16\",\"options\":\"grey\",\"itemPrice\":\"75.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"5.00\",\"itemTotalCost\":\"155.00\"},{\"id\":41,\"itemName\":\"rayban sunglass\",\"websiteUrl\":\"amazon.com\\/sunglass\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"2\",\"options\":\"blue\",\"itemPrice\":\"10.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"21.00\"},{\"id\":42,\"itemName\":\"levis jeans\",\"websiteUrl\":\"amazon.com\\/jeans\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"5\",\"options\":\"light blue\",\"itemPrice\":\"8.00\",\"itemQuantity\":\"3\",\"itemShippingCost\":\"2.00\",\"itemTotalCost\":\"26.00\"}],\"payment\":{\"paymentMethodId\":6,\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\"}}',6,'unpaid','2018-09-20 12:58:39','0',NULL,NULL),(51,'INVNG15-40-20180920',NULL,40,'shopforme','invoice','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,353.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"353.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"353.00\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":353,\"totalWeight\":7,\"totalQuantity\":4},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":43,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"red\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"3\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"151.00\"},{\"id\":44,\"itemName\":\"iphone 7plus\",\"websiteUrl\":\"amazon.com\\/iphone\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"40\",\"siteProductId\":\"16\",\"options\":\"grey\",\"itemPrice\":\"75.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"5.00\",\"itemTotalCost\":\"155.00\"},{\"id\":45,\"itemName\":\"rayban sunglass\",\"websiteUrl\":\"amazon.com\\/sunglass\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"2\",\"options\":\"blue\",\"itemPrice\":\"10.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"21.00\"},{\"id\":46,\"itemName\":\"levis jeans\",\"websiteUrl\":\"amazon.com\\/jeans\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"5\",\"options\":\"light blue\",\"itemPrice\":\"8.00\",\"itemQuantity\":\"3\",\"itemShippingCost\":\"2.00\",\"itemTotalCost\":\"26.00\"}],\"payment\":{\"paymentMethodId\":6,\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\"}}',6,'unpaid','2018-09-20 12:59:43','0',NULL,NULL),(52,'INVNG15-40-20180920',NULL,40,'shopforme','receipt','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,353.00,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"353.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"353.00\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":353,\"totalWeight\":7,\"totalQuantity\":4},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":43,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"red\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"3\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"151.00\"},{\"id\":44,\"itemName\":\"iphone 7plus\",\"websiteUrl\":\"amazon.com\\/iphone\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"40\",\"siteProductId\":\"16\",\"options\":\"grey\",\"itemPrice\":\"75.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"5.00\",\"itemTotalCost\":\"155.00\"},{\"id\":45,\"itemName\":\"rayban sunglass\",\"websiteUrl\":\"amazon.com\\/sunglass\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"2\",\"options\":\"blue\",\"itemPrice\":\"10.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"21.00\"},{\"id\":46,\"itemName\":\"levis jeans\",\"websiteUrl\":\"amazon.com\\/jeans\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"5\",\"options\":\"light blue\",\"itemPrice\":\"8.00\",\"itemQuantity\":\"3\",\"itemShippingCost\":\"2.00\",\"itemTotalCost\":\"26.00\"}],\"payment\":{\"paymentMethodId\":6,\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\"}}',6,'paid','2018-09-20 12:59:43','0',NULL,NULL),(53,'INVNG5-9-20180830',24,NULL,'othershipment','invoice','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Somnath Pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,2015.09,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"2000.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":2000,\"totalTax\":1.9679999999999997,\"isInsuranceCharged\":\"Y\",\"totalInsurance\":0,\"totalCost\":2015.088,\"totalWeight\":3.5,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"procurementId\":9,\"itemName\":\"Car radio\",\"websiteUrl\":\"http:\\/\\/www.flipcart.com\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"38\",\"siteProductId\":\"48\",\"itemDescription\":\"Test Description\",\"year\":\"2018\",\"itemMake\":\"WO\",\"itemModel\":\"Q7\",\"itemPrice\":\"200.00\",\"itemQuantity\":10,\"itemShippingCost\":\"0.00\",\"itemTotalCost\":\"2000.00\"}],\"payment\":{\"paymentMethodId\":8,\"paymentMethodName\":\"Wire Transfer\",\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null}}',8,'unpaid','2018-08-30 13:04:35','0',NULL,NULL),(54,'INVNG15-41-20180921',22,41,'shopforme','invoice','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,638.79,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"502.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"502.00\",\"totalTax\":6.51,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":638.79,\"totalWeight\":3,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":47,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"silver\",\"itemPrice\":\"100.00\",\"itemQuantity\":\"5\",\"itemShippingCost\":\"2.00\",\"itemTotalCost\":\"502.00\"}],\"payment\":{\"paymentMethodId\":5,\"paymentMethodName\":\"GTBank : Pay at Bank : Act# : 0121339039\"},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-10-07\",\"isDutyCharged\":\"Y\",\"shippingCost\":55.37,\"totalClearingDuty\":74.91,\"totalShippingCost\":130.28}}',5,'unpaid','2018-09-21 17:53:07','0',NULL,NULL),(55,'INVNG15-41-20180921',22,41,'shopforme','receipt','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,638.79,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"502.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":\"502.00\",\"totalTax\":6.51,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":638.79,\"totalWeight\":3,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":47,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"silver\",\"itemPrice\":\"100.00\",\"itemQuantity\":\"5\",\"itemShippingCost\":\"2.00\",\"itemTotalCost\":\"502.00\"}],\"payment\":{\"paymentMethodId\":5,\"paymentMethodName\":\"GTBank : Pay at Bank : Act# : 0121339039\"},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-10-07\",\"isDutyCharged\":\"Y\",\"shippingCost\":55.37,\"totalClearingDuty\":74.91,\"totalShippingCost\":130.28}}',5,'paid','2018-09-21 17:53:07','0',NULL,NULL),(56,'RECPNG5-24-20180925',24,24,'othershipment','receipt','N','NG5','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456','Somnath Pahari','somnath.pahari@indusnet.co.in','211 South Meadows Court',NULL,'639','2','163','77479','713-530-1160',NULL,2015.09,'{\"shipment\":{\"urgent\":\"N\",\"totalItemCost\":\"2000.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"0.00\",\"totalProcurementCost\":2000,\"totalTax\":1.9679999999999997,\"isInsuranceCharged\":\"Y\",\"totalInsurance\":0,\"totalCost\":2015.088,\"totalWeight\":3.5,\"totalQuantity\":1},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":2,\"toCity\":639,\"toAddress\":\"211 South Meadows Court\",\"toAlternateAddress\":null,\"toZipCode\":\"77479\",\"toName\":\"somnath pahari\",\"toEmail\":\"somnath.pahari@indusnet.co.in\",\"toPhone\":\"713-530-1160\"},\"packages\":[{\"procurementId\":9,\"itemName\":\"Car radio\",\"websiteUrl\":\"http:\\/\\/www.flipcart.com\",\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"38\",\"siteProductId\":\"48\",\"itemDescription\":\"Test Description\",\"year\":\"2018\",\"itemMake\":\"WO\",\"itemModel\":\"Q7\",\"itemPrice\":\"200.00\",\"itemQuantity\":10,\"itemShippingCost\":\"0.00\",\"itemTotalCost\":\"2000.00\"}],\"payment\":{\"paymentMethodId\":8,\"paymentMethodName\":\"Wire Transfer\",\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null}}',8,'paid','2018-09-25 18:25:10','0',NULL,NULL),(57,'INVNG15-42-20180926',NULL,42,'shopforme','invoice','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,1446.87,'{\"shipment\":{\"urgent\":\"Y\",\"totalItemCost\":\"1103.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"11.03\",\"totalProcurementCost\":\"1114.03\",\"totalTax\":10.55,\"isInsuranceCharged\":\"Y\",\"totalInsurance\":111.29,\"totalCost\":1446.87,\"totalWeight\":5.5,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":48,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"red\",\"itemPrice\":\"10.00\",\"itemQuantity\":\"5\",\"itemShippingCost\":\"3.00\",\"itemTotalCost\":\"53.00\"},{\"id\":49,\"itemName\":\"nike shoe\",\"websiteUrl\":\"www.amazon.com\\/nike\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"10\",\"options\":\"flurocent green\",\"itemPrice\":\"520.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"1050.00\"}],\"payment\":{\"paymentMethodId\":6,\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\"},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-10-12\",\"isDutyCharged\":\"Y\",\"shippingCost\":65.97,\"totalClearingDuty\":145.03,\"totalShippingCost\":211}}',6,'unpaid','2018-09-26 15:01:07','0',NULL,NULL),(58,'INVNG15-43-20180926',NULL,43,'shopforme','invoice','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,576.71,'{\"shipment\":{\"urgent\":\"Y\",\"totalItemCost\":\"571.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"5.71\",\"totalProcurementCost\":\"576.71\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":576.71,\"totalWeight\":2,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":50,\"itemName\":\"titan men\",\"websiteUrl\":\"www.amazon.com\\/titan\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"12\",\"options\":\"golden\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"51.00\"},{\"id\":51,\"itemName\":\"titan women\",\"websiteUrl\":\"www.amazon.com\\/titan\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"12\",\"options\":\"silver\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"10\",\"itemShippingCost\":\"20.00\",\"itemTotalCost\":\"520.00\"}],\"payment\":{\"paymentMethodId\":5,\"paymentMethodName\":\"GTBank : Pay at Bank : Act# : 0121339039\"}}',5,'unpaid','2018-09-26 15:05:02','0',NULL,NULL),(59,'INVNG15-42-20180926',NULL,42,'shopforme','receipt','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,1446.87,'{\"shipment\":{\"urgent\":\"Y\",\"totalItemCost\":\"1103.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"11.03\",\"totalProcurementCost\":\"1114.03\",\"totalTax\":10.55,\"isInsuranceCharged\":\"Y\",\"totalInsurance\":111.29,\"totalCost\":1446.87,\"totalWeight\":5.5,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":48,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"red\",\"itemPrice\":\"10.00\",\"itemQuantity\":\"5\",\"itemShippingCost\":\"3.00\",\"itemTotalCost\":\"53.00\"},{\"id\":49,\"itemName\":\"nike shoe\",\"websiteUrl\":\"www.amazon.com\\/nike\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"10\",\"options\":\"flurocent green\",\"itemPrice\":\"520.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"1050.00\"}],\"payment\":{\"paymentMethodId\":6,\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\"},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-10-12\",\"isDutyCharged\":\"Y\",\"shippingCost\":65.97,\"totalClearingDuty\":145.03,\"totalShippingCost\":211}}',6,'paid','2018-09-26 15:01:07','0',NULL,NULL),(60,'INVNG15-42-20180926',25,0,'shopforme','receipt','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,333.53,'{\"shipment\":{\"totalItemCost\":\"1103.00\",\"totalTax\":10.55,\"isInsuranceCharged\":\"Y\",\"totalInsurance\":111.29,\"totalCost\":333.53000000000003,\"totalWeight\":5.5,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":48,\"itemName\":\"ipad\",\"websiteUrl\":\"amazon.com\\/ipad\",\"storeId\":\"1\",\"siteCategoryId\":\"3\",\"siteSubCategoryId\":\"39\",\"siteProductId\":\"23\",\"options\":\"red\",\"itemPrice\":\"10.00\",\"itemQuantity\":\"5\",\"itemShippingCost\":\"3.00\",\"itemTotalCost\":\"53.00\"},{\"id\":49,\"itemName\":\"nike shoe\",\"websiteUrl\":\"www.amazon.com\\/nike\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"10\",\"options\":\"flurocent green\",\"itemPrice\":\"520.00\",\"itemQuantity\":\"2\",\"itemShippingCost\":\"10.00\",\"itemTotalCost\":\"1050.00\"}],\"payment\":{\"paymentMethodId\":6,\"paymentMethodName\":\"GTBank: $ : Act# : 0121340105\"},\"shippingcharges\":{\"shippingMethod\":\"Y\",\"shippingid\":184,\"shipping\":\"Standard Shipping\",\"estimateDeliveryDate\":\"2018-10-12\",\"isDutyCharged\":\"Y\",\"shippingCost\":65.97,\"totalClearingDuty\":145.03,\"totalShippingCost\":211}}',6,'paid','2018-09-26 15:01:07','0',NULL,NULL),(61,'INVNG15-43-20180926',NULL,43,'shopforme','receipt','N','NG15','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456','Tathagata sur','tathagata.sur@indusnet.co.uk','10 A ROY PARA BYE LANE','KOLKATA j','53','4','163','700091','+91+90+92+9208961186006',NULL,576.71,'{\"shipment\":{\"urgent\":\"Y\",\"totalItemCost\":\"571.00\",\"totalProcessingFee\":\"0.00\",\"urgentPurchaseCost\":\"5.71\",\"totalProcurementCost\":\"576.71\",\"totalTax\":0,\"isInsuranceCharged\":\"N\",\"totalInsurance\":\"\",\"totalCost\":576.71,\"totalWeight\":2,\"totalQuantity\":2},\"warehouse\":{\"fromAddress\":\"12011 Westbrae Parkway #B\",\"fromZipCode\":\"77031\",\"fromCountry\":230,\"fromState\":120,\"fromCity\":256},\"shippingaddress\":{\"toCountry\":163,\"toState\":4,\"toCity\":53,\"toAddress\":\"10 A ROY PARA BYE LANE\",\"toAlternateAddress\":\"KOLKATA j\",\"toZipCode\":\"700091\",\"toName\":\"Tathagata sur\",\"toEmail\":\"tathagata.sur@indusnet.co.uk\",\"toPhone\":\"+91+90+92+9208961186006\"},\"packages\":[{\"id\":50,\"itemName\":\"titan men\",\"websiteUrl\":\"www.amazon.com\\/titan\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"12\",\"options\":\"golden\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"1\",\"itemShippingCost\":\"1.00\",\"itemTotalCost\":\"51.00\"},{\"id\":51,\"itemName\":\"titan women\",\"websiteUrl\":\"www.amazon.com\\/titan\",\"storeId\":\"1\",\"siteCategoryId\":\"4\",\"siteSubCategoryId\":\"41\",\"siteProductId\":\"12\",\"options\":\"silver\",\"itemPrice\":\"50.00\",\"itemQuantity\":\"10\",\"itemShippingCost\":\"20.00\",\"itemTotalCost\":\"520.00\"}],\"payment\":{\"paymentMethodId\":5,\"paymentMethodName\":\"GTBank : Pay at Bank : Act# : 0121339039\"}}',5,'paid','2018-09-26 15:05:02','0',NULL,NULL);
/*!40000 ALTER TABLE `stmd_invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_location_types`
--

DROP TABLE IF EXISTS `stmd_location_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_location_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_location_types`
--

LOCK TABLES `stmd_location_types` WRITE;
/*!40000 ALTER TABLE `stmd_location_types` DISABLE KEYS */;
INSERT INTO `stmd_location_types` VALUES (1,'Residence','1','0'),(2,'Office','1','0'),(3,'Barracks','1','0'),(4,'Port','1','0'),(5,'Church','1','0'),(6,'Mosque','1','0'),(7,'Trade show/ Convention centre','1','0'),(8,'Airport','1','0'),(9,'Open land field','1','0'),(10,'Farm/ Forest Area','1','0'),(11,'Agent location','1','0'),(12,'Mall/shop','1','0'),(13,'Others','1','0'),(14,'Fast food outlet','1','0');
/*!40000 ALTER TABLE `stmd_location_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_locations`
--

DROP TABLE IF EXISTS `stmd_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `countryId` int(11) NOT NULL,
  `stateId` int(11) NOT NULL,
  `cityId` int(11) NOT NULL,
  `address` varchar(255) DEFAULT '',
  `businessName` varchar(128) NOT NULL DEFAULT '',
  `phone` varchar(32) DEFAULT '',
  `email` varchar(128) DEFAULT '',
  `fax` varchar(32) DEFAULT '',
  `comment` text,
  `displayOrder` int(11) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `deleted` enum('1','0') DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `countryId` (`countryId`),
  KEY `stateId` (`stateId`),
  KEY `cityId` (`cityId`),
  CONSTRAINT `stmd_locations_ibfk_1` FOREIGN KEY (`countryId`) REFERENCES `stmd_countries` (`id`),
  CONSTRAINT `stmd_locations_ibfk_2` FOREIGN KEY (`stateId`) REFERENCES `stmd_states` (`id`),
  CONSTRAINT `stmd_locations_ibfk_3` FOREIGN KEY (`cityId`) REFERENCES `stmd_cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_locations`
--

LOCK TABLES `stmd_locations` WRITE;
/*!40000 ALTER TABLE `stmd_locations` DISABLE KEYS */;
INSERT INTO `stmd_locations` VALUES (65,163,5,2,'Address 1','New','0324234023','admin@stmd.com','0249234234','Test',1,'1','0',1,'2018-05-11 16:46:42',1,'2018-05-23 13:23:52',NULL,NULL),(66,163,3,21,'Test Address','Test',NULL,NULL,NULL,NULL,2,'1','0',1,'2018-05-11 16:51:41',1,'2018-05-11 17:26:23',NULL,NULL),(67,3,176,551,'123 road','Test','55656546',NULL,NULL,NULL,3,'1','0',1,'2018-05-16 06:58:31',1,'2018-05-23 13:23:59',NULL,NULL);
/*!40000 ALTER TABLE `stmd_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_medialibrary`
--

DROP TABLE IF EXISTS `stmd_medialibrary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_medialibrary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileType` enum('I','V') NOT NULL COMMENT 'I = image, V = video',
  `fileName` text,
  `fileEmbedCode` text,
  `fileUrl` text,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `deleted` enum('0','1') NOT NULL COMMENT '0=not deleted, 1 = deleted',
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_medialibrary`
--

LOCK TABLES `stmd_medialibrary` WRITE;
/*!40000 ALTER TABLE `stmd_medialibrary` DISABLE KEYS */;
INSERT INTO `stmd_medialibrary` VALUES (1,'I','1531889706_images.jpeg',NULL,NULL,1,'2018-07-18 04:55:05','0',NULL,NULL,NULL,NULL),(2,'I','1531889718_images-8.jpg',NULL,NULL,1,'2018-07-18 04:55:18','0',NULL,NULL,NULL,NULL),(3,'I','1531889735_Rotating_earth_(large).gif',NULL,NULL,1,'2018-07-18 04:55:35','0',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_medialibrary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_menu`
--

DROP TABLE IF EXISTS `stmd_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` enum('1','0') NOT NULL COMMENT '0:header,1:footer',
  `pannel` enum('0','1','2','3','4') NOT NULL DEFAULT '0',
  `parentId` int(11) NOT NULL DEFAULT '0',
  `label` varchar(255) NOT NULL,
  `linkType` enum('1','2') NOT NULL COMMENT '1:external,2:internal',
  `link` varchar(255) NOT NULL,
  `cmsPageLink` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `menuImage` varchar(255) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `modifiedOn` datetime NOT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_menu`
--

LOCK TABLES `stmd_menu` WRITE;
/*!40000 ALTER TABLE `stmd_menu` DISABLE KEYS */;
INSERT INTO `stmd_menu` VALUES (1,'0','0',0,'Menu 1','1','http://google.com/','',1,NULL,'1','1',1,'2018-06-06 05:35:54',1,'2018-06-06 05:36:04');
/*!40000 ALTER TABLE `stmd_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_migrations`
--

DROP TABLE IF EXISTS `stmd_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_migrations`
--

LOCK TABLES `stmd_migrations` WRITE;
/*!40000 ALTER TABLE `stmd_migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_newsletter`
--

DROP TABLE IF EXISTS `stmd_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emailTemplateId` int(11) NOT NULL,
  `newsletterName` varchar(255) DEFAULT NULL,
  `newsletterDescription` text NOT NULL,
  `subscriberId` text NOT NULL,
  `scheduleDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scheduleRepeat` enum('0','1') NOT NULL,
  `allowedHtmlTags` enum('Y','N') NOT NULL DEFAULT 'Y',
  `scheduleStatus` enum('Q','S','R') NOT NULL DEFAULT 'Q' COMMENT 'Q=Queued, S=Sent, R=Repeat',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(10) NOT NULL DEFAULT '0',
  `updatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(10) NOT NULL DEFAULT '0',
  `deletedBy` int(10) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL COMMENT '0:not deleted,1:deleted',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1:active,0:inactive',
  `expired` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_newsletter`
--

LOCK TABLES `stmd_newsletter` WRITE;
/*!40000 ALTER TABLE `stmd_newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_newslettersubscribermapping`
--

DROP TABLE IF EXISTS `stmd_newslettersubscribermapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_newslettersubscribermapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletterId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `sendStatus` enum('0','1') NOT NULL COMMENT '0=Not Sent, 1=Sent',
  `sendDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_newslettersubscribermapping`
--

LOCK TABLES `stmd_newslettersubscribermapping` WRITE;
/*!40000 ALTER TABLE `stmd_newslettersubscribermapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_newslettersubscribermapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_newslettersubscription`
--

DROP TABLE IF EXISTS `stmd_newslettersubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_newslettersubscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=Active, 1=Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_newslettersubscription`
--

LOCK TABLES `stmd_newslettersubscription` WRITE;
/*!40000 ALTER TABLE `stmd_newslettersubscription` DISABLE KEYS */;
INSERT INTO `stmd_newslettersubscription` VALUES (1,3,'somnath.pahari@indusnet.co.in','0'),(2,1,'paulami.sarkar@indusnet.co.in','0');
/*!40000 ALTER TABLE `stmd_newslettersubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_oauth_access_tokens`
--

DROP TABLE IF EXISTS `stmd_oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_oauth_access_tokens`
--

LOCK TABLES `stmd_oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `stmd_oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `stmd_oauth_access_tokens` VALUES ('1369d748f2e38a580e18f37e3a2094ef34996ad853d3c65674b12e8a9bbbc636f355352413e66450',3,1,'login','[]',0,'2018-09-26 02:17:14','2018-09-26 02:17:14','2019-09-26 07:47:14'),('2016397bf07357c39445f0e1d75c7d6efc948e03b198d55379f6160584f220bb9bc0e296debd8f16',3,1,'login','[]',0,'2018-09-26 02:49:20','2018-09-26 02:49:20','2019-09-26 08:19:20'),('22e2cb3644cda7e8d2379d690660aa483c305c2b6d2b3cd3ff18c06e00564e864d332df12eb9858d',3,1,'login','[]',0,'2018-09-26 02:11:37','2018-09-26 02:11:37','2019-09-26 07:41:37'),('2d226d517bc0606626c86cabaf431acb6843b6ffdaa7dcb5b5c27ff2fd5fe287b63cfb9e03a0101d',3,1,'login','[]',0,'2018-09-25 00:43:23','2018-09-25 00:43:23','2019-09-25 06:13:23'),('31bb6304d5b0d8a3aafc7d334cef187a26b6c738153475ccd915b883c2889f98603b221dd18bbd8a',3,1,'login','[]',0,'2018-09-26 02:23:06','2018-09-26 02:23:06','2019-09-26 07:53:06'),('3643a3a260646133cec12caee079842e6c0bb725a7c2c1d296f0656e6817d5f9da269dc4dd384e59',3,1,'login','[]',0,'2018-09-26 02:06:03','2018-09-26 02:06:03','2019-09-26 07:36:03'),('36c11ae55123f58a61f80f19b730b02fdaac47aee6645b4e0723e17ed67e00f77a76e61cf1ae5e5a',7,1,'login','[]',0,'2018-09-11 15:55:24','2018-09-11 15:55:24','2019-09-11 15:55:24'),('3f06813084dbd195b96fbcbd48c01cb2ff0a85e5a7b3a8d9b14cf44e8d3f0d75ed2611979247712d',3,1,'login','[]',0,'2018-09-26 02:47:46','2018-09-26 02:47:46','2019-09-26 08:17:46'),('45ada584b63cce1211ce4e853a9399c55d781e51b4c77e29c12bf4f5e728c3256409c2b1b1afa03a',3,1,'login','[]',0,'2018-09-26 02:34:32','2018-09-26 02:34:32','2019-09-26 08:04:32'),('53e63b6d26fd60aabc33c97518b790e8e4357bfd02153ade55154e124e97f4df62c97007bc345bb3',7,1,'login','[]',0,'2018-09-18 12:46:50','2018-09-18 12:46:50','2019-09-18 12:46:50'),('62de794fb473c9079389fa2e462a3be6ac6bd32bd95e66febcdbf9b91e37fce7346bad7f5599a7d8',3,1,'login','[]',0,'2018-09-26 03:53:26','2018-09-26 03:53:26','2019-09-26 09:23:26'),('647d2865316f38c0ad537666f49fcf983dcf81c2f40bd95d8c7b3529790fec5994c808dcda8c36aa',7,1,'login','[]',0,'2018-09-11 09:37:36','2018-09-11 09:37:36','2019-09-11 09:37:36'),('6a06e0a1bcb81b4cba47f50ef16c225a43219e236bc9bcf5b80e86c26703744f9e59ec8759191d85',7,1,'login','[]',0,'2018-09-11 09:28:19','2018-09-11 09:28:19','2019-09-11 09:28:19'),('842d819e7a29df150c36e49df91a167be99ac0b0bd99cfee6c35104295a865610b79edf92ed9f285',3,1,'login','[]',0,'2018-09-26 01:55:56','2018-09-26 01:55:56','2019-09-26 07:25:56'),('860ac3c5f78aa2f0881d9bb1983f6d558c25591bfad47cdb29e1c5a589851c0690096e8a0ab847b8',1,1,'login','[]',0,'2018-07-13 09:33:43','2018-07-13 09:33:43','2019-07-13 15:03:43'),('9aa095faf20fb0aa2fedd9cec2f3e63a4a17b6e0880bf58c0274e471e04187bdfec49abe674f71f0',1,1,'login','[]',0,'2018-07-13 09:17:05','2018-07-13 09:17:05','2019-07-13 14:47:05'),('bbdd7ac87f0974ce96d11557d774ea93a4830cf3469e485367baf4b12c80f86f2af7b896c0dd4d70',1,1,'login','[]',0,'2018-07-13 09:10:55','2018-07-13 09:10:55','2019-07-13 14:40:55'),('cd3754ff43fb4a69575704fdbcc729dd34b40c4f967f24bbce438cb44981fb5433399c3e95219250',18,1,'login','[]',0,'2018-09-18 12:03:55','2018-09-18 12:03:55','2019-09-18 12:03:55'),('d657ec74690cedccc4a590c37fb9ff3b2f34f046d5c17e0c10ff48bdf1b26ab9ea4b8c1f7dbc8259',3,1,'login','[]',0,'2018-09-26 02:04:32','2018-09-26 02:04:32','2019-09-26 07:34:32'),('d7a9b50732c24ffbac52f55b0fc4d7dc4bd3ba3817df1d3de7a6a05c8a54a9b51c09e7b9d2503450',3,1,'login','[]',0,'2018-09-26 02:54:17','2018-09-26 02:54:17','2019-09-26 08:24:17'),('dee8f8b45bfe8e6616910bad63ba449bf978e1d4b8eae450e7f4df09a198d8304a162c9e52f0e8f2',1,1,'login','[]',0,'2018-07-13 09:11:18','2018-07-13 09:11:18','2019-07-13 14:41:18'),('efae4e0939d5fe928f62fffe7a272e099a5db3ed55194e3a4d1bc6ef95c54098275033e5a58e8321',7,1,'login','[]',0,'2018-09-18 13:51:15','2018-09-18 13:51:15','2019-09-18 13:51:15'),('f7181d32fda71a2b3cf60d94edc66d2d1101db773ce72aafe74cc8c045f29c55ee1129256e4770da',3,1,'login','[]',0,'2018-09-26 01:57:37','2018-09-26 01:57:37','2019-09-26 07:27:37');
/*!40000 ALTER TABLE `stmd_oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_oauth_auth_codes`
--

DROP TABLE IF EXISTS `stmd_oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_oauth_auth_codes`
--

LOCK TABLES `stmd_oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `stmd_oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_oauth_clients`
--

DROP TABLE IF EXISTS `stmd_oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_oauth_clients`
--

LOCK TABLES `stmd_oauth_clients` WRITE;
/*!40000 ALTER TABLE `stmd_oauth_clients` DISABLE KEYS */;
INSERT INTO `stmd_oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','0912Dm3cJ69VxB6y2dht2fC5N1n5UMQ2Wa0pPVzB','http://localhost',1,0,0,'2018-07-13 06:37:49','2018-07-13 06:37:49'),(2,NULL,'Laravel Password Grant Client','jaFUESgEgyYuFera8c4R41fMwiNh04e5czUWV6Hx','http://localhost',0,1,0,'2018-07-13 06:37:49','2018-07-13 06:37:49');
/*!40000 ALTER TABLE `stmd_oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `stmd_oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_oauth_personal_access_clients`
--

LOCK TABLES `stmd_oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `stmd_oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `stmd_oauth_personal_access_clients` VALUES (1,1,'2018-07-13 06:37:49','2018-07-13 06:37:49');
/*!40000 ALTER TABLE `stmd_oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `stmd_oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_oauth_refresh_tokens`
--

LOCK TABLES `stmd_oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `stmd_oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_offerconditions`
--

DROP TABLE IF EXISTS `stmd_offerconditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_offerconditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offerId` int(11) NOT NULL,
  `keyword` text NOT NULL,
  `valuesSerialized` text NOT NULL,
  `jsonValues` text,
  `setNo` int(11) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `offercondition_ibfk_1` (`offerId`),
  CONSTRAINT `offercondition_ibfk_1` FOREIGN KEY (`offerId`) REFERENCES `stmd_offers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_offerconditions`
--

LOCK TABLES `stmd_offerconditions` WRITE;
/*!40000 ALTER TABLE `stmd_offerconditions` DISABLE KEYS */;
INSERT INTO `stmd_offerconditions` VALUES (1,1,'customer_of_a_specific_shipping_location','a:1:{i:0;a:3:{s:15:\"countrySelected\";s:3:\"163\";s:13:\"stateSelected\";s:1:\"2\";s:12:\"citySelected\";s:3:\"639\";}}','[{\"countrySelected\":\"163\",\"stateSelected\":\"2\",\"citySelected\":\"639\"}]',1,'0',1,'2018-09-11 06:26:47',NULL,NULL,NULL,NULL),(2,2,'customer_get_discounts_and_special_offers_on_certain_product_or_categories','a:1:{i:0;a:1:{i:0;a:3:{s:7:\"setname\";s:8:\"Products\";s:6:\"values\";a:1:{i:0;s:2:\"16\";}s:5:\"setno\";i:1;}}}','[[{\"setname\":\"Products\",\"values\":[\"16\"],\"setno\":1}]]',1,'0',1,'2018-09-11 15:45:23',NULL,NULL,NULL,NULL),(4,3,'discount_on_total_amount_of_shipping','a:2:{s:7:\"Minimum\";s:6:\"500.00\";s:7:\"Maximum\";s:7:\"1500.00\";}','{\"shipping_cost\":{\"from\":\"500.00\",\"to\":\"1500.00\"}}',1,'0',1,'2018-09-26 14:58:10',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_offerconditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_offers`
--

DROP TABLE IF EXISTS `stmd_offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortName` varchar(255) DEFAULT NULL,
  `couponCode` varchar(100) DEFAULT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '2' COMMENT '0 - inactive, 1 - active, 2 - pending',
  `offerRangeDateFrom` varchar(15) DEFAULT NULL,
  `offerRangeDateTo` varchar(15) DEFAULT NULL,
  `bonusKeyword` text,
  `bonusValuesSerialized` text,
  `promoImage` text,
  `promoText` text,
  `promoDetailed` text,
  `promoCar` text,
  `promoTotal` text,
  `isSetCondition` enum('0','1') NOT NULL DEFAULT '0',
  `isSetBonus` enum('0','1') NOT NULL DEFAULT '0',
  `isSetPromo` enum('0','1') NOT NULL DEFAULT '0',
  `isSetOffer` enum('0','1') NOT NULL DEFAULT '0',
  `noOfSet` int(11) NOT NULL DEFAULT '1',
  `isSent` enum('N','Y') DEFAULT 'N' COMMENT 'Is this coupon code sent, Y=Yes, No=No, If set as Yes, coupon code cannot be editable from UI',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_offers`
--

LOCK TABLES `stmd_offers` WRITE;
/*!40000 ALTER TABLE `stmd_offers` DISABLE KEYS */;
INSERT INTO `stmd_offers` VALUES (1,'Nigeria10','nigeria10','1','09/01/2018','09/30/2018','give_discount','a:4:{s:8:\"discount\";s:5:\"10.00\";s:4:\"type\";s:8:\"Absolute\";s:17:\"discountNotExceed\";s:4:\"0.00\";s:10:\"exceedType\";s:3:\"Yes\";}',NULL,'<p>Add Description</p>','<p>Add Description</p>','<p>Add Description</p>','<p>Add Description</p>','1','1','1','0',1,'N','0',1,'2018-09-11 06:26:47',1,'2018-09-11 06:27:50',NULL,NULL),(2,'Phone Promo','Phone Offer','1','10/02/2018','10/12/2018','give_discount','a:4:{s:8:\"discount\";s:5:\"10.00\";s:4:\"type\";s:7:\"Percent\";s:17:\"discountNotExceed\";s:5:\"25.00\";s:10:\"exceedType\";s:3:\"Yes\";}',NULL,'<p>Add Description</p>','<p>Add Description</p>','<p>Add Description</p>','<p>Add Description</p>','1','1','1','0',1,'N','0',1,'2018-09-11 15:45:23',1,'2018-09-11 15:48:16',NULL,NULL),(3,'Untitled 3','pur500','1','09/26/2018','10/20/2018','give_discount','a:4:{s:8:\"discount\";s:5:\"10.00\";s:4:\"type\";s:7:\"Percent\";s:17:\"discountNotExceed\";s:4:\"0.00\";s:10:\"exceedType\";s:3:\"Yes\";}',NULL,'<p>Add Description</p>','<p>Add Description</p>','<p>Add Description</p>','<p>Add Description</p>','1','1','1','0',1,'N','0',1,'2018-09-26 14:57:27',1,'2018-09-26 14:59:15',NULL,NULL);
/*!40000 ALTER TABLE `stmd_offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_orders`
--

DROP TABLE IF EXISTS `stmd_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderNumber` varchar(25) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `ShipmentId` int(11) NOT NULL,
  `totalCost` decimal(12,2) NOT NULL,
  `status` enum('0','1','2','3','4','5') NOT NULL DEFAULT '1' COMMENT '0 => ''Not finished'',1=>.''Submitted'',2=>''Processed'',3=>''Declined'',4=>''Failed'',5=>''Complete''',
  `type` enum('shipment','autoshipment') NOT NULL DEFAULT 'shipment',
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `stmd_orders_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_orders`
--

LOCK TABLES `stmd_orders` WRITE;
/*!40000 ALTER TABLE `stmd_orders` DISABLE KEYS */;
INSERT INTO `stmd_orders` VALUES (2,'OD-21340404',13,9,146.94,'5','shipment','2018-09-14 13:34:52',1),(3,'OD-31339527',13,11,0.00,'5','shipment','2018-09-14 13:44:48',1),(4,'OD-4323182',3,1,40.20,'5','shipment','2018-09-14 13:46:02',1),(5,'OD-5367544',3,21,0.00,'5','shipment','2018-09-20 18:51:44',1),(6,'OD-6380948',3,22,1277.58,'5','shipment','2018-09-21 18:26:35',1);
/*!40000 ALTER TABLE `stmd_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_orders_bak`
--

DROP TABLE IF EXISTS `stmd_orders_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_orders_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `giftcertDiscount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `giftcertId` text CHARACTER SET latin1 NOT NULL,
  `subtotal` decimal(12,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `coupon` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `couponDiscount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `shippingId` int(11) NOT NULL DEFAULT '0',
  `tracking` varchar(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `shippingCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `packagingInventory` decimal(12,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `taxesApplied` text CHARACTER SET latin1 NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('0','1','2','3','4','5') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT '0 => ''Not finished'',1=>.''Submitted'',2=>''Processed'',3=>''Declined'',4=>''Failed'',5=>''Complete''',
  `flag` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'N',
  `notes` text CHARACTER SET latin1 NOT NULL,
  `details` text CHARACTER SET latin1 NOT NULL,
  `customerNotes` text CHARACTER SET latin1 NOT NULL,
  `customer` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingTitle` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingFirstname` varchar(128) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingLastname` varchar(128) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingAddress` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingCity` varchar(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingCounty` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingState` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingCountry` char(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingZipcode` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingZip4` varchar(4) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingPhone` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `billingFax` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `url` varchar(128) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `email` varchar(128) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `clickid` int(11) NOT NULL DEFAULT '0',
  `extra` mediumtext CHARACTER SET latin1 NOT NULL,
  `paymentId` int(11) NOT NULL DEFAULT '0',
  `paymentSurcharge` decimal(12,2) NOT NULL DEFAULT '0.00',
  `taxNumber` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `taxExempt` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'N',
  `initTotal` decimal(12,2) NOT NULL DEFAULT '0.00',
  `accessKey` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `statuses` text CHARACTER SET latin1 NOT NULL,
  `storageNotifications` text CHARACTER SET latin1 NOT NULL,
  `shipmentId` int(11) NOT NULL DEFAULT '0',
  `shipping_date` int(11) NOT NULL DEFAULT '0',
  `gtsFeed` varchar(1) CHARACTER SET latin1 NOT NULL DEFAULT 'N',
  `gtsReason` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `totalInsurance` decimal(12,2) NOT NULL DEFAULT '0.00',
  `totalExtra` decimal(12,2) NOT NULL DEFAULT '0.00',
  `cardDiscount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `cardid` int(11) NOT NULL,
  `codeDiscount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `otherCharges` decimal(12,2) NOT NULL DEFAULT '0.00',
  `deliveryCompany` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT 'others',
  `serviceType` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `duty` decimal(12,2) NOT NULL DEFAULT '0.00',
  `clearing` decimal(12,2) NOT NULL DEFAULT '0.00',
  `totalWarranty` decimal(12,2) NOT NULL DEFAULT '0.00',
  `personalItemsCharge` decimal(12,2) NOT NULL DEFAULT '0.00',
  `warranty` text CHARACTER SET latin1 NOT NULL,
  `tsDays` int(11) NOT NULL DEFAULT '0',
  `autoShipmentid` int(11) NOT NULL DEFAULT '0',
  `autoShipment` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_date` (`date`),
  KEY `b_state` (`billingState`),
  KEY `b_country` (`billingCountry`),
  KEY `clickid` (`clickid`),
  KEY `userid` (`userId`),
  KEY `paymentid` (`paymentId`),
  KEY `shippingid` (`shippingId`),
  KEY `tracking` (`tracking`),
  KEY `shipmentid` (`shipmentId`),
  CONSTRAINT `shipment fk` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`),
  CONSTRAINT `shipping fk` FOREIGN KEY (`shippingId`) REFERENCES `stmd_shippingmethods` (`shippingid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userid fk` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_orders_bak`
--

LOCK TABLES `stmd_orders_bak` WRITE;
/*!40000 ALTER TABLE `stmd_orders_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_orders_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_other_charges`
--

DROP TABLE IF EXISTS `stmd_other_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_other_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `orderby` int(11) NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_other_charges`
--

LOCK TABLES `stmd_other_charges` WRITE;
/*!40000 ALTER TABLE `stmd_other_charges` DISABLE KEYS */;
INSERT INTO `stmd_other_charges` VALUES (14,'Export Filing - Greater than $2500/item',25.00,38,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(15,'Unidentified/Wrong Package Addressing',6.99,28,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(16,'Copy of Customer Invoice',2.99,36,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(18,'Unit Sharing',5.99,29,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(24,'Balance For Shipping Sedan',1280.00,40,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(32,'Hazmat Shipping (0 to 5 lbs)',10.00,30,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(33,'Hazmat Shipping (5 to 10 lbs)',15.00,31,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(34,'Hazmat Shipping (10 to 20 lbs)',20.00,32,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(35,'Hazmat Shipping (20 to 50 lbs)',40.00,33,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(36,'Hazmat Shipping (50 to 100 lbs)',75.00,34,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(37,'Hazmat Shipping (100 and Abover lbs)',100.00,39,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(38,'Motor Cycle Crating',599.00,27,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(40,'TV Crating',29.99,1,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(42,'Engine Purge',29.00,37,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(44,'Fragile Packaging',4.99,2,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(46,'Charge Back Processing',35.00,35,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(54,'Delivery Count (20 to 50 items)',2.99,3,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(55,'Delivery Count (50 to 100 items)',4.99,4,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(56,'Delivery Count (More than 100 items)',9.99,5,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(58,'Liquid Packaging',4.99,2,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(59,'Laptop Box - Small (14x14x6)',4.99,14,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(60,'Laptop Box - Med/Large (18x18x6)',5.99,15,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(61,'Cell Phone(s) Shipping Box (12x6x6)',1.99,12,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(62,'Tablet Shipping Box (12x12x6)',3.99,13,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(63,'Jewelry/Watch Box (6x6x6)',1.99,11,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(64,'Secured Packing Box (12x12x12)',3.99,16,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(65,'Secured Packing Box (18x12x12)',4.99,17,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(66,'Secured Packing Box (24x18x12)',5.99,19,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(67,'Secured Packing Box (24x24x12)',5.99,20,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(68,'Secured Packing Box (24x24x18)',6.99,21,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(69,'Secured Packing Box (24x24x24)',7.99,22,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(70,'Secured Packing Box (36x24x24)',12.99,23,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(71,'Secured Packing Box (30x30x30)',14.99,24,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(72,'Secured Packing Box (36x36x36)',19.99,25,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(73,'Secured Packing Box (48x40x36)',24.99,26,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(74,'Secured Packing Box (24x12x12)',4.99,18,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(75,'Secured Packing Box (18x18x12)',4.99,18,'1','2018-06-05 13:54:47',0,'2018-06-05 13:54:47',0,'0','2018-06-05 13:54:47',0),(76,'Test',10.00,0,'1','2018-06-05 15:21:49',1,'2018-06-05 15:22:09',1,'1','2018-06-05 15:22:16',1),(77,'TV Crating',100.00,1,'1','2018-06-22 13:55:23',1,'2018-06-22 13:55:23',0,'1','2018-06-22 13:55:30',1);
/*!40000 ALTER TABLE `stmd_other_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_packaging_charges`
--

DROP TABLE IF EXISTS `stmd_packaging_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_packaging_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weightFrom` double(12,2) NOT NULL,
  `weightTo` double(12,2) NOT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_packaging_charges`
--

LOCK TABLES `stmd_packaging_charges` WRITE;
/*!40000 ALTER TABLE `stmd_packaging_charges` DISABLE KEYS */;
INSERT INTO `stmd_packaging_charges` VALUES (4,1.00,20.00,10.00,'1','2018-06-05 15:32:22',0,'2018-06-06 07:56:35',1,'0','2018-06-05 15:32:22',0),(5,1.00,20.00,3.99,'1','2018-06-05 15:32:22',0,'2018-06-05 15:32:22',0,'1','2018-06-14 07:46:24',1),(6,21.00,50.00,5.99,'1','2018-06-05 15:32:22',0,'2018-06-05 15:32:22',0,'0','2018-06-05 15:32:22',0),(7,51.00,100.00,12.99,'1','2018-06-05 15:32:22',0,'2018-06-05 15:32:22',0,'0','2018-06-05 15:32:22',0),(8,61.00,100.00,79.00,'1','2018-06-05 15:32:22',0,'2018-06-06 07:57:00',1,'1','2018-06-14 07:46:06',1),(10,10.00,50.00,12.00,'1','2018-06-06 07:59:49',1,'2018-06-06 07:59:49',0,'0','2018-06-06 07:59:49',0);
/*!40000 ALTER TABLE `stmd_packaging_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_partners`
--

DROP TABLE IF EXISTS `stmd_partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 NOT NULL,
  `fileName` text,
  `type` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0- Banking, 1- Logistic',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1-active, 0-deactive',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `createdOn` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_partners`
--

LOCK TABLES `stmd_partners` WRITE;
/*!40000 ALTER TABLE `stmd_partners` DISABLE KEYS */;
INSERT INTO `stmd_partners` VALUES (1,'1st','1536643008_downloadsnapdeal.png','0','1','0','2018-09-11 05:16:48',1),(2,'2nd','1536643027_download (1).png','1','1','0','2018-09-11 05:17:07',1);
/*!40000 ALTER TABLE `stmd_partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_passwordresetlog`
--

DROP TABLE IF EXISTS `stmd_passwordresetlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_passwordresetlog` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_passwordresetlog`
--

LOCK TABLES `stmd_passwordresetlog` WRITE;
/*!40000 ALTER TABLE `stmd_passwordresetlog` DISABLE KEYS */;
INSERT INTO `stmd_passwordresetlog` VALUES ('paulami.sarkar@indusnet.co.in','$2y$10$aFBjj.gnJ0lqMmLolHOFAuGyE/ryvw4ip5GU/NRCQIL.SNsAvinH2','2018-05-23 14:13:09'),('admin@stmd.com','$2y$10$avnd0hjQ/rTATanJBSDzKu0.DpA6MDUeswYg.uP2Aw5Gin52T4m/i','2018-05-24 05:17:06');
/*!40000 ALTER TABLE `stmd_passwordresetlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_payment_methods`
--

DROP TABLE IF EXISTS `stmd_payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paymentMethodKey` varchar(255) NOT NULL,
  `paymentMethod` varchar(255) NOT NULL,
  `paymentDetails` varchar(255) NOT NULL DEFAULT '',
  `orderby` int(11) NOT NULL DEFAULT '0',
  `paymentGatewayRequired` enum('Y','N') NOT NULL DEFAULT 'N',
  `paymentGatewayId` int(11) DEFAULT NULL,
  `surcharge` decimal(12,2) NOT NULL DEFAULT '0.00',
  `surchargeType` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=$, 1=%',
  `countries` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderby` (`orderby`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_payment_methods`
--

LOCK TABLES `stmd_payment_methods` WRITE;
/*!40000 ALTER TABLE `stmd_payment_methods` DISABLE KEYS */;
INSERT INTO `stmd_payment_methods` VALUES (1,'credit_debit_card','Credit / Debit Cards','',1,'Y',2,20.00,'0','163,230','1','2018-06-13 07:59:05',0,'2018-09-13 08:18:06',1,'0','2018-06-13 14:46:07',1),(2,'check_cash_payment','Check/Cash In Office (US Only)','Check - US Office Only.',2,'N',NULL,0.00,'0','163,230','1','2018-08-16 10:54:12',0,'2018-08-16 10:54:12',0,'0','2018-08-16 10:54:12',0),(3,'paypal_checkout','PayPal',' 	Valid Paypal account holders only. A 2.99% processing fee applies. ',3,'N',NULL,2.99,'0','163,230','1','2018-08-16 10:57:21',0,'2018-08-16 10:57:21',0,'1','2018-08-16 10:57:21',0),(4,'bank_online_transfer','GTBank: # : Online Trf  : Act# : 0121339039','Transfer to American AirSea Cargo Nig. Ltd: Sort Code: 058152609: Please note your Order Number in Comments field for us to identify your payment. A 5% VAT applies.',4,'N',NULL,5.00,'1','163,230','1','2018-08-16 10:57:53',0,'2018-08-16 10:57:53',0,'0','2018-08-16 10:57:53',0),(5,'bank_pay_at_bank','GTBank : Pay at Bank : Act# : 0121339039','Print Your Invoice, & Pay In Any GTBank: American AirSea Cargo Nig. Ltd (=N=): Act#: 0121339039: A 5% VAT applies.',5,'N',NULL,5.00,'1','163,230','1','2018-08-16 10:58:31',0,'2018-08-16 10:58:31',0,'0','2018-08-16 10:58:31',0),(6,'bank_account_pay','GTBank: $ : Act# : 0121340105','Print Your Invoice, & Pay in Dollars In Any GTBank: American AirSea Cargo Nig Ltd. A 5% VAT applies.',6,'N',NULL,5.00,'1','163,230','1','2018-08-16 10:59:22',0,'2018-08-16 10:59:22',0,'0','2018-08-16 10:59:22',0),(7,'ewallet','E-Wallet','Store your e-wallet code and use it over and over to pay.',7,'N',NULL,0.00,'0','163,230','1','2018-08-16 10:59:56',0,'2018-08-16 10:59:56',0,'0','2018-08-16 10:59:56',0),(8,'wire_transfer','Wire Transfer','Send wires to: \r\nAmerican AirSea Cargo LLC; Bank of America; 1739 West Belfort Street, Stafford, Texas - 77477, USA; Bank Sort Code-BOFAUS3N; Account Number-5860 2260 3718',8,'N',NULL,15.00,'1','163,230','1','2018-08-16 11:00:44',0,'2018-08-16 11:00:44',0,'0','2018-08-16 11:00:44',0),(9,'paystack_checkout','Paystack','Testing',9,'N',NULL,2.00,'1','163,230','1','2018-09-03 07:57:23',0,'2018-09-03 07:57:23',0,'0','2018-09-03 07:57:23',0);
/*!40000 ALTER TABLE `stmd_payment_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_payment_transactions`
--

DROP TABLE IF EXISTS `stmd_payment_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_payment_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `paymentMethodId` int(11) NOT NULL,
  `paidFor` enum('shopforme','autopart','othervehicle','buyacar','shipacar') NOT NULL,
  `paidForId` int(11) NOT NULL COMMENT 'Refers to Respective TABLE id',
  `amountPaid` double(12,2) NOT NULL,
  `transactionOn` datetime NOT NULL,
  `transactionId` varchar(255) DEFAULT NULL,
  `status` enum('unpaid','paid') NOT NULL DEFAULT 'unpaid',
  `errorMsg` text,
  `transactionData` text,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `stmd_payment_transactions_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_payment_transactions`
--

LOCK TABLES `stmd_payment_transactions` WRITE;
/*!40000 ALTER TABLE `stmd_payment_transactions` DISABLE KEYS */;
INSERT INTO `stmd_payment_transactions` VALUES (1,3,4,'buyacar',3,6481.20,'2018-08-30 12:13:44',NULL,'unpaid',NULL,NULL),(2,3,6,'shipacar',1,40.20,'2018-08-30 12:20:02',NULL,'unpaid',NULL,NULL),(4,3,4,'buyacar',7,7447.35,'2018-08-30 12:35:08',NULL,'unpaid',NULL,NULL),(5,3,4,'buyacar',8,7769.40,'2018-08-30 12:38:30',NULL,'unpaid',NULL,NULL),(6,3,5,'shipacar',2,40.20,'2018-08-30 12:47:50',NULL,'unpaid',NULL,NULL),(7,3,8,'autopart',9,2015.09,'2018-08-30 13:04:35',NULL,'paid',NULL,'{\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null}'),(10,3,1,'shopforme',12,59.99,'2018-09-11 06:50:10','3BG461580S961220X','paid',NULL,'{\"TIMESTAMP\":\"2018-09-11T06:50:15Z\",\"CORRELATIONID\":\"ae6ef4b3758e1\",\"ACK\":\"Success\",\"VERSION\":\"85.0\",\"BUILD\":\"46457558\",\"AMT\":\"59.99\",\"CURRENCYCODE\":\"USD\",\"AVSCODE\":\"Y\",\"CVV2MATCH\":\"M\",\"TRANSACTIONID\":\"3BG461580S961220X\"}'),(11,3,1,'buyacar',13,7049.99,'2018-09-11 07:00:33','17970520FX9541106','paid',NULL,'{\"TIMESTAMP\":\"2018-09-11T07:00:38Z\",\"CORRELATIONID\":\"a2df332678701\",\"ACK\":\"Success\",\"VERSION\":\"85.0\",\"BUILD\":\"46457558\",\"AMT\":\"7049.99\",\"CURRENCYCODE\":\"USD\",\"AVSCODE\":\"Y\",\"CVV2MATCH\":\"M\",\"TRANSACTIONID\":\"17970520FX9541106\"}'),(12,3,1,'shopforme',17,129.98,'2018-09-11 07:09:18','73790803S7519214X','paid',NULL,'{\"TIMESTAMP\":\"2018-09-11T07:09:22Z\",\"CORRELATIONID\":\"249f2b15e441\",\"ACK\":\"Success\",\"VERSION\":\"85.0\",\"BUILD\":\"46457558\",\"AMT\":\"129.98\",\"CURRENCYCODE\":\"USD\",\"AVSCODE\":\"Y\",\"CVV2MATCH\":\"M\",\"TRANSACTIONID\":\"73790803S7519214X\"}'),(13,3,7,'shipacar',4,39.03,'2018-09-11 07:19:32',NULL,'paid',NULL,NULL),(14,3,7,'autopart',19,1648.56,'2018-09-11 07:37:50',NULL,'paid',NULL,'{\"ewalletId\":5}'),(15,3,8,'autopart',20,362.23,'2018-09-11 07:51:48',NULL,'paid',NULL,'{\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null}'),(16,7,1,'shopforme',21,248.00,'2018-09-11 09:14:47',NULL,'paid',NULL,NULL),(17,7,1,'shopforme',22,94.00,'2018-09-11 09:21:54',NULL,'paid',NULL,NULL),(18,3,2,'buyacar',23,71081.48,'2018-09-12 11:26:11',NULL,'unpaid',NULL,NULL),(19,3,2,'buyacar',24,53403.39,'2018-09-12 11:49:05',NULL,'unpaid',NULL,NULL),(20,3,6,'buyacar',25,85954.64,'2018-09-12 11:55:56',NULL,'unpaid',NULL,NULL),(21,3,2,'buyacar',26,26823.47,'2018-09-12 11:59:18',NULL,'unpaid',NULL,NULL),(22,3,2,'buyacar',27,26823.47,'2018-09-12 11:59:22',NULL,'unpaid',NULL,NULL),(23,3,2,'shipacar',8,73.47,'2018-09-12 12:47:10',NULL,'unpaid',NULL,NULL),(24,3,8,'autopart',28,516.20,'2018-09-12 13:25:45',NULL,'paid',NULL,'{\"poNumber\":null,\"companyName\":null,\"buyerName\":null,\"position\":null}'),(25,3,2,'buyacar',29,26823.47,'2018-09-12 13:36:59',NULL,'unpaid',NULL,NULL),(26,3,2,'shipacar',9,73.47,'2018-09-12 13:42:27',NULL,'paid',NULL,NULL),(27,13,1,'shopforme',30,218.00,'2018-09-13 07:55:16',NULL,'paid',NULL,NULL),(28,13,1,'autopart',33,179.00,'2018-09-13 08:17:10',NULL,'unpaid',NULL,NULL),(29,13,1,'autopart',34,480.00,'2018-09-13 08:23:21',NULL,'unpaid',NULL,NULL),(30,13,1,'shopforme',35,369.00,'2018-09-13 08:53:41',NULL,'paid',NULL,'{\"responseCode\":\"3\",\"authCode\":[],\"avsResultCode\":\"P\",\"cvvResultCode\":[],\"cavvResultCode\":[],\"transId\":\"0\",\"refTransID\":[],\"transHash\":\"4122FE30C5FE44C33DA34C8BC910DC7B\",\"testRequest\":\"0\",\"accountNumber\":\"XXXX1881\",\"accountType\":\"Visa\",\"errors\":{\"error\":{\"errorCode\":\"8\",\"errorText\":\"The credit card has expired.\"}},\"transHashSha2\":[]}'),(31,13,1,'autopart',36,75.00,'2018-09-13 09:11:04',NULL,'unpaid',NULL,NULL),(32,3,6,'shopforme',40,353.00,'2018-09-20 12:59:43',NULL,'paid',NULL,NULL),(33,3,5,'shopforme',41,638.79,'2018-09-21 17:53:07',NULL,'paid',NULL,NULL),(34,3,6,'shopforme',42,1446.87,'2018-09-26 15:01:07',NULL,'paid',NULL,NULL),(35,3,5,'shopforme',43,576.71,'2018-09-26 15:05:02',NULL,'paid',NULL,NULL);
/*!40000 ALTER TABLE `stmd_payment_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_paymentgateway_configurations`
--

DROP TABLE IF EXISTS `stmd_paymentgateway_configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_paymentgateway_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paymentGatewayId` int(11) NOT NULL,
  `configurationKeys` varchar(255) NOT NULL,
  `configurationValues` varchar(255) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `updatedOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_paymentgateway_configurations`
--

LOCK TABLES `stmd_paymentgateway_configurations` WRITE;
/*!40000 ALTER TABLE `stmd_paymentgateway_configurations` DISABLE KEYS */;
INSERT INTO `stmd_paymentgateway_configurations` VALUES (1,1,'Mode','Test',1,'2018-09-11 06:49:19'),(2,1,'API_access_username','niladrib.banerjee_api1.indusnet.co.in',1,'2018-09-11 06:49:19'),(3,1,'API_access_password','PDWP9VHLS6WZNSW9',1,'2018-09-11 06:49:19'),(4,1,'API_signature','AQk8BMTQ-qLVciqD5wec-8NzOGIZAAaf5HuzJofTE2Hev5hO-THMPeXN',1,'2018-09-11 06:49:19'),(5,1,'setUrl','https://api-3t.sandbox.paypal.com/nvp',1,'2018-09-11 06:49:19'),(6,2,'API_Login_ID','6J5Mk4cJs8',1,'2018-09-11 06:53:46'),(7,2,'Transaction_key','5f54XwuRm2k6Q5UL',1,'2018-09-11 06:53:46'),(8,2,'Mode','Test',1,'2018-09-11 06:53:46'),(9,2,'setUrl','https://apitest.authorize.net/xml/v1/request.api',1,'2018-09-11 06:53:46'),(10,9,'setUrl','https://api.paystack.co/transaction/verify/',1,'2018-09-06 12:54:01'),(11,9,'Mode','Test',1,'2018-09-06 12:54:01'),(12,9,'Publickey','pk_test_273b5ed8833ecf5fadebdef88d14eadb2d20ed79',1,'2018-09-06 12:54:01'),(13,9,'Secretkey','sk_test_9d2b7788b8810d03d8c7a7315b7107f609c4ab40',1,'2018-09-06 12:54:01');
/*!40000 ALTER TABLE `stmd_paymentgateway_configurations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_paymentgateway_configurations_Sept7`
--

DROP TABLE IF EXISTS `stmd_paymentgateway_configurations_Sept7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_paymentgateway_configurations_Sept7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paymentGatewayId` int(11) NOT NULL,
  `configurationKeys` varchar(255) NOT NULL,
  `configurationValues` varchar(255) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `updatedOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_paymentgateway_configurations_Sept7`
--

LOCK TABLES `stmd_paymentgateway_configurations_Sept7` WRITE;
/*!40000 ALTER TABLE `stmd_paymentgateway_configurations_Sept7` DISABLE KEYS */;
INSERT INTO `stmd_paymentgateway_configurations_Sept7` VALUES (1,1,'Test/Live mode','Test',1,'2018-07-09 05:59:05'),(2,1,'Currency','162',1,'2018-07-09 05:59:05'),(3,1,'Order prefix','ord_',1,'2018-07-09 05:59:05'),(4,1,'Type','credentials',1,'2018-07-09 05:59:05'),(5,1,'Contact Email','contact@shoptomydoor.com',1,'2018-07-09 05:59:05'),(6,1,'API access username','nduka_api1.americanairseacargo.com',1,'2018-07-09 05:59:05'),(7,1,'API access password','123456',1,'2018-07-09 05:59:05'),(8,1,'Use PayPal authentication method','Certificate',1,'2018-07-09 05:59:05'),(9,1,'API signature','AFcWxV21C7fd0v3bYYYRCpSSRl31AQjoG0ip8WmHbZ7.lONkZjDzvI-P',1,'2018-07-09 05:59:05'),(10,2,'Account type','cnp_ecommerce',1,'2018-06-14 00:00:00'),(11,2,'API Login ID','2x23Maq34DUn',1,'2018-06-14 00:00:00'),(12,2,'Transaction key','85apR3qGd2DW88km',1,'2018-06-14 00:00:00'),(13,2,'MD5 hash value','46787',1,'2018-06-14 00:00:00'),(14,2,'Test/Live mode','Live',1,'2018-06-14 00:00:00'),(15,2,'Order prefix','123',1,'2018-06-14 00:00:00');
/*!40000 ALTER TABLE `stmd_paymentgateway_configurations_Sept7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_paymentgateways`
--

DROP TABLE IF EXISTS `stmd_paymentgateways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_paymentgateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paymentGatewayName` varchar(100) NOT NULL,
  `paymentGatewayKey` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_paymentgateways`
--

LOCK TABLES `stmd_paymentgateways` WRITE;
/*!40000 ALTER TABLE `stmd_paymentgateways` DISABLE KEYS */;
INSERT INTO `stmd_paymentgateways` VALUES (1,'Paypal','PS'),(2,'Authorizenet','AIM');
/*!40000 ALTER TABLE `stmd_paymentgateways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_paymenttax_settings`
--

DROP TABLE IF EXISTS `stmd_paymenttax_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_paymenttax_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paymentMethodId` int(11) NOT NULL,
  `countryId` int(11) NOT NULL,
  `taxValue` decimal(12,2) NOT NULL,
  `taxtype` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=$, 1=%',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_paymenttax_settings`
--

LOCK TABLES `stmd_paymenttax_settings` WRITE;
/*!40000 ALTER TABLE `stmd_paymenttax_settings` DISABLE KEYS */;
INSERT INTO `stmd_paymenttax_settings` VALUES (1,1,163,10.00,'1','2018-06-21 10:43:43',1,'2018-06-21 10:43:43',0,'0'),(2,1,230,10.00,'1','2018-06-21 10:43:43',1,'2018-06-21 10:43:43',0,'0');
/*!40000 ALTER TABLE `stmd_paymenttax_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_procurement_item_status_log`
--

DROP TABLE IF EXISTS `stmd_procurement_item_status_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_procurement_item_status_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procurementId` int(11) NOT NULL,
  `procurementItemId` int(11) NOT NULL,
  `oldStatus` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `updatedOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_procurement_item_status_log`
--

LOCK TABLES `stmd_procurement_item_status_log` WRITE;
/*!40000 ALTER TABLE `stmd_procurement_item_status_log` DISABLE KEYS */;
INSERT INTO `stmd_procurement_item_status_log` VALUES (1,7,1,'none','submitted','2018-08-30 12:35:08'),(2,8,2,'none','submitted','2018-08-30 12:38:30'),(3,9,9,'','submitted','2018-08-30 13:04:35'),(4,10,4,'','submitted','2018-09-11 05:59:51'),(5,4,4,'submitted','orderplaced','2018-09-11 06:00:40'),(6,4,4,'received','received','2018-09-11 06:00:55'),(7,4,4,'received','received','2018-09-11 06:00:55'),(8,11,5,'','submitted','2018-09-11 06:43:38'),(9,12,6,'','submitted','2018-09-11 06:50:10'),(10,13,7,'none','submitted','2018-09-11 07:00:33'),(11,14,8,'','submitted','2018-09-11 07:03:14'),(12,13,7,'submitted','orderplaced','2018-09-11 07:05:53'),(13,13,7,'orderplaced','pickedup','2018-09-11 07:06:01'),(14,15,9,'','submitted','2018-09-11 07:06:09'),(15,13,7,'pickedup','received','2018-09-11 07:06:17'),(16,16,10,'','submitted','2018-09-11 07:08:21'),(17,17,11,'','submitted','2018-09-11 07:09:18'),(18,18,12,'','submitted','2018-09-11 07:30:26'),(19,19,13,'','submitted','2018-09-11 07:37:50'),(20,13,13,'submitted','orderplaced','2018-09-11 07:39:15'),(21,13,13,'received','received','2018-09-11 07:39:40'),(22,20,14,'','submitted','2018-09-11 07:51:48'),(23,21,15,'','submitted','2018-09-11 09:14:47'),(24,21,16,'','submitted','2018-09-11 09:14:47'),(25,22,17,'','submitted','2018-09-11 09:21:54'),(26,17,17,'submitted','orderplaced','2018-09-11 09:23:53'),(27,17,17,'received','received','2018-09-11 09:24:30'),(28,17,17,'received','received','2018-09-11 09:24:30'),(29,23,18,'none','submitted','2018-09-12 11:26:11'),(30,23,18,'submitted','orderplaced','2018-09-12 11:35:09'),(31,23,18,'orderplaced','pickedup','2018-09-12 11:35:33'),(32,23,18,'pickedup','received','2018-09-12 11:36:27'),(33,24,19,'none','submitted','2018-09-12 11:49:05'),(34,24,19,'submitted','orderplaced','2018-09-12 11:51:04'),(35,25,20,'none','submitted','2018-09-12 11:55:56'),(36,25,20,'submitted','orderplaced','2018-09-12 11:56:52'),(37,25,20,'orderplaced','pickedup','2018-09-12 11:57:04'),(38,25,20,'pickedup','received','2018-09-12 11:57:08'),(39,26,21,'none','submitted','2018-09-12 11:59:18'),(40,27,22,'none','submitted','2018-09-12 11:59:22'),(41,27,22,'submitted','orderplaced','2018-09-12 12:02:08'),(42,27,22,'orderplaced','pickedup','2018-09-12 12:02:16'),(43,27,22,'pickedup','received','2018-09-12 12:02:21'),(44,28,23,'','submitted','2018-09-12 13:25:45'),(45,23,23,'submitted','orderplaced','2018-09-12 13:31:31'),(46,23,23,'received','received','2018-09-12 13:31:53'),(47,29,24,'none','submitted','2018-09-12 13:36:59'),(48,15,15,'submitted','orderplaced','2018-09-13 06:11:25'),(49,16,16,'submitted','orderplaced','2018-09-13 06:11:25'),(50,15,15,'received','received','2018-09-13 06:12:42'),(51,15,15,'received','received','2018-09-13 06:12:42'),(52,16,16,'received','received','2018-09-13 06:12:42'),(53,16,16,'received','received','2018-09-13 06:12:42'),(54,30,25,'','submitted','2018-09-13 07:55:16'),(55,30,26,'','submitted','2018-09-13 07:55:16'),(56,25,25,'submitted','orderplaced','2018-09-13 07:57:39'),(57,26,26,'submitted','orderplaced','2018-09-13 07:57:39'),(58,25,25,'received','received','2018-09-13 07:58:08'),(59,25,25,'received','received','2018-09-13 07:58:08'),(60,26,26,'received','received','2018-09-13 07:58:08'),(61,26,26,'received','received','2018-09-13 07:58:08'),(62,31,27,'','submitted','2018-09-13 08:06:44'),(63,32,28,'','submitted','2018-09-13 08:14:39'),(64,33,29,'','submitted','2018-09-13 08:17:10'),(65,34,30,'','submitted','2018-09-13 08:23:21'),(66,30,30,'submitted','orderplaced','2018-09-13 08:24:51'),(67,30,30,'received','received','2018-09-13 08:25:07'),(68,35,31,'','submitted','2018-09-13 08:53:41'),(69,35,32,'','submitted','2018-09-13 08:53:41'),(70,31,31,'submitted','orderplaced','2018-09-13 08:56:17'),(71,32,32,'submitted','orderplaced','2018-09-13 08:56:17'),(72,31,31,'received','received','2018-09-13 08:58:18'),(73,31,31,'received','received','2018-09-13 08:58:18'),(74,32,32,'received','received','2018-09-13 08:58:18'),(75,32,32,'received','received','2018-09-13 08:58:18'),(76,36,33,'','submitted','2018-09-13 09:11:04'),(77,33,33,'submitted','orderplaced','2018-09-13 09:13:56'),(78,33,33,'received','received','2018-09-13 09:15:05'),(79,11,11,'submitted','orderplaced','2018-09-14 08:36:38'),(80,11,11,'received','received','2018-09-14 08:36:56'),(81,11,11,'received','received','2018-09-14 08:36:56'),(82,37,34,'','submitted','2018-09-19 06:44:18'),(83,38,35,'','submitted','2018-09-20 12:56:55'),(84,38,36,'','submitted','2018-09-20 12:56:55'),(85,38,37,'','submitted','2018-09-20 12:56:55'),(86,38,38,'','submitted','2018-09-20 12:56:55'),(87,39,39,'','submitted','2018-09-20 12:58:39'),(88,39,40,'','submitted','2018-09-20 12:58:39'),(89,39,41,'','submitted','2018-09-20 12:58:39'),(90,39,42,'','submitted','2018-09-20 12:58:39'),(91,40,43,'','submitted','2018-09-20 12:59:43'),(92,40,44,'','submitted','2018-09-20 12:59:43'),(93,40,45,'','submitted','2018-09-20 12:59:43'),(94,40,46,'','submitted','2018-09-20 12:59:43'),(95,43,43,'submitted','orderplaced','2018-09-20 13:02:06'),(96,44,44,'submitted','orderplaced','2018-09-20 13:02:06'),(97,43,43,'received','received','2018-09-20 13:02:50'),(98,43,43,'received','received','2018-09-20 13:02:50'),(99,44,44,'received','received','2018-09-20 13:02:50'),(100,44,44,'received','received','2018-09-20 13:02:50'),(101,3,3,'submitted','orderplaced','2018-09-21 15:48:34'),(102,3,3,'received','received','2018-09-21 15:49:36'),(103,41,47,'','submitted','2018-09-21 17:53:07'),(104,47,47,'submitted','orderplaced','2018-09-21 17:57:31'),(105,47,47,'received','received','2018-09-21 17:58:01'),(106,47,47,'received','received','2018-09-21 17:58:01'),(107,40,43,'submitted','orderplaced','2018-09-21 00:00:00'),(108,40,43,'orderplaced','received','2018-09-24 00:00:00'),(109,40,44,'submitted','orderplaced','2018-09-21 00:00:00'),(110,40,44,'orderplaced','received','2018-09-24 00:00:00'),(111,42,48,'','submitted','2018-09-26 15:01:07'),(112,42,49,'','submitted','2018-09-26 15:01:07'),(113,43,50,'','submitted','2018-09-26 15:05:02'),(114,43,51,'','submitted','2018-09-26 15:05:02'),(115,48,48,'submitted','orderplaced','2018-09-26 15:12:47'),(116,49,49,'submitted','orderplaced','2018-09-26 15:12:47'),(117,48,48,'received','received','2018-09-26 15:13:56'),(118,48,48,'received','received','2018-09-26 15:13:56'),(119,49,49,'received','received','2018-09-26 15:13:56'),(120,49,49,'received','received','2018-09-26 15:13:56'),(121,50,50,'submitted','orderplaced','2018-09-26 17:37:00'),(122,50,50,'received','received','2018-09-26 17:37:27'),(123,50,50,'received','received','2018-09-26 17:37:27'),(124,51,51,'submitted','orderplaced','2018-09-26 17:44:21'),(125,51,51,'received','received','2018-09-26 17:44:34'),(126,51,51,'received','received','2018-09-26 17:44:34');
/*!40000 ALTER TABLE `stmd_procurement_item_status_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_procurement_items`
--

DROP TABLE IF EXISTS `stmd_procurement_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_procurement_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procurementId` int(11) NOT NULL,
  `storeId` int(11) DEFAULT NULL,
  `siteCategoryId` int(11) DEFAULT NULL,
  `siteSubCategoryId` int(11) DEFAULT NULL,
  `siteProductId` int(11) DEFAULT NULL,
  `websiteUrl` text,
  `itemName` varchar(255) DEFAULT NULL,
  `itemDescription` text,
  `options` varchar(255) DEFAULT NULL,
  `productSKU` int(11) DEFAULT NULL,
  `carWebsiteId` int(11) DEFAULT NULL,
  `makeId` int(11) DEFAULT NULL,
  `modelId` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `milage` varchar(255) DEFAULT NULL,
  `vinNumber` varchar(255) DEFAULT NULL,
  `itemMake` varchar(100) NOT NULL,
  `itemModel` varchar(100) NOT NULL,
  `pickupStateId` int(11) DEFAULT NULL,
  `pickupCityId` int(11) DEFAULT NULL,
  `itemImage` varchar(255) DEFAULT NULL,
  `itemPrice` decimal(12,2) NOT NULL,
  `itemQuantity` int(11) NOT NULL,
  `itemShippingCost` decimal(12,2) DEFAULT NULL,
  `itemTotalCost` decimal(12,2) NOT NULL,
  `status` enum('submitted','orderplaced','pickedup','received','unavailable','addedtodelivery') NOT NULL DEFAULT 'submitted',
  `deliveryCompanyId` int(11) DEFAULT NULL,
  `receivedDate` date DEFAULT NULL,
  `trackingNumber` varchar(255) DEFAULT NULL,
  `trackingLock` enum('0','1') NOT NULL DEFAULT '0',
  `deliveryNotes` text,
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `deletedBy` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `procurementId` (`procurementId`),
  KEY `siteCategoryId` (`siteCategoryId`),
  KEY `siteSubCategoryId` (`siteSubCategoryId`),
  KEY `siteProductId` (`siteProductId`),
  KEY `makeId` (`makeId`),
  KEY `modelId` (`modelId`),
  KEY `pickupStateId` (`pickupStateId`),
  KEY `pickupCityId` (`pickupCityId`),
  KEY `carWebsiteId` (`carWebsiteId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `stmd_procurement_items_ibfk_1` FOREIGN KEY (`procurementId`) REFERENCES `stmd_procurement_shipments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_procurement_items_ibfk_5` FOREIGN KEY (`makeId`) REFERENCES `stmd_auto_make` (`id`),
  CONSTRAINT `stmd_procurement_items_ibfk_6` FOREIGN KEY (`modelId`) REFERENCES `stmd_auto_model` (`id`),
  CONSTRAINT `stmd_procurement_items_ibfk_7` FOREIGN KEY (`pickupStateId`) REFERENCES `stmd_states` (`id`),
  CONSTRAINT `stmd_procurement_items_ibfk_8` FOREIGN KEY (`pickupCityId`) REFERENCES `stmd_cities` (`id`),
  CONSTRAINT `stmd_procurement_items_ibfk_9` FOREIGN KEY (`carWebsiteId`) REFERENCES `stmd_auto_websites` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_procurement_items`
--

LOCK TABLES `stmd_procurement_items` WRITE;
/*!40000 ALTER TABLE `stmd_procurement_items` DISABLE KEYS */;
INSERT INTO `stmd_procurement_items` VALUES (1,7,NULL,NULL,NULL,NULL,'www.cargurus.com/acura',NULL,NULL,'black',NULL,1,4,2,2017,'25','123456','','',NULL,NULL,'1535632478_car-vehicle-luxury-112460.jpg',6900.00,1,NULL,6900.00,'submitted',NULL,'2018-08-30',NULL,'0',NULL,'0',''),(2,8,NULL,NULL,NULL,NULL,'www.cargurus.com/acura',NULL,NULL,'black',NULL,1,4,2,2017,'25','123456','','',NULL,NULL,'1535632680_download.jpeg',7200.00,1,NULL,7200.00,'submitted',NULL,'2018-08-30',NULL,'0',NULL,'0',''),(3,9,1,27,38,48,'http://www.flipcart.com','Car radio','Test Description',NULL,NULL,NULL,NULL,NULL,2018,NULL,NULL,'WO','Q7',NULL,NULL,NULL,200.00,10,0.00,2000.00,'received',2,'2018-09-01','111122228877','0',NULL,'0',''),(6,12,1,NULL,NULL,NULL,'https://www.amazon.com/dp/B01DFKC2SO/ref=ods_gw_ha_d_bt_propoff_090418?pf_rd_p=5f8214f2-ab41-4000-9a4e-298f4e650978&pf_rd_r=PN8RJ2RCDZ5JXF420N3V','Echo Dot (2nd Generation) - Smart speaker with Alexa',NULL,'Black',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,49.99,1,10.00,59.99,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(7,13,NULL,NULL,NULL,NULL,'https://www.cargurus.com/Cars/inventorylisting/viewDetailsFilterViewInventoryListing.action?sourceContext=carGurusHomePageModel&entitySelectingHelper.selectedEntity=d191&zip=85001#listing=210629583',NULL,NULL,'Silver',NULL,1,4,2,2017,'160692','19UYA424X3A013345','','',NULL,NULL,'1536647034_acura-cl-2001-2003.jpg',5995.00,1,NULL,5995.00,'received',NULL,'2018-09-11',NULL,'0',NULL,'0',''),(11,17,1,NULL,NULL,NULL,'https://www.amazon.com/dp/B01DFKC2SO/ref=ods_gw_ha_d_bt_propoff_090418?pf_rd_p=5f8214f2-ab41-4000-9a4e-298f4e650978&pf_rd_r=PN8RJ2RCDZ5JXF420N3V','Echo Dot (2nd Generation)',NULL,'Black',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,49.99,2,10.00,109.98,'addedtodelivery',17,'2018-09-30','546546','0','Test','0',''),(12,18,1,27,53,68,'https://www.carid.com/auto-parts.html','Tires','NT421Q Tires by Nitto®. Season: All Season. Type: Performance, Truck / SUV. This NT421Q tire is Nitto’s premier all-season tire built specifically for your crossover or SUV. From style to function, this tire has been created with the...\nIntelligent Maintenance Indicators',NULL,NULL,NULL,NULL,NULL,2017,NULL,NULL,'BMW','Q7',NULL,NULL,NULL,116.00,4,0.00,464.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(13,19,1,27,53,68,'https://www.carid.com/auto-parts.html','Tires','NT421Q Tires by Nitto®. Season: All Season. Type: Performance, Truck / SUV. This NT421Q tire is Nitto’s premier all-season tire built specifically for your crossover or SUV. From style to function, this tire has been created with the...\nIntelligent Maintenance Indicators',NULL,NULL,NULL,NULL,NULL,2017,NULL,NULL,'BMW','Q7',NULL,NULL,NULL,116.00,4,0.00,464.00,'received',17,'2018-09-08','11111','0',NULL,'0',''),(14,20,1,27,53,68,'https://www.carid.com/auto-parts.html','Tires','Test',NULL,NULL,NULL,NULL,NULL,2017,NULL,NULL,'BMW','Q7',NULL,NULL,NULL,116.00,2,0.00,232.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(15,21,1,3,39,23,'https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1','Fire HD 10 Tablet',NULL,'Red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,100.00,1,10.00,110.00,'addedtodelivery',17,'2018-09-26','78978978','0',NULL,'0',''),(16,21,1,3,39,23,'https://www.amazon.com/dp/B01IO618J8/ref=fs_ods_tab_an','Fire 7 Tablet',NULL,'Red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,49.00,2,20.00,118.00,'addedtodelivery',10,'2018-09-30','78978979','0',NULL,'0',''),(17,22,1,3,39,23,'https://www.amazon.com/dp/B01IO618J8/ref=fs_ods_tab_an','Fire 7 Tablet',NULL,'Red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,49.00,1,20.00,69.00,'addedtodelivery',17,'2018-09-11','3214656','0',NULL,'0',''),(18,23,NULL,NULL,NULL,NULL,'web.com',NULL,NULL,NULL,NULL,1,4,2,2016,NULL,NULL,'','',NULL,NULL,NULL,60000.00,1,NULL,60000.00,'received',NULL,'2018-09-12',NULL,'0',NULL,'0',''),(19,24,NULL,NULL,NULL,NULL,'pp21.com',NULL,NULL,'Gray',NULL,1,4,2,2013,'11','OP00977YGD7787O','','',NULL,NULL,'1536752909_maxresdefault.jpg',45667.00,1,NULL,45667.00,'orderplaced',NULL,'2018-09-12',NULL,'0',NULL,'0',''),(20,25,NULL,NULL,NULL,NULL,'www.makemytrip.com',NULL,NULL,'black',NULL,1,4,2,2011,'12','OP00977YGD7787O','','',NULL,NULL,NULL,80000.00,1,NULL,80000.00,'received',NULL,'2018-09-12',NULL,'0',NULL,'0',''),(21,26,NULL,NULL,NULL,NULL,'pp23.com',NULL,NULL,'Red',NULL,1,4,2,2017,'11','11','','',NULL,NULL,'1536753542_maxresdefault.jpg',25000.00,1,NULL,25000.00,'submitted',NULL,'2018-09-12',NULL,'0',NULL,'0',''),(22,27,NULL,NULL,NULL,NULL,'pp23.com',NULL,NULL,'Red',NULL,1,4,2,2017,'11','11','','',NULL,NULL,'1536753542_maxresdefault.jpg',25000.00,1,NULL,25000.00,'received',NULL,'2018-09-12',NULL,'0',NULL,'0',''),(23,28,1,27,55,70,'https://www.carid.com/auto-parts.html','Custom HeadLights','Custom HeadLights',NULL,NULL,NULL,NULL,NULL,2017,NULL,NULL,'BMW','Q7',NULL,NULL,NULL,100.00,2,100.00,300.00,'addedtodelivery',17,'2018-09-11','2222','0',NULL,'0',''),(24,29,NULL,NULL,NULL,NULL,'pp23.com',NULL,NULL,'black',NULL,1,4,2,2013,'12','OP00977YGD7787O','','',NULL,NULL,'1536759414_download (1).jpeg',25000.00,1,NULL,25000.00,'submitted',NULL,'2018-09-12',NULL,'0',NULL,'0',''),(25,30,1,3,NULL,NULL,'https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1','Fire HD 10 Tablet',NULL,'Red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,149.00,1,10.00,159.00,'addedtodelivery',17,'2018-09-13','4646464','0',NULL,'0',''),(26,30,1,3,NULL,NULL,'https://www.amazon.com/Apple-iPad-WiFi-2018-Model/dp/B07D5S5LHF/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1','Fire 7 Tablet',NULL,'Red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,49.00,1,10.00,59.00,'addedtodelivery',17,'2018-09-13','558788','0',NULL,'0',''),(27,31,1,27,38,48,'https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8','GOOACC Nylon Bumper','GOOACC Nylon Bumper',NULL,NULL,NULL,NULL,NULL,2011,NULL,NULL,'Audi','Model',NULL,NULL,NULL,149.00,1,10.00,159.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(28,32,1,27,38,48,'https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8','GOOACC Nylon Bumper','GOOACC Nylon Bumper',NULL,NULL,NULL,NULL,NULL,2011,NULL,NULL,'Audi','Model',NULL,NULL,NULL,149.00,1,10.00,159.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(29,33,1,27,38,48,'https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8','GOOACC Nylon Bumper','GOOACC Nylon Bumper',NULL,NULL,NULL,NULL,NULL,2011,NULL,NULL,'Audi','Model',NULL,NULL,NULL,149.00,1,10.00,159.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(30,34,1,27,53,67,'https://www.carid.com/tires/all-season/','Car Tire','TERRAIN GRIPPER A/T G Tires by AMP®. Season: All Season. Type: Truck / SUV, All Terrain / Off Road / Mud. AMP Tires is proud to introduce the latest addition to its line of off-road light truck tires - the all–new terrain Gripper A/T more details on - https://www.carid.com/tires/all-season/',NULL,NULL,NULL,NULL,NULL,2017,NULL,NULL,'Audi','Q7',NULL,NULL,NULL,115.00,4,0.00,460.00,'addedtodelivery',17,'2018-09-11','4444','0',NULL,'0',''),(31,35,1,3,39,23,'https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1','Fire HD 10 Tablet',NULL,'RED',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,149.00,1,10.00,159.00,'addedtodelivery',17,'2018-09-13','798798897','0',NULL,'0',''),(32,35,1,3,39,23,'https://www.amazon.com/Apple-iPad-WiFi-2018-Model/dp/B07D5S5LHF/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1','Fire 7 Tablet',NULL,'Red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,200.00,1,10.00,210.00,'addedtodelivery',17,'2018-09-13','8787212121','0',NULL,'0',''),(33,36,1,27,53,67,'https://www.carid.com/amp-tires/terrain-gripper-at-g-84753193.html','terrain-gripper','terrain-gripper',NULL,NULL,NULL,NULL,NULL,2014,NULL,NULL,'Make','Model',NULL,NULL,NULL,50.00,1,5.00,55.00,'addedtodelivery',17,'2018-09-13','32123132','0',NULL,'0',''),(34,37,1,3,39,23,'https://www.amazon.com/Apple-iPad-WiFi-2018-Model/dp/B07D5S5LHF/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1','Apple iPad with WiFi (2018 Model) (32 GB, Gold)',NULL,'REd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,100.00,1,10.00,110.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(35,38,1,3,39,23,'amazon.com/ipad','ipad',NULL,'red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,50.00,3,1.00,151.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(36,38,1,3,40,16,'amazon.com/iphone','iphone 7plus',NULL,'grey',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,75.00,2,5.00,155.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(37,38,1,4,41,2,'amazon.com/sunglass','rayban sunglass',NULL,'blue',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,10.00,2,1.00,21.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(38,38,1,4,41,5,'amazon.com/jeans','levis jeans',NULL,'light blue',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,8.00,3,2.00,26.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(39,39,1,3,39,23,'amazon.com/ipad','ipad',NULL,'red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,50.00,3,1.00,151.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(40,39,1,3,40,16,'amazon.com/iphone','iphone 7plus',NULL,'grey',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,75.00,2,5.00,155.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(41,39,1,4,41,2,'amazon.com/sunglass','rayban sunglass',NULL,'blue',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,10.00,2,1.00,21.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(42,39,1,4,41,5,'amazon.com/jeans','levis jeans',NULL,'light blue',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,8.00,3,2.00,26.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(43,40,1,3,39,23,'amazon.com/ipad','ipad',NULL,'red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,50.00,3,1.00,151.00,'addedtodelivery',17,'2018-09-18','11112222','1','test','0',''),(44,40,1,3,40,16,'amazon.com/iphone','iphone 7plus',NULL,'grey',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,75.00,2,5.00,155.00,'addedtodelivery',17,'2018-09-18','11112222','1','tsting','0',''),(45,40,1,4,41,2,'amazon.com/sunglass','rayban sunglass',NULL,'blue',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,10.00,2,1.00,21.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(46,40,1,4,41,5,'amazon.com/jeans','levis jeans',NULL,'light blue',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,8.00,3,2.00,26.00,'submitted',NULL,NULL,NULL,'0',NULL,'0',''),(47,41,1,3,39,23,'amazon.com/ipad','ipad',NULL,'silver',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,100.00,5,2.00,502.00,'addedtodelivery',9,'2018-09-20','789654','0','test note','0',''),(48,42,1,3,39,23,'amazon.com/ipad','ipad',NULL,'red',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,10.00,5,3.00,53.00,'addedtodelivery',4,'2018-09-24','5566','0','test delivery note 48','0',''),(49,42,1,4,41,10,'www.amazon.com/nike','nike shoe',NULL,'flurocent green',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,520.00,2,10.00,1050.00,'addedtodelivery',2,'2018-09-24','5566','0','test delivery note 49','0',''),(50,43,1,4,41,12,'www.amazon.com/titan','titan men',NULL,'golden',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,50.00,1,1.00,51.00,'addedtodelivery',2,'2018-09-01','5566','0','test package 50','0',''),(51,43,1,4,41,12,'www.amazon.com/titan','titan women',NULL,'silver',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,50.00,10,20.00,520.00,'addedtodelivery',17,'2018-09-01','42445','0','dfgdfgfd','0','');
/*!40000 ALTER TABLE `stmd_procurement_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_procurement_shipments`
--

DROP TABLE IF EXISTS `stmd_procurement_shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_procurement_shipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `warehouseId` int(11) NOT NULL,
  `fromCountry` int(11) NOT NULL,
  `fromState` int(11) DEFAULT NULL,
  `fromCity` int(11) DEFAULT NULL,
  `fromAddress` varchar(255) NOT NULL,
  `fromZipCode` varchar(255) NOT NULL,
  `fromName` varchar(255) NOT NULL,
  `fromEmail` varchar(255) NOT NULL,
  `fromPhone` varchar(255) NOT NULL,
  `toCountry` int(11) NOT NULL,
  `toState` int(11) DEFAULT NULL,
  `toCity` int(11) DEFAULT NULL,
  `toAddress` varchar(255) NOT NULL,
  `toZipCode` varchar(255) NOT NULL,
  `toName` varchar(255) NOT NULL,
  `toEmail` varchar(255) NOT NULL,
  `toPhone` varchar(255) NOT NULL,
  `procurementType` enum('buycarforme','shopforme','autopart','othervehicle') NOT NULL DEFAULT 'shopforme',
  `urgent` enum('Y','N') NOT NULL DEFAULT 'N',
  `totalItemCost` decimal(12,2) NOT NULL,
  `totalProcessingFee` decimal(12,2) NOT NULL,
  `urgentPurchaseCost` decimal(12,2) NOT NULL,
  `totalProcurementCost` decimal(12,2) NOT NULL COMMENT 'Shop For Me Cost Total Only',
  `totalWeight` decimal(12,2) DEFAULT NULL,
  `status` enum('draft','requestforcost','submitted','itemreceived','completed','rejected') NOT NULL,
  `procurementLocked` enum('Y','N') NOT NULL DEFAULT 'N',
  `totalPickupCost` decimal(12,2) NOT NULL,
  `totalShippingCost` decimal(12,2) NOT NULL,
  `totalClearingDuty` decimal(12,2) DEFAULT NULL,
  `isDutyCharged` enum('1','0') NOT NULL DEFAULT '1',
  `totalInsurance` decimal(12,2) DEFAULT NULL,
  `totalTax` decimal(12,2) DEFAULT NULL,
  `totalDiscount` decimal(12,2) DEFAULT NULL,
  `couponcodeApplied` varchar(255) DEFAULT NULL,
  `totalCost` decimal(12,2) NOT NULL,
  `paymentStatus` enum('paid','unpaid') NOT NULL DEFAULT 'unpaid',
  `paymentMethodId` int(11) DEFAULT NULL,
  `paymentReceivedOn` datetime DEFAULT NULL,
  `shippingMethod` enum('Y','N') NOT NULL DEFAULT 'N',
  `shippingMethodId` int(11) DEFAULT NULL,
  `isCurrencyChanged` enum('Y','N') NOT NULL DEFAULT 'N',
  `defaultCurrencyCode` varchar(4) DEFAULT NULL,
  `paidCurrencyCode` varchar(4) DEFAULT NULL,
  `exchangeRate` decimal(12,2) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdByType` enum('admin','user') NOT NULL DEFAULT 'user',
  `createdOn` datetime NOT NULL,
  `orderDate` datetime DEFAULT NULL,
  `estimateDeliveryDate` date DEFAULT NULL COMMENT 'Estimated Date of Delivery to Your Warehouse',
  `deliveredOn` date DEFAULT NULL COMMENT ' Date of Product Receipt in Warehouse',
  `deleted` enum('1','0') DEFAULT '0',
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `shippingMethodId` (`shippingMethodId`),
  KEY `warehouseId` (`warehouseId`),
  KEY `fromCountry` (`fromCountry`),
  KEY `fromState` (`fromState`),
  KEY `fromCity` (`fromCity`),
  KEY `toCountry` (`toCountry`),
  KEY `toState` (`toState`),
  KEY `toCity` (`toCity`),
  CONSTRAINT `stmd_procurement_shipments_ibfk_10` FOREIGN KEY (`warehouseId`) REFERENCES `stmd_warehousemaster` (`id`),
  CONSTRAINT `stmd_procurement_shipments_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_procurement_shipments_ibfk_3` FOREIGN KEY (`shippingMethodId`) REFERENCES `stmd_shippingmethods` (`shippingid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_procurement_shipments_ibfk_4` FOREIGN KEY (`fromCountry`) REFERENCES `stmd_countries` (`id`),
  CONSTRAINT `stmd_procurement_shipments_ibfk_5` FOREIGN KEY (`fromState`) REFERENCES `stmd_states` (`id`),
  CONSTRAINT `stmd_procurement_shipments_ibfk_6` FOREIGN KEY (`fromCity`) REFERENCES `stmd_cities` (`id`),
  CONSTRAINT `stmd_procurement_shipments_ibfk_7` FOREIGN KEY (`toCountry`) REFERENCES `stmd_countries` (`id`),
  CONSTRAINT `stmd_procurement_shipments_ibfk_8` FOREIGN KEY (`toState`) REFERENCES `stmd_states` (`id`),
  CONSTRAINT `stmd_procurement_shipments_ibfk_9` FOREIGN KEY (`toCity`) REFERENCES `stmd_cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_procurement_shipments`
--

LOCK TABLES `stmd_procurement_shipments` WRITE;
/*!40000 ALTER TABLE `stmd_procurement_shipments` DISABLE KEYS */;
INSERT INTO `stmd_procurement_shipments` VALUES (7,3,1,230,91,356,'','','','','',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','buycarforme','N',6900.00,483.00,0.00,7383.00,0.00,'submitted','Y',15.55,23.48,0.00,'0',0.00,25.32,0.00,NULL,7447.35,'unpaid',4,NULL,'N',NULL,'N','USD','USD',1.00,3,'user','2018-08-30 12:35:08',NULL,NULL,NULL,'0',NULL,NULL),(8,3,1,230,91,356,'','','','','',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','buycarforme','N',7200.00,504.00,0.00,7704.00,0.00,'submitted','Y',15.55,23.48,0.00,'0',0.00,26.37,0.00,NULL,7769.40,'unpaid',4,NULL,'N',NULL,'N','USD','USD',1.00,3,'user','2018-08-30 12:38:30',NULL,NULL,NULL,'0',NULL,NULL),(9,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Somnath Pahari','somnath.pahari@indusnet.co.in','713-530-1160',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','autopart','N',2000.00,0.00,0.00,2000.00,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,NULL,NULL,NULL,2015.09,'paid',8,'2018-09-21 15:48:13','N',NULL,'N',NULL,'USD',1.00,3,'user','2018-08-30 13:04:35','2018-08-30 13:04:35',NULL,NULL,'0',NULL,NULL),(12,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','shopforme','N',59.99,0.00,0.00,59.99,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,0.00,NULL,NULL,59.99,'paid',1,'2018-09-11 06:50:10','N',NULL,'Y','USD','INR',72.35,3,'user','2018-09-11 06:50:10','2018-09-11 06:50:10',NULL,NULL,'0',NULL,NULL),(13,3,1,230,91,356,'','','','','',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','buycarforme','N',5995.00,419.65,0.00,6414.65,0.00,'completed','Y',15.55,23.48,0.00,'0',586.31,20.00,10.00,'nigeria10',7049.99,'paid',1,NULL,'N',NULL,'N','USD','USD',1.00,3,'user','2018-09-11 07:00:33',NULL,NULL,NULL,'0',NULL,NULL),(17,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. Somnath Pahari','somnath.pahari@indusnet.co.in','123456',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','shopforme','N',109.98,0.00,0.00,109.98,NULL,'completed','Y',0.00,0.00,NULL,'1',NULL,20.00,NULL,NULL,129.98,'paid',1,'2018-09-11 07:09:18','N',NULL,'N','USD','USD',1.00,3,'user','2018-09-11 07:09:18','2018-09-11 07:09:18',NULL,NULL,'0',NULL,NULL),(18,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Somnath Pahari','somnath.pahari@indusnet.co.in','713-530-1160',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','autopart','N',464.00,0.00,0.00,464.00,NULL,'submitted','Y',0.00,113.24,0.00,'',NULL,NULL,NULL,NULL,577.24,'unpaid',7,'2018-09-11 07:30:26','Y',192,'N','USD','USD',1.00,3,'user','2018-09-11 07:30:26','2018-09-11 07:30:26',NULL,NULL,'0',NULL,NULL),(19,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Somnath Pahari','somnath.pahari@indusnet.co.in','713-530-1160',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','autopart','N',464.00,0.00,0.00,464.00,NULL,'completed','Y',0.00,1138.21,523.18,'',NULL,NULL,NULL,NULL,1648.56,'paid',7,'2018-09-11 07:37:50','Y',184,'N','USD','USD',1.00,3,'user','2018-09-11 07:37:50','2018-09-11 07:37:50',NULL,NULL,'0',NULL,NULL),(20,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Somnath Pahari','somnath.pahari@indusnet.co.in','713-530-1160',163,2,639,'211 South Meadows Court','77479','somnath pahari','somnath.pahari@indusnet.co.in','713-530-1160','autopart','N',232.00,0.00,0.00,232.00,NULL,'submitted','Y',0.00,113.24,0.00,'',NULL,NULL,NULL,NULL,362.23,'paid',8,'2018-09-13 07:26:46','Y',192,'N','USD','USD',1.00,3,'user','2018-09-11 07:51:48','2018-09-11 07:51:48',NULL,NULL,'0',NULL,NULL),(21,7,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. Nduka Udeh','n.udeh@aascargo.com','123456',163,5,378,'12011 Westbrae Parkway','77031','Nduka Udeh','n.udeh@shoptomydoor.com','7135301160','shopforme','N',228.00,0.00,0.00,228.00,NULL,'completed','Y',0.00,0.00,NULL,'1',NULL,20.00,NULL,NULL,248.00,'paid',1,'2018-09-13 06:11:15','N',NULL,'N','USD','USD',1.00,7,'user','2018-09-11 09:14:47','2018-09-11 09:14:47',NULL,NULL,'0',NULL,NULL),(22,7,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. Nduka Udeh','n.udeh@aascargo.com','123456',163,5,378,'12011 Westbrae Parkway','77031','Nduka Udeh','n.udeh@shoptomydoor.com','7135301160','shopforme','Y',69.00,0.00,5.00,74.00,NULL,'completed','Y',0.00,0.00,NULL,'1',NULL,20.00,NULL,NULL,94.00,'paid',1,'2018-09-11 09:23:18','N',NULL,'N','USD','USD',1.00,7,'user','2018-09-11 09:21:54','2018-09-11 09:21:54',NULL,NULL,'0',NULL,NULL),(23,3,1,230,91,356,'','','','','',163,15,638,'Address 1','67676','Paulami Sarkar','paulami.sarkar@indusnet.co.in','6767','buycarforme','N',60000.00,4200.00,0.00,64200.00,0.00,'submitted','Y',990.00,23.48,0.00,'0',5868.00,0.00,0.00,NULL,71081.48,'paid',2,'2018-09-12 11:33:40','N',NULL,'N','USD','USD',1.00,3,'user','2018-09-12 11:26:11',NULL,NULL,NULL,'0',NULL,NULL),(24,3,1,230,80,13,'','','','','',163,15,638,'Address 1','67676','Paulami Sarkar','paulami.sarkar@indusnet.co.in','6767','buycarforme','N',45667.00,3196.69,0.00,48863.69,0.00,'submitted','Y',49.99,23.48,0.00,'0',4466.23,0.00,0.00,NULL,53403.39,'paid',2,'2018-09-12 11:49:36','N',NULL,'N','USD','USD',1.00,3,'user','2018-09-12 11:49:05',NULL,NULL,NULL,'0',NULL,NULL),(25,3,1,230,80,13,'','','','','',163,15,638,'Address 1','67676','Paulami Sarkar','paulami.sarkar@indusnet.co.in','6767','buycarforme','N',80000.00,5600.00,0.00,85600.00,0.00,'submitted','Y',49.99,23.48,0.00,'0',0.00,281.17,0.00,NULL,85954.64,'paid',6,'2018-09-12 11:56:32','N',NULL,'N','USD','USD',1.00,3,'user','2018-09-12 11:55:56',NULL,NULL,NULL,'0',NULL,NULL),(26,3,1,230,80,13,'','','','','',163,15,638,'Address 1','67676','Paulami Sarkar','paulami.sarkar@indusnet.co.in','6767','buycarforme','N',25000.00,1750.00,0.00,26750.00,0.00,'submitted','Y',49.99,23.48,0.00,'0',0.00,0.00,0.00,NULL,26823.47,'unpaid',2,NULL,'N',NULL,'N','USD','USD',1.00,3,'user','2018-09-12 11:59:18',NULL,NULL,NULL,'0',NULL,NULL),(27,3,1,230,80,13,'','','','','',163,15,638,'Address 1','67676','Paulami Sarkar','paulami.sarkar@indusnet.co.in','6767','buycarforme','N',25000.00,1750.00,0.00,26750.00,0.00,'completed','Y',49.99,23.48,0.00,'0',0.00,0.00,0.00,NULL,26823.47,'paid',2,'2018-09-12 12:01:56','N',NULL,'N','USD','USD',1.00,3,'user','2018-09-12 11:59:22',NULL,NULL,NULL,'0',NULL,NULL),(28,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Somnath Pahari','somnath.pahari@indusnet.co.in','713-530-1160',163,15,638,'Address 1','67676','Paulami Sarkar','paulami.sarkar@indusnet.co.in','6767','autopart','N',300.00,0.00,0.00,300.00,NULL,'completed','Y',0.00,188.00,0.00,'',NULL,NULL,NULL,NULL,516.20,'paid',8,'2018-09-12 13:26:53','Y',192,'N','USD','USD',1.00,3,'user','2018-09-12 13:25:45','2018-09-12 13:25:45',NULL,NULL,'0',NULL,NULL),(29,3,1,230,80,13,'','','','','',163,15,638,'Address 1','67676','Paulami Sarkar','paulami.sarkar@indusnet.co.in','6767','buycarforme','N',25000.00,1750.00,0.00,26750.00,0.00,'submitted','Y',49.99,23.48,0.00,'0',0.00,0.00,0.00,NULL,26823.47,'unpaid',2,NULL,'N',NULL,'N','USD','USD',1.00,3,'user','2018-09-12 13:36:59',NULL,NULL,NULL,'0',NULL,NULL),(30,13,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608',230,90,588,'SDF Building','852000','Joy Karmakar','karmakar.joy@gmail.com','9831723608','shopforme','N',218.00,0.00,0.00,218.00,NULL,'completed','Y',0.00,0.00,NULL,'1',NULL,0.00,NULL,NULL,218.00,'paid',1,'2018-09-13 07:56:30','N',NULL,'Y','USD','INR',71.95,13,'user','2018-09-13 07:55:16','2018-09-13 07:55:16',NULL,NULL,'0',NULL,NULL),(31,13,1,230,120,256,'12011 Westbrae Parkway #B','77031','Joy Karmakar','karmakar.joy@gmail.com','9831723608',230,90,522,'SDF Building','852000','Joy Karmakar','karmakar.joy@gmail.com','9831723608','autopart','Y',159.00,0.00,0.00,159.00,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,NULL,NULL,NULL,179.00,'unpaid',1,'2018-09-13 08:06:44','N',NULL,'N','USD','USD',1.00,13,'user','2018-09-13 08:06:44','2018-09-13 08:06:44',NULL,NULL,'0',NULL,NULL),(32,13,1,230,120,256,'12011 Westbrae Parkway #B','77031','Joy Karmakar','karmakar.joy@gmail.com','9831723608',230,90,522,'SDF Building','852000','Joy Karmakar','karmakar.joy@gmail.com','9831723608','autopart','Y',159.00,0.00,0.00,159.00,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,NULL,NULL,NULL,179.00,'unpaid',1,'2018-09-13 08:14:39','N',NULL,'N','USD','USD',1.00,13,'user','2018-09-13 08:14:39','2018-09-13 08:14:39',NULL,NULL,'0',NULL,NULL),(33,13,1,230,120,256,'12011 Westbrae Parkway #B','77031','Joy Karmakar','karmakar.joy@gmail.com','9831723608',230,90,522,'SDF Building','852000','Joy Karmakar','karmakar.joy@gmail.com','9831723608','autopart','Y',159.00,0.00,0.00,159.00,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,NULL,NULL,NULL,179.00,'unpaid',1,'2018-09-13 08:17:10','N',NULL,'N','USD','USD',1.00,13,'user','2018-09-13 08:17:10','2018-09-13 08:17:10',NULL,NULL,'0',NULL,NULL),(34,13,1,230,120,256,'12011 Westbrae Parkway #B','77031','Joy Karmakar','karmakar.joy@gmail.com','9831723608',230,90,239,'SDF Building','852000','Joy Karmakar','karmakar.joy@gmail.com','9831723608','autopart','N',460.00,0.00,0.00,460.00,NULL,'completed','Y',0.00,0.00,NULL,'1',NULL,NULL,NULL,NULL,480.00,'paid',1,'2018-09-13 08:23:21','N',NULL,'N','USD','USD',1.00,13,'user','2018-09-13 08:23:21','2018-09-13 08:23:21',NULL,NULL,'0',NULL,NULL),(35,13,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. Joy Karmakar','karmakar.joy@gmail.com','9831723608',230,90,588,'SDF Building','852000','Joy Karmakar','karmakar.joy@gmail.com','9831723608','shopforme','N',369.00,0.00,0.00,369.00,NULL,'completed','Y',0.00,0.00,NULL,'1',NULL,0.00,NULL,NULL,369.00,'paid',1,'2018-09-13 08:55:23','N',NULL,'Y','USD','INR',71.95,13,'user','2018-09-13 08:53:41','2018-09-13 08:53:41',NULL,NULL,'0',NULL,NULL),(36,13,1,230,120,256,'12011 Westbrae Parkway #B','77031','Joy Karmakar','karmakar.joy@gmail.com','9831723608',230,90,588,'SDF Building','852000','Joy Karmakar','karmakar.joy@gmail.com','9831723608','autopart','N',55.00,0.00,0.00,55.00,NULL,'completed','Y',0.00,0.00,NULL,'1',NULL,NULL,NULL,NULL,75.00,'paid',1,'2018-09-13 09:11:04','N',NULL,'N','USD','USD',1.00,13,'user','2018-09-13 09:11:04','2018-09-13 09:11:04',NULL,NULL,'0',NULL,NULL),(37,7,1,230,120,256,'12011 Westbrae Parkway #B','77031','Nduka Udeh','n.udeh@aascargo.com','123456',163,5,378,'12011 Westbrae Parkway','77031','Nduka Udeh','n.udeh@shoptomydoor.com','7135301160','shopforme','N',110.00,0.00,0.00,110.00,3.00,'requestforcost','N',0.00,0.00,NULL,'1',NULL,NULL,NULL,NULL,110.00,'unpaid',NULL,'2018-09-19 06:44:18','N',NULL,'N',NULL,NULL,NULL,7,'user','2018-09-19 06:44:18','2018-09-19 06:44:18',NULL,NULL,'0',NULL,NULL),(38,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456',163,4,53,'10 A ROY PARA BYE LANE','700091','Tathagata sur','tathagata.sur@indusnet.co.uk','+91+90+92+9208961186006','shopforme','N',353.00,0.00,0.00,353.00,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,0.00,NULL,NULL,353.00,'unpaid',6,NULL,'N',NULL,'N','USD','USD',1.00,3,'user','2018-09-20 12:56:55','2018-09-20 12:56:55',NULL,NULL,'0',NULL,NULL),(39,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456',163,4,53,'10 A ROY PARA BYE LANE','700091','Tathagata sur','tathagata.sur@indusnet.co.uk','+91+90+92+9208961186006','shopforme','N',353.00,0.00,0.00,353.00,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,0.00,NULL,NULL,353.00,'unpaid',6,NULL,'N',NULL,'N','USD','USD',1.00,3,'user','2018-09-20 12:58:39','2018-09-20 12:58:39',NULL,NULL,'0',NULL,NULL),(40,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456',163,4,53,'10 A ROY PARA BYE LANE','700091','Tathagata sur','tathagata.sur@indusnet.co.uk','+91+90+92+9208961186006','shopforme','N',353.00,0.00,0.00,353.00,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,0.00,NULL,NULL,353.00,'paid',6,'2018-09-20 13:01:50','N',NULL,'N','USD','USD',1.00,3,'user','2018-09-20 12:59:43','2018-09-20 12:59:43',NULL,NULL,'0',NULL,NULL),(41,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456',163,4,53,'10 A ROY PARA BYE LANE','700091','Tathagata sur','tathagata.sur@indusnet.co.uk','+91+90+92+9208961186006','shopforme','N',502.00,0.00,0.00,502.00,NULL,'completed','Y',0.00,55.37,74.91,'',NULL,6.51,NULL,NULL,638.79,'paid',5,'2018-09-21 17:53:44','Y',184,'N','USD','USD',1.00,3,'user','2018-09-21 17:53:07','2018-09-21 17:53:07',NULL,NULL,'0',NULL,NULL),(42,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456',163,4,53,'10 A ROY PARA BYE LANE','700091','Tathagata sur','tathagata.sur@indusnet.co.uk','+91+90+92+9208961186006','shopforme','Y',1103.00,0.00,11.03,1114.03,NULL,'completed','Y',0.00,65.97,145.03,'',111.29,10.55,NULL,NULL,1446.87,'paid',6,'2018-09-26 15:12:35','Y',184,'N','USD','USD',1.00,3,'user','2018-09-26 15:01:07','2018-09-26 15:01:07',NULL,NULL,'0',NULL,NULL),(43,3,1,230,120,256,'12011 Westbrae Parkway #B','77031','Mr. S Pahari','somnath.pahari@indusnet.co.in','123456',163,4,53,'10 A ROY PARA BYE LANE','700091','Tathagata sur','tathagata.sur@indusnet.co.uk','+91+90+92+9208961186006','shopforme','Y',571.00,0.00,5.71,576.71,NULL,'submitted','Y',0.00,0.00,NULL,'1',NULL,0.00,NULL,NULL,576.71,'paid',5,'2018-09-26 17:36:48','N',NULL,'N','USD','USD',1.00,3,'user','2018-09-26 15:05:02','2018-09-26 15:05:02',NULL,NULL,'0',NULL,NULL);
/*!40000 ALTER TABLE `stmd_procurement_shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_procurement_warehouse_messages`
--

DROP TABLE IF EXISTS `stmd_procurement_warehouse_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_procurement_warehouse_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procurementId` int(11) NOT NULL,
  `messageId` int(11) NOT NULL,
  `sentBy` int(11) NOT NULL,
  `sentOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`procurementId`),
  KEY `messageId` (`messageId`),
  CONSTRAINT `stmd_procurement_warehouse_messages_ibfk_1` FOREIGN KEY (`procurementId`) REFERENCES `stmd_procurement_shipments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_procurement_warehouse_messages_ibfk_2` FOREIGN KEY (`messageId`) REFERENCES `stmd_warehouse_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_procurement_warehouse_messages`
--

LOCK TABLES `stmd_procurement_warehouse_messages` WRITE;
/*!40000 ALTER TABLE `stmd_procurement_warehouse_messages` DISABLE KEYS */;
INSERT INTO `stmd_procurement_warehouse_messages` VALUES (1,35,1,1,'2018-09-13 09:01:21');
/*!40000 ALTER TABLE `stmd_procurement_warehouse_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_procurementfees`
--

DROP TABLE IF EXISTS `stmd_procurementfees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_procurementfees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feeLeft` varchar(32) NOT NULL,
  `feeRight` varchar(32) NOT NULL,
  `feeType` enum('A','P') NOT NULL DEFAULT 'P',
  `value` varchar(32) NOT NULL,
  `warehouseId` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_procurementfees`
--

LOCK TABLES `stmd_procurementfees` WRITE;
/*!40000 ALTER TABLE `stmd_procurementfees` DISABLE KEYS */;
INSERT INTO `stmd_procurementfees` VALUES (1,'1','100','A','20',1,'2018-05-28 12:47:33',0,'2018-05-29 15:09:03',1,'1','2018-05-30 11:51:32',1),(2,'100.1','5000','P','7',1,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'1','2018-06-26 12:15:08',1),(5,'5000.1','10000','P','7',1,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(6,'10000.1','1000000','P','7',1,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(7,'1','165','A','25',2,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(8,'165.1','500','P','12',2,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(9,'500.1','1000','P','10',2,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(10,'1000.1','5000','P','9',2,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(11,'5000.1','10000','P','7',2,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(12,'10000.1','1000000','P','6',2,'2018-05-28 12:47:33',0,'2018-06-21 11:12:57',1,'0','2018-05-28 12:47:33',0),(13,'1','100','A','25',3,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'1','2018-05-29 15:09:26',1),(14,'100.1','500','P','12',3,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(15,'500.1','1000','P','10',3,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(16,'1000.1','5000','P','8',3,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(17,'5000.1','10000','P','7',3,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(18,'10000.1','1000000','P','6',3,'2018-05-28 12:47:33',0,'2018-05-28 12:47:33',0,'0','2018-05-28 12:47:33',0),(19,'10','50','P','2',2,'2018-05-29 15:04:10',1,'2018-05-29 15:04:10',0,'0','2018-05-29 15:04:10',0);
/*!40000 ALTER TABLE `stmd_procurementfees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_rewardsettings`
--

DROP TABLE IF EXISTS `stmd_rewardsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_rewardsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `descr` text CHARACTER SET utf8,
  `points` int(11) NOT NULL,
  `note_10` text CHARACTER SET utf8,
  `note_50` text CHARACTER SET utf8,
  `note_100` text CHARACTER SET utf8,
  `image` text,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `modifiedOn` datetime DEFAULT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_rewardsettings`
--

LOCK TABLES `stmd_rewardsettings` WRITE;
/*!40000 ALTER TABLE `stmd_rewardsettings` DISABLE KEYS */;
INSERT INTO `stmd_rewardsettings` VALUES (1,'Bronze','<ul>\r\n	<li>Start earning at just $1 spend</li>\r\n	<li>No discounts until you hit Silver</li>\r\n</ul>',1,NULL,NULL,NULL,'bronze.png','0','2018-09-04 12:26:13',1),(2,'Silver','<div>\r\n	Coming Soon</div>\r\n',5000,NULL,NULL,NULL,'silver.png','0',NULL,NULL),(3,'Gold','<div>\r\n	Coming Soon</div>\r\n',10000,NULL,NULL,NULL,'gold.png','0',NULL,NULL),(4,'Platinum','<div>\r\n	Coming Soon</div>\r\n',100000,NULL,NULL,NULL,'platinum.png','0',NULL,NULL);
/*!40000 ALTER TABLE `stmd_rewardsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_schedule_images`
--

DROP TABLE IF EXISTS `stmd_schedule_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_schedule_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL,
  `imageName` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `stmd_schedule_images_ibfk_1` (`scheduleId`),
  CONSTRAINT `stmd_schedule_images_ibfk_1` FOREIGN KEY (`scheduleId`) REFERENCES `stmd_scheduled_pickups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_schedule_images`
--

LOCK TABLES `stmd_schedule_images` WRITE;
/*!40000 ALTER TABLE `stmd_schedule_images` DISABLE KEYS */;
INSERT INTO `stmd_schedule_images` VALUES (1,1,'order_tracking_online-256.png','1','2018-06-05 10:38:20',1,'2018-06-05 10:38:20',0,'0','2018-06-05 10:38:20',0),(2,1,'order_tracking_online-256.png','1','2018-06-05 10:38:20',1,'2018-06-05 10:38:20',0,'0','2018-06-05 10:38:20',0);
/*!40000 ALTER TABLE `stmd_schedule_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_schedule_notification`
--

DROP TABLE IF EXISTS `stmd_schedule_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_schedule_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referenceId` int(11) NOT NULL COMMENT 'Shipment/Order Id',
  `customerId` int(11) NOT NULL,
  `emailTemplateId` int(11) NOT NULL,
  `smsTemplateId` int(11) NOT NULL,
  `notificationType` enum('S','O') NOT NULL DEFAULT 'S' COMMENT 'S=Shipment, O=Order',
  `scheduleDate` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emailSentStatus` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=Not sent, 1=Sent ',
  `smsSentStatus` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=Not sent, 1=Sent ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_schedule_notification`
--

LOCK TABLES `stmd_schedule_notification` WRITE;
/*!40000 ALTER TABLE `stmd_schedule_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_schedule_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_scheduled_pickups`
--

DROP TABLE IF EXISTS `stmd_scheduled_pickups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_scheduled_pickups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `totalPackages` int(11) NOT NULL,
  `totalWeight` double(12,2) NOT NULL,
  `totalWeightUnit` varchar(5) NOT NULL,
  `dimList` text NOT NULL,
  `shipDate` date NOT NULL,
  `requestedPickupTime` datetime NOT NULL,
  `requestReceivedTime` datetime NOT NULL,
  `instructions` text NOT NULL,
  `scheduleFromCountry` int(11) NOT NULL,
  `scheduleFromCity` int(11) NOT NULL,
  `scheduleFromState` int(11) NOT NULL,
  `scheduleFromAddress` varchar(255) NOT NULL,
  `scheduleFromZipcode` int(8) NOT NULL,
  `scheduleFromName` varchar(255) NOT NULL,
  `scheduleFromCompany` varchar(255) NOT NULL,
  `scheduleFromAddress2` varchar(255) NOT NULL,
  `scheduleFromPhone` varchar(255) NOT NULL,
  `scheduleFromEmail` varchar(255) NOT NULL,
  `scheduleTocountry` int(11) NOT NULL,
  `scheduleToCity` int(11) NOT NULL,
  `scheduleToState` int(11) NOT NULL,
  `scheduleToAddress` varchar(255) NOT NULL,
  `scheduleToZipcode` int(8) NOT NULL,
  `scheduleToName` varchar(255) NOT NULL,
  `scheduleToCompany` varchar(255) NOT NULL,
  `scheduleToAddress2` varchar(255) NOT NULL,
  `scheduleToPhone` varchar(255) NOT NULL,
  `scheduleToEmail` varchar(255) NOT NULL,
  `scheduleStatus` enum('P','S','C') NOT NULL DEFAULT 'P' COMMENT 'P=Pending, S=Scheduled, C=Completed',
  `scheduleFromResidence` enum('Y','N') NOT NULL DEFAULT 'Y',
  `pickupType` enum('H','A') NOT NULL DEFAULT 'A' COMMENT 'H=Houston A=Anywhere',
  `scheduleFromResidenceCode` int(11) NOT NULL,
  `assignedDriverId` int(11) NOT NULL,
  `assignedDate` datetime NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `assignedDriverId` (`assignedDriverId`),
  KEY `scheduleFromCountry` (`scheduleFromCountry`),
  KEY `scheduleFromCity` (`scheduleFromCity`),
  KEY `scheduleFromCity_2` (`scheduleFromCity`),
  KEY `scheduleFromState` (`scheduleFromState`),
  KEY `scheduleTocountry` (`scheduleTocountry`),
  KEY `scheduleToState` (`scheduleToState`),
  KEY `scheduleToCity` (`scheduleToCity`),
  CONSTRAINT `stmd_scheduled_pickups_ibfk_1` FOREIGN KEY (`assignedDriverId`) REFERENCES `stmd_drivers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_scheduled_pickups_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_scheduled_pickups_ibfk_3` FOREIGN KEY (`scheduleFromCountry`) REFERENCES `stmd_countries` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_scheduled_pickups_ibfk_4` FOREIGN KEY (`scheduleFromState`) REFERENCES `stmd_states` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_scheduled_pickups_ibfk_5` FOREIGN KEY (`scheduleFromCity`) REFERENCES `stmd_cities` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_scheduled_pickups_ibfk_6` FOREIGN KEY (`scheduleTocountry`) REFERENCES `stmd_countries` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_scheduled_pickups_ibfk_7` FOREIGN KEY (`scheduleToState`) REFERENCES `stmd_states` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `stmd_scheduled_pickups_ibfk_8` FOREIGN KEY (`scheduleToCity`) REFERENCES `stmd_cities` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_scheduled_pickups`
--

LOCK TABLES `stmd_scheduled_pickups` WRITE;
/*!40000 ALTER TABLE `stmd_scheduled_pickups` DISABLE KEYS */;
INSERT INTO `stmd_scheduled_pickups` VALUES (1,1,3,100.00,'lbs','6 bundles of hair\r\n5 pairs of shoes\r\nBaby formula','2018-06-01','2018-06-01 10:29:30','2018-06-11 08:24:23','Please remove from box and pack contents into sealed bubble wrap envelope. then re calculate the cost please. shipping fees is more expensive then the item',230,13,80,'211 South Meadows Court',77031,'Paulami','INT','','9852633994','paulami.sarkar@inudusnet.co.in',230,256,120,'Fairfield Inn & Suites Cookeville',77031,'','','','','fairfieldinn17@gmail.com','S','Y','H',0,2,'2018-06-08 00:00:00','1','2018-06-01 10:29:46',1,'2018-06-07 14:17:16',1,'0','2018-06-01 14:51:14',1);
/*!40000 ALTER TABLE `stmd_scheduled_pickups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_sentlogcouponcodes`
--

DROP TABLE IF EXISTS `stmd_sentlogcouponcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_sentlogcouponcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `email` text NOT NULL,
  `couponCode` text NOT NULL,
  `sent` enum('Y','N') NOT NULL DEFAULT 'N',
  `sentDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_sentlogcouponcodes`
--

LOCK TABLES `stmd_sentlogcouponcodes` WRITE;
/*!40000 ALTER TABLE `stmd_sentlogcouponcodes` DISABLE KEYS */;
INSERT INTO `stmd_sentlogcouponcodes` VALUES (1,1,'Paulami','Sarkar','paulami.sarkar@indusnet.co.in','A097','Y',NULL),(2,2,'ANUMITA','BANERJEE','ANUMITADAS@GMAIL.COM','A097','Y',NULL),(3,3,'Somnath','Pahari','somnath.pahari@indusnet.co.in','A097','Y',NULL),(4,6,'virat','kohli','virat@gmail.com','A097','Y',NULL),(5,1,'Paulami','Sarkar','paulami.sarkar@indusnet.co.in','OFFER10','Y',NULL),(6,2,'ANUMITA','BANERJEE','ANUMITADAS@GMAIL.COM','OFFER10','Y',NULL),(7,3,'Somnath','Pahari','somnath.pahari@indusnet.co.in','OFFER10','Y',NULL),(8,6,'virat','kohli','virat@gmail.com','OFFER10','Y',NULL),(9,1,'Paulami','Sarkar','paulami.sarkar@indusnet.co.in','OFFER10','Y',NULL),(10,2,'ANUMITA','BANERJEE','ANUMITADAS@GMAIL.COM','OFFER10','Y',NULL),(11,3,'Somnath','Pahari','somnath.pahari@indusnet.co.in','OFFER10','Y',NULL),(12,6,'virat','kohli','virat@gmail.com','OFFER10','Y',NULL),(13,6,'virat','kohli','virat@gmail.com','OFFER10','Y',NULL),(14,3,'Somnath','Pahari','somnath.pahari@indusnet.co.in','OFFER10','Y',NULL),(15,2,'ANUMITA','BANERJEE','ANUMITADAS@GMAIL.COM','OFFER10','Y',NULL),(16,1,'Paulami','Sarkar','paulami.sarkar@indusnet.co.in','OFFER10','Y',NULL);
/*!40000 ALTER TABLE `stmd_sentlogcouponcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_servicetypes`
--

DROP TABLE IF EXISTS `stmd_servicetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_servicetypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_servicetypes`
--

LOCK TABLES `stmd_servicetypes` WRITE;
/*!40000 ALTER TABLE `stmd_servicetypes` DISABLE KEYS */;
INSERT INTO `stmd_servicetypes` VALUES (5,'AASC_Air_USA','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(6,'AASC_Air_UK','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(7,'AASC_Air_China','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(8,'AASC_Ocean_USA','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(9,'AASC_Ocean_China','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(10,'AASC_Ocean_UK','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(11,'Procurement_USA','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'1','2018-05-17 06:01:37',1),(12,'Procurement_UK','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(13,'Procurement_China','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(14,'Home_Delivery','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(15,'Warehousing_Nigeria','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(16,'Warehousing_USA','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(17,'Other Income_USA','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(18,'Other Income_Nigeria','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(20,'Fedex_USA','1','2018-05-16 14:56:08',0,'2018-05-22 09:25:03',1,'0','2018-05-16 14:56:08',0),(21,'Clearing_Nigeria','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(22,'Insurance_Nigeria','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(23,'Return_USA','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(24,'AASC_Ocean_RORO','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(25,'AASC_Ocean_FCL','1','2018-05-16 14:56:08',0,'2018-05-16 14:56:08',0,'0','2018-05-16 14:56:08',0),(26,'DHL_USA','1','2018-05-16 14:56:08',0,'2018-05-17 07:52:27',0,'0','2018-05-16 14:56:08',0),(27,'Test12','1','2018-05-16 16:03:15',0,'2018-05-16 16:04:26',0,'1','2018-05-16 16:13:40',1),(28,'Test11','1','2018-05-16 16:16:47',0,'2018-05-16 16:16:47',0,'1','2018-05-16 16:16:52',1),(29,'Test123','1','2018-05-17 07:50:11',0,'2018-05-17 07:50:24',0,'1','2018-05-17 07:50:29',1),(30,'aaaa','1','2018-05-17 07:52:47',0,'2018-05-17 07:52:48',0,'1','2018-05-17 07:52:54',1),(31,'Test22','1','2018-05-22 09:12:34',1,'2018-05-22 09:13:03',1,'1','2018-05-22 09:13:31',1),(32,'evinceweb','1','2018-05-22 12:36:10',1,'2018-05-22 12:36:10',0,'0','2018-05-22 12:36:10',0);
/*!40000 ALTER TABLE `stmd_servicetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_deliveries`
--

DROP TABLE IF EXISTS `stmd_shipment_deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL,
  `deliveredOn` date NOT NULL,
  `deliveryCompanyId` int(11) DEFAULT NULL,
  `dispatchCompanyId` int(11) DEFAULT NULL,
  `length` decimal(12,2) NOT NULL DEFAULT '0.00',
  `width` decimal(12,2) NOT NULL DEFAULT '0.00',
  `height` decimal(12,2) NOT NULL DEFAULT '0.00',
  `weight` decimal(12,2) DEFAULT '0.00',
  `chargeableWeight` decimal(12,2) DEFAULT '0.00',
  `totalItemCost` decimal(12,2) DEFAULT NULL,
  `shippingCost` decimal(12,2) DEFAULT NULL,
  `clearingDutyCost` decimal(12,2) DEFAULT NULL,
  `isDutyCharged` enum('1','0') NOT NULL DEFAULT '0',
  `inventoryCharge` decimal(12,2) DEFAULT NULL,
  `otherChargeCost` decimal(12,2) DEFAULT NULL,
  `maxStorageDate` date DEFAULT NULL,
  `storageCharge` decimal(12,2) DEFAULT NULL,
  `totalCost` decimal(12,2) DEFAULT NULL,
  `tracking` varchar(255) DEFAULT NULL,
  `tracking2` varchar(255) DEFAULT '',
  `snapshot` enum('Y','N') NOT NULL DEFAULT 'N',
  `snapshotRequestedOn` datetime DEFAULT NULL,
  `recount` enum('Y','N') NOT NULL DEFAULT 'N',
  `recountRequestedOn` datetime DEFAULT NULL,
  `reweigh` enum('Y','N') NOT NULL DEFAULT 'N',
  `reweighRequestedOn` datetime DEFAULT NULL,
  `wrongInventory` enum('Y','N') NOT NULL DEFAULT 'N',
  `wrongInventoryMessage` text,
  `received` enum('Y','N') NOT NULL DEFAULT 'N',
  `shippingMethodId` int(11) DEFAULT NULL,
  `defaultCurrencyCode` varchar(4) DEFAULT NULL,
  `paidCurrencyCode` varchar(4) DEFAULT NULL,
  `exchangeRate` decimal(12,2) DEFAULT NULL,
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `createdByType` enum('user','admin') NOT NULL DEFAULT 'user',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `deletedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedByType` enum('user','admin') DEFAULT 'user',
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  KEY `deliveryCompanyId` (`deliveryCompanyId`),
  KEY `dispatchCompanyId` (`dispatchCompanyId`),
  CONSTRAINT `stmd_shipment_deliveries_ibfk_3` FOREIGN KEY (`deliveryCompanyId`) REFERENCES `stmd_delivery_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_shipment_deliveries_ibfk_4` FOREIGN KEY (`dispatchCompanyId`) REFERENCES `stmd_dispatch_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_deliveries`
--

LOCK TABLES `stmd_shipment_deliveries` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_deliveries` DISABLE KEYS */;
INSERT INTO `stmd_shipment_deliveries` VALUES (1,1,'0000-00-00',NULL,NULL,0.00,0.00,0.00,NULL,NULL,3761.49,55.37,400.53,'',0.69,20.97,'0000-00-00',0.00,4239.05,NULL,'','Y','2018-09-14 14:13:38','Y','2018-09-14 14:14:08','Y','2018-09-14 14:14:18','N',NULL,'Y',184,'USD','USD',1.00,'0','admin',1,'2018-09-11 06:01:04',NULL,NULL,'user'),(2,2,'0000-00-00',NULL,NULL,0.00,0.00,0.00,NULL,NULL,3761.49,55.37,400.53,'',0.69,6.99,'0000-00-00',0.00,4225.07,NULL,'','Y','2018-09-14 14:14:43','N',NULL,'N',NULL,'N',NULL,'Y',184,'USD','USD',1.00,'0','admin',1,'2018-09-11 06:01:05',NULL,NULL,'user'),(3,5,'0000-00-00',NULL,NULL,0.00,0.00,0.00,3.00,3.00,69.00,NULL,NULL,'0',0.69,0.00,'2018-09-25',0.00,69.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-11 09:25:20',NULL,NULL,'user'),(4,6,'0000-00-00',NULL,NULL,0.00,0.00,0.00,0.00,0.00,300.00,100.00,0.00,'',0.69,0.00,'0000-00-00',0.00,516.89,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',192,'USD','USD',1.00,'0','admin',1,'2018-09-12 13:32:02',NULL,NULL,'user'),(5,7,'0000-00-00',NULL,NULL,0.00,0.00,0.00,6.00,6.00,228.00,NULL,NULL,'0',0.69,0.00,'2018-10-14',0.00,228.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-13 06:13:03',NULL,NULL,'user'),(6,8,'0000-00-00',NULL,NULL,0.00,0.00,0.00,0.00,0.00,159.00,NULL,NULL,'0',0.69,0.00,'2018-09-27',0.00,159.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-13 07:58:22',NULL,NULL,'user'),(7,8,'0000-00-00',NULL,NULL,0.00,0.00,0.00,0.00,0.00,59.00,NULL,NULL,'0',0.69,0.00,'2018-09-27',0.00,59.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-13 07:58:38',NULL,NULL,'user'),(8,9,'0000-00-00',NULL,NULL,0.00,0.00,0.00,0.00,0.00,NULL,0.00,0.00,'1',0.69,0.00,'2018-09-25',0.00,480.00,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-13 08:25:31',NULL,NULL,'user'),(9,10,'0000-00-00',NULL,NULL,0.00,0.00,0.00,3.00,3.00,159.00,NULL,NULL,'0',0.69,0.00,'2018-09-27',0.00,159.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-13 09:04:10',NULL,NULL,'user'),(10,10,'0000-00-00',NULL,NULL,0.00,0.00,0.00,3.00,3.00,210.00,NULL,NULL,'0',0.69,0.00,'2018-09-27',0.00,210.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-13 09:06:37',NULL,NULL,'user'),(11,11,'0000-00-00',NULL,NULL,0.00,0.00,0.00,0.00,0.00,NULL,5.00,0.00,'1',0.69,0.00,'2018-09-27',0.00,75.00,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-13 09:15:37',NULL,NULL,'user'),(12,12,'0000-00-00',NULL,NULL,0.00,0.00,0.00,0.00,0.00,109.98,NULL,NULL,'0',0.69,0.00,'2018-10-14',0.00,110.67,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-14 08:37:11',NULL,NULL,'user'),(14,15,'0000-00-00',NULL,NULL,10.00,20.00,30.00,50.00,50.00,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'N',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-19 20:09:35',NULL,NULL,'user'),(15,16,'0000-00-00',NULL,NULL,10.00,20.00,30.00,50.00,50.00,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'N',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-19 20:10:27',NULL,NULL,'user'),(16,17,'0000-00-00',NULL,NULL,10.00,20.00,30.00,50.00,50.00,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'N',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-19 20:10:56',NULL,NULL,'user'),(17,18,'0000-00-00',NULL,NULL,0.00,0.00,0.00,4.50,4.50,306.00,NULL,NULL,'0',0.69,0.00,'2018-10-02',0.00,306.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-20 13:03:08',NULL,NULL,'user'),(18,19,'0000-00-00',NULL,NULL,25.00,25.00,25.00,5.00,112.41,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-20 15:38:40',NULL,NULL,'user'),(19,20,'0000-00-00',NULL,NULL,25.00,25.00,25.00,5.00,112.41,35.00,NULL,NULL,'0',0.69,4.99,'2018-10-04',11.24,43.67,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,'USD','USD',1.00,'0','admin',1,'2018-09-20 15:39:18',NULL,NULL,'user'),(20,21,'0000-00-00',NULL,NULL,20.00,10.00,5.00,100.00,100.00,80.00,0.00,0.00,'0',0.69,29.99,'2018-10-04',10.00,110.68,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',184,'USD','USD',1.00,'0','admin',1,'2018-09-20 16:48:37',NULL,NULL,'user'),(21,22,'0000-00-00',NULL,NULL,0.00,0.00,0.00,3.00,3.00,502.00,55.37,74.91,'',0.69,0.00,'0000-00-00',0.00,639.48,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',184,NULL,NULL,NULL,'0','admin',1,'2018-09-21 17:58:05',NULL,NULL,'user'),(22,24,'0000-00-00',NULL,NULL,5.00,10.00,15.00,20.00,20.00,200.00,0.00,0.00,'0',0.69,0.00,'2018-10-08',2.00,0.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-24 16:47:24',NULL,NULL,'user'),(23,24,'2018-09-25',NULL,NULL,25.00,25.00,25.00,20.00,112.41,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,'N',NULL,'N',NULL,'N',NULL,'N',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 14:46:53',NULL,NULL,'user'),(24,24,'2018-09-25',NULL,NULL,25.00,25.00,25.00,20.00,112.41,NULL,0.00,0.00,'0',NULL,0.00,NULL,NULL,NULL,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 15:03:01',NULL,NULL,'user'),(25,24,'2018-09-25',NULL,NULL,25.00,25.00,25.00,20.00,112.41,NULL,0.00,0.00,'0',NULL,0.00,NULL,NULL,NULL,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 15:03:26',NULL,NULL,'user'),(26,24,'2018-09-25',NULL,NULL,25.00,25.00,25.00,20.00,112.41,NULL,0.00,0.00,'0',NULL,0.00,NULL,NULL,NULL,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 15:03:37',NULL,NULL,'user'),(27,24,'2018-09-25',NULL,NULL,25.00,25.00,25.00,20.00,112.41,NULL,0.00,0.00,'0',NULL,0.00,NULL,NULL,NULL,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 15:03:58',NULL,NULL,'user'),(28,24,'2018-09-25',NULL,NULL,25.00,25.00,25.00,20.00,112.41,3100.00,0.00,0.00,'0',0.69,0.00,'2018-10-09',11.24,0.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 15:04:21',NULL,NULL,'user'),(29,24,'2018-09-25',NULL,NULL,25.00,25.00,25.00,20.00,112.41,3100.00,0.00,0.00,'0',0.69,0.00,'2018-10-09',11.24,0.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 15:04:47',NULL,NULL,'user'),(30,24,'2018-09-25',NULL,NULL,20.00,20.00,20.00,100.00,100.00,595.00,0.00,0.00,'0',0.69,0.00,'2018-10-09',10.00,0.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 15:09:29',NULL,NULL,'user'),(31,24,'2018-09-25',NULL,NULL,1.00,1.00,1.00,1.00,1.00,20.00,0.00,0.00,'0',0.69,0.00,'2018-10-09',0.10,0.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-25 15:16:28',NULL,NULL,'user'),(32,25,'0000-00-00',NULL,NULL,0.00,0.00,0.00,NULL,NULL,1103.00,65.97,145.03,'',0.69,0.00,'0000-00-00',0.00,333.53,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',184,NULL,NULL,NULL,'0','admin',1,'2018-09-26 16:24:16',NULL,NULL,'user'),(35,28,'0000-00-00',NULL,NULL,0.00,0.00,0.00,0.00,0.00,NULL,NULL,NULL,'0',0.69,0.00,'2018-09-15',0.00,0.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-26 17:41:50',NULL,NULL,'user'),(36,29,'0000-00-00',NULL,NULL,0.00,0.00,0.00,0.00,0.00,NULL,NULL,NULL,'0',0.69,0.00,'2018-09-15',0.00,0.69,NULL,'','N',NULL,'N',NULL,'N',NULL,'N',NULL,'Y',NULL,NULL,NULL,NULL,'0','admin',1,'2018-09-26 17:45:41',NULL,NULL,'user');
/*!40000 ALTER TABLE `stmd_shipment_deliveries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_labelPrintLog`
--

DROP TABLE IF EXISTS `stmd_shipment_labelPrintLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_labelPrintLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL,
  `labelType` varchar(100) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdByEmail` varchar(100) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_labelPrintLog`
--

LOCK TABLES `stmd_shipment_labelPrintLog` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_labelPrintLog` DISABLE KEYS */;
INSERT INTO `stmd_shipment_labelPrintLog` VALUES (1,22,'Shipping Label',1,'admin@stmd.com','2018-09-21 19:07:55'),(2,22,'Red Star Label',1,'admin@stmd.com','2018-09-21 19:10:40'),(3,21,'Red Star Label',1,'admin@stmd.com','2018-09-21 19:27:05');
/*!40000 ALTER TABLE `stmd_shipment_labelPrintLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_labels`
--

DROP TABLE IF EXISTS `stmd_shipment_labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dispatchDate` int(11) NOT NULL DEFAULT '0',
  `shipmentId` int(11) NOT NULL DEFAULT '0',
  `labelType` enum('standard','red_star','dhl','dhl_ci','nations_delivery') NOT NULL DEFAULT 'standard',
  PRIMARY KEY (`id`),
  KEY `stmd_shipment_labels_ibfk_1` (`shipmentId`),
  CONSTRAINT `stmd_shipment_labels_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_labels`
--

LOCK TABLES `stmd_shipment_labels` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_labels` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_shipment_labels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_other_charges`
--

DROP TABLE IF EXISTS `stmd_shipment_other_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_other_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL DEFAULT '0',
  `deliveryId` int(11) NOT NULL,
  `otherChargeId` int(11) DEFAULT '0',
  `otherChargeName` varchar(255) NOT NULL,
  `otherChargeAmount` decimal(12,2) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `createdBy` varchar(255) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  CONSTRAINT `stmd_shipment_other_charges_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_other_charges`
--

LOCK TABLES `stmd_shipment_other_charges` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_other_charges` DISABLE KEYS */;
INSERT INTO `stmd_shipment_other_charges` VALUES (1,1,1,0,'Delivery Snap Shot Charge',6.99,NULL,'somnath.pahari@indusnet.co.in','2018-09-14 14:13:38'),(2,1,1,0,'Delivery Recount Charge',6.99,NULL,'somnath.pahari@indusnet.co.in','2018-09-14 14:14:08'),(3,1,1,0,'Delivery Reweigh Charge',6.99,NULL,'somnath.pahari@indusnet.co.in','2018-09-14 14:14:18'),(4,2,2,0,'Delivery Snap Shot Charge',6.99,NULL,'somnath.pahari@indusnet.co.in','2018-09-14 14:14:43'),(5,20,19,58,'Liquid Packaging',4.99,'this needs lequid package','admin@stmd.com','2018-09-20 16:00:40'),(6,20,19,54,'Delivery Count (20 to 50 items)',2.99,'ttt','admin@stmd.com','2018-09-20 16:38:20'),(7,20,19,55,'Delivery Count (50 to 100 items)',4.99,'hhh','admin@stmd.com','2018-09-20 16:40:52'),(8,21,20,40,'TV Crating',29.99,'tv to pack','admin@stmd.com','2018-09-20 16:50:24'),(9,21,20,40,'TV Crating',29.99,'tv to pack','admin@stmd.com','2018-09-20 16:50:55');
/*!40000 ALTER TABLE `stmd_shipment_other_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_packagedetails`
--

DROP TABLE IF EXISTS `stmd_shipment_packagedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_packagedetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL DEFAULT '0',
  `message` text CHARACTER SET latin1 NOT NULL,
  `user` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  CONSTRAINT `stmd_shipment_packagedetails_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_packagedetails`
--

LOCK TABLES `stmd_shipment_packagedetails` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_packagedetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_shipment_packagedetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_packages`
--

DROP TABLE IF EXISTS `stmd_shipment_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL DEFAULT '0',
  `deliveryId` int(11) DEFAULT NULL,
  `storeId` int(11) NOT NULL,
  `siteCategoryId` int(11) DEFAULT NULL,
  `siteSubCategoryId` int(11) DEFAULT NULL,
  `siteProductId` int(11) DEFAULT NULL,
  `itemName` text,
  `websiteUrl` varchar(255) DEFAULT '',
  `weight` decimal(12,2) NOT NULL DEFAULT '0.00',
  `options` varchar(255) DEFAULT '',
  `itemDescription` text NOT NULL,
  `itemMake` varchar(100) NOT NULL,
  `itemModel` varchar(100) NOT NULL,
  `note` varchar(255) DEFAULT '',
  `deliveryCompanyId` int(11) NOT NULL,
  `deliveredOn` date DEFAULT NULL,
  `tracking` varchar(255) NOT NULL,
  `trackingLock` enum('0','1') NOT NULL DEFAULT '0',
  `tracking2` varchar(255) DEFAULT NULL,
  `deliveryNotes` text,
  `snapshotOpt` enum('Y','N') NOT NULL DEFAULT 'N',
  `snapshotImage` varchar(255) NOT NULL,
  `itemType` enum('N','H','S') NOT NULL DEFAULT 'N' COMMENT 'N=Normal,H=Hazmat,S=Secured',
  `itemQuantity` int(11) NOT NULL DEFAULT '0',
  `itemPrice` decimal(12,2) DEFAULT NULL,
  `itemShippingCost` decimal(12,2) DEFAULT NULL,
  `itemTotalCost` decimal(12,2) DEFAULT NULL,
  `type` enum('P','I') NOT NULL DEFAULT 'P' COMMENT 'P = Delivery Packages, I = Packhage Items',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedByType` enum('user','admin') NOT NULL DEFAULT 'user',
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  KEY `deliveryId` (`deliveryId`),
  KEY `categoryId` (`siteCategoryId`),
  KEY `subcategoryId` (`siteSubCategoryId`),
  KEY `productId` (`siteProductId`),
  CONSTRAINT `stmd_shipment_packages_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_shipment_packages_ibfk_2` FOREIGN KEY (`deliveryId`) REFERENCES `stmd_shipment_deliveries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_packages`
--

LOCK TABLES `stmd_shipment_packages` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_packages` DISABLE KEYS */;
INSERT INTO `stmd_shipment_packages` VALUES (1,1,1,0,3,39,23,'Apple iPad(6th Gen) Tablet (9.7 inch, 32GB, Wi-Fi)','https://www.amazon.in/Apple-iPad-Tablet-Wi-Fi-Space/dp/B07C4YKR3J/ref=sr_1_1?s=computers&ie=UTF8&qid=1536644836&sr=1-1&keywords=ipad',3.00,'Space Grey','','','','',0,NULL,'','0',NULL,NULL,'Y','','N',1,3661.49,100.00,3761.49,'I','0',NULL,NULL,'user'),(2,2,2,0,3,39,23,'Apple iPad(6th Gen) Tablet (9.7 inch, 32GB, Wi-Fi)','https://www.amazon.in/Apple-iPad-Tablet-Wi-Fi-Space/dp/B07C4YKR3J/ref=sr_1_1?s=computers&ie=UTF8&qid=1536644836&sr=1-1&keywords=ipad',3.00,'Space Grey','','','','',0,NULL,'','0',NULL,NULL,'Y','','N',1,3661.49,100.00,3761.49,'I','0',NULL,NULL,'user'),(3,5,3,0,3,39,23,'Fire 7 Tablet','https://www.amazon.com/dp/B01IO618J8/ref=fs_ods_tab_an',3.00,'Red','','','','',0,NULL,'','0',NULL,NULL,'N','','N',1,49.00,20.00,69.00,'I','0',NULL,NULL,'user'),(4,6,4,0,27,55,70,'Custom HeadLights','https://www.carid.com/auto-parts.html',200.00,NULL,'Custom HeadLights','BMW','Q7','',0,NULL,'','0',NULL,NULL,'N','','N',2,100.00,100.00,300.00,'I','0',NULL,NULL,'user'),(5,7,5,0,3,39,23,'Fire HD 10 Tablet','https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1',3.00,'Red','','','','',17,'2018-09-26','78978978','0',NULL,NULL,'N','','N',1,100.00,10.00,110.00,'I','0',NULL,NULL,'user'),(6,7,5,0,3,39,23,'Fire 7 Tablet','https://www.amazon.com/dp/B01IO618J8/ref=fs_ods_tab_an',3.00,'Red','','','','',10,'2018-09-30','78978979','0',NULL,NULL,'N','','N',2,49.00,20.00,118.00,'I','0',NULL,NULL,'user'),(7,8,6,0,3,NULL,NULL,'Fire HD 10 Tablet','https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1',0.00,'Red','','','','',17,'2018-09-13','4646464','0',NULL,NULL,'N','','N',1,149.00,10.00,159.00,'I','0',NULL,NULL,'user'),(8,8,7,0,3,NULL,NULL,'Fire 7 Tablet','https://www.amazon.com/Apple-iPad-WiFi-2018-Model/dp/B07D5S5LHF/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1',0.00,'Red','','','','',17,'2018-09-13','558788','0',NULL,NULL,'N','','N',1,49.00,10.00,59.00,'I','0',NULL,NULL,'user'),(9,9,8,0,27,53,67,'Car Tire','https://www.carid.com/tires/all-season/',109.00,NULL,'TERRAIN GRIPPER A/T G Tires by AMP®. Season: All Season. Type: Truck / SUV, All Terrain / Off Road / Mud. AMP Tires is proud to introduce the latest addition to its line of off-road light truck tires - the all–new terrain Gripper A/T more details on - https://www.carid.com/tires/all-season/','Audi','Q7','',0,NULL,'','0',NULL,NULL,'N','','N',4,115.00,0.00,460.00,'I','0',NULL,NULL,'user'),(10,10,9,0,3,39,23,'Fire HD 10 Tablet','https://www.amazon.com/All-New-Amazon-Fire-HD-10-Inch-Tablet-32GB-Blue/dp/B01M6YJEAH/ref=sr_1_1_sspa?ie=UTF8&qid=1536656484&sr=8-1-spons&keywords=tablets&psc=1',3.00,'RED','','','','',17,'2018-09-13','798798897','0',NULL,NULL,'N','','N',1,149.00,10.00,159.00,'I','0',NULL,NULL,'user'),(11,10,10,0,3,39,23,'Fire 7 Tablet','https://www.amazon.com/Apple-iPad-WiFi-2018-Model/dp/B07D5S5LHF/ref=sr_1_1_sspa?s=pc&ie=UTF8&qid=1536739566&sr=1-1-spons&keywords=ipad&psc=1',3.00,'Red','','','','',17,'2018-09-13','8787212121','0',NULL,NULL,'N','','N',1,200.00,10.00,210.00,'I','0',NULL,NULL,'user'),(12,11,11,0,27,53,67,'terrain-gripper','https://www.carid.com/amp-tires/terrain-gripper-at-g-84753193.html',109.00,NULL,'terrain-gripper','Make','Model','',0,NULL,'','0',NULL,NULL,'N','','N',1,50.00,5.00,55.00,'I','0',NULL,NULL,'user'),(13,12,12,0,NULL,NULL,NULL,'Echo Dot (2nd Generation)','https://www.amazon.com/dp/B01DFKC2SO/ref=ods_gw_ha_d_bt_propoff_090418?pf_rd_p=5f8214f2-ab41-4000-9a4e-298f4e650978&pf_rd_r=PN8RJ2RCDZ5JXF420N3V',0.00,'Black','','','','',17,'2018-09-30','546546','0',NULL,'Test','N','','N',2,49.99,10.00,109.98,'I','0',NULL,NULL,'user'),(14,16,15,1,NULL,NULL,NULL,'test1','',0.00,'','','','','',2,'2018-09-20','111111','0',NULL,'test1 note','N','','N',2,10.00,NULL,20.00,'I','0',NULL,NULL,'user'),(15,16,15,4,NULL,NULL,NULL,'test2','',0.00,'','','','','',2,'2018-09-07','7888778','0',NULL,'test2 note','N','','N',5,50.00,NULL,250.00,'I','0',NULL,NULL,'user'),(16,17,16,1,NULL,NULL,NULL,'test1','',0.00,'','','','','',2,'2018-09-20','111111','0',NULL,'test1 note','N','','N',2,10.00,NULL,20.00,'I','0',NULL,NULL,'user'),(17,17,16,4,NULL,NULL,NULL,'test2','',0.00,'','','','','',2,'2018-09-07','7888778','0',NULL,'test2 note','N','','N',5,50.00,NULL,250.00,'I','0',NULL,NULL,'user'),(18,18,17,0,3,39,23,'ipad','amazon.com/ipad',3.00,'red','','','','',17,'2018-09-18','11112222','1',NULL,'test','N','','N',3,50.00,1.00,151.00,'I','0',NULL,NULL,'user'),(19,18,17,0,3,40,16,'iphone 7plus','amazon.com/iphone',1.50,'grey','','','','',17,'2018-09-18','11112222','0',NULL,'tsting','N','','N',2,75.00,5.00,155.00,'I','0',NULL,NULL,'user'),(20,19,18,1,NULL,NULL,NULL,'bata shoe','',0.00,'','','','','',4,'2018-09-12','11112222','0',NULL,'shoe note','N','','N',5,3.00,NULL,15.00,'I','0',NULL,NULL,'user'),(21,19,18,4,NULL,NULL,NULL,'levis jeans','',0.00,'','','','','',2,'2018-09-06','11221122','0',NULL,'jeans note','N','','N',2,10.00,NULL,20.00,'I','0',NULL,NULL,'user'),(22,20,19,1,NULL,NULL,NULL,'bata shoe','',0.00,'','','','','',4,'2018-09-12','11112222','1',NULL,'shoe note','N','','N',5,3.00,NULL,15.00,'I','0',NULL,NULL,'user'),(23,20,19,4,NULL,NULL,NULL,'levis jeans','',0.00,'','','','','',2,'2018-09-06','1122112245','0',NULL,'jeans note','N','','N',2,10.00,NULL,20.00,'I','0',NULL,NULL,'user'),(24,21,20,1,NULL,NULL,NULL,'jeans','',0.00,'','','','','',2,'2018-09-20','999999','0',NULL,'test jeans','N','','N',6,5.00,NULL,30.00,'I','0',NULL,NULL,'user'),(25,21,20,1,NULL,NULL,NULL,'sunglass','',0.00,'','','','','',8,'2018-09-20','999999','0',NULL,'test sunglass','N','','N',5,10.00,NULL,50.00,'I','0',NULL,NULL,'user'),(26,22,21,0,3,39,23,'ipad','amazon.com/ipad',3.00,'silver','','','','',9,'2018-09-20','789654','0',NULL,'test note','N','','N',5,100.00,2.00,502.00,'I','0',NULL,NULL,'user'),(27,24,22,4,0,0,0,'Ipad','',0.00,'','','','','',4,'2018-09-28','11112222','0',NULL,'test delivery note','N','','N',2,50.00,NULL,100.00,'I','0',NULL,NULL,'user'),(28,24,22,0,0,0,0,'fish','',0.00,'','','','','',2,'2018-09-21','11112222','0',NULL,'test note today','N','','N',10,10.00,NULL,100.00,'I','1','2018-09-24 19:11:55',1,'admin'),(29,24,27,1,4,41,12,'rolex','',0.00,'','','','','',10,'2018-09-01','1122','0',NULL,'test','N','','N',2,1400.00,NULL,2800.00,'I','0',NULL,NULL,'user'),(30,24,27,4,4,41,19,'UCB suit','',0.00,'','','','','',4,'2018-09-01','8899','0',NULL,'test note','N','','N',3,100.00,NULL,300.00,'I','0',NULL,NULL,'user'),(31,24,28,1,4,41,12,'rolex','',0.00,'','','','','',10,'2018-09-01','1122','0',NULL,'test','N','','N',2,1400.00,NULL,2800.00,'I','0',NULL,NULL,'user'),(32,24,28,4,4,41,19,'UCB suit','',0.00,'','','','','',4,'2018-09-01','8899','0',NULL,'test note','N','','N',3,100.00,NULL,300.00,'I','0',NULL,NULL,'user'),(33,24,29,1,4,41,12,'rolex','',0.00,'','','','','',10,'2018-09-01','1122','0',NULL,'test','N','','N',2,1400.00,NULL,2800.00,'I','0',NULL,NULL,'user'),(34,24,29,4,4,41,19,'UCB suit','',0.00,'','','','','',4,'2018-09-01','8899','0',NULL,'test note','N','','N',3,100.00,NULL,300.00,'I','0',NULL,NULL,'user'),(35,24,30,1,5,44,22,'Canon','',0.00,'','','','','',17,'2018-09-01','112233','0',NULL,'canon note','N','','N',5,79.00,NULL,395.00,'I','0',NULL,NULL,'user'),(36,24,30,4,3,40,16,'redmi 6 pro','',0.00,'','','','','',17,'2018-09-01','112233','0',NULL,'6pro note','N','','N',1,200.00,NULL,200.00,'I','0',NULL,NULL,'user'),(37,24,31,0,0,0,0,'fish and chips','',0.00,'','','','','',2,'2018-09-01','7744','0',NULL,NULL,'N','','N',10,2.00,NULL,20.00,'I','0',NULL,NULL,'user'),(38,25,32,0,3,39,23,'ipad','amazon.com/ipad',3.00,'red','','','','',4,'2018-09-24','5566','0',NULL,'test delivery note 48','N','','N',5,10.00,3.00,53.00,'I','0',NULL,NULL,'user'),(39,25,32,0,4,41,10,'nike shoe','www.amazon.com/nike',2.50,'flurocent green','','','','',2,'2018-09-24','5566','0',NULL,'test delivery note 49','N','','N',2,520.00,10.00,1050.00,'I','0',NULL,NULL,'user');
/*!40000 ALTER TABLE `stmd_shipment_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_snapshots`
--

DROP TABLE IF EXISTS `stmd_shipment_snapshots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_snapshots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL,
  `deliveryId` int(11) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `imageName` varchar(255) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `addedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  CONSTRAINT `stmd_shipment_snapshots_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_snapshots`
--

LOCK TABLES `stmd_shipment_snapshots` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_snapshots` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_shipment_snapshots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_status_log`
--

DROP TABLE IF EXISTS `stmd_shipment_status_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_status_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL,
  `deliveryId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `oldStatus` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `updatedOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_status_log`
--

LOCK TABLES `stmd_shipment_status_log` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_status_log` DISABLE KEYS */;
INSERT INTO `stmd_shipment_status_log` VALUES (1,1,1,1,'','submitted','2018-09-11 06:01:04'),(2,2,2,2,'','submitted','2018-09-11 06:01:05'),(3,5,3,0,'none','in_warehouse','2018-09-11 09:25:20'),(4,6,4,0,'none','in_warehouse','2018-09-12 13:32:02'),(5,7,5,0,'none','in_warehouse','2018-09-13 06:13:03'),(6,8,6,0,'none','in_warehouse','2018-09-13 07:58:22'),(7,8,7,0,'none','in_warehouse','2018-09-13 07:58:38'),(8,9,8,0,'none','in_warehouse','2018-09-13 08:25:31'),(9,10,9,0,'none','in_warehouse','2018-09-13 09:04:10'),(10,10,10,0,'none','in_warehouse','2018-09-13 09:06:37'),(11,11,11,0,'none','in_warehouse','2018-09-13 09:15:37'),(12,12,12,0,'none','in_warehouse','2018-09-14 08:37:11'),(13,18,17,0,'none','in_warehouse','2018-09-20 13:03:08'),(14,20,19,0,'none','in_transit','2018-09-11 12:00:00'),(15,20,19,0,'in_transit','custom_clearing','2018-09-12 12:00:00'),(16,20,19,0,'custom_clearing','custom_clearing','2018-09-11 12:00:00'),(17,20,19,0,'custom_clearing','custom_clearing','2018-09-12 12:00:00'),(18,21,20,0,'none','in_warehouse','2018-09-20 16:48:37'),(19,22,21,26,'','submitted','2018-09-21 17:58:05'),(20,24,22,0,'none','in_warehouse','2018-09-24 16:47:24'),(21,24,29,0,'none','in_warehouse','2018-09-25 15:04:47'),(22,24,30,0,'none','in_warehouse','2018-09-25 15:09:29'),(23,24,31,0,'none','in_warehouse','2018-09-25 15:16:28'),(24,25,32,38,'','submitted','2018-09-26 16:24:16'),(25,25,32,39,'','submitted','2018-09-26 16:24:16');
/*!40000 ALTER TABLE `stmd_shipment_status_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_tracking`
--

DROP TABLE IF EXISTS `stmd_shipment_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updateTypeId` int(11) NOT NULL,
  `deliveryCompanyId` int(11) NOT NULL,
  `deliveredOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `message` varchar(255) DEFAULT NULL,
  `trackingLock` enum('0','1') NOT NULL DEFAULT '0',
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `updateTypeId` (`updateTypeId`),
  KEY `deliveryCompanyId` (`deliveryCompanyId`),
  CONSTRAINT `stmd_shipment_tracking_ibfk_1` FOREIGN KEY (`updateTypeId`) REFERENCES `stmd_updatetypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stmd_shipment_tracking_ibfk_2` FOREIGN KEY (`deliveryCompanyId`) REFERENCES `stmd_delivery_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_tracking`
--

LOCK TABLES `stmd_shipment_tracking` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_tracking` DISABLE KEYS */;
INSERT INTO `stmd_shipment_tracking` VALUES (1,1,7,'2018-06-20 00:00:00','TV Crating','Test','0','0',1,'2018-06-25 07:28:02',NULL,NULL),(2,5,4,'2018-06-20 00:00:00','New Shipment','Test','0','0',1,'2018-06-26 12:21:26',NULL,NULL),(3,4,10,'2018-06-30 00:00:00','Test Tracking',NULL,'0','0',1,'2018-06-26 12:24:28',NULL,NULL),(4,3,1,'2018-09-08 00:00:00','Sept Week 3 batch 1','Testing','0','0',1,'2018-09-18 13:25:24',NULL,NULL),(5,1,2,'2018-09-06 00:00:00','tracking sep 21','cust message','0','0',1,'2018-09-21 13:33:18',NULL,NULL),(6,1,2,'2018-09-06 00:00:00','tracking sep 21','cust message','1','0',1,'2018-09-21 13:38:46',NULL,NULL);
/*!40000 ALTER TABLE `stmd_shipment_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_tracking_consolidation`
--

DROP TABLE IF EXISTS `stmd_shipment_tracking_consolidation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_tracking_consolidation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `userEmail` varchar(255) NOT NULL,
  `deliveredOn` datetime NOT NULL,
  `createdBy` datetime NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_tracking_consolidation`
--

LOCK TABLES `stmd_shipment_tracking_consolidation` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_tracking_consolidation` DISABLE KEYS */;
INSERT INTO `stmd_shipment_tracking_consolidation` VALUES (1,'Tracking1','0','admin@stmd.com','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-06-26 07:24:47',NULL,NULL,NULL,NULL),(2,'Tracking2','0','admin@stmd.com','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-06-26 07:25:25',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_shipment_tracking_consolidation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_tracking_files`
--

DROP TABLE IF EXISTS `stmd_shipment_tracking_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_tracking_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trackingId` int(11) NOT NULL,
  `fileName` text NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trackingId` (`trackingId`),
  CONSTRAINT `stmd_shipment_tracking_files_ibfk_1` FOREIGN KEY (`trackingId`) REFERENCES `stmd_shipment_tracking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_tracking_files`
--

LOCK TABLES `stmd_shipment_tracking_files` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_tracking_files` DISABLE KEYS */;
INSERT INTO `stmd_shipment_tracking_files` VALUES (1,3,'1530015868_barcode.png',1,'2018-06-26 12:24:28'),(2,4,'1537277124_Combine and Save.jpg',1,'2018-09-18 13:25:24'),(3,6,'1537517326_createshipment.png',1,'2018-09-21 13:38:46');
/*!40000 ALTER TABLE `stmd_shipment_tracking_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_tracking_files_consolidation`
--

DROP TABLE IF EXISTS `stmd_shipment_tracking_files_consolidation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_tracking_files_consolidation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trackingId` int(11) NOT NULL,
  `fileName` text NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trackingId` (`trackingId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_tracking_files_consolidation`
--

LOCK TABLES `stmd_shipment_tracking_files_consolidation` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_tracking_files_consolidation` DISABLE KEYS */;
INSERT INTO `stmd_shipment_tracking_files_consolidation` VALUES (1,19,'1530192199_internet.png',1,'2018-06-28 18:53:19'),(3,20,'1530192252_internet.png',1,'2018-06-28 18:54:12'),(4,20,'1530192252_truck-4.jpg',1,'2018-06-28 18:54:12'),(5,20,'1530192252_DuraStar_LocalDelivery_M_2x_750x520.png',1,'2018-06-28 18:54:12');
/*!40000 ALTER TABLE `stmd_shipment_tracking_files_consolidation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_tracking_notes`
--

DROP TABLE IF EXISTS `stmd_shipment_tracking_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_tracking_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trackingId` int(11) NOT NULL DEFAULT '0',
  `notes` varchar(64) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trackingId` (`trackingId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_tracking_notes`
--

LOCK TABLES `stmd_shipment_tracking_notes` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_tracking_notes` DISABLE KEYS */;
INSERT INTO `stmd_shipment_tracking_notes` VALUES (1,1,'Test',1,'2018-06-25 07:28:02'),(2,4,'Testing',1,'2018-09-18 13:25:24'),(3,6,'test notes',1,'2018-09-21 13:38:46');
/*!40000 ALTER TABLE `stmd_shipment_tracking_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_tracking_notes_consolidation`
--

DROP TABLE IF EXISTS `stmd_shipment_tracking_notes_consolidation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_tracking_notes_consolidation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trackingId` int(11) NOT NULL DEFAULT '0',
  `notes` varchar(64) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trackingId` (`trackingId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_tracking_notes_consolidation`
--

LOCK TABLES `stmd_shipment_tracking_notes_consolidation` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_tracking_notes_consolidation` DISABLE KEYS */;
INSERT INTO `stmd_shipment_tracking_notes_consolidation` VALUES (1,2,'tyrtyrty',1,'2018-06-26 07:25:25');
/*!40000 ALTER TABLE `stmd_shipment_tracking_notes_consolidation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_tracking_numbers`
--

DROP TABLE IF EXISTS `stmd_shipment_tracking_numbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_tracking_numbers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trackingId` int(11) NOT NULL DEFAULT '0',
  `trackingNumber` varchar(64) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trackingId` (`trackingId`),
  CONSTRAINT `stmd_shipment_tracking_numbers_ibfk_1` FOREIGN KEY (`trackingId`) REFERENCES `stmd_shipment_tracking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_tracking_numbers`
--

LOCK TABLES `stmd_shipment_tracking_numbers` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_tracking_numbers` DISABLE KEYS */;
INSERT INTO `stmd_shipment_tracking_numbers` VALUES (1,1,'#003933',1,'2018-06-25 07:28:02'),(2,1,'#00499949',1,'2018-06-25 07:28:02'),(3,3,'#0999993',1,'2018-06-26 12:24:28'),(4,4,'12343, 343123, 65334',1,'2018-09-18 13:25:24'),(21,6,'1111',1,'2018-09-21 15:00:52'),(22,6,'2222',1,'2018-09-21 15:00:52'),(23,6,'3333',1,'2018-09-21 15:00:52');
/*!40000 ALTER TABLE `stmd_shipment_tracking_numbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_tracking_numbers_consolidation`
--

DROP TABLE IF EXISTS `stmd_shipment_tracking_numbers_consolidation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_tracking_numbers_consolidation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trackingId` int(11) NOT NULL DEFAULT '0',
  `trackingNumber` varchar(64) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trackingId` (`trackingId`),
  CONSTRAINT `stmd_shipment_tracking_numbers_consolidation_ibfk_1` FOREIGN KEY (`trackingId`) REFERENCES `stmd_shipment_tracking_consolidation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_tracking_numbers_consolidation`
--

LOCK TABLES `stmd_shipment_tracking_numbers_consolidation` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_tracking_numbers_consolidation` DISABLE KEYS */;
INSERT INTO `stmd_shipment_tracking_numbers_consolidation` VALUES (1,2,'78999956',1,'2018-06-26 07:25:25'),(2,2,'5554455',1,'2018-06-26 07:25:25'),(3,2,'4545455',1,'2018-06-26 07:25:25');
/*!40000 ALTER TABLE `stmd_shipment_tracking_numbers_consolidation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_warehouse_locations`
--

DROP TABLE IF EXISTS `stmd_shipment_warehouse_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_warehouse_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL,
  `warehouseRowId` int(11) NOT NULL,
  `warehouseZoneId` int(11) NOT NULL,
  `createdByType` enum('user','admin') NOT NULL DEFAULT 'user',
  `createdBy` int(11) NOT NULL,
  `createdOn` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  KEY `warehouseRowId` (`warehouseRowId`),
  KEY `warehouseZoneId` (`warehouseZoneId`),
  CONSTRAINT `stmd_shipment_warehouse_locations_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`),
  CONSTRAINT `stmd_shipment_warehouse_locations_ibfk_2` FOREIGN KEY (`warehouseRowId`) REFERENCES `stmd_warehouse_locations` (`id`),
  CONSTRAINT `stmd_shipment_warehouse_locations_ibfk_3` FOREIGN KEY (`warehouseZoneId`) REFERENCES `stmd_warehouse_locations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_warehouse_locations`
--

LOCK TABLES `stmd_shipment_warehouse_locations` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_warehouse_locations` DISABLE KEYS */;
INSERT INTO `stmd_shipment_warehouse_locations` VALUES (2,15,33,177,'admin',1,2018),(3,16,33,177,'admin',1,2018),(4,17,33,177,'admin',1,2018),(5,19,35,304,'admin',1,2018),(6,20,35,304,'admin',1,2018),(7,20,36,349,'user',1,2018),(9,21,34,225,'admin',1,2018),(10,23,31,79,'admin',1,2018),(11,24,31,78,'admin',1,2018);
/*!40000 ALTER TABLE `stmd_shipment_warehouse_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_warehouse_messages`
--

DROP TABLE IF EXISTS `stmd_shipment_warehouse_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_warehouse_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL,
  `messageId` int(11) NOT NULL,
  `sentBy` int(11) NOT NULL,
  `sentOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  KEY `messageId` (`messageId`),
  CONSTRAINT `stmd_shipment_warehouse_messages_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_warehouse_messages`
--

LOCK TABLES `stmd_shipment_warehouse_messages` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_warehouse_messages` DISABLE KEYS */;
INSERT INTO `stmd_shipment_warehouse_messages` VALUES (1,7,1,1,'2018-09-13 06:31:20'),(2,20,10,1,'2018-09-20 15:56:52');
/*!40000 ALTER TABLE `stmd_shipment_warehouse_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipment_warehouse_notes`
--

DROP TABLE IF EXISTS `stmd_shipment_warehouse_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipment_warehouse_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL,
  `message` text NOT NULL,
  `sentBy` int(11) NOT NULL,
  `sentOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  CONSTRAINT `stmd_shipment_warehouse_notes_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipment_warehouse_notes`
--

LOCK TABLES `stmd_shipment_warehouse_notes` WRITE;
/*!40000 ALTER TABLE `stmd_shipment_warehouse_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_shipment_warehouse_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipmentfiles`
--

DROP TABLE IF EXISTS `stmd_shipmentfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipmentfiles` (
  `fileid` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`fileid`),
  KEY `shipmentId` (`shipmentId`),
  CONSTRAINT `stmd_shipmentfiles_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipmentfiles`
--

LOCK TABLES `stmd_shipmentfiles` WRITE;
/*!40000 ALTER TABLE `stmd_shipmentfiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `stmd_shipmentfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipments`
--

DROP TABLE IF EXISTS `stmd_shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0',
  `warehouseId` int(11) NOT NULL,
  `shipmentType` enum('buycarforme','shopforme','autopart','othervehicle','othershipment') CHARACTER SET latin1 DEFAULT 'shopforme',
  `fromCountry` int(11) NOT NULL,
  `fromState` int(11) DEFAULT NULL,
  `fromCity` int(11) DEFAULT NULL,
  `fromName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `fromCompany` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `fromAddress` varchar(255) CHARACTER SET latin1 NOT NULL,
  `fromZipCode` varchar(255) NOT NULL,
  `fromPhone` varchar(32) CHARACTER SET latin1 NOT NULL,
  `fromEmail` varchar(128) CHARACTER SET latin1 NOT NULL,
  `toCountry` int(11) NOT NULL,
  `toState` int(11) DEFAULT NULL,
  `toCity` int(11) DEFAULT NULL,
  `toName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `toCompany` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `toAddress` varchar(255) CHARACTER SET latin1 NOT NULL,
  `toZipCode` varchar(255) NOT NULL,
  `toPhone` varchar(32) CHARACTER SET latin1 NOT NULL,
  `toEmail` varchar(128) CHARACTER SET latin1 NOT NULL,
  `shipmentStatus` enum('0','1','2','3','4','5','6') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT '0 = Shipment Submiited, 1= In Warehouse, 2= In Transit, 3= Customs clearing. 4=In destination warehouse, 5=Out for delivery, 6= Delivered',
  `firstReceived` datetime DEFAULT NULL,
  `hideFromCustomer` enum('Y','N') CHARACTER SET latin1 NOT NULL DEFAULT 'N',
  `totalItemCost` decimal(12,2) DEFAULT NULL,
  `totalWeight` decimal(12,2) DEFAULT NULL,
  `totalShippingCost` decimal(12,2) DEFAULT NULL,
  `totalClearingDuty` decimal(12,2) DEFAULT NULL,
  `isDutyCharged` enum('1','0') DEFAULT '1',
  `totalInsurance` decimal(12,2) DEFAULT NULL,
  `totalTax` decimal(12,2) DEFAULT NULL,
  `totalOtherCharges` decimal(12,2) DEFAULT NULL,
  `totalDiscount` decimal(12,2) DEFAULT NULL,
  `couponcodeApplied` varchar(255) DEFAULT NULL,
  `totalCost` decimal(12,2) DEFAULT NULL,
  `maxStorageDate` date DEFAULT NULL COMMENT 'Date after which storage charge will be applicable',
  `storageCharge` decimal(12,2) DEFAULT NULL,
  `isManualChargeEnabled` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'This is to idenfiy if manual storage charge entered or not',
  `storageChargeModifiedBy` varchar(255) DEFAULT NULL,
  `storageChargeModifiedOn` datetime DEFAULT NULL,
  `prepaid` enum('Y','N') CHARACTER SET latin1 NOT NULL DEFAULT 'N',
  `paymentStatus` enum('unpaid','paid') CHARACTER SET latin1 NOT NULL DEFAULT 'unpaid',
  `paymentMethodId` int(11) DEFAULT NULL,
  `paymentReceivedOn` datetime DEFAULT NULL,
  `shippingMethod` enum('Y','N') NOT NULL DEFAULT 'N',
  `shippingMethodId` int(11) DEFAULT NULL,
  `allowedPackaging` enum('Y','N') NOT NULL DEFAULT 'N',
  `wrongInventory` enum('Y','N') NOT NULL DEFAULT 'N',
  `deleted` enum('1','0') DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdByType` enum('user','admin') NOT NULL DEFAULT 'user',
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedByType` enum('user','admin') NOT NULL DEFAULT 'user',
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `warehouseId` (`warehouseId`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipments`
--

LOCK TABLES `stmd_shipments` WRITE;
/*!40000 ALTER TABLE `stmd_shipments` DISABLE KEYS */;
INSERT INTO `stmd_shipments` VALUES (1,3,1,'shopforme',230,120,256,'Mr. Somnath Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,2,639,'somnath pahari',NULL,'211 South Meadows Court','77479','713-530-1160','somnath.pahari@indusnet.co.in','0','2018-09-11 06:01:04','N',3761.49,NULL,55.37,400.53,'',NULL,0.00,NULL,NULL,NULL,4217.39,NULL,NULL,'N',NULL,NULL,'Y','paid',7,'2018-09-11 05:59:51','N',NULL,'N','N','0',1,'admin','2018-09-11 06:01:04',NULL,'user',NULL,NULL,NULL),(2,3,1,'shopforme',230,120,256,'Mr. Somnath Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,2,639,'somnath pahari',NULL,'211 South Meadows Court','77479','713-530-1160','somnath.pahari@indusnet.co.in','0','2018-09-11 06:01:05','N',3761.49,NULL,55.37,400.53,'',NULL,0.00,NULL,NULL,NULL,4217.39,NULL,NULL,'N',NULL,NULL,'Y','paid',7,'2018-09-11 05:59:51','N',NULL,'N','N','0',1,'admin','2018-09-11 06:01:05',NULL,'user',NULL,NULL,NULL),(3,3,1,'autopart',230,120,256,'Somnath Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','713-530-1160','somnath.pahari@indusnet.co.in',163,2,639,'somnath pahari',NULL,'211 South Meadows Court','77479','713-530-1160','somnath.pahari@indusnet.co.in','5','2018-09-11 07:39:57','N',464.00,NULL,0.00,523.18,'',NULL,NULL,NULL,NULL,NULL,1648.56,NULL,NULL,'N',NULL,NULL,'Y','paid',7,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-11 07:39:57',1,'user','2018-09-17 14:14:23',NULL,NULL),(4,3,1,'autopart',230,120,256,'Somnath Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','713-530-1160','somnath.pahari@indusnet.co.in',163,2,639,'somnath pahari',NULL,'211 South Meadows Court','77479','713-530-1160','somnath.pahari@indusnet.co.in','5','2018-09-11 07:40:04','N',464.00,NULL,0.00,523.18,'',NULL,NULL,NULL,NULL,NULL,1648.56,NULL,NULL,'N',NULL,NULL,'Y','paid',7,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-11 07:40:04',1,'user','2018-09-17 14:14:23',NULL,NULL),(5,7,1,'shopforme',230,120,256,'Mr. Nduka Udeh',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','n.udeh@aascargo.com',163,5,378,'Nduka Udeh',NULL,'12011 Westbrae Parkway','77031','7135301160','n.udeh@shoptomydoor.com','0','2018-09-11 09:25:20','N',69.00,NULL,0.00,NULL,'1',NULL,20.00,NULL,NULL,NULL,94.00,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-11 09:23:18','N',NULL,'N','N','0',1,'admin','2018-09-11 09:25:20',NULL,'user',NULL,NULL,NULL),(6,3,1,'autopart',230,120,256,'Somnath Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','713-530-1160','somnath.pahari@indusnet.co.in',163,15,638,'Paulami Sarkar',NULL,'Address 1','67676','6767','paulami.sarkar@indusnet.co.in','5','2018-09-12 13:32:02','N',300.00,NULL,100.00,0.00,'',NULL,NULL,NULL,NULL,NULL,516.20,NULL,NULL,'N',NULL,NULL,'Y','paid',8,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-12 13:32:02',1,'user','2018-09-17 14:14:23',NULL,NULL),(7,7,1,'shopforme',230,120,256,'Mr. Nduka Udeh',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','n.udeh@aascargo.com',163,5,378,'Nduka Udeh',NULL,'12011 Westbrae Parkway','77031','7135301160','n.udeh@shoptomydoor.com','0','2018-09-13 06:13:03','N',228.00,NULL,0.00,NULL,'1',NULL,20.00,NULL,NULL,NULL,248.00,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-13 06:11:15','N',NULL,'N','N','0',1,'admin','2018-09-13 06:13:03',NULL,'user',NULL,NULL,NULL),(8,13,1,'shopforme',230,120,256,'Mr. Joy Karmakar',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','9831723608','karmakar.joy@gmail.com',230,90,588,'Joy Karmakar',NULL,'SDF Building','852000','9831723608','karmakar.joy@gmail.com','0','2018-09-13 07:58:22','N',218.00,NULL,0.00,NULL,'1',NULL,0.00,NULL,NULL,NULL,218.00,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-13 07:56:30','N',NULL,'N','N','0',1,'admin','2018-09-13 07:58:22',NULL,'user',NULL,NULL,NULL),(9,13,1,'autopart',230,120,256,'Joy Karmakar',NULL,'12011 Westbrae Parkway #B','77031','9831723608','karmakar.joy@gmail.com',230,90,239,'Joy Karmakar',NULL,'SDF Building','852000','9831723608','karmakar.joy@gmail.com','5','2018-09-13 08:25:31','N',460.00,NULL,0.00,NULL,'1',NULL,NULL,NULL,NULL,NULL,480.00,NULL,NULL,'N',NULL,NULL,'N','paid',7,'2018-09-13 00:00:00','Y',2,'N','N','0',1,'admin','2018-09-13 08:25:31',1,'user','2018-09-17 14:14:23',NULL,NULL),(10,13,1,'shopforme',230,120,256,'Mr. Joy Karmakar',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','9831723608','karmakar.joy@gmail.com',230,90,588,'Joy Karmakar',NULL,'SDF Building','852000','9831723608','karmakar.joy@gmail.com','0','2018-09-13 09:04:10','N',369.00,NULL,0.00,NULL,'1',NULL,0.00,NULL,NULL,NULL,369.00,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-13 08:55:23','N',NULL,'N','N','0',1,'admin','2018-09-13 09:04:10',NULL,'user',NULL,NULL,NULL),(11,13,1,'autopart',230,120,256,'Joy Karmakar',NULL,'12011 Westbrae Parkway #B','77031','9831723608','karmakar.joy@gmail.com',230,90,588,'Joy Karmakar',NULL,'SDF Building','852000','9831723608','karmakar.joy@gmail.com','6','2018-09-13 09:15:37','N',55.00,NULL,5.00,NULL,'1',NULL,NULL,NULL,NULL,NULL,75.00,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-13 09:15:37',1,'user','2018-09-20 19:48:46',NULL,NULL),(12,3,1,'shopforme',230,120,256,'Mr. Somnath Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,2,639,'somnath pahari',NULL,'211 South Meadows Court','77479','713-530-1160','somnath.pahari@indusnet.co.in','0','2018-09-14 08:37:11','N',109.98,NULL,0.00,NULL,'1',NULL,20.00,NULL,NULL,NULL,129.98,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-11 07:09:18','N',NULL,'N','N','0',1,'admin','2018-09-14 08:37:11',NULL,'user',NULL,NULL,NULL),(15,3,1,'othershipment',230,120,256,'S Pahari',NULL,'12011 Westbrae Parkway #B','','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-19 20:09:35','N',NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',NULL,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-19 20:09:35',NULL,'user',NULL,NULL,NULL),(16,3,1,'othershipment',230,120,256,'S Pahari',NULL,'12011 Westbrae Parkway #B','','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-19 20:10:27','N',NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',NULL,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-19 20:10:27',NULL,'user',NULL,NULL,NULL),(17,3,1,'othershipment',230,120,256,'S Pahari',NULL,'12011 Westbrae Parkway #B','','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-19 20:10:56','N',NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',NULL,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-19 20:10:56',NULL,'user',NULL,NULL,NULL),(18,3,1,'shopforme',230,120,256,'Mr. S Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','700091','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-20 13:03:08','N',353.00,NULL,0.00,NULL,'1',NULL,0.00,NULL,NULL,NULL,353.00,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-20 13:01:50','N',NULL,'N','N','0',1,'admin','2018-09-20 13:03:08',NULL,'user',NULL,NULL,NULL),(19,3,3,'othershipment',48,154,348,'S Pahari',NULL,'American AirSea Cargo, C/O Mokes International Transportation','','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','4','2018-09-20 15:38:40','N',NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',NULL,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-20 15:38:40',1,'user','2018-09-20 18:59:33',NULL,NULL),(20,3,3,'othershipment',48,154,348,'S Pahari',NULL,'American AirSea Cargo, C/O Mokes International Transportation','','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','4','2018-09-20 10:09:19','N',NULL,NULL,NULL,NULL,'1',NULL,NULL,12.97,NULL,NULL,NULL,'2018-10-04',11.24,'N',NULL,NULL,'N','unpaid',NULL,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-20 15:39:18',1,'user','2018-09-20 18:59:33',NULL,NULL),(21,3,1,'othershipment',230,120,256,'S Pahari',NULL,'12011 Westbrae Parkway #B','','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','5','2018-09-20 11:18:37','N',NULL,NULL,NULL,NULL,'1',NULL,NULL,59.98,NULL,NULL,NULL,'2018-10-04',10.00,'N',NULL,NULL,'N','paid',5,NULL,'N',NULL,'Y','N','0',1,'admin','2018-09-20 16:48:37',1,'user','2018-09-20 18:58:58',NULL,NULL),(22,3,1,'shopforme',230,120,256,'Mr. S Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','700091','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-21 17:58:05','N',502.00,NULL,55.37,74.91,'',NULL,6.51,NULL,NULL,NULL,638.79,NULL,NULL,'N',NULL,NULL,'Y','paid',5,'2018-09-21 17:53:44','N',NULL,'Y','N','0',1,'admin','2018-09-21 17:58:05',NULL,'user',NULL,NULL,NULL),(23,3,1,'othershipment',230,120,256,'S Pahari',NULL,'12011 Westbrae Parkway #B','','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-24 16:39:09','N',NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',NULL,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-24 16:39:09',NULL,'user',NULL,NULL,NULL),(24,3,1,'othershipment',230,120,256,'S Pahari',NULL,'12011 Westbrae Parkway #B','','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-25 09:46:28','N',NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-09',0.10,'N',NULL,NULL,'N','paid',5,NULL,'N',NULL,'N','N','0',1,'admin','2018-09-24 16:47:24',NULL,'user',NULL,NULL,NULL),(25,3,1,'shopforme',230,120,256,'Mr. S Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','700091','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-26 16:24:16','N',1103.00,NULL,65.97,145.03,'',111.29,10.55,NULL,0.00,NULL,333.53,NULL,NULL,'N',NULL,NULL,'Y','paid',6,'2018-09-26 15:12:35','N',NULL,'N','N','0',1,'admin','2018-09-26 16:24:16',NULL,'user',NULL,NULL,NULL),(26,3,1,'shopforme',230,120,256,'Mr. S Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','700091','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-26 17:37:51','N',571.00,NULL,0.00,NULL,'1',NULL,0.00,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-26 17:36:48','N',NULL,'N','N','0',1,'admin','2018-09-26 17:37:51',NULL,'user',NULL,NULL,NULL),(27,3,1,'shopforme',230,120,256,'Mr. S Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','700091','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-26 17:39:30','N',571.00,NULL,0.00,NULL,'1',NULL,0.00,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-26 17:36:48','N',NULL,'N','N','0',1,'admin','2018-09-26 17:39:30',NULL,'user',NULL,NULL,NULL),(28,3,1,'shopforme',230,120,256,'Mr. S Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','700091','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-26 17:41:50','N',571.00,NULL,0.00,NULL,'1',NULL,0.00,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-26 17:36:48','N',NULL,'N','N','0',1,'admin','2018-09-26 17:41:50',NULL,'user',NULL,NULL,NULL),(29,3,1,'shopforme',230,120,256,'Mr. S Pahari',NULL,'12011 Westbrae Parkway #B','12011 Westbrae Parkway #B','123456','somnath.pahari@indusnet.co.in',163,4,53,'Tathagata sur',NULL,'10 A ROY PARA BYE LANE','700091','+91+90+92+9208961186006','tathagata.sur@indusnet.co.uk','0','2018-09-26 17:45:41','N',571.00,NULL,0.00,NULL,'1',NULL,0.00,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N','unpaid',0,'2018-09-26 17:36:48','N',NULL,'N','N','0',1,'admin','2018-09-26 17:45:41',NULL,'user',NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shipments_packaging`
--

DROP TABLE IF EXISTS `stmd_shipments_packaging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shipments_packaging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipmentId` int(11) NOT NULL,
  `length` decimal(12,2) NOT NULL,
  `width` decimal(12,2) NOT NULL,
  `height` decimal(12,2) NOT NULL,
  `weight` decimal(12,2) NOT NULL,
  `chargeableWeight` decimal(12,2) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shipmentId` (`shipmentId`),
  CONSTRAINT `stmd_shipments_packaging_ibfk_1` FOREIGN KEY (`shipmentId`) REFERENCES `stmd_shipments_26-9` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shipments_packaging`
--

LOCK TABLES `stmd_shipments_packaging` WRITE;
/*!40000 ALTER TABLE `stmd_shipments_packaging` DISABLE KEYS */;
INSERT INTO `stmd_shipments_packaging` VALUES (1,21,25.00,24.00,23.00,100.00,100.00,1,'2018-09-20 18:57:50'),(2,22,7.00,7.00,7.00,3.00,3.00,1,'2018-09-21 18:30:27');
/*!40000 ALTER TABLE `stmd_shipments_packaging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shippingcharges`
--

DROP TABLE IF EXISTS `stmd_shippingcharges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shippingcharges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shippingId` int(11) NOT NULL DEFAULT '0',
  `zoneId` int(11) NOT NULL DEFAULT '0',
  `pzoneId` int(11) NOT NULL,
  `provider` int(11) NOT NULL DEFAULT '0',
  `minWeight` decimal(12,2) NOT NULL DEFAULT '0.00',
  `maxWeight` decimal(12,2) NOT NULL DEFAULT '1000000.00',
  `flatRate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `shippingChargePerpound` decimal(12,2) NOT NULL DEFAULT '0.00',
  `clearingPortHandling` decimal(12,2) NOT NULL DEFAULT '0.00',
  `clearingPortHandlingPerpound` decimal(12,2) NOT NULL DEFAULT '0.00',
  `dispatchCost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `dispatchCostPerpound` decimal(12,2) NOT NULL DEFAULT '0.00',
  `insurancePercent` decimal(3,2) NOT NULL DEFAULT '0.00',
  `customDutyPercent` decimal(3,2) NOT NULL DEFAULT '0.00',
  `inventoryCharge` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `provider` (`provider`),
  KEY `shippingid` (`shippingId`),
  KEY `maxweight` (`maxWeight`),
  KEY `zoneid` (`zoneId`),
  KEY `source_zone_fk` (`pzoneId`),
  CONSTRAINT `destination_zone_fk` FOREIGN KEY (`zoneId`) REFERENCES `stmd_zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `source_zone_fk` FOREIGN KEY (`pzoneId`) REFERENCES `stmd_zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=702 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shippingcharges`
--

LOCK TABLES `stmd_shippingcharges` WRITE;
/*!40000 ALTER TABLE `stmd_shippingcharges` DISABLE KEYS */;
INSERT INTO `stmd_shippingcharges` VALUES (3,184,3,4,2,0.00,50.00,22.50,3.59,13.99,3.59,20.15,0.65,9.99,9.99,0.69),(6,184,1,4,2,0.00,50.00,22.50,3.99,13.99,3.99,15.39,1.05,9.99,9.99,0.95),(52,189,7,4,2,0.00,999999.99,39.00,1.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(137,192,3,4,2,0.00,1843.00,10.00,0.89,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(138,192,1,4,2,0.00,1843.00,15.00,1.19,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(139,192,2,4,2,0.00,1843.00,15.00,1.34,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(143,192,1,4,2,1844.00,3687.00,15.00,1.10,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(146,184,2,4,2,0.00,50.00,22.50,4.19,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(149,192,2,4,2,1844.00,3687.00,15.00,1.25,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(172,190,1,8,2,0.00,50.00,15.00,4.29,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(173,190,2,8,2,0.00,50.00,15.00,4.49,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(174,190,3,8,2,0.00,50.00,15.00,3.89,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(184,192,3,4,2,1844.00,3687.00,10.00,0.80,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(185,192,3,4,2,3688.00,14750.00,10.00,0.70,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(186,192,3,4,2,14751.00,29500.00,10.00,0.65,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(187,192,3,4,2,29501.00,999999.99,10.00,0.60,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(188,192,1,4,2,14751.00,29500.00,15.00,0.95,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(189,192,1,4,2,3688.00,14750.00,15.00,1.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(190,192,1,4,2,29501.00,999999.99,15.00,0.90,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(191,192,2,4,2,14751.00,29500.00,15.00,1.10,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(192,192,2,4,2,3688.00,14750.00,15.00,1.15,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(193,192,2,4,2,29501.00,999999.99,15.00,1.05,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(209,184,1,4,2,50.01,999999.99,15.00,3.99,13.99,3.99,15.39,1.05,9.99,9.99,0.95),(211,190,1,8,2,50.01,999999.99,15.00,4.29,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(214,184,2,4,2,50.01,999999.99,15.00,4.19,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(215,190,2,8,2,50.01,999999.99,15.00,4.49,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(217,184,3,4,2,50.01,999999.99,15.00,3.59,15.99,3.59,22.25,0.75,9.99,9.99,0.69),(219,190,3,8,2,50.01,999999.99,15.00,3.89,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(231,196,1,4,2,0.00,20.00,60.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(232,196,1,4,2,20.01,50.00,60.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(235,196,1,4,2,50.01,999999.99,100.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(236,196,2,4,2,0.00,20.00,65.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(239,196,2,4,2,20.01,50.00,65.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(240,196,2,4,2,50.01,999999.99,100.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(241,196,3,4,2,20.01,50.00,60.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(244,196,3,4,2,0.00,20.00,60.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(245,196,3,4,2,50.01,999999.99,80.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(367,199,1,4,2,0.00,10.00,80.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(368,199,1,4,2,10.01,20.00,85.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(369,199,1,4,2,20.01,50.00,100.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(370,199,1,4,2,50.01,100.00,120.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(371,199,1,4,2,100.01,150.00,180.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(372,199,1,4,2,150.01,999999.99,180.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(373,199,3,4,2,0.00,10.00,80.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(374,199,3,4,2,10.01,20.00,85.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(375,199,3,4,2,20.01,50.00,100.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(376,199,3,4,2,50.01,100.00,120.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(377,199,3,4,2,100.01,150.00,180.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(378,199,3,4,2,150.01,999999.99,180.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(379,199,2,4,2,0.00,10.00,80.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(380,199,2,4,2,10.01,20.00,85.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(381,199,2,4,2,20.01,50.00,100.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(382,199,2,4,2,50.01,100.00,120.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(383,199,2,4,2,100.01,150.00,180.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(384,199,2,4,2,150.01,999999.99,180.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(391,187,50,4,2,0.00,10.00,45.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(392,187,50,4,2,10.01,20.00,65.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(394,199,39,4,2,50.01,999999.99,70.00,3.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(395,199,39,4,2,20.01,50.00,70.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(396,199,39,4,2,0.00,20.00,50.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(397,199,50,4,2,50.00,999999.99,120.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(398,199,50,4,2,20.01,50.00,100.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(399,199,50,4,2,0.00,20.00,80.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(400,199,45,4,2,100.01,999999.99,45.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(401,199,45,4,2,20.01,100.00,55.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(402,199,45,4,2,10.01,20.00,55.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(404,199,45,4,2,0.00,10.00,55.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(405,199,43,4,2,10.01,20.00,55.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(406,199,43,4,2,0.00,10.00,55.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(407,199,43,4,2,100.01,999999.99,45.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(409,199,43,4,2,20.01,100.00,55.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(410,199,52,4,2,50.01,999999.99,80.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(411,199,52,4,2,0.00,50.00,60.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(412,199,47,4,2,0.00,999999.99,55.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(413,199,51,4,2,0.00,50.00,60.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(414,199,51,4,2,50.01,999999.99,60.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(415,199,41,4,2,10.01,999999.99,70.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(416,199,41,4,2,0.00,10.00,60.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(417,199,42,4,2,0.00,10.00,60.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(418,199,42,4,2,10.01,999999.99,70.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(419,199,44,4,2,0.00,20.00,55.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(420,199,44,4,2,20.01,999999.99,45.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(421,199,40,4,2,50.01,999999.99,70.00,3.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(422,199,40,4,2,20.01,50.00,70.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(423,199,40,4,2,0.00,20.00,50.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(425,199,48,4,2,0.00,999999.99,55.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(426,199,46,4,2,0.00,50.00,45.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(427,199,46,4,2,50.01,999999.99,45.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(428,199,49,4,2,100.01,999999.99,55.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(429,199,49,4,2,50.01,100.00,70.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(430,199,49,4,2,20.01,50.00,70.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(431,199,49,4,2,0.00,20.00,60.00,12.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(432,187,50,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(433,187,50,4,2,70.01,999999.99,140.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(434,187,50,4,2,50.01,70.00,110.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(435,187,45,4,2,50.01,999999.99,60.00,4.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(436,187,45,4,2,20.01,50.00,40.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(437,187,45,4,2,0.00,20.00,50.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(438,187,43,4,2,0.00,20.00,50.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(439,187,43,4,2,50.01,999999.99,60.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(440,187,43,4,2,20.01,50.00,60.00,4.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(441,187,52,4,2,50.01,999999.99,70.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(443,187,52,4,2,0.00,50.00,50.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(444,187,47,4,2,20.01,50.00,55.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(445,187,47,4,2,0.00,20.00,55.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(446,187,47,4,2,50.01,999999.99,75.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(447,187,39,4,2,70.01,999999.99,40.00,3.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(448,187,39,4,2,20.01,70.00,40.00,3.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(449,187,39,4,2,0.00,20.00,40.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(450,187,51,4,2,20.01,50.00,50.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(451,187,51,4,2,0.00,20.00,50.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(452,187,51,4,2,50.01,999999.99,100.00,4.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(453,187,41,4,2,50.01,999999.99,50.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(454,187,41,4,2,20.01,50.00,50.00,4.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(455,187,41,4,2,10.01,20.00,50.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(456,187,41,4,2,0.00,10.00,50.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(457,187,42,4,2,0.00,10.00,50.00,12.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(458,187,42,4,2,70.01,999999.99,60.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(459,187,42,4,2,50.01,70.00,90.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(460,187,42,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(461,187,42,4,2,10.01,20.00,60.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(462,187,44,4,2,10.01,50.00,50.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(463,187,44,4,2,0.00,10.00,50.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(464,187,44,4,2,70.01,999999.99,60.00,4.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(465,187,44,4,2,50.01,70.00,60.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(466,187,40,4,2,70.01,999999.99,40.00,3.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(467,187,40,4,2,20.01,70.00,40.00,3.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(468,187,40,4,2,0.00,20.00,40.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(469,187,48,4,2,0.00,20.00,50.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(470,187,48,4,2,50.00,999999.99,70.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(471,187,48,4,2,20.01,50.00,50.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(472,187,46,4,2,50.01,999999.99,40.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(473,187,46,4,2,10.01,50.00,40.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(474,187,46,4,2,0.00,10.00,40.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(475,187,49,4,2,0.00,10.00,55.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(476,187,49,4,2,70.01,999999.99,75.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(477,187,49,4,2,50.01,70.00,75.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(478,187,49,4,2,20.01,50.00,75.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(479,187,49,4,2,10.01,20.00,55.00,10.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(489,191,1,51,2,0.00,50.00,15.00,4.21,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(490,191,1,51,2,50.01,999999.99,15.00,4.21,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(491,191,2,51,2,0.00,50.00,15.00,4.41,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(492,191,2,51,2,50.01,999999.99,15.00,4.41,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(493,191,3,51,2,0.00,50.00,15.00,3.81,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(494,191,3,51,2,50.01,999999.99,15.00,3.81,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(495,187,1,4,2,50.01,70.00,110.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(496,187,1,4,2,70.01,999999.99,140.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(497,187,1,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(498,187,1,4,2,10.01,20.00,65.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(499,187,1,4,2,0.00,10.00,45.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(500,187,2,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(501,187,2,2,2,10.01,20.00,65.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(502,187,2,4,2,0.00,10.00,45.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(503,187,2,4,2,70.01,999999.99,140.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(504,187,2,4,2,50.01,70.00,110.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(505,187,3,4,2,70.01,999999.99,140.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(506,187,3,4,2,50.01,70.00,110.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(507,187,3,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(508,187,3,4,2,10.01,20.00,65.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(509,187,3,4,2,0.00,10.00,45.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(531,200,8,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(535,200,39,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(537,200,40,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(539,200,41,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(541,200,42,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(543,200,43,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(545,200,44,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(547,200,45,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(549,200,46,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(551,200,47,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(553,200,48,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(555,200,49,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(557,200,50,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(559,200,51,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(561,200,52,4,2,0.00,70.00,44.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(563,196,50,4,2,50.01,999999.99,66.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(564,196,50,4,2,20.01,50.00,56.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(565,196,50,4,2,0.00,20.00,56.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(566,196,39,4,2,20.01,50.00,55.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(567,196,39,4,2,50.01,999999.99,80.00,3.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(568,196,39,4,2,0.00,20.00,45.00,4.80,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(569,196,40,4,2,0.00,20.00,50.00,4.80,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(570,196,40,4,2,20.01,50.00,55.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(571,196,40,4,2,50.01,999999.99,80.00,3.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(572,196,41,4,2,50.01,999999.99,100.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(573,196,41,4,2,20.01,50.00,60.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(575,196,41,4,2,0.00,20.00,50.00,5.80,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(576,196,42,4,2,50.01,999999.99,100.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(577,196,42,4,2,20.01,50.00,65.00,8.30,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(578,196,42,4,2,0.00,20.00,60.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(579,196,44,4,2,50.01,999999.99,100.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(580,196,44,4,2,20.01,50.00,65.00,8.30,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(581,196,44,4,2,0.00,20.00,60.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(582,196,45,4,2,50.01,999999.99,100.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(583,196,45,4,2,20.01,50.00,50.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(584,196,45,4,2,0.00,20.00,50.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(585,196,43,4,2,50.01,999999.99,100.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(586,196,43,4,2,0.00,20.00,50.00,6.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(587,196,43,4,2,20.01,50.00,60.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(588,196,52,4,2,20.01,50.00,70.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(589,196,52,4,2,0.00,20.00,60.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(590,196,52,4,2,50.01,999999.99,110.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(591,196,46,4,2,20.01,50.00,55.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(592,196,46,4,2,0.00,20.00,50.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(593,196,46,46,2,50.01,999999.99,100.00,4.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(594,196,49,4,2,0.00,20.00,50.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(596,196,49,4,2,20.01,50.00,60.00,7.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(597,196,49,4,2,50.01,999999.99,110.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(598,196,48,4,2,0.00,20.00,55.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(599,196,48,4,2,20.01,50.00,55.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(600,196,48,4,2,50.01,999999.99,80.00,5.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(613,196,54,4,2,0.00,20.00,56.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(614,196,54,4,2,20.01,50.00,56.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(615,196,54,4,2,50.01,999999.99,66.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(616,187,54,4,2,0.00,10.00,45.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(617,187,54,4,2,10.01,20.00,65.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(618,187,54,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(619,187,54,4,2,50.01,70.00,110.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(620,187,54,4,2,70.01,999999.99,140.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(623,196,55,4,2,0.00,20.00,56.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(624,196,55,4,2,20.01,50.00,56.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(625,196,55,4,2,50.01,999999.99,66.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(626,187,55,4,2,0.00,10.00,45.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(627,187,55,4,2,10.01,20.00,65.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(628,187,55,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(629,187,55,4,2,50.01,70.00,110.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(630,187,55,4,2,70.01,999999.99,140.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(633,196,56,4,2,0.00,20.00,56.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(634,196,56,4,2,20.01,50.00,56.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(635,196,56,4,2,50.01,999999.99,66.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(636,187,56,4,2,0.00,10.00,45.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(637,187,56,4,2,10.01,20.00,65.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(638,187,56,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(639,187,56,4,2,50.01,70.00,110.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(640,187,56,4,2,70.01,999999.99,140.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(643,196,57,4,2,0.00,20.00,56.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(644,196,57,4,2,20.01,50.00,56.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(645,196,57,4,2,50.01,999999.99,56.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(646,187,57,4,2,0.00,10.00,45.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(647,187,57,4,2,10.01,20.00,65.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(648,187,57,4,2,20.01,50.00,70.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(649,187,57,4,2,50.01,70.00,110.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(650,187,57,4,2,70.00,999999.99,140.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(653,196,58,4,2,0.00,20.00,56.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(654,196,58,4,2,20.01,50.00,56.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(655,196,58,4,2,50.01,999999.99,56.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(656,187,58,4,2,0.00,10.00,45.00,11.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(657,187,58,4,2,10.01,20.00,65.00,9.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(658,187,58,4,2,20.01,50.00,70.00,9.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(659,187,58,4,2,50.01,70.00,110.00,8.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(660,187,58,4,2,70.01,999999.99,140.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(661,196,60,4,2,0.00,20.00,56.00,8.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(662,196,60,4,2,20.01,50.00,56.00,7.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(663,196,60,4,2,50.01,999999.99,66.00,6.50,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(676,201,60,4,2,0.00,11.00,77.25,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(677,201,60,4,2,11.01,22.00,82.25,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(678,201,60,4,2,22.01,33.00,88.50,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(679,201,60,4,2,440.01,660.00,268.44,3.76,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(680,201,60,4,2,33.01,44.00,94.75,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(681,201,60,4,2,44.01,55.00,169.38,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(682,201,60,4,2,55.01,110.00,169.38,3.76,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(683,201,60,4,2,110.01,440.00,230.93,3.76,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(684,201,60,4,2,660.01,880.00,343.44,3.76,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(685,201,60,4,2,880.01,999999.99,418.44,3.26,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(686,201,61,4,2,11.01,22.00,56.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(687,201,61,4,2,22.01,33.00,56.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(688,201,61,4,2,33.01,44.00,56.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(689,201,61,4,2,44.01,55.00,124.38,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(690,201,61,4,2,55.01,110.00,134.38,3.76,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(691,201,61,4,2,110.01,440.00,185.94,3.76,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(692,201,61,4,2,440.01,660.00,223.44,3.76,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(693,201,61,4,2,660.01,880.00,298.44,3.76,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(694,201,61,4,2,0.00,11.00,56.00,4.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(695,201,61,4,2,880.01,999999.99,373.44,3.26,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(696,184,1,4,2,999999.99,1999999.99,500.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(697,123,1,4,2,0.00,999999.99,250.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(698,196,43,44,2,0.00,999999.99,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(699,184,1,51,2,5.00,999999.99,5.00,5.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),(700,202,1,4,0,0.00,150.00,20.99,2.99,10.39,1.99,10.99,1.19,5.00,9.99,0.69),(701,202,1,4,0,151.00,250.00,59.00,2.00,19.00,2.00,15.00,1.00,9.99,9.99,1.59);
/*!40000 ALTER TABLE `stmd_shippingcharges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_shippingmethods`
--

DROP TABLE IF EXISTS `stmd_shippingmethods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_shippingmethods` (
  `shippingid` int(11) NOT NULL AUTO_INCREMENT,
  `shipping` varchar(255) NOT NULL DEFAULT '',
  `shipping_time` varchar(128) NOT NULL DEFAULT '',
  `destination` char(1) NOT NULL DEFAULT 'I',
  `code` varchar(32) NOT NULL DEFAULT '',
  `subcode` varchar(32) NOT NULL DEFAULT '',
  `orderby` int(11) NOT NULL DEFAULT '0',
  `active` char(1) NOT NULL DEFAULT 'Y',
  `intershipper_code` varchar(32) NOT NULL DEFAULT '',
  `weight_min` decimal(12,2) NOT NULL DEFAULT '0.00',
  `weight_limit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `service_code` int(11) NOT NULL DEFAULT '0',
  `is_cod` char(1) NOT NULL DEFAULT '',
  `is_new` char(1) NOT NULL DEFAULT '',
  `amazon_service` varchar(32) NOT NULL DEFAULT 'Standard',
  `gc_shipping` varchar(30) NOT NULL DEFAULT '',
  `sea` varchar(1) NOT NULL DEFAULT '',
  `learn_more` text,
  `is_duty` char(1) NOT NULL DEFAULT '',
  `days` int(11) NOT NULL DEFAULT '0',
  `companyLogo` varchar(255) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shippingid`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_shippingmethods`
--

LOCK TABLES `stmd_shippingmethods` WRITE;
/*!40000 ALTER TABLE `stmd_shippingmethods` DISABLE KEYS */;
INSERT INTO `stmd_shippingmethods` VALUES (184,'Standard Shipping','6 - 11 Business Days','I','','',0,'Y','',0.00,50000.00,0,'N','','Standard','','','Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door. Shipment departs every Thursday from the US and delivery time counts from the Thursday when shipments departs. Payment must be made by 12 noon on Wednesday for the shipment to meet delivery for that week.','Y',16,'logoSm.jpg','0','2018-07-09 07:03:01',0,0,'2018-05-16 12:59:34','2018-05-16 12:59:34',0),(185,'Fedex Priority','3 - 5 Business Days','I','','',7,'N','',0.00,50000.00,0,'N','','Standard','','','Delivery, clearing and documentation is handled by Fedex. With this option, once items are shipped out, we provide you with a Fedex tracking number. You are to co-ordinate directly with Fedex on all issues of duties and other charges if applicable, and are to pay them directly if required.','',7,'fedex.jpg','0','2018-07-09 07:03:01',0,0,'2018-05-16 12:59:34','2018-05-16 12:59:34',0),(187,'Fedex Economy','4 - 7 Business Days','I','','',9,'N','',0.00,50000.00,0,'N','','Standard','','','Delivery, clearing and documentation is handled by Fedex. With this option, once items are shipped out, we provide you with a Fedex tracking number. You are to co-ordinate directly with Fedex on all issues of duties and other charges if applicable, and are to pay them directly if required.','',9,'fedex.jpg','0','2018-07-09 07:03:01',0,0,'2018-05-16 12:59:34','2018-05-16 12:59:34',0),(189,'Fedex - USA','2 - 5 Business Days','L','','',6,'Y','',0.00,500000.00,0,'N','','Standard','','','Delivery, clearing and documentation is handled by Fedex. With this option, once items are shipped out, we provide you with a Fedex tracking number. You are to co-ordinate directly with Fedex on all issues of duties and other charges if applicable, and are to pay them directly if required.','',5,'fedex.jpg','0','2018-07-09 07:03:01',0,0,'2018-05-16 12:59:34','2018-05-16 12:59:34',0),(190,'Standard Shipping (UK)','8 - 13 Business Days','I','','',1,'Y','',0.00,50000.00,0,'N','','Standard','','','Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door. Shipment departs every forthnight from the UK and delivery time counts from the date of departure.','Y',18,'logoSm.jpg','0','2018-07-09 07:03:01',0,0,'2018-05-16 12:59:34','2018-05-16 12:59:34',0),(191,'Standard Shipping (China)','8 - 15 Business Days','I','','',3,'Y','',0.00,50000.00,0,'N','','Standard','','','Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door.','Y',20,'logoSm.jpg','0','2018-07-09 07:03:01',0,0,'2018-05-16 12:59:34','2018-05-16 12:59:34',0),(192,'Standard Shipping (Sea)','6 - 30 Business Days','I','','',4,'Y','',0.00,50000.00,0,'N','','Standard','','Y','Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door.','Y',70,'logoSm.jpg','0','2018-07-09 07:03:52',0,0,'2018-05-16 12:59:34','2018-05-16 12:59:34',0),(195,'Fedex - UK','1 - 3 Business Days','l','','',8,'Y','',0.00,150.00,0,'N','','Standard','','Y','Delivery, clearing and documentation is handled by Fedex. With this option, once items are shipped out, we provide you with a Fedex tracking number. You are to co-ordinate directly with Fedex on all issues of duties and other charges if applicable, and are to pay them directly if required.','Y',5,'fedex.jpg','0','2018-07-09 07:04:01',0,1,'2018-05-17 10:40:58','2018-05-16 12:59:34',0),(196,'Express Shipping (DHL)','1 - 4 Business Days','l','','',5,'Y','',1000.00,12500.00,0,'Y','','Standard','','Y','Delivery, clearing and documentation is handled by DHL directly, and by using this option, you authorize them to handle your shipment. DHL will contact you directly if needed and you will relate directly with them.','N',6,'dhl.jpg','0','2018-07-09 07:04:16',0,1,'2018-05-17 10:42:12','2018-05-16 12:59:34',0),(199,'Express Shipping (UPS)','1 - 4 Business Days','I','','',10,'Y','',0.00,50000.00,0,'N','','Standard','','Y','Delivery, clearing and documentation is handled by UPS directly, and by using this option, you authorize them to handle your shipment. UPS will contact you directly if needed and you will relate directly with them.','Y',8,'dhl.jpg','0','2018-07-09 07:04:33',0,1,'2018-05-17 10:41:10','2018-05-16 12:59:34',0),(200,'Expedited Shipping','9 - 14 Business Days','I','','',11,'Y','',0.00,70.00,0,'N','','Standard','','Y','Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door. Shipment departs every Thursday from the US and delivery time counts from the Thursday when shipments departs. Payment must be made by 12 noon on Wednesday for the shipment to meet delivery for that week.','Y',11,'dhl.jpg','0','2018-07-09 07:08:58',0,1,'2018-07-09 07:08:57','2018-05-16 12:59:34',0),(201,'Standard Shipping - GH','6 - 11 Business Days','I','','',2,'Y','',0.00,50000.00,0,'N','','Standard','','','Payment here covers shipping to your door, clearing as well as airline handling charges but excludes customs duty to Govt. Our partners Modern World Logistics (+233302783216 - www.modernworldlogistics.com.com) will process duty charges and will pass them to you on arrival of shipment at Ghana airport to pay direct to the Govt.','',16,'logoSm.jpg','0','2018-07-09 07:03:01',0,0,'2018-05-16 12:59:34','2018-05-16 12:59:34',0),(202,'Test','1 - 6 Business Days','l','','',0,'Y','',0.00,50.00,0,'N','','Standard','','Y','dsfsfsdfsdf','Y',0,'relnc.png','1','2018-07-09 07:04:55',1,1,'2018-05-22 09:11:34','2018-05-22 09:10:57',1),(203,'Demo','6-11 Business Days','I','','',0,'Y','',12.66,33.00,0,'N','','Standard','','Y',NULL,'Y',0,'logo.png','1','2018-07-09 07:03:01',1,0,'2018-06-18 08:15:05','2018-06-18 08:15:05',1),(204,'Test method','2 - 16 Business Days','I','','',0,'Y','',0.00,50.99,0,'N','','Standard','','N','sas','N',0,'202490609-Screen Shot 2014-06-04 at 09.55.38.png','1','2018-09-12 14:17:33',1,1,'2018-07-09 06:59:54','2018-07-09 06:59:12',1),(205,'New test shipping','10 - 60 Business Days','l','','',0,'Y','',10.00,50.00,0,'N','','Standard','','Y',NULL,'N',0,'colorful-shopping-sale-banner-template_1201-1308.jpg','1','2018-07-13 06:18:11',1,1,'2018-07-13 06:17:59','2018-07-13 06:14:46',1);
/*!40000 ALTER TABLE `stmd_shippingmethods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_sitecategories`
--

DROP TABLE IF EXISTS `stmd_sitecategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_sitecategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentCategoryId` int(11) NOT NULL DEFAULT '-1',
  `categoryName` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `type` enum('shopforme','autoparts') NOT NULL DEFAULT 'shopforme',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_sitecategories`
--

LOCK TABLES `stmd_sitecategories` WRITE;
/*!40000 ALTER TABLE `stmd_sitecategories` DISABLE KEYS */;
INSERT INTO `stmd_sitecategories` VALUES (3,-1,'Tablets & Phones','1','shopforme'),(4,-1,'Fashion','1','shopforme'),(5,-1,'Electronics','1','shopforme'),(6,-1,'TV','1','shopforme'),(7,-1,'Books, CD & DVD\'s','1','shopforme'),(23,-1,'Children - Wears','1','shopforme'),(27,-1,'Auto Parts','1','autoparts'),(28,-1,'Sports','1','shopforme'),(29,-1,'Baby','1','shopforme'),(30,-1,'Beauty and Health','1','shopforme'),(36,-1,'Computers','1','shopforme'),(37,36,'Laptops','1','shopforme'),(38,27,'Parts','1','shopforme'),(39,3,'Tablets','1','shopforme'),(40,3,'Phones','1','shopforme'),(41,4,'Men\'s','1','shopforme'),(42,4,'Women\'s','1','shopforme'),(43,5,'Camera','1','shopforme'),(44,5,'Video Game Consoles','1','shopforme'),(45,7,'CD\'s and DVD\'s','1','shopforme'),(46,7,'Books','1','shopforme'),(47,7,'Magazines','1','shopforme'),(48,6,'TV','1','shopforme'),(49,36,'Apple Laptops','1','shopforme'),(50,36,'Desktops','1','shopforme'),(51,23,'Boys','1','shopforme'),(52,23,'Girls','1','shopforme'),(53,27,'Tires','1','shopforme'),(54,27,'Electronics','1','shopforme'),(55,27,'Body Parts','1','shopforme'),(56,27,'Engine and Internal','1','shopforme'),(57,28,'Football','1','shopforme'),(58,28,'Basketball','1','shopforme'),(59,29,'Diapers','1','shopforme'),(60,29,'Crib','1','shopforme'),(61,29,'Car Seat','1','shopforme'),(62,29,'Food','1','shopforme'),(63,30,'Perfumes','1','shopforme'),(64,30,'Body cream','1','shopforme'),(65,30,'Hair and Shampoo','1','shopforme'),(66,30,'Facials','1','shopforme'),(77,-1,'E-commerce itemss','1','shopforme'),(78,27,'E-commerce itemss','1','shopforme'),(79,-1,'Household items','1','shopforme'),(80,4,'Shoes','1','shopforme');
/*!40000 ALTER TABLE `stmd_sitecategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_sitepages`
--

DROP TABLE IF EXISTS `stmd_sitepages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_sitepages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageType` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1 for static page and 2 for landing page',
  `slug` varchar(255) NOT NULL,
  `pageHeader` varchar(255) NOT NULL,
  `pageBanner` varchar(255) DEFAULT NULL,
  `pageContent` longtext NOT NULL,
  `pageTitle` varchar(255) DEFAULT NULL,
  `metaKeyword` longtext,
  `metaDescription` longtext,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `displayOrder` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_sitepages`
--

LOCK TABLES `stmd_sitepages` WRITE;
/*!40000 ALTER TABLE `stmd_sitepages` DISABLE KEYS */;
INSERT INTO `stmd_sitepages` VALUES (1,'1','about-us','About Us',NULL,'<p><img class=\"ui-widget-content\" src=\"http://localhost/stmd/public/uploads/media/original/1532524395_about us potrature.jpg\" width=\"100%\" /></p>\r\n\r\n<h3>Who We Are</h3>\r\n\r\n<p>Shoptomydoor is a company that allows you to shop from over 80,000 online stores in USA, UK and China using smart consolidation service. We take the stress of shopping and shipping off you and deliver to your doorstep anywhere you are. Beginning in 2009 we have served well over 200,000 satisfied customers and delivered over 9 million items stress free from USA, UK and China to their doorstep. We have become popular with our exceptional smart consolidation among people who desire quality at the right price.</p>\r\n\r\n<h3>Our Core Value</h3>\r\n\r\n<p><strong>Stress-free delivery with excellence in every detail to ensure maximum customer satisfaction</strong></p>\r\n\r\n<h3>Our Vision</h3>\r\n\r\n<ul class=\\\"list-ui\\\">\r\n	<li>To create a world where goods (raw materials and finished products) move quickly across borders.</li>\r\n	<li>A world where information technology and transportation networks shrink time and distance, creating competitive advantages to all our customers.</li>\r\n</ul>\r\n\r\n<h3>Our History</h3>\r\n\r\n<p><img alt=\"\" src=\"https://www.shoptomydoor.com/Site_Pictures/EProcure/history1.jpg\" width=\"100%\" /></p>','Shipping to Nigeria | What is Shoptomydoor? | Find out More about Us!','shipping from USA and UK to Nigeria, Shop &amp; ship from USA to Nigeria, shipping companies in Nigeria, shipping to Nigeria from US, cargo to Nigeria, shipping from Nigeria to us, procurement from US, Shipping cost from Nigeria to USA,  online stores that ship to Nigeria, door to door shipping from USA to Nigeria, US stores that ship to Nigeria, shipping companies in Nigeria, Air shipping to Nigeria, ship to Nigeria','Shoptomydoor is Texas and Atlanta based shipping company that ships to anywhere in Nigeria! &amp;#8730; Know more about us &amp; start shipping for lesser! No hidden charges &amp; 100% money back guarantee!','1',2),(8,'1','privacy-policy','Privacy Policy',NULL,'<h2>Privacy Policy</h2>\r\n\r\n<p>Welcome to shoptomydoor privacy policy which describes use, disclosure, collection, retention, and protection of your personal information. We are pleased about your interest in our service, hence it is important to protect your personal information. By using our service, you are accepting the terms of this Privacy Policy regardless of how it is accessed or used, including through mobile devices.</p>\r\n\r\n<h3>Personal Information</h3>\r\n\r\n<p>When you visit and use our web site we may request your contact information, including but not limited to your first and last name, telephone number, email address, home address. The information we collect is necessary to complete, support and analyze your shipping request from this site and for internal purposes and to comply with any requirements of law. This information may be disclosed to our staff and only to third parties involved in the completion of your transaction and the eventual delivery of your shipment. We will use your email to respond to your questions and to provide you with information about specials offerings and discounts from aascargo.com. Without this information we will be unable to complete your shipping. The information that we collect from you helps us personalize and continually improve upon your shipping experience, as we strive to make it easier and more enjoyable on your next visit. You have the option not to provide certain information; however, withholding information may prevent you from being able to take full advantage of some of our web site features.</p>\r\n\r\n<h3>Purchase Information</h3>\r\n\r\n<p>Any information we collect during the use of this site is disclosed only to our staff, and any third party involved in the completion of your transaction if necessary. Information we collect includes but is not limited to your payment method. We use this information to process your order and analyze and support your use of the aascargo.com site.</p>\r\n\r\n<h3>Credit/ATM Card Security</h3>\r\n\r\n<p>The security of your personal information is extremely important to us. We strive to always protect your online transactions, and use a technology called Secure Sockets Layer (SSL). We recommend you use a browser that supports SSL, so that, your personal information will be automatically encrypted, or encoded, before it is sent over the Internet. SSL works with Netscape, Microsoft Internet Explorer, AOL, and Apple&#39;s Safari&#39;s browser. This ensures that only Aascargo.com can read your personal information. SSL technology represents the highest level of security available on the Internet.</p>\r\n\r\n<p>We have taken security a step further by using Comodos Extended Validation (EV) SSL technology. This is the most trusted form of SSL. Most newer browsers made for EV SSL, including IE 7 and above, Firefox 3, Opera 9.5. will automatically tell you if a site is secured by the presence of a green bar near the address. EV SSL is the most difficult form of SSL to purchase as it requires total verification of the company and their operations to ensure it is issued to a legitimate company. It automatically encrypts information traveling over the Internet, verifies the identity of the transacting servers through certificates and digital signatures, and confirms that the integrity of message content is maintained throughout transmission. All user information we collect on this site is collected through secure web pages, and some data is in encrypted form. We follow reasonable technical and management practices to help protect the confidentiality, security and integrity of data stored on our system. While no computer system is completely secure, we believe the measures implemented by our web site reduces the likelihood of security problems to a level appropriate to the type of data involved. Our EV logo, basically says it all.</p>\r\n\r\n<p>To further protect your information, we recommend you do not allow anyone else to know your password. Please keep it strictly confidential. No representative of Aascargo.com will ever ask you for your password. The confidentiality of your password is yours to protect.</p>\r\n\r\n<h3>Use of Cookies</h3>\r\n\r\n<p>We may use cookies and other Internet technologies to manage our web sites, e-mail programs and to offer you personalized experiences. Cookies are small files placed on your hard drive that helps us to provide products and services to you. Cookies are an industry-standard technology, which make the use of our online shopping cart possible, among other features, and our use of cookies does not infringe upon your privacy. Your browser can always be set to decline cookies should you desire.</p>\r\n\r\n<h3>How long do we keep user information?</h3>\r\n\r\n<p>We generally keep user data on our server or in our archives for as long as we need it. We may alter this practice according to changing requirements. For example, we may delete some data if needed to free up storage space. We may keep other data for longer periods if the law requires it. In addition, information posted in a public forum could stay in the public domain indefinitely. Data management requests are administered in an orderly manner to the extent feasible and within our direct control. Note: we have greater control over recently collected data than for archived data. Once data is removed from the system and archived, it may not be feasible to accommodate specific requests. In those cases, our general data retention policy applies.</p>\r\n\r\n<h3>When we may disclose your information</h3>\r\n\r\n<p>We may disclose your information if necessary to protect our legal rights, if the information relates to actual or threatened harmful conduct, or aascargo.com believes that such action is necessary to conform to the requirements of the law or comply with governmental orders, court orders, or to protect and defend the property or rights of aascargo.com the users of its website or the public. This includes exchanging information with other companies and organizations for fraud protection and prevention, credit risk protection, and other prohibited or illegal activities. If Aascargo.com should ever file for bankruptcy or merge with another company, we may disclose the information you provide to us at aascargo.com website to a third party or the company we merge with. Except as described above, we will not disclose your personal information to other third parties unless you have provided consent. We do not sell or rent your personal information to third parties for their marketing purposes without your express consent</p>\r\n\r\n<h3>Other Sites Privacy</h3>\r\n\r\n<p>This Site may contain links to third party websites, and aascargo.com is not responsible for the privacy practices or the content of such websites. This privacy statement also does not apply to the web sites of our business partners or to any other third parties, even if their web sites are linked to our web site. We recommend you review the privacy policies of each web site you visit.</p>\r\n\r\n<h3>Your consent to this policy</h3>\r\n\r\n<p>By using the aascargo.com web site, you agree to this Privacy and Security Policy. This is our only Privacy and Security Policy and it supersedes any other. Our Terms of use take precedence over any conflicting Policy provision. If you have any questions, or would like further clarification on any of our policies, please contact us.</p>\r\n\r\n<h3>Legal Disclaimer</h3>\r\n\r\n<p>This Site operates AS-IS and IS-AVAILABLE, without liability of any kind. We are not responsible for events beyond our direct control. This Privacy and Security Policy is governed by Texas law, excluding conflicts of law principles. Any legal actions against us must be commenced in Texas within one year after the claim arose or be barred. Under no circumstance will any employee, directors or officers of American AirSea Cargo LLC be liable for any direct or indirect losses or damages arising out of or in connection with the use of the aascargo.com site. User agrees to defend, indemnify and hold harmless shoptomydoor.com and its affiliates from and against any and all claims, damages, costs and expenses, including attorneys&#39; fees, arising from or related to your use of the site.</p>','Shipping to Nigeria | Privacy Policy | Shoptomydoor','Privacy policy, privacy notice, privacy statement','Read through Shoptomydoor\\\'s privacy policy for shipping for a better shipping experience. Register now & start shipping. No hidden charges!','1',0),(9,'1','terms-conditions','Terms & Conditions','1533030954_thumb-1920-82317.jpg','<h2>Terms &amp; Conditions for Shipping</h2>\r\n\r\n<h3>Definitions</h3>\r\n\r\n<ul class=\"\\&quot;list-ui\\&quot;\">\r\n	<li>In this shipping agreement, &ldquo;we&rdquo;, &ldquo;our&rdquo;, &ldquo;us&rdquo;, and &ldquo;American AirSea Cargo&rdquo; refer to American AirSea Cargo LLC, its subsidiary and branches</li>\r\n	<li>&ldquo;You&rdquo; and &ldquo;Your&rdquo; refer to the shipper and its employees, principals and agents. The shipper in this agreement is the person or entity who delivered the cargo to American AirSea Cargo and who requested that the shipment be transported, and/ or any person/entity having an interest in the shipment and/or who acts as an agent of the shipper.</li>\r\n	<li>&ldquo;Package&rdquo; means any pallet, container, envelope etc that is accepted by us for delivery, and includes items tendered by you using our automated application, manifests or airway bills.</li>\r\n	<li>&ldquo;Shipment&rdquo; means one or more packages which are moving on a single house airway, an airway bill, or invoice number or shipment number, or manifested from an automated shipping application and accepted by us.</li>\r\n	<li>&quot;Dangerous Goods&quot; means cargo which is noxious, hazardous, inflammable, explosive or offensive (including radioactive materials) or may become noxious, hazardous, inflammable, explosive or offensive or radioactive or may become liable to cause damage to any person or property whatsoever whether prescribed by laws or otherwise.</li>\r\n	<li>&quot;Airfreight Convention&quot; means whichever may be applicable of the:\r\n	<ol class=\"\\&quot;list-ol\\&quot;\">\r\n		<li>Convention for the Unification of Certain Rules for International Carriage by Air signed at Montreal on 28 May 1999; or</li>\r\n		<li>Convention for the Unification of Certain Rules relating to International Carriage by Air, signed at Warsaw on 12 October 1929; either unamended or amended by the Hague Protocol 1955; at Guatemala City 1971, by the additional Protocol No 3 of Montreal 1975 and/or by the additional Protocol No. 4 of Montreal 1975.</li>\r\n	</ol>\r\n	</li>\r\n</ul>\r\n\r\n<h3>Agreement to the Terms of Service</h3>\r\n\r\n<p>This Agreement is not negotiable and therefore binding on both parties once we accept your shipment. Thus, by dropping off your shipment with us or sending your shipment to us or authorizing us to pick up your shipment, you agree to all the terms and conditions herein contained. It sets forth the rights, duties and obligations and in certain cases liabilities of parties having interest in any shipment. You also agree and acknowledge that we reserve the right, at our sole discretion to change or modify these terms and conditions at any time. Please review these terms and conditions periodically, especially before sending your shipment, as your use of our service each time constitutes an acceptance of any modified terms and conditions.</p>\r\n\r\n<ol class=\"\\&quot;list-ol\\&quot;\">\r\n	<li><strong>Your Obligation</strong>\r\n\r\n	<ul class=\"\\&quot;list-ui\\&quot;\">\r\n		<li>Shipper warrants and undertakes that each article in the shipment is <strong>properly and correctly described</strong> on the shipping document, and that any export document, accepted for transport, is properly marked and addressed and packaged to protect the enclosed goods for safe transportation.</li>\r\n		<li>Shipper also warrants that the dimensions and weight of the packages as manifested in our automated shipping application is correct, and that if such information is not correct, we reserve the right to weigh and measure the shipment and apply the appropriate charges.</li>\r\n		<li>You undertake that there are no dangerous goods in the shipment and that if there is any dangerous item as published by the Dangerous Goods Regulation from IATA, that you have stated it in the applicable shipping documents to comply with all International Air Transport Association (&ldquo;IATA&rdquo;) regulations or other applicable law(s) for shipment of dangerous goods.</li>\r\n		<li>You agree that you are aware of our published shipping rates and charges at the time of dropping shipment with us, and that you are also aware that shipping rates are based on actual or dimensional weight, whichever is greater. Dimensional weights for International Airfreight shall be calculated based on the standard formula used by either Fedex, UPS or DHL and where there is a difference in all, the formula for DHL shall apply. Please see our Frequently Asked Questions published on our website for more details or see examples of shipping calculations by clicking here.</li>\r\n		<li>You agree that we may complete, correct or replace for you, the documents submitted if found to be inappropriate for the service or destination requested and at your expense.</li>\r\n		<li>You agree to make payment in advance of shipment and agree that the shipment may be delayed or dropped totally if payment is not received 24 hours before estimated departure.</li>\r\n	</ul>\r\n	</li>\r\n	<li><strong>Undertaking against illegal use/ Fraud</strong>\r\n	<ul class=\"\\&quot;list-ui\\&quot;\">\r\n		<li>You undertake that the contents of all shipments are goods owned by you and obtained through legitimate means. You agree not to deliver any shipment to us that has a lien on it, or that is not lawfully owned by you.</li>\r\n		<li>You further undertake that any use of the Service and/or Account shall be in compliance with all applicable laws in the state of Texas, including all Federal, State and Local laws in the United State of America, as well as other applicable International law, including laws related to the transportation and export of commercial matter, which may include without limitation laws related to banking, money laundering, trade sanctions and terrorist activities.</li>\r\n		<li>You agree that any goods delivered to us, later discovered to have been obtained through fraudulent or other unlawful or inappropriate means or in violation of this clause may result in the forfeiture, return to sender, or destruction of shipment, along with notification to the police and/or appropriate government authorities either in the country of departure or destination, and that such goods may only be released to you upon a written authorization from the government agency.</li>\r\n		<li>You further agree that all shipping costs will still be paid by you and that any payment made to us for goods that have not shipped will be used as cost of fraud investigation. In that regard, you agree that your account will immediately be suspended, and you will be prosecuted in the law court of your locality.</li>\r\n	</ul>\r\n	</li>\r\n</ol>',NULL,NULL,'','1',0);
/*!40000 ALTER TABLE `stmd_sitepages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_siteproducts`
--

DROP TABLE IF EXISTS `stmd_siteproducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_siteproducts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productName` varchar(255) NOT NULL DEFAULT '',
  `categoryId` int(11) NOT NULL,
  `subCategoryId` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) DEFAULT NULL,
  `weight` decimal(12,2) NOT NULL DEFAULT '0.00',
  `image` varchar(255) NOT NULL DEFAULT '',
  `message` text,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_siteproducts`
--

LOCK TABLES `stmd_siteproducts` WRITE;
/*!40000 ALTER TABLE `stmd_siteproducts` DISABLE KEYS */;
INSERT INTO `stmd_siteproducts` VALUES (1,'Laptop',36,37,0,7.00,'Laptop.JPG','<p>\r\n	All brands of 11 in laptops delivered at the stated price. Save by shopping now on <a href=\"http://www.amazon.com\">www.amazon.com</a></p>\r\n','1'),(2,'Sun Glasses',4,41,0,1.00,'aaaaa2.png','<p>\r\n	Make sure you have these shades on when you&#39;re <span data-scayt_word=\"rollin\" data-scaytid=\"1\">rollin</span>&#39; in your drop top, hand tooled acetate frame, wire injected temple core. Save by shopping now on <a href=\"http://www.6pm.com\" target=\"_blank\">www.6pm.com</a>&nbsp;&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a></p>\r\n','1'),(3,'Small handbag',4,42,0,1.50,'001.jpg','<p>\r\n	<span style=\"color: rgb(17, 17, 17); font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;\">Add a touch of <span data-scayt_word=\"glamour\" data-scaytid=\"2\">glamour</span> everywhere you go with&nbsp;</span>All brands of&nbsp;Small hand <span style=\"color: rgb(17, 17, 17); font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;\">bag. d</span>elivered at the stated price. Save by shopping now on &nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://myhabit.com/\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://ebay.com/\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://aldoeshoes.com/\" target=\"_blank\">aldoeshoes.com</a></p>\r\n','1'),(4,'Jeans',4,42,0,1.50,'Untiwwwwtled-2.png','<p>\r\n	All brands of&nbsp;Jeans delivered at the stated price put&nbsp;<span style=\"color: rgb(17, 17, 17); font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;\">a touch of&nbsp;</span><span data-scayt_word=\"glamour\" data-scaytid=\"3\">glamour</span>&nbsp;to your outfits . Save by shopping now on&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>&nbsp;<a href=\"http://www.myhabit.com/\" target=\"_blank\">www.myhabit.com</a>&nbsp;<a href=\"http://www.zappos.com/\" target=\"_blank\">www.zappos.com</a></p>\r\n','1'),(5,'Jeans',4,41,0,1.50,'Untjjjjjjjjjjjitled-2.png','<p>\r\n	All brands of&nbsp;Jeans delivered at the stated price put&nbsp;<span style=\"color: rgb(17, 17, 17); font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;\">a touch of&nbsp;</span><span data-scayt_word=\"glamour\" data-scaytid=\"4\">glamour</span> to your outfits . Save by shopping now on&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a></p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.myhabit.com/\" target=\"_blank\">www.myhabit.com</a></p>\r\n<p>\r\n	<a href=\"http://www.zappos.com/\" target=\"_blank\">www.zappos.com</a></p>\r\n','1'),(6,'Women shoes (without box)',4,42,0,2.00,'5.jpg','<p>\r\n	Shop all brands of&nbsp;Women shoes (without box) delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://myhabit.com/\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://ebay.com/\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" target=\"_blank\">aldoeshoes.com</a></p>\r\n','1'),(7,'Medium hand bag',4,42,0,2.00,'02.jpg','<p>\r\n	All brands of Medium&nbsp;<span style=\"color: rgb(17, 17, 17); font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;\">handbag is the perfect companion for any occasion. d</span>elivered at the stated price. Save by shopping now on&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://myhabit.com/\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://ebay.com/\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" target=\"_blank\">aldoeshoes.com</a></p>\r\n','1'),(8,'Women shoes (with box)',4,42,0,2.50,'4-03.jpg','<p>\r\n	Shop all brands of&nbsp;Women shoes (with box) delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on &nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://myhabit.com/\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://ebay.com/\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" target=\"_blank\">aldoeshoes.com</a></p>\r\n','1'),(9,'Men shoes (without box)',4,41,0,2.00,'6.jpg','<p>\r\n	Shop all brands of men shoes (without box) delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://myhabit.com/\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://ebay.com/\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" target=\"_blank\">aldoeshoes.com</a></p>\r\n','1'),(10,'Men shoes (with box)',4,41,0,2.50,'7.jpg','<div id=\"learn_more_50_tooltip\" style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	<p>\r\n		Shop all brands of men shoes (with box) delivered at the stated price.</p>\r\n	<p>\r\n		Save by shopping now on</p>\r\n	<p>\r\n		&nbsp;<a href=\"http://www.amazon.com/\" style=\"color: rgb(4, 63, 160);\">www.amazon.com</a>,</p>\r\n	<p>\r\n		&nbsp;<a href=\"http://myhabit.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">myhabit.com</a>,</p>\r\n	<p>\r\n		&nbsp;<a href=\"http://ebay.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">aldoeshoes.com</a></p>\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n','1'),(11,'Belts',4,41,0,1.00,'as.jpg','<p>\r\n	Shop all brands of&nbsp;<span data-scayt_word=\"Jewelries\" data-scaytid=\"7\">belts</span>&nbsp;delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://www.zappos.com/\" target=\"_blank\">www.zappos.com<span style=\"line-height: 14.3999996185303px;\">&nbsp;</span></a></p>\r\n<p>\r\n	<a href=\"http://bluefly.com\" target=\"_blank\">bluefly.com</a></p>\r\n<p>\r\n	<a href=\"http://tmlewin.com\" target=\"_blank\">tmlewin.com</a></p>\r\n<p>\r\n	<a href=\"http://10dollarmall.com\" target=\"_blank\">10dollarmall.com</a></p>\r\n','1'),(12,'Watches',4,41,0,1.00,'8-01.jpg','<p>\r\n	Accent your wrist with sophisticated look.</p>\r\n<p>\r\n	Shop all brands of Watches delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 1.2em;\">.</span></p>\r\n','1'),(13,'bra',4,42,0,0.50,'Unbraaaaaatitled-2.jpg','<p>\r\n	Supportive underwire push-up bra with decorative adjustable straps, mesh embroidered cups and a double hook closure on back. Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://www.zappos.com/\" target=\"_blank\">www.zappos.com<span style=\"line-height: 14.3999996185303px;\">&nbsp;</span></a></p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/\" target=\"_blank\">www.burlingtoncoatfactory.com</a></p>\r\n','1'),(14,'Men shirt',4,41,0,0.50,'new Untitled-2.png','<p>\r\n	Sheer your excellent, shop all brands of men shirt. &nbsp;Save by shopping now on&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>&nbsp;<a href=\"http://www.myhabit.com/\" target=\"_blank\">www.myhabit.com</a>&nbsp;<a href=\"http://www.zappos.com/\" target=\"_blank\">www.zappos.com</a>&nbsp;<a href=\"http://www.ebay.com/\" target=\"_blank\">www.ebay.com</a></p>\r\n','1'),(15,'Large handbag',4,42,0,3.00,'1234.jpg','<p>\r\n	<span style=\"color: rgb(17, 17, 17); font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;\">Classic and great spirited colors and luxurious&nbsp;</span>medium hand bags delivered to you. Get awesome bags at great prices on: &nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>.&nbsp;<a href=\"http://myhabit.com/\" target=\"_blank\">myhabit.com</a>,&nbsp;<a href=\"http://ebay.com/\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" target=\"_blank\">aldoeshoes.com</a></p>\r\n','1'),(16,'Cell Phones',3,40,0,1.50,'11.jpg','<p>\r\n	Shop all brands of phones delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">, &nbsp;</span><a href=\"http://www.bhphotovideo.com/\" target=\"_blank\">www.bhphotovideo.com</a></p>\r\n','1'),(17,'Womens wig',4,42,0,1.00,'WE-01.jpg','<p>\r\n	Shop all brands of Women wig delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n','1'),(18,'Women Suit',4,42,0,2.00,'123.jpg','<p>\r\n	Shop all brands of Women suit delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n','1'),(19,'Men Suit',4,41,0,2.00,'112.jpg','<p>\r\n	Shop all brands of Men Suit delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://www.zappos.com/\" target=\"_blank\">www.zappos.com<span style=\"line-height: 14.3999996185303px;\">&nbsp;</span></a></p>\r\n','1'),(20,'Women Gown',4,42,0,1.00,'1223.jpg','<p>\r\n	Shop all brands of Women Gown delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://www.zappos.com/\" target=\"_blank\">www.zappos.com<span style=\"line-height: 14.3999996185303px;\">&nbsp;</span></a></p>\r\n','1'),(21,'Jewelries',4,42,0,1.00,'WER-01.jpg','<p>\r\n	Shop all brands of&nbsp;<span data-scayt_word=\"Jewelries\" data-scaytid=\"7\">Jewelries</span>&nbsp;delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://www.zappos.com/\" target=\"_blank\">www.zappos.com<span style=\"line-height: 14.3999996185303px;\">&nbsp;</span></a></p>\r\n','1'),(22,'Camera',5,44,0,2.00,'cam.jpg','<p>\r\n	Shop all brands of&nbsp;<span data-scayt_word=\"Jewelries\" data-scaytid=\"7\">camera</span>&nbsp;delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://beachcamera.com\" target=\"_blank\">beachcamera.com</a>,</p>\r\n<p>\r\n	<a href=\"http://bhphotovideo.com\" target=\"_blank\">bhphotovideo.com</a></p>\r\n','1'),(23,'Ipads/tablets',3,39,0,3.00,'pad.jpg','<p>\r\n	Shop&nbsp;all&nbsp;<span data-scayt_word=\"brandsof\" data-scaytid=\"4\">brandsof</span>&nbsp;<span data-scayt_word=\"Ipads\" data-scaytid=\"5\">Ipads</span>/tablets&nbsp;</p>\r\n<p>\r\n	delivered at the stated price.</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://bhphotovideo.com/\" target=\"_blank\">bhphotovideo.com</a></p>\r\n<p>\r\n	<a href=\"http://newegg.com\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(24,'DVD',7,46,0,0.60,'dvd.jpg','<div>\r\n	These are one-time record <span data-scayt_word=\"discs\" data-scaytid=\"7\">discs</span>, ensuring that your music, images are quality. Shop all brands of DVD and CD delivered at the stated price.Save by shopping now on</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p>\r\n	<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://bhphotovideo.com/\" target=\"_blank\">bhphotovideo.com</a></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(25,'CD',7,46,0,0.50,'cd2-2.jpg','<p>\r\n	<span style=\"color: rgb(51, 51, 51); font-family: verdana, arial, helvetica, sans-serif; font-size: small;\">These are one-time record <span data-scayt_word=\"discs\" data-scaytid=\"1\">discs</span>, ensuring that your music, images are quality.&nbsp;</span>Shop&nbsp;all&nbsp;brands of CD delivered at the stated price.Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://bhphotovideo.com/\" target=\"_blank\">bhphotovideo.com</a></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(26,'Memory',0,25,0,1.00,'new memory.jpg','<p>\r\n	<span style=\"color: rgb(51, 51, 51); font-family: verdana, arial, helvetica, sans-serif; font-size: small;\">For high speed of your laptops&nbsp;</span>Shop&nbsp;all&nbsp;brands of memory delivered at the stated price.Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://bhphotovideo.com/\" target=\"_blank\">bhphotovideo.com</a></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(27,'Video Cards',0,25,0,1.50,'new video card.jpg','<p>\r\n	<font color=\"#333333\" face=\"verdana, arial, helvetica, sans-serif\" size=\"2\">S</font>hop&nbsp;all&nbsp;brands of Video Cards delivered at the stated price.Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(28,'Router',0,25,0,3.00,'new router.jpg','<p>\r\n	<font color=\"#333333\" face=\"verdana, arial, helvetica, sans-serif\" size=\"2\">S</font>hop&nbsp;all&nbsp;brands of Routers delivered at the stated price.Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(29,'Switch (4 to 8 ports)',0,25,0,3.00,'new Switch (4 to 8 ports).jpg','<p>\r\n	<font color=\"#333333\" face=\"verdana, arial, helvetica, sans-serif\" size=\"2\">S</font>hop&nbsp;all&nbsp;brands of Switch (4 to 8 ports)s delivered at the stated price. Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(30,'Switch (12 to 16 ports)',0,25,0,4.00,'new  Switch (12 to 16 ports).jpg','<p>\r\n	<font color=\"#333333\" face=\"verdana, arial, helvetica, sans-serif\" size=\"2\">S</font>hop&nbsp;all&nbsp;brands of Switch Switch (12 to 16 ports)&nbsp;delivered at the stated price. Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(31,'Switch (24 to 48 ports)',0,25,0,8.00,'new Switch (24 to 48 ports).jpg','<p>\r\n	<font color=\"#333333\" face=\"verdana, arial, helvetica, sans-serif\" size=\"2\">S</font>hop&nbsp;all&nbsp;brands of Switch <span data-scayt_word=\"SwitchSwitch\" data-scaytid=\"2\">SwitchSwitch</span> (24 to 48 ports)delivered at the stated price. Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(32,'Flash Memory',0,25,0,1.00,'new flash memory.jpg','<p>\r\n	<font color=\"#333333\" face=\"verdana, arial, helvetica, sans-serif\" size=\"2\">S</font>hop&nbsp;all&nbsp;brands of Flash Memory delivered at the stated price. Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(33,'Laptop Bags',0,25,0,6.00,'lapto bag-1.jpg','<p>\r\n	Shop all brands of laptop bags for proper handling of your laptop system delivered at the stated price. Save by shopping now on&nbsp;</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n</p>','1'),(34,'Mouse',0,25,0,1.00,'mouise1.jpg','<p>\r\n	<font color=\"#333333\" face=\"verdana, arial, helvetica, sans-serif\" size=\"2\">S</font>hop&nbsp;all&nbsp;brands of Mouse delivered at the stated price. Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(35,'Laptop Battery',0,25,0,2.00,'batery1.jpg','<p>\r\n	<font color=\"#333333\" face=\"verdana, arial, helvetica, sans-serif\" size=\"2\">S</font>hop&nbsp;all&nbsp;brands of Laptop Batteries delivered at the stated price. Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n<p>\r\n	<a href=\"http://datavis.com/\" target=\"_blank\">datavis.com</a></p>\r\n','1'),(36,'Shoes - Kids (No box)',23,61,0,0.50,'new kkids shooeee.jpg','<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Shop all brands of Shoes - Kids (No box) delivered at the stated price.</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Save by shopping now on</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"color: rgb(4, 63, 160);\">www.amazon.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://myhabit.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">aldoeshoes.com</a></p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	<a href=\"http://www.carters.com/\" target=\"_blank\">carters.com</a></p>\r\n','1'),(37,'Shoes - Girls (No box)',23,61,0,1.00,'grils.jpg','<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Shop all brands of Shoes -Girls (No box) delivered at the stated price.</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Save by shopping now on</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"color: rgb(4, 63, 160);\">www.amazon.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://myhabit.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">aldoeshoes.com</a></p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	<a href=\"http://www.carters.com/\" target=\"_blank\">carters.com</a></p>\r\n','1'),(38,'Shoes - Boys (No box)',23,60,0,1.00,'new kids shhoe.jpg','<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Shop all brands of Shoes - Shoes - Boys (No box) delivered at the stated price.</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Save by shopping now on</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"color: rgb(4, 63, 160);\">www.amazon.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://myhabit.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">aldoeshoes.com</a></p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	<a href=\"http://www.carters.com/\" target=\"_blank\">carters.com</a></p>\r\n','1'),(39,'Shoes - Kids (with box)',23,61,0,1.00,'new kids qwith box.jpg','<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Shop all brands of Shoes - Shoes - Kids (with box) delivered at the stated price.</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Save by shopping now on</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"color: rgb(4, 63, 160);\">www.amazon.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://myhabit.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">aldoeshoes.com</a></p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	<a href=\"http://www.carters.com/\" target=\"_blank\">carters.com</a></p>\r\n','1'),(40,'Shoes - Girls (with box)',23,61,0,1.50,'new girl shoe with box.jpg','<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Shop all brands of Shoes - Girls (with box) delivered at the stated price.</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Save by shopping now on</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"color: rgb(4, 63, 160);\">www.amazon.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://myhabit.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">aldoeshoes.com</a></p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	<a href=\"http://www.carters.com/\" target=\"_blank\">carters.com</a></p>\r\n','1'),(41,'Shoes - Boys (with box)',23,60,0,1.50,'new boy shoe with box.jpg','<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Shop all brands of Shoes - Boys (with box) delivered at the stated price.</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	Save by shopping now on</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"color: rgb(4, 63, 160);\">www.amazon.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://myhabit.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">myhabit.com</a>,</p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;<a href=\"http://aldoeshoes.com/\" style=\"color: rgb(4, 63, 160);\" target=\"_blank\">aldoeshoes.com</a></p>\r\n<p style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">\r\n	<a href=\"http://www.carters.com/\" target=\"_blank\">carters.com</a></p>\r\n','1'),(42,'50 to 55 in TV',6,50,0,90.00,'S2-01.jpg','<div>\r\n	Amazing Brightness, Clarity and Color detail.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Shop Samsung <span data-scayt_word=\"C6\" data-scaytid=\"14\">C6</span>(6203) 55&quot;</div>\r\n<div>\r\n	delivered at the stated price. Save by shopping now on</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\">www.amazon.com</a><span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">,</span></div>\r\n<p>\r\n	<span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">&nbsp;</span><a href=\"http://ebay.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n','1'),(43,'60 to 65 in TV',6,50,0,120.00,'S2-02.jpg','<div>\r\n	Amazing Brightness, Clarity and Color detail.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Shop Samsung <span data-scayt_word=\"SC6300\" data-scaytid=\"12\">SC6300</span> (55&quot;)</div>\r\n<div>\r\n	delivered at the stated price. Save by shopping now on</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\">www.amazon.com</a><span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">,</span></div>\r\n<p>\r\n	<span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">&nbsp;</span><a href=\"http://ebay.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n','1'),(44,'70 to 75 in TV',6,50,0,137.00,'S2.png','<div>\r\n	Amazing Brightness, Clarity and Color detail.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Shop Samsung <span data-scayt_word=\"C77100\" data-scaytid=\"14\">C77100</span> (60&quot;)</div>\r\n<div>\r\n	delivered at the stated price. Save by shopping now on</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\">www.amazon.com</a><span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">,</span></div>\r\n<p>\r\n	<span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">&nbsp;</span><a href=\"http://ebay.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n','1'),(45,'80 to 85 in TV',6,50,0,200.00,'S2.jpg','<div>\r\n	Amazing Brightness, Clarity and Color detail.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Shop Samsung <span data-scayt_word=\"C55203\" data-scaytid=\"15\">C55203</span> (40&quot;)</div>\r\n<div>\r\n	delivered at the stated price. Save by shopping now on</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\">www.amazon.com</a><span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">,</span></div>\r\n<p>\r\n	<span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">&nbsp;</span><a href=\"http://ebay.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n','1'),(46,'Cars/Mini van tires',27,62,0,43.70,'Untitled-9.png','<div>\r\n	Original&nbsp;Cars/Mini van tires that Includes a water zone that propels water away from the tire.</div>\r\n<div>\r\n	delivered at the stated price. Save by shopping now on</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\">www.amazon.com</a><span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">,</span></div>\r\n<p>\r\n	<span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">&nbsp;</span><a href=\"http://ebay.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://www.goodyear.com/\" target=\"_blank\">goodyear.com</a></p>\r\n','1'),(47,'Baby Car Seat',29,70,0,55.00,'Carseat\'.jpg','<p>\r\n	The best and comfortable bay car seat for baby.</p>\r\n<p>\r\n	Get baby car seat on:</p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&amp;field-keywords=baby+car+seat&amp;rh=i%3Aaps%2Ck%3Ababy+car+seat&amp;ajr=0\" target=\"_blank\">amazon.com</a></p>\r\n','1'),(48,'Car radio',27,38,0,3.50,'new wiper blade-01.jpg','<div>\r\n	Original&nbsp;Car radio delivered at the stated price. Save by shopping now on</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\">www.amazon.com</a><span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">,</span></div>\r\n<p>\r\n	<span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">&nbsp;</span><a href=\"http://ebay.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://www.sony.com/\" target=\"_blank\">sony.com</a></p>\r\n','1'),(49,'Little snugglers size 3 (124)',29,68,0,14.30,'huggies little snuggler size (124).jpg','<p>\r\n	Genuine <span data-scayt_word=\"Huggies\" data-scaytid=\"7\">Huggies</span>&nbsp;at great discounted&nbsp;prices at:</p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/catalog/searchresults.aspx?filter=&amp;search=huggies\" target=\"_blank\">burlingtoncoatfactory.com</a></p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_ss_c_0_7?url=search-alias%3Dbaby-products&amp;field-keywords=huggies&amp;sprefix=huggies%2Caps%2C306\" target=\"_blank\">amazon.com</a></p>\r\n','1'),(50,'Little snugglers size 4 (70)',29,68,0,8.60,'huggies little snuggler size 4(70).jpg','<p>\r\n	Care for your baby. Use original <span data-scayt_word=\"huggies\" data-scaytid=\"4\">huggies</span>.</p>\r\n<p>\r\n	Buy quality for discounted prices on:</p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/catalog/searchresults.aspx?filter=&amp;search=huggies\" target=\"_blank\">burlingtoncoatfactory.com</a></p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_ss_c_0_7?url=search-alias%3Dbaby-products&amp;field-keywords=huggies&amp;sprefix=huggies%2Caps%2C306\" target=\"_blank\">amazon.com</a></p>\r\n','1'),(51,'Oil Filter',27,65,0,1.00,'new oil filter.jpg','<div>\r\n	Original&nbsp;Oil Filter that filters dirty completely, delivered at the stated price. Save by shopping now on</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\">www.amazon.com</a><span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">,</span></div>\r\n<p>\r\n	<span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">&nbsp;</span><a href=\"http://ebay.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://www.autopartswarehouse.com/\" target=\"_blank\"><span data-scayt_word=\"autopartswarehouse\" data-scaytid=\"3\">autopartswarehouse</span></a></p>\r\n','1'),(52,'Similac Baby foods',29,71,0,1.00,'Similac.jpg','<p>\r\n	Perfect nutrition for your baby. Get original&nbsp;<span data-scayt_word=\"Similac\" data-scaytid=\"5\">Similac</span> baby food on&nbsp;</p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_ss_fb_0_6?url=search-alias%3Dbaby-products&amp;field-keywords=similac&amp;sprefix=simila%2Csporting%2C329\" target=\"_blank\">amazom.com</a></p>\r\n','1'),(53,'Baby dry size 4 (180)',29,68,0,18.40,'4 180.jpg','<p>\r\n	Original pampers care for your baby&#39;s tenders skin and keeps baby&#39;s crib dry.</p>\r\n<p>\r\n	Buy original pampers from</p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Dbaby-products&amp;field-keywords=pampers&amp;rh=n%3A165796011%2Ck%3Apampers\" target=\"_blank\">amazon.com</a></p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/catalog/searchresults.aspx?filter=&amp;search=pamper\" target=\"_blank\">burlingtoncoatfactory.com</a></p>\r\n','1'),(54,'Swaddlers size 1 (100)',29,68,0,9.20,'s1.jpg','<p>\r\n	Original pampers care for your baby&#39;s tenders skin and keeps baby&#39;s crib dry.</p>\r\n<p>\r\n	Get original pampers from at great discounted prices at&nbsp;</p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Dbaby-products&amp;field-keywords=pampers&amp;rh=n%3A165796011%2Ck%3Apampers\" target=\"_blank\">amazon.com</a></p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/catalog/searchresults.aspx?filter=&amp;search=pamper\" target=\"_blank\">burlingtoncoatfactory.com</a></p>\r\n','1'),(55,'Swaddler size 2 (92)',29,68,0,9.20,'pampers swaddlers size 2 pack of 92.jpg','<p>\r\n	Genuine pampers care for your baby&#39;s tenders skin and keeps baby&#39;s crib dry.</p>\r\n<p>\r\n	Get original pampers from at great discounted prices at&nbsp;</p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Dbaby-products&amp;field-keywords=pampers&amp;rh=n%3A165796011%2Ck%3Apampers\" target=\"_blank\">amazon.com</a></p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/catalog/searchresults.aspx?filter=&amp;search=pamper\" target=\"_blank\">burlingtoncoatfactory.com</a></p>\r\n','1'),(56,'Baby dry size 5 (160)',29,68,0,18.50,'5 160.jpg','<p>\r\n	Genuine pampers care for your baby&#39;s tenders skin and keeps baby&#39;s crib dry.</p>\r\n<p>\r\n	Buy genuine &nbsp;pampers awesome discounted prices at&nbsp;</p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Dbaby-products&amp;field-keywords=pampers&amp;rh=n%3A165796011%2Ck%3Apampers\" target=\"_blank\">amazon.com</a></p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/catalog/searchresults.aspx?filter=&amp;search=pamper\" target=\"_blank\">burlingtoncoatfactory.com</a></p>\r\n','1'),(57,'Canon pixma MG 7120',25,54,0,23.70,'canon pixma MG 7120.jpg','<p>\r\n	Shop original Canon <span data-scayt_word=\"pixma\" data-scaytid=\"8\">pixma</span> MG 7120 &nbsp;delivered at the stated price. Save by shopping now on</p>\r\n<div>\r\n	&nbsp;<a href=\"http://www.amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\">www.amazon.com</a><span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">,</span></div>\r\n<p>\r\n	<span style=\"color: rgb(0, 78, 89); font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center;\">&nbsp;</span><a href=\"http://ebay.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: 11px; text-align: center; color: rgb(4, 63, 160);\" target=\"_blank\">ebay.com</a>,&nbsp;</p>\r\n<p>\r\n	<a href=\"http://bhphotovideo.com/\" target=\"_blank\">bhphotovideo.com</a></p>\r\n<p>\r\n	<a href=\"http://electronicexpress.com/\" target=\"_blank\">electronicexpress.com</a></p>\r\n<p>\r\n	<a href=\"http://newegg.com/\" target=\"_blank\">newegg.com</a></p>\r\n','1'),(58,'Swaddlers Diapers Size 3 (162)',29,68,0,18.30,'swaddler size 3 162 count.png','<p>\r\n	Genuine pampers care for your baby&#39;s tenders skin and keeps baby&#39;s crib dry.</p>\r\n<p>\r\n	Buy genuine &nbsp;pampers awesome discounted prices at&nbsp;</p>\r\n<p>\r\n	<a href=\"http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Dbaby-products&amp;field-keywords=pampers&amp;rh=n%3A165796011%2Ck%3Apampers\" target=\"_blank\">amazon.com</a></p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/catalog/searchresults.aspx?filter=&amp;search=pamper\" target=\"_blank\">burlingtoncoatfactory.com</a></p>\r\n','1'),(59,'Baby Crib',29,69,0,79.30,'baby crib.jpg','<p>\r\n	Get befitting beautiful and quality baby crib that gives your baby the tender care he deserves.</p>\r\n<p>\r\n	See <a href=\"http://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias%3Dbaby-products&amp;field-keywords=baby+crib&amp;rh=n%3A165796011%2Ck%3Ababy+crib\" target=\"_blank\">amazon.com</a></p>\r\n<p>\r\n	<a href=\"http://www.burlingtoncoatfactory.com/babydepot/Furniture/Cribs/Convertible-Cribs/Web-Only/Mini-Dora-2-in-1-Convertible-Crib-White-310145777.aspx?h=56978\" target=\"_blank\">burlingtoncoatfactory.com</a></p>\r\n','1'),(60,'Nautica Voyage By Nautica',30,72,0,1.00,'Unnntitled-1.png','<p>\r\n	EDT SPRAY 3.4 OZ Design House: Nautica Year Introduced: 2006 Fragrance Notes: Amber, Apple, Drenched Mimosa, Greenleaf, Cedar Wood, Water Lotus, Moss, Musk, Sailcloth Accord Recommended Use: Daytime</p>\r\n<p>\r\n	Save by shopping now on</p>\r\n<p>\r\n	&nbsp;<a href=\"http://www.amazon.com/\">www.amazon.com</a>,</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;<a href=\"http://ebay.com/\" style=\"line-height: 1.2em;\" target=\"_blank\">ebay.com</a><span style=\"line-height: 14.3999996185303px;\">,</span></p>\r\n','1'),(61,'Book (Less than 100 pages)',7,47,0,0.40,'book-01.jpg','<p>\r\n	Develop yourself through books.</p>\r\n<p>\r\n	You can buy <span data-scayt_word=\"1000s\" data-scaytid=\"1\">1000s</span> of book online from USA</p>\r\n<p>\r\n	<a href=\"http://amazon.com\">amazon.com</a></p>\r\n','1'),(62,'Book (100-300 pages)',7,47,0,1.20,'book-02.jpg','<p style=\"font-size: 12px;\">\r\n	Develop yourself through books.</p>\r\n<p style=\"font-size: 12px;\">\r\n	You can buy&nbsp;<span data-scayt_word=\"1000s\" data-scaytid=\"3\">1000s</span>&nbsp;of book online from USA</p>\r\n<p style=\"font-size: 12px;\">\r\n	<a href=\"http://amazon.com/\">amazon.com</a></p>\r\n','1'),(63,'Book (Above 500 pages)',7,47,0,4.00,'book.jpg','<p>\r\n	Develop yourself through books.</p>\r\n<p>\r\n	You can buy&nbsp;<span data-scayt_word=\"1000s\" data-scaytid=\"1\">1000s</span>&nbsp;of book online from USA</p>\r\n<p>\r\n	<a href=\"http://amazon.com/\">amazon.com</a></p>\r\n','1'),(64,'PlayStation 4 500GB Console',5,45,0,10.50,'ps4.png','<p>\r\n	<span style=\"color: rgb(51, 51, 51); font-family: verdana, arial, helvetica, sans-serif; font-size: small;\">The Limited Edition Uncharted 4 PlayStation 4 Bundle. Featuring a custom Gray Blue console, matching <span data-scayt_word=\"DualShock\" data-scaytid=\"10\">DualShock</span> 4 Wireless controller, and Uncharted 4: A Thief&#39;s End, it&#39;s everything an aspiring treasure hunter needs to travel to exotic locations and rediscover fortunes forgotten by time. Shop from <a href=\"http://Amazon.com\" target=\"_blank\">Amazon.com</a></span></p>\r\n','1'),(65,'Apple laptops',36,51,0,11.00,'apple laptop.png','<p>\r\n	<span style=\"color: rgb(51, 51, 51); font-family: verdana, arial, helvetica, sans-serif; font-size: small;\">MacBook Air, the most mobile Mac in every way, shape, and form. It features all-flash storage, a Multi-Touch <span data-scayt_word=\"trackpad\" data-scaytid=\"20\">trackpad</span>, a long-lasting battery, a high-resolution display, an Intel Core 2 Duo processor, and NVIDIA graphics.</span></p>\r\n<p>\r\n	<span style=\"color: rgb(51, 51, 51); font-family: verdana, arial, helvetica, sans-serif; font-size: small;\">Shop from&nbsp;</span><a href=\"http://amazon.com/\" style=\"font-family: verdana, arial, helvetica, sans-serif; font-size: small;\" target=\"_blank\">Amazon.com</a></p>\r\n','1'),(66,'Desktop',36,50,0,25.00,'Desktop1.png','<p>Desktop&lt;p&gt;&lt;img class=&quot;ui-widget-content&quot; src=&quot;http://10.0.14.103/shoptomydoordev/public/uploads/media/original/1531889718_images-8.jpg&quot; /&gt;&lt;/p&gt;</p>','1'),(67,'TERRAIN GRIPPER A/T G',27,53,0,109.00,'tire.jpg','<p>TERRAIN GRIPPER A/T G Tires by AMP&reg;. Season: All Season. Type: Truck / SUV, All Terrain / Off Road / Mud. AMP Tires is proud to introduce the latest addition to its line of off-road light truck tires - the all&ndash;new terrain Gripper A/T...<br />\r\nPerformance Tread Provides Excellent Off-Road Handling and a Smooth Ride.<br />\r\nMulti-Step Tread Blocks Ensure Great Traction In Any Road Conditions.&nbsp;</p>','1'),(68,'NT421Q',27,53,0,116.00,'tire.jpg','<p>NT421Q Tires by Nitto&reg;. Season: All Season. Type: Performance, Truck / SUV. This NT421Q tire is Nitto&rsquo;s premier all-season tire built specifically for your crossover or SUV. From style to function, this tire has been created with the...<br />\r\nIntelligent Maintenance Indicators<br />\r\n&nbsp;</p>','1'),(69,'Turbochargers & Superchargers',27,54,0,1000.00,'s-l300.jpg','<p>Performance results of this product are highly dependent upon your vehicle&#39;s modifications and tuning/calibration. The horsepower numbers represented are calculated based strictly on choke flow of the compressor map (total turbo capability), which represents the potential flywheel horsepower.&nbsp;</p>','1'),(70,'Spyder - Custom Headlights',27,55,0,200.00,'light.jpg','<p>&nbsp;Rectangular Black Projector LED Headlight (PRO-JH-7X6LED-HL-BK) by Spyder&reg;, 1 Piece. Black housing, clear lens. High/Low Beam. Low beam is on with two projectors lighted. High beam is on with all together lighted. These retrofit headlights are the most efficient way to get improved illumination while significantly enhancing the aesthetic appeal of your classic treasure. Built using modern technologies, these lights will deliver enhanced performance. CA Prop 65 WARNING - This product can expose you to chemicals including chromium (hexavalent compounds), which are known to the State of California to cause cancer or birth defects or other reproductive harm.&nbsp;</p>','1');
/*!40000 ALTER TABLE `stmd_siteproducts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_smstemplates`
--

DROP TABLE IF EXISTS `stmd_smstemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_smstemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateType` enum('G','O','S','T','W') NOT NULL DEFAULT 'G' COMMENT 'G=General,O=Order/Shipment Notification,S=Order Status (single),T=Storage Notification (single)',
  `templateKey` varchar(255) NOT NULL,
  `templateBody` text,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(10) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(10) NOT NULL DEFAULT '0',
  `deletedBy` int(10) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL COMMENT '0:not deleted,1:deleted',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1:active,0:inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_smstemplates`
--

LOCK TABLES `stmd_smstemplates` WRITE;
/*!40000 ALTER TABLE `stmd_smstemplates` DISABLE KEYS */;
INSERT INTO `stmd_smstemplates` VALUES (1,'G','registration_active','Please activate your account','2018-05-11 07:52:36',1,'2018-07-24 14:17:46',1,0,'0','1'),(2,'O','delayed_shipment','Delayed Shipment','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1'),(3,'O','dispatched_from_lagos','Dispatched From Lagos','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1'),(4,'O','shipped_out_of_USA','Shipped Out of USA','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1'),(5,'O','shipped_out_of_UK','Shipped Out of UK','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1'),(6,'O','arrived_lagos_airport','Arrived Lagos Airport','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1'),(7,'O','arrived_lagos_seaport','Arrived Lagos Seaport','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1'),(8,'O','pickup_partners_office','Partner Pick Up','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1'),(9,'O','transit_from_usa','In Transit from USA/UK/China','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1'),(11,'O','pickup_lagos_office','Lagos Office Pick Up','2018-06-26 09:52:33',1,'2018-06-26 09:52:33',0,0,'0','1');
/*!40000 ALTER TABLE `stmd_smstemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_sociallinks`
--

DROP TABLE IF EXISTS `stmd_sociallinks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_sociallinks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media` varchar(255) NOT NULL,
  `faclass` varchar(255) NOT NULL,
  `mediaName` varchar(255) NOT NULL,
  `mediaLink` varchar(255) NOT NULL,
  `display` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_sociallinks`
--

LOCK TABLES `stmd_sociallinks` WRITE;
/*!40000 ALTER TABLE `stmd_sociallinks` DISABLE KEYS */;
INSERT INTO `stmd_sociallinks` VALUES (1,'fb','fa-facebook','Facebook','https://www.facebook.com/shoptomydoor/',1),(2,'gplus','fa-google-plus','Google Plus','https://plus.google.com/u/0/+Aascargoshipping/',1),(3,'twit','fa-twitter','Twitter','https://twitter.com/shop2mydoor/',1),(4,'linked','fa-linkedin','Linked In','https://www.linkedin.com/company/2709411?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A2709411%2Cidx%3A3-3-8%2CtarId%3A1464088751909%2Ctas%3Ashopto',1),(5,'instagram','ion-social-instagram','Instagram','https://www.instagram.com/shop2mydoor/',1);
/*!40000 ALTER TABLE `stmd_sociallinks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_splitshipmentcontents`
--

DROP TABLE IF EXISTS `stmd_splitshipmentcontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_splitshipmentcontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `splitShipmentType` enum('s','a','r','n') NOT NULL COMMENT '1 for sea shipping, 2 for air shipping, 3 for return shipping, 4 for notes',
  `shipmentTypeContents` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_splitshipmentcontents`
--

LOCK TABLES `stmd_splitshipmentcontents` WRITE;
/*!40000 ALTER TABLE `stmd_splitshipmentcontents` DISABLE KEYS */;
INSERT INTO `stmd_splitshipmentcontents` VALUES (1,'a','5 to 8 Weeks','1'),(2,'a','6 to 12 Business Days','1'),(4,'n','Thank you for your business.','1'),(5,'r','Return Processing Fee','1'),(6,'r','Return can take place upto 7 business days','1'),(7,'a','10 to 12 business days','0');
/*!40000 ALTER TABLE `stmd_splitshipmentcontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_states`
--

DROP TABLE IF EXISTS `stmd_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `countryCode` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `code` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name` varchar(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`countryCode`,`code`),
  UNIQUE KEY `state` (`name`),
  CONSTRAINT `stmd_states_ibfk_1` FOREIGN KEY (`countryCode`) REFERENCES `stmd_countries` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_states`
--

LOCK TABLES `stmd_states` WRITE;
/*!40000 ALTER TABLE `stmd_states` DISABLE KEYS */;
INSERT INTO `stmd_states` VALUES (1,'NG','DT','Delta','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(2,'NG','LG','Lagos','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(3,'NG','ABJ','Abuja','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(4,'NG','RV','Rivers','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(5,'NG','AB','Abia','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(6,'NG','AD','Adamawa','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(7,'NG','AK','Akwa Ibom','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(8,'NG','AN','Anambra','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(9,'NG','BA','Bauchi','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(10,'NG','BY','Bayelsa','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(11,'NG','BN','Benue','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(12,'NG','BO','Borno','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(13,'NG','CR','Cross River','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(14,'NG','EB','Ebonyi','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(15,'NG','ED','Edo','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(16,'NG','EK','Ekiti','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(17,'NG','EN','Enugu','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(18,'NG','GO','Gombe','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(19,'NG','IM','Imo','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(20,'NG','JG','Jigawa','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(21,'NG','KD','Kaduna','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(22,'NG','KN','Kano','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(23,'NG','KT','Katsina','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(24,'NG','KB','Kebbi','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(25,'NG','KO','Kogi','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(26,'NG','KW','Kwara','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(27,'NG','NA','Nasarawa','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(28,'NG','NG','Niger','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(29,'NG','OG','Ogun','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(30,'NG','ON','Ondo','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(31,'NG','OS','Osun','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(32,'NG','OY','Oyo','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(33,'NG','PL','Plateau','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(34,'NG','SO','Sokoto','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(35,'NG','TR','Taraba','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(36,'NG','YO','Yobe','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(37,'NG','ZA','Zamfara','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(80,'US','TX','Texas','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(88,'US','AR','Arkansas','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(89,'US','AL','Alabama','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(90,'US','AK','Alaska','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(91,'US','AZ','Arizona','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(92,'US','CA','California','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(93,'US','CO','Colorado','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(94,'US','CT','Connecticut','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(95,'US','DE','Delaware','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(96,'US','DC','District of Colombia','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(97,'US','FL','Florida','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(98,'US','GA','Georgia','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(99,'US','HI','Hawaii','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(100,'US','ID','Idaho','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(101,'US','IL','Illinois','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(102,'US','IN','Indiana','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(103,'US','IA','Iowa','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(104,'US','KS','Kansas','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(105,'US','KY','Kentucky','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(106,'US','LA','Louisiana','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(107,'US','ME','Maine','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(108,'US','MD','Maryland','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(109,'US','MA','Massachusetts','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(110,'US','MI','Michigan','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(111,'US','MN','Minnesota','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(112,'US','MS','Mississippi','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(113,'US','MO','Missouri','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(114,'US','MT','Montana','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(115,'US','NE','Nebraska','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(116,'US','NV','Nevada','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(117,'US','NH','New Hampshire','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(118,'US','NJ','New Jersey','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(119,'US','NM','New Mexico','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(120,'US','NY','New York','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(121,'US','NC','North Carolina','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(122,'US','ND','North Dakota','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(123,'US','OH','Ohio','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(124,'US','OK','Oklahoma','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(125,'US','OR','Oregon','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(126,'US','PA','Pennsylvania','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(127,'US','RI','Rhode Island','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(128,'US','SC','South Carolina','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(129,'US','SD','South Dakota','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(130,'US','TN','Tennessee','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(131,'US','UT','Utah','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(132,'US','VT','Vermont','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(133,'US','VA','Virginia','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(134,'US','WA','Washington','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(135,'US','WV','West Virginia','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(136,'US','WI','Wisconsin','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(137,'US','WY','Wyoming','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(138,'BZ','BZE','Belize','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(139,'GB','WHL','Whales','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(140,'ZA','GAU','Gauteng','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(141,'FR','PR','Paris','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(152,'TT','PS','Port of Spain','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(153,'GB','ENG','England','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(154,'CN','GD','Guangdong','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(155,'DE','HE','Hesse','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(156,'DE','NRW','North Rhine-Westphalia','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(158,'GB','SCOT','Scotland','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(159,'ES','VA','Valencia','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(160,'AO','HU','Huila','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(161,'ZA','WS','Western Cape','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(162,'ZA','ES','Eastern Cape','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(163,'ZA','FS','Free State','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(164,'ZA','LP','Limpopo','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(165,'ZA','KN','Kwazulu Natal','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(166,'ZA','MP','Mpumalanga','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(167,'ZA','NR','Northern Cape','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(168,'ZA','NW','North West ','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(169,'IT','LS','La Spezia','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(171,'BZ','CR','Central Region','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(172,'BZ','EB','East Belize','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(173,'AF','PW','Parwan','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(174,'AF','HM','Helmand','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(175,'AF','KB','Kabul','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(176,'AF','BK','Balkh','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(177,'AF','KD','Kandahar','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(178,'AF','KH','Khost','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(179,'AF','HR','Herat','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(180,'AF','LG','Logar','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(181,'AF','NG','Nangarhar','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(182,'AF','ZB','Zabul','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(184,'AF','PK','Paktika','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(185,'AF','OG','Oruzgan','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(186,'AL','BT','Berat','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(187,'AL','DB','Diber','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(188,'PH','PA','Pampanga','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(190,'TT','TG','Tobago','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(191,'RU','MS','Moscow','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(192,'AO','LU','Luanda','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(193,'VI','ST','St John','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(194,'ZA','ML','Milnerton','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(196,'GB','NTHLN','Northern Ireland','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(197,'PH','CA','Cavite','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(198,'NR','AB','Abia (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(199,'NR','ABJ','Abuja (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(200,'NR','AD','<script></script>','1','0',0,'0000-00-00 00:00:00',1,'2018-07-24 06:22:51',NULL,NULL),(201,'NR','AK','Akwa Ibom (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(202,'NR','AN','Anambra (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(203,'NR','BA','Bauchi (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(204,'NR','BY','Bayelsa (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(205,'NR','BN','Benue (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(206,'NR','BO','Borno (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(207,'NR','CR','Cross River (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(208,'NR','DT','Delta (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(209,'NR','EB','Ebonyi (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(210,'NR','ED','Edo (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(211,'NR','EK','Ekiti (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(212,'NR','EN','Enugu (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(213,'NR','GO','Gombe (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(214,'NR','IM','Imo (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(215,'NR','JG','Jigawa (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(216,'NR','KD','Kaduna (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(217,'NR','KN','Kano (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(218,'NR','KT','Katsina (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(219,'NR','KB','Kebbi (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(220,'NR','KO','Kogi (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(221,'NR','KW','Kwara (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(222,'NR','LG','Lagos (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(223,'NR','NA','Nasarawa (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(224,'NR','NG','Niger (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(225,'NR','OG','Ogun (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(226,'NR','ON','Ondo (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(227,'NR','OS','Osun (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(228,'NR','OY','Oyo (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(229,'NR','PL','Plateau (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(230,'NR','RV','Rivers (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(231,'NR','SO','Sokoto (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(232,'NR','TR','Taraba (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(233,'NR','YO','Yobe (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(234,'NR','ZA','Zamfara (DO)','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(236,'CG','BZV','Brazzaville','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(248,'GH','ASH','Ashanti','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(249,'GH','BRA','Brong-Ahafo','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(250,'GH','CEN','Central','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(251,'GH','EST','Eastern','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(252,'GH','ACC','Greater Accra','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(253,'GH','NRT','Northern','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(254,'GH','UWT','Upper West','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(255,'GH','UET','Upper East','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(256,'GH','VLT','Volta','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(257,'GH','WST','Western','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),(258,'AF','AE','Test1','1','1',1,'2018-05-11 07:56:29',1,'2018-05-11 07:57:18',1,NULL),(259,'AF','TT','Test','1','0',1,'2018-05-11 07:57:36',NULL,NULL,NULL,NULL),(262,'AF','FF','Test232','1','1',1,'2018-05-16 06:55:11',NULL,NULL,1,NULL),(271,'YY','WB','WestBengal','1','0',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_stores`
--

DROP TABLE IF EXISTS `stmd_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storeName` varchar(255) NOT NULL,
  `storeURL` varchar(255) DEFAULT NULL,
  `storeIcon` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_stores`
--

LOCK TABLES `stmd_stores` WRITE;
/*!40000 ALTER TABLE `stmd_stores` DISABLE KEYS */;
INSERT INTO `stmd_stores` VALUES (1,'Amazon','http://www.amazon.com','amazon.png','1','2018-05-30 14:59:45',1,'2018-05-30 14:59:45',0,'0','2018-05-30 14:59:45',0),(2,'Test','dsfsdfdf','IMG-20171002-WA0007.jpg','1','2018-05-30 15:13:56',1,'2018-05-30 15:13:56',0,'1','2018-05-30 15:15:22',1),(3,'Test','dsfsdfdf','1527854582_noimage.png','1','2018-05-31 06:08:56',1,'2018-06-01 12:03:02',1,'1','2018-06-01 12:04:57',1),(4,'Ebay','http://ebay.com',NULL,'1','2018-09-11 09:00:26',1,'2018-09-11 09:00:27',0,'0','2018-09-11 09:00:27',0);
/*!40000 ALTER TABLE `stmd_stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_taxes`
--

DROP TABLE IF EXISTS `stmd_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_taxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxName` varchar(50) NOT NULL,
  `taxDisplayName` varchar(50) DEFAULT NULL,
  `formula` text NOT NULL,
  `addressType` enum('S','B') NOT NULL COMMENT 'S=Shipping Address, B=Billing Address',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `regNumber` varchar(50) NOT NULL,
  `priority` int(3) DEFAULT '0',
  `formulaArrays` text NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_taxes`
--

LOCK TABLES `stmd_taxes` WRITE;
/*!40000 ALTER TABLE `stmd_taxes` DISABLE KEYS */;
INSERT INTO `stmd_taxes` VALUES (1,'VAT','VAT','ST+4','S','1','89798897979',1,'ST,+,4','0',1,'2018-06-07 08:24:52',NULL,NULL,NULL,NULL),(2,'Test','','ST','B','1','',0,'ST','1',1,'2018-09-12 14:19:32',1,'2018-09-12 14:20:14',1,'2018-09-12 14:20:23');
/*!40000 ALTER TABLE `stmd_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_taxrates`
--

DROP TABLE IF EXISTS `stmd_taxrates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_taxrates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxId` int(11) NOT NULL,
  `zoneId` int(11) NOT NULL,
  `rateValue` decimal(10,2) NOT NULL,
  `rateType` enum('$','%') NOT NULL COMMENT '$ means the default currency symbol	',
  `deleted` enum('0','1') DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tax` (`taxId`),
  KEY `fk_zone` (`zoneId`),
  CONSTRAINT `fk_tax` FOREIGN KEY (`taxId`) REFERENCES `stmd_taxes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_zone` FOREIGN KEY (`zoneId`) REFERENCES `stmd_zones` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_taxrates`
--

LOCK TABLES `stmd_taxrates` WRITE;
/*!40000 ALTER TABLE `stmd_taxrates` DISABLE KEYS */;
INSERT INTO `stmd_taxrates` VALUES (1,1,55,1.75,'%','0',1,'2018-06-08 08:02:40',NULL,NULL,NULL,NULL),(2,1,58,1.00,'$','0',1,'2018-06-11 11:04:38',NULL,NULL,NULL,NULL),(3,1,59,0.20,'$','0',1,'2018-06-12 05:20:20',NULL,NULL,NULL,NULL),(4,2,7,10.00,'%','0',1,'2018-09-12 14:19:57',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_taxrates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_testimonials`
--

DROP TABLE IF EXISTS `stmd_testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `author` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_testimonials`
--

LOCK TABLES `stmd_testimonials` WRITE;
/*!40000 ALTER TABLE `stmd_testimonials` DISABLE KEYS */;
INSERT INTO `stmd_testimonials` VALUES (3,'Years back, I use to think how I will be able to buy from USA. But now God has used Aascargo to make my online purchase a dream of the past. No fruad and my goods get to me as planned. I will always thank all of you for making me to believe and have the trust in your company. Keep up, you are NO 1.','Imeodu Ukpa','rascom@live.com','1','2018-05-08 08:27:29',0,'2018-05-18 12:28:04',1,'0','2018-05-18 11:14:39',0),(4,'I just dey jollof and jolificate. Life has never been better.\r\n\r\nMany thanks AASCARGO TEAM. Thumbs up!!!','BOLARINWA OSIYALE','bosiyale@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(5,'I ordered some items via amazon and used AASC as my shipping company. loved the way the items were repackaged by AASC staff. got my stuffs pretty fsst at a fair price. thanks. will sure do business with you some other time.','OUWASEUN AFOLABI','Seunlovesall@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(7,'what can i say than to say kudos to aascargo shipping company, i give you 3 gbosa, i recieve my shipment, well repackaged, thanks for efficient and effective delivery services, i can recommend you to anybody, anywhere, anytime, keep it up.','OGUNLEKE OLUBAYO TUNDE','ogunleketunde@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(8,'my instinct has never failed me, but America airsea cargo proved me wrong I gave it a trial and they delivered above my expectations. I encourage everyone to give it a trial. keep it up. kudos to your customer care, they are wonderful.','maxwell agbamu','max.agbamu@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(9,'Am a living witness to this unique innovation ','Maxi Collins','accountmaxi@hotmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(10,'If only Ocean shipping delays could be reduced, aascargo would be one of the best.','Yubee','akpakpanubong@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(12,'First of all let me commend your services and operations. Your ability to carter for a vast array of clients/customers over thousands of miles apart is very impressive as well as your ability to handle customers\' varying enquiries and issues is also highly commendable.\r\n\r\nI personally have not had any issues or complaints above your services as I have always received my goods intact and promptly delivered and I must say that your shipping cost is very competitive and affordable. Though it would be gratifying if the registration fees for the premium account could be re-evaluated and reviewed downward so as to encourage more people to sign up for them.\r\n\r\nI would also like to recommend frequent customer service operations courses and training for your customer service personnel to better equip them with the industry and international standard on how to respond to the various types of customers\' requests, enquiries and complaints.\r\n\r\nFrequent questionnaires could also be sent out to your customers either half yearly, quaterly or how ever is appropriate to your needs. This would give you more detailed perspective on how customers feel about certain aspects of your operations.\r\n\r\nIn conclusion, I would like to say qudos to you and I hope that the standard of your operations would not only be maintained but with time, become the best in the industry.\r\n\r\nThank you.','USMAN ABBA BELLO','abba.ub@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(13,'Secure, fast and affordable shipping service. I\'ve been a customer for American AirSea Cargo for over a year now, and never had a complain since. I was one of their first customers and I\'m proud to say, I have always experienced a tremendous service ever since. Keep up the good work... Thanks','Akinyanju Adeyanju','akkenzogag@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(14,'HI, I RECEIVED MY EQUIPMENT IN GOOD CONDITION TODAY. THANKS FOR UR WONDERFUL SERVICE. YOU GUYS ARE THE BEST','NELSON IZIREN','neliziafe@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(15,'Excellent Service AASCargo!!!!\r\n\r\nGreetings,\r\n\r\nI wish to commend the AAS Cargo warehousing team for the noticeable improvement in service as regards updating Warehouse information and shipping out. The improvement is evident in the handling of my recent order/shipment #18499.\r\n\r\nPlease keep it up; your excellent service is bringing smiles to your customers and be assured of my continued patronage and referrals to your service.\r\n\r\nBest regards.','Tyru','tokhade@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(16,'Aas Cargo has proven to offer the best and the most reliable service in this industry, thumbs up guys!!!','eze .C. ukpabi','eze_kingbybirth@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(17,'My purchases have always come in in good condition. It is really a relief to be able to buy goods from the US. I must also commend AAS for their prompt response to emails.','ADAEZE UDECHUKWU','ada_u2001@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(18,'ME EXPERIENCE WITH AASCARGO ARE WONDERFUL FOR THE PAST 2YRS NOW.GUARANTEED SAFETY OF YOUR CARGO IS 100%.NIGERIAN OFFICE NEED SOME IMPROVEMENT IN CUSTOMER SERVICE.','donatus ezeji','dtexintltd@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(19,'Many thanks AAS Cargo for their exemplary service. I found no difficulties with the delivery process and the their customer service staff were highly professional.','Ekpo Enodien','ekpoenodien@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(20,'AAS CARGO HAS BEEN OF TREMENDOUS HELP. I JUST CHECK UP WHAT I WANT FROM THE U.S AND PAY AND IT IS DELIVERED TO MY DOORSTEP. .....SPLENDID.','LESLIE AMANGALA','amangalaleslie@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(21,'What an Excellent Work,Thank you for making my dreams come true.Happy with Aascargo.Thank you for your effort.','Aishat wurno','aishatbellowurno@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(22,'I have used AAS for about a year now and so far so good.Warehouse personnel should be more careful as I experienced some mix up which caused delay in arrival. Apart from this,I\'ll recommend AAS to everyone. Good customer service,safety and good condition of goods on arrival guaranteed.','Igwe Adaugo','babylawyer4zos@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(23,'We have used aascargo twice and must say we are quite satisfied so far with their services. We had our orders delivered intact and to our doorstep with no funny charges. It is wonderful to be able to go online, place an order and have an address in the u.s to have your items delivered to.\r\nThank you aascargo. You have made shopping from u.s so much easier.','Diana Soladoye','','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(24,'AASGARGO is reliable and cost friendly. Keep up the good work guys!','Chidinma Jerry-Oji','chidinma.jerry-oji@jerrysbond.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(25,'The way my order is organise in the warehouse are encouraging. hope to receive my shipment soon. aascargo you\'re the best freight  forward yet. You\'ve save me lot of dollar!!!!!!. Dele WO','Dele Wilson Ogunbiyi','biyi2luv4@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(26,'AASCARGO is the best so far,I have renewed my subscription and even registered for a London address because of their efficiency.They have made my shopping and delivery so so easy,and saved my life a couple of times by delivering at my door step when I have a deadline to meet.','Aysha Wushishi','ayshawushishi@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(27,'Thanks AAS Cargo for professional help and service. AAS cargo are prompt and update items. Their staff are highly professional and their system is very user friendly. thank you.','Farouk Baba-Ahmed','fbaboss@hotmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(28,'My items have always come in in good condition and their rate is cheap so i can recommend AAS to everyone. Keep it up AAS','Ekhoragbon Kingsley','ekosas1987@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(29,'Shipping with aascargo has been a sweet experience, with you by my side am good to go. All i have to say is thank you for being there for me.','Gambo Fatima Abacha','lafatsexclusive@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(30,'The services keep improving daily.Keep it up I am satisfied having tried several other companies','Dapo Ojo','cplphotographers@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(31,'i\'ve being using AAS cargo for almost a year now and apart for some few mix-up in delivery my good and delays in arrival of goods ( due to custom clearing ) to my doorstep, they are quite prompt whenever the goods gets to their warehouse. they are also one of the the cheapest if not the cheapest. they\'ve  made shopping online very easy, will surely recommend it to friends and family','GBENGA E','','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(32,'Its a fantastic gateway for online shopping which nigerians has been starved of in the past, i made quite alot of shipments, some with minor hiccups and some with major hiccups, but most times shipments are delivered on time and brilliantly, however what kept my confidence is the company\'s propensity for improved services and response to complaints, it always puts a smile on my face to receive email or delivery from aascargo.....','Khayruldeen Abubakar','Khayruldeen@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(33,'American AirSea Cargo is great. The simple and user friendly processes in executing delivery is commendable. I am proud of your service. Adeline Ndoma','adeline ndoma','ndoma2@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(34,'I like the follow up response and tracking of my orders. It is a great experience. The return order is also great.','MERIMON YAKUBU ZEPHANIAH','merimony2k@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(35,'I have been using American AirSea Cargo for more than a year now and apart for some few mix-up in my delivery Address, delivering my orders and delays in arrival of goods to my doorstep, they are quite prompt whenever the goods gets to their warehouse. Their pricing is considerate compared to other similar service providers. They\'ve made shopping online very easy, I will recommend their services to friends and family','Chidiebere Okoronkwo','Chidiebere.Okoronkwo@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(36,'You guys should keep up the good work.','Tony Eyagbajumi','hammasgroup@ymail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(37,'The goods arrived on time and it was well packaged, the tracking system was also excellent to the very last detail. kudos keep it up.','NIKE OWOYOMI','natolaolu@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(38,'AASCARGO has really made shopping for me and my household I no longer have to admire what people abroad use I can now get them at my finger tip.','Adebusola Ogunyemi','ade_busola12@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(39,'I have been using aascargo for about a year now, buying stuff from Amazon and carid, and I must say aascargo are reliable, delays happen from time time but 8 out 10, they deliver, they always deliver, will them recommend to anyone any day.\r\nP S\r\nshipping charges depend on what you purchase, the bigger, the costly.','julian Mshelia','julianmshelia@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(40,'Ever since hearing about AAS Cargo on Inspiration FM a little over a year ago, I decided to give them a trial and I must confess I have not been disappointed. What once seem like a dream (shopping from US stores and other stores across the world and have them delivered to your doorstep) is now a reality! I have never lost any time and delivery albeit with some delays (usually due to custom and clearing inspection) is always on point. Did I mention their excellent customer service (with Oluchi being my favourite customer service representative). All I have to say is THANK YOU and please keep the flag flying ever so high!','Emmanuel Ekpo','darlington2000@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(41,'AASCargo for me, showed up at the right time with just what i needed (a proxy) for shopping and shipping from the US. I feel very in control of all my dealings and I like the fact that you are alerted by email on every step, till your stuff arrives.','David O','bluetoothworldmail@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(43,'American airsea cargo is an establishment that relate well with customers, with american airseacargo, goods don\'t get lost,I have been using their service for a few years now and so far I am impressed.  \r\nKeep getting better team','OLAYEMI IJENWA','olanmele2429@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(44,'What a company! After roaming, finally rested here. This is just the best I have seen. Though it is not the cheapest, it is the best. I had met one that ships a laptop for about 4dollar per lb to anywhere...meaning one can ship a laptop for about 50dollar maximum. BUT YOU WILL SURELY RECEIVE IT BROKEN AFTER TWO MONTHS. AASCargo is the most dependable. From the time I place my order from the marchant to the time it gets to me at my doorstep just takes two weeks. Thanks to the team.','Joe Nwabueze','JoeObiefuna@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(45,'I was initially discouraged by a friend about using AASCARGO for my shipment cos of time it takes package to arrive. This may be due to the experince he had with them.\r\nHowever, after trying other shipping company, I was forced to give AASCARGO a trial due to the exorbitant shipping cost of the other company and I never regreted I did. The delivery time surpasses my expectation and the cost was very pocket friendly. Good job!','SAMUEL IDOWU','idowusa01@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(46,'Just Joined Though; My Relatives and Friends have been singing wonderful praises of this company and its obvious they are well satisfied with your services;thats rare in this part of the world,keep up the good work.','RAPHAEL OKOJIE','ademartins88@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(47,'Signed up last year and brought in lots of stuff from the U.S. All I had to was shop online with my Visa card and have the store ship it to Houston. I always look forward to FedEx knocking at my door here in Lagos, to deliver my goods.','Flora A','pacem99@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(48,'wowww, we really still have some establishments to put our trust on, i started like a joke to buy something worth just $100, to see how reliable AASC is due to past experiences i\'ve had, but i tell you, now i shop worth thousands of dollars and sleep peacefully to wait for my package and i have never been disappointed.','INNOCENT SAMUEL','lootts@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(49,'great company,affordable freight,quick customer care response,in short this establishment is simply the best.','temitope osiyale','osiyaletemitope@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(50,'What appealed to me most was the warehouse address system. You can\'t beat that.','charles asianya','charlesasianya@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(51,'My first trial with AASCARGO proves you guys are the best!Can\'t believe I got all my baby\'s stuffs right on time and intact. Will keep on singing AASCARGO praises to my family and friends. Alot of them are ready to partner with you guys.','Lucy Nwaobi','candyluberry@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(54,'I had tried a Shipping company because I wanted to shop from USA. My experience was such that I was receiving bills with no indication of what I was paying for. Worst of all there was often a mix up with my children\'s orders and collection was a nightmare as they do not deliver door to door. \r\n\r\nSince I learnt of AASCargo I have no regret and no turningback. What I enjoy most is their Customer Service, Consolidation and delivery door to door. I do not want to hear about any other shipper let alone try them. I am a 100% satisfied customer.','Rabiu Aliyu','rabiualiyu@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(55,'I\'ve been using aascargo for well over a year now and it has been a great experience and useful for my business. great customer service and timely shipments. Thumbs Up!','Ade Adeyemi','info@thetimemerchantco.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(56,'Warehouse update is really great now. It is  fast and efficient. Great work warehouse staff!! I hope to see a continuous improvement in the overall service.','Priye Aya','aya.priye@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(57,'I have used AAS for 3 years now. They were the first shipping place I ever used. I am not one to use a service without testing others. I felt shipping with them was a bit costly. I tried other shippers and I had issues of missing items which to this day were never found. I have returned back to AAS cargo and I am not going elsewhere. Repackaging goods is awesome; better still is the warehouse updates and shipment status updates. Customer Service is really good, though can be improved upon. I am really impressed especially because I was there through these improvements. Keep up the good work and do not rest on your laurels.','Priye Aya','aya.priye@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(58,'American Air sea cargo does it for me. ALWAYS','Ndidi Osibo','endee_88@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(59,'Aas Cargo does a wonderful job, shopping online is now so easy!','Adebusola','','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(60,'I like the fact they respond appropriately to your concerns. It is very nice to see a company that is customer service oriented.','Omegie Nsofor','omegiee@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(63,'AASCargo Is the Best among the rest...Keep it up.','Christopher Mogaji','hamberstun10@live.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(66,'you are proudly Nigerian company......the best forwarding company for 2013.','Imeodu Ukpa','rascom@live.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(68,'I received my first international shipment with Aascargo at my office today at noon, it was just like magic!! Package arrived in perfect condition and was well wrapped. The tracking feature came in very useful and am especially impressed with that. I am already recommending their service to my friends. Please keep up the standard.\r\nkayode.','Kayode Banjo','banjokayode@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(69,'U ppl at just Godsent, very intelligent concept. Thumbs up aascargo.','Ifeoma Udoh','Udoh_ifeoma@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(75,'Received my second shipment today. Warehouse packaging was excellent and tamper proof. Items were delivered intact and in good condition. I am very impressed. Work still needs to be done to make the site more user friendly.','Kayode Banjo','banjokayode@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(77,'I have been a customer of AAScargo for several months now and I ship out almost every week. Over the months, it has been a roller coaster ride. AAS cargo is unique in terms of services offered. Nevertheless, they had efficiency issues so once in a while, there is one issue or the other to complain about. I wrote a damning testimonial some weeks ago but my position has changed since then.\r\n\r\nThe customer service agents are polite and always ready to help. Even in charged situations, they remain so. It is commendable.\r\n\r\nSecondly, AAS cargo listens. When you complain, they switfly make changes to correct their mistakes. That is also very commendable. It shows that the company cares about its customers.\r\n\r\nLastly, I wholeheartedly recommend AAS cargo. Your package may be delayed and sometimes left behind, but it will never get lost. Not once has my package gone missing or stolen. Most of the time, shipping is prompt with delays only occuring due to external factors. So if you are on the fence, am encouraging you to jump right in, AAS cargo is reliable.','Efemena Ozore','efeozore@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(78,'I absolutely love the warehouse address system. You can\'t beat that. With the management working very hard to improve the delivery time, I am very sure that this company will offer one of the best service in the world, American AirSea Cargo is well plan.','Prince Uduak Pedro','princeuduakp@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(79,'I received my first ever international shipment from USA with Aascargo at my office, it was just like miracle!! Package arrived in good condition and was well wrapped. The e-mail update was good and I am waiting for it at the gate not up to 30mins the package arived what a wonderful company and am especially impressed with that. I am recommending your service to my friends. Please keep up the standard.\r\nSunday Ojelabi','Sunday Ojelabi','sunnylab85@hotmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(80,'This saves a lot of troubles purchasing and shipping to Nigeria. It feels like shopping from your back yard. I have not tried any shoping/shipping outfit before ASSCARGO and I don\'t have the need to try another after it.','Samuel K','','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(81,'Sorry I did not do this earlier, package get to its destination on time and in safe condition. Sure will recommend my friend and the efficiency of American Air Sea Cargo. Thank You and keep up the good work.','Charles Ejiofor','goldmind4u@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(82,'you guys are the bomb...i received my order in due time and intact!!! thanks femi for the packaging....i have recommended aascargo to 5 people who have already registered. good job guys... keep it up!!!','Afolayan Fiyinfoluwa','ffiyinfolu@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(84,'AAS Cargo is the BOMB!!! Got my first shippment hassle free. Looking forward to my next shipment...\r\nOnly hope that sped of delivery could be made faster... In all it is worth the quid...','PIUS SULE','pius.sule@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(85,'Their shipment is fast...and easy...but customer service need more training...','Pamela Onyenobi','pamela.onyenobi@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(89,'Sooo sooo Excited\r\n\r\nWow I can\'t stop smiling from ear to ear, American Airsea cargo is simply the best, just got my items delivered today so excited, the bags are awesome, very great job to the entire team, sincerely apreciate u all, I just can\'t wait to start using my UBAPrepaidCard, that am sure will b extremely awesome, 1ce again thank u America Airsea cargo.','Eunice Jiya-Chitumu','nnaisa18@yahoo.co.uk','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(90,'Thank you for your concern and effort,may God continue too guide and protect you,and too lead us to the right path.ameen.thanks once again','Aishat wurno','aishatbellowurno@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(92,'Goods are shipped in good condition but uk warehouse do not update client\'s items on the site, hence a customer doesnt know when they have recieved items and when they will be shipped. Each time, I go through a silent period not getting any update on my goods. Please work o  that. Thank you','Adeniyi Adekoya','noadek@hotmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(93,'I had tried three other shippers before I learnt of AASC.  Previous shippers were horrendous;  One delivered my wardrobe after 5 months, when each time I called, I was reassured it would be delivered in \'two weeks\'.  Another Shipper\'s clerk was rude and asked that I take my business elsewhere when I went in to inquire on the delivery delay whilst her \'boss\' sat sanguinely next to her.  That took 3 Months.  The 3rd shipper\'s staff was polite but delivered two weeks late and all my goods were busted out of their containers.   AASC experience was like a dream: The staff was polite, the goods arrived on schedule as promised and not a single needle shifted out of its confines.  One would be a masochist to go anywhere else.','Nike Otuyelu','Nike.o@usa.net','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(97,'I just cant stop shopping in US, UK and China ever since i came across Aascargo. You guys keep up the good work. Thumbs Up','Noah Jimoh','noahthesuccess@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(100,'AAS Cargo is simply the best there is!! Shopping online has never been so easy and stress free!! My first time shipping with the company and  I am one happy customer....                                           Their Customer service is excellent with quick and polite response to customers concerns and complaints;  their Procurement  service is very good (just know that the procurement charge is cheaper when you list  all the items to be purchased in a single procurement order);  they  frequently update your warehouse items and you always receive email notifications to that effect;  they give refunds where applicable and when necessary;  their shipping prices are moderate and the shipping time is fairly ok (though in my own case, this was probably due to delays in shipping during the xmas holiday season);  their consolidation service (ie repackaging)  saved me a ton of money;  their tracking feature is very good, shipment updates are timely and my  goods arrived in excellent condition. Over all, I had a very pleasant experience and will continue shipping with AAS till further notice. Pls keep up the good work!!','Uwafili Yvonne','get2ivie@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(104,'keep up the good work!!','adetola okunola','adetolaokunola@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(105,'American Air Sea Cargo in my accessment is the best shipping company to nigeria. They do their job with passion and professionalism. Kudos!! Keep up the good work!!.\r\nJosh Idodo','Joshua Idodo','herbman98@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(107,'I commend the  professionalism of your staff, the friendliness of website and your speedy delivery of shipments. AAS CARGO IS SECOND TO NONE.','solayinka Agboola','pkaycee@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(108,'The best thing about aascargo\'s service is their prompt response to your customer care needs. That perhaps is due to an installed application assigned for the particular feat and a relative functioning customer care unit. \r\n\r\nHowever, non-invasive requests or instructions by customers aren\'t adhered to, despite their politeness. This suggests a disconnect between operations unit, administration and customer care units.\r\n\r\nMy summary is; this is a good company but its service to me was a bit disorganized despite correspondence between both parties.\r\n\r\nI am a bit unhappy, but I shall try them again giving the benefit of doubt due to the very good shipment of one of my first batch orders.\r\n\r\nBest of luck.','\'Ladi','ladiabudu@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(109,'Aascargo just recently successfully delivered some sensitive high value shipments to me. I was impressed as there were no issues whatsoever. The warehouse guys did a fantastic job with the packaging ensuring the safety of the items. Aascargo is efficient and professional.','Kayode Banjo','banjokayode@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(110,'i use to enjoy asscargo services but recently some agents just mix up items and at the end it causes so much delay.will suggest proper training of agents to avoid such.','Benedict Akpezi Edewor','benvoice@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(117,'Once again Aascargo has impressed me. The delivery arrived somewhat late but it arrived nonetheless. Fedex did a good job of ensuring delivery. I am particularly happy with Aascargo because they are service oriented company that values their customer. 5 star rating from me.','Kayode Banjo','banjokayode@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(118,'Bravo, aascargo! My shipment arrived on time. Can\'t wait to introduce friends & colleagues.','Mike Effiong','mikey.effiong@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(121,'aascargo is the best.','wande owoyemi','wande1759@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(123,'It too good to be true. A friend introduced me to this company since last year but I was skeptical to use them, but last month I just said let me try them and so far I love the way they have been handling me items. keep it up TEAM AASCARGO.','CHIBUZOR DANIEL-UMOH','chibuzor_n@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(124,'I must admit, your update and professionalism in highly commendable. Appreciate the good work.','Tyna Nisakpo','proten22ltd@yahoo.co.uk','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(125,'I want to commend the efforts of online customer service representative of one Emmanuel .O He is so quick to response  other representatives are nothing to write home about they just leave your conversation without attending to your questions but Emmanuel was so polite and  relieved me of all my worries.\r\nI really appreciate his services.','Francis Adebesin','afrotimy@yahoo.co.uk','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(126,'I have been using shoptomydoor for more than a year now and I have shipped dozens of packages. These guys keep getting better. Virtually every problem I had with their services has been resolved and they are more efficient than ever. I highly recommend shoptomydoor','Efemena Ozore','efeozore@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(127,'I have been using shoptomydoor for more than a year now and I have shipped dozens of packages. These guys keep getting better. Virtually every problem I had with their services has been resolved and they are more efficient than ever. I highly recommend shoptomydoor','Efemena Ozore','efeozore@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(128,'Like they use to say that a trial will surely convince you AASCARGO now shoptomydoor kudos to you for the job well done for me,they deliver my order as they promise ,although there is a little problem during the process,that need to improved on customers care agents,kudos to the lady that handle my cosigne miss or Mrs oby  she try her possible best kindly put more effort you really tried a lot keep it up','LAWAL Almansur','almoney10@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(129,'I want to special thank the management of shoptomydoor I have received my package.I am now convinced that you are for real Thank you !!!!','Oluwashina Ekundayo','ekuns4sure@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(130,'Just Got My Package this afternoon, you guys rock, am coming again , improve on timing though.','Augustine Nnajiego','Nnajiegoaustine@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(132,'WOW! FROM AMERICA TO GOMBE STATE! \r\nI RECEIVED MY PACK TODAY AND CAN\'T WAIT TO TESTIFY. \r\nSHOP TO MY DOOR, WE\'VE JUST STARTED.','Hezekiah Samaila','hezacodigitalworld@yahoo.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(133,'Shoptomydoor recently did what they call upgrade. For me its a Downgrade. I found out about shoptomydoor through a colleague who spoke so well about them so I decided to try my US and China warehouse, Shoptomydoor uses 2 weeks to calculate the weight of the goods you ship to their China warehouse. This info is not in their webpage, I had to call first before i was informed. Prior to that, I was they ship unopened from their outlet. One out of two of my US goods was shipped, I was not informed till I started asking questions. They have at least 3, three different answers to one question. They are not truthful, they don\'t keep to their words and I wished I read up about them on Nairaland. Don\'t use them please.','Gloria Nwosu','prinuo@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(134,'I just found the perfect solution to my insatiable shopping needs! Since the first time I used shoptomydoor, I find myself coming over and over again. Excellent services and prompt delivery.  I am stuck to you guys!','INYANG FOLAKEMI','folakemi.odeyemi@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(135,'Sincerely, I will be an ingrate not to drop my commendation. Being my first time to ship with them all the way from US, I was skeptical in the first stance but to my amazement, my orders were delivered today around 11am after 11days from the states. Thanks o...Quality service delivery with timeline updates.','samsudeen adebayo','samsudeen_adebayo@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(137,'Seriously if other Nigerian owned companies do business like this, then there is still hope. I\'ve been shipping with shoptomydoor.com and regardless of some issues sometimes I still come back. The reason is the highly professional manner in which they resolve all these issue and ensure that their customers get satisfaction. I\'m highly impressed and have recommended their services to many friends and colleagues. Keep it up.','Eze Uba','ezeuba@gmail.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(138,'Though SHOPTOMYDOOR is doing great work in consolidating and delivering packages, their response to customers concerns is inadequate. They just choose the issues they fell like responding to and ignore others. Most times I ask the same questions more than three times in email before I get a response. I believe the email interaction functions 24hrs a day, yet it takes days before I get explanations I seek. This is my experience and its too bad.','David Okoroafor','da_evido@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(139,'When you fail to try out different things in life, you will never know if they are good or not. I tried shoptomydoor, it delivered without itches. A trial will convince you.','Tsado Joel','yisajy@gmail.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(140,'I have been receiving my order intact until last month when 5 items where stolen, shoptomydoor received but this time denied responsibility, will like to advice that you check your orders before signing for because insurance does not cover. I must say am not happy, will advice you pick air shipment as it\'s faster and no one get to play with your intelligence.','Suzie Ibrahim','suzielove@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(141,'I have been begging for my items to be updated in my warehouse after 24 hours of delivery. I know this is a bid to make me incur store charges. I know this is a Nigerian thing but I think we can do better.','David Okoroafor','da_evido@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(142,'Have you ever wondered if you could just shop, click and ship without calls? Shoptomydoor makes it look like magic.','Danielson Iwambe','sesughi@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(143,'This Sham of a service is my worst experience ever. I have never regretted anything like using this service in my life and will never recommend it or use it again.\r\n\r\nvery typical! \r\n\r\ni am sure that the bad testimonials are being deleted, becos i cannot understand this reviews. no business will have a 100% positive testimonials.','Zarah Yarema','zmyarema@gmail.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(144,'please can someone help me with this site,,, i have some items in my warehouse and im ready to do the processing,i want to ship them to nigeria....contact me on 08035339000','Daniel James James','bradtaylor372@outlook.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(145,'I actually had received my orders twice, you are highly trusted\r\n\r\nBut please do something on timing and avoid multiple messaging after payment to avoid confusion','emmanuel onyema','emmaxplore@gmail.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(146,'I have just suffered my FIRST major disappointment with Shoptomydoor. Despite having called the customer care line twice today (including at 3:30PM) and I informed them that I am sending someone from LEKKI to AJAO ESTATE WAREHOUSE to pick up my shipment, I just got the rude shock now (when I just made a follow-up call to confirm receipts of the 3:44PM email below) that the warehouse is closed and my shipment was not dropped for me for pick up.\r\n\r\n\r\nThe person I sent has already spent over TWO HOURS in traffic and is almost about to get to the warehouse. So, all my efforts (including calls and email correspondences) is wasted.\r\n\r\n\r\nThis is NOT good customer service as I have been dealing with AmericanAirSeaCargo for over a year now.','OLUWOLE OLUYEMI','oluyemioluwole@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(147,'I have just suffered my FIRST major disappointment with Shoptomydoor.\r\n\r\n\r\nDespite having called the customer care line twice today (including at 3:30PM) and I informed them that I am sending someone from LEKKI to AJAO ESTATE WAREHOUSE to pick up my shipment, I just got the rude shock now (when I just made a follow-up call to confirm receipts of the 3:44PM email below) that the warehouse is closed and my shipment was not dropped for me for pick up.\r\n\r\n\r\nThe person I sent has already spent over TWO HOURS in traffic and is almost about to get to the warehouse. So, all my efforts (including calls and email correspondences) is wasted.\r\n\r\n\r\nThis is NOT good customer service as I have been dealing with AmericanAirSeaCargo for over a year now.','OLUWOLE OLUYEMI','oluyemioluwole@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(148,'Will not be using Shoptomydoor for any of urgent shipments except am willing to wait 3 weeks to have them delivered. Please change the info on your website, 5 - 7 business days for delivery is quite misleading.','OPEYEMI OLU-OPALEYE','yhemmy_20@yahoo.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(149,'Shoptomydoor sucks, i ordered for items from amazon, which was delivered, shoptomydoor prepared my shipment, and charged me for them, i paid and got my receipt but to my greatest surprise my goods didnt come complete.','Dereck Oneya','saheedaazim@gmail.com','0','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(150,'You guys are so wonderful, first some of my items didn\'t get to me and all i got is SORRY....... Now my TV arrived and chain/ticket holder is missing again rite???? I have done my last bit with you guys, all my friends are going to know this story','Dereck Oneya','saheedaazim@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-18 11:14:39',0,'0','2018-05-18 11:14:39',0),(151,'hmm its a dream come true in a nutshells','BOBO BENSON BAGWIBA','ahanamagloballinks@gmail.com','1','2018-05-08 08:27:29',0,'2018-05-23 13:29:16',1,'0','2018-05-18 11:14:39',0),(152,'asdasd','sdasd','asdasd','0','2018-05-18 11:56:20',1,'2018-05-18 11:56:20',0,'1','2018-05-18 11:56:52',1),(153,'xvcxcvxcv','sdasd','ANUMITADAS@GMAIL.COM','1','2018-05-18 11:57:48',1,'2018-05-18 11:57:49',0,'1','2018-05-18 11:57:58',1),(154,'zsxzxczc','cxvxcv','ANUMITADAS@GMAIL.COM','1','2018-05-18 12:27:44',1,'2018-05-18 12:27:44',0,'1','2018-05-18 12:27:51',1),(155,'You guys are so wonderful, first some of my items didn\'t get to me and all i got is SORRY....... Now my TV arrived and chain/ticket holder is missing again rite???? I have done my last bit with you guys, all my friends are going to know this story','Joy','joy.karmakar@indusnet.co.in','0','2018-05-22 07:35:22',1,'2018-05-22 07:35:22',0,'1','2018-05-22 07:35:33',1);
/*!40000 ALTER TABLE `stmd_testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_updatetypes`
--

DROP TABLE IF EXISTS `stmd_updatetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_updatetypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deletedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_updatetypes`
--

LOCK TABLES `stmd_updatetypes` WRITE;
/*!40000 ALTER TABLE `stmd_updatetypes` DISABLE KEYS */;
INSERT INTO `stmd_updatetypes` VALUES (1,'Receive from carrier','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(2,'Transfer for update','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(3,'Ship out - Air','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(4,'Receive locally - Air','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(5,'Dispatch Locally','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(6,'Ship Out - Sea','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(7,'Ship Out - DHL','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(8,'Receive Locally - Sea','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(9,'Warehouse to Pick Up Room - Air','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(10,'Warehouse to Pick Up Room - Sea','1','2018-05-16 14:56:29',0,'2018-05-16 14:56:29',0,'0','2018-05-16 14:56:29',0),(11,'Test123','1','2018-05-17 05:57:30',0,'2018-05-17 05:59:49',0,'1','2018-05-17 06:02:10',1),(12,'Test11122','1','2018-05-17 07:13:16',0,'2018-05-22 09:16:18',1,'0','2018-05-17 07:13:16',0);
/*!40000 ALTER TABLE `stmd_updatetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_user_cart`
--

DROP TABLE IF EXISTS `stmd_user_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_user_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` enum('autoparts','shopforme','buy_a_car','ship_my_car') NOT NULL DEFAULT 'shopforme',
  `cartContent` text NOT NULL,
  `savedForLater` enum('Y','N') NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `stmd_user_cart_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_user_cart`
--

LOCK TABLES `stmd_user_cart` WRITE;
/*!40000 ALTER TABLE `stmd_user_cart` DISABLE KEYS */;
INSERT INTO `stmd_user_cart` VALUES (24,7,'autoparts','{\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"38\",\"siteProductId\":\"48\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/GOOACC-Clips-Automotive-Furniture-Expansion\\/dp\\/B008427D88\\/ref=s\",\"itemName\":\"GOOACC Nylon Bumper\",\"itemDescription\":\"GOOACC Nylon Bumper\",\"itemYear\":\"2014\",\"itemMake\":\"Audi\",\"itemModel\":\"Model\",\"itemPrice\":\"200\",\"itemQuantity\":1,\"itemShippingCost\":\"5\",\"itemTotalCost\":205,\"siteProductImage\":null,\"type\":\"autopart\"}','N','2018-09-11 09:33:42'),(25,7,'autoparts','{\"storeId\":\"1\",\"siteCategoryId\":\"27\",\"siteSubCategoryId\":\"38\",\"siteProductId\":\"48\",\"websiteUrl\":\"https:\\/\\/www.amazon.com\\/GOOACC-Clips-Automotive-Furniture-Expansion\\/dp\\/B008427D88\\/ref=sr_1_cc_1?s=aps&ie=UTF8&qid=1536658364&sr=1-1-catcorr&keywords=auto\",\"itemName\":\"GOOACC Nylon Bumper\",\"itemDescription\":\"GOOACC Nylon Bumper\",\"itemYear\":\"2016\",\"itemMake\":\"Make\",\"itemModel\":\"model\",\"itemPrice\":\"250\",\"itemQuantity\":1,\"itemShippingCost\":\"5\",\"itemTotalCost\":255,\"siteProductImage\":null,\"type\":\"autopart\"}','N','2018-09-11 09:39:06');
/*!40000 ALTER TABLE `stmd_user_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_user_shipmentsettings`
--

DROP TABLE IF EXISTS `stmd_user_shipmentsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_user_shipmentsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `fieldType` varchar(50) NOT NULL,
  `fieldValue` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `stmd_user_shipmentsettings_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_user_shipmentsettings`
--

LOCK TABLES `stmd_user_shipmentsettings` WRITE;
/*!40000 ALTER TABLE `stmd_user_shipmentsettings` DISABLE KEYS */;
INSERT INTO `stmd_user_shipmentsettings` VALUES (3,3,'special_instruction','no special instr for now.'),(4,3,'remove_shoe_box','1'),(5,3,'original_box','1'),(6,3,'magazine_letter','1'),(7,3,'include_original_invoice','1'),(8,3,'declare_items_personal','1'),(9,3,'declare_items_gifts','1'),(10,3,'scan_invoice','1'),(11,3,'take_photo','1');
/*!40000 ALTER TABLE `stmd_user_shipmentsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_users`
--

DROP TABLE IF EXISTS `stmd_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueId` varchar(100) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `unitNumber` int(11) DEFAULT NULL,
  `title` varchar(6) NOT NULL DEFAULT '',
  `firstName` varchar(128) NOT NULL DEFAULT '',
  `lastName` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `contactNumber` varchar(50) NOT NULL,
  `cityId` int(11) NOT NULL,
  `stateId` int(11) DEFAULT NULL,
  `countryId` int(11) NOT NULL,
  `company` varchar(255) NOT NULL DEFAULT '',
  `dateOfBirth` date DEFAULT NULL,
  `referrer` varchar(255) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `activationToken` varchar(100) DEFAULT NULL,
  `profileImage` varchar(255) DEFAULT NULL,
  `verifiedCode` text COMMENT 'OTP',
  `otpSentDate` datetime DEFAULT NULL COMMENT 'Verification Code (OTP) sent date',
  `verifiedOn` datetime DEFAULT NULL COMMENT 'when verified',
  `registeredBy` enum('website','app') NOT NULL DEFAULT 'website',
  `referralCode` varchar(255) DEFAULT NULL,
  `referredBy` int(11) DEFAULT NULL,
  `deleted` enum('1','0') NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `deletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_users`
--

LOCK TABLES `stmd_users` WRITE;
/*!40000 ALTER TABLE `stmd_users` DISABLE KEYS */;
INSERT INTO `stmd_users` VALUES (1,'AO01','AE5',5,'f','Tathagata','sur','paulami.sarkar@indusnet.co.in','$2y$10$n/6s3mU6kvr9LfmhTZmTnePQLdwxL582bWRXafKRDNEL8q8gGnCpq','9234829384934',576,NULL,9,'int','2018-09-14',NULL,'1',NULL,'default.jpg',NULL,NULL,NULL,'website',NULL,NULL,'1',1,'2018-05-21 16:01:45',1,'2018-05-22 10:47:09',1,'2018-09-13 06:15:58'),(2,'NG02','0',0,'Mr.','ANUMITA','BANERJEE','ANUMITADAS@GMAIL.COM','$2y$10$KCoh.TIAVE0ZjFhgTUFnPeJLSMkZ6WWDsI6cNhVZyeivnZvaNIBZ6','9836888522',669,5,163,'INT','1984-06-13',NULL,'1',NULL,'default.jpg',NULL,NULL,NULL,'website',NULL,NULL,'0',1,'2018-05-22 10:45:56',1,NULL,NULL,NULL),(3,'NG05','NG15',15,'Mr.','S','Pahari','somnath.pahari@indusnet.co.in','$2y$10$5aZKMdwZBdAytAMZwS13JOumZpCAwI4DDfifI5Cfmk8xRMgu.lL46','123456',784,2,163,'INT','1987-09-18',NULL,'1',NULL,'1536646701_images.png',NULL,NULL,NULL,'website','994BC6',NULL,'0',1,'2018-06-01 07:59:29',1,'2018-06-08 10:03:14',NULL,NULL),(6,NULL,NULL,NULL,'Mr.','virat','kohli','virat@gmail.com','$2y$10$MSXhUIUuQQBW1kJdFTq92euf7xdhsOiQn26AB3htkUudopJsaLV7u','123456789',0,NULL,0,'123456',NULL,NULL,'1','A51DBEE0AE6521801378B5390F938814',NULL,NULL,NULL,NULL,'website',NULL,NULL,'0',1,'2018-07-20 12:32:03',NULL,NULL,NULL,NULL),(7,NULL,'NG1',1,'Mr.','Nduka','Udeh','n.udeh@aascargo.com','$2y$10$f3ZKIgRn8rk3uHjwG3tIfuY37ff1yYkVNilUcxXTG9PZxrsGV33Fa','123456',0,NULL,0,'American AirSea Cargo',NULL,NULL,'1','C37E51D3125DA238D24ED419580E52EE','default.jpg',NULL,NULL,NULL,'website','E4306B',NULL,'0',1,'2018-08-31 05:32:27',NULL,NULL,NULL,NULL),(8,NULL,NULL,NULL,'mr','Mithun','Sen','mithun.sen.bakup@indusnet.co.in','$2y$10$/i4JIGPR6Pr1eW4iZb1PU.Huib5Fmmt.XQv.ksBx93YXy1T6FU7u.','9874461917',0,NULL,0,'INT',NULL,NULL,'0','0079643D71BA4CFE7D2D29C70C89BEAC','default.jpg',NULL,NULL,NULL,'app','14DC62',NULL,'0',1,'2018-09-10 10:36:38',NULL,NULL,NULL,NULL),(9,NULL,NULL,NULL,'mr','Tathagata','sur','sur.tathagata@indusnet.co.in','$2y$10$vLe4Ju90SAVFUKcsFKXoOe8vrPrbaTHD8JpcDacVPKim/enj3v3rC','8961186006',0,NULL,0,'int',NULL,NULL,'0','3BE603F33D5128368F3A628DF10579F7','default.jpg',NULL,NULL,NULL,'app','7C3BE8',NULL,'0',1,'2018-09-11 11:18:15',NULL,NULL,NULL,NULL),(10,NULL,NULL,NULL,'mr','Tathagata','sur','sur.tathagata@gmail.com','$2y$10$UnF29uoWlPhOac4Ao7vNSOee/wYIdVv4gDh7xtu1hN7HogxL5JhtG','8961186006',0,NULL,0,'indusnet',NULL,NULL,'0','913558A3729A51A34CF2341EB194099B','default.jpg','95204','2018-09-13 05:17:23',NULL,'app','7445B6',NULL,'0',1,'2018-09-13 05:17:23',NULL,NULL,NULL,NULL),(11,NULL,NULL,NULL,'mr','Tathagata','sur','tathagata.sur@indusnet.co.in','$2y$10$Jx1gTdFvyOVgmvyDNqgh4O/GH3SQ26YN9t6aI4qyWWg7SYUEIIdqe','8961186006',0,NULL,0,'int',NULL,NULL,'0','2BD664E7096189DAF1B17E7377521C70','default.jpg','412051','2018-09-13 05:26:55',NULL,'app','825C7A',NULL,'0',1,'2018-09-13 05:26:55',NULL,NULL,NULL,NULL),(12,NULL,NULL,NULL,'mr','Tathagata','sur','tathagata.sur@indusnet.co.uk','$2y$10$SgFQMKdov3qiURumivkrA.kd5f11O1WOBggchAIGafrmtwYdN81Ui','8961186006',0,NULL,0,'int',NULL,NULL,'0','ECE010D0ABA35727D609FF64D3C92A01','default.jpg','745835','2018-09-13 05:30:30',NULL,'app','8DAAE2',NULL,'0',1,'2018-09-13 05:30:30',NULL,NULL,NULL,NULL),(13,NULL,'US2',2,'Mr.','Joy','Karmakar','karmakar.joy@gmail.com','$2y$10$dZCj72hEj7GgTF7jV5WFg.P66dXXi.vO.4zmBhc8pPxAQ9hpBKhMK','9831723608',0,NULL,0,'Indus net Tech',NULL,NULL,'1','ED66CBD0B18C16EAA0DF140B7C4C4958','default.jpg',NULL,NULL,NULL,'website','5674DA',NULL,'0',1,'2018-09-13 06:40:04',NULL,NULL,NULL,NULL),(14,NULL,'US1',1,'Mr.','Arun','Roy','joy.karmakar@indusnet.co.in','$2y$10$IZPnmxrYagiRwm1kCpVrsun3SjmzhKPlbU2MaapXvo2r5KD4glTBG','9831593608',0,NULL,0,'Indus Net',NULL,NULL,'1','FD902FE6AFC6C0B2ABA3A891F7B0686E','default.jpg',NULL,NULL,NULL,'website','4C54B4',NULL,'0',1,'2018-09-13 09:33:31',NULL,NULL,NULL,NULL),(15,NULL,NULL,NULL,'Mr.','Nduka','Udeh','n.udeh@shiptonaija.com','$2y$10$drv.Brq467JEtJqCJm1dQe4i0gLHoQu6fh0AfXvEwByKqSVYztVYa','+2348182500891',0,NULL,0,'AASC',NULL,NULL,'0','39BDC27CD7D41B0C4EBCE6AD4AD66AC2','default.jpg',NULL,NULL,NULL,'website','A24CBD',NULL,'0',1,'2018-09-13 10:53:16',NULL,NULL,NULL,NULL),(16,NULL,NULL,16,'Mr.','Mithun','Sen','mithun.sen@indusnet.co.in','$2y$10$ZnfNjjN4UeQDEnAozJg4lul0k8qQmBpyq/Oh3r2qUNxVH4FS2zNdG','8820731284',0,NULL,0,'INT',NULL,NULL,'0','9EC275EE0F28E647C499722A4F3C4066','default.jpg','342223','2018-09-18 11:16:57',NULL,'app','A1804A',NULL,'0',1,'2018-09-18 11:16:57',NULL,NULL,NULL,NULL),(17,NULL,NULL,16,'Mr.','Mithun','Sen','mithun.sen1@indusnet.co.in','$2y$10$U2UYPuwOuGif7YDSRc36aeue/PcENrecVmf5H1Q2mYNiCH7MrTyFi','1234567890',0,NULL,0,'INT',NULL,NULL,'0','87CA25CD4341DE03A702B32D526C81DB','default.jpg','705671','2018-09-18 11:44:58',NULL,'app','EF46FD',NULL,'0',1,'2018-09-18 11:44:58',NULL,NULL,NULL,NULL),(18,NULL,'NG17',17,'Mr.','Mithun','Sen','mithun.sen2@indusnet.co.in','$2y$10$6dLKFiW7DywXPYX6SGIk6eILZ.puiSykMcsKmt5Vs2MrkP73GfCti','1234567890',0,NULL,0,'INT',NULL,NULL,'1',NULL,'default.jpg',NULL,NULL,'2018-09-18 12:02:17','app','250E05',NULL,'0',1,'2018-09-18 12:00:43',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `stmd_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stmd_users_notification`
--

DROP TABLE IF EXISTS `stmd_users_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmd_users_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` text NOT NULL,
  `senderId` int(11) NOT NULL,
  `sentOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `stmd_users_notification_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `stmd_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stmd_users_notification`
--

LOCK TABLES `stmd_users_notification` WRITE;
/*!40000 ALTER TABLE `stmd_users_notification` DISABLE KEYS */;
INSERT INTO `stmd_users_notification` VALUES (1,1,'<p>Test</p>',1,'2018-05-23 17:32:57'),(2,1,'<p>Test Message</p>\r\n\r\n<p>Hello</p>\r\n\r\n<p>fdfdssdfsd</p>',1,'2018-05-23 18:28:20'),(3,1,'<p>tghfghfgh</p>',1,'2018-05-23 18:28:26'),(4,2,'<p>Test User</p>',1,'2018-05-23 13:09:01'),(5,1,'<p>Test Message</p>\r\n\r\n<p>&nbsp;</p>',1,'2018-05-23 13:09:15'),(6,1,'<p>Test</p>',1,'2018-05-24 05:25:51'),(7,1,'<p>Test</p>',1,'2018-05-24 05:26:20'),(8,1,'<p>Test</p>',1,'2018-05-24 05:29:27'),(9,1,'<p>Test</p>',1,'2018-05-24 05:30:18'),(10,1,'<p>uiyuiyuiyu</p>',1,'2018-05-24 05:32:13'),(11,1,'<p>fdsfsdf</p>',1,'2018-05-24 05:33:08'),(12,1,'<p>hgjghjghj</p>',1,'2018-06-04 14:40:40'),(13,1,'<p>hgjghjghj</p>',1,'2018-06-04 14:41:22'),(14,3,'<p>Test Email</p>',1,'2018-09-13 05:52:11'),(15,3,'<p>Test Email</p>',1,'2018-09-13 05:56:46'),(16,3,'<p>Test Email</p>',1,'2018-09-13 05:57:05'),(17,3,'<p>Test Email</p>',1,'2018-09-13 05:57:37');
/*!40000 ALTER TABLE `stmd_users_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `stmd_view_shipment`
--

DROP TABLE IF EXISTS `stmd_view_shipment`;
/*!50001 DROP VIEW IF EXISTS `stmd_view_shipment`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `stmd_view_shipment` AS SELECT 
 1 AS `id`,
 1 AS `userId`,
 1 AS `warehouseId`,
 1 AS `shipmentType`,
 1 AS `fromCountry`,
 1 AS `fromCountryName`,
 1 AS `fromState`,
 1 AS `fromStateName`,
 1 AS `fromCity`,
 1 AS `fromCityName`,
 1 AS `fromZipCode`,
 1 AS `fromName`,
 1 AS `fromCompany`,
 1 AS `fromAddress`,
 1 AS `fromPhone`,
 1 AS `fromEmail`,
 1 AS `toCountry`,
 1 AS `toCountryName`,
 1 AS `toState`,
 1 AS `toStateName`,
 1 AS `toCity`,
 1 AS `toCityName`,
 1 AS `toZipCode`,
 1 AS `toName`,
 1 AS `toCompany`,
 1 AS `toAddress`,
 1 AS `toPhone`,
 1 AS `toEmail`,
 1 AS `shipmentStatus`,
 1 AS `firstReceived`,
 1 AS `hideFromCustomer`,
 1 AS `totalItemCost`,
 1 AS `totalWeight`,
 1 AS `totalShippingCost`,
 1 AS `totalClearingDuty`,
 1 AS `isDutyCharged`,
 1 AS `totalInsurance`,
 1 AS `totalTax`,
 1 AS `totalOtherCharges`,
 1 AS `totalDiscount`,
 1 AS `totalCost`,
 1 AS `prepaid`,
 1 AS `paymentStatus`,
 1 AS `paymentMethodId`,
 1 AS `paymentMethod`,
 1 AS `paymentReceivedOn`,
 1 AS `shippingMethod`,
 1 AS `warehouseCountry`,
 1 AS `warehouseCountryName`,
 1 AS `warehouseState`,
 1 AS `warehouseStateName`,
 1 AS `warehouseCity`,
 1 AS `warehouseCityName`,
 1 AS `storageCharge`,
 1 AS `storageChargeModifiedBy`,
 1 AS `allowedPackaging`,
 1 AS `storageChargeModifiedOn`,
 1 AS `deleted`,
 1 AS `createdBy`,
 1 AS `createdByType`,
 1 AS `createdOn`,
 1 AS `modifiedBy`,
 1 AS `modifiedOn`,
 1 AS `deletedBy`,
 1 AS `deletedOn`,
 1 AS `userUnit`,
 1 AS `customerName`,
 1 AS `customerEmail`,
 1 AS `maxStorageDate`,
 1 AS `shippingMethodId`,
 1 AS `shippingName`,
 1 AS `couponCodeApplied`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `stmd_view_shipmentdetails`
--

DROP TABLE IF EXISTS `stmd_view_shipmentdetails`;
/*!50001 DROP VIEW IF EXISTS `stmd_view_shipmentdetails`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `stmd_view_shipmentdetails` AS SELECT 
 1 AS `shipmentId`,
 1 AS `deliveryId`,
 1 AS `packageId`,
 1 AS `itemCategoryId`,
 1 AS `itemCategoryName`,
 1 AS `itemSubCategoryId`,
 1 AS `itemSubCategoryName`,
 1 AS `itemProductId`,
 1 AS `itemProductName`,
 1 AS `storeId`,
 1 AS `storeName`,
 1 AS `itemName`,
 1 AS `itemWebsiteUrl`,
 1 AS `itemWeight`,
 1 AS `itemOptions`,
 1 AS `itemDescription`,
 1 AS `itemMake`,
 1 AS `itemModel`,
 1 AS `itemNote`,
 1 AS `snapshotOpt`,
 1 AS `itemSnapshotImage`,
 1 AS `itemType`,
 1 AS `itemQuantity`,
 1 AS `itemPrice`,
 1 AS `itemShippingCost`,
 1 AS `itemTotalCost`,
 1 AS `packageType`,
 1 AS `deliveredOn`,
 1 AS `deliveryCompanyId`,
 1 AS `deliveryCompany`,
 1 AS `dispatchCompanyId`,
 1 AS `dispatchCompany`,
 1 AS `deliveryLength`,
 1 AS `deliveryWidth`,
 1 AS `deliveryHeight`,
 1 AS `deliveryWeight`,
 1 AS `deliveryChargeableWeight`,
 1 AS `totalItemCost`,
 1 AS `shippingCost`,
 1 AS `clearingDutyCost`,
 1 AS `isDutyCharged`,
 1 AS `inventoryCharge`,
 1 AS `storageCharge`,
 1 AS `otherChargeCost`,
 1 AS `totalCost`,
 1 AS `shippingMethodId`,
 1 AS `defaultCurrencyCode`,
 1 AS `paidCurrencyCode`,
 1 AS `exchangeRate`,
 1 AS `tracking`,
 1 AS `trackingLock`,
 1 AS `tracking2`,
 1 AS `deliveryNotes`,
 1 AS `snapshot`,
 1 AS `snapshotRequestedOn`,
 1 AS `recount`,
 1 AS `recountRequestedOn`,
 1 AS `reweigh`,
 1 AS `reweighRequestedOn`,
 1 AS `wrongInventory`,
 1 AS `wrongInventoryMessage`,
 1 AS `received`,
 1 AS `deleted`,
 1 AS `deletedByEmail`,
 1 AS `packageDeleted`,
 1 AS `deliveryAddedBy`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `stmd_view_shipmentdetails_20-9-18`
--

DROP TABLE IF EXISTS `stmd_view_shipmentdetails_20-9-18`;
