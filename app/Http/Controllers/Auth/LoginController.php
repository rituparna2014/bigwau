<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Hash;
use Auth;
use DB;
use App\Model\UserAdmin;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/administrator/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function logout() {
        /*         * *************** Insert log into admin log table ********************* */
//        $adminLog = new UserAdmin;
//        $adminLogOut = $adminLog->userLogInsert(Auth::user()->id, 'Update');

        session()->put('admin_session_user_id', '');
        session()->put('admin_session_user_firstname', '');
        session()->put('admin_session_user_lastname', '');
        session()->put('admin_session_user_email', '');
        session()->put('admin_session_user_userType', '');
        session()->put('admin_session_user_userTypeName', '');

        /* ---------------------------------------------------------------------- */
        Auth::logout(); \Session::flush();
        return Redirect::to($this->redirectTo)->with('successMessage', 'You have successfully logged out.'); // redirection to login screen
    }

    /**
     *  METHOD TO LOG IN FROM ADMIN
     */
    public function doLogin(Request $request) {

        // Creating Rules for Email and Password

        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|min:6');


        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form

        if ($validator->fails()) {

            return Redirect::to($this->redirectTo)->withErrors($validator) // send back all errors to the login form
                            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password'),
                'status' => '1',
            );

            $remember_me = $request->has('remember');
            if ($remember_me == true) {
                setcookie('bigwau_adminUser_username', Input::get('email'), time() + (86400 * 30), "/"); // 86400 = 1 day
                setcookie('bigwau_adminUser_password', Input::get('password'), time() + (86400 * 30), "/"); // 86400 = 1 day
                setcookie('bigwau_adminUser_checkbox_remember', 'checked', time() + (86400 * 30), "/"); // 86400 = 1 day
            } else {
                setcookie('bigwau_adminUser_username', '', time() + (86400 * 30), "/"); // 86400 = 1 day
                setcookie('bigwau_adminUser_password', '', time() + (86400 * 30), "/"); // 86400 = 1 day
                setcookie('bigwau_adminUser_checkbox_remember', '', time() + (86400 * 30), "/"); // 86400 = 1 day
            }
            // attempt to do the login
            if (Auth::guard('admin')->attempt($userdata)) {
             
                $user = auth()->guard('admin')->user();

                $request->session()->put('admin_session_user_id', $user->id);
                $request->session()->put('admin_session_user_firstname', $user->firstName);
                $request->session()->put('admin_session_user_lastname', $user->lastName);
                $request->session()->put('admin_session_user_email', $user->email);
                $request->session()->put('admin_session_user_userType', $user->userType);

                $userType = DB::table('adminusertype')
                        ->where('id', $user->userType)
                        ->first();
                
                $request->session()->put('admin_session_user_userTypeName', $userType->userTypeName);

                /*                 * *************** Insert log into admin log table ********************* */
                $adminLog = new \App\Model\Adminuserlog;
                $adminLog = $adminLog->userLogInsert($user->id, 'Insert');
                /* ---------------------------------------------------------------------- */

                return Redirect::intended('/administrator/dashboard')->with('successMessage', 'You have successfully logged in.');
            } else {
                // validation not successful, send back to form
                return Redirect::to($this->redirectTo)->with('errorMessage', 'Email Address or Password does not match.');
            }
        }
    }

}
