<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\UserAdmin;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Redirect;
use Hash;
use Config;
use Auth;
use customhelper;
use App\Model\Emailtemplate;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo = '/administrator/login';

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    //protected $redirectTo = '/administrator/password/reset';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:admin');
        Config::set("auth.defaults.passwords","admins");
    }
	
	
	 /**
     * METHOD TO RESET ADMIN PASSWORD
     */
    public function resetauthenticatedView(){
        $data['title'] = "Administrative Panel :: Profile Management :: Change Password";
        $data['pageTitle'] = "Profile Management :: Change Password";
            
        return view('Administrator.passwords.reset', $data);
    }
	 /**
     * Reset password for logged in users.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetAuthenticated(Request $request)
    {
        $this->validate($request, [
            'old_password'     => 'required',
            'new_password'     => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ]);
        
        $data = $request->all();
 
        $user = UserAdmin::find(auth()->user()->id);
        $checkResult = Hash::check($data['old_password'], $user->password);
        if(!$checkResult || empty($checkResult)){
             return back()->with('errorMessage','The specified password does not match the old password');
        }else{
            // code to update password
            $credentials['email'] = auth()->user()->email;
            $credentials['password'] = $data['confirm_password'];

            $user = auth()->user();
            $user->update(['password' => Hash::make($credentials['password'])]);
    
            return Redirect::to('administrator/password/reset')->with('successMessage', 'Password have changed successfuly.');
            //return $user->save() ? Password::PASSWORD_RESET : 'Error';
        }
    }

    public function reset(Request $request){


       
        $validator = \Validator::make($request->all(), [
                'password'     => 'required|min:6',
                'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {

            $values['email'] = $request->email;
            return \Redirect::to('administrator/password/reset/'.$request->token)->withErrors($validator->errors())->withInput($values);
        }

        else { 
            $userdata = array(
                'email' => $request->email,
                'password' => $request->password,
                'status' => '1',
            );



            $user = UserAdmin::where('email', $request->email)->first();
            $user->password = Hash::make($request->password);
            $user->passwordLastChanged = Config::get('constants.CURRENTDATE');
            $user->save();

            setcookie('bigwau_adminUser_username', $request->email, time() + (86400 * 30), "/"); // 86400 = 1 day
            setcookie('bigwau_adminUser_password', $request->password, time() + (86400 * 30), "/"); // 86400 = 1 day
            setcookie('bigwau_adminUser_checkbox_remember', 'checked', time() + (86400 * 30), "/"); // 86400 = 1 day
            $request->session()->put('admin_session_user_id', $user->id);
            $request->session()->put('admin_session_user_firstname', $user->firstName);
            $request->session()->put('admin_session_user_lastname', $user->lastName);
            $request->session()->put('admin_session_user_email', $user->email);
            $request->session()->put('admin_session_user_userType', $user->userType);

            $userType = \DB::table('adminusertype')
                    ->where('id', $user->userType)
                    ->first();

            $request->session()->put('admin_session_user_userTypeName', $userType->userTypeName); 
            
            $PasswordLogTable = \DB::table('passwordresetlog')->where('email', $request->email)->delete();
            if (Auth::guard('admin')->attempt($userdata)) {
               
                /* ++++++++++ email functionality ++++++++ */
                $emailTemplate = Emailtemplate::where('templateKey', 'successful_reset_password')->first();
                $replace['[NAME]'] = $user->firstName . " " . $user->lastName;
                $replace['[PASSWORD]'] = $request->password;
                $to = $user->email;
                customhelper::SendMail($emailTemplate, $replace, $to);
                /* ++++++++++ end of email functionality ++++ */

                return redirect('/administrator/dashboard')->with('successMessage', 'Your password has successfully updated.');
            }
            //dd($user);
        }
    }
}
