<?php

namespace App\Http\Controllers\Frontend;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\helpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Log;
use Excel;
use Mail;
use Illuminate\Routing\Route;
use customhelper;
use App\Model\Buyers;
use App\Model\Sellers;
use App\Model\Service;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    public function index(Request $request) {
        $data = array();
        $data['title'] = "Bigwau Home";

        $data['contentTop'] = array('breadcrumbText' => 'Dashboard', 'contentTitle' => 'Dashboard', 'pageInfo' => '');
        $data['pageTitle'] = "Bigwau Home";
        return view('Frontend.home.index', $data);
    }

   

    

 

}
