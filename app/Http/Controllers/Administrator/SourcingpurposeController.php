<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sourcingpurpose;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;
use DB;
class SourcingpurposeController extends Controller {

    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index() {

        //echo '77';exit;
        $data = array();

        $sourcingpurposeObj = new Sourcingpurpose();

        if (\Request::isMethod('post')) {
            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            
                        
            \Session::forget('SOURCINGPURPOSE');
            \Session::push('SOURCINGPURPOSE.searchData', $searchData);
            \Session::push('SOURCINGPURPOSE.searchDisplay', $perpage);
            \Session::push('SOURCINGPURPOSE.field', $sortField);
            \Session::push('SOURCINGPURPOSE.type', $sortOrder);

            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.SourcingPurposes'), Auth::user()->userType); // call the helper function
            if($findRole['canView'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
           
        } 
        else {           
                $sortField = \Session::get('SOURCINGPURPOSE.field');
                $sortType = \Session::get('SOURCINGPURPOSE.type');
                $perpage = \Session::get('SOURCINGPURPOSE.searchDisplay');
                $searchData = \Session::get('SOURCINGPURPOSE.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.SourcingPurposes'), Auth::user()->userType); // call the helper function
                
                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
        }
        $param = array();
        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        
        $records = $sourcingpurposeObj->getData($param);

        $data['param'] = $param;
        $data['page'] = $records->currentPage();
        $data['records'] = $records;       
        $data['forntendUrl'] = Config::get('constants.frontendUrl');
        
        $data['pageTitle'] = 'Sourcing Purpose';
        $data['title'] = "Sourcing Purpose :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => 'Sourcing Purpose Management', 'contentTitle' => 'Sourcing Purpose', 'pageInfo' => 'This section allows you to manage sourcing purposes');
        

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.sourcingpurpose.index', $data);
    }

    public function addeditsourcingpurposes($id = -1, $page = 1) {

        $sourcingpurposeObj = new Sourcingpurpose();
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $sourcingpurposeObj->find($id);
            $data['record'] = $record;
        } else {
            $data['action'] = 'Add';
            $data['record'] = $sourcingpurposeObj;
        }
        
        $data['page'] = $page;
        
        $data['pageTitle'] = $data['action'] . ' Sourcing Purpose';
        $data['title'] = "Sourcing Purpose :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Sourcing Purpose', 'Add/Edit Sourcing Purpose'), 'contentTitle' => 'Sourcing Purpose', 'pageInfo' => 'This section allows you to manage sourcing purpose');
       
        return view('Administrator.sourcingpurpose.addeditpage', $data);
    }

    public function addeditsourcingpurposesrecord($id = -1, $page = '-1', Request $request) {

        $sourcingpurposeObj = new Sourcingpurpose();
        
        if ($id != '-1') {
            $sourcingpurposeObj = $sourcingpurposeObj->find($id);
            
        }
        
        //if ($id == '-1')
        
        $sourcingpurposeObj->title   = $request->input('titles');
        $sourcingpurposeObj->description = ($request->input('description'));
        
        if ($sourcingpurposeObj->save())
            return redirect(route('sourcingpurposes'))->with('successMessage', 'Information saved successfuly.');
    }

    public function editsourcingpurposesstatus($id = -1, $status) {
        $sourcingpurposeObj = new Sourcingpurpose();
        if ($id != -1)
            $sourcingpurposeObj = $sourcingpurposeObj->find($id);
            $sourcingpurposeObj->status = $status;

        if ($sourcingpurposeObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function deleterecord($id = -1,$page = 1) {

        $sourcingpurposeObj = new Sourcingpurpose();

        if ($id != '-1') {
            $sourcingpurposeObj = $sourcingpurposeObj->find($id);
            if ($sourcingpurposeObj->delete())
                return redirect(route('sourcingpurposes'))->with('successMessage', 'Information deleted successfully.');
        }
    }

}
