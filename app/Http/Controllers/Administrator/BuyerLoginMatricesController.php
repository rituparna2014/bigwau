<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAdmin;
use App\libraries\dbHelpers;
//use App\libraries\helpers;
//use App\libraries\dbHelpers;
use Auth;
use App\Model\Buyerloginmatrix;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;   
use customhelper;
use Config;

class BuyerLoginMatricesController extends Controller {

    public $_perPage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    
    public function index(Route $route, Request $request, $id) {
        //echo 'hiii';die;
        $data = array();
        //echo Config::get('constants.PermissionMenuIds.Buyers');die;
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.BuyerLoginMatrices'), Auth::user()->userType); // call the helper function
            
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;

        }

        $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

        $field = \Input::get('field', 'id');
        $type = \Input::get('type', 'desc');

        $param['field'] = $field;
        $param['type'] = $type;
        $param['searchDisplay'] = $searchDisplay;

        $sort = array(
            'date' => array('current' => 'sorting'),
        );
        
        
        $notifyRecord = Buyerloginmatrix::getList($param, $id);
        #print_r($userRecord);
        #dd($notifyRecord);
        $data['notifyRecord'] = $notifyRecord;
        $data['getId'] = $id;
        $data['title'] = "Buyer Login Matrices :: ADMIN - Bigwau";
        $data['pageTitle'] = "View Buyer Login Matrices";
        $data['contentTop'] = array('breadcrumbText'=>'Buyer Login Matrices','contentTitle'=>'View Buyer Login Matrices','pageInfo'=>'This section allows you to view buyer Login Matrices');
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');
        return view('Administrator.loginmatrices.buyerlogin',$data);

    }

    



}
