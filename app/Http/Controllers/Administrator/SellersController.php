<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Model\Country;
use App\Model\Buyers;
use App\Model\Sellers;
use App\Model\Permission;
use App\Model\UserPermission;
use App\Model\UserTypeDefaultPermission;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Hash;
use Illuminate\Routing\Route;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Model\Adminmenuuserpermission;
use customhelper;
use App\Model\Emailtemplate;
use App\Model\Subscription;
use App\Model\Subscriptionrequest;
use App\Model\Subscriptionhistories;

class SellersController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Sellers'), Auth::user()->userType); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SELLERDATA');
            \Session::push('SELLERDATA.searchDisplay', $searchDisplay);
            \Session::push('SELLERDATA.field', $field);
            \Session::push('SELLERDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;



            // Search fields if any
            if ($request->has('search_firstName')) {
                $param['search_firstName'] = $request->search_firstName;
                \Session::push('SELLERDATA.search_firstName', $request->search_firstName);
            }
            if ($request->has('search_lastName')) {
                $param['search_lastName'] = $request->search_lastName;
                \Session::push('SELLERDATA.search_lastName', $request->search_lastName);
            }
            if ($request->has('search_companyName')) {
                $param['search_companyName'] = $request->search_companyName;
                \Session::push('SELLERDATA.search_companyName', $request->search_companyName);
            }
            if ($request->has('search_status')) {
                $param['search_status'] = $request->search_status;
                \Session::push('SELLERDATA.search_status', $request->search_status);
            }
            if ($request->has('search_rangedate')) {
                $param['search_rangedate'] = $request->search_rangedate;
                \Session::push('SELLERDATA.search_rangedate', $request->search_rangedate);
                if ($param['search_rangedate'] == 'custom') {
                    $param['search_reservation'] = $request->search_reservation;
                    \Session::push('SELLERDATA.search_reservation', $request->search_reservation);
                }
            }
           
            #dd(session()->all());
        } else {
            $sortField = \Session::get('SELLERDATA.field');
            $sortType = \Session::get('SELLERDATA.type');
            $searchDisplay = \Session::get('SELLERDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'firstName' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'sellerType' => array('current' => 'sorting'),
            'companyName' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );
        
        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        

        //\DB::enableQueryLog();

        $sellersData = Sellers::getSellersList($param);
        /*$query = \DB::getQueryLog();
        print_r($query);exit;*/

     

        $data['title'] = "Administrative Panel :: Sellers";
        $data['contentTop'] = array('breadcrumbText' => 'Sellers', 'contentTitle' => 'Sellers', 'pageInfo' => 'This section allows you to manage seller profile');
        $data['pageTitle'] = "Sellers";
        $data['page'] = $sellersData->currentPage();
        $data['sellersData'] = $sellersData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        
        return view('Administrator.sellers.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Sellers'), Auth::user()->userType); // call the helper function

        $data['title'] = "Administrative Panel :: Sellers";
        $data['contentTop'] = array('breadcrumbText' => array('Sellers','Add/Edit Seller'), 'contentTitle' => 'Sellers', 'pageInfo' => 'This section allows you to manage seller profile');
        $data['pageTitle'] = "Sellers";
        $data['page'] = !empty($page) ? $page : '1';
       
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        $data['subscriptionList'] = Subscription::select('term', 'amount')->where('status', '1')->where('deleted', '0')->get(); 

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $seller = Sellers::find($id);
           
            $data['seller'] = $seller;
            
            
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
           
            
        }
        return view('Administrator.sellers.addedit', $data);
    }

    /*     * Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $seller = new Sellers;
        
        $validator = Validator::make($request->all(), [
                    'firstName' => 'required:' . $seller->table . ',firstName,'. $id,
                    'lastName' => 'required:' . $seller->table . ',lastName,'. $id,
                    'buyerId' => 'required:' . $seller->table . ',buyerId,'. $id,
                    'buyerType' => 'required:' . $seller->table . ',buyerType,'. $id,
                    'companyName' => 'required|min:3|max:100:' . $seller->table . ',companyName,'. $id,
                    'email' => 'required|email|unique:'.$seller->table .  ',email,'. $id,
                    'phone' => 'required|numeric:' . $seller->table . ',phone,'. $id,
                    'IBAN' => 'required:' . $seller->table . ',IBAN,'. $id,
                    'BIC' => 'required:' . $seller->table . ',BIC,'. $id,
                    'bankOwnerName' => 'required:' . $seller->table . ',bankOwnerName,'. $id,
                    'bankOwnerAddress' => 'required:' . $seller->table . ',bankOwnerAddress,'. $id,
                    'fee' => 'required:' . $seller->table . ',fee,'. $id,
                    //'warehouseid' => 'required|numeric:' . $userAdmin->table . ',warehouseid,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $seller = Sellers::find($id);
                $seller->updatedBy = Auth::user()->id;
                $seller->updatedOn = Config::get('constants.CURRENTDATE');
            }
            
            $seller->firstName = $request->firstName;
            $seller->lastName = $request->lastName;
            $seller->sellerId = $request->buyerId;
            $seller->sellerType = $request->buyerType;
            $seller->companyName = $request->companyName;
            $seller->email = $request->email;
            $rawPassword = $request->password;
            $seller->password = Hash::make($rawPassword);
            /*$buyer->password = Hash::make($rawPassword);
            $buyer->remember_token = Hash::make($rawPassword);*/
            $seller->profileDescription = $request->profileDescription;
            $seller->birthday = \Carbon\Carbon::parse($request->birthday)->format('Y-m-d');
            $seller->phone = $request->phone;
            $seller->timezone = $request->timezone;
            $seller->nationality = $request->nationality;
            $seller->profession = $request->profession;
            $seller->IBAN = $request->IBAN;
            $seller->BIC = $request->BIC;
            $seller->bankOwnerName = $request->bankOwnerName;
            $seller->bankOwnerAddress = $request->bankOwnerAddress;
            $seller->annualIncome = $request->annualIncome;
            $seller->fee = $request->fee;
            $seller->paymentStatus = 'pending';
            if($seller->subscription_end_date < date('Y-m-d')){
                $seller->subscription_start_date = date('Y-m-d');
                $time = strtotime(date("Y-m-d"));
                $duration = ($seller->fee == '500')?"+1 month":"+1 year";
                $final = date("Y-m-d", strtotime($duration, $time));
                $seller->subscription_end_date = $final;

                /*Insert into subscriptionhistory table to track the histories of the seller*/
                
                $subscriptionhistory = new Subscriptionhistories;

                $subscriptionhistoryUp = Subscriptionhistories::find($id);
                $subscriptionhistoryUp->subscriptionStatus = 'inactive'; 
                $subscriptionhistoryUp->save(); 

                $subscriptionhistory->sellerId = $id;
                $subscriptionhistory->amount = $request->fee;
                $subscriptionhistory->startDate = date("Y-m-d");
                $subscriptionhistory->endDate = $final;
                $subscriptionhistory->subscriptionStatus = 'active';
                $subscriptionhistory->createdDate = date('Y-m-d');
                $subscriptionhistory->save();
                $subscriptionhistoryId = $subscriptionhistory->id;
            }else{
                $subscrionRequest = new Subscriptionrequest;
                $subscrionRequest->sellerId = $id;
                $subscrionRequest->subscription_request_date = date("Y-m-d");
                $subscrionRequest->subscription_request_amount = $request->fee;
                $subscrionRequest->payment_status = 'pending';
                $subscrionRequest->save();

            }
            
            $seller->save();
            $sellerId = $seller->id;
            return redirect('/administrator/sellers')->with('successMessage', 'Seller Information saved successfuly.');
        }
    }

    /** Add
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function adddata($page, Request $request) {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $seller = new Sellers;

        $validator = Validator::make($request->all(), [
                    'firstName' => 'required:' . $seller->table . ',firstName,',
                    'lastName' => 'required:' . $seller->table . ',lastName,',
                    'buyerId' => 'required:' . $seller->table . ',buyerId,',
                    'buyerType' => 'required:' . $seller->table . ',buyerType,',
                    'companyName' => 'required|min:3|max:100:' . $seller->table . ',companyName,',
                    'email' => 'required|email|unique:'.$seller->table .  ',email,',
                    'password' => 'required:'.$seller->table .  ',password,',
                    'phone' => 'required|numeric:' . $seller->table . ',phone,',
                    'IBAN' => 'required:' . $seller->table . ',IBAN,',
                    'BIC' => 'required:' . $seller->table . ',BIC,',
                    'bankOwnerName' => 'required:' . $seller->table . ',bankOwnerName,',
                    'bankOwnerAddress' => 'required:' . $seller->table . ',bankOwnerAddress,',
                    'fee' => 'required:' . $seller->table . ',fee,',
                    //'warehouseid' => 'required|numeric:' . $userAdmin->table . ',warehouseid,' . $id,
        ]);

        if ($validator->fails()) {
           
            $values['firstName'] = $request->firstName;
            $values['lastName'] = $request->lastName;
            $values['sellerId'] = $request->buyerId;
            $values['sellerType'] = $request->buyerType;
            $values['companyName'] = $request->companyName;           
            $values['email'] = $request->email;
            $values['password'] = $request->password;
            $values['phone'] = $request->phone;
            $values['IBAN'] = $request->IBAN;
            $values['BIC'] = $request->BIC;
            $values['bankOwnerName'] = $request->bankOwnerName;
            $values['bankOwnerAddress'] = $request->bankOwnerAddress;
            $values['fee'] = $request->fee;
            
            
            return redirect()->back()->withErrors($validator->errors())->withInput($values);
        } else {
            
            $seller->firstName = $request->firstName;
            $seller->lastName = $request->lastName;
            $seller->sellerId = $request->buyerId;
            $seller->sellerType = $request->buyerType;
            $seller->companyName = $request->companyName;
            $seller->email = $request->email;
            $rawPassword = $request->password;
            $seller->password = Hash::make($rawPassword);
            $seller->remember_token = Hash::make($rawPassword);
            $seller->profileDescription = $request->profileDescription;
            $seller->birthday = \Carbon\Carbon::parse($request->birthday)->format('Y-m-d');
            $seller->phone = $request->phone;
            $seller->timezone = $request->timezone;
            $seller->nationality = $request->nationality;
            $seller->profession = $request->profession;
            $seller->IBAN = $request->IBAN;
            $seller->BIC = $request->BIC;
            $seller->bankOwnerName = $request->bankOwnerName;
            $seller->bankOwnerAddress = $request->bankOwnerAddress;
            $seller->annualIncome = $request->annualIncome;
            $seller->fee = $request->fee;
            $seller->subscription_start_date = date("Y-m-d");
            if($request->fee == '500')
            {
                $time = strtotime(date("Y-m-d"));
                $final = date("Y-m-d", strtotime("+1 month", $time));
                $seller->subscription_end_date = $final;
            }
            else if($request->fee == '1500'){
                $time = strtotime(date("Y-m-d"));
                $final = date("Y-m-d", strtotime("+1 year", $time));
                $seller->subscription_end_date = $final;
            }
            $seller->address = $request->address;            
            
            $seller->createdBy = Auth::user()->id;
            $seller->status = 1;            
            $seller->save();
            $sellerId = $seller->id;

            /*Insert into subscriptionhistory table to track the histories of the seller*/

            $subscriptionhistory = new Subscriptionhistories;
            $subscriptionhistory->amount = $request->fee;
            $subscriptionhistory->sellerId = $seller->id;
            $subscriptionhistory->startDate = date("Y-m-d");
            $subscriptionhistory->endDate = $final;
            $subscriptionhistory->subscriptionStatus = 'active';
            $subscriptionhistory->createdDate = date('Y-m-d');
            $subscriptionhistory->save();
            $subscriptionhistoryId = $subscriptionhistory->id;

            return redirect('/administrator/sellers')->with('successMessage', 'Buyer added successfuly.');
        }
    }

    // Generate random password
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Sellers::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/sellers/?page=' . $page)->with('successMessage', 'Status changed successfully.');
            } else {
                return \Redirect::to('administrator/sellers/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/sellers/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to change password
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changepassword($id, $page, Request $request) {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = $id;

        if (\Request::isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'new_password'     => 'required|min:6',
                'confirm_password' => 'required|same:new_password',
                ]);

                if ($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }    
                else { 
                    $user = Sellers::find($request->id);
                    $credentials['password'] = $request->confirm_password;
                    $user->password = Hash::make($credentials['password']);
                    $user->passwordLastChanged = Config::get('constants.CURRENTDATE');
                    $user->save();
                    return response()->json(['success'=>'Password has changed successfuly.']);
                }
            }
        return view('Administrator.sellers.changepassword', $data);
    }


    /**
     * Method used to Export All with selected fields
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function exportall($page, Request $request) {


        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/sellers/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {
            $sellerDataExcel = Sellers::getSellersListExcel($request->selectall)->get();

            //dd($adminUserDataExcel);

            ob_end_clean();
            ob_start();
            Excel::create("Sellers_" . Carbon::now(), function($excel) use($sellerDataExcel) {
                $excel->sheet('Sheet 1', function($sheet) use($sellerDataExcel) {
                    $sheet->fromArray($sellerDataExcel);
                });
            })->export('xlsx');
            ob_flush();
            return \Redirect::to('administrator/sellers/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

    /**
     * Method used to Export selected with selected fields
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function exportSelected($page, Request $request) {
        if (\Request::isMethod('post')) {
            $sellerDataExcelSelected = Sellers::getSellerListExcelSelected($request->selectedField, $request->selected)->get(); 
            
            $excelName = "Sellers_" . Carbon::now();
            $path = public_path('export/' . $excelName);

            ob_end_clean();
            ob_start();
            Excel::create($excelName, function($excel) use($sellerDataExcelSelected) {
                $excel->sheet('Sheet 1', function($sheet) use($sellerDataExcelSelected) {
                    $sheet->fromArray($sellerDataExcelSelected);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            
            return response()->json(['path' => $creatingPath]);
            
        }
    }

    /**
     * Method used to show the Role for Admin Users
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function editRole($id = 0) {

        
        if (!empty($id)) {
           
            $userAdminDetails = UserAdmin::find($id);
         
           
        } else {
            dd('No User Found! Please Click on the Back button of the browser');
        }

        

        if ($userAdminDetails->id =='') {
            dd('No User Found! Please Click on the Back button of the browser');
        }

        $userTypeId = $userAdminDetails->userType;

        if($userAdminDetails->id != Auth::user()->id) {
            if($userAdminDetails->userType == 1){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            if((Auth::user()->userType == 3 || Auth::user()->userType == 4) && ($userAdminDetails->id != Auth::user()->id)) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
        }
        $data = array();
        $permissionRecord = Permission::all(); // to fetch the core permissions from base table
        $permissionUserRecord = UserTypeDefaultPermission::getPermissionByUserTypeId($userTypeId);



        $data['permissionCount'] = count($permissionRecord);

        $permissionRecordEntered = UserPermission::getPermissionRecords($id, 'adminUserId');

        #dd($permissionRecordEntered);

        if (count($permissionRecordEntered) == 0) {
            // if we found anyone's permissions are empty, at first insert the permission to that individual users
            foreach ($permissionUserRecord as $val) {
                $perm = UserPermission::insertPermission($val->adminMenuId, $id, $val->permissionView, $val->permissionAdd, $val->permissionEdit, $val->permissionDelete);
            }
        }

        $permissionRecordEntered = UserPermission::getPermissionRecords($id, 'adminUserId');

        $data['arr'] = $this->categoryTree($permissionRecord, $permissionRecordEntered);



        $data['title'] = "User Roles :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "User Roles";
        $data['contentTop'] = array('breadcrumbText' => 'User Roles', 'contentTitle' => 'User Roles', 'pageInfo' => 'This section allows you to manage to change individual user roles');
        $data['userId'] = $userAdminDetails->id;

        $data['name'] = $userAdminDetails->firstName. " ". $userAdminDetails->lastName;
        $data['email'] = $userAdminDetails->email;

        $data['name'] = $userAdminDetails->firstName . " " . $userAdminDetails->lastName;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        return view('Administrator.adminusers.permission', $data);
    }

    function categoryTree($permissionRecord, $permissionRecordEntered, $parent_id = 0, $sub_mark = '') {
        $html = "";
        $event = "event";

        $arrowImagePath = url('/') . "/public/administrator/img/enter_row.png";
        foreach ($permissionRecord as $row) {

            foreach ($permissionRecordEntered as $keyword => $value) {
                if ($row->id == $value->adminMenuId) {
                    $checkedView = $value->permissionView == 1 ? 'checked' : '';
                    $checkedAdd = $value->permissionAdd == 1 ? 'checked' : '';
                    $checkedEdit = $value->permissionEdit == 1 ? 'checked' : '';
                    $checkedDelete = $value->permissionDelete == 1 ? 'checked' : '';
                    if ($checkedView == 'checked' || $checkedAdd == 'checked' || $checkedEdit == 'checked' || $checkedDelete == 'checked') {
                        $checkedParent = "checked";
                    } else {
                        $checkedParent = "";
                    }
                }
            }

            if ($row->adminMenuParentId == $parent_id) {
                if ($sub_mark == '') {

                    //if($permissionRecordEntered[1]->permissionView==0){echo 'checked';} 

                    $html .='<tr class="firstRow">
                <td><label><input type="checkbox" ' . $checkedParent . ' name="chk_' . $row->id . '" value="' . $row->id . '" id=' . $string = preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . ' class="flat-red me">
                        <span class="radioSpan">' . $row->adminMenuName . '</span></label>
                </td>
                <span id="' . $row->adminMenuName . '">
                <td><label><input type="checkbox" ' . $checkedView . ' name="view_' . $row->id . '" value="1" id="view' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . 'event);"></label></td>
                <td><label><input type="checkbox" ' . $checkedAdd . ' name="add_' . $row->id . '" value="1" id="add' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . 'event);"></label></td>
                <td><label><input type="checkbox" ' . $checkedEdit . ' name="edit_' . $row->id . '" value="1" id="edit' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . 'event);"></label></td>
                <td><label><input type="checkbox" ' . $checkedDelete . ' name="delete_' . $row->id . '" value="1" id="delete' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . 'event);"></label>
                </td>
                </span>
                </tr>';
                } else if ($sub_mark == '---') {
                    $html .='<tr>
                        <td style="padding-left:10px;"><img src=' . $arrowImagePath . '>
                            <label><input type="checkbox" ' . $checkedParent . ' name="chk_' . $row->id . '" value="' . $row->id . '" id=' . $string = preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . ' class="flat-red me ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '">
                                <span class="radioSpan">' . $row->adminMenuName . '</span></label>
                        </td>
                <span id="spanID' . $row->id . '">
                <td><label><input type="checkbox" ' . $checkedView . ' name="view_' . $row->id . '" value="1" id="view' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedAdd . ' name="add_' . $row->id . '" value="1" id="add' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedEdit . ' name="edit_' . $row->id . '" value="1" id="edit' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedDelete . ' name="delete_' . $row->id . '" value="1" id="delete' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label>
                </span>
                </tr>';
                } else {
                    $html .='<tr>
                        <td style="padding-left:40px;"><img src=' . $arrowImagePath . '>
                            <label><input type="checkbox" ' . $checkedParent . ' name="chk_' . $row->id . '" value="' . $row->id . '" id=' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . ' class="flat-red me ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '">
                                <span class="radioSpan">' . $row->adminMenuName . '</span></label>
                        </td>
                <td><label><input type="checkbox" ' . $checkedView . ' name="view_' . $row->id . '" value="1" id="view' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedAdd . ' name="add_' . $row->id . '" value="1" id="add' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName . "'," . "'" . $event) . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedEdit . ' name="edit_' . $row->id . '" value="1" id="edit' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName . "'," . "'" . $event) . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedDelete . ' name="delete_' . $row->id . '" value="1" id="delete' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName . "'," . "'" . $event) . "'" . ');"></label>
                </tr>';
                }

                $html .='' . $this->categoryTree($permissionRecord, $permissionRecordEntered, $row->id, $sub_mark . '---');
                /* $html .= '</span></label>';
                  $html .="</td></tr>"; */
            }
        }
        return $html;
    }

    // User Role Modification
    public function saveRole(Request $request, $id) {
        //dd($request->All());
        //dd($request->chk_1);;

        $lastMenuId = Permission::orderby('id', 'desc')->first();


        for ($i = 1; $i <= $lastMenuId->id; $i++) {
            $adminMenuId = 'chk_' . $i;
            $adminMenuId2 = $request->$adminMenuId;


            $permView = 'view_' . $i;
            $permAdd = 'add_' . $i;
            $permEdit = 'edit_' . $i;
            $permDelete = 'delete_' . $i;

            $permView2 = $request->$permView;
            $permAdd2 = $request->$permAdd;
            $permEdit2 = $request->$permEdit;
            $permDelete2 = $request->$permDelete;


            if (isset($permView2)) {
                $setview = $permView2;
            } else {
                $setview = 0;
            }


            if (isset($permAdd2)) {
                $setadd = $permAdd2;
            } else {
                $setadd = 0;
            }

            if (isset($permEdit2)) {
                $setedit = $permEdit2;
            } else {
                $setedit = 0;
            }

            if (isset($permDelete2)) {
                $setdelete = $permDelete2;
            } else {
                $setdelete = 0;
            }


            if ($setadd == null) {
                $setadd = 0;
            }

            if ($setedit == null) {
                $setedit = 0;
            }

            if ($setview == null) {
                $setview = 0;
            }

            if ($setdelete == null) {
                $setdelete = 0;
            }

            // ********* Insert Or update the data into user type permission table **************
            $perm = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $i, 'adminUserId' => $id));
            $perm->permissionView = $setview;
            $perm->permissionAdd = $setadd;
            $perm->permissionEdit = $setedit;
            $perm->permissionDelete = $setdelete;
            $perm->adminMenuId = $i;
            $perm->adminUserId = $id;
            $perm->save();

            /*if($setview == 1 || $setadd == 1 || $setedit == 1 || $setdelete == 1){

                $recordMasterRoleSecondLevel = Adminmenuuserpermission::recordmasterRole($i);
                if($recordMasterRoleSecondLevel[0]->adminMenuParentId != 0){
                    $permMaster = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $recordMasterRoleSecondLevel[0]->adminMenuParentId, 'adminUserId' => $id));
                    $permMaster->permissionView = 1;
                    $permMaster->permissionAdd = 1;
                    $permMaster->permissionEdit = 1;
                    $permMaster->permissionDelete = 1;
                    $permMaster->save();

                    $recordMasterRoleFirstLevel = Adminmenuuserpermission::recordmasterRole($recordMasterRoleSecondLevel[0]->adminMenuParentId);
                    if($recordMasterRoleFirstLevel[0]->adminMenuParentId != 0){
                    $permMaster = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $recordMasterRoleFirstLevel[0]->adminMenuParentId, 'adminUserId' => $id));
                    $permMaster->permissionView = 1;
                    $permMaster->permissionAdd = 1;
                    $permMaster->permissionEdit = 1;
                    $permMaster->permissionDelete = 1;
                    $permMaster->save();
                    }

                }

            }*/



            // this is for default permissions add function into the userType permission table (for type: 2, 3, 4)
            //$usertypePermCreation = Permission::menuCreation($i, $id);
        }


        return redirect('/administrator/adminusers')->with('successMessage', 'Permission set successfully');

        //dd('end');
    }

    /**
     * Method used to clear search history
     * @return type
     */
    public function showall() {
        \Session::forget('SELLERDATA');
        return \Redirect::to('administrator/sellers');
    }


    /**
     * Method used to inherit/export permissions to admin users
     * @param integer $page
     * @param request $request
     * @return type
     */
    public function inheritPermission($page, Request $request) {
        
        foreach($request->selected as $keyUser => $valUser){
            $userAdmin = UserAdmin::find($valUser);

            /* GET THE USER TYPE */
            $userTypeId = $userAdmin->userType;

            /* DELETE USER */
            Adminmenuuserpermission::where('adminUserId', $valUser)->delete();

            /* GET THE DEFAULT PERMISSIONS */
            $UserTypeDefaultPermission = UserTypeDefaultPermission::getPermissionByUserTypeId($userTypeId);

            /* INSERT THE PERMISSIONS INTO ADMIN USERS PERM TABLE */
            foreach ($UserTypeDefaultPermission as $val) {
                $perm = UserPermission::insertPermission($val->adminMenuId, $valUser, $val->permissionView, $val->permissionAdd, $val->permissionEdit, $val->permissionDelete);
            }
        }

        $returnValue = "Completed";
        return response()->json(['returnValue' => $returnValue]);
    }

    public function view(Route $route, Request $request, $id) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Sellers'), Auth::user()->userType); // call the helper function

        $data['title'] = "Administrative Panel :: Sellers";
        $data['contentTop'] = array('breadcrumbText' => array('Sellers','View Seller'), 'contentTitle' => 'Sellers', 'pageInfo' => 'This section allows you to view seller profile');
        $data['pageTitle'] = "View Sellers";
        $data['page'] = !empty($page) ? $page : '1';
       
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        $data['sellerDetails'] = Sellers::find($id); 
        $data['subscriptionHistory'] = Subscriptionhistories::find($id); 
       
        return view('Administrator.sellers.view', $data);
    }

}
