<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Annualpurchase;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;
use DB;
class AnnualpurchaseController extends Controller {

    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index() {

        //echo '77';exit;
        $data = array();

        $annualpurchaseObj = new Annualpurchase();
        
        if (\Request::isMethod('post')) {
            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            
                        
            \Session::forget('ANNUALPURCHASE');
            \Session::push('ANNUALPURCHASE.searchData', $searchData);
            \Session::push('ANNUALPURCHASE.searchDisplay', $perpage);
            \Session::push('ANNUALPURCHASE.field', $sortField);
            \Session::push('ANNUALPURCHASE.type', $sortOrder);

            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AnnualPurchasingValue'), Auth::user()->userType); // call the helper function
            if($findRole['canView'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
           
        } 
        else {           
                $sortField = \Session::get('ANNUALPURCHASE.field');
                $sortType = \Session::get('ANNUALPURCHASE.type');
                $perpage = \Session::get('ANNUALPURCHASE.searchDisplay');
                $searchData = \Session::get('ANNUALPURCHASE.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AnnualPurchasingValue'), Auth::user()->userType); // call the helper function
                
                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
        }
        $param = array();
        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        
        $records = $annualpurchaseObj->getData($param);
        #dd($records);
        $data['param'] = $param;
        $data['page'] = $records->currentPage();
        $data['records'] = $records;       
        $data['forntendUrl'] = Config::get('constants.frontendUrl');
        
        $data['pageTitle'] = 'Annual Purchasing Value';
        $data['title'] = "Annual Purchasing Value :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => 'Annual Purchasing Value Management', 'contentTitle' => 'Resell Items', 'pageInfo' => 'This section allows you to manage annual purchasing value');
        

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.annualpurchase.index', $data);
    }

    public function addeditannualpurchase($id = -1, $page = 1) {

        $annualpurchaseObj = new Annualpurchase();
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $annualpurchaseObj->find($id);
            $data['record'] = $record;
        } else {
            $data['action'] = 'Add';
            $data['record'] = $annualpurchaseObj;
        }
        
        $data['page'] = $page;
        
        $data['pageTitle'] = $data['action'] . ' Annual Purchase Value';
        $data['title'] = "Annual Purchase Value :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Annual Purchase Value', 'Add/Edit Resell Items'), 'contentTitle' => 'Annual Purchase Value', 'pageInfo' => 'This section allows you to manage annual purchase value');
       
        return view('Administrator.annualpurchase.addeditpage', $data);
    }

    public function addeditannualpurchaserecord($id = -1, $page = '-1', Request $request) {

        $annualpurchaseObj = new Annualpurchase();
        
        if ($id != '-1') {
            $annualpurchaseObj = $annualpurchaseObj->find($id);
            
        }
        
        $annualpurchaseObj->title   = $request->input('titles');
        $annualpurchaseObj->description = ($request->input('description'));
    #dd($annualpurchaseObj);
        if ($annualpurchaseObj->save())
            return redirect(route('annualpurchase'))->with('successMessage', 'Information saved successfuly.');
    }

    public function editannualpurchasestatus($id = -1, $status) {
        $annualpurchaseObj = new Annualpurchase();
        if ($id != -1)
            $annualpurchaseObj = $annualpurchaseObj->find($id);
            $annualpurchaseObj->status = $status;

        if ($annualpurchaseObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function deleterecord($id = -1,$page = 1) {

        $annualpurchaseObj = new Annualpurchase();

        if ($id != '-1') {
            $annualpurchaseObj = $annualpurchaseObj->find($id);
            if ($annualpurchaseObj->delete())
                return redirect(route('annualpurchase'))->with('successMessage', 'Information deleted successfully.');
        }
    }

}
