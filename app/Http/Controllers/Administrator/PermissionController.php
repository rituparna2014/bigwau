<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAdmin;
use Auth;
use App\Model\User;
use App\Model\Permission;
use App\Model\UserTypeDefaultPermission;
use Illuminate\Contracts\Auth\Authenticatable;
use customhelper;
use Config;
use App\Model\Adminmenuuserpermission;

class PermissionController extends Controller {
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('auth:admin');
    }
    public function index() {
        /*  DO NOTHING  */
        dd();
    }

    
    /**
     * Method for show the information
     * @param integer $id
     * @return array
     */
    public function show($id) {

        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AdminRoles'), Auth::user()->userType);
        //$findRole = customhelper::seePermission(4, Auth::user()->id);

        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', 'You don\'t have required permission');
        }

        if($id == 1){
            return \Redirect::to('administrator/dashboard/')->with('errorMessage', 'You don\'t have required permission');
        }

        /*  GET ALL PERMISSION RECORDS  */
        $permissionRecord = Permission::all();
        //echo $permissionRecord;die;
        $data['userTypeId'] = $id;
        $data['permissionCount'] = count($permissionRecord);
        
        /*  GET PERMISSION RECORDS BY USER TYPE  */
        $permissionRecordEntered = Permission::getPermissionRecords($id, 'userTypeId');
        
        if(count($permissionRecordEntered) == 0){

            $default = UserTypeDefaultPermission::where('userTypeId', '4')->get();
            

            foreach($default as $key => $val){
                $newDefaultPermission = new UserTypeDefaultPermission;
                $newDefaultPermission->userTypeId = $id;
                $newDefaultPermission->adminMenuId = $val->adminMenuId;
                $newDefaultPermission->permissionView = $val->permissionView;
                $newDefaultPermission->permissionAdd = $val->permissionAdd;
                $newDefaultPermission->permissionEdit = $val->permissionEdit;
                $newDefaultPermission->permissionDelete = $val->permissionDelete;
                $newDefaultPermission->save();
            }
            
        }
        
        /*  GENERATE THE HTML FOR PERMISSIONS  */
        $data['arr'] = $this->categoryTree($permissionRecord, $permissionRecordEntered);
        
        
        /* SET DATA FOR VIEW  */
        $data['userTypeName'] = UserAdmin::getUserTypeNameById($id);        
        $data['title'] = "Admin Roles :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Admin Roles";
        $data['contentTop'] = array('breadcrumbText'=>'Admin Roles','contentTitle'=>'Admin Roles','pageInfo'=>'This section allows you to manage to change admin roles');
        
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUser', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminRoles57');

        return view('Administrator.permission.permissionAdd', $data);

    }

    

    /**
     * Method for generate the html for permissions
     * @param object $permissionRecord
     * @param object $permissionRecordEntered
     * @param integer $parent_id
     * @param string $sub_mark
     * @return string
     */
    function categoryTree($permissionRecord, $permissionRecordEntered, $parent_id = 0, $sub_mark = '')
    { 
    /* GENERATE PARENT, SUB PARENT AND CHILDREN NODES */
    $html = "";
    $event = "event";
    $checkedParent = '';
    $checkedView = '';
    $checkedAdd = '';
    $checkedEdit = '';
    $checkedDelete = '';
    $arrowImagePath = url('/')."/public/administrator/img/enter_row.png";
    foreach($permissionRecord as $row) {
        
        foreach($permissionRecordEntered as $keyword => $value){
            if($row->id == $value->adminMenuId){
                    $checkedView = $value->permissionView == 1 ? 'checked' : '';
                    $checkedAdd = $value->permissionAdd == 1 ? 'checked' : '';
                    $checkedEdit = $value->permissionEdit == 1 ? 'checked' : '';
                    $checkedDelete = $value->permissionDelete == 1 ? 'checked' : '';
                    if($checkedView == 'checked' || $checkedAdd == 'checked' || $checkedEdit == 'checked' || $checkedDelete == 'checked'){ $checkedParent = "checked"; } else { $checkedParent = ""; }
                    
            }
        }

        if ($row->adminMenuParentId == $parent_id) {
            if($sub_mark == ''){

                $html .='<tr class="firstRow">
                <td><label><input type="checkbox" '.$checkedParent.' name="chk_'. $row->id .'" value="'.$row->id.'" id='.$string = preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).' class="flat-red me">
                        <span class="radioSpan">'.$row->adminMenuName.'</span></label>
                </td>
                <span id="'.$row->adminMenuName.'">
                <td><label><input type="checkbox" '.$checkedView.' name="view_'. $row->id .'" value="1" id="view'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". 'event);"></label></td>
                <td><label><input type="checkbox" '.$checkedAdd.' name="add_'. $row->id .'" value="1" id="add'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". 'event);"></label></td>
                <td><label><input type="checkbox" '.$checkedEdit.' name="edit_'. $row->id .'" value="1" id="edit'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". 'event);"></label></td>
                <td><label><input type="checkbox" '.$checkedDelete.' name="delete_'. $row->id .'" value="1" id="delete'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". 'event);"></label>
                </td>
                </span>
                </tr>';
            }
            else if($sub_mark == '---'){
                $html .='<tr>
                        <td style="padding-left:10px;"><img src='.$arrowImagePath.'>
                            <label><input type="checkbox" '.$checkedParent.' name="chk_'.$row->id.'" value="'.$row->id.'" id='.$string = preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).' class="flat-red me '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'">
                                <span class="radioSpan">'.$row->adminMenuName.'</span></label>
                        </td>
                <span id="spanID'.$row->id.'">
                <td><label><input type="checkbox" '.$checkedView.' name="view_'. $row->id .'" value="1" id="view'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". "'".$event. "'".');"></label></td>
                <td><label><input type="checkbox" '.$checkedAdd.' name="add_'. $row->id .'" value="1" id="add'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". "'".$event. "'".');"></label></td>
                <td><label><input type="checkbox" '.$checkedEdit.' name="edit_'. $row->id .'" value="1" id="edit'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". "'".$event. "'".');"></label></td>
                <td><label><input type="checkbox" '.$checkedDelete.' name="delete_'. $row->id .'" value="1" id="delete'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". "'".$event. "'".');"></label>
                </span>
                </tr>';
            }
            else {
                $html .='<tr>
                        <td style="padding-left:40px;"><img src='.$arrowImagePath.'>
                            <label><input type="checkbox" '.$checkedParent.' name="chk_'.$row->id.'" value="'.$row->id.'" id='.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).' class="flat-red me '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'">
                                <span class="radioSpan">'.$row->adminMenuName.'</span></label>
                        </td>
                <td><label><input type="checkbox" '.$checkedView.' name="view_'. $row->id .'" value="1" id="view'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName)."',". "'".$event. "'".');"></label></td>
                <td><label><input type="checkbox" '.$checkedAdd.' name="add_'. $row->id .'" value="1" id="add'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName."',". "'".$event). "'".');"></label></td>
                <td><label><input type="checkbox" '.$checkedEdit.' name="edit_'. $row->id .'" value="1" id="edit'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName."',". "'".$event). "'".');"></label></td>
                <td><label><input type="checkbox" '.$checkedDelete.' name="delete_'. $row->id .'" value="1" id="delete'. $row->id .'" class="flat-red '.preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName).'" onclick="check_btn('."'".preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName."',". "'".$event). "'".');"></label>
                </tr>';
            }

            $html .=''.$this->categoryTree($permissionRecord, $permissionRecordEntered, $row->id, $sub_mark.'---');
            
        }
        
    }   
    return $html;
    }

    
    /**
     * Method for edit the information
     * @param integer $id
     */
    public function edit($id, Request $request)
    {
        dd('Edit prohibited');
    }

    /**
     * Method for update the permission
     * @param integer $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AdminRoles'), Auth::user()->userType); 
        if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', 'You don\'t have required permission');
        }

        /*  GET EACH PERMISSION ROW AS CHECKED  */

        $lastMenuId = Permission::orderby('id', 'desc')->first();


        for($i=1; $i<=$lastMenuId->id; $i++){
            $adminMenuId = 'chk_'.$i;
            
            
            $permView = 'view_'.$i;
            $permAdd = 'add_'.$i;
            $permEdit = 'edit_'.$i;
            $permDelete = 'delete_'.$i;

            $permView2 = $request->$permView;
            $permAdd2 = $request->$permAdd;
            $permEdit2 = $request->$permEdit;
            $permDelete2 = $request->$permDelete;
            

            if(isset($permView2)){
                $setview = $permView2;
            } else {
                $setview = 0;
            }
            
            
            if(isset($permAdd2)){
                $setadd = $permAdd2;
            } else {
                $setadd = 0;
            }

            if(isset($permEdit2)){
                $setedit = $permEdit2;
            } else {
                $setedit = 0;
            }

            if(isset($permDelete2)){
                $setdelete = $permDelete2;
            } else {
                $setdelete = 0;
            }
            

            if($setadd == null){
                $setadd = 0;
            }

            if($setedit == null){
                $setedit = 0;
            }

            if($setview == null){
                $setview = 0;
            }

            if($setdelete == null){
                $setdelete = 0;
            }
            
            /*  INSERT OR UPDATE THE DATA BASED ON USER TYPE  */
            $perm = UserTypeDefaultPermission::firstOrNew(array('adminMenuId' => $i, 'userTypeId' => $id));
            
            $perm->permissionView               = $setview;
            $perm->permissionAdd                = $setadd;
            $perm->permissionEdit               = $setedit;
            $perm->permissionDelete             = $setdelete;
            $perm->adminMenuId                  = $i;
            $perm->userTypeId                   = $id;
            $perm->save();


            
            /*if($setview == 1 || $setadd == 1 || $setedit == 1 || $setdelete == 1){

                $recordMasterRoleSecondLevel = Adminmenuuserpermission::recordmasterRole($i);
                if($recordMasterRoleSecondLevel[0]->adminMenuParentId != 0){
                    $permMaster = UserTypeDefaultPermission::firstOrNew(array('adminMenuId' => $recordMasterRoleSecondLevel[0]->adminMenuParentId, 'userTypeId' => $id));
                    $permMaster->permissionView = 1;
                    $permMaster->permissionAdd = 1;
                    $permMaster->permissionEdit = 1;
                    $permMaster->permissionDelete = 1;
                    $permMaster->save();

                    $recordMasterRoleFirstLevel = Adminmenuuserpermission::recordmasterRole($recordMasterRoleSecondLevel[0]->adminMenuParentId);
                    if($recordMasterRoleFirstLevel[0]->adminMenuParentId != 0){
                    $permMaster = UserTypeDefaultPermission::firstOrNew(array('adminMenuId' => $recordMasterRoleFirstLevel[0]->adminMenuParentId, 'userTypeId' => $id));
                    $permMaster->permissionView = 1;
                    $permMaster->permissionAdd = 1;
                    $permMaster->permissionEdit = 1;
                    $permMaster->permissionDelete = 1;
                    $permMaster->save();
                    }

                }

            }*/
        }

        return redirect('/administrator/adminrole/')->with('successMessage', 'Permission set successfully');
    }


}
