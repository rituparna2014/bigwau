<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Contentpage;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;

class ContentpageController extends Controller {

    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index() {

        //echo '77';exit;
        $data = array();

        $contentpageObj = new Contentpage();

        if (\Request::isMethod('post')) {
            

            /* GET POST VALUE  */

            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            
                        
            \Session::forget('RECORD');
            \Session::push('RECORD.searchData', $searchData);
            \Session::push('RECORD.searchDisplay', $perpage);
            \Session::push('RECORD.field', $sortField);
            \Session::push('RECORD.type', $sortOrder);

            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Contentpage'), Auth::user()->userType); // call the helper function
            if($findRole['canView'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
           
        } 
        else {           

                $sortField = \Session::get('RECORD.field');
                $sortType = \Session::get('RECORD.type');
                $perpage = \Session::get('RECORD.searchDisplay');
                $searchData = \Session::get('RECORD.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Contentpage'), Auth::user()->userType); // call the helper function


                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
           
        }

        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        
        $records = $contentpageObj->getData($param);

        $data['param'] = $param;
        $data['page'] = $records->currentPage();
        $data['records'] = $records;       
        $data['forntendUrl'] = Config::get('constants.frontendUrl');
        
        $data['pageTitle'] = 'Contents';
        $data['title'] = "Contents :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Contents', 'Contents'), 'contentTitle' => 'Contents', 'pageInfo' => 'This section allows you to manage contents');
        

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.contentpage.index', $data);
    }

    public function addeditcontent($id = -1, $page = 1) {

        $contentpageObj = new Contentpage();
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $contentpageObj->find($id);
            $data['record'] = $record;
        } else {
            $data['action'] = 'Add';
            $data['record'] = $contentpageObj;
        }
        
        $data['page'] = $page;
        
        $data['pageTitle'] = $data['action'] . ' Content';
        $data['title'] = "Contents :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Contents', 'Add/Edit Content'), 'contentTitle' => 'Contents', 'pageInfo' => 'This section allows you to manage contents');
       
        return view('Administrator.contentpage.addeditpage', $data);
    }

    public function addeditcontentrecord($id = -1, $page = '-1', Request $request) {

        $contentpageObj = new Contentpage();
        $uploadImage = 0;

        if ($request->logo) {
            $this->validate($request, [
                'logo' => 'image|mimes:jpeg,png,jpg,gif|max:4000|dimensions:min_width=1600,min_height=626',
            ]);

            $validator = Validator::make(\Input::all(), [
                        'pageHeader' => 'required',
                        'logo' => 'image|mimes:jpeg,png,jpg,gif|max:4000|dimensions:min_width=1600,min_height=626',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {

                /* Upload banner image */
                if ($request->hasFile('logo')) {
                    $image = $request->file('logo');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/site_page');
                    $image->move($destinationPath, $name);
                    $contentpageObj->pageBanner = $name;
                    $uploadImage = 1;
                }
            }
        }

        if ($id != '-1') {
            $contentpageObj = $contentpageObj->find($id);
            if ($uploadImage == 1)
                $contentpageObj->pageBanner = $name;
        }
        
        if ($id == '-1')
            $contentpageObj->slug = strtolower(str_replace(' ', '_', preg_replace('/[^\w\s]/', '', $request->input('pageHeader'))));
        $contentpageObj->pageHeader = $request->input('pageHeader');
        $contentpageObj->pageContent = ($request->input('pageContent'));
        $contentpageObj->pageTitle = $request->input('pageTitle');
        $contentpageObj->metaKeyword = $request->input('metaKeyword');
        $contentpageObj->metaDescription = addslashes($request->input('metaDescription'));
        if ($uploadImage)
            $contentpageObj->pageBanner = $name;



        if ($contentpageObj->save())
            return redirect(route('contentlist'))->with('successMessage', 'Information saved successfuly.');
    }

    public function editstatus($id = -1, $status) {

        $contentpageObj = new Sitepage();
        if ($id != -1)
            $contentpageObj = $contentpageObj->find($id);
        $contentpageObj->status = $status;

        if ($contentpageObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function deleterecord($id = -1,$page = 1) {

        $contentpageObj = new Contentpage();

        if ($id != '-1') {
            $contentpageObj = $contentpageObj->find($id);
            if ($contentpageObj->delete())
                return redirect(route('contentlist'))->with('successMessage', 'Information deleted successfuly.');
        }
    }

}
