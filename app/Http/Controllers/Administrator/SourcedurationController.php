<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sourceduration;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;
use DB;
class SourcedurationController extends Controller {

    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index() {

        //echo '77';exit;
        $data = array();

        $sourcedurationObj = new Sourceduration();
        
        if (\Request::isMethod('post')) {
            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            
                        
            \Session::forget('SOURCEDURATION');
            \Session::push('SOURCEDURATION.searchData', $searchData);
            \Session::push('SOURCEDURATION.searchDisplay', $perpage);
            \Session::push('SOURCEDURATION.field', $sortField);
            \Session::push('SOURCEDURATION.type', $sortOrder);

            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.SourceDuration'), Auth::user()->userType); // call the helper function
            if($findRole['canView'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
           
        } 
        else {           
                $sortField = \Session::get('SOURCEDURATION.field');
                $sortType = \Session::get('SOURCEDURATION.type');
                $perpage = \Session::get('SOURCEDURATION.searchDisplay');
                $searchData = \Session::get('SOURCEDURATION.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.SourceDuration'), Auth::user()->userType); // call the helper function
                
                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
        }
        $param = array();
        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        
        $records = $sourcedurationObj->getData($param);
        #dd($records);
        $data['param'] = $param;
        $data['page'] = $records->currentPage();
        $data['records'] = $records;       
        $data['forntendUrl'] = Config::get('constants.frontendUrl');
        
        $data['pageTitle'] = 'Source Duration';
        $data['title'] = "Source Duration :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => 'Source Duration Management', 'contentTitle' => 'Source Duration', 'pageInfo' => 'This section allows you to manage annual source duration');
        

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.sourceduration.index', $data);
    }

    public function addeditsourceduration($id = -1, $page = 1) {

        $sourcedurationObj = new Sourceduration();
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $sourcedurationObj->find($id);
            $data['record'] = $record;
        } else {
            $data['action'] = 'Add';
            $data['record'] = $sourcedurationObj;
        }
        
        $data['page'] = $page;
        
        $data['pageTitle'] = $data['action'] . ' Source Duration';
        $data['title'] = "Source Duration :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Source Duration', 'Add/Edit Source Duration'), 'contentTitle' => 'Source Duration', 'pageInfo' => 'This section allows you to manage annual source duration');
       
        return view('Administrator.sourceduration.addeditpage', $data);
    }

    public function addeditsourcedurationrecord($id = -1, $page = '-1', Request $request) {

        $sourcedurationObj = new Sourceduration();
        
        if ($id != '-1') {
            $sourcedurationObj = $sourcedurationObj->find($id);
            
        }
        
        $sourcedurationObj->title   = $request->input('titles');
        $sourcedurationObj->description = ($request->input('description'));
        #dd($annualpurchaseObj);
        if ($sourcedurationObj->save())
            return redirect(route('sourceduration'))->with('successMessage', 'Information saved successfuly.');
    }

    public function editsourcedurationstatus($id = -1, $status) {
        $sourcedurationObj = new Sourceduration();
        if ($id != -1)
            $sourcedurationObj = $sourcedurationObj->find($id);
            $sourcedurationObj->status = $status;

        if ($sourcedurationObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function deleterecord($id = -1,$page = 1) {

        $sourcedurationObj = new Sourceduration();

        if ($id != '-1') {
            $sourcedurationObj = $sourcedurationObj->find($id);
            if ($sourcedurationObj->delete())
                return redirect(route('sourceduration'))->with('successMessage', 'Information deleted successfully.');
        }
    }

}
