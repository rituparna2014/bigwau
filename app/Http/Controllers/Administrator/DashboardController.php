<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\helpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Log;
use Excel;
use Mail;
use Illuminate\Routing\Route;
use customhelper;
use App\Model\Buyers;
use App\Model\Sellers;
use App\Model\Service;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function getIndex(Request $request) {
        $data = array();
        $data['title'] = "ADMIN";

        $data['contentTop'] = array('breadcrumbText' => 'Dashboard', 'contentTitle' => 'Dashboard', 'pageInfo' => '');
        $data['pageTitle'] = "Dashboard";

        $data['activeBuyers'] = \App\Model\Buyers::select(DB::raw("count(id) AS totalActiveBuyers"))
                        ->where("status", '1')
                        ->where("deleted", '0')                                             
                        ->count();
        $data['inactiveBuyers'] = \App\Model\Buyers::select(DB::raw("count(id) AS totalActiveBuyers"))
                        ->where("status", '0')
                        ->where("deleted", '0')                                             
                        ->count();

        $data['activeSellers'] = \App\Model\Sellers::select(DB::raw("count(id) AS totalActiveSellers"))
                        ->where("status", '1')
                        ->where("deleted", '0')                                             
                        ->count();
        $data['inactiveSellers'] = \App\Model\Sellers::select(DB::raw("count(id) AS totalActiveSellers"))
                        ->where("status", '0')
                        ->where("deleted", '0')                                             
                        ->count();

        $data['activeSevices'] = \App\Model\Service::select(DB::raw("count(id) AS totalActiveServices"))
                        ->where("status", '1')
                        ->where("deleted", '0')                                             
                        ->count();

        $subscriptionCharge = \App\Model\Sellers::select(DB::raw("sum(fee) AS totalAmount"))
                        ->where("status", '1')
                        ->where("deleted", '0') 
                        ->groupBy("id")       
                        ->get()->toArray();

        $data['topTenSellers'] = \App\Model\Sellers::select('*')
                        ->where("status", '1')
                        ->where("deleted", '0')
                        ->orderBy('createdOn', 'desc')
                        ->take(10)                                             
                        ->get()->toArray();

        $data['recentBuyers'] = \App\Model\Buyers::select('*')
                        ->where("status", '1')
                        ->where("deleted", '0')
                        ->orderBy('createdOn', 'desc')
                        ->take(10)                                             
                        ->get()->toArray();

       
        

        $data['totalFee'] = ($subscriptionCharge[0]['totalAmount']>1)?$subscriptionCharge[0]['totalAmount']:'0';
       
        return view('Administrator.dashboard.index', $data);
    }

   

    

 

}
