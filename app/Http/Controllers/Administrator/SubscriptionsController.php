<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Subscription;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;

class SubscriptionsController extends Controller {

    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index() {

        //echo '77';exit;
        $data = array();

        $subscriptionObj = new Subscription();

        if (\Request::isMethod('post')) {
            

            /* GET POST VALUE  */

            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            
                        
            \Session::forget('RECORDSUB');
            \Session::push('RECORDSUB.searchData', $searchData);
            \Session::push('RECORDSUB.searchDisplay', $perpage);
            \Session::push('RECORDSUB.field', $sortField);
            \Session::push('RECORDSUB.type', $sortOrder);

            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Subscriptions'), Auth::user()->userType); // call the helper function
            if($findRole['canView'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
           
        } 
        else {           

                $sortField = \Session::get('RECORDSUB.field');
                $sortType = \Session::get('RECORDSUB.type');
                $perpage = \Session::get('RECORDSUB.searchDisplay');
                $searchData = \Session::get('RECORDSUB.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Subscriptions'), Auth::user()->userType); // call the helper function


                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
           
        }

        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        
        $records = $subscriptionObj->getData($param);

        $data['param'] = $param;
        $data['page'] = $records->currentPage();
        $data['records'] = $records;       
        $data['forntendUrl'] = Config::get('constants.frontendUrl');
        
        $data['pageTitle'] = 'Subscriptions';
        $data['title'] = "Subscriptions :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => 'Subscription Management', 'contentTitle' => 'Subscriptions', 'pageInfo' => 'This section allows you to manage subscription');
        

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.subscriptions.index', $data);
    }

    public function addeditsubscription($id = -1, $page = 1) {

        $subscriptionObj = new Subscription();
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $subscriptionObj->find($id);
            $data['record'] = $record;
        } else {
            $data['action'] = 'Add';
            $data['record'] = $subscriptionObj;
        }
        
        $data['page'] = $page;
        
        $data['pageTitle'] = $data['action'] . ' Subscription';
        $data['title'] = "Subscriptions :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Subscriptions', 'Add/Edit Subscription'), 'contentTitle' => 'Subscriptions', 'pageInfo' => 'This section allows you to manage subscriptions');
       
        return view('Administrator.subscriptions.addeditpage', $data);
    }

    public function addeditsubscriptionrecord($id = -1, $page = '-1', Request $request) {

        $subscriptionObj = new Subscription();
        

        if ($id != '-1') {
            $subscriptionObj = $subscriptionObj->find($id);
            
        }
        
        if ($id == '-1')
        
        $subscriptionObj->term   = $request->input('term');
        $subscriptionObj->amount = ($request->input('amount'));
        
        if ($subscriptionObj->save())
            return redirect(route('subscription'))->with('successMessage', 'Information saved successfuly.');
    }

    public function editsubscriptionstatus($id = -1, $status) {

        $subscriptionObj = new Subscription();
        if ($id != -1)
            $subscriptionObj = $subscriptionObj->find($id);
        $subscriptionObj->status = $status;

        if ($subscriptionObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function deletesubscription($id = -1,$page = 1) {

        $subscriptionObj = new Subscription();

        if ($id != '-1') {
            $subscriptionObj = $subscriptionObj->find($id);
            if ($subscriptionObj->delete())
                return redirect(route('subscription'))->with('successMessage', 'Information deleted successfully.');
        }
    }

}
