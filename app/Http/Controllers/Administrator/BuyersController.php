<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Model\Country;
use App\Model\Buyers;
use App\Model\Permission;
use App\Model\UserPermission;
use App\Model\UserTypeDefaultPermission;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Hash;
use Illuminate\Routing\Route;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Model\Adminmenuuserpermission;
use customhelper;
use App\Model\Emailtemplate;

class BuyersController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        //echo Config::get('constants.PermissionMenuIds.Buyers');die;
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Buyers'), Auth::user()->userType); // call the helper function
            
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('BUYERDATA');
            \Session::push('BUYERDATA.searchDisplay', $searchDisplay);
            \Session::push('BUYERDATA.field', $field);
            \Session::push('BUYERDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;



            // Search fields if any
            if ($request->has('search_firstName')) {
                $param['search_firstName'] = $request->search_firstName;
                \Session::push('BUYERDATA.search_firstName', $request->search_firstName);
            }
            if ($request->has('search_lastName')) {
                $param['search_lastName'] = $request->search_lastName;
                \Session::push('BUYERDATA.search_lastName', $request->search_lastName);
            }
            if ($request->has('search_companyName')) {
                $param['search_companyName'] = $request->search_companyName;
                \Session::push('BUYERDATA.search_companyName', $request->search_companyName);
            }
            if ($request->has('search_status')) {
                $param['search_status'] = $request->search_status;
                \Session::push('BUYERDATA.search_status', $request->search_status);
            }
            if ($request->has('search_rangedate')) {
                $param['search_rangedate'] = $request->search_rangedate;
                \Session::push('BUYERDATA.search_rangedate', $request->search_rangedate);
                if ($param['search_rangedate'] == 'custom') {
                    $param['search_reservation'] = $request->search_reservation;
                    \Session::push('BUYERDATA.search_reservation', $request->search_reservation);
                }
            }
           
            #dd(session()->all());
        } else {
            $sortField = \Session::get('BUYERDATA.field');
            $sortType = \Session::get('BUYERDATA.type');
            $searchDisplay = \Session::get('BUYERDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'firstName' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'buyerType' => array('current' => 'sorting'),
            'companyName' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        

       
        $buyersData = Buyers::getBuyersList($param);
        
     

        $data['title'] = "Administrative Panel :: Buyers";
        $data['contentTop'] = array('breadcrumbText' => 'Buyers', 'contentTitle' => 'Buyers', 'pageInfo' => 'This section allows you to manage buyer profile');
        $data['pageTitle'] = "Buyers";
        $data['page'] = $buyersData->currentPage();
        $data['buyersData'] = $buyersData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        
        return view('Administrator.buyers.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Buyers'), Auth::user()->userType); // call the helper function

        $data['title'] = "Administrative Panel :: Buyers";
        $data['contentTop'] = array('breadcrumbText' => array('Buyers','Add/Edit Buyer'), 'contentTitle' => 'Buyers', 'pageInfo' => 'This section allows you to manage buyer profile');
        $data['pageTitle'] = "Buyers";
        $data['page'] = !empty($page) ? $page : '1';
       
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $buyer = Buyers::find($id);
           
            $data['buyer'] = $buyer;
            
            
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
           
            
        }
        return view('Administrator.buyers.addedit', $data);
    }

    /*     * Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $buyer = new Buyers;
        
        $validator = Validator::make($request->all(), [
                    'firstName' => 'required:' . $buyer->table . ',firstName,'. $id,
                    'lastName' => 'required:' . $buyer->table . ',lastName,'. $id,
                    'buyerId' => 'required:' . $buyer->table . ',buyerId,'. $id,
                    'buyerType' => 'required:' . $buyer->table . ',buyerType,'. $id,
                    'companyName' => 'required|min:3|max:100:' . $buyer->table . ',companyName,'. $id,
                    'email' => 'required|email|unique:'.$buyer->table .  ',email,'. $id,
                    'phone' => 'required|numeric:' . $buyer->table . ',phone,'. $id,
                    'IBAN' => 'required:' . $buyer->table . ',IBAN,'. $id,
                    'BIC' => 'required:' . $buyer->table . ',BIC,'. $id,
                    'bankOwnerName' => 'required:' . $buyer->table . ',bankOwnerName,'. $id,
                    'bankOwnerAddress' => 'required:' . $buyer->table . ',bankOwnerAddress,'. $id,
                        //'warehouseid' => 'required|numeric:' . $userAdmin->table . ',warehouseid,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $buyer = Buyers::find($id);
                $buyer->updatedBy = Auth::user()->id;
                $buyer->updatedOn = Config::get('constants.CURRENTDATE');
            }
            
            $buyer->firstName = $request->firstName;
            $buyer->lastName = $request->lastName;
            $buyer->buyerId = $request->buyerId;
            $buyer->buyerType = $request->buyerType;
            $buyer->companyName = $request->companyName;
            $buyer->email = $request->email;
            $rawPassword = $request->password;
            /*$buyer->password = Hash::make($rawPassword);
            $buyer->remember_token = Hash::make($rawPassword);*/
            $buyer->profileDescription = $request->profileDescription;
            $buyer->birthday = \Carbon\Carbon::parse($request->birthday)->format('Y-m-d');
            $buyer->phone = $request->phone;
            $buyer->timezone = $request->timezone;
            $buyer->nationality = $request->nationality;
            $buyer->profession = $request->profession;
            $buyer->IBAN = $request->IBAN;
            $buyer->BIC = $request->BIC;
            $buyer->bankOwnerName = $request->bankOwnerName;
            $buyer->bankOwnerAddress = $request->bankOwnerAddress;
            $buyer->annualIncome = $request->annualIncome;
            $buyer->fee = $request->fee;
            $buyer->address = $request->address;


            $buyer->save();
            $buyerId = $buyer->id;


           

            return redirect('/administrator/buyers')->with('successMessage', 'Buyer Information saved successfuly.');
        }
    }

    /** Add
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function adddata($page, Request $request) {



        #dd($request->warehouseId);
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $buyer = new Buyers;

        $validator = Validator::make($request->all(), [
                    'firstName' => 'required:' . $buyer->table . ',firstName,',
                    'lastName' => 'required:' . $buyer->table . ',lastName,',
                    'buyerId' => 'required:' . $buyer->table . ',buyerId,',
                    'buyerType' => 'required:' . $buyer->table . ',buyerType,',
                    'companyName' => 'required|min:3|max:100:' . $buyer->table . ',companyName,',
                    'email' => 'required|email|unique:'.$buyer->table .  ',email,',
                    'password' => 'required:'.$buyer->table .  ',password,',
                    'phone' => 'required|numeric:' . $buyer->table . ',phone,',
                    'IBAN' => 'required:' . $buyer->table . ',IBAN,',
                    'BIC' => 'required:' . $buyer->table . ',BIC,',
                    'bankOwnerName' => 'required:' . $buyer->table . ',bankOwnerName,',
                    'bankOwnerAddress' => 'required:' . $buyer->table . ',bankOwnerAddress,',
                        //'warehouseid' => 'required|numeric:' . $userAdmin->table . ',warehouseid,' . $id,
        ]);

        if ($validator->fails()) {
           
            $values['firstName'] = $request->firstName;
            $values['lastName'] = $request->lastName;
            $values['buyerId'] = $request->buyerId;
            $values['buyerType'] = $request->buyerType;
            $values['companyName'] = $request->companyName;           
            $values['email'] = $request->email;
            $values['password'] = $request->password;
            $values['phone'] = $request->phone;
            $values['IBAN'] = $request->IBAN;
            $values['BIC'] = $request->BIC;
            $values['bankOwnerName'] = $request->bankOwnerName;
            $values['bankOwnerAddress'] = $request->bankOwnerAddress;

            return redirect()->back()->withErrors($validator->errors())->withInput($values);
        } else {
            
            $buyer->firstName = $request->firstName;
            $buyer->lastName = $request->lastName;
            $buyer->buyerId = $request->buyerId;
            $buyer->buyerType = $request->buyerType;
            $buyer->companyName = $request->companyName;
            $buyer->email = $request->email;
            $rawPassword = $request->password;
            $buyer->password = Hash::make($rawPassword);
            $buyer->remember_token = Hash::make($rawPassword);
            $buyer->profileDescription = $request->profileDescription;
            $buyer->birthday = \Carbon\Carbon::parse($request->birthday)->format('Y-m-d');
            $buyer->phone = $request->phone;
            $buyer->timezone = $request->timezone;
            $buyer->nationality = $request->nationality;
            $buyer->profession = $request->profession;
            $buyer->IBAN = $request->IBAN;
            $buyer->BIC = $request->BIC;
            $buyer->bankOwnerName = $request->bankOwnerName;
            $buyer->bankOwnerAddress = $request->bankOwnerAddress;
            $buyer->annualIncome = $request->annualIncome;
            $buyer->fee = $request->fee;
            $buyer->address = $request->address;            
            
            $buyer->createdBy = Auth::user()->id;
            $buyer->status = 1;            
            $buyer->save();
            $buyerId = $buyer->id;

            /* ++++++++++ email functionality ++++++++ */
            /*$Newuser = UserAdmin::find($userAdminId);
            $emailTemplate = Emailtemplate::where('templateKey', 'creation_admin')->first();
            $replace['[NAME]'] = $Newuser->firstName . " " . $Newuser->lastName;
            $replace['[PASSWORD]'] = $rawPassword;
            $replace['[EMAIL]'] = $Newuser->email;
            $to = $Newuser->email;
            customhelper::SendMail($emailTemplate, $replace, $to);*/
            /* ++++++++++ end of email functionality ++++ */

            // *********** Get the values for the permission of an user type ***************** //


           

            return redirect('/administrator/buyers')->with('successMessage', 'Buyer added successfuly.');
        }
    }

    // Generate random password
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Buyers::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/buyers/?page=' . $page)->with('successMessage', 'Status changed successfully.');
            } else {
                return \Redirect::to('administrator/buyers/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/buyers/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to change password
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changepassword($id, $page, Request $request) {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = $id;

        if (\Request::isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'new_password'     => 'required|min:6',
                'confirm_password' => 'required|same:new_password',
                ]);

                if ($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()->all()]);
                    //return redirect()->back()->withErrors($validator->errors());
                }    
                else { 
                    //$data = array();
                    //$data = $request->all();
             
                    $user = Buyers::find($request->id);
                    $credentials['password'] = $request->confirm_password;

                    //$user = $request->userId;
                    $user->password = Hash::make($credentials['password']);
                    $user->passwordLastChanged = Config::get('constants.CURRENTDATE');
                    $user->save();

                    /* ++++++++++ email functionality ++++++++ */
                   /* $emailTemplate = Emailtemplate::where('templateKey', 'change_password_admin')->first();
                    $replace['[NAME]'] = $user->firstName . " " . $user->lastName;
                    $replace['[PASSWORD]'] = $request->confirm_password;
                    $to = $user->email;
                    customhelper::SendMail($emailTemplate, $replace, $to);*/
                    /* ++++++++++ end of email functionality ++++ */


                    return response()->json(['success'=>'Password has changed successfuly.']);
                    //return Redirect::to('administrator/password/reset')->with('successMessage', 'Password have changed successfuly.');
                    //return $user->save() ? Password::PASSWORD_RESET : 'Error';
                    
                }
        }
            return view('Administrator.buyers.changepassword', $data);

       
    }

    /**
     * Method used to send notification to admin users
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function notify($id = '0', $page = '', Request $request) {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = $id;
        $data['buyer'] = Buyers::find($id);
        if (\Request::isMethod('post')) {

            //if ($request->has('notify_email') != '') {
                $notify_email = $request->input('notify_email');
                $message = $request->input('message');

                $page = !empty($page) ? $page : '1';

                $createrModifierId = Auth::user()->id;
                if (!empty($message)) {
                    //$newPassword = Hash::make($this->generateRandomString());
                    if (Buyers::sendNotification($id, $createrModifierId, $message)) {
                        /* ++++++++++ email functionality ++++++++ */
                        $emailTemplate = Emailtemplate::where('templateKey', 'notification_admin')->first();
                        $replace['[NAME]'] = $data['buyer']->firstName . " " . $data['buyer']->lastName;
                        $replace['[NOTIFICATION]'] = html_entity_decode($message);
                        $to = $data['buyer']->email;
                        customhelper::SendMail($emailTemplate, $replace, $to);
                        /* ++++++++++ end of email functionality ++++ */
                        return \Redirect::to('administrator/buyers/?page=' . $page)->with('successMessage', 'Notification successfully sent.');
                    } else {
                        return \Redirect::to('administrator/buyers/?page=' . $page)->with('errorMessage', 'Please specify message');
                    }
                } else {
                    return \Redirect::to('administrator/buyers/?page=' . $page)->with('errorMessage', 'Please specify message');
                }
            //}
        }
        return view('Administrator.buyers.notificationhistory', $data);
    }

    /**
     * Method used to Export All with selected fields
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function exportall($page, Request $request) {


        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/buyers/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {
            $buyerDataExcel = Buyers::getBuyersListExcel($request->selectall)->get();

            //dd($adminUserDataExcel);

            ob_end_clean();
            ob_start();
            Excel::create("Buyers" . Carbon::now(), function($excel) use($buyerDataExcel) {
                $excel->sheet('Sheet 1', function($sheet) use($buyerDataExcel) {
                    $sheet->fromArray($buyerDataExcel);
                });
            })->export('xlsx');
            ob_flush();
            return \Redirect::to('administrator/buyers/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

    /**
     * Method used to Export selected with selected fields
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function exportSelected($page, Request $request) {


        
        if (\Request::isMethod('post')) {
            $buyerDataExcelSelected = Buyers::getBuyerListExcelSelected($request->selectedField, $request->selected)->get(); 
            $excelName = "Buyers_" . Carbon::now();
            $path = public_path('export/' . $excelName);

            ob_end_clean();
            ob_start();
            Excel::create($excelName, function($excel) use($buyerDataExcelSelected) {
                $excel->sheet('Sheet 1', function($sheet) use($buyerDataExcelSelected) {
                    $sheet->fromArray($buyerDataExcelSelected);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            
            return response()->json(['path' => $creatingPath]);
            
        }
    }

    /**
     * Method used to show the Role for Admin Users
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function editRole($id = 0) {

        
        if (!empty($id)) {
           
            $userAdminDetails = UserAdmin::find($id);
         
           
        } else {
            dd('No User Found! Please Click on the Back button of the browser');
        }

        

        if ($userAdminDetails->id =='') {
            dd('No User Found! Please Click on the Back button of the browser');
        }

        $userTypeId = $userAdminDetails->userType;

        if($userAdminDetails->id != Auth::user()->id) {
            if($userAdminDetails->userType == 1){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            if((Auth::user()->userType == 3 || Auth::user()->userType == 4) && ($userAdminDetails->id != Auth::user()->id)) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
        }
        $data = array();
        $permissionRecord = Permission::all(); // to fetch the core permissions from base table
        $permissionUserRecord = UserTypeDefaultPermission::getPermissionByUserTypeId($userTypeId);



        $data['permissionCount'] = count($permissionRecord);

        $permissionRecordEntered = UserPermission::getPermissionRecords($id, 'adminUserId');

        #dd($permissionRecordEntered);

        if (count($permissionRecordEntered) == 0) {
            // if we found anyone's permissions are empty, at first insert the permission to that individual users
            foreach ($permissionUserRecord as $val) {
                $perm = UserPermission::insertPermission($val->adminMenuId, $id, $val->permissionView, $val->permissionAdd, $val->permissionEdit, $val->permissionDelete);
            }
        }

        $permissionRecordEntered = UserPermission::getPermissionRecords($id, 'adminUserId');

        $data['arr'] = $this->categoryTree($permissionRecord, $permissionRecordEntered);



        $data['title'] = "User Roles :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "User Roles";
        $data['contentTop'] = array('breadcrumbText' => 'User Roles', 'contentTitle' => 'User Roles', 'pageInfo' => 'This section allows you to manage to change individual user roles');
        $data['userId'] = $userAdminDetails->id;

        $data['name'] = $userAdminDetails->firstName. " ". $userAdminDetails->lastName;
        $data['email'] = $userAdminDetails->email;

        $data['name'] = $userAdminDetails->firstName . " " . $userAdminDetails->lastName;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        return view('Administrator.adminusers.permission', $data);
    }

    function categoryTree($permissionRecord, $permissionRecordEntered, $parent_id = 0, $sub_mark = '') {
        $html = "";
        $event = "event";

        $arrowImagePath = url('/') . "/public/administrator/img/enter_row.png";
        foreach ($permissionRecord as $row) {

            foreach ($permissionRecordEntered as $keyword => $value) {
                if ($row->id == $value->adminMenuId) {
                    $checkedView = $value->permissionView == 1 ? 'checked' : '';
                    $checkedAdd = $value->permissionAdd == 1 ? 'checked' : '';
                    $checkedEdit = $value->permissionEdit == 1 ? 'checked' : '';
                    $checkedDelete = $value->permissionDelete == 1 ? 'checked' : '';
                    if ($checkedView == 'checked' || $checkedAdd == 'checked' || $checkedEdit == 'checked' || $checkedDelete == 'checked') {
                        $checkedParent = "checked";
                    } else {
                        $checkedParent = "";
                    }
                }
            }

            if ($row->adminMenuParentId == $parent_id) {
                if ($sub_mark == '') {

                    //if($permissionRecordEntered[1]->permissionView==0){echo 'checked';} 

                    $html .='<tr class="firstRow">
                <td><label><input type="checkbox" ' . $checkedParent . ' name="chk_' . $row->id . '" value="' . $row->id . '" id=' . $string = preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . ' class="flat-red me">
                        <span class="radioSpan">' . $row->adminMenuName . '</span></label>
                </td>
                <span id="' . $row->adminMenuName . '">
                <td><label><input type="checkbox" ' . $checkedView . ' name="view_' . $row->id . '" value="1" id="view' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . 'event);"></label></td>
                <td><label><input type="checkbox" ' . $checkedAdd . ' name="add_' . $row->id . '" value="1" id="add' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . 'event);"></label></td>
                <td><label><input type="checkbox" ' . $checkedEdit . ' name="edit_' . $row->id . '" value="1" id="edit' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . 'event);"></label></td>
                <td><label><input type="checkbox" ' . $checkedDelete . ' name="delete_' . $row->id . '" value="1" id="delete' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . 'event);"></label>
                </td>
                </span>
                </tr>';
                } else if ($sub_mark == '---') {
                    $html .='<tr>
                        <td style="padding-left:10px;"><img src=' . $arrowImagePath . '>
                            <label><input type="checkbox" ' . $checkedParent . ' name="chk_' . $row->id . '" value="' . $row->id . '" id=' . $string = preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . ' class="flat-red me ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '">
                                <span class="radioSpan">' . $row->adminMenuName . '</span></label>
                        </td>
                <span id="spanID' . $row->id . '">
                <td><label><input type="checkbox" ' . $checkedView . ' name="view_' . $row->id . '" value="1" id="view' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedAdd . ' name="add_' . $row->id . '" value="1" id="add' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedEdit . ' name="edit_' . $row->id . '" value="1" id="edit' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedDelete . ' name="delete_' . $row->id . '" value="1" id="delete' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label>
                </span>
                </tr>';
                } else {
                    $html .='<tr>
                        <td style="padding-left:40px;"><img src=' . $arrowImagePath . '>
                            <label><input type="checkbox" ' . $checkedParent . ' name="chk_' . $row->id . '" value="' . $row->id . '" id=' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . ' class="flat-red me ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '">
                                <span class="radioSpan">' . $row->adminMenuName . '</span></label>
                        </td>
                <td><label><input type="checkbox" ' . $checkedView . ' name="view_' . $row->id . '" value="1" id="view' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . "'," . "'" . $event . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedAdd . ' name="add_' . $row->id . '" value="1" id="add' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName . "'," . "'" . $event) . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedEdit . ' name="edit_' . $row->id . '" value="1" id="edit' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName . "'," . "'" . $event) . "'" . ');"></label></td>
                <td><label><input type="checkbox" ' . $checkedDelete . ' name="delete_' . $row->id . '" value="1" id="delete' . $row->id . '" class="flat-red ' . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName) . '" onclick="check_btn(' . "'" . preg_replace("/[^a-zA-Z]/", "", $row->adminMenuName . "'," . "'" . $event) . "'" . ');"></label>
                </tr>';
                }

                $html .='' . $this->categoryTree($permissionRecord, $permissionRecordEntered, $row->id, $sub_mark . '---');
                /* $html .= '</span></label>';
                  $html .="</td></tr>"; */
            }
        }
        return $html;
    }

    // User Role Modification
    public function saveRole(Request $request, $id) {
        //dd($request->All());
        //dd($request->chk_1);;

        $lastMenuId = Permission::orderby('id', 'desc')->first();


        for ($i = 1; $i <= $lastMenuId->id; $i++) {
            $adminMenuId = 'chk_' . $i;
            $adminMenuId2 = $request->$adminMenuId;


            $permView = 'view_' . $i;
            $permAdd = 'add_' . $i;
            $permEdit = 'edit_' . $i;
            $permDelete = 'delete_' . $i;

            $permView2 = $request->$permView;
            $permAdd2 = $request->$permAdd;
            $permEdit2 = $request->$permEdit;
            $permDelete2 = $request->$permDelete;


            if (isset($permView2)) {
                $setview = $permView2;
            } else {
                $setview = 0;
            }


            if (isset($permAdd2)) {
                $setadd = $permAdd2;
            } else {
                $setadd = 0;
            }

            if (isset($permEdit2)) {
                $setedit = $permEdit2;
            } else {
                $setedit = 0;
            }

            if (isset($permDelete2)) {
                $setdelete = $permDelete2;
            } else {
                $setdelete = 0;
            }


            if ($setadd == null) {
                $setadd = 0;
            }

            if ($setedit == null) {
                $setedit = 0;
            }

            if ($setview == null) {
                $setview = 0;
            }

            if ($setdelete == null) {
                $setdelete = 0;
            }

            // ********* Insert Or update the data into user type permission table **************
            $perm = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $i, 'adminUserId' => $id));
            $perm->permissionView = $setview;
            $perm->permissionAdd = $setadd;
            $perm->permissionEdit = $setedit;
            $perm->permissionDelete = $setdelete;
            $perm->adminMenuId = $i;
            $perm->adminUserId = $id;
            $perm->save();

            /*if($setview == 1 || $setadd == 1 || $setedit == 1 || $setdelete == 1){

                $recordMasterRoleSecondLevel = Adminmenuuserpermission::recordmasterRole($i);
                if($recordMasterRoleSecondLevel[0]->adminMenuParentId != 0){
                    $permMaster = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $recordMasterRoleSecondLevel[0]->adminMenuParentId, 'adminUserId' => $id));
                    $permMaster->permissionView = 1;
                    $permMaster->permissionAdd = 1;
                    $permMaster->permissionEdit = 1;
                    $permMaster->permissionDelete = 1;
                    $permMaster->save();

                    $recordMasterRoleFirstLevel = Adminmenuuserpermission::recordmasterRole($recordMasterRoleSecondLevel[0]->adminMenuParentId);
                    if($recordMasterRoleFirstLevel[0]->adminMenuParentId != 0){
                    $permMaster = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $recordMasterRoleFirstLevel[0]->adminMenuParentId, 'adminUserId' => $id));
                    $permMaster->permissionView = 1;
                    $permMaster->permissionAdd = 1;
                    $permMaster->permissionEdit = 1;
                    $permMaster->permissionDelete = 1;
                    $permMaster->save();
                    }

                }

            }*/



            // this is for default permissions add function into the userType permission table (for type: 2, 3, 4)
            //$usertypePermCreation = Permission::menuCreation($i, $id);
        }


        return redirect('/administrator/adminusers')->with('successMessage', 'Permission set successfully');

        //dd('end');
    }

    /**
     * Method used to clear search history
     * @return type
     */
    public function showall() {
        \Session::forget('BUYERDATA');
        return \Redirect::to('administrator/buyers');
    }


    /**
     * Method used to inherit/export permissions to admin users
     * @param integer $page
     * @param request $request
     * @return type
     */
    public function inheritPermission($page, Request $request) {
        
        foreach($request->selected as $keyUser => $valUser){
            $userAdmin = UserAdmin::find($valUser);

            /* GET THE USER TYPE */
            $userTypeId = $userAdmin->userType;

            /* DELETE USER */
            Adminmenuuserpermission::where('adminUserId', $valUser)->delete();

            /* GET THE DEFAULT PERMISSIONS */
            $UserTypeDefaultPermission = UserTypeDefaultPermission::getPermissionByUserTypeId($userTypeId);

            /* INSERT THE PERMISSIONS INTO ADMIN USERS PERM TABLE */
            foreach ($UserTypeDefaultPermission as $val) {
                $perm = UserPermission::insertPermission($val->adminMenuId, $valUser, $val->permissionView, $val->permissionAdd, $val->permissionEdit, $val->permissionDelete);
            }
        }

        $returnValue = "Completed";
        return response()->json(['returnValue' => $returnValue]);
    }

}
