<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ResellItem;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;
use DB;
class ResellitemsController extends Controller {

    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index() {

        //echo '77';exit;
        $data = array();

        $resellItemObj = new Resellitem();
        
        if (\Request::isMethod('post')) {
            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            
                        
            \Session::forget('RESELLITEM');
            \Session::push('RESELLITEM.searchData', $searchData);
            \Session::push('RESELLITEM.searchDisplay', $perpage);
            \Session::push('RESELLITEM.field', $sortField);
            \Session::push('RESELLITEM.type', $sortOrder);

            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.ResellItems'), Auth::user()->userType); // call the helper function
            if($findRole['canView'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
           
        } 
        else {           
                $sortField = \Session::get('RESELLITEM.field');
                $sortType = \Session::get('RESELLITEM.type');
                $perpage = \Session::get('RESELLITEM.searchDisplay');
                $searchData = \Session::get('RESELLITEM.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.ResellItems'), Auth::user()->userType); // call the helper function
                
                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
        }
        $param = array();
        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        
        $records = $resellItemObj->getData($param);
        #dd($records);
        $data['param'] = $param;
        $data['page'] = $records->currentPage();
        $data['records'] = $records;       
        $data['forntendUrl'] = Config::get('constants.frontendUrl');
        
        $data['pageTitle'] = 'Resell Items';
        $data['title'] = "Resell Items :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => 'Resell Items Management', 'contentTitle' => 'Resell Items', 'pageInfo' => 'This section allows you to manage resell items');
        

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.resellitem.index', $data);
    }

    public function addeditresellitems($id = -1, $page = 1) {

        $resellItemObj = new Resellitem();
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $resellItemObj->find($id);
            $data['record'] = $record;
        } else {
            $data['action'] = 'Add';
            $data['record'] = $resellItemObj;
        }
        
        $data['page'] = $page;
        
        $data['pageTitle'] = $data['action'] . ' Resell Items';
        $data['title'] = "Resell Items :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Resell Items', 'Add/Edit Resell Items'), 'contentTitle' => 'Resell Items', 'pageInfo' => 'This section allows you to manage resell items');
       
        return view('Administrator.resellitem.addeditpage', $data);
    }

    public function addeditresellitemsrecord($id = -1, $page = '-1', Request $request) {

        $resellItemObj = new Resellitem();
        
        if ($id != '-1') {
            $resellItemObj = $resellItemObj->find($id);
            
        }
        
        //if ($id == '-1')
        
        $resellItemObj->title   = $request->input('title');
        $resellItemObj->description = ($request->input('description'));
        
        if ($resellItemObj->save())
            return redirect(route('resellitems'))->with('successMessage', 'Information saved successfuly.');
    }

    public function editresellitemsstatus($id = -1, $status) {
        $resellItemObj = new Resellitem();
        if ($id != -1)
            $resellItemObj = $resellItemObj->find($id);
            $resellItemObj->status = $status;

        if ($resellItemObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function deleterecord($id = -1,$page = 1) {

        $resellItemObj = new Resellitem();

        if ($id != '-1') {
            $resellItemObj = $resellItemObj->find($id);
            if ($resellItemObj->delete())
                return redirect(route('resellitems'))->with('successMessage', 'Information deleted successfully.');
        }
    }

}
