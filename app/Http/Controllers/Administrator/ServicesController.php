<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Service;
use App\Model\Sellers;
use App\Model\Sitecategory;
use App\Model\Serviceattributes;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;

class ServicesController extends Controller {

    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
        $this->html = '';
    }

    public function index() {

        //echo '77';exit;
        $data = array();

        $serviceObj = new Service();

        if (\Request::isMethod('post')) {
            

            /* GET POST VALUE  */

            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            
                        
            \Session::forget('SERVICE');
            \Session::push('SERVICE.searchData', $searchData);
            \Session::push('SERVICE.searchDisplay', $perpage);
            \Session::push('SERVICE.field', $sortField);
            \Session::push('SERVICE.type', $sortOrder);

            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Services'), Auth::user()->userType); // call the helper function
            if($findRole['canView'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
           
        } 
        else {           

                $sortField = \Session::get('SERVICE.field');
                $sortType = \Session::get('SERVICE.type');
                $perpage = \Session::get('SERVICE.searchDisplay');
                $searchData = \Session::get('SERVICE.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Services'), Auth::user()->userType); // call the helper function


                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
           
        }

        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        
        $records = $serviceObj->getData($param);

        $data['param'] = $param;
        $data['page'] = $records->currentPage();
        $data['records'] = $records;       
        $data['forntendUrl'] = Config::get('constants.frontendUrl');
        
        $data['pageTitle'] = 'Services';
        $data['title'] = "Services :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Services', 'Services'), 'contentTitle' => 'Services', 'pageInfo' => 'This section allows you to manage services');
        

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.services.index', $data);
    }

    public function getsubcategorylist(Request $request){
        $subcat_id = $request->subcat_id;
        $serviceObj = new Service();
        $data['CategoryList'] = Sitecategory::where('parentCategoryId',$request->catid)->where('status', '1')->where('is_deleted', '0')->orderBy('categoryName', 'ASC')->get();

        $showDiv = '';
        $showDiv .= '<div class="col-md-12"><div class="form-group">
                    <label>Service Sub Category</label>                               
                    <select name="serviceSubCatId" id="serviceSubCatId" class="customSelect form-control" required="" onchange="">
                    <option value="0">Select Sub Category</option>';
        if(isset($data['CategoryList']) && $data['CategoryList'] != ''){
            foreach($data['CategoryList'] as $val) {
            if($val["id"]== $subcat_id)
                $selected = 'selected';
            else
                $selected = '';                         
        $showDiv .= '<option value="'.$val["id"].'" '.$selected.'>'.$val["categoryName"].'</option>';
        }}                           
                                    
        $showDiv .= '</select>
                    </div></div>';
        echo $showDiv;exit;
    }

    public function addeditservice($id = -1, $page = 1) {

        $serviceObj = new Service();
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $serviceObj->find($id);
            $data['record'] = $record;
            $data['catId'] = $data['record']['serviceCatId'];
        } else {
            $data['action'] = 'Add';
            $data['record'] = $serviceObj;
            $data['catId'] = 0;
        }


        
        $data['page'] = $page;

        $data['sellerList'] = Sellers::where('status', '1')->where('deleted', '0')->orderBy('firstName', 'ASC')->get();

        $data['treeView']  = $this->categoryTree(0,'',$data['catId']);

        //$data['CategoryList'] = Sitecategory::where('parentCategoryId', '0')->where('status', '1')->where('is_deleted', '0')->orderBy('categoryName', 'ASC')->get();
        
        $data['pageTitle'] = $data['action'] . ' Service';
        $data['title'] = "Services :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Services', 'Add/Edit Service'), 'contentTitle' => 'Services', 'pageInfo' => 'This section allows you to manage services');
       
        return view('Administrator.services.addeditpage', $data);
    }

    function categoryTree($parent_id = 0, $sub_mark = '',$catId){
   
    $categoryList = Sitecategory::where('parentCategoryId', $parent_id)->get()->toArray(); 
   
    if(count($categoryList)> 0){
        foreach($categoryList as $row){
            $selected = '';
            if($row['id'] == $catId){
                $selected = "selected='selected'";
            }
           $this->html .= "<option value='".$row['id']."' ".$selected.">".$sub_mark.$row['categoryName']."</option>";
            $this->categoryTree($row['id'], $sub_mark.'&nbsp;',$catId);

            
        }
    }

    return $this->html;

    
   }


   public function addeditserviceattribute($id = -1, $page = 1) {

        $serviceObj = new Serviceattributes();   
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = Serviceattributes::where('serviceId',$id)
            ->where('status','1')->where('deleted','0')->get();
           
            $data['records'] = $record;


           
        } else {
            $data['action'] = 'Add';
            $data['record'] = $serviceObj;
           
        }


        
        $data['page'] = $page;

        $data['pageTitle'] = 'Service Attributes';
        $data['title'] = "Services :: ADMIN - Bigwau";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Services', 'Add/Edit Attribute'), 'contentTitle' => 'Services', 'pageInfo' => 'This section allows you to manage service attributes');
       
        return view('Administrator.services.addeditattribute', $data);
    }


    public function addeditserviceattributerecord($id = -1, $page = '-1', Request $request) {
        
        $serviceObj = new Serviceattributes();        
        $input = $request->all();
        $attributes = $input['attributeName'];
        $attributesVal = $input['attributeVal'];

        

        Serviceattributes::where('serviceId', $id)->delete();
       
        for($count=0;$count<count($attributes);$count++){


            $obj = 'serviceobj_'.$count;
            $obj = new Serviceattributes();
            
            echo $obj->serviceId  = $id;
            echo $obj->attributeName  = $attributes[$count];
            echo $obj->attributeVal   = $attributesVal[$count];
            echo $obj->createdOn      =  \Carbon\Carbon::parse($request->createdon)->format('Y-m-d');

            $obj->save();

        }
        return redirect(route('servicelist'))->with('successMessage', 'Information saved successfuly.');
    }

    public function addeditservicerecord($id = -1, $page = '-1', Request $request) {
        $serviceObj = new Service();
        $uploadImage = 0;

        if ($request->logo) {
            $this->validate($request, [
                'logo' => 'image|mimes:jpeg,png,jpg,gif|max:4000',
            ]);

            $validator = Validator::make(\Input::all(), [
                        'serviceName' => 'required',
                        'logo' => 'image|mimes:jpeg,png,jpg,gif|max:4000',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {

                /* Upload banner image */
                if ($request->hasFile('logo')) {
                    $image = $request->file('logo');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/service');
                    $image->move($destinationPath, $name);
                    $serviceObj->image = $name;
                    $uploadImage = 1;
                }
            }
        }

        if ($id != '-1') {
            $serviceObj = $serviceObj->find($id);
            if ($uploadImage == 1)
                $serviceObj->image = $name;
        }

        $data['sellerList'] = Sellers::where('status', '1')->where('deleted', '0')->orderBy('firstName', 'ASC')->get();

        

        $data['CategoryList'] = Sitecategory::where('status', '1')->where('is_deleted', '0')->orderBy('categoryName', 'ASC')->get();
        
        
        $serviceObj->serviceName = $request->serviceName;
        $serviceObj->sellerId = $request->sellerId;
        $serviceObj->serviceCatId = $request->serviceCatId;
        $serviceObj->description = $request->description;
        $serviceObj->createdOn = !empty($request->createdon) ? \Carbon\Carbon::parse($request->createdon)->format('Y-m-d') : '';
        if ($uploadImage)
            $serviceObj->image = $name;

        if ($serviceObj->save())
            return redirect(route('addeditserviceattribute',['id' =>$serviceObj->id]))->with('successMessage', 'Information saved successfuly.');
    }
/*
    public function editservicestatus($id = -1, $status) {

        $serviceObj = new Service();
        if ($id != -1)
            $serviceObj = $serviceObj->find($id);
        $serviceObj->status = $status;

        if ($serviceObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }*/

    public function editservicestatus($id, $status) {

        /* GET USER ID  */
        $createrModifierId = Auth::user()->id;

        /* CHECK WHETHER THE USER ID IS NOT EMPTY  */
        if (!empty($id)) {

            /* CHANGE STATUS  */
            if (Service::changeStatus($id, $status)) {

                /* STATUS HAS CHANGED SUCCESSFULLY  */
                return \Redirect::to('administrator/servicelist/')->with('successMessage', 'Service status changed successfully.');
            } else {

                /* UNSUCCESSFULLY ATTEMPT */
                return \Redirect::to('administrator/servicelist/')->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/servicelist/')->with('errorMessage', 'Error in operation!');
        }
    }

    public function deleteservice($id = -1) {

        $serviceObj = new Service();

        if (!empty($id)) {

            /* CHANGE STATUS  */
            if (Service::deleteStatus($id)) {

                /* STATUS HAS CHANGED SUCCESSFULLY  */
                return \Redirect::to('administrator/servicelist/')->with('successMessage', 'Service deleted successfully.');
            } else {

                /* UNSUCCESSFULLY ATTEMPT */
                return \Redirect::to('administrator/servicelist/')->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/servicelist/')->with('errorMessage', 'Error in operation!');
        }
    }

}
