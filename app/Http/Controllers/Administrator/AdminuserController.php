<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Model\Country;
use App\Model\UserAdmin;
use App\Model\Permission;
use App\Model\UserPermission;
use App\Model\UserTypeDefaultPermission;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Hash;
use Illuminate\Routing\Route;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Model\Adminmenuuserpermission;
use customhelper;
//use App\Model\Emailtemplate;

class AdminuserController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AdminUsers'), Auth::user()->userType); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('ADMINUSERDATA');
            \Session::push('ADMINUSERDATA.searchDisplay', $searchDisplay);
            \Session::push('ADMINUSERDATA.field', $field);
            \Session::push('ADMINUSERDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;



            // Search fields if any
            if ($request->has('search_firstName')) {
                $param['search_firstName'] = $request->search_firstName;
                \Session::push('ADMINUSERDATA.search_firstName', $request->search_firstName);
            }
            if ($request->has('search_lastName')) {
                $param['search_lastName'] = $request->search_lastName;
                \Session::push('ADMINUSERDATA.search_lastName', $request->search_lastName);
            }
            if ($request->has('search_email')) {
                $param['search_email'] = $request->search_email;
                \Session::push('ADMINUSERDATA.search_email', $request->search_email);
            }
            if ($request->has('search_status')) {
                $param['search_status'] = $request->search_status;
                \Session::push('ADMINUSERDATA.search_status', $request->search_status);
            }
            if ($request->has('search_rangedate')) {
                $param['search_rangedate'] = $request->search_rangedate;
                \Session::push('ADMINUSERDATA.search_rangedate', $request->search_rangedate);
                if ($param['search_rangedate'] == 'custom') {
                    $param['search_reservation'] = $request->search_reservation;
                    \Session::push('ADMINUSERDATA.search_reservation', $request->search_reservation);
                }
            }
            
            #dd(session()->all());
        } else {
            $sortField = \Session::get('ADMINUSERDATA.field');
            $sortType = \Session::get('ADMINUSERDATA.type');
            $searchDisplay = \Session::get('ADMINUSERDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }



        /* BUILD SORTING ARRAY */
        $sort = array(
            'firstName' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'userType' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';


        $adminUserData = UserAdmin::getAdminUserList($param);

        $data['title'] = "Administrative Panel :: Admin Users";
        $data['contentTop'] = array('breadcrumbText' => 'Admin Users', 'contentTitle' => 'Admin Users', 'pageInfo' => 'This section allows you to manage admin profile');
        $data['pageTitle'] = "Admin Users";
        $data['page'] = $adminUserData->currentPage();
        $data['adminUserData'] = $adminUserData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        
        return view('Administrator.adminusers.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AdminUsers'), Auth::user()->userType); // call the helper function

        $data['title'] = "Administrative Panel :: Admin Users";
        $data['contentTop'] = array('breadcrumbText' => array('Admin Users','Add/Edit User'), 'contentTitle' => 'Admin Users', 'pageInfo' => 'This section allows you to manage admin profile');
        $data['pageTitle'] = "Admin Users";
        $data['page'] = !empty($page) ? $page : '1';
       
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $userAdmin = UserAdmin::find($id);
           
            $fetchAdminUserType = UserAdmin::fetchAdminUserTypeDefault();
           
            $data['userAdmin'] = $userAdmin;
            
            $data['typeAdminUser'] = $fetchAdminUserType;
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
           
            $fetchAdminUserType = UserAdmin::fetchAdminUserTypeDefault();
            $data['typeAdminUser'] = $fetchAdminUserType;
        }
        return view('Administrator.adminusers.addedit', $data);
    }

    /*     * Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $userAdmin = new UserAdmin;

        $validator = Validator::make($request->all(), [
                    'firstName' => 'required:' . $userAdmin->table . ',firstName,' . $id,
                    'lastName' => 'required:' . $userAdmin->table . ',lastName,' . $id,
                    'company' => 'required|min:3|max:100:' . $userAdmin->table . ',company,' . $id,
                    //'email' => 'required|email:' . $userAdmin->table . ',email,' . $id,
                    'contactno' => 'required|numeric:' . $userAdmin->table . ',contactno,' . $id,
                        //'warehouseid' => 'required|numeric:' . $userAdmin->table . ',warehouseid,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $userAdmin = UserAdmin::find($id);
                $userAdmin->updatedBy = Auth::user()->id;
                $userAdmin->updatedOn = Config::get('constants.CURRENTDATE');
            }
            $userAdmin->firstName = $request->firstName;
            $userAdmin->lastName = $request->lastName;
            $userAdmin->contactno = $request->contactno;            
            $userAdmin->company = $request->company;
            $userAdmin->userType = $request->usertype;
            //$userAdmin->email = $request->email;
            $userAdmin->save();
            $userAdminId = $userAdmin->id;


           

            return redirect('/administrator/adminusers')->with('successMessage', 'Admin User information saved successfuly.');
        }
    }

    /** Add
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function adddata($page, Request $request) {



        #dd($request->warehouseId);
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $userAdmin = new UserAdmin;

        $validator = Validator::make($request->all(), [
                    'firstName' => 'required:' . $userAdmin->table . ',firstName,',
                    'lastName' => 'required:' . $userAdmin->table . ',lastName,',
                    'company' => 'required|min:3|max:100:' . $userAdmin->table . ',company,',
                    'email' => 'required|email|unique:'.$userAdmin->table .  ',email,',
                    'contactno' => 'required|numeric:' . $userAdmin->table . ',contactno,',
                        //'warehouseid' => 'required|numeric:' . $userAdmin->table . ',warehouseid,' . $id,
        ]);

        if ($validator->fails()) {
            
            $values['firstName'] = $request->firstName;
            $values['lastName'] = $request->lastName;
            $values['company'] = $request->company;
            $values['contactno'] = $request->contactno;
            $values['email'] = $request->email;           
            $values['usertype'] = $request->usertype;

            return redirect()->back()->withErrors($validator->errors())->withInput($values);
        } else {
            $userAdmin->firstName = $request->firstName;
            $userAdmin->lastName = $request->lastName;
            $userAdmin->contactno = $request->contactno;          
            $userAdmin->company = $request->company;
            $userAdmin->email = $request->email;
            $rawPassword = $this->generateRandomString();
            $userAdmin->password = Hash::make($rawPassword);
            $userAdmin->remember_token = Hash::make($rawPassword);
            $userAdmin->userType = 1;
            $userAdmin->createdBy = Auth::user()->id;
            $userAdmin->status = 1;
            $userAdmin->userType = $request->usertype;
            //$userAdmin->createdBy = Auth::user()->id;
            //$userAdmin->email = $request->email;
            $userAdmin->save();
            $userAdminId = $userAdmin->id;

            /* ++++++++++ email functionality ++++++++ */
            /*$Newuser = UserAdmin::find($userAdminId);
            $emailTemplate = Emailtemplate::where('templateKey', 'creation_admin')->first();
            $replace['[NAME]'] = $Newuser->firstName . " " . $Newuser->lastName;
            $replace['[PASSWORD]'] = $rawPassword;
            $replace['[EMAIL]'] = $Newuser->email;
            $to = $Newuser->email;
            customhelper::SendMail($emailTemplate, $replace, $to);*/
            /* ++++++++++ end of email functionality ++++ */

            // *********** Get the values for the permission of an user type ***************** //


            $permissionRecordEntered = Permission::getPermissionRecords($request->usertype, 'userTypeId');

            foreach ($permissionRecordEntered as $val) {
                // Insert the permission from default table to user table during user creation
                $perm = UserPermission::insertPermission($val->adminMenuId, $userAdminId, $val->permissionView, $val->permissionAdd, $val->permissionEdit, $val->permissionDelete);
            }

            return redirect('/administrator/adminusers')->with('successMessage', 'Admin User information created successfuly.');
        }
    }

    // Generate random password
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (UserAdmin::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/adminusers/?page=' . $page)->with('successMessage', 'Admin User status changed successfully.');
            } else {
                return \Redirect::to('administrator/adminusers/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/adminusers/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to change password
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changepassword($id, $page, Request $request) {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = $id;

        if (\Request::isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'new_password'     => 'required|min:6',
                'confirm_password' => 'required|same:new_password',
                ]);

                if ($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()->all()]);
                    //return redirect()->back()->withErrors($validator->errors());
                }    
                else { 
                    //$data = array();
                    //$data = $request->all();
             
                    $user = UserAdmin::find($request->id);
                    $credentials['password'] = $request->confirm_password;

                    //$user = $request->userId;
                    $user->password = Hash::make($credentials['password']);
                    $user->passwordLastChanged = Config::get('constants.CURRENTDATE');
                    $user->save();

                    /* ++++++++++ email functionality ++++++++ */
                   /* $emailTemplate = Emailtemplate::where('templateKey', 'change_password_admin')->first();
                    $replace['[NAME]'] = $user->firstName . " " . $user->lastName;
                    $replace['[PASSWORD]'] = $request->confirm_password;
                    $to = $user->email;
                    customhelper::SendMail($emailTemplate, $replace, $to);*/
                    /* ++++++++++ end of email functionality ++++ */


                    return response()->json(['success'=>'Password has changed successfuly.']);
                    //return Redirect::to('administrator/password/reset')->with('successMessage', 'Password have changed successfuly.');
                    //return $user->save() ? Password::PASSWORD_RESET : 'Error';
                    
                }
        }
            return view('Administrator.adminusers.changepassword', $data);

       
    }



    /**
     * Method used to show the Role for Admin Users
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function editRole($id = 0) {

        
        if (!empty($id)) {
           
            $userAdminDetails = UserAdmin::find($id);
         
           
        } else {
            dd('No User Found! Please Click on the Back button of the browser');
        }

        

        if ($userAdminDetails->id =='') {
            dd('No User Found! Please Click on the Back button of the browser');
        }

        $userTypeId = $userAdminDetails->userType;

        if($userAdminDetails->id != Auth::user()->id) {
            if($userAdminDetails->userType == 1){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            if((Auth::user()->userType == 3 || Auth::user()->userType == 4) && ($userAdminDetails->id != Auth::user()->id)) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
        }
        $data = array();
        $permissionRecord = Permission::all(); // to fetch the core permissions from base table
        $permissionUserRecord = UserTypeDefaultPermission::getPermissionByUserTypeId($userTypeId);



        $data['permissionCount'] = count($permissionRecord);

        $permissionRecordEntered = UserPermission::getPermissionRecords($id, 'adminUserId');

        #dd($permissionRecordEntered);

        if (count($permissionRecordEntered) == 0) {
            // if we found anyone's permissions are empty, at first insert the permission to that individual users
            foreach ($permissionUserRecord as $val) {
                $perm = UserPermission::insertPermission($val->adminMenuId, $id, $val->permissionView, $val->permissionAdd, $val->permissionEdit, $val->permissionDelete);
            }
        }

        $permissionRecordEntered = UserPermission::getPermissionRecords($id, 'adminUserId');

        $data['arr'] = $this->categoryTree($permissionRecord, $permissionRecordEntered);



        $data['title'] = "User Roles :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "User Roles";
        $data['contentTop'] = array('breadcrumbText' => 'User Roles', 'contentTitle' => 'User Roles', 'pageInfo' => 'This section allows you to manage to change individual user roles');
        $data['userId'] = $userAdminDetails->id;

        $data['name'] = $userAdminDetails->firstName. " ". $userAdminDetails->lastName;
        $data['email'] = $userAdminDetails->email;

        $data['name'] = $userAdminDetails->firstName . " " . $userAdminDetails->lastName;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminUsers57');

        return view('Administrator.adminusers.permission', $data);
    }

   

    // User Role Modification
    public function saveRole(Request $request, $id) {
        //dd($request->All());
        //dd($request->chk_1);;

        $lastMenuId = Permission::orderby('id', 'desc')->first();


        for ($i = 1; $i <= $lastMenuId->id; $i++) {
            $adminMenuId = 'chk_' . $i;
            $adminMenuId2 = $request->$adminMenuId;


            $permView = 'view_' . $i;
            $permAdd = 'add_' . $i;
            $permEdit = 'edit_' . $i;
            $permDelete = 'delete_' . $i;

            $permView2 = $request->$permView;
            $permAdd2 = $request->$permAdd;
            $permEdit2 = $request->$permEdit;
            $permDelete2 = $request->$permDelete;


            if (isset($permView2)) {
                $setview = $permView2;
            } else {
                $setview = 0;
            }


            if (isset($permAdd2)) {
                $setadd = $permAdd2;
            } else {
                $setadd = 0;
            }

            if (isset($permEdit2)) {
                $setedit = $permEdit2;
            } else {
                $setedit = 0;
            }

            if (isset($permDelete2)) {
                $setdelete = $permDelete2;
            } else {
                $setdelete = 0;
            }


            if ($setadd == null) {
                $setadd = 0;
            }

            if ($setedit == null) {
                $setedit = 0;
            }

            if ($setview == null) {
                $setview = 0;
            }

            if ($setdelete == null) {
                $setdelete = 0;
            }

            // ********* Insert Or update the data into user type permission table **************
            $perm = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $i, 'adminUserId' => $id));
            $perm->permissionView = $setview;
            $perm->permissionAdd = $setadd;
            $perm->permissionEdit = $setedit;
            $perm->permissionDelete = $setdelete;
            $perm->adminMenuId = $i;
            $perm->adminUserId = $id;
            $perm->save();

            /*if($setview == 1 || $setadd == 1 || $setedit == 1 || $setdelete == 1){

                $recordMasterRoleSecondLevel = Adminmenuuserpermission::recordmasterRole($i);
                if($recordMasterRoleSecondLevel[0]->adminMenuParentId != 0){
                    $permMaster = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $recordMasterRoleSecondLevel[0]->adminMenuParentId, 'adminUserId' => $id));
                    $permMaster->permissionView = 1;
                    $permMaster->permissionAdd = 1;
                    $permMaster->permissionEdit = 1;
                    $permMaster->permissionDelete = 1;
                    $permMaster->save();

                    $recordMasterRoleFirstLevel = Adminmenuuserpermission::recordmasterRole($recordMasterRoleSecondLevel[0]->adminMenuParentId);
                    if($recordMasterRoleFirstLevel[0]->adminMenuParentId != 0){
                    $permMaster = Adminmenuuserpermission::firstOrNew(array('adminMenuId' => $recordMasterRoleFirstLevel[0]->adminMenuParentId, 'adminUserId' => $id));
                    $permMaster->permissionView = 1;
                    $permMaster->permissionAdd = 1;
                    $permMaster->permissionEdit = 1;
                    $permMaster->permissionDelete = 1;
                    $permMaster->save();
                    }

                }

            }*/



            // this is for default permissions add function into the userType permission table (for type: 2, 3, 4)
            //$usertypePermCreation = Permission::menuCreation($i, $id);
        }


        return redirect('/administrator/adminusers')->with('successMessage', 'Permission set successfully');

        //dd('end');
    }

    /**
     * Method used to clear search history
     * @return type
     */
    public function showall() {
        \Session::forget('ADMINUSERDATA');
        return \Redirect::to('administrator/adminusers');
    }


    /**
     * Method used to inherit/export permissions to admin users
     * @param integer $page
     * @param request $request
     * @return type
     */
    public function inheritPermission($page, Request $request) {
        
        foreach($request->selected as $keyUser => $valUser){
            $userAdmin = UserAdmin::find($valUser);

            /* GET THE USER TYPE */
            $userTypeId = $userAdmin->userType;

            /* DELETE USER */
            Adminmenuuserpermission::where('adminUserId', $valUser)->delete();

            /* GET THE DEFAULT PERMISSIONS */
            $UserTypeDefaultPermission = UserTypeDefaultPermission::getPermissionByUserTypeId($userTypeId);

            /* INSERT THE PERMISSIONS INTO ADMIN USERS PERM TABLE */
            foreach ($UserTypeDefaultPermission as $val) {
                $perm = UserPermission::insertPermission($val->adminMenuId, $valUser, $val->permissionView, $val->permissionAdd, $val->permissionEdit, $val->permissionDelete);
            }
        }

        $returnValue = "Completed";
        return response()->json(['returnValue' => $returnValue]);
    }

}
