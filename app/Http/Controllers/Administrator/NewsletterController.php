<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Newsletter;
use App\Model\Emailtemplate;
use App\Model\Subscriber;
use App\Model\Sellers;
use App\Model\Buyers;
use App\Model\Newslettersubscribermapping;
use App\Model\Newsletterscheduler;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Mail;
use customhelper;

class NewsletterController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index() {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Newsletter'), Auth::user()->userType); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchText = trim(\Input::get('search', ''));
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('NEWSLETTERDATA');
            \Session::push('NEWSLETTERDATA.field', $field);
            \Session::push('NEWSLETTERDATA.type', $type);
            \Session::push('NEWSLETTERDATA.searchDisplay', $searchDisplay);


            $param['field'] = !empty($field) ? $field : 'id';
            $param['type'] = !empty($type) ? $type : 'desc';
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('NEWSLETTERDATA.field');
            $sortType = \Session::get('NEWSLETTERDATA.type');
            $searchDisplay = \Session::get('NEWSLETTERDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'createdOn' => array('current' => 'sorting')
        );


        /* SET SORTING ARRAY  */

        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        $newsletterData = Newsletter::getNewsletterList($param);
        
       

        /* BUILD DATA FOR VIEW  */
        $data['title'] = "Newsletter Management :: ADMIN - Bigwau";
        $data = array();
        $data['pageTitle'] = "Newsletter Management";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Contents', 'Newsletter'), 'contentTitle' => 'Newsletter', 'pageInfo' => 'This section allows you to manage Newsletter');
        $data['page'] = $newsletterData->currentPage();
        $data['newsletterData'] = $newsletterData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.newsletter.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Newsletter'), Auth::user()->userType); // call the helper function
        if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $data['title'] = "Add Newsletter :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Newsletter :: Add";
        $data['contentTop'] = array('breadcrumbText' => 'Dashboard', 'contentTitle' => 'Newsletter', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '0';
        
        $data['emailTemplate'] = Emailtemplate::where('status', '1')->where('deleted', '0')->get(['id','templateKey']);
        $data['subscribers'] = Subscriber::where('status','1')->where('is_deleted', '0')->get()->pluck('email');

        $data['sellers'] = Sellers::where('status','1')->where('deleted', '0')->get()->pluck('email');
        $data['buyers'] = Buyers::where('status','1')->where('deleted', '0')->get()->pluck('email');
        
        if (!empty($id)) {
            $data['id'] = $id;
            $data['title'] = "ADMIN - Bigwau :: Newsletter Template :: Update Newsletter";
            $data['pageTitle'] = "Update Newsletter Template";
            $data['subscriberEmailselect'] = Newsletterscheduler::where('newsletterId',$id)->where('email_subscribers', '!=' , NULL)->get()->pluck('email_subscribers');
            
            $data['sellerEmailselect'] = Newsletterscheduler::where('newsletterId',$id)->where('email_sellers','!=',NULL)->get()->pluck('email_sellers');
            $data['buyerEmailselect'] = Newsletterscheduler::where('newsletterId',$id)->where('email_buyers','!=',NULL)->get()->pluck('email_buyers');
            $data['contentTop'] = array('breadcrumbText' => array('Manage Contents', 'Add Newsletter'), 'contentTitle' => 'Newsletter', 'pageInfo' => '');
            $content = Newsletter::find($id);
            $data['content'] = $content;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = "Add Newsletter";
            $data['content'] = array();
            $data['subscriberEmailselect'] = '';
            $data['sellerEmailselect'] = '';
            $data['buyerEmailselect'] = '';
        }

        return view('Administrator.newsletter.addedit', $data);
    }

    /**
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {
        $data['page'] = !empty($page) ? $page : '0';
        $data['id'] = 0;
        $scheduleTime = '';

        $newsletter = new Newsletter;
        
        $validator = Validator::make($request->all(), [
                    'emailtemplate'=> 'required',
                    'newsletterName' => 'required',
                    'scheduleNewsletterType' => 'required',
                    'scheduleRepeat' => ($request->scheduleNewsletterType != 4)?'required':'',
        ]);
      

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $newsletter = Newsletter::find($id);
            } 
            
            
            $newsletter->createdOn = \Carbon\Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s');
            $newsletter->createdBy = Auth::user()->id;
            $newsletter->emailTemplateKey = $request->emailtemplate;
            $newsletter->newsletterName = !empty($request->newsletterName) ? $request->newsletterName : '';
            $newsletter->scheduleNewsletterType  = !empty($request->scheduleNewsletterType) ? $request->scheduleNewsletterType : '';
            $newsletter->scheduleDate = !empty($request->scheduleDate) ? $request->scheduleDate: '';
            $newsletter->scheduleTime = !empty($request->scheduleTime && $request->scheduleNewsletterType == 4) ? $request->scheduleTime : '';
            $newsletter->scheduleRepeat = !empty($request->scheduleRepeat) ? $request->scheduleRepeat : 'N';
            $newsletter->save();
            $newsletterId = $newsletter->id;
            /*Add Subscriber, Seller & Buyers name to scheduler table*/
            if (!empty($id)) {
                $newsletterscheduler = Newsletterscheduler::where('newsletterId',$id)->where('email_subscribers','!=',NULL)->where('status','Q')->orWhere('status','P')->get()->pluck('email_subscribers')->toArray();
                $searchSubs = array_unique(array_merge($newsletterscheduler,$request->searchSubs));
                if(!empty($searchSubs)){
                    Newsletterscheduler::where('newsletterId',$id)->where('email_subscribers','!=',NULL)->delete();
                }

                $newsletterseller = Newsletterscheduler::where('newsletterId',$id)->where('email_sellers','!=',NULL)->where('status','Q')->orWhere('status','P')->get()->pluck('email_sellers')->toArray();
                $searchSellers = array_unique(array_merge($newsletterseller,$request->searchSellers));
                if(!empty($searchSellers)){
                    Newsletterscheduler::where('newsletterId',$id)->where('email_sellers','!=',NULL)->delete();
                }

                $newsletterbuyer = Newsletterscheduler::where('newsletterId',$id)->where('email_buyers','!=',NULL)->where('status','Q')->orWhere('status','P')->get()->pluck('email_buyers')->toArray();
                $searchBuyers = array_unique(array_merge($newsletterbuyer,$request->searchBuyers));
                if(!empty($searchBuyers)){
                    Newsletterscheduler::where('newsletterId',$id)->where('email_buyers','!=',NULL)->delete();
                }
            }else{
                $searchSubs = $request->searchSubs;
                $searchSellers = $request->searchSellers;
                $searchBuyers = $request->searchBuyers;
            }

            foreach($searchSubs as $emails_sub)
            {
                $data = [
                    'newsletterId' => $newsletterId,
                    'email_subscribers' => $emails_sub,
                   ]; 
                   Newsletterscheduler::insert($data);
            }

            foreach($searchSellers as $emails_sellers)
            {
                $data = [
                    'newsletterId' => $newsletterId,
                    'email_sellers' => $emails_sellers,
                   ]; 
                   Newsletterscheduler::insert($data);
            }

            foreach($searchBuyers as $emails_buyers)
            {
                $data = [
                    'newsletterId' => $newsletterId,
                    'email_buyers' => $emails_buyers,
                   ]; 
                   Newsletterscheduler::insert($data);
            }
            
            return redirect('/administrator/newsletter/index')->with('successMessage', 'Newsletter Template saved successfuly.');
        }
    }

    /**
     * Method used to unset search session data
     */
    public function cleardata() {
        \Session::forget('NEWSLETTERDATA');
        return \Redirect::to('administrator/newsletter');
    }
    
    /**
     * Method used to send message forcefully
     * @param $id 
     * @return 
     */
    public function sendmessage($id, $page)
    {
       $data = $selectedSubscriberList=array();
      
       
       $data['id'] = $id;
       $data['page'] = $page;       
       $data['pageTitle'] = "Newsletter :: Send";
       
        
       $data['subscriberList'] = Subscriber::where('status', '1')->get();
       
       $data['customerList'] = \App\Model\User::where('deleted', '0')->get(); 
       
      $selectedSubscriberList = Newslettersubscribermapping::select('userId')->where('newsletterId', $id)->get()->toArray();
       
       if(count($selectedSubscriberList)>0)
       {
           $data['selectedSubscriberList'] = $selectedSubscriberList;
       }else{
           $data['selectedSubscriberList'] = array();
       }
       
       
       return view('Administrator.newsletter.subscriber', $data);
        
    }
    
    /**
     * Method used to save subscriber list to send message through corn
     * @param $newsletterId
     * @return 
     */
     public function savesubscriber($newsletterId, $page, Request $request)
     {
       if (\Request::isMethod('post')) {
         $data = array();

            $data['page'] = !empty($page) ? $page : '0';
            $data['id'] = 0;

            $senderlist = array_merge($request->subscriberlist, $request->customerlist);

            $newsletter = new Newslettersubscribermapping;
        
            foreach($senderlist as $sender)
            {
               $newsletter->newsletterId =  $newsletterId;
               $newsletter->userId =  $sender;
               $newsletter->sendStatus =  '0';
               $newsletter->sendDate =  \Carbon\Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s');
               
               $newsletter->save();
            }

            
        
            return redirect('/administrator/newsletter/index')->with('successMessage', 'Subscriber added successfuly.');
       }else{
            return redirect('/administrator/newsletter/index')->with('errorMessage', 'Subscriber not added.');
       }
     }

    public function editnewsletterstatus($id, $status) {

       /* CHECK WHETHER THE USER ID IS NOT EMPTY  */
        if (!empty($id)) {

            /* CHANGE STATUS  */
            if (Newsletter::changeStatus($id, $status)) {

                /* STATUS HAS CHANGED SUCCESSFULLY  */
                return \Redirect::to('administrator/newsletter/')->with('successMessage', 'Newsletter status changed successfully.');
            } else {

                /* UNSUCCESSFULLY ATTEMPT */
                return \Redirect::to('administrator/newsletter/')->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/newsletter/')->with('errorMessage', 'Error in operation!');
        }
    }

    /*Delete newsletter*/

    public function deletenewsletter($id = -1) {

        $newsObj = new Newsletter();

        if (!empty($id)) {

            /* CHANGE STATUS  */
            if (Newsletter::deleteNewsletter($id)) {

                /* STATUS HAS CHANGED SUCCESSFULLY  */
                return \Redirect::to('administrator/newsletter/')->with('successMessage', 'Newsletter deleted successfully.');
            } else {

                /* UNSUCCESSFULLY ATTEMPT */
                return \Redirect::to('administrator/newsletter/')->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/servicelist/')->with('errorMessage', 'Error in operation!');
        }
    }
     
}
