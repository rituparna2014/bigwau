<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Model\Content;
//use App\libraries\helpers;
//use App\libraries\dbHelpers;
use Auth;
use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest')->except('doLogout');
    }

    public function index() {
        //$session = session()->all();
        if(\Session::get('admin_session_user_id')!=''){
            return Redirect::intended('/administrator/dashboard');
        }
        if(Auth::check() == 1){
            return Redirect::intended('/administrator/dashboard')->with('successMessage', 'You have successfully logged in.');
        }
        //print_r($session);
        //echo $session['_token'];
        //if($session['_token'] == '')
        $data = array();
        $data['title'] = "Bigwau - Administrator Panel";
        $data['breadcrumb'] = 'dashboard';
        return view('Administrator.index.index', $data);
    }

}
