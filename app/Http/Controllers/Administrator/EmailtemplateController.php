<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Emailtemplate;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;
use Carbon\Carbon;

class EmailtemplateController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index() {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Emailtemplate'), Auth::user()->userType); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchText = trim(\Input::get('search', ''));
            $searchByType = \Input::get('searchByType', 'G');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('EMAILTEMPLATEDATA');
            \Session::push('EMAILTEMPLATEDATA.field', $field);
            \Session::push('EMAILTEMPLATEDATA.type', $type);
            \Session::push('EMAILTEMPLATEDATA.searchByType', $searchByType);
            \Session::push('EMAILTEMPLATEDATA.searchDisplay', $searchDisplay);


            $param['field'] = !empty($field) ? $field : 'id';
            $param['type'] = !empty($type) ? $type : 'desc';
            $param['searchByType'] = $searchByType;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('EMAILTEMPLATEDATA.field');
            $sortType = \Session::get('EMAILTEMPLATEDATA.type');
            $searchByType = \Session::get('EMAILTEMPLATEDATA.searchByType');
            $searchDisplay = \Session::get('EMAILTEMPLATEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByType'] = !empty($searchByType) ? $searchByType[0] : 'G';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'createdOn' => array('current' => 'sorting'),
            'templateKey' => array('current' => 'sorting'),
        );


        /* SET SORTING ARRAY  */

        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        
        
        $emailTemplateKeys = collect(\App\Model\Emailtemplatekey::where('templateType', $param['searchByType'])->get());
        $data['emailTemplateKeysList'] = $emailTemplateKeys->mapWithKeys(function($item) {
            return [$item['keyname'] => $item['name']];
        });
        
        $emailData = Emailtemplate::getEmailTemplateList($param);


        /* BUILD DATA FOR VIEW  */
        $data['title'] = "Email Template Management :: ADMIN - Bigwau";

        $data['pageTitle'] = "Email Template Management";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Contents', 'Email Templates'), 'contentTitle' => 'Email Template', 'pageInfo' => 'This section allows you to manage templates for email notifications');
        $data['page'] = $emailData->currentPage();
        $data['emailData'] = $emailData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.emailtemplate.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Add Email Template :: ADMIN - Bigwau";
        $data['pageTitle'] = "Email Template :: Add";
        $data['contentTop'] = array('breadcrumbText' => array('Manage Contents', 'Add/Edit Email Template'), 'contentTitle' => 'Email Template', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '0';
        
        /* $templateKey = Config::get('constants.templateKey.TEMPLATEKEY');
        $data['templateKey'] = unserialize($templateKey); */

        $data['templateVars'] = Config::get('constants.emailTemplateVariables');

        /* GET TEMPLATE KEYS */
        $data['templateKey'] = \App\Model\Emailtemplatekey::all();

        if (!empty($id)) {
            $data['id'] = $id;
            $data['title'] = "ADMIN - Bigwau :: Email Template :: Update Email";
            $data['pageTitle'] = "Update Email Template";
            $data['contentTop'] = array('breadcrumbText' => array('Manage Contents', 'Add Email Template'), 'contentTitle' => 'Email Template', 'pageInfo' => '');
            $content = Emailtemplate::find($id);
            $data['content'] = $content;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = "Add Email Template";
            $data['content'] = array();
        }

        return view('Administrator.emailtemplate.addedit', $data);
    }

    /**
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '0';
        $data['id'] = 0;

        $emailtemplate = new Emailtemplate;

        if ($request->templateType != 'G') {
            $validator = Validator::make($request->all(), [
                        'templateSubject' => 'required|regex:/^[a-z0-9 .\-]+$/i',
                        'templateBody' => 'required',
                        'fromName' => 'required|regex:/^[a-z0-9 .\-]+$/i',
                        'fromemail' => 'required|email'
            ]);
        } else {
            if (empty($id)) {
                $validator = Validator::make($request->all(), [
                        'templateKey' => 'required|unique:' . $emailtemplate->table . ',templateKey,' . $id . '|max:255',
                        'templateSubject' => 'required|regex:/^[a-z0-9 .\-]+$/i',
                        'templateBody' => 'required',
                        'fromName' => 'required|regex:/^[a-z0-9 .\-]+$/i',
                        'fromemail' => 'required|email'
            ]);
            }else{
                $validator = Validator::make($request->all(), [
                        'templateSubject' => 'required',
                        'templateBody' => 'required',
                        'fromName' => 'required',
                        'fromemail' => 'required|email'
            ]);
            }
            
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            if (!empty($id)) {
                $emailtemplate = Emailtemplate::find($id);
                $emailtemplate->updatedOn = Config::get('constants.CURRENTDATE');
                $emailtemplate->updatedBy = Auth::user()->id;
            } else {
                $emailtemplate->createdOn = Config::get('constants.CURRENTDATE');
                $emailtemplate->createdBy = Auth::user()->id;
            }

            //$emailtemplate->templateType = $request->templateType;
            $emailtemplate->templateKey = !empty($request->templateKey) ? $request->templateKey : '';
            $emailtemplate->templateSubject = !empty($request->templateSubject) ? $request->templateSubject : '';
            $emailtemplate->templateBody = !empty($request->templateBody) ? $request->templateBody : '';
            $emailtemplate->fromName = !empty($request->fromName) ? $request->fromName : '';
            $emailtemplate->fromemail = !empty($request->fromemail) ? $request->fromemail : '';
            $emailtemplate->save();

            return redirect('/administrator/emailtemplate/index')->with('successMessage', 'Email Template saved successfuly.');
        }
    }

    /**
     * Method used to unset search session data
     *
     */
    public function cleardata() {
        \Session::forget('EMAILTEMPLATEDATA');
        return \Redirect::to('administrator/emailtemplate');
    }

    /**
     * Method used to generate the keys as options in html
     * @return string
     */
    public function generateKeyOptions(Request $request) {
        $html = '';

        /* GET THE KEYS */
        $templateKeys = \App\Model\Emailtemplatekey::where('templateType', $request->type)->get();

        /* GENERATE THE HTML */
        foreach($templateKeys as $keys){
            $html .= "<option value=\"$keys->keyname\">$keys->name</option>";
        }

        echo $html;

    }


    public function deleteEmailtemplate($id = -1,$page = 1) {

        $emailtemplateObj = new Emailtemplate();

        if ($id != '-1') {
            $emailtemplateObj = $emailtemplateObj->find($id);
            if ($emailtemplateObj->delete())
                return redirect(route('emailtemplate'))->with('successMessage', 'Information deleted successfully.');
        }
    }

}
