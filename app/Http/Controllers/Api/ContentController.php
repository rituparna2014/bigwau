<?php

namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Banner;
use App\Model\Blockcontent;
use App\Model\Stores;
use App\Model\Sitepage;
use App\Model\Contactus;
use App\Model\Emailtemplate;
use customhelper;
use DB;
use Auth;
use Config;

class ContentController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->_perPage = 10;
    }

    public function getbannerimage($type = 1) {
        $data = Banner::where('type', $type)->where('status', '1')->orderBy('displayOrder')->get();
        foreach ($data as $index => $eachdata) {
            $data[$index]->imagePath = url('uploads/banner/' . $eachdata->imagePath);
            $data[$index]->bannerContent = stripslashes($eachdata->bannerContent);
            $data[$index]->pageLink = $eachdata->pageLink;
        }
        return json_encode(array('results' => $data));
    }

    public function gethomepageblockcontents() {
        $results = array();
        $data = Blockcontent::where('status', '1')->get();

        if (!empty($data)) {
            foreach ($data as $key => $eachdata) {
                $results[$key]['title'] = $eachdata->contentTitle;
                $results[$key]['subtitle'] = $eachdata->contentSubTitle;
                $results[$key]['slug'] = $eachdata->slug;
                $results[$key]['content'] = stripslashes($eachdata->content);
                if ($eachdata->contentImage != '')
                    $results[$key]['image'] = url('uploads/blockcontent/' . $eachdata->contentImage);
                else
                    $results[$key]['image'] = "";
            }
        }
        return json_encode(array('results' => $results));
    }

    public function getstoredata() {

        $results = array();
        $data = Stores::where('status', '1')->where('deleted', '0')->get();
        if (!empty($data)) {
            foreach ($data as $key => $eachdata) {
                $results[$key]['name'] = $eachdata->storeName;
                $results[$key]['url'] = $eachdata->storeURL;
                $results[$key]['icon'] = url('uploads/stores/' . $eachdata->storeIcon);
            }
        }

        return json_encode(array('results' => $results));
    }

    public function getpagecontent(Request $request) {
        $data = array();

        $pageContent = Sitepage::where('slug', $request->urlSlug)->get();

        if ($pageContent->isNotEmpty()) {
            if (isset($pageContent[0]->pageBanner) && $pageContent[0]->pageBanner != '')
                $pageContent[0]->pageBanner = url('uploads/site_page/' . $pageContent[0]->pageBanner);

            $data = array('status' => 1, 'pageContent' => $pageContent);
        } else
            $data = array('status' => 0);

        return json_encode($data);
    }

    public function getpagetype(Request $request) {
        $pageContent = Sitepage::select('pageType')->where('slug', $request->urlSlug)->first();

        if (!empty($pageContent))
            return $pageContent->toJson();
        else
            return '';
    }

    public function getregistrationbanner() {

        $results = array();
        $data = Banner::where('type', '2')->where('isDefault', '1')->first();
        if (!empty($data))
            $results['bannerImage'] = url('uploads/banner/' . $data->imagePath);

        return response()->json([
                    'status' => '1',
                    'results' => $results
                        ], 200);
    }

    public function registrationBannerById($imageId) {

        $results = array();
        $data = Banner::where('type', '2')->where('id', $imageId)->first();
        if (!empty($data))
            $results['bannerImage'] = url('uploads/banner/' . $data->imagePath);

        return response()->json([
                    'status' => '1',
                    'results' => $results
                        ], 200);
    }

    public function savecontactusrequest(Request $request) {

        //return json_encode($request->firstName);

        $contactObj = new Contactus;

        $contactObj->name = $request->firstName . ' ' . $request->lastName;
        $contactObj->company = $request->company;
        $contactObj->address = $request->address;
        $contactObj->alternateAddress = $request->alternateAddress;
        $contactObj->country = $request->country;
        $contactObj->state = $request->state;
        $contactObj->city = $request->city;
        $contactObj->zip = $request->zip;
        $contactObj->phone = $request->phone;
        $contactObj->email = $request->email;
        $contactObj->fax = $request->fax;
        $contactObj->website = $request->website;
        $contactObj->subject = $request->reasontype;
        $contactObj->message = $request->message;
        //$contactObj->reason_id = $request->reasontype;

        if ($contactObj->save()) {
            $emailTemplate = Emailtemplate::where('templateKey', 'contact_us')->first();
            $to = "contact@shoptomydoor.com";//$request->email;
            $isSend = customhelper::SendMail($emailTemplate, array(), $to);

            return response()->json([
                        'status' => '1',
                        'results' => 'success'
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => 'insert error'
                            ], 200);
        }
    }

    public function savesendmessage(Request $request)
    {

        $contactObj = new Contactus;

        $contactObj->name = $request->firstName . ' ' . $request->lastName;
        $contactObj->phone = $request->phone;
        $contactObj->email = $request->email;
        $contactObj->subject = $request->subject;
        $contactObj->reason_id = $request->reasontype;
        $contactObj->message = $request->message;
        $contactObj->mail_comes_form = '1';

        if ($contactObj->save()) {
            $emailTemplate = Emailtemplate::where('templateKey', 'send_us_message')->first();
            $to = "contact@shoptomydoor.com";//$request->email;
            $isSend = customhelper::SendMail($emailTemplate, array(), $to);

            return response()->json([
                        'status' => '1',
                        'results' => 'success'
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => 'insert error'
                            ], 200);
        }
    }

    /**
     * Method for show FAQs
     * @return jSON
     */
    public function getFaq() {

        $results = array();
        $data['Faq'] = \App\Model\Faq::where('deleted', '0')->where('status', 'Active')->orderby('displayOrder', 'asc')->get();

        return response()->json([
                    'results' => $data
                        ], 200);
    }

    public function getcontactinfo() {
        $result = array();
        $contactEmail = \App\Model\Generalsettings::where('settingsKey', 'contact_email')->first();
        $result['email'] = $contactEmail->settingsValue;
        $contactNumber = \App\Model\Generalsettings::where('settingsKey', 'contact_number')->first();
        if (!empty($contactNumber)) {
            $numberData = json_decode($contactNumber->settingsValue);
            if (!empty($numberData)) {
                $numCount = 0;
                foreach ($numberData as $index => $eachNumber) {
                    if ($numCount >= 2)
                        break;
                    $numParts = explode('^', $eachNumber);
                    $result['number'][$index]['img'] = "https://www.countryflags.io/" . $numParts[0] . "/flat/64.png";
                    $result['number'][$index]['title'] = $numParts[1];
                    $numCount++;
                }
            }
            return response()->json([
                        'results' => json_encode($result)
                            ], 200);
        }
    }

    public function getpartnercontent() {

        $partnerContent = \App\Model\Partner::where('contentType', 'content')->where('status', '1')->where('deleted', '0')->orderBy('id', 'asc')->get();
        return response()->json([
                    'results' => $partnerContent
                        ], 200);
    }

    /**
     * Method for show Partners for Banking
     * @return jSON
     */
    public function getpartnerbanking() {
        $results = array();
        $PartnerBankingValues = array();

        $PartnerBanking = \App\Model\Partner::where('contentType', 'image')->where('deleted', '0')->where('status', '1')->where('type', '0')->orderby('id', 'asc')->get();

        foreach ($PartnerBanking as $val) {
            if (file_exists(public_path('uploads/partner/thumb/') . $val->fileName)) {
                $PartnerBankingValues['Bimages'][] = url('/uploads/partner/thumb') . "/" . $val->fileName;
            } else {
                $PartnerBankingValues['Bimages'][] = url('/administrator/img/default-no-img.jpg');
            }
        }

        $data['PartnerBanking'] = $PartnerBankingValues;
        return response()->json([
                    'results' => $data
                        ], 200);
    }

    /**
     * Method for show Partners for Logistic
     * @return jSON
     */
    public function getpartnerlogistic() {

        $results = array();
        $PartnerLogisticValues = array();

        $PartnerLogistic = \App\Model\Partner::where('contentType', 'image')->where('deleted', '0')->where('status', '1')->where('type', '1')->orderby('id', 'asc')->get();

        foreach ($PartnerLogistic as $val) {
            if (file_exists(public_path('uploads/partner/thumb/') . $val->fileName)) {
                $PartnerLogisticValues['Limages'][] = url('/uploads/partner/thumb') . "/" . $val->fileName;
            } else {
                $PartnerLogisticValues['Limages'][] = url('/administrator/img/default-no-img.jpg');
            }
        }

        $data['PartnerLogistic'] = $PartnerLogisticValues;

        return response()->json([
                    'results' => $data
                        ], 200);
    }

    /**
     * Method for get reward details
     * @return jSON
     */
    public function getrewarddetails(Request $request) {

        $userId = $request->userId;
        $data = array();

        /* GET USER NAME */
        $getQueryUserName = \App\Model\User::select('id', 'firstName', 'lastName', 'email', 'unit')->where('id', $userId)->first();
        $data['userName'] = $getQueryUserName->firstName . " " . $getQueryUserName->lastName;

        /* GET WALLET FUND */
        $walletQuery = \App\Model\Ewallet::where('recipientEmail', $getQueryUserName['email'])->first();
        if (!empty($walletQuery)) {
            $data['walletBalance'] = customhelper::getCurrencySymbolFormat($walletQuery->amount);
        } else {
            $data['walletBalance'] = customhelper::getCurrencySymbolFormat(0.00);
        }

        /* GET TOTAL ORDERS WITH ORDER AMOUNT */
        $getQueryOrders = \App\Model\Invoice::select(DB::raw("SUM(totalBillingAmount) as totalCost"), DB::raw("COUNT(id) as totalOrders"))->where('createdOn', 'LIKE', date('Y') . '-%')
                        ->where('userUnit', $getQueryUserName['unit'])->where("extracostCharged", 'N')->where('invoiceType', 'receipt')->get();
        if (!empty($getQueryOrders)) {
            $data['totalOrders'] = $getQueryOrders[0]->totalOrders;
            $data['totalCost'] = customhelper::getCurrencySymbolFormat($getQueryOrders[0]->totalCost);
        } else {
            $data['totalOrders'] = 0;
            $data['totalCost'] = customhelper::getCurrencySymbolFormat(0.00);
        }

        /* GET BALANCE POINTS */
        $getQueryBalancePoints = \App\Model\Fundpoint::where('userId', $userId)->first();
        if (!empty($getQueryBalancePoints)) {

            /* COUNT TOTAL POINT EARN */
            $getQueryEarnedPoints = \App\Model\Fundpointtransaction::where('userId', $userId)->where('type', 'A')->sum('point');
            $data['earnedPoints'] = $getQueryEarnedPoints;


            $data['balancePoint'] = $getQueryBalancePoints->point;

            /* GET TOTAL EARNED POINTS TRANSACTIONS */


            /* GET DATA FROM REWARD SETTINGS */
            $gainedPoint = ($getQueryBalancePoints->point == 0 ? 1 : $getQueryBalancePoints->point);

            $getQueryReward = \App\Model\Rewardsetting::where('deleted', '0')->whereRaw('points <= ' . $gainedPoint)->orderby('id', 'desc')->first();
            if (!empty($getQueryReward)) {
                $data['rewardName'] = $getQueryReward->name;
                $data['rewardImage'] = (file_exists(public_path() . '/uploads/rewardpoints/' . $getQueryReward->image) ? url('uploads/rewardpoints/' . $getQueryReward->image) : url('administrator/img/default-no-img.jpg') );

                /* POINTS REMAINING TO ACHIEVE NEXT LEVEL */
                $reminingPointsQuery = \App\Model\Rewardsetting::where('deleted', '0')->where("id", ">", $getQueryReward->id)->orderby('id', 'asc')->first();
                $data['remaining'] = ($gainedPoint == 1 ? $reminingPointsQuery->points : $reminingPointsQuery->points - $gainedPoint);
            } else {
                $data['rewardName'] = "No Reward";
                $data['rewardImage'] = '';
                $data['remaining'] = 'N/A';
            }



            /* POINTS NEAR TO EXPIRE */
            $splitExpDate = '';

            $nearToExpireQuery = \App\Model\Fundpointtransaction::where('expiredDate', ">", date('Y-m-d'))->orderby("expiredDate", 'asc')->first();
            if (!empty($nearToExpireQuery)) {
                $splitExpDate = explode(" ", $nearToExpireQuery->expiredDate);
                $totalPointsExpired = \App\Model\Fundpointtransaction::select(DB::raw("SUM(point) as point"))->where('expiredDate', "LIKE", trim($splitExpDate[0]) . "%")->get();

                $dateExp = date_create(trim($splitExpDate[0]));
                $data['expString'] = $totalPointsExpired[0]->point . " points will expires on " . date_format($dateExp, "M dS, Y");
                $data['expStringShort'] = date_format($dateExp, "M dS, Y");
            } else {
                $data['expString'] = '';
                $data['expStringShort'] = '';
            }
        } else {

            /* NO POINT IN MAIN POINT TABLE */
            $data['balancePoint'] = 0;
            $data['earnedPoints'] = 0;

            $gainedPoint = 1;

            /* GET DATA FROM REWARD SETTINGS */
            $getQueryReward = \App\Model\Rewardsetting::where('deleted', '0')->orderby('points', 'asc')->first();
            if (!empty($getQueryReward)) {
                $data['rewardName'] = $getQueryReward->name;
                $data['rewardImage'] = url('uploads/rewardpoints/' . $getQueryReward->image);

                /* POINTS REMAINING TO ACHIEVE NEXT LEVEL */
                $reminingPointsQuery = \App\Model\Rewardsetting::where('deleted', '0')->where("id", ">", $getQueryReward->id)->orderby('id', 'asc')->first();
                if (!empty($reminingPointsQuery)) {
                    $data['remaining'] = $reminingPointsQuery->points - $gainedPoint + 1;
                } else {
                    $data['remaining'] = 0;
                }
            } else {
                $data['rewardName'] = "No Reward";
                $data['rewardImage'] = '';
                $data['remaining'] = 0;
            }





            $data['expString'] = '';
            $data['expStringShort'] = 'N/A';
        }

        return response()->json([
                    'results' => $data
                        ], 200);
    }

    /**
     * Method for get fund point transaction
     * @return jSON
     */
    public function getpointtransction(Request $request) {
        $where = " 1 ";
        if ($request->activity == 'month')
            $where .= "  AND MONTH(date) ='" . \Carbon\Carbon::now()->month . "'";
        else if ($request->activity == 'week')
            $where .= "  AND date BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
        else if ($request->activity == 'year')
            $where .= "  AND date LIKE '" . date('Y') . '-%' . "'";
        else
            $where .= "";

        $page = $request->currentPage;
        $offset = ($page - 1) * $this->_perPage;

        $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
        $offset1 = ($page - 1) * $perPage;


        $totalItems = \App\Model\Fundpointtransaction::where('userId', $request->userId)->whereRaw($where)->count();
        $queryTransctions = \App\Model\Fundpointtransaction::selectRaw("point, userId, id, senderId, reason, type, DATE_FORMAT(date,'%m/%d/%Y') as date")
                        ->where('userId', $request->userId)->whereRaw($where)
                        ->orderby('id', 'desc')->take($perPage)->skip($offset1)->get();

        if (!empty($queryTransctions)) {
            $data['transctions'] = $queryTransctions;
            return response()->json([
                        'results' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => "$perPage",
                        'countData' => 1,
                            ], 200);
        } else {
            $data['transctions'] = array();
            return response()->json([
                        'results' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $perPage,
                        'countData' => 0,
                            ], 200);
        }
    }

    /**
     * Method for get wallet code
     * @return jSON
     */
    public function getwalletcode(Request $request) {
        $data = array();
        /* MATCH THE PASSWORD */
        $queryTransctions = \App\Model\User::where('id', $request->userId)->first();

        $checkPwd = \Hash::check($request->inputPassword, $queryTransctions->password);

        if ($checkPwd) {

            /* TO GET THE EWALLET CODE */
            $nearToExpireQuery = \App\Model\Ewallet::where('recipientEmail', $queryTransctions->email)->first();
            if (!empty($nearToExpireQuery)) {
                $data['walletCode'] = $nearToExpireQuery->ewalletId;

                return response()->json([
                            'results' => $data,
                            'countData' => 1
                                ], 200);
            } else {
                $data['walletCode'] = "Sorry! No E-Wallet Code for now.";
                return response()->json([
                            'results' => $data,
                            'countData' => 1
                                ], 200);
            }
        } else {
            return response()->json([
                        'results' => $data,
                        'countData' => 0
                            ], 200);
        }
    }

    /**
     * Method for get paystack settings
     * @return jSON
     */
    public function getpaystacksettings(Request $request) {

        $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", 9)->get();


        $data['key'] = $getSettings[2]->configurationValues;

        return response()->json([
                    'results' => $data,
                        ], 200);
    }

    /**
     * Method for get documents for customers
     * @return jSON
     */
    public function getdocuments(Request $request) {
        $data = array();
        $fileExt = '';
        $base64Img = '';
        $encodedImg = '';
        $imageSmall = '';
        $userId = $request->userId;
        $documentId = '';

        $getDoc = \App\Model\Document::where("userId", $userId)->where('deleted', '0')->orderby('id', 'desc')->get();
        if (!empty($getDoc)) {
            foreach ($getDoc as $key => $val) {

                if (file_exists(public_path() . "/uploads/documents/" . $userId . "/" . $val->filename)) {

                    $encodedImg = base64_encode(file_get_contents(public_path() . "/uploads/documents/" . $userId . "/" . $val->filename));
                    $fileExt = pathinfo(public_path() . "/uploads/documents/" . $userId . "/" . $val->filename, PATHINFO_EXTENSION);

                    if ($fileExt == 'png' || $fileExt == 'jpeg' || $fileExt == 'jpg' || $fileExt == 'svg' || $fileExt == 'bmp' || $fileExt == 'gif') {
                        $imageSmall = 'images.png';
                        $base64Img = 'data:image/' . $fileExt . ';base64,' . $encodedImg;
                    } elseif ($fileExt == 'pdf' || $fileExt == 'PDF') {
                        $imageSmall = 'pdf.png';
                        $base64Img = url('/') . "/uploads/documents/" . $userId . "/" . $val->filename;
                    } else {
                        $imageSmall = 'doc.png';
                        $base64Img = url('/') . "/uploads/documents/" . $userId . "/" . $val->filename;
                    }

                    $data[] = array(
                        "des" => $val->descr,
                        "document" => $val->filename,
                        "base64" => $base64Img,
                        "fileExt" => $imageSmall,
                        "documentId" => $val->id,
                        "createdOn" => date("m/d/Y", strtotime($val->createdOn))
                    );
                } else {
                    $data[] = array(
                        "des" => $val->descr,
                        "document" => $val->filename,
                        "base64" => '',
                        "fileExt" => 'image-not-available.jpg',
                        "documentId" => $val->id,
                        "createdOn" => $val->createdOn
                    );
                }
            }
        }


        return response()->json([
                    'results' => $data,
                        ], 200);
    }

    /**
     * Method for delete document for customers
     * @return jSON
     */
    public function deletedocument(Request $request) {
        if ($request->docId)
            $userId = $request->userId;
        $docId = $request->docId;
        $imgPath = '';
        $data = array();
        /* GET FILE NAME */
        $getRecord = \App\Model\Document::where("userId", $userId)->where('id', $docId)->first();

        if (!empty($getRecord)) {
            $imgPath = public_path() . "/uploads/documents/" . $userId . "/" . $getRecord->filename;

            #chmod($imgPath, 0777);

            /* DELETE ACTUAL FILE */
            if (file_exists($imgPath)) {
                //unlink($imgPath);
            }

            /* DELETE RECORD */

            $doc = \App\Model\Document::find($docId);
            $doc->deletedBy = $userId;
            $doc->deletedOn = Config::get('constants.CURRENTDATE');
            $doc->deleted = '1';
            $doc->save();


            $data[] = 'Document deleted';
            return response()->json([
                        'results' => $data,
                            ], 200);
        }
        $data[] = 'Nothing found';
        return response()->json([
                    'results' => $data,
                        ], 200);
    }

    /**
     * Method for upload document for customers
     * @return jSON
     */
    public function uploaddocument($userId, Request $request) {
        $data = array();

        if ($request->hasFile('itemImage')) {

            $validator = Validator::make($request->all(), [
                        'itemImage' => 'mimes:jpeg,jpg,gif,png,bmp,svg,doc,docx,odt,xls,xlsx,pdf,txt|max:4096'
            ]);

            if ($validator->fails()) {
                $err = json_decode($validator->messages());

                $data[] = $err->itemImage[0];

                return response()->json([
                            'results' => $data,
                                ], 200);
            }


            $image = $request->file('itemImage');
            $name = time() . '_' . $image->getClientOriginalName();

            $destinationPath = public_path('/uploads/documents/' . $userId);
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath);
                chmod($destinationPath, 0777);
            }

            $image->move($destinationPath, $name);

            /* INSERT QUERY */
            $doc = new \App\Model\Document;
            $values = array('userId' => $userId, 'filename' => $name, 'descr' => $request->descr, 'createdOn' => Config::get('constants.CURRENTDATE'), 'createdBy' => $userId);
            $doc->insert($values);

            $data[] = 'Uploaded successfully';
            return response()->json([
                        'results' => $data,
                            ], 200);
        }
        $data[] = 'Error in operation';
        return response()->json([
                    'results' => $data,
                        ], 200);
    }

    /**
     * Method for upload document for customers
     * @return jSON
     */
    public function postclaim($userId, Request $request) {
        $data = array();
        $message = '';
        $user = \App\Model\User::find($userId);
        $adminuser = \App\Model\UserAdmin::find(1);

        /* VALIDATE ORDER ID */
        $validateOrderNumber = \App\Model\Order::where("userId", $userId)->where("orderNumber", filter_var($request->orderNumber, FILTER_SANITIZE_STRING))->first();
        if (empty($validateOrderNumber)) {
            $data['res'] = "Order number not matched";
            $data['status'] = '0';
            return response()->json([
                        'results' => $data,
                            ], 200);
        }

        /* GET THE FORM VALUES */
        $itemDescription = filter_var($request->itemDescription, FILTER_SANITIZE_STRING);
        $orderNumber = filter_var($request->orderNumber, FILTER_SANITIZE_STRING);
        $itemNumber = filter_var($request->itemNumber, FILTER_SANITIZE_STRING);
        $dateOfPurchase = filter_var($request->dateOfPurchase, FILTER_SANITIZE_STRING);
        $purchaseDateOfItem = filter_var($request->purchaseDateOfItem, FILTER_SANITIZE_STRING);
        $purchaseDateOfItem = substr($purchaseDateOfItem,0,(strpos($purchaseDateOfItem,'(')-1));
        $purchaseDateOfItem = \Carbon\Carbon::parse($purchaseDateOfItem)->format('Y-m-d H:i:s');
        $placeOfPurchase = filter_var($request->placeOfPurchase, FILTER_SANITIZE_STRING);
        $totalAmountClaimed = filter_var($request->totalAmountClaimed, FILTER_SANITIZE_STRING);
        $natureOfDamage = filter_var($request->natureOfDamage, FILTER_SANITIZE_STRING);
        $dateItemsReceived = filter_var($request->dateItemsReceived, FILTER_SANITIZE_STRING);
        $shipmentVia = filter_var($request->shipmentVia, FILTER_SANITIZE_STRING);

        $cliamentName = filter_var($request->cliamentName, FILTER_SANITIZE_STRING);
        $cliamentPhone = filter_var($request->cliamentPhone, FILTER_SANITIZE_STRING);
        $cliamentAddress = filter_var($request->cliamentAddress, FILTER_SANITIZE_STRING);
        $request->claimDate = substr($request->claimDate,0,(strpos($request->claimDate,'(')-1));
        $claimDate = \Carbon\Carbon::parse($request->claimDate)->format('Y-m-d H:i:s');

        $loss = $request->loss;
        $damage = $request->damage;
        $incomplete = $request->incomplete;

        if ($loss)
            $loss = "Loss";
        if ($damage)
            $damage = "Damage";
        if ($incomplete)
            $incomplete = "Incomplete";

        $makeArray = array(
            "Descriptionofitem" => $itemDescription,
            "OrderNumber" => $orderNumber,
            "ItemNumber" => $itemNumber,
            "Valueifitematdateofpurchase" => $dateOfPurchase,
            "PurchaseDateforItem" => $purchaseDateOfItem,
            "PlaceOfPurchase" => $placeOfPurchase,
            "Totalamountbeenclaimed" => $totalAmountClaimed,
            "Explainnatureofdamage" => $natureOfDamage,
            "Dateitemswerereceived" => $dateItemsReceived,
            "shipmentVia" => $shipmentVia,
            "claimDate" => $claimDate,
            "cliamentName" => $cliamentName,
            "cliamentPhone" => $cliamentPhone,
            "cliamentAddress" => $cliamentAddress,
            "loss" => $loss,
            "damage" => $damage,
            "incomplete" => $incomplete
        );

        $message .= '
        <p><b>Description of item:</b> ' . $itemDescription . '</p>
        <p><b>Order Number:</b> ' . $orderNumber . '</p>
        <p><b>Item number:</b> ' . $itemNumber . '</p>
        <p><b>Value if item at date of purchase:</b> ' . $dateOfPurchase . '</p>
        <p><b>Purchase date for item:</b> ' . $purchaseDateOfItem . '</p>
        <p><b>Place of purchase:</b> ' . $placeOfPurchase . '</p>
        <p><b>Total amount been claimed:</b> ' . $totalAmountClaimed . '</p>
        <p><b>Explain nature of damage:</b> ' . $natureOfDamage . '</p>
        <p><b>Shipment via:</b>    ' . $shipmentVia . '</p>
        <p><b>Date items were received:</b>    ' . $dateItemsReceived . '</p>
        <p><b>The Claim is made against the carrier for:<b> ' . $loss . '&nbsp;' . $damage . '&nbsp;' . $incomplete . '</p>
        <p><b>Claim Date:</b> ' . $claimDate . '</p>
        <p><b>Name of Claimant:</b> ' . $cliamentName . '</p>
        <p><b>Address:</b> ' . $cliamentAddress . '</p>
        <p><b>Phone:</b> ' . $cliamentPhone . '</p>
        ';


        /* IF SECOND FILE [Upload Photo of Damaged Item(If Applicable)] EXISTS */
        if ($request->hasFile('damagedItemImage')) {

            $validator = Validator::make($request->all(), [
                        'itemImage' => 'mimes:jpeg,jpg,gif,png,bmp,svg,doc,docx,odt,xls,xlsx,pdf,txt|max:4096'
            ]);

            if ($validator->fails()) {
                $err = json_decode($validator->messages());

                $data['res'] = $err->itemImage[0];
                $data['status'] = '0';
                return response()->json([
                            'results' => $data,
                                ], 200);
            }

            $image2 = $request->file('damagedItemImage');
            $name2 = time() . '_' . $image2->getClientOriginalName();

            $attachmentpath2 = public_path('/uploads/fileaclaim/' . $userId) . "/" . $name2;
            $actualpath2 = url('/uploads/fileaclaim/' . $userId) . "/" . $name2;

            $destinationPath2 = public_path('/uploads/fileaclaim/' . $userId);
            if (!file_exists($destinationPath2)) {
                mkdir($destinationPath2);
                chmod($destinationPath2, 0777);
            }

            $image2->move($destinationPath2, $name2);

            $makeArray['image2'] = $actualpath2;
        }


        /* IF FILE EXISTS */
        if ($request->hasFile('itemImage')) {

            $validator = Validator::make($request->all(), [
                        'itemImage' => 'mimes:jpeg,jpg,gif,png,bmp,svg,doc,docx,odt,xls,xlsx,pdf,txt|max:4096'
            ]);

            if ($validator->fails()) {
                $err = json_decode($validator->messages());

                $data['res'] = $err->itemImage[0];
                $data['status'] = '0';
                return response()->json([
                            'results' => $data,
                                ], 200);
            }


            $image = $request->file('itemImage');
            $name = time() . '_' . $image->getClientOriginalName();

            $attachmentpath = public_path('/uploads/fileaclaim/' . $userId) . "/" . $name;
            $actualpath = url('/uploads/fileaclaim/' . $userId) . "/" . $name;

            $destinationPath = public_path('/uploads/fileaclaim/' . $userId);
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath);
                chmod($destinationPath, 0777);
            }

            $image->move($destinationPath, $name);

            $mailTemplatevars = Config::get('constants.emailTemplateVariables');
            $emailTemplate = Emailtemplate::where('templateKey', 'File_a_claim')->first();
            $arrayvalues = [];
            $replace['[NAME]'] = $user->firstName . " " . $user->lastName;
            $replace['[EMAIL]'] = $user->email;
            $replace['[NOTIFICATION]'] = $message;
            $to = $adminuser->email;

            foreach ($mailTemplatevars as $key => $val) {
                if (isset($replace[$val]) == $val) {
                    $arrayvalues[] = $replace[$val];
                } else {
                    $arrayvalues[] = '';
                }
            }

            $content = str_replace($mailTemplatevars, $arrayvalues, stripslashes($emailTemplate->templateBody));

            /* ++++++++++ email functionality ++++++++ */
            \Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($to, $attachmentpath, $emailTemplate, $user, $request, $attachmentpath2) {
                $message->from($user->email, $user->firstName . " " . $user->lastName);
                $message->subject($emailTemplate->templateSubject);
                $message->to($to);
                $message->attach($attachmentpath);
                if ($request->hasFile('damagedItemImage')) {
                    $message->attach($attachmentpath2);
                }
            });
            /* ++++++++++ end of email functionality ++++ */

            $makeArray['image'] = $actualpath;

            $this->postcontact($user, $makeArray);

            $data['res'] = 'Thank you. We will get back to you within 2 business days.';
            $data['status'] = '1';
            return response()->json([
                        'results' => $data,
                            ], 200);
        } else {
            /* ++++++++++ email functionality ++++++++ */
            $emailTemplate = Emailtemplate::where('templateKey', 'File_a_claim')->first();
            $replace['[NAME]'] = $user->firstName . " " . $user->lastName;
            $replace['[EMAIL]'] = $user->email;
            $replace['[NOTIFICATION]'] = $message;
            $to = $adminuser->email;
            customhelper::SendMail($emailTemplate, $replace, $to);
            /* ++++++++++ end of email functionality ++++ */

            $this->postcontact($user, $makeArray);

            $data['res'] = 'Thank you. We will get back to you within 2 business days.';
            $data['status'] = '1';
            return response()->json([
                        'results' => $data,
                            ], 200);
        }
    }

    public function postcontact($user, $makeArray) {

        /* SAVE THIS RECORD INTO CONTACT US TABLE */

        $contact = new Contactus;
        $contact->name = $user->firstName . " " . $user->lastName;
        $contact->address = "";
        $contact->alternateAddress = "";
        $contact->country = $user->countryId;
        $contact->state = $user->stateId;
        $contact->city = $user->cityId;
        $contact->company = $user->company;
        $contact->phone = $user->contactNumber;
        $contact->email = $user->email;
        $contact->subject = "File a Claim";
        $contact->email = $user->email;
        $contact->message = json_encode($makeArray);
        $contact->status = '1';
        $contact->reason_id = '11';
        $contact->postedOn = Config::get('constants.CURRENTDATE');
        $contact->save();
    }

    /**
     * Method for get user messages
     * @return jSON
     */
    public function getmessages(Request $request) {

        //dd(\Carbon\Carbon::now()->startOfMonth()->subMonth()->toDateString());
        //dd(\Carbon\Carbon::now()->endOfMonth()->toDateString());

        $where = " 1 ";
        if ($request->activity == 'month')
            $where .= "  AND MONTH(sentOn) ='" . \Carbon\Carbon::now()->month . "'";
        else if ($request->activity == 'month2')
            $where .= "  AND sentOn BETWEEN '" . \Carbon\Carbon::now()->startOfMonth()->subMonth()->toDateString() . "' AND '" . \Carbon\Carbon::now()->endOfMonth()->toDateString() . "'";
        else if ($request->activity == 'week')
            $where .= "  AND sentOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
        else if ($request->activity == 'week2')
            $where .= "  AND sentOn BETWEEN '" . \Carbon\Carbon::today()->subWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
        else if ($request->activity == 'year')
            $where .= "  AND sentOn LIKE '" . date('Y') . '-%' . "'";
        else if ($request->activity == 'day')
            $where .= "  AND sentOn BETWEEN '" . \Carbon\Carbon::now()->subDay()->toDateTimeString() . "' AND '" . \Carbon\Carbon::now() . "'";
        else
            $where .= "";

        $unot = new \App\Model\Usernotification();
        $unotTable = $unot->prefix . $unot->table;

        if ($request->msgtype != '') {
            $where .= "  AND " . $unotTable . ".status = '" . $request->msgtype . "'";
        }

        $page = $request->currentPage;
        $offset = ($page - 1) * $this->_perPage;

        $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
        $offset1 = ($page - 1) * $perPage;


        $totalItems = \App\Model\Usernotification::where('userId', $request->userId)->whereRaw($where)->count();
        $queryTransctions = \App\Model\User::getmessages($request->userId, $where, $perPage, $offset1);

        if (!empty($queryTransctions)) {
            $data['transctions'] = $queryTransctions;
            return response()->json([
                        'results' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => "$perPage",
                        'countData' => 1,
                            ], 200);
        } else {
            $data['transctions'] = array();
            return response()->json([
                        'results' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $perPage,
                        'countData' => 0,
                            ], 200);
        }
    }

    /**
     * Method for get message (notification) content
     * @return jSON
     */
    public function getmessagecontent(Request $request) {
        $data = array();

        if ($request->msgId) {

            $content = \App\Model\Usernotification::where('userId', $request->userId)->where("id", $request->msgId)->first();

            $data['subject'] = $content->subject;
            $data['message'] = html_entity_decode($content->message);

            $notifObj = \App\Model\Usernotification::find($request->msgId);
            $notifObj->status = '1';
            $notifObj->save();

            return response()->json([
                        'results' => $data,
                        'countData' => 1
                            ], 200);
        } else {
            return response()->json([
                        'results' => $data,
                        'countData' => 0
                            ], 200);
        }
    }

    /**
     * Method for get claims list
     * @return jSON
     */
    public function getclaims(Request $request) {
        $data = array();

        $reply = new \App\Model\Contactreply;


        $replyTable = $reply->table;


        $contact = new \App\Model\Contactus;
        $contactTable = $contact->table;

        $page = $request->currentPage;
        $offset = ($page - 1) * $this->_perPage;

        $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
        $offset1 = ($page - 1) * $perPage;


        $useEmail = $request->useEmail;

        $totalItems = \App\Model\Contactus::where('email', $useEmail)->where($contact->table . '.status', '3')->where($contact->table . '.reason_id', 11)->join($replyTable, "$replyTable.contactusId", "=", "$contactTable.id")->count();

        $resultSet = \App\Model\Contactus::where('email', $useEmail)->where($contact->table . '.status', '3')->where($contact->table . '.reason_id', 11)->join($replyTable, $replyTable . ".contactusId", "=", $contactTable . ".id")->orderby($contact->table . '.id', 'desc')->take($perPage)->skip($offset1)->get();
        ;

        if (!empty($resultSet)) {
            $data['resultSet'] = $resultSet;
            return response()->json([
                        'results' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $perPage,
                        'countData' => 1,
                            ], 200);
        } else {
            $data['resultSet'] = array();
            return response()->json([
                        'results' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $perPage,
                        'countData' => 0,
                            ], 200);
        }
    }

    /**
     * Method for forgot password in front end
     * @return jSON
     */
    public function userforgotpassword(Request $request) {

        $inputEmail = $request->inputEmail;

        $data = array();

        /* MATCH THE PASSWORD */
        $queryTransctions = \App\Model\User::where('email', $inputEmail)->first();

        if (empty($queryTransctions)) {
            return response()->json([
                        'results' => 'Email does not match. Please correct.',
                        'countData' => 0,
                        'color' => 'text-danger',
                            ], 200);
        }

        $rememberToken = $this->generateRandomString(15);
        $serverUrl = $request->serverUrl . "forgot-password/" . $rememberToken;


        $usreObj = \App\Model\User::find($queryTransctions->id);
        $usreObj->rememberToken = $rememberToken;
        $usreObj->save();

        /* ++++++++++ email functionality ++++++++ */
        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'user_forgot_password')->first();
        $replace['[NAME]'] = $queryTransctions->firstName . " " . $queryTransctions->lastName;
        $replace['[TOKEN]'] = $serverUrl;
        $to = $queryTransctions->email;
        $mail = customhelper::SendMail($emailTemplate, $replace, $to);
        /* ++++++++++ end of email functionality ++++ */


        return response()->json([
                    'results' => 'You will receive an email for further instruction.',
                    'countData' => 0,
                    'color' => 'text-success',
                        ], 200);
    }

    /**
     * Method for update password for forgot password functionality
     * @param string $token
     * @return jSON
     */
    public function userforgotpasswordtoken(Request $request) {
        $token = $request->token;

        if ($token == '') {
            return response()->json([
                        'results' => 'No Token',
                        'countData' => 0,
                        'color' => 'text-danger',
                            ], 200);
        }

        $checkToken = \App\Model\User::where('rememberToken', $token)->first();
        if (empty($checkToken)) {
            return response()->json([
                        'results' => 'Token mismatched',
                        'countData' => 0,
                        'color' => 'text-danger',
                            ], 200);
        }

        $password = $request->password;
        $confirmPassword = $request->confirmPassword;

        if ($password == '') {
            return response()->json([
                        'results' => 'Password cannot be blank',
                        'countData' => 0,
                        'color' => 'text-danger',
                            ], 200);
        }

        if ($password !== $confirmPassword) {
            return response()->json([
                        'results' => 'Passwords mismatched',
                        'countData' => 0,
                            ], 200);
        }

        /* PASSWORD PATTERN MATCHIN */
        if ($this->is_valid_password($password) == false) {
            $err = '';
            return response()->json([
                        'results' => 'Password must contains minimum 8 characters, atleast one digit, one special character, one uppercase letter',
                        'countData' => 0,
                            ], 200);
        }


        $user = \App\Model\User::find($checkToken->id);
        $user->password = \Hash::make($password);
        $user->rememberToken = NULL;
        $user->save();

        return response()->json([
                    'results' => 'Password has been updated. Please login to your account',
                    'countData' => 1,
                    'color' => 'text-success',
                        ], 200);
    }

    /**
     * Method for valid password checking
     * @param string $password
     * @return boolean
     */
    public function is_valid_password($password) {
        return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&^()_?+=])[A-Za-z\d@$_!%*?&-^]{8,20}$/', $password) ? true : false;
    }

    /**
     * Method for valid password checking
     * @param integer $length
     * @return string
     */
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Method for get the header menus
     * @return array
     */
    public function getheadernavigation() {
        $data = array();
        $data['baseurl'] = str_replace('/public', '', url('web/#/'));
        $nav = \App\Model\Menu::where('deleted', '0')->where('position', '0')->where('status', '1')->orderby('sort', 'asc')->get();
        if (!empty($nav)) {
            $data['headermNav'] = $nav;
        }
        return $data;
    }

    /**
     * Method for get the footer menus
     * @return array
     */
    public function getfooternavigation() {
        $data = array();
        $nav = \App\Model\Menu::where('deleted', '0')->where('position', '1')->where('status', '1')->orderby('sort', 'asc')->get();
        if (!empty($nav)) {
            $data['footermNav'] = $nav;
        }
        return $data;
    }

    public function getsociallinks() {

        $footerLinks = \App\Model\Socialmedia::where('display', '1')->get();

        return response()->json([
                    'status' => '1',
                    'results' => $footerLinks,
                        ], 200);
    }

    public function gethomepageadbanner() {
        $results = array();
        $data = Banner::where('type', '7')->where('isDefault', '1')->first();
        if (!empty($data)) {
            $results['bannerImage'] = url('uploads/banner/' . $data->imagePath);
            $results['pageLink'] = $data->pageLink;
        }

        return response()->json([
                    'status' => '1',
                    'results' => $results
                        ], 200);
    }

    /**
     * Method used to get the categories of customer help
     * @return json array
     */
    public function getallcustomerhelpcategory() {
        $data = array();
        $helpSections = array();
        $helpCat = array();
        $byGroup = array();

        $helpCat = \App\Model\Customerhelpcategories::getallcat();


        if (!empty($helpCat)) {
            foreach ($helpCat as $val) {
                $helpSections[] = \App\Model\Customerhelpsection::getallsection($val->id);
            }


            foreach ($helpSections as $key2 => $val2) {
                if (!empty($val2[0])) {
                    //$arr[] = array('name' => $key2, 'link' => '', 'children' => []);
                    $arr2 = array();
                    foreach ($val2 as $key3 => $val3) {
                        $helpCatbyId = \App\Model\Customerhelpcategories::getcatbyid($val2[0]->catId);
                        $arr2[] = array(
                            'name' => $val3->title, 'catId' => $val3->catId, 'link' => '/help/' . $helpCatbyId->slug . "/" . $val3->slug, 'subChildren' => []
                        );
                    }
                    $helpCatbyId = \App\Model\Customerhelpcategories::getcatbyid($val2[0]->catId);
                    $arr[] = array('name' => $helpCatbyId->name, 'link' => '', 'children' => $arr2);
                }
            }
        }


        return response()->json([
                    'status' => '1',
                    'results' => $arr,
                    'cat' => $helpCat,
                        ], 200);
    }

    /**
     * Method used to get the first help section if exist
     * @return json array
     */
    public function firsthelpsection(Request $request) {
        $data = array();

        if ($request->href == "") {
            $data['first'] = \App\Model\Customerhelpsection::where("deleted", '0')->where("status", '1')->orderby("orders", "asc")->first();
        } else {
            $data['first'] = \App\Model\Customerhelpsection::where("deleted", '0')->where("status", '1')->where("slug", $request->href)->first();
        }

        return response()->json([
                    'status' => '1',
                    'data' => $data
                        ], 200);
    }

    /**
     * Method for get paypal ipn settings
     * @return jSON
     */
    public function getpaypalipnsettings(Request $request) {
        $data = array();
        $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", 10)->where('configurationKeys', 'clientID')->first()->toArray();
        $getMode = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", 10)->where('configurationKeys', 'Mode')->first()->toArray();
        $data['key'] = $getSettings['configurationValues'];
        $data['mode'] = ($getMode['configurationValues']=='Live'?'Production':'Sandbox');
        /* CURRENCY EXCHANGE IF CURRENCY NOT IN USD */
        if (!empty($request->currencyCode)) {
            $exchangeRate = \App\Model\Currency::currencyExchangeRate($request->currencyCode, 'USD');
            $data['total'] = \App\Model\Currency::currencyConverter($request->total, $exchangeRate);
            $data['currencyCode'] = 'USD';
        } else {
            $data['total'] = $request->total;
            $data['currencyCode'] = 'USD';
        }

        return response()->json([
                    'results' => $data,
                        ], 200);
    }

    /**
     * Method for get paypal ipn settings
     * @return jSON
     */
    public function getcontactusreasons(Request $request) {
        $data = array();
        $reasons = \App\Model\Contactreason::where("status", '1')->whereNotIn('id', [11, 12])->get();

        if (!empty($reasons)) {
            $data['reasons'] = $reasons;
        }

        return response()->json([
                    'results' => $data,
                        ], 200);
    }

}
