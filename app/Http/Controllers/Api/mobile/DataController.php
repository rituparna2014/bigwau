<?php

namespace App\Http\Controllers\Api\mobile;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouse;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use DB;

class DataController extends Controller {

    public function getallwarehouse_mobile() {
        
        $results = array();
        $allWarehouse = Warehouse::where('status', '1')->where('deleted', '0')->with('country', 'state', 'city')->get();
        $warehouseBanner = \App\Model\Banner::select('imagePath')->where('type', '3')->where('bannerType', 'image')->where('status','1')->get()->toArray();
        if (!empty($allWarehouse)) {
            $results = $allWarehouse;

            for($i=0; $i<count($results);$i++)
            {
               $results[$i]['imagepath'] = url('uploads/warehouse/' .$results[$i]['image']); 
               $results[$i]['name'] = $results[$i]['country']['name'];
               $results[$i]['code'] = $results[$i]['country']['code'];  
            }
        }
    
        if(!empty($warehouseBanner))
        {
            $banner = url('uploads/banner/'.$warehouseBanner[0]['imagePath']);
        }else{
            $banner = '';
        }
    
        if(!empty($results)){

            return response()->json([
            'status' => '200',
            'data' => (object)['results'=>$results,'banner'=>$banner],
            'message' => 'success'
            ], 200);

        } else {

            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
            ], 200);

        }
        
    }

    public function getstorelist_mobile(Request $request) {
        
        if(empty($request->type)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else if(!empty($request->type) && !in_array($request->type, ['shopforme','autopart'])){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured, invalid type'
            ], 200);

        }else {

            $data = array();
            $type = $request->type;
            $warehouseId = $request->warehouseId;

            if (!empty($warehouseId)) {
                $data = \App\Model\Stores::getAllStores(array('type' => $type, 'warehouseId' => $warehouseId));
            } else {
                $data = \App\Model\Stores::select('id', 'storeName')
                        ->where('deleted', '0')
                        ->where('status', '1')
                        ->where('storeType', $type)
                        ->get();
            }
            if(count($data)){

                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            }   
        }
        
    }

    public function getcategorylist_mobile(Request $request) {
        
        $data = array();

        $type = $request->type;
        $storeId = $request->storeId;

        if (!empty($storeId)) {
            $data = \App\Model\Stores::getAllCategoriesByStore(array('type' => $type,  'storeId' => $storeId));
        } else {
            if (!empty($type)) {
                $data = \App\Model\Sitecategory::select('id', 'categoryName')
                        ->where('parentCategoryId', '-1')
                        ->where('status', '1')
                        ->where('type', $type)
                        ->get();
            } else {
                $data = \App\Model\Sitecategory::select('id', 'categoryName')
                        ->where('parentCategoryId', '-1')
                        ->where('status', '1')
                        ->get();
            }
        }

        if(count($data)){

            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success'
            ], 200);

        } else {

            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
            ], 200);

        }

    }

    public function getsubcategorylist_mobile(Request $request) {
        
        if(empty($request->categoryId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $id = $request->categoryId;
            $data = array();

            $data = \App\Model\Sitecategory::select('id', 'categoryName')
                    ->where('parentCategoryId', $id)
                    ->where('status', '1')
                    ->get();

            if(count($data)){

                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            }        
        }

    }

    public function getproductlist_mobile(Request $request) {
        
        if(empty($request->categoryId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $id = $request->categoryId;
            $results = array();
            $data = \App\Model\Siteproduct::select('id', 'productName', 'image')
                    ->where('subCategoryId', $id)
                    ->where('status', '1')
                    ->get();


            if (!empty($data)) {
                foreach ($data as $key => $eachdata) {
                    $results[$key]['id'] = $eachdata->id;
                    $results[$key]['productName'] = $eachdata->productName;
                    if ($eachdata->image != '')
                        $results[$key]['image'] = url('uploads/site_products/' . $eachdata->image);
                    else
                        $results[$key]['image'] = "";
                }

                return response()->json([
                'status' => '200',
                'data' => $results,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            }
        }

    }

    public function getmakelist_mobile(Request $request) {
        $type = '0';
        if(!empty($request->typeId)){
            $type = $request->typeId;
        }

        $data = \App\Model\Automake::where('deleted', '0')->where('type', $type)->where('status', '0')->orderBy('name', 'ASC')->get();

        if(count($data)){ 
            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success',
            ], 200);
        } else {
            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);
        }
    }

    public function getmodellist_mobile(Request $request) {
        
        if(empty($request->makeId)){

             return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $makeId = $request->makeId;
            $data = \App\Model\Automodel::where('makeId', $makeId)->where('makeType', '0')->where('deleted', '0')->orderBy('name', 'ASC')->get();

            if(count($data)){ 
                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success',
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }
        }
    }

    public function getwarehouselist_mobile() {

        $data = \App\Model\Warehouse::warehouseCountryMap();
        if(count($data)){ 
            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success',
            ], 200);
        } else {
            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);
        }
    }

    public function getautoallcharges_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->warehouseId)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId = $request->userId;
            $warehouseId = $request->warehouseId;
            $results = array();
            $itemPrice = "0.00";
            $insuranceCost = "0.00";
            $fromCity = "";
            if ($request->price != '')
                $itemPrice = $request->price;
            if ($request->fromCity != '')
                $fromCity = $request->fromCity;

            $userShippingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->first();

            $param = array(
                'fromCountry' => $request->fromCountry,
                'fromState' => $request->fromState,
                'fromCity' => $fromCity,
                'toCountry' => $userShippingAddress->countryId,
                'toState' => $userShippingAddress->stateId,
                'toCity' => $userShippingAddress->cityId
            );
            $currencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);
            $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
            $exchangeRate = 1;
            

            $autoData = array('make' => $request->make, 'model' => $request->model);
            $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
            if (!empty($shippingCostsArr)) {
                $shippingCost = $shippingCostsArr['totalShippingCost'];
                $insuranceCost = $shippingCostsArr['insurance'];
            }
            $processingFee = \App\Model\Procurementfees::getProcessingFee($warehouseId, $itemPrice);
            $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $fromCity);

            $totalCost = round(($processingFee + $pickupCost + $shippingCost + $itemPrice+$insuranceCost), 2);
            $results['processingFee'] = $processingFee;
            $results['pickupCost'] = $pickupCost;
            $results['shippingCost'] = $shippingCost;
            $results['totalCost'] = $totalCost;
            $results['currencyCode'] = $currencyCode;
            $results['defaultCurrency'] = $currencyCode;
            $results['currencySymbol'] = $currencySymbol;
            $results['exchangeRate'] = $exchangeRate;
            $results['insuranceCost'] = $insuranceCost;
            $results['isInsuranceCharged'] = 'Y';

            if(!empty($results)){
                return response()->json([
                'status' => '200',
                'data' => $results,
                'message' => 'success',
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }
            
        }
    }

    public function getshipmycarallcharges_mobile(Request $request) {

        if(empty($request->formData) || empty($request->userId) || empty($request->warehouseId)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $results = array();
            $formData = $request->formData;
            $userId = $request->userId;
            $warehouseId = $request->warehouseId;
            $itemPrice = "";
            $fromCity = "";
            $insuranceCost = "0.00";

            if ($formData['itemPrice'] != '')
                $itemPrice = $formData['itemPrice'];
            if ($formData['pickupCity'] != '')
                $fromCity = $formData['pickupCity'];

            $param = array(
                'fromCountry' => $formData['pickupCountry'],
                'fromState' => $formData['pickupState'],
                'fromCity' => $fromCity,
                'toCountry' => $formData['destinationCountry'],
                'toState' => $formData['destinationState'],
                'toCity' => $formData['destinationCity']
            );

            $autoData = array('make' => $formData['makeId'], 'model' => $formData['modelId']);
            $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
            if (!empty($shippingCostsArr)) {
                $shippingCost = $shippingCostsArr['totalShippingCost'];
                $insuranceCost = $shippingCostsArr['insurance'];
            }
            $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $fromCity);
            $totalCost = round(($pickupCost + $shippingCost + $insuranceCost), 2);

            $currencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);
            $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
            $exchangeRate = 1;
            $results['currencyCode'] = $currencyCode;
            $results['defaultCurrency'] = $currencyCode;
            $results['currencySymbol'] = $currencySymbol;
            $results['exchangeRate'] = $exchangeRate;
            $results['pickupCost'] = $pickupCost;
            $results['shippingCost'] = $shippingCost;
            $results['totalCost'] = $totalCost;
            $results['insuranceCost'] = $insuranceCost;
            $results['isInsuranceCharged'] = 'Y';

            if(!empty($results)){ 
                return response()->json([
                'status' => '200',
                'data' => $results,
                'message' => 'success',
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }

        }
    }

    /*payment method list*/
    public function getpaymentmethodlist_mobile(Request $request) {
        
        if (empty($request->billingCountryId)) {
            
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else { 

            $paymentMethodList = \App\Model\Paymentmethod::getallpaymentmethods($request->billingCountryId);
            
            if(!empty($paymentMethodList)){

                return response()->json([
                'status' => '200',
                'data' =>$paymentMethodList,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' =>(object)[],
                'message' => 'success'
                ], 200);

            }
        }
    }

    public function getamountforpaystack_mobile(Request $request) {

        if(empty($request->payAmount) || empty($request->defaultCurrency)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $payAmount = round($request->payAmount, 2);
            $defaultCurrency = $request->defaultCurrency;
            if($defaultCurrency != 'NGN')
            {
              $exchangeRateToNaira = \App\Model\Currency::currencyExchangeRate($defaultCurrency, 'NGN');
              $convertedAmount = round(($payAmount * $exchangeRateToNaira), 2);
            }else{
              $convertedAmount = $payAmount;
            }
            $amountForPaystack = round($convertedAmount * 100, 2);
            
            return response()->json([
                    'status' => '200',
                    'data' => (object)['amountForPaystack' => $amountForPaystack],
                    'message' => 'success',
                ], 200);

        }

    }

    /*get location country list*/
    public function getlocationcountrylist_mobile() {
        
        $data = array();
        $location = new \App\Model\Location;
        $country = new Country;
        $locationTable = $location->table;
        $countryTable = $country->table;
        $data = DB::table($countryTable)->select(DB::raw("DISTINCT ".$country->prefix."$countryTable.id"),"$countryTable.*")->join("$locationTable","$countryTable.id","=","$locationTable.countryId")->where("$locationTable.status", "1")->where("$locationTable.deleted", "0")->get();
        if(!empty($data)){
            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success'
                ], 200);

        } else {
            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
                ], 200);
        }
    }

    /*get location details*/
    public function getlocationdetails_mobile(Request $request) {

        if(empty($request->countryId) || empty($request->cityId)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $countryId = $request->countryId;
            $stateId = $request->stateId;
            $cityId = $request->cityId;
            $locationDetails = array();

            $where = "countryId=".$countryId." AND cityId=".$cityId;
            if($stateId!="")
                $where .= " AND stateId=".$stateId;
            $locationDetails = \App\Model\Location::whereRaw($where)
                                ->where('status','1')->where('deleted','0')->get();

            
            if($locationDetails->count()>0)
            {
                return response()->json([
                'status' => '200',
                'data' => $locationDetails,
                'message' => 'success'
                    ], 200);
            }
            else
            {
                return response()->json([
                'status' => '400',
                'data' => (object)[],
                'message' => 'No location found for selected country/state/city'
                    ], 200);
            }
        }

    }

    public function shipmenttrackingdetails_mobile(Request $request) {

        if(empty($request->orderNumber)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'Please provide order number!'
            ], 200);

        } else {

            $orderNumber = $request->orderNumber;
            $result = array();
            $orderData = \App\Model\Order::where('orderNumber', $orderNumber)->first();
            if (!empty($orderData)) {

                if ($orderData->type == 'shipment') {

                    $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatus();
                    $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);
                    $deliveryData = \App\Model\Shipmentdelivery::where('shipmentId', $orderData->shipmentId)->get();
                    $deliveryNum = 1;
                    if ($deliveryData->count() > 0) {
                        foreach ($deliveryData as $eachDeliveryData) {
                            $shippingMethodDetails = \App\Model\Shippingmethods::where('shippingid',$eachDeliveryData->shippingMethodId)->first();
                            $itemArr = array('deliveryId' => $deliveryNum,'shippingMethod' => $shippingMethodDetails->shipping,'shippingMethodLogo'=>url('uploads/shipping/'.$shippingMethodDetails->companyLogo));
                            $statusdata = array();
                            $shipmentStatusLog = \App\Model\Shipmentstatuslog::where('shipmentId', $orderData->shipmentId)->where('deliveryId', $eachDeliveryData->id)->get();
                            $statusCompletedIndex = 0;
                            if ($shipmentStatusLog->count() > 0) {
                                foreach ($shipmentStatusLog as $eachStatus) {
                                    $statusdata[] = array(
                                        'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                        'date' => $eachStatus->updatedOn,
                                        'completed' => true,
                                        'visited' => true,
                                        'comment' => $eachStatus->comments,
                                    );
                                    $statusCompletedIndex++;
                                }
                            }
                            for($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                                $statusdata[] = array(
                                    'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                                    'date' => '',
                                    'completed' => FALSE,
                                    'visited' => FALSE,
                                );
                            }
                            $itemArr['statusDetails'] = $statusdata;
                            array_push($result, $itemArr);
                            $deliveryNum++;
                        }
                        
                        
                    }


                } else if ($orderData->type == 'autoshipment') {
                
                    $shipmentDeliveryStatus = \App\Model\Autoshipment::allStatus();
                    $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);
                    $itemArr = array('deliveryId' => '','shippingMethod'=>'','shippingMethodLogo'=>'');
                    $statusdata = array();
                    $shipmentStatusLog = \App\Model\Autoshipmentstatuslog::where('autoshipmentId', $orderData->shipmentId)->get();
                    $statusCompletedIndex = 0;
                    if ($shipmentStatusLog->count() > 0) {
                        foreach ($shipmentStatusLog as $eachStatus) {
                            $statusdata[] = array(
                                'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                'date' => $eachStatus->updatedOn,
                                'completed' => true,
                                'visited' => true,
                                
                            );
                            $statusCompletedIndex++;
                        }
                    }
                    for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                        $statusdata[] = array(
                            'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                            'date' => '',
                            'completed' => FALSE,
                            'visited' => FALSE,
                        );
                    }
                    $itemArr['statusDetails'] = $statusdata;
                    array_push($result, $itemArr);
                }
            
                if ($orderData->type == 'fillship') {
                    $itemArr['statusDetails'] = \App\Model\Fillshipshipmentstatuslog::getStatusLog($orderData->shipmentId);
                    array_push($result, $itemArr);
                }

                if (!empty($result)) {

                    return response()->json([
                        'status' => '200',
                        'data' => $result,
                        'message' => 'success'
                            ], 200);

                } else {

                    return response()->json([
                    'status' => '400',
                    'data' => (object)[],
                    'message' => 'No shipment details found!'
                        ], 200);
                }



            } else {

               return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'Invalid order number!'
                ], 200);
             
            }

        }
    }

    public function itemarriveddetails_mobile(Request $request) {

        if(empty($request->trackingNumber)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);


        } else {

            $trackingNumber = $request->trackingNumber;
            $result = array();
            
            $shipmentArrivedDetails = \App\Model\Shipmenttrackingnumber::where('trackingNumber','LIKE',"%$trackingNumber%")->first();
            if (!empty($shipmentArrivedDetails)) {

                //Search for the item in quick ship out which are in shipment tracking

                $itemInQuickshipout = \App\Model\Commercialinvoice::where('invoiceDetails','LIKE',"%$trackingNumber%")->first();

                if(!empty($itemInQuickshipout))
                {
                    //Find Quick shipout and get details of log

                    $quickshipoutId = $itemInQuickshipout->id;

                    $details = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $quickshipoutId)->groupBy('status')->orderBy('id')->get();

                    $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatusQuickShipout();
                    $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);

                     $statusCompletedIndex = 0;
                    if ($details->count() > 0) {
                        foreach ($details as $eachStatus) {
                            $statusdata[] = array(
                                'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                'date' => $eachStatus->updatedOn,
                                'completed' => true,
                                'visited' => true,
                                'comment' => $eachStatus->comments,
                            );
                            $statusCompletedIndex++;
                        }
                    }
                    for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                        $statusdata[] = array(
                            'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                            'date' => '',
                            'completed' => FALSE,
                            'visited' => FALSE,
                        );
                    }

                    $itemArr['statusDetails'] = $statusdata;
                    array_push($result, $itemArr);

                    return response()->json([
                        'status' => '200',
                        'data' => $result,
                        'message' => 'success'
                    ], 200);


                }   else{

                    return response()->json([
                        'status' => '200',
                        'data' => \Carbon\Carbon::parse($shipmentArrivedDetails->createdOn)->format('m-d-Y h:i a'),
                        'message' => 'success'
                    ], 200);

                }
                
            } else {

                //Search for the item in quick ship out which are not in shipment tracking
                
                $itemInQuickshipout = \App\Model\Commercialinvoice::where('invoiceDetails','LIKE',"%$trackingNumber%")->first();

                if(!empty($itemInQuickshipout))
                {
                    //Find Quick shipout and get details of log

                    $quickshipoutId = $itemInQuickshipout->id;

                    $details = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $quickshipoutId)->groupBy('status')->orderBy('id')->get();

                    $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatusQuickShipout();
                    $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);

                    $statusCompletedIndex = 0;

                    if ($details->count() > 0) {
                        foreach ($details as $eachStatus) {
                            $statusdata[] = array(
                                'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                'date' => $eachStatus->updatedOn,
                                'completed' => true,
                                'visited' => true,
                                'comment' => $eachStatus->comments,
                            );
                            $statusCompletedIndex++;
                        }
                    }

                    for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                        $statusdata[] = array(
                            'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                            'date' => '',
                            'completed' => FALSE,
                            'visited' => FALSE,
                        );
                    }

                    $itemArr['statusDetails'] = $statusdata;
                    array_push($result, $itemArr);

                    return response()->json([
                        'status' => '200',
                        'data' => $result,
                        'message' => 'success'
                    ], 200);

                } else {

                    return response()->json([
                        'status' => '200',
                        'data' => (object)[],
                        'message' => 'success'
                    ], 200);

                }
                
            }
        }
    }

    public function getamountforpayeezy_mobile(Request $request) {
        
        if(empty($request->payAmount) || empty($request->defaultCurrency)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $payAmount = round($request->payAmount, 2);
            $defaultCurrency = $request->defaultCurrency;
            
            if($defaultCurrency != 'USD')
            {
               $exchangeRateToNaira = \App\Model\Currency::currencyExchangeRate($defaultCurrency, 'USD');
               $convertedAmount = ($payAmount * $exchangeRateToNaira); 
            }else{
                $convertedAmount = $payAmount;
            }
            
            $amountDisplay = round($convertedAmount,2);
            $amountForPayeezy = round($amountDisplay*100);
            //$amountForPayeezy = $amountDisplay*100;
            return response()->json([
                'status' => '200',
                'data' => array('amountForPayeezy' => $amountForPayeezy, 'amountDisplay' => $amountDisplay),
                'message' => 'success'
            ], 200);

        }
    }

}
