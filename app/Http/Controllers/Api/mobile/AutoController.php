<?php
namespace App\Http\Controllers\Api\mobile;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Autowebsite;
use App\Model\Autowebsitemap;
use App\Model\Automake;
use App\Model\Automodel;
use App\Model\Autoshipmentitemimage;
use DB;
use config;
use customhelper;
use PDF;
use Mail;

class AutoController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->_perPage = 10;
    }

    public function getwebsiteist_mobile() {
        $data = Autowebsite::where('deleted', '0')->where('status', '1')->get();

        foreach($data as $recordId => $record)
        {
            $data[$recordId]->logo = url('uploads/auto/'.$record->logo);
        }
        if(count($data)){
            return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success',
            ], 200);
        } else {
            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);
        }
    }

    public function getwebsitemake_mobile(Request $request) {
        
        if(empty($request->websiteId)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {
            $websiteId = $request->websiteId;
            $data = Autowebsitemap::select(DB::raw("distinct makeId"))->where('websiteId',$websiteId)->with('make')->get();
            if(count($data)){ 
                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success',
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }
        }
    }

    public function getwebsitemodel_mobile(Request $request) {
        
        if(empty($request->websiteId) || empty($request->makeId)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {
        
            $websiteId = $request->websiteId; 
            $makeId = $request->makeId;
            $data = Autowebsitemap::select(DB::raw('modelId'))->where('websiteId',$websiteId)->where('makeId',$makeId)->with('model')->get();
            
            if(count($data)){ 
                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success',
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }
            
        }
    }

    public function savebuycardata_mobile(Request $request) {

        if(empty($request->userId) || empty($request->warehouseId)){
            
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured in input'
            ], 200);

        } else {

            $userId = $request->userId;
            $warehouseId = $request->warehouseId;
            $datasave = \App\Model\Procurement::saveBuyCarForMe($userId,$warehouseId,$request);

            if(is_array($datasave) && !empty($datasave) && !array_key_exists('payment_error',$datasave))
            {
                $invoiceUniqueId = $datasave['invoiceUniqueId'];
                $invoiceId = $datasave['invoiceId'];
                $procurementId = $datasave['procurementId'];
                $to = $datasave['userEmail'];
                if (isset($invoiceId)) {
                    $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                    $data['invoiceFor'] = 'frontend';
                    if($datasave['paymentStatus']== 'unpaid') 
                    {
                        $fileName = "Invoice_" . $invoiceUniqueId . ".pdf";
                        PDF::loadView('Administrator.autoshipment.buyacarinvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                        Mail::send(['html' => 'mail'], ['content' => 'Invoice for Buy a car for Me #' . $procurementId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                            $message->from(Config::get('constants.EmailSettings.FROMEMAIL'), Config::get('constants.EmailSettings.FROMNAME'));
                            $message->subject("$invoiceUniqueId - Invoice Details");
                            $message->to($to);
                            $message->attach(public_path('exports/invoice/' . $fileName));
                        });
                    }
                    else if($datasave['paymentStatus'] == 'paid')
                    {
                        $fileName = "Receipt_" . $invoiceUniqueId . ".pdf";
                        PDF::loadView('Administrator.autoshipment.buyacarreceipt', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                        Mail::send(['html' => 'mail'], ['content' => 'Receipt for Buy a car for Me #' . $procurementId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                            $message->from(Config::get('constants.EmailSettings.FROMEMAIL'), Config::get('constants.EmailSettings.FROMNAME'));
                            $message->subject("$invoiceUniqueId - Receipt Details");
                            $message->to($to);
                            $message->attach(public_path('exports/invoice/' . $fileName));
                        });
                    }
                    
                }
            
                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            } else if(!empty($datasave) && array_key_exists('payment_error', $datasave)) {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => $datasave['error_msg']
                ], 200);

                
            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);

            }
                
        }


    }

    public function saveshipmycar_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->warehouseId)){
            
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured in input'
            ], 200);

        } else {

            $userId = $request->userId;
            $warehouseId = $request->warehouseId;
            
            $datasave = \App\Model\Autoshipment::saveShipMyCar($request,$userId,$warehouseId);
            
            if(is_array($datasave) && !empty($datasave) && !array_key_exists('payment_error',$datasave))
            {
                $invoiceUniqueId = $datasave['invoiceUniqueId'];
                $invoiceId = $datasave['invoiceId'];
                $autoShipmentId = $datasave['autoShipmentId'];
                $to = $datasave['userEmail'];
                if (isset($invoiceId)) {
                    $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                    $data['invoiceFor'] = 'frontend';
                    if($datasave['paymentStatus']== 'unpaid') 
                    {
                        $fileName = "Invoice_" . $invoiceUniqueId . ".pdf";
                        PDF::loadView('Administrator.autoshipment.buyacarinvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                        Mail::send(['html' => 'mail'], ['content' => 'Invoice for Ship My Car #' . $autoShipmentId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                            $message->from(Config::get('constants.EmailSettings.FROMEMAIL'), Config::get('constants.EmailSettings.FROMNAME'));
                            $message->subject("$invoiceUniqueId - Invoice Details");
                            $message->to($to);
                            $message->attach(public_path('exports/invoice/' . $fileName));
                        });
                    }
                    else if($datasave['paymentStatus'] == 'paid')
                    {
                        $fileName = "Receipt_" . $invoiceUniqueId . ".pdf";
                        PDF::loadView('Administrator.autoshipment.buyacarreceipt', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                        Mail::send(['html' => 'mail'], ['content' => 'Receipt for Ship My Car #' . $autoShipmentId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                            $message->from(Config::get('constants.EmailSettings.FROMEMAIL'), Config::get('constants.EmailSettings.FROMNAME'));
                            $message->subject("$invoiceUniqueId - Receipt Details");
                            $message->to($to);
                            $message->attach(public_path('exports/invoice/' . $fileName));
                        });
                    }
                    
                }
            
                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            } else if(!empty($datasave) && array_key_exists('payment_error', $datasave)){

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => $datasave['error_msg']
                ], 200);


            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);

            }
        }
        
    }


    /**
     * Method for get details for the car I have bought
     * @param Request $request
     * @return array
     */
    public function getcarihaveboughtdetails_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->currentPage) || empty($request->perPage)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            /* GET THE USER ID */
            $userId = $request->userId;

            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
            $offset1 = ($page - 1) * $perPage;


            $statusExcept = "completed";

            $totalItems = \App\Model\Procurement::gettotalcarsihaveboughtdetails($userId);
            $ps = \App\Model\Procurement::getcarsihaveboughtdetails($userId, $perPage, $offset1);
            
            #dd($ps);
            $cars = array();
            
            $imagePath = url('/').'/uploads/auto/carpickup/';

            foreach($ps as $key => $val){

                $GetTracks = \App\Model\Procurementitemstatus::where('procurementId', $val->procurementId)->where("procurementItemId", $val->id)->get();
                $countTracks = count($GetTracks);
                $tracks = array();
                foreach($GetTracks as $keyT => $valT){
                    $rewriteKey = $keyT + 1;

                    
                    if($valT->status == "submitted"){
                        if($rewriteKey != $countTracks){
                               $tracks[] = array(
                                "title"           => "Submitted",
                                "date"            => $valT->updatedOn,
                                "completed"       => true,
                                "visited"         => false
                                );
                            } else {
                                /* LAST KEY */
                                $tracks[] = array(
                                "title"           => "Submitted",
                                "date"            => $valT->updatedOn,
                                "completed"       => false,
                                "visited"         => true
                                );

                                $tracks[] = array(
                                "title"           => "Order Placed",
                                "date"            => "",
                                "completed"       => false,
                                "visited"         => false
                                );

                                $tracks[] = array(
                                "title"           => "Picked Up",
                                "date"            => "",
                                "completed"       => false,
                                "visited"         => false
                                );

                                $tracks[] = array(
                                "title"           => "Received",
                                "date"            => "",
                                "completed"       => false,
                                "visited"         => false
                                );
                        } 
                    }

                    if($valT->status == "orderplaced"){
                        if($rewriteKey != $countTracks){
                               $tracks[] = array(
                                "title"           => "Order Placed",
                                "date"            => $valT->updatedOn,
                                "completed"       => true,
                                "visited"         => false
                                );
                            } else {
                                /* LAST KEY */
                                $tracks[] = array(
                                "title"           => "Order Placed",
                                "date"            => $valT->updatedOn,
                                "completed"       => false,
                                "visited"         => true
                                );

                                $tracks[] = array(
                                "title"           => "Picked Up",
                                "date"            => "",
                                "completed"       => false,
                                "visited"         => false
                                );

                                $tracks[] = array(
                                "title"           => "Received",
                                "date"            => "",
                                "completed"       => false,
                                "visited"         => false
                                );
                        } 
                    }

                    if($valT->status == "pickedup"){
                        if($rewriteKey != $countTracks){
                               $tracks[] = array(
                                "title"           => "Picked Up",
                                "date"            => $valT->updatedOn,
                                "completed"       => true,
                                "visited"         => false
                                );
                            } else {
                                /* LAST KEY */
                                $tracks[] = array(
                                "title"           => "Picked Up",
                                "date"            => $valT->updatedOn,
                                "completed"       => false,
                                "visited"         => true
                                );

                                $tracks[] = array(
                                "title"           => "Received",
                                "date"            => "",
                                "completed"       => false,
                                "visited"         => false
                                );
                        } 
                    }

                    if($valT->status == "received"){
                        if($rewriteKey != $countTracks){
                               $tracks[] = array(
                                "title"           => "Received",
                                "date"            => $valT->updatedOn,
                                "completed"       => true,
                                "visited"         => false
                                );
                            } else {
                                /* LAST KEY */
                                $tracks[] = array(
                                "title"           => "Received",
                                "date"            => $valT->updatedOn,
                                "completed"       => false,
                                "visited"         => true
                                );
                        } 
                    }
                    
                }

                if($val->itemImage != ""){
                    $carImg             = $imagePath.$userId."/".$val->itemImage;
                } else {
                    $carImg             = url('/')."/administrator/img/default-no-img.jpg";
                }


                /* SET CURRENCY SYMBOL LOGIC */
                $totalItemCost = customhelper::getCurrencySymbolFormat($val->totalItemCost * $val->exchangeRate, $val->paidCurrencyCode);
                $totalProcessingFee = customhelper::getCurrencySymbolFormat($val->totalProcessingFee * $val->exchangeRate, $val->paidCurrencyCode);
                $pickupCost = customhelper::getCurrencySymbolFormat($val->totalPickupCost * $val->exchangeRate, $val->paidCurrencyCode);
                $shippingCost = customhelper::getCurrencySymbolFormat($val->totalShippingCost * $val->exchangeRate, $val->paidCurrencyCode);
                $insuranceCost = customhelper::getCurrencySymbolFormat($val->totalInsurance * $val->exchangeRate, $val->paidCurrencyCode);
                $taxCost = customhelper::getCurrencySymbolFormat($val->totalTax * $val->exchangeRate, $val->paidCurrencyCode);
                $discountCost = customhelper::getCurrencySymbolFormat($val->totalDiscount * $val->exchangeRate, $val->paidCurrencyCode);
                $totalCost = customhelper::getCurrencySymbolFormat($val->totalCost * $val->exchangeRate, $val->paidCurrencyCode);
                /*if($val->isCurrencyChanged != "Y"){
                    
                    if($val->exchangeRate != NULL){
                        $totalItemCost = customhelper::getCurrencySymbolFormat($val->totalItemCost * $val->exchangeRate, $val->defaultCurrencyCode);
                        $totalProcessingFee = customhelper::getCurrencySymbolFormat($val->totalProcessingFee * $val->exchangeRate, $val->defaultCurrencyCode);
                        $pickupCost = customhelper::getCurrencySymbolFormat($val->totalPickupCost * $val->exchangeRate, $val->defaultCurrencyCode);
                        $shippingCost = customhelper::getCurrencySymbolFormat($val->totalShippingCost * $val->exchangeRate, $val->defaultCurrencyCode);
                        $totalProcurementCost = customhelper::getCurrencySymbolFormat($val->totalProcurementCost * $val->exchangeRate, $val->defaultCurrencyCode);
                    } else {
                        $totalItemCost = customhelper::getCurrencySymbolFormat($val->totalItemCost, $val->defaultCurrencyCode);
                        $totalProcessingFee = customhelper::getCurrencySymbolFormat($val->totalProcessingFee, $val->defaultCurrencyCode);
                        $pickupCost = customhelper::getCurrencySymbolFormat($val->totalPickupCost, $val->defaultCurrencyCode);
                        $totalProcurementCost = customhelper::getCurrencySymbolFormat($val->totalProcurementCost, $val->defaultCurrencyCode);
                    }
                } else {
                    if($val->exchangeRate != NULL){
                        $totalItemCost = customhelper::getCurrencySymbolFormat($val->totalItemCost * $val->exchangeRate, $val->paidCurrencyCode);
                        $totalProcessingFee = customhelper::getCurrencySymbolFormat($val->totalProcessingFee * $val->exchangeRate, $val->paidCurrencyCode);
                        $pickupCost = customhelper::getCurrencySymbolFormat($val->totalPickupCost * $val->exchangeRate, $val->paidCurrencyCode);
                        $totalProcurementCost = customhelper::getCurrencySymbolFormat($val->totalProcurementCost * $val->exchangeRate, $val->paidCurrencyCode);
                    } else {
                        $totalItemCost = customhelper::getCurrencySymbolFormat($val->totalItemCost, $val->paidCurrencyCode);
                        $totalProcessingFee = customhelper::getCurrencySymbolFormat($val->totalProcessingFee, $val->paidCurrencyCode);
                        $pickupCost = customhelper::getCurrencySymbolFormat($val->totalPickupCost, $val->paidCurrencyCode);
                        $totalProcurementCost = customhelper::getCurrencySymbolFormat($val->totalProcurementCost, $val->paidCurrencyCode);
                    }
                }*/


                /* CREATE ARRAY */
                $cars[] = array("carItem"=>array(
                    "make"              => $val->MAKENAME,
                    "model"             => $val->MODELNAME,
                    "Millage"             => $val->milage,
                    "procurementId"     => $val->procurementId,
                    "vin"               => $val->vinNumber,
                    "storeId"           => $val->storeId,
                    "siteCategoryId"    => $val->siteCategoryId,
                    "siteSubCategoryId" => $val->siteSubCategoryId,
                    "siteProductId"     => $val->siteProductId,
                    "websiteUrl"        => $val->websiteUrl,
                    "itemName"          => $val->itemName,
                    "itemDescription"   => $val->itemDescription,
                    "options"           => $val->options,
                    "productSKU"        => $val->productSKU,
                    "carWebsiteId"      => $val->carWebsiteId,
                    "year"              => $val->year,
                    "pickupStateId"     => $val->pickupStateId,
                    "pickupCityId"      => $val->pickupCityId,
                    "itemImage"         => $carImg,
                    "itemPrice"         => $val->itemPrice,
                    "itemQuantity"      => $val->itemQuantity,
                    "itemShippingCost"  => $val->itemShippingCost,
                    "itemTotalCost"     => $val->itemTotalCost,
                    "status"            => $val->status,
                    "deliveryCompanyId" => $val->deliveryCompanyId,
                    "trackingNumber"    => $val->trackingNumber,
                    "receivedDate"      => $val->receivedDate,
                    "fromCountry"       => $val->FROMCOUNTRY,
                    "toCountry"         => $val->TOCOUNTRY,
                    "fromState"         => $val->FROMSTATE,
                    "toState"           => $val->TOSTATE,
                    "fromCity"          => $val->FROMCITY,
                    "toCity"            => $val->TOCITY,
                    "websiteName"        => $val->CARWEBSITENAME,
                    "totalItemCost"     => $totalItemCost,
                    "totalProcessingFee"    => $totalProcessingFee,
                    "pickupCost" => $pickupCost,
                    "shippingCost" => $shippingCost,
                    "insuranceCost" => $insuranceCost,
                    "taxCost" => $taxCost,
                    "discountCost" => $discountCost,
                    "totalCostPaid" => $totalCost,
                    "exchangeRate"         => $val->exchangeRate,
                    "paidCurrencyCode"  => $val->paidCurrencyCode,
                    "defaultCurrencyCode"   => $val->defaultCurrencyCode,
                    "isCurrencyChanged"     => $val->isCurrencyChanged,
                    "paymentMethodId"   => $val->paymentMethodId,
                    "paymentStatus"     => $val->paymentStatus,
                    "totalCost"         => customhelper::getCurrencySymbolFormat($val->totalCost),
                    "totalTax"          => $val->totalTax,
                    "totalInsurance"    => $val->totalInsurance,
                    "isDutyCharged"     => $val->isDutyCharged,
                    "totalClearingDuty" => $val->totalClearingDuty,
                    "totalShippingCost" => customhelper::getCurrencySymbolFormat($val->totalShippingCost),
                    "totalPickupCost"   => $val->totalPickupCost,
                    "tracks"            => $tracks
                    )
                );



            }
            
            if (!empty($cars)) {
                
                return response()->json([
                'status' => '200',
                'data' => ['cars'=>$cars,'totalItems'=> $totalItems,'itemsPerPage'=> $perPage],
                'message' => 'success'
                ], 200);


            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);
            }
        }

    }
    
    

    /**
     * Method for get warehouse details for car under auto section
     * @param Request $request
     * @return array
     */
    public function getcarwarehousedetails_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->currentPage) || empty($request->perPage)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            /* GET THE USER ID */
            $userId = $request->userId;

            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
            $offset1 = ($page - 1) * $perPage;


            $statusExcept = "completed";

            $totalItems = \App\Model\Autoshipment::gettotalcarwarehousedetails($userId);
            $ps = \App\Model\Autoshipment::getcarwarehousedetails($userId, $perPage, $offset1);
            
            $cars = array();
            
            $imagePath = url('/').'/uploads/auto/carpickup/';

            foreach($ps as $key => $val){

                $auto_id = $val->AutoId;

                $GetTracks = \App\Model\Autoshipmentstatuslog::where('autoshipmentId', $auto_id)->orderby("id", 'desc')->get();
                $countTracks = count($GetTracks);
                $tracks = array();

                $tracks[] = array("title"=> "Submitted", "date"=> '', "completed"=> false, "visited"=> false);
                $tracks[] = array("title"=> "Order Placed","date"=> '',"completed"=> false,"visited"=> false);
                $tracks[] = array("title"=> "Picked Up","date"=> '',"completed"=> false,"visited"=> false);
                $tracks[] = array("title"=> "Received","date"=> '',"completed"=> false,"visited"=> false);
                $tracks[] = array("title"=> "Custom Verification","date"=> '',"completed"=> false,"visited"=> false);
                $tracks[] = array("title"=> "Sailed","date"=> '',"completed"=> false,"visited"=> false);
                $tracks[] = array("title"=> "Custom Clearing","date"=> '',"completed"=> false,"visited"=> false);
                $tracks[] = array("title"=> "Avail Pickup","date"=> '',"completed"=> false,"visited"=> false);
                $tracks[] = array("title"=> "Completed","date"=> '',"completed"=> false,"visited"=> false);


                foreach($GetTracks as $keyT => $valT){
                    $rewriteKey = $keyT + 1;

                    /* MAKE DEFAULT ARRAY */

                    if($valT->status == "submitted"){
                        if($rewriteKey != $countTracks){
                           $tracks[0] = array("title"=> "Submitted","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[0] = array("title"=> "Submitted","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    if($valT->status == "orderplaced"){
                        if($rewriteKey != $countTracks){
                           $tracks[1] = array("title"=> "Order Placed","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[1] = array("title"=> "Order Placed","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    if($valT->status == "pickedup"){
                        if($rewriteKey != $countTracks){
                           $tracks[2] = array("title"=> "Picked Up","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[2] = array("title"=> "Picked Up","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    if($valT->status == "received"){
                        if($rewriteKey != $countTracks){
                           $tracks[3] = array("title"=> "Received","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[3] = array("title"=> "Received","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    if($valT->status == "customverification"){
                        if($rewriteKey != $countTracks){
                           $tracks[4] = array("title"=> "Custom Verification","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[4] = array("title"=> "Custom Verification","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    

                    if($valT->status == "sailed"){
                        if($rewriteKey != $countTracks){
                           $tracks[5] = array("title"=> "Sailed","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[5] = array("title"=> "Sailed","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    if($valT->status == "customclearing"){
                        if($rewriteKey != $countTracks){
                           $tracks[6] = array("title"=> "Custom Clearing","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[6] = array("title"=> "Custom Clearing","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    if($valT->status == "availpickup"){
                        if($rewriteKey != $countTracks){
                           $tracks[7] = array("title"=> "Avail Pickup","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[7] = array("title"=> "Avail Pickup","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    if($valT->status == "completed"){
                        if($rewriteKey != $countTracks){
                           $tracks[8] = array("title"=> "Completed","date"=> $valT->updatedOn,"completed"=> true,"visited"=> false);
                        } else {
                        /* LAST KEY */
                            $tracks[8] = array("title"=> "Completed","date"=> $valT->updatedOn,"completed"=> false,"visited"=> true);
                        } 
                    }

                    
                }

                


                /* SET CURRENCY SYMBOL LOGIC */
                if($val->isCurrencyChanged != "Y"){
                    if($val->exchangeRate != NULL){
                        $pickupCost = customhelper::getCurrencySymbolFormat($val->pickupCost * $val->exchangeRate, $val->defaultCurrencyCode);
                        $insuranceCost = customhelper::getCurrencySymbolFormat($val->insuranceCost * $val->exchangeRate, $val->defaultCurrencyCode);
                        $taxCost = customhelper::getCurrencySymbolFormat($val->taxCost * $val->exchangeRate, $val->defaultCurrencyCode);
                        $shipmentCost = customhelper::getCurrencySymbolFormat($val->shipmentCost * $val->exchangeRate, $val->defaultCurrencyCode);
                        $totalCost = customhelper::getCurrencySymbolFormat($val->totalCost * $val->exchangeRate, $val->defaultCurrencyCode);
                    } else {
                        $pickupCost = customhelper::getCurrencySymbolFormat($val->pickupCost, $val->defaultCurrencyCode);
                        $insuranceCost = customhelper::getCurrencySymbolFormat($val->insuranceCost, $val->defaultCurrencyCode);
                        $taxCost = customhelper::getCurrencySymbolFormat($val->taxCost, $val->defaultCurrencyCode);
                        $shipmentCost = customhelper::getCurrencySymbolFormat($val->shipmentCost, $val->defaultCurrencyCode);
                        $totalCost = customhelper::getCurrencySymbolFormat($val->totalCost, $val->defaultCurrencyCode);
                    }
                } else {
                    if($val->exchangeRate != NULL){
                        $pickupCost = customhelper::getCurrencySymbolFormat($val->pickupCost * $val->exchangeRate, $val->paidCurrencyCode);
                        $insuranceCost = customhelper::getCurrencySymbolFormat($val->insuranceCost * $val->exchangeRate, $val->paidCurrencyCode);
                        $taxCost = customhelper::getCurrencySymbolFormat($val->taxCost * $val->exchangeRate, $val->paidCurrencyCode);
                        $shipmentCost = customhelper::getCurrencySymbolFormat($val->shipmentCost * $val->exchangeRate, $val->paidCurrencyCode);
                        $totalCost = customhelper::getCurrencySymbolFormat($val->totalCost * $val->exchangeRate, $val->paidCurrencyCode);
                    } else {
                        $pickupCost = customhelper::getCurrencySymbolFormat($val->pickupCost, $val->paidCurrencyCode);
                        $insuranceCost = customhelper::getCurrencySymbolFormat($val->insuranceCost, $val->paidCurrencyCode);
                        $taxCost = customhelper::getCurrencySymbolFormat($val->taxCost, $val->paidCurrencyCode);
                        $shipmentCost = customhelper::getCurrencySymbolFormat($val->shipmentCost, $val->paidCurrencyCode);
                        $totalCost = customhelper::getCurrencySymbolFormat($val->totalCost, $val->paidCurrencyCode);
                    }
                }

                $carCondition = ($val->carCondition == 'not_drivable') ? 'Not Drivable' : 'Drivable';

                /* GET IMAGE */
                    $carTitleStatus = "";
                    $getImages = \App\Model\Autoshipmentitemimage::where('autoshipmentId', $auto_id)->where("deleted", '0')->where("imageType", 'carpicture')->get();
                    if(count($getImages) > 0){ 
                        if (file_exists(public_path()."/uploads/auto/carpickup/".$userId."/".$getImages[0]->filename)) {
                            $carPic = $imagePath.$userId."/".$getImages[0]->filename;
                        } else {
                            $carPic = url('/')."/administrator/img/default-no-img.jpg";
                        }
                    } else {
                        $carPic = url('/')."/administrator/img/default-no-img.jpg";
                    }
                    
                    $getImagesTitle = \App\Model\Autoshipmentitemimage::where('autoshipmentId', $auto_id)->where("deleted", '0')->where("imageType", 'cartitle')->get();
                    $fileExt = '';
                    $base64Img = '';
                    $imgName = '';
                    if(count($getImagesTitle) > 0){
                        if (file_exists(public_path()."/uploads/auto/carpickup/".$userId."/".$getImagesTitle[0]->filename)) {
                            $carTitle = $imagePath.$userId."/".$getImagesTitle[0]->filename;
                            $carTitleStatus = "Y";
                            $encodedImg = base64_encode(file_get_contents(public_path()."/uploads/auto/carpickup/".$userId."/".$getImagesTitle[0]->filename));
                            $fileExt = pathinfo(public_path()."/uploads/auto/carpickup/".$userId."/".$getImagesTitle[0]->filename, PATHINFO_EXTENSION);
                            $base64Img = 'data:image/' . $fileExt . ';base64,' . $encodedImg;
                            $imgName = $getImagesTitle[0]->filename;
                        } else {
                            $carTitle = url('/')."/administrator/img/default-no-img.jpg";
                            $carTitleStatus = "N";
                        }
                    } else {
                        $carTitle = url('/')."/administrator/img/default-no-img.jpg";
                        $carTitleStatus = "N";
                    }


                /* CREATE ARRAY */
                $cars[] = array("carItem"=>array(
                    "id"                => $auto_id,
                    "make"              => $val->MAKENAME,
                    "model"             => $val->MODELNAME,
                    "Millage"             => $val->milage,
                    "vin"               => $val->vinNumber,
                    "websiteUrl"        => $val->websiteLink,
                    "options"           => $val->color,
                    "carCondition"      => $carCondition,
                    "year"              => $val->year,

                    "itemImage"         => $carPic,
                    "carTitle"          => $carTitle,
                    "carTitleStatus"    => $carTitleStatus,
                    "base64Img"         => $base64Img,
                    "fileExt"           => $fileExt,
                    "imgName"           => $imgName,

                    "fromCountry"       => $val->PFROMCOUNTRY,
                    "toCountry"         => $val->DTOCOUNTRY,
                    "fromState"         => $val->PFROMSTATE,
                    "toState"           => $val->DTOSTATE,
                    "fromCity"          => $val->PFROMCITY,
                    "toCity"            => $val->DTOCITY,

                    "pickupPhone"       => $val->pickupPhone,
                    "pickupEmail"       => $val->pickupEmail,
                    "pickupLocationType"    => $val->pickupLocationType,
                    "pickupDate"        => $val->pickupDate,
                    "pickupName"        => $val->pickupName,
                    "pickupAddress"     => $val->pickupAddress,

                    "recceiverName"     => $val->recceiverName,
                    "receiverAddress"   => $val->receiverAddress,
                    "receiverPhone"     => $val->receiverPhone,
                    "receiverEmail"     => $val->receiverEmail,
                    
                    "exchangeRate"      => $val->exchangeRate,
                    "paidCurrencyCode"  => $val->paidCurrencyCode,
                    "defaultCurrencyCode"   => $val->defaultCurrencyCode,
                    "isCurrencyChanged"     => $val->isCurrencyChanged,
                    "paymentMethodId"   => $val->paymentMethodId,
                    "paymentStatus"     => $val->paymentStatus,

                    "pickupCost"        => $pickupCost,
                    "insuranceCost"     => $insuranceCost,
                    "taxCost"           => $taxCost,
                    "shipmentCost"      => $shipmentCost,
                    "totalCost"         => $totalCost,
        
                    "tracks"            => $tracks
                    )
                );



            }
            
            if (!empty($cars)) {

                return response()->json([
                'status' => '200',
                'data' => ['cars'=>$cars,'totalItems'=> $totalItems,'itemsPerPage'=> $perPage],
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);
                
            }
        }

    }
    
    /**
     * Method used to validate coupon code
     * @param request $request
     * @return string
     */
    public static function validatecouponcode_mobile(Request $request) {

        if(empty($request->warehouseId) || empty($request->userId) || empty($request->couponCode)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

        
            /* INITALIZE THE VARIABLE AS FALSE */
            $match = false;
            
            $userId = $request->userId;
            $warehouseId = $request->warehouseId;

            /* GET THE COUPON CODE */
            $code = $request->couponCode;

            /* VALIDATE THE COUPON CODE */
            $currDate = date('m/d/Y');


            /* GET THE LOG */
            $couponLog = \App\Model\Couponlog::where('code', $code)->where('userId',$userId)->count();
            if ($couponLog == 1) {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Coupon already used',
                ], 200);

            }


            //DB::enableQueryLog();

            /* GET THE OFFER ID */
            $offerIdQuery = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->get();

            if (count($offerIdQuery) == 0) {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Invalid coupon',
                ], 200);

            }

            $offerCondQuery = \App\Model\Offercondition::where('offerId', $offerIdQuery[0]->id)->where('keyword', 'new_customer_reg_from_app')->count();

            if ($offerCondQuery == 0) {
                /* QUERY FOR NOT REGISTERED BY APP */
                $where = "'$currDate' between offerRangeDateFrom and offerRangeDateTo";
                $ifCodeExist = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->whereRaw($where)->get();
            } else {
                /* QUERY FOR REGISTERED BY APP */
                $ifCodeExist = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->get();
            }

            //dd(DB::getQueryLog());

            if (count($ifCodeExist) < 1) {
                
                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Invalid coupon',
                ], 200);

            } else {
                /* COUPON CODE IS VALID, DO SEARCH TO CHECK CONDITIONS */

                /* IF REGISTERED BY APP */
                $offerId = $offerIdQuery[0]->id;

                if ($offerCondQuery > 0) {
                    $userDetails = \App\Model\User::where("id", $userId)->get();
                    $createdOn = explode(" ", $userDetails[0]->createdOn);
                    $date1 = date_create($createdOn[0]);
                    $date2 = date_create(date('Y-m-d'));
                    $diff = date_diff($date1, $date2);
                    $exactDay = $diff->format("%d");

                    $getOffer = \App\Model\Offer::where('id', $offerIdQuery[0]->id)->first();
                    if ($exactDay > $getOffer->offerRangeDateFrom) {
                        
                        return response()->json([
                            'status' => '400',
                            'data' => null,
                            'message' => 'Invalid coupon',
                        ], 200);
                        exit;
                    } else {
                        //Nothing
                    }
                }

                /* GET THE CONDITIONS */
                $offerConditions = \App\Model\Offercondition::where('offerId', $ifCodeExist[0]->id)->get();

                /* GET THE CUSTOMER ADDRESS DETAILS */
                $userAddress = \App\Model\Addressbook::where("userId", $userId)->where('deleted', '0')->get();

                /* GET THE CART ITEMS ARRAY */
                foreach ($offerConditions as $keyO => $valO) {

                    /* CONDITION 3 */
                    if ($valO->keyword == "discount_on_total_amount_of_shipping") {

                        $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $ifCodeExist[0]->id)
                                ->where('keyword', 'discount_on_total_amount_of_shipping')
                                ->get();
                        $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                        $decodedValues = json_decode($OfferCond);

                        /* GET THE SHIPPING AMOUNT */
                        $shippingAmount = $totalWeight = $request->shippingCost;

                        if ($shippingAmount == null) {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }


                        if ($shippingAmount >= $decodedValues->shipping_cost->from && $shippingAmount <= $decodedValues->shipping_cost->to) {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        } else {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }
                    }
                    
                    /* CONDITION 4 */
                    if ($valO->keyword == "customer_has_not_purchased_any_goods_for_a_certain_timeframe") {

                        $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $ifCodeExist[0]->id)
                                ->where('keyword', 'customer_has_not_purchased_any_goods_for_a_certain_timeframe')
                                ->get();
                        $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                        $decodedValues = json_decode($OfferCond);

                        /* GET THE DATES FROM JSON DECODE */
                        $from = date_format(date_create($decodedValues->user_not_purchased->from), "Y-m-d");
                        $to = date_format(date_create($decodedValues->user_not_purchased->to), "Y-m-d");

                        /* GET THE LAST DATE OF THE PURCHASE */
                        $lastOrder = \App\Model\Order::where('userId', $userId)->where('status', '5')->orderby('id', 'desc')->get();
                        if(count($lastOrder)>0)
                        {
                            $notPurchasedFromRaw = explode(" ", $lastOrder[0]->createdDate);
                            $notPurchasedFrom = $notPurchasedFromRaw[0];

                            /* CALCULATE PART */
                            if (( $notPurchasedFrom >= $from ) && ( $notPurchasedFrom <= $to )) {
                                /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                                $match = false;
                                break;
                            } else {
                                /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                                $match = true;
                            }
                        }
                        else
                            $match = true;
                    }
                    
                    /* CONDITION 9 */
                    if ($valO->keyword == "new_customer_reg_from_app") {

                        /* GET THE CUSTOMER DETAILS */
                        $userDetails = \App\Model\User::where("id", $userId)->get();

                        if ($userDetails[0]->registeredBy == 'app') {
                            $appReg = '["Y"]';
                        } else {
                            $appReg = '["N"]';
                        }

                        $query = \App\Model\Offercondition::where('offerId', $ifCodeExist[0]->id)->where('keyword', 'new_customer_reg_from_app')
                                ->whereRaw('json_contains(jsonValues, ' . "'" . $appReg . "'" . ', \'$.app_user_registration\')');
                        if ($query->count() == 0) {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        } else {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        }
                    }
                    
                    /* CONDITION 2 */
                    if ($valO->keyword == "customer_of_a_specific_shipping_location") {

                        /* GET LOCATION DETAILS */
                        if($request->type == 'buy_a_car')
                        {
                            $userShippingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->first();
                            $countryId = $userShippingAddress->countryId;
                            $stateId = $userShippingAddress->stateId;
                            $cityId = $userShippingAddress->cityId;
                        }
                        else
                        {
                            $countryId = $request->destinationCountry;
                            $stateId = $request->destinationState;
                            $cityId = $request->destinationCity;
                        }
                        


                        if ($stateId == null) {
                            $stringState = "N/A";
                        } else {
                            $stringState = $stateId;
                        }

                        if ($cityId == null) {
                            $stringCity = "N/A";
                        } else {
                            $stringCity = $cityId;
                        }

                        //$makeArray = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"' . $stringCity . '"';
                        $makeArray = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"' . $stringCity . '"';
                        $makeArray2 = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"N/A"';
                        $makeArray3 = '"countrySelected":"' . $countryId . '","stateSelected":"N/A","citySelected":"N/A"';

                        #$condition2 = \App\Model\Offercondition::where('offerId', $ifCodeExist[0]->id)->where('keyword', 'customer_of_a_specific_shipping_location')->whereRaw('json_contains(jsonValues, \'{' . $makeArray . '}\')')->get();
                        $query = $condition2 = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_of_a_specific_shipping_location');

                            $query->where(function($q) use ($makeArray, $makeArray2, $makeArray3) {
                            
                                $q->whereRaw('json_contains(jsonValues, \'{' . $makeArray . '}\')');
                                $q->orWhereRaw('json_contains(jsonValues, \'{' . $makeArray2 . '}\')');
                                $q->orWhereRaw('json_contains(jsonValues, \'{' . $makeArray3 . '}\')');

                            });
                        #if (count($condition2) > 0) {
                        if (count($query->get()) > 0) {
                            $match = true;
                        } else {
                            $match = false;
                            break;
                        }
                    }
                }
                if ($match == false) {

                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'Invalid coupon',
                    ], 200);

                } else {

                    /* CALCULATE LOGIC */
                    $bonus = unserialize($offerIdQuery[0]->bonusValuesSerialized);
                    $countBonusArray = count($bonus);

                    /* GET THE TOTAL SHIPPING DISCOUNT FROM LOCAL STORAGE */
                    $totalShippingCost = $request->shippingCost;

                    /* GET THE AMOUNT */
                    if ($countBonusArray == 4) {
                        $AmountOrPoint = "Amount";
                        $pointToBeDiscounted = 0.00;
                        $type = $bonus["type"];
                        if ($type == 'Absolute') {
                            if ($bonus["discount"] > $totalShippingCost) {
                                $amountToBeDiscounted = $totalShippingCost;
                            } else {
                                $amountToBeDiscounted = $bonus["discount"];
                            }
                        } else {
                            $discountNotExceed = $bonus["discountNotExceed"];
                            $getAmount = (($totalShippingCost * $bonus["discount"]) / 100);
                            if ($getAmount > $discountNotExceed) {
                                $amountToBeDiscounted = $discountNotExceed;
                            } else {
                                $amountToBeDiscounted = number_format($getAmount, 2);
                            }
                        }
                    } else {
                        /* GET THE POINTS */
                        $AmountOrPoint = "Point";
                        $amountToBeDiscounted = 0.00;
                        if ($bonus['bonusPoint'] == 1) {
                            $pointToBeDiscounted = round($bonus["fixedAmount"]);
                        } else {
                            /* HANDLE DIVISION BY ZERO */
                            if ($bonus["bonusPer"] > 0) {
                                $getPer = ($totalShippingCost / $bonus["bonusPer"]);
                                $pointToBeDiscounted = round($getPer * $bonus["amountPer"]);
                            } else {
                                $pointToBeDiscounted = 0;
                            }
                        }
                    }

                    return response()->json([
                        'status' => '200',
                        'data' => (object)[
                            'message' => 'Coupon applied',
                            'amount_or_point' => $AmountOrPoint,
                            'amount_to_be_discounted' => $amountToBeDiscounted,
                            'point_to_be_discounted' => $pointToBeDiscounted,
                            'couponId' => $offerId
                        ],
                        'message' => 'success',
                    ], 200);

                }
            }
        }
    }
    
    public function getlocationtype_mobile() {
        
        $data = \App\Model\Locationtype::where('status','1')->where('deleted','0')->get();
        if(count($data)){
            return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success',
            ], 200);
        } else {
            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);
        }
    }

}
