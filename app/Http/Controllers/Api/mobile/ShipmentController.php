<?php

namespace App\Http\Controllers\Api\mobile;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Viewshipment;
use App\Model\ViewShipmentDetails;
use App\Model\Shipment;
use App\Model\Shipmentdelivery;
use App\Model\Shippingmethods;
use App\Model\Shippingcharges;
use App\Model\Shipmentpackage;
use App\Model\Shipmentothercharges;
use App\Model\Paymenttransaction;
use App\Model\Procurement;
use App\Model\Procurementitem;
use App\Model\Autoshipmentitemimage;
use App\Model\Autoshipment;
use App\Model\Invoice;
use App\Model\Othershipmentcharges;
use Config;
use Auth;
use customhelper;
use DB;
use Mail;
use PDF;
use Carbon\Carbon;

class ShipmentController extends Controller {

    public function __construct() {
        $this->_perPage = 20;
    }

    public function getusershipmentlist_mobile(Request $request) {
        
        if (empty($request->userId)) {
            
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $data = array();
            $userId = $request->userId;

            $where = 1;

            $warehouse = $request->warehouse;
            $deliverySnapShot = \App\Model\Generalsettings::where('settingsKey', 'delivery_snap_shot')->first();
            $itemSnapShot = \App\Model\Generalsettings::where('settingsKey', 'item_snap_shot')->first();
            $recountCost = \App\Model\Generalsettings::where('settingsKey', 'recount_cost')->first();
            $reweightCost = \App\Model\Generalsettings::where('settingsKey', 'reweight_cost')->first();
            $returnCost = \App\Model\Generalsettings::where('settingsKey', 'return_item_cost')->first();

            if (!empty($warehouse))
                $where .= " AND warehouseId = " . $warehouse['id'];

            $shipmentList = Viewshipment::where('userId', $userId)
                            ->where('prepaid', 'N')
                            ->where('paymentStatus', 'unpaid')
                            ->where('shippingMethod', 'N')
                            ->where('deleted', '0')->whereRaw($where)->orderBy('id', 'DESC')->get()->toArray();
            if (!empty($shipmentList)) {
                foreach ($shipmentList as $key => $shipment) {
                    $data['shipments'][$key] = $shipment;
                    $deliveryData = ViewShipmentDetails::where("shipmentId", $shipment['id'])->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();
                    $data['shipments'][$key]['delivery'] = $this->getDeliveryDetails($deliveryData);
                    $data['shipments'][$key]['storageCharge'] = $this->storageCalculation($deliveryData);
                    $data['shipments'][$key]['showShippingCharges'] = array();
                    $data['shipments'][$key]['isCheckOut'] = false;
                    $data['shipments'][$key]['wrongInvoice'] = 'N';
                    $data['shipments'][$key]['wrongInvoiceFile'] = '';
                    $data['shipments'][$key]['reweight_cost'] = customhelper::getCurrencySymbolFormat($reweightCost->settingsValue);
                    $data['shipments'][$key]['delivery_snap_shot'] = customhelper::getCurrencySymbolFormat($deliverySnapShot->settingsValue);
                    $data['shipments'][$key]['item_snap_shot'] = customhelper::getCurrencySymbolFormat($itemSnapShot->settingsValue);
                    $data['shipments'][$key]['recount_cost'] = customhelper::getCurrencySymbolFormat($recountCost->settingsValue);
                    $data['shipments'][$key]['return_cost'] = customhelper::getCurrencySymbolFormat($returnCost->settingsValue);
                    $data['shipments'][$key]['allItemReturned'] = Shipmentpackage::allItemReturned($shipment['id'],0,"shipment");
                    $data['shipments'][$key]['anyItemReturnRequest'] = Shipmentpackage::itemReturnRequested($shipment['id'],0,"shipment");
                    $wrongInvoiceData = \App\Model\Shipmentfile::where('shipmentId', $shipment['id'])->where('type', 'wronginvoice')->first();
                    if (!empty($wrongInvoiceData)) {
                        $data['shipments'][$key]['wrongInvoice'] = 'Y';
                        $data['shipments'][$key]['wrongInvoiceFile'] = url('uploads/shipments/' . $shipment['id'] . '/' . $wrongInvoiceData->filename);
                    }
                }
            }

            if(!empty($data)){

                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            }
        }
    }

    public function allshippingmethodlist_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->shipmentId) || empty($request->shipment) || empty($request->deliveries)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $shippingMethod = $shippingMethodData = array();
            $totalChargeableWeight = $totalQuantity = $totalShipmentChargeableWeight = $totalShipmentInventoryCharge = $totalShipmentOtherCharge = 0;
            $totalShipmentItemCost = $totalDeliveryCost = $totalShipmentStorageCharge = $totalDeliveryShipping = $totalClearingDuty = $shippingCost = $totalInsurance = 0;

            $shipmentId = $request->shipmentId;
            $userId = $request->userId;
            $shipmentDeliveryList = $request->deliveries;
            $data = $request->shipment;

            $isDutyCharged = 0;
            foreach ($shipmentDeliveryList as $delivery) {
                if($delivery['chargeableWeight'] == '' || $delivery['chargeableWeight']=='0.00')
                    continue;
                $deliveryStorageCharge = 0;
                $totalItemCost = customhelper::getExtractCurrency($delivery['totalItemCost']);
                $otherChargeCost = customhelper::getExtractCurrency($delivery['otherChargeCost']);

                $totalShipmentChargeableWeight += $delivery['chargeableWeight'];
                $totalShipmentInventoryCharge += customhelper::getExtractCurrency($delivery['inventoryCharge']);
                $totalShipmentOtherCharge += $otherChargeCost;
                $totalShipmentItemCost += customhelper::getExtractCurrency($delivery['totalItemCost']);
                /* Storage charge calculation */
                $maxStorageDate = Carbon::parse($delivery['maxStorageDate']);
                $now = Carbon::now();
                $diff = $maxStorageDate->diffInDays($now, FALSE);
                if ($diff > 0 && !empty($delivery['storageCharge'])) {
                    $totalShipmentStorageCharge += $diff * customhelper::getExtractCurrency($delivery['storageCharge']);
                    $deliveryStorageCharge = $diff * customhelper::getExtractCurrency($delivery['storageCharge']);
                }
                $totalDeliveryCost += customhelper::getExtractCurrency($delivery['totalDeliveryCost']) + $deliveryStorageCharge;

                if (!empty($delivery['isDutyCharged']))
                    $isDutyCharged = 1;

                if (!empty($delivery['shippingMethods'])) {
                    foreach ($delivery['shippingMethods'] as $deliveryShippingMethod) {
                        if (!array_key_exists($deliveryShippingMethod['shippingid'], $shippingMethod)) {
                            $totalChargeableWeight = $delivery['chargeableWeight'];
                            $totalQuantity = $delivery['totalQty'];
                            $totalShipmentCost = $totalItemCost + $otherChargeCost;
                            $deliveryId = $delivery['id'];

                            $shippingMethod[$deliveryShippingMethod['shippingid']] = array(
                                'totalWeight' => $totalChargeableWeight,
                                'totalQuantity' => $totalQuantity,
                                'totalShipmentCost' => $totalShipmentCost,
                                'deliveryId' => array(
                                    $delivery['id'],
                                ),
                            );
                        } else {
                            $totalChargeableWeight = $shippingMethod[$deliveryShippingMethod['shippingid']]['totalWeight'] + $delivery['chargeableWeight'];
                            $totalQuantity = $shippingMethod[$deliveryShippingMethod['shippingid']]['totalQuantity'] + $delivery['totalQty'];
                            $totalShipmentCost = $shippingMethod[$deliveryShippingMethod['shippingid']]['totalShipmentCost'] + ($totalItemCost + $otherChargeCost);

                            $shippingMethod[$deliveryShippingMethod['shippingid']]['totalWeight'] = $totalChargeableWeight;
                            $shippingMethod[$deliveryShippingMethod['shippingid']]['totalQuantity'] = $totalQuantity;
                            $shippingMethod[$deliveryShippingMethod['shippingid']]['totalShipmentCost'] = $totalShipmentCost;
                            array_push($shippingMethod[$deliveryShippingMethod['shippingid']]['deliveryId'], $delivery['id']);
                        }
                    }
                }
            }

            if (!empty($shippingMethod)) {
                foreach ($shippingMethod as $key => $row) {
                    $param = array(
                        'userId' => $userId,
                        'fromCountry' => $data['fromCountry'],
                        'fromState' => $data['fromState'],
                        'fromCity' => $data['fromCity'],
                        'toCountry' => $data['toCountry'],
                        'toState' => $data['toState'],
                        'toCity' => $data['toCity'],
                        'totalWeight' => !empty($row['totalWeight']) ? $row['totalWeight'] : 0,
                        'totalProcurementCost' => $row['totalShipmentCost'],
                        'totalQuantity' => $row['totalQuantity'],
                        'shippingId' => $key,
                    );
                    $shippingMethodCharge = Shippingmethods::calculateShippingMethodCharges($param);
                    if (!empty($shippingMethodCharge)) {
                        $shippingCost += $shippingMethodCharge[0]['shippingCost'];
                        $totalDeliveryShipping += $shippingMethodCharge[0]['totalShippingCost'];
                        $totalClearingDuty += $shippingMethodCharge[0]['clearingDuty'];
                        $totalInsurance += $shippingMethodCharge[0]['insurance'];

                        foreach ($shippingMethodCharge[0] as $field => $value) {
                            $shippingMethodData[$key]['showShippingCharges'] = $data['id'];
                            if ($field == 'companyLogo') {
                                $shippingMethodData[$key][$field] = asset('uploads/shipping/' . $value);
                            } else if ($field == 'shippingCost' || $field == 'duty' || $field == 'clearing' || $field == 'clearingDuty' || $field == 'totalShippingCost') {
                                $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat($value);
                            } else if ($field == 'days') {
                                $shippingMethodData[$key][$field] = $value;
                                $shippingMethodData[$key]['estimatedDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($value)->format('D, M dS Y');
                            } else if ($field == 'total') {
                                $shippingMethodData[$key][$field] = $value;
                            } else {
                                $shippingMethodData[$key][$field] = $value;
                            }
                        }
                    }
                }
            }

            if(!empty($shippingMethodData)){
                
                return response()->json([
                'status' => '200',
                'data' => (object)['shippingcharges'=>$shippingMethodData],
                'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
                ], 200);

            }

        }
    }

    /**
     * Method to fetch delivery details data
     * @param array $data
     * @return array
     */
    private function getDeliveryDetails($data) {
        if (empty($data))
            return false;

        $deliveryData = [];
        $deliveryData['deliveries'] = [];

        $totalPackage = $totalWeight = $totalChargeableWeight = $totalValue = $totalDelivery = 0;
        $itemTypeList = '';
        $wrongInventroy = 'N';

        foreach ($data as $row) {
            if (!array_key_exists($row['deliveryId'], $deliveryData['deliveries'])) {
                $totalWeight = $totalWeight + $row['deliveryWeight'];
                $totalChargeableWeight = $totalChargeableWeight + $row['deliveryChargeableWeight'];

                $deliveryData['deliveries'][$row['deliveryId']]['id'] = $row['deliveryId'];
                $deliveryData['deliveries'][$row['deliveryId']]['length'] = $row['deliveryLength'];
                $deliveryData['deliveries'][$row['deliveryId']]['width'] = $row['deliveryWidth'];
                $deliveryData['deliveries'][$row['deliveryId']]['height'] = $row['deliveryHeight'];
                $deliveryData['deliveries'][$row['deliveryId']]['weight'] = !empty($row['deliveryWeight']) ? $row['deliveryWeight'] : '0';
                $deliveryData['deliveries'][$row['deliveryId']]['chargeableWeight'] = !empty($row['deliveryChargeableWeight']) ? $row['deliveryChargeableWeight'] : '0';

                $deliveryData['deliveries'][$row['deliveryId']]['totalItemCost'] = customhelper::getCurrencySymbolFormat($row['totalItemCost']);
                $deliveryData['deliveries'][$row['deliveryId']]['inventoryCharge'] = customhelper::getCurrencySymbolFormat($row['inventoryCharge']);
                $deliveryData['deliveries'][$row['deliveryId']]['totalCost'] = customhelper::getCurrencySymbolFormat($row['totalCost']);
                $deliveryData['deliveries'][$row['deliveryId']]['maxStorageDate'] = isset($row['maxStorageDate']) ? $row['maxStorageDate'] : '';
                $deliveryData['deliveries'][$row['deliveryId']]['storageCharge'] = customhelper::getCurrencySymbolFormat($row['storageCharge']);
                $deliveryData['deliveries'][$row['deliveryId']]['shippingCost'] = customhelper::getCurrencySymbolFormat($row['shippingCost']);
                $deliveryData['deliveries'][$row['deliveryId']]['otherChargeCost'] = customhelper::getCurrencySymbolFormat($row['otherChargeCost']);
                $deliveryData['deliveries'][$row['deliveryId']]['totalDeliveryCost'] = customhelper::getCurrencySymbolFormat($row['totalCost'] - $row['totalItemCost']);

                $deliveryData['deliveries'][$row['deliveryId']]['snapshot'] = $row['snapshot'];
                $deliveryData['deliveries'][$row['deliveryId']]['snapshotRequestedOn'] = $row['snapshotRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['recount'] = $row['recount'];
                $deliveryData['deliveries'][$row['deliveryId']]['recountRequestedOn'] = $row['recountRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['reweigh'] = $row['reweigh'];
                $deliveryData['deliveries'][$row['deliveryId']]['reweighRequestedOn'] = $row['reweighRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['received'] = $row['received'];
                $deliveryData['deliveries'][$row['deliveryId']]['wrongInventory'] = "";
                $deliveryData['deliveries'][$row['deliveryId']]['deliveryAddedBy'] = $row['deliveryAddedBy'];

                $deliveryData['deliveries'][$row['deliveryId']]['shippingMethodId'] = !empty($row['shippingMethodId']) ? $row['shippingMethodId'] : "";

                $deliveryData['deliveries'][$row['deliveryId']]['shippingMethods'] = $this->getshippingmethodlist($row['shipmentId'], $row['deliveryId']);
                $deliveryData['deliveries'][$row['deliveryId']]['shipmentId'] = $row['shipmentId'];
                $deliveryData['deliveries'][$row['deliveryId']]['deliveryAllItemReturned'] = Shipmentpackage::allItemReturned(0,$row['deliveryId']);
                if($deliveryData['deliveries'][$row['deliveryId']]['deliveryAllItemReturned'] == '1')
                {
                    $deliveryData['deliveries'][$row['deliveryId']]['returnItem'] = "Y";
                }else{
                    $deliveryData['deliveries'][$row['deliveryId']]['returnItem'] = "N";
                }

                $totalDelivery++;
            }
            if (!isset($deliveryData['deliveries'][$row['deliveryId']]['totalQty']))
                $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] = 0;
            if($row['itemReturn'] == '0')
            {
                $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] = $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] + $row['itemQuantity'];
                $totalPackage = $totalPackage + $row['itemQuantity'];
            }
            $totalValue += $row['itemTotalCost'];
            $itemTypeList .= $row['itemType'] . ',';

            $deliveryData['deliveries'][$row['deliveryId']]['packages'][] = array(
                'id' => $row['packageId'],
                'siteCategoryId' => $row['itemCategoryId'],
                'categoryName' => $row['itemCategoryName'],
                'siteSubCategoryId' => $row['itemSubCategoryId'],
                'subcategoryName' => $row['itemSubCategoryName'],
                'siteProductId' => $row['itemProductId'],
                'productName' => $row['itemProductName'],
                'storeId' => $row['storeId'],
                'storeName' => $row['storeName'],
                'itemName' => $row['itemName'],
                'websiteUrl' => $row['itemWebsiteUrl'],
                'weight' => $row['itemWeight'],
                'options' => $row['itemOptions'],
                'note' => $row['itemNote'],
                'snapshotImage' => $row['itemSnapshotImage'],
                'snapshotOpt' => $row['snapshotOpt'],
                'itemType' => $row['itemType'],
                'itemQuantity' => $row['itemQuantity'],
                'itemMinPrice' => $row['itemMinPrice'],
                'itemMaxPrice' => $row['itemPrice'],
                'itemPrice' => customhelper::getCurrencySymbolFormat($row['itemPrice']),
                'itemShippingCost' => customhelper::getCurrencySymbolFormat($row['itemShippingCost']),
                'itemTotalCost' => customhelper::getCurrencySymbolFormat($row['itemTotalCost']),
                'type' => $row['packageType'],
                'deliveryCompany' => $row['deliveryCompany'],
                'deliveredOn' => $row['deliveredOn'],
                'tracking' => $row['tracking'],
                'tracking2' => $row['tracking2'],
                'deliveryNotes' => $row['deliveryNotes'],
                'itemPriceEdited' => $row['itemPriceEdited'],
                'itemDiscountedInvoiceFile'=> $row['itemDiscountedInvoiceFile'],
                // 'itemDiscountedInvoiceFilePath' => url('/administrator/shipments/downloaddiscountedinvoice/'. $row['packageId']),
                'itemDiscountedInvoiceFilePath'=> url('uploads/discounted_invoice/'. $row['itemDiscountedInvoiceFile']),
                'shipmentId'=>$row['shipmentId'],
                'itemReturn' => $row['itemReturn'],
                'returnLabel' => $row['returnLabel'],
            );
        }

        $deliveryData['totalPackage'] = $totalPackage;
        $deliveryData['totalWeight'] = $totalWeight;
        $deliveryData['totalDelivery'] = $totalDelivery;
        $deliveryData['totalChargeableWeight'] = $totalChargeableWeight;
        $deliveryData['totalValue'] = customhelper::getCurrencySymbolFormat($totalValue);
        $deliveryData['itemType'] = rtrim($itemTypeList, ',');

        return $deliveryData;
    }

    public function storageCalculation($deliveryData, $returnData = 'charge') {

        $totalShipmentStorageCharge = '0.00';
        $storageDates = array();
        if (!empty($deliveryData)) {
            foreach ($deliveryData as $delivery) {

                /* Storage charge calculation */
                $maxStorageDateRaw = isset($delivery['maxStorageDate']) ? $delivery['maxStorageDate'] : '';
                $maxStorageDate = Carbon::parse($maxStorageDateRaw);
                $now = Carbon::now();
                $diff = $maxStorageDate->diffInDays($now, FALSE);
                if ($diff > 0 && !empty($delivery['storageCharge'])) {
                    $totalShipmentStorageCharge += $diff * customhelper::getExtractCurrency($delivery['storageCharge']);
                }
            }
        }

        return $totalShipmentStorageCharge;
    }

    private function getshippingmethodlist($shipmentId, $deliveryId) {
        if (!empty($shipmentId) && !empty($deliveryId)) {
            /* Fetch shipment record */
            $shipmentData = Viewshipment::find($shipmentId)->toArray();
            /* Fetch shipment packages and delivery record */
            $shipmentDetailsData = ViewShipmentDetails::where("deliveryId", $deliveryId)->get()->toArray();

            /* Fetch all those shipping methods for which shipping charges exist 
              between source and destination zone */
            $availableShippingMethods = Shippingmethods::getDeliveryShippingMethods($shipmentData, $shipmentDetailsData);
            $shippingMethods = $availableShippingMethods;

            return $shippingMethods;
        } else {
            return false;
        }
    }

    public function calculatedeliveryshipping_mobile(Request $request) {

        if (!empty($request->deliveryId) && !empty($request->shipmentId) && !empty($request->userId) && !empty($request->shippingId) /*&& !empty($request->totalQuantity)*/) {

            $shipment = Viewshipment::select('fromCountry', 'fromState', 'fromCity', 'toCountry', 'toState', 'toCity')->where("id", $request->shipmentId)->first();
            $shipmentDetails = ViewShipmentDetails::select('deliveryChargeableWeight', 'totalItemCost', 'inventoryCharge', 'storageCharge', 'maxStorageDate', 'otherChargeCost', 'totalCost')->where("deliveryId", $request->deliveryId)->first();

            $totalShipmentCost = $shipmentDetails->totalItemCost + $shipmentDetails->otherChargeCost;
            $storageCharge = '0.00';
            if (time() > strtotime($shipmentDetails->maxStorageDate)) {
              $storageCharge = $shipmentDetails->storageCharge * floor((time() - strtotime($shipmentDetails->maxStorageDate)) / 86400);
            }

            /*  FETCH SHIPPING METHOD CHARGES DETAILS */
            $param = array(
                'userId' => $request->userId,
                'fromCountry' => $shipment->fromCountry,
                'fromState' => $shipment->fromState,
                'fromCity' => $shipment->fromCity,
                'toCountry' => $shipment->toCountry,
                'toState' => $shipment->toState,
                'toCity' => $shipment->toCity,
                'totalWeight' => !empty($shipmentDetails->deliveryChargeableWeight) ? $shipmentDetails->deliveryChargeableWeight : 0,
                'shippingId' => $request->shippingId,
                'totalProcurementCost' => $totalShipmentCost,
                'totalQuantity' => $request->totalQuantity,
                'inventoryCharge' => $shipmentDetails->inventoryCharge,
                'storageCharge' => $storageCharge,
                'otherChargeCost' => $shipmentDetails->otherChargeCost,
            );
            $shippingCharge = Shippingmethods::calculateShippingMethodCharges($param);
            $otherChargesDetails = Shipmentothercharges::where('shipmentId', $request->shipmentId)->where('deliveryId', $request->deliveryId)->get();

           
            if(count($otherChargesDetails)>0)
            {
              for($i=0; $i<count($otherChargesDetails); $i++)
              {
                if($otherChargesDetails[$i]->otherChargeId > 0)
                {
                    $otherChargesDetails[$i]['otherChargeName'] = Othershipmentcharges::where('id', $otherChargesDetails[$i]['otherChargeId'])->pluck('name')->first();
                }

                $otherDetailHtml[$i] = "<p>Charge Name:".$otherChargesDetails[$i]['otherChargeName']."<br>Charge Amount:".customhelper::getCurrencySymbolFormat($otherChargesDetails[$i]['otherChargeAmount'])."<br>";
                if(!empty($otherChargesDetails[$i]['notes']))
                    $otherDetailHtml[$i].="Notes:".$otherChargesDetails[$i]['notes']."</p>";
                else
                    $otherDetailHtml[$i].="</p>";
              }
            }else{
                $otherDetailHtml = array();
            }

            if (!empty($shippingCharge)) {
                $data = array();
                foreach ($shippingCharge[0] as $key => $value) {
                    if ($key == 'shippingCost' || $key == 'clearing' || $key == 'duty' || $key == 'totalShippingCost' || $key == 'clearingDuty' || $key == 'total')
                        $data[$key] = customhelper::getCurrencySymbolFormat($value);
                    else
                        $data[$key] = $value;
                }
            }

            $data['otherCharge'] = implode("\n", $otherDetailHtml);
            
            if (!empty($shippingCharge)) {

                return response()->json([
                    'status' => '200',
                    'data' => $data,
                    'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }
    }

    public function adddeliveryothercharges_mobile(Request $request) {

        if (!empty($request->shipmentId) && !empty($request->deliveryId) && !empty($request->email) && !empty($request->type)) {

            if($request->type == 'snapshot')
            {
                $deliverySnapShot = \App\Model\Generalsettings::where('settingsKey', 'delivery_snap_shot')->first();

                $otherCharge = !empty($deliverySnapShot->settingsValue) ? $deliverySnapShot->settingsValue : 0;
            }
            else if($request->type == 'recount')
            {
                $recountCost = \App\Model\Generalsettings::where('settingsKey', 'recount_cost')->first();

                $otherCharge = !empty($recountCost->settingsValue) ? $recountCost->settingsValue : 0;
            }
            else
            {
                $reweightCost = \App\Model\Generalsettings::where('settingsKey', 'reweight_cost')->first();

                $otherCharge = !empty($reweightCost->settingsValue) ? $reweightCost->settingsValue : 0;
            }

            if ($request->type == 'snapshot')
                $otherChargeName = "Delivery Snap Shot Charge";
            else if ($request->type == 'recount')
                $otherChargeName = "Delivery Recount Charge";
            else
                $otherChargeName = "Delivery Reweigh Charge";

            /* Update Shipment Other Charges */
            $shipmentOtherCharge = new Shipmentothercharges;
            $shipmentOtherCharge->shipmentId = $request->shipmentId;
            $shipmentOtherCharge->deliveryId = $request->deliveryId;
            $shipmentOtherCharge->otherChargeName = $otherChargeName;
            $shipmentOtherCharge->otherChargeAmount = $otherCharge;
            $shipmentOtherCharge->createdBy = $request->email;
            $shipmentOtherCharge->createdOn = Config::get('constants.CURRENTDATE');
            $shipmentOtherCharge->save();

            /* Update Shipment Delivery */
            $shipmentDelivery = Shipmentdelivery::find($request->deliveryId);
            $shipmentDelivery->otherChargeCost = $shipmentDelivery->otherChargeCost + $otherCharge;
            $shipmentDelivery->totalCost = $shipmentDelivery->totalCost + $otherCharge;

            if ($request->type == 'snapshot') {
                $shipmentDelivery->snapshot = 'Y';
                $shipmentDelivery->snapshotRequestedOn = Config::get('constants.CURRENTDATE');
            } else if ($request->type == 'recount') {
                $shipmentDelivery->recount = 'Y';
                $shipmentDelivery->recountRequestedOn = Config::get('constants.CURRENTDATE');
            } else {
                $shipmentDelivery->reweigh = 'Y';
                $shipmentDelivery->reweighRequestedOn = Config::get('constants.CURRENTDATE');
            }

            $shipmentDelivery->save();

            if ($request->type == 'snapshot') {
                /* Update Shipment Pacakges */
                Shipmentpackage::where('deliveryId', $request->deliveryId)
                        ->where('deleted', '0')
                        ->update(['snapshotOpt' => 'Y']);
            }

            /* Update Shipment Table */
            Shipment::find($request->shipmentId)->increment('totalOtherCharges', $otherCharge);

            return response()->json([
                'status' => '200',
                'data' => (object)['otherChargeAmount' => customhelper::getCurrencySymbolFormat($shipmentDelivery->otherChargeCost),'totalCost' => customhelper::getCurrencySymbolFormat($shipmentDelivery->totalCost)],
                'message' => 'success',
            ], 200);

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);
        }
    }

    public function getshippimgmethod_mobile(Request $request) {

        if (!empty($request->userId) && !empty($request->data)) {

            $shippingMethod = $shippingMethodData = array();
            $totalChargeableWeight = $totalQuantity = $totalShipmentChargeableWeight = $totalShipmentInventoryCharge = $totalShipmentOtherCharge = 0;
            $totalShipmentItemCost = $totalDeliveryCost = $totalShipmentStorageCharge = $totalDeliveryShipping = $totalClearingDuty = $shippingCost = $totalInsurance = 0;
            $isShipmentSplit = 0;
            $shipmentMethodListForSplit = array();
            $userId = $request->userId;
            $data = $request->data;

            $shipmentId = $data['id'];
            $shipmentDeliveryList = $data['delivery']['deliveries'];
            $isDutyCharged = 0;
            //print_r($shipmentDeliveryList);exit;
            if (!empty($shipmentDeliveryList)) {
                
                foreach ($shipmentDeliveryList as $delivery) {
                    if(!empty($delivery['shippingMethodId']) && $delivery['deliveryAllItemReturned'] == "0")
                    {
                        $deliveryStorageCharge = 0;
                        $totalItemCost = customhelper::getExtractCurrency($delivery['totalItemCost']);
                        $otherChargeCost = customhelper::getExtractCurrency($delivery['otherChargeCost']);
                        if($delivery['deliveryAllItemReturned'] == "0")
                        {
                            $totalShipmentChargeableWeight += $delivery['chargeableWeight'];
                        }else{
                            $totalChargeableWeight -= $delivery['chargeableWeight'];
                        }
                        $totalShipmentInventoryCharge += customhelper::getExtractCurrency($delivery['inventoryCharge']);
                        $totalShipmentOtherCharge += $otherChargeCost;
                        $totalShipmentItemCost += customhelper::getExtractCurrency($delivery['totalItemCost']);
                        /* Storage charge calculation */
                        $maxStorageDate = Carbon::parse($delivery['maxStorageDate']);
                        $now = Carbon::now();
                        $diff = $maxStorageDate->diffInDays($now, FALSE);
                        if ($diff > 0 && !empty($delivery['storageCharge'])) {
                            $totalShipmentStorageCharge += $diff * customhelper::getExtractCurrency($delivery['storageCharge']);
                            $deliveryStorageCharge = $diff * customhelper::getExtractCurrency($delivery['storageCharge']);
                        }
                        $deliveryCost = customhelper::getExtractCurrency($delivery['totalDeliveryCost']);
                        $totalDeliveryCost += $deliveryCost + $deliveryStorageCharge;

                        if (!empty($delivery['isDutyCharged']))
                            $isDutyCharged = 1;

                        if (!array_key_exists($delivery['shippingMethodId'], $shippingMethod)) {
                            if($delivery['deliveryAllItemReturned'] == "0")
                            {
                                $totalChargeableWeight = $delivery['chargeableWeight'];
                            }
                            $totalQuantity = $delivery['totalQty'];
                            $totalShipmentCost = $totalItemCost + $otherChargeCost;
                            $deliveryId = $delivery['id'];

                            $shippingMethod[$delivery['shippingMethodId']] = array(
                                'totalWeight' => $totalChargeableWeight,
                                'totalQuantity' => $totalQuantity,
                                'totalShipmentCost' => $totalShipmentCost,
                                'deliveryId' => $delivery['id'],
                            );
                        } else {
                            if($delivery['deliveryAllItemReturned'] == "0")
                            {
                                $totalChargeableWeight += $delivery['chargeableWeight'];
                            } else {
                                $totalChargeableWeight -= $delivery['chargeableWeight'];
                            }
                            $totalQuantity = $totalQuantity + $delivery['totalQty'];
                            $totalShipmentCost += ($totalItemCost + $otherChargeCost);
                            $deliveryId = $deliveryId . "," . $delivery['id'];

                            $shippingMethod[$delivery['shippingMethodId']] = array(
                                'totalWeight' => $totalChargeableWeight,
                                'totalQuantity' => $totalQuantity,
                                'totalShipmentCost' => $totalShipmentCost,
                                'deliveryId' => $deliveryId,
                            );
                        }

                        if(!empty($shipmentMethodListForSplit) && !in_array($delivery['shippingMethodId'],$shipmentMethodListForSplit))
                        {
                            $isShipmentSplit = 1;
                        }
                        $shipmentMethodListForSplit[] = $delivery['shippingMethodId'];
                    }
                }
                
                $otherChargesDetails = Shipmentothercharges::where('shipmentId', $shipmentId)->get();

                if (!empty($shippingMethod)) {
                    foreach ($shippingMethod as $key => $row) {
                        $param = array(
                            'userId' => $userId,
                            'fromCountry' => $data['fromCountry'],
                            'fromState' => $data['fromState'],
                            'fromCity' => $data['fromCity'],
                            'toCountry' => $data['toCountry'],
                            'toState' => $data['toState'],
                            'toCity' => $data['toCity'],
                            'totalWeight' => !empty($row['totalWeight']) ? $row['totalWeight'] : 0,
                            'totalProcurementCost' => $row['totalShipmentCost'],
                            'totalQuantity' => $row['totalQuantity'],
                            'shippingId' => $key,
                        );
                        $shippingMethodCharge = Shippingmethods::calculateShippingMethodCharges($param);
                        if (!empty($shippingMethodCharge)) {
                            $shippingCost += $shippingMethodCharge[0]['shippingCost'];
                            $totalDeliveryShipping += $shippingMethodCharge[0]['totalShippingCost'];
                            $totalClearingDuty += $shippingMethodCharge[0]['clearingDuty'];
                            $totalInsurance += $shippingMethodCharge[0]['insurance'];

                            foreach ($shippingMethodCharge[0] as $field => $value) {
                                $shippingMethodData[$key]['showShippingCharges'] = $data['id'];
                                if ($field == 'companyLogo') {
                                    $shippingMethodData[$key][$field] = asset('uploads/shipping/' . $value);
                                } else if ($field == 'shippingCost' || $field == 'duty' || $field == 'clearing' || $field == 'clearingDuty' || $field == 'totalShippingCost') {
                                    $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat($value);
                                } else if ($field == 'days') {
                                    $shippingMethodData[$key][$field] = $value;
                                    $shippingMethodData[$key]['estimatedDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($value)->format('D, M dS Y');
                                } else {
                                    $shippingMethodData[$key][$field] = $value;
                                }
                            }
                        }
                    }
                }

                if($isShipmentSplit == 1)
                {
                    $splitShipmentCost = \App\Model\Generalsettings::where('settingsKey', 'split_shipment_cost')->first();
                    $splitShipmentCost = !empty($splitShipmentCost->settingsValue) ? $splitShipmentCost->settingsValue : 0;
                    $totalShipmentOtherCharge += $splitShipmentCost;
                }
                //////////////////////////////////////////
                if(count($otherChargesDetails)>0)
                {
                  for($i=0; $i<count($otherChargesDetails); $i++)
                  {
                    if($otherChargesDetails[$i]->otherChargeId > 0)
                    {
                        $otherChargesDetails[$i]['otherChargeName'] = Othershipmentcharges::where('id', $otherChargesDetails[$i]['otherChargeId'])->pluck('name')->first();
                    }

                    $otherDetailHtml[$i] = "<p>Charge Name:".$otherChargesDetails[$i]['otherChargeName']."<br>Charge Amount:".customhelper::getCurrencySymbolFormat($otherChargesDetails[$i]['otherChargeAmount'])."<br>";

                    if($isShipmentSplit == 1)
                    {
                         $splitShipmentCost = \App\Model\Generalsettings::where('settingsKey', 'split_shipment_cost')->first();
                        $splitShipmentCost = !empty($splitShipmentCost->settingsValue) ? $splitShipmentCost->settingsValue : 0;

                       $otherDetailHtml[$i].= "Split Shipment Cost:" .customhelper::getCurrencySymbolFormat($splitShipmentCost);
                    }
                    if(!empty($otherChargesDetails[$i]['notes']))
                        $otherDetailHtml[$i].="Notes:".$otherChargesDetails[$i]['notes']."</p>";
                    else
                        $otherDetailHtml[$i].="</p>";
                  }
                }else{
                    $otherDetailHtml = array();
                }
                ////////////////////////////////////////////////

                $totalShipmentCost = $totalDeliveryShipping + $totalShipmentInventoryCharge + $totalShipmentOtherCharge + $totalShipmentStorageCharge;

                $savingCost = ($totalDeliveryCost - $totalShipmentCost);
                $savingCostPercentage = round(($savingCost / $totalDeliveryCost) * 100, 2);

                $shipment = array(
                    'totalWeight' => $totalShipmentChargeableWeight,
                    'inventoryCharge' => customhelper::getCurrencySymbolFormat($totalShipmentInventoryCharge),
                    'otherCharge' => customhelper::getCurrencySymbolFormat($totalShipmentOtherCharge),
                    'totalItemCost' => customhelper::getCurrencySymbolFormat($totalShipmentItemCost),
                    'storageCharge' => customhelper::getCurrencySymbolFormat($totalShipmentStorageCharge),
                    'shippingCost' => customhelper::getCurrencySymbolFormat($shippingCost),
                    'totalShippingCost' => customhelper::getCurrencySymbolFormat($totalDeliveryShipping),
                    'totalDeliveryCost' => customhelper::getCurrencySymbolFormat($totalDeliveryCost),
                    'totalClearingDuty' => customhelper::getCurrencySymbolFormat($totalClearingDuty),
                    'isDutyCharged' => $isDutyCharged,
                    'totalShipmentCost' => customhelper::getCurrencySymbolFormat($totalShipmentCost),
                    'savingCost' => customhelper::getCurrencySymbolFormat($savingCost),
                    'totalInsurance' => $totalInsurance,
                    'savingCostPercentage' => $savingCostPercentage,
                    'otherChargeComment' => $otherDetailHtml
                );

                return response()->json([
                    'status' => '200',
                    'data' => (object)['shippingcharges' => $shippingMethodData,'shipment' => $shipment],
                    'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
                ], 200);

            }

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }
    }

    public function warehousecheckout_mobile(Request $request) {

        if (!empty($request->id) && !empty($request->userId) && !empty($request->delivery) && !empty($request->shipment) && !empty($request->userUnit) && !empty($request->customerName) && !empty($request->customerEmail) && !empty($request->warehouseId) && !empty($request->shipmentType) && !empty($request->fromCountry) && !empty($request->fromCountryName) && !empty($request->fromState) && !empty($request->fromStateName) && !empty($request->fromCity) && !empty($request->fromCityName) /*&& !empty($request->fromZipCode)*/ /*&& !empty($request->fromCompany)*/ && !empty($request->fromAddress) && !empty($request->fromPhone) && !empty($request->fromEmail) && !empty($request->toCountry) && !empty($request->toState) && !empty($request->toStateName) && !empty($request->toCity) && !empty($request->toCityName) /*&& !empty($request->toZipCode)*/ && !empty($request->toAddress) && !empty($request->toName)/* && !empty($request->toCompany)*/ && !empty($request->toEmail) && !empty($request->toPhone) /*&& !empty($request->shipmentStatus)*/ && !empty($request->firstReceived) && !empty($request->maxStorageDate) /*&& !empty($request->totalItemCost) *//*&& !empty($request->totalTax) && !empty($request->totalDiscount)*/) {

            $shipment = array(
                'id' => $request->id,
                'userId' => $request->userId,
                'userUnit' => $request->userUnit,
                'customerName' => $request->customerName,
                'customerEmail' => $request->customerEmail,
                'warehouseId' => $request->warehouseId,
                'shipmentType' => $request->shipmentType,
                'fromCountry' => $request->fromCountry,
                'fromCountryName' => $request->fromCountryName,
                'fromState' => $request->fromState,
                'fromStateName' => $request->fromStateName,
                'fromCity' => $request->fromCity,
                'fromCityName' => $request->fromCityName,
                'fromZipCode' => $request->fromZipCode,
                'fromCompany' => $request->fromCompany,
                'fromAddress' => $request->fromAddress,
                'fromPhone' => $request->fromPhone,
                'fromEmail' => $request->fromEmail,
                'toCountry' => $request->toCountry,
                'toState' => $request->toState,
                'toStateName' => $request->toStateName,
                'toCity' => $request->toCity,
                'toCityName' => $request->toCityName,
                'toZipCode' => $request->toZipCode,
                'toAddress' => $request->toAddress,
                'toName' => $request->toName,
                'toCompany' => $request->toCompany,
                'toEmail' => $request->toEmail,
                'toPhone' => $request->toPhone,
                'shipmentStatus' => $request->shipmentStatus,
                'firstReceived' => $request->firstReceived,
                //'shipmentStatus' => $request->shipmentStatus,
                'maxStorageDate' => $request->maxStorageDate,
                'totalItemCost' => customhelper::getExtractCurrency($request->totalItemCost),
                'totalTax' => customhelper::getExtractCurrency($request->totalTax),
                'totalDiscount' => customhelper::getExtractCurrency($request->totalDiscount),
                'isInsuranceCharged' => 'Y',
                'exchangeRate' => 1,
            );

            if (!empty($request->shipment)) {
                $shipment['inventoryCharge'] = customhelper::getExtractCurrency($request->shipment['inventoryCharge']);
                $shipment['totalInsurance'] = $request->shipment['totalInsurance'];
                $shipment['storageCharge'] = customhelper::getExtractCurrency($request->shipment['storageCharge']);
                $shipment['totalWeight'] = $request->shipment['totalWeight'];
                $shipment['shippingCost'] = customhelper::getExtractCurrency($request->shipment['shippingCost']);
                $shipment['totalShippingCost'] = customhelper::getExtractCurrency($request->shipment['totalShippingCost']);
                $shipment['totalClearingDuty'] = customhelper::getExtractCurrency($request->shipment['totalClearingDuty']);
                $shipment['totalOtherCharges'] = customhelper::getExtractCurrency($request->shipment['otherCharge']);
                $shipment['isDutyCharged'] = $request->shipment['isDutyCharged'];
                $shipment['totalCost'] = customhelper::getExtractCurrency($request->shipment['totalShipmentCost']) + $shipment['totalInsurance'];
            }

            $shipment['defaultCurrencySymbol'] = $shipment['currencySymbol'] = customhelper::getCurrencySymbolCode();
            $shipment['isCurrencyChanged'] = 'N';
            $shipment['defaultCurrencyCode'] = $shipment['currencyCode'] = customhelper::getCurrencySymbolCode('', true);

            if (!empty($request->delivery['deliveries'])) {
                foreach ($request->delivery['deliveries'] as $count => $delivery) {
                    foreach ($delivery as $key => $value) {
                        if ($key == 'packages') {
                            foreach ($delivery['packages'] as $count1 => $row) {
                                foreach ($row as $packageKey => $packageVal) {
                                    if ($packageKey == 'itemPrice' || $packageKey == 'itemShippingCost' || $packageKey == 'itemTotalCost')
                                        $shipment['delivery']['deliveries'][$count][$key][$count1][$packageKey] = customhelper::getExtractCurrency($packageVal);
                                    else
                                        $shipment['delivery']['deliveries'][$count][$key][$count1][$packageKey] = $packageVal;
                                }
                            }
                        } else {
                            $shipment['delivery']['deliveries'][$count][$key] = customhelper::getExtractCurrency($value);
                        }
                    }
                }
            }

            if(!empty($shipment)){

                return response()->json([
                    'status' => '200',
                    'data' => (object)['shipment' => $shipment],
                    'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }


        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }
    }

    public function updateuserwarehouse_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->toCurrency) || empty($request->fromCurrency)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId  = $request->userId;
            $shipment = array();
            $fromCurrency = $request->fromCurrency;
            $toCurrency = $request->toCurrency;

            $deafultCurrency = customhelper::getCurrencySymbolCode('', true);

            $shipment['defaultCurrencySymbol'] = customhelper::getCurrencySymbolCode();
            $shipment['defaultCurrencyCode'] = $deafultCurrency;
            $shipment['isCurrencyChanged'] = ($request->toCurrency != $deafultCurrency) ? 'Y' : 'N';
            $shipment['currencySymbol'] = customhelper::getCurrencySymbolCode($request->toCurrency);
            $shipment['currencyCode'] = $request->toCurrency;
            $shipment['exchangeRate'] = $exchangeRate = \App\Model\Currency::currencyExchangeRate($deafultCurrency, $request->toCurrency);

            if(!empty($shipment)){ 

                return response()->json([
                'status' => '200',
                'data' => (object)['shipment' => $shipment],
                'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);

            }

        }

    }

    /**
     * Method used to validate coupon code
     * @param request $request
     * @return string
     */
    public static function validatecouponcode_mobile(Request $request) {

        if(empty($request->userId) || empty($request->coupon)){
            
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {
        
            /* INITALIZE THE VARIABLE AS FALSE */
            $match = false;

            /* GET THE COUPON CODE */
            $code = $request->coupon;

            /* FETCH USER DETAILS */
            $userId = $request->userId;
            $userDetails = \App\Model\User::where("id", $userId)->first();

            /* VALIDATE THE COUPON CODE */
            $currDate = date('m/d/Y');

            /* GET THE LOG */
            $couponLog = \App\Model\Couponlog::where('userId', $userId)->where('code', $code)->count();
            if ($couponLog == 1) {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Coupon already used!',
                ], 200);

            }

            /* GET THE OFFER ID */
            $offerIdQuery = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->get();

            if (count($offerIdQuery) == 0) {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Invalid coupon!',
                ], 200);

            }

            $offerCondQuery = \App\Model\Offercondition::where('offerId', $offerIdQuery[0]->id)->where('keyword', 'new_customer_reg_from_app')->count();

            if ($offerCondQuery == 0) {
                /* QUERY FOR NOT REGISTERED BY APP */
                $where = "'$currDate' between offerRangeDateFrom and offerRangeDateTo";
                $ifCodeExist = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->whereRaw($where)->get();
            } else {
                /* QUERY FOR REGISTERED BY APP */
                $ifCodeExist = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->get();
            }

            if (empty($ifCodeExist)) {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Invalid coupon!',
                ], 200);

            } else {
                /* COUPON CODE IS VALID, DO SEARCH TO CHECK CONDITIONS */
                $offerId = $ifCodeExist[0]->id;

                /* IF REGISTERED BY APP */
                if ($offerCondQuery > 0) {
                    $createdOn = explode(" ", $userDetails->createdOn);
                    $date1 = date_create($createdOn[0]);
                    $date2 = date_create(date('Y-m-d'));
                    $diff = date_diff($date1, $date2);
                    $exactDay = $diff->format("%d");

                    $getOffer = \App\Model\Offer::where('id', $offerIdQuery[0]->id)->first();
                    if ($exactDay > $getOffer->offerRangeDateFrom) {
                        
                        return response()->json([
                            'status' => '400',
                            'data' => null,
                            'message' => 'Invalid coupon!',
                        ], 200);
                        exit;
                    } else {
                        //Nothing
                    }
                }

                /* GET THE CONDITIONS */
                $offerConditions = \App\Model\Offercondition::where('offerId', $offerId)->get();

                /* GET THE CUSTOMER ADDRESS DETAILS */
                $userAddress = \App\Model\Addressbook::where("userId", $userId)->where('deleted', '0')->get();

                /* GET THE CART ITEMS ARRAY */
                foreach ($offerConditions as $keyO => $valO) {

                    /* CONDITION 3 */
                    if ($valO->keyword == "discount_on_total_amount_of_shipping") {

                        $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                                ->where('keyword', 'discount_on_total_amount_of_shipping')
                                ->get();
                        $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                        $decodedValues = json_decode($OfferCond);

                        /* GET THE SHIPPING AMOUNT */
                        $shippingAmount = $totalWeight = $request->usercart['shippingCost'];

                        if ($shippingAmount == null) {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }


                        if ($shippingAmount >= $decodedValues->shipping_cost->from && $shippingAmount <= $decodedValues->shipping_cost->to) {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        } else {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }
                    }

                    /* CONDITION 4 */
                    if ($valO->keyword == "customer_has_not_purchased_any_goods_for_a_certain_timeframe") {

                        $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                                ->where('keyword', 'customer_has_not_purchased_any_goods_for_a_certain_timeframe')
                                ->get();
                        $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                        $decodedValues = json_decode($OfferCond);

                        /* GET THE DATES FROM JSON DECODE */
                        $from = date_format(date_create($decodedValues->user_not_purchased->from), "Y-m-d");
                        $to = date_format(date_create($decodedValues->user_not_purchased->to), "Y-m-d");

                        /* GET THE LAST DATE OF THE PURCHASE */
                        $lastOrder = \App\Model\Order::where('userId', $userId)->orderby('id', 'desc')->first();
                        if (!empty($lastOrder)) {
                            $notPurchasedFrom = data('Y-m-d', strtotime($lastOrder->createdDate));

                            /* CALCULATE PART */
                            if (( $notPurchasedFrom >= $from ) && ( $notPurchasedFrom <= $to )) {
                                /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                                $match = false;
                                break;
                            } else {
                                /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                                $match = true;
                            }
                        }
                    }

                    /* CONDITION 5 */
                    if ($valO->keyword == "customer_purchased_an_item_from_particular_store") {

                        /* GET THE STORE IDS FROM CART */
                        /* foreach ($request->usercart['items'] as $keycart => $valCart) {
                          $storeId[] = "'[" . '"' . $valCart['storeId'] . '"' . "]'";
                          } */

                        foreach ($request->usercart['delivery']['deliveries'] as $value) {
                            foreach ($value['packages'] as $keyname => $valCart) {
                                $storeId[] = "'[" . '"' . $valCart['storeId'] . '"' . "]'";
                            }
                        }

                        $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_purchased_an_item_from_particular_store');

                        $query->where(function($q) use ($storeId) {
                            foreach ($storeId as $keyQ => $condition) {
                                if ($keyQ == 0) {
                                    $q->whereRaw('json_contains(jsonValues, ' . $condition . ', \'$.store_id\')');
                                } else {
                                    $q->orWhereRaw('json_contains(jsonValues, ' . $condition . ', \'$.store_id\')');
                                }
                            }
                        });

                        if (count($query->get()) > 0) {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        } else {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }
                    }


                    /* CONDITION 6 */
                    if ($valO->keyword == "discount_on_total_weight_of_shipping") {

                        /* MATCH THE WEIGHT WHICH MAY FALL UNDER THE RANGE */
                        $totalWeight = $request->usercart['totalWeight'];

                        $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                                ->where('keyword', 'discount_on_total_weight_of_shipping')
                                ->get();
                        $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                        $decodedValues = json_decode($OfferCond);


                        if ($totalWeight >= $decodedValues->shipping_weight->from && $totalWeight <= $decodedValues->shipping_weight->to) {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        } else {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }
                    }

                    /* CONDITION 7 */
                    if ($valO->keyword == "discount_on_total_weight_of_shipping_for_any_specific_customer") {
                        $createdOn = date('Y-m-d', strtotime($userDetails->createdOn));

                        $date1 = date_create($createdOn);
                        $date2 = date_create(date('Y-m-d'));
                        $diff = date_diff($date1, $date2);

                        /* GET THE ACTUAL MONTH */
                        $exactMonth = $diff->format("%m");

                        /* GET THE MONTH DEFINED IN THE CONDITION */
                        $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                                ->where('keyword', 'discount_on_total_weight_of_shipping_for_any_specific_customer')
                                ->get();
                        $monthOffer = $offerConditionByKeyword[0]->jsonValues;
                        $decodedValues = json_decode($monthOffer);
                        $duration = $decodedValues->shipping_weight_for_specific_user->duration;

                        if ($exactMonth >= $duration) {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;

                            /* NOW MATCH THE WEIGHT WHICH MAY FALL UNDER THE RANGE */
                            $totalWeight = $request->usercart['totalWeight'];
                            if ($totalWeight >= $decodedValues->shipping_weight_for_specific_user->from && $totalWeight <= $decodedValues->shipping_weight_for_specific_user->to) {
                                /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                                $match = true;
                            } else {
                                /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                                $match = false;
                                break;
                            }
                        } else {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }
                    }

                    /* CONDITION 8 */
                    if ($valO->keyword == "customer_is_using_a_particular_shipping_method") {

                        /* GET THE SHIPPING METHOD ID */
                        #$id = $request->usercart['data']['shipmentMethodId'];

                        /* IF NO METHOD FOUND, EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        if (empty($request->usercart['delivery']['deliveries'])) {
                            $match = false;
                            break;
                        }

                        foreach ($request->usercart['delivery']['deliveries'] as $keyname => $value) {
                            $id = $value['shippingMethodId'];
                            $shippingMethodId = '["' . $id . '"]';

                            $query = \App\Model\Offercondition::where('offerId', $offerId)
                                    ->where('keyword', 'customer_is_using_a_particular_shipping_method')
                                    ->whereRaw('json_contains(jsonValues, ' . "'" . $shippingMethodId . "'" . ', \'$.shipping_method\')');
                            if ($query->count() == 1) {
                                $match = true;
                                break;
                            }
                        }
                    }

                    /* CONDITION 9 */
                    if ($valO->keyword == "new_customer_reg_from_app") {
                        if ($userDetails->registeredBy == 'app')
                            $appReg = '["Y"]';
                        else
                            $appReg = '["N"]';

                        $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'new_customer_reg_from_app')
                                ->whereRaw('json_contains(jsonValues, ' . "'" . $appReg . "'" . ', \'$.app_user_registration\')');
                        if ($query->count() == 0) {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        } else {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        }
                    }

                    /* CONDITION 1 */
                    if ($valO->keyword == "customer_get_discounts_and_special_offers_on_certain_product_or_categories") {

                        foreach ($request->usercart['delivery']['deliveries'] as $value) {
                            foreach ($value['packages'] as $keyname => $valCart) {
                                if ($valCart['siteProductId'] != null) {
                                    $pId[] = array('{"values":["' . $valCart['siteProductId'] . '"]}');
                                }
                                if ($valCart['siteCategoryId'] != null) {
                                    $pId[] = array('{"values":"' . $valCart['siteCategoryId'] . '"}');
                                }
                            }
                        }

                        /* IF ARRAY IS BLANK THEN CONDITION IS NOT SATISFIED */
                        if (count($pId) == 0) {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }

                        $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_get_discounts_and_special_offers_on_certain_product_or_categories');
                        $query->where(function($q) use ($pId) {
                            foreach ($pId as $keyQ => $condition) {
                                if ($keyQ == 0) {
                                    $q->whereRaw('json_contains(jsonValues, ' . "'" . $condition[0] . "'" . ')');
                                } else {
                                    $q->orWhereRaw('json_contains(jsonValues, ' . "'" . $condition[0] . "'" . ')');
                                }
                            }
                        });

                        if (count($query->get()) > 0) {
                            $match = true;
                        } else {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }
                    }

                    /* CONDITION 2 */
                    if ($valO->keyword == "customer_of_a_specific_shipping_location") {

                        /* GET LOCATION DETAILS */
                        $countryId = $request->usercart['personaldetails']['shippingCountryId'];
                        $stateId = $request->usercart['personaldetails']['shippingStateId'];
                        $cityId = $request->usercart['personaldetails']['shippingCityId'];

                        if ($stateId == null) {
                            $stringState = "N/A";
                        } else {
                            $stringState = $stateId;
                        }

                        if ($cityId == null) {
                            $stringCity = "N/A";
                        } else {
                            $stringCity = $cityId;
                        }

                        $makeArray = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"' . $stringCity . '"';

                        $makeArray2 = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"N/A"';

                        $makeArray3 = '"countrySelected":"' . $countryId . '","stateSelected":"N/A","citySelected":"N/A"';



                        $condition2 = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_of_a_specific_shipping_location')
                                ->whereRaw('json_contains(jsonValues, \'{' . $makeArray . '}\')')
                                ->orWhereRaw('json_contains(jsonValues, \'{' . $makeArray2 . '}\')')
                                ->orWhereRaw('json_contains(jsonValues, \'{' . $makeArray3 . '}\')')
                                ->get();

                        if (count($condition2) > 0) {
                            $match = true;
                        } else {
                            $match = false;
                        }
                    }
                }
                if ($match == false) {
                    
                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'Invalid coupon!',
                    ], 200);

                } else {

                    /* CALCULATE LOGIC */
                    $bonus = unserialize($offerIdQuery[0]->bonusValuesSerialized);
                    $countBonusArray = count($bonus);

                    /* GET THE TOTAL SHIPPING DISCOUNT FROM LOCAL STORAGE */

                    $exchangeRate = isset($request->usercart['exchangeRate']) ? $request->usercart['exchangeRate'] : 1;
                    $totalShippingCost = $request->usercart['totalShippingCost'] * $exchangeRate;


                    /* GET THE AMOUNT */
                    if ($countBonusArray == 4) {
                        $AmountOrPoint = "Amount";
                        $pointToBeDiscounted = 0.00;
                        $type = $bonus["type"];
                        if ($type == 'Absolute') {
                            if ($bonus["discount"] > $totalShippingCost) {
                                $amountToBeDiscounted = $totalShippingCost;
                            } else {
                                $amountToBeDiscounted = $bonus["discount"];
                            }
                        } else {
                            $discountNotExceed = $bonus["discountNotExceed"];
                            $getAmount = (($totalShippingCost * $bonus["discount"]) / 100);
                            if ($getAmount > $discountNotExceed) {
                                $amountToBeDiscounted = $discountNotExceed;
                            } else {
                                $amountToBeDiscounted = number_format($getAmount, 2);
                            }
                        }
                    } else {
                        /* GET THE POINTS */
                        $AmountOrPoint = "Point";
                        $amountToBeDiscounted = 0.00;
                        if ($bonus['bonusPoint'] == 1) {
                            $pointToBeDiscounted = round($bonus["fixedAmount"]);
                        } else {
                            /* HANDLE DIVISION BY ZERO */
                            if ($bonus["bonusPer"] > 0) {
                                $getPer = ($totalShippingCost / $bonus["bonusPer"]);
                                $pointToBeDiscounted = round($getPer * $bonus["amountPer"]);
                            } else {
                                $pointToBeDiscounted = 0;
                            }
                        }
                    }

                    return response()->json([
                        'status' => '200',
                        'data' => (object)['amount_or_point' => $AmountOrPoint,
                                'amount_to_be_discounted' => $amountToBeDiscounted,
                                'point_to_be_discounted' => $pointToBeDiscounted],
                        'message' => 'Coupon applied',
                    ], 200);

                }
            }
        }
    }

    /**
     * Method used to submit order details
     * 
     * @return string
     */
    public function submitorder_mobile(Request $request) {
        
        if(empty($request->id) || empty($request->userId) || empty($request->warehouseId) /*|| empty($request->totalItemCost)*/ || empty($request->isInsuranceCharged) /*|| empty($request->totalTax)*/ || empty($request->shippingCost) || empty($request->totalClearingDuty) || empty($request->isDutyCharged) /*|| empty($request->totalInsurance) || empty($request->totalOtherCharges) || empty($request->inventoryCharge)*/ || empty($request->delivery) /*|| empty($request->storageCharge)*/ || empty($request->paymentMethod) || empty($request->personaldetails)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId = $request->userId;
            $paymentStatus = 'unpaid';
            $transactionId = $couponCode = $discountAmount = $discountPoint = $discountType = '';
            $transactionData = $transactionErrorMsg = array();
            $couponData = array();
            $paymentMode = 'offline';
            $paymentErrorMessage = '';

            /* Fetch User Details */
            $userData = User::find($userId);
            $totalInsurance = 0;

            /* Fetch Warehouse Details */
            $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

            $shipmentId = $request->id;
            $totalItemCost = $request->totalItemCost;
            $isInsuranceCharged = $request->isInsuranceCharged;
            $totalTax = isset($request->totalTax) ? $request->totalTax : 0;
            $shippingCost = $request->shippingCost;
            $totalClearingDuty = $request->totalClearingDuty;
            $isDutyCharged = $request->isDutyCharged;
            $totalInsurance = $request->totalInsurance;
            $totalOtherCharges = $request->totalOtherCharges;
            $totalCost = $request->totalCost + $totalTax;
            $inventoryCharge = $request->inventoryCharge;

            $couponCode = '';
            if (isset($request->coupondetails)) {
                $totalBDiscountCost = $request->totalBDiscountCost;
                $couponCode = $request->coupondetails['couponCode'];
                $discountAmount = $request->coupondetails['discountAmount'];
                $discountPoint = $request->coupondetails['discountPoint'];
                $discountType = $request->coupondetails['discountType'];
                $offerData = \App\Model\Offer::select('id')->where('couponCode', $couponCode)->first();
                $couponId = $offerData['id'];
            }

            /*  SET DATA FOR OFFLINE GATEWAYS */
            if ($request->paymentMethod['paymentMethodKey'] == 'bank_online_transfer' || $request->paymentMethod['paymentMethodKey'] == 'bank_pay_at_bank' || $request->paymentMethod['paymentMethodKey'] == 'bank_account_pay' || $request->paymentMethod['paymentMethodKey'] == 'check_cash_payment')
                $paymentStatus = 'paid';

            /*  SET DATA FOR WIRE TRANSFER PAYMENT */
            if ($request->paymentMethod['paymentMethodKey'] == 'wire_transfer') {
                $paymentStatus = 'paid';

                $trasactionData = json_encode(array(
                    'poNumber' => $request->paymentdetails['poNumber'],
                    'companyName' => $request->paymentdetails['companyName'],
                    'buyerName' => $request->paymentdetails['buyerName'],
                    'position' => $request->paymentdetails['position'],
                        )
                );
            }

            /*  PROCESS EWALLET PAYMENT */
            if ($request->paymentMethod['paymentMethodKey'] == 'ewallet') {
                $eawalletid = (int) $request->paymentdetails['id'];
                $userEwallet = \App\Model\Ewallet::find($eawalletid)->decrement('amount', $totalCost);
                $ewalletTransaction = new \App\Model\Ewallettransaction;
                $ewalletTransaction->userId = $userId;
                $ewalletTransaction->ewalletId = $eawalletid;
                $ewalletTransaction->amount = $totalCost;
                $ewalletTransaction->transactionType = 'debit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');

                if ($ewalletTransaction->save())
                    $paymentStatus = 'paid';
                else
                    $paymentStatus = 'failed';

                $paymentMode = 'online';

                $transactionData = json_encode(array(
                    'ewalletId' => $request->paymentdetails['ewalletId'],
                        )
                );
            }
            /* PROCESS PAYSTACK DATA */
            if ($request->paymentMethod['paymentMethodKey'] == 'paystack_checkout') {
                if (!empty($request->paystackData)) {
                    $checkoutReturn = Paymenttransaction::paystack($request->paystackCreatedReference);
                    if ($checkoutReturn) {
                        $paymentMode = 'online';
                        $paymentStatus = 'paid';
                        $transactionId = $request->paystackData['trans'];
                        $transactionData = json_encode($request->paystackData);
                    } else {
                        $paymentStatus = 'failed';
                    }
                }
            }

            /*  PROCESS CREDIT CARD PAYMENT */
            if ($request->paymentMethod['paymentMethodKey'] == 'credit_debit_card') {
                $paymentMethod = \App\Model\Paymentmethod::where('paymentMethodKey', $request->paymentMethod['paymentMethodKey'])->first();
                if ($paymentMethod->paymentGatewayId == 1) {
                    $checkoutData = array();
                    $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                    $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                    $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                    $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                    $checkoutData['customerFirstName'] = $userData->firstName;
                    $checkoutData['customerLastName'] = $userData->lastName;
                    $checkoutData['customerAddress'] = $request->personaldetails['billingAddress'];
                    $checkoutData['customerCity'] = isset($request->personaldetails['billingCityName']) ? $request->personaldetails['billingCityName'] : "";
                    $checkoutData['customerState'] = isset($request->personaldetails['billingStateName']) ? $request->personaldetails['billingStateName'] : "";
                    $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                    $checkoutData['customerZip'] = $request->personaldetails['billingZipcode'];
                    $checkoutData['amount'] = round($totalCost, 2);
                    $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;

                    $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);

                    if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                        $paymentStatus = 'paid';
                        $paymentMode = 'online';
                        $transactionId = $checkoutReturn['TRANSACTIONID'];
                        $transactionData = json_encode($checkoutReturn);
                    } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($checkoutReturn);
                        $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    } else if ($checkoutReturn == 'error') {
                        $paymentStatus = 'failed';
                    }
                } else {
                    $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                    $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                    $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                    $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                    $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                    $checkoutData['customerFirstName'] = $request->personaldetails['billingFirstName'];
                    $checkoutData['customerLastName'] = $request->personaldetails['billingLastName'];
                    $checkoutData['customerAddress'] = $request->personaldetails['billingAddress'];
                    $checkoutData['customerCity'] = isset($request->personaldetails['billingCityName']) ? $request->personaldetails['billingCityName'] : "";
                    $checkoutData['customerState'] = isset($request->personaldetails['billingStateName']) ? $request->personaldetails['billingStateName'] : "";
                    $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                    $checkoutData['customerShippingAddress'] = $request->personaldetails['shippingAddress'];
                    $checkoutData['customerShippingCity'] = isset($request->personaldetails['shippingCityName']) ? $request->personaldetails['shippingCityName'] : "";
                    $checkoutData['customerShippingState'] = isset($request->personaldetails['shippingStateName']) ? $request->personaldetails['shippingStateName'] : "";
                    $checkoutData['customerShippingCountry'] = $request->personaldetails['shippingCountryName'];
                    $checkoutData['customerShippingZip'] = $request->personaldetails['shippingZipcode'];
                    $checkoutData['amount'] = round($totalCost, 2);
                    $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;
                    $checkoutData['shippingFirstName'] = $request->personaldetails['shippingFirstName'];
                    $checkoutData['shippingLastName'] = $request->personaldetails['shippingFirstName'];

                    $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);
                    if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                        $paymentStatus = 'paid';
                        $paymentMode = 'online';
                        $transactionId = $checkoutReturn['transactionResponse']['transId'];
                        $transactionData = json_encode($checkoutReturn['transactionResponse']);
                    } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                        $paymentStatus = 'failed';
                        if (!empty($checkoutReturn['transactionResponse'])) {
                            $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);
                            if (isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                                $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                            else
                                $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                        }else {
                            if (!empty($checkoutReturn['messages']['message'])) {
                                $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['messages']['message']['text'];
                            }
                        }
                    }
                }
            }

            if ($request->paymentMethod['paymentMethodKey'] == 'paypalstandard') {
                if (isset($request->paypalData)) {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $request->paypalData['orderID'];
                    $transactionData = json_encode($request->paypalData);
                } else {
                    $paymentStatus = 'error';
                }
            }
            if($request->paymentMethod['paymentMethodKey'] == 'payeezy') {
                if(!empty($request->payeezyData)) {
                    $checkoutData['amount'] = $request->payeezyData["paidAmount"];
                    $checkoutData['method'] = $request->payeezyData["paymentType"];
                    $checkoutData['currency_code'] = "USD";
                    $checkoutData['type'] = $request->payeezyData["cardType"];
                    $checkoutData['cardholder_name'] = $request->payeezyData["cardHolderName"];
                    $checkoutData['card_number'] = $request->payeezyData['ccardNumber'];
                    $checkoutData['exp_date'] = $request->payeezyData['expiryMonth'].substr($request->payeezyData['expiryYear'],2);
                    $checkoutData['cvv'] = $request->payeezyData['cvvCode'];
                    $checkoutReturn = Paymenttransaction::processpayeezy($checkoutData);
                
                    if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved')
                    {
                        $paymentStatus = 'paid';
                        $paymentMode = 'online';
                        $transactionId = $checkoutReturn['transaction_id'];
                        $transactionData = json_encode($checkoutReturn);
                    }
                    else if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed')
                    {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = 'Payment failed. ' .$checkoutReturn['Error']['messages'][0]['description'];
                    }
                    else
                    {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                    }
                
                } else {
                    $paymentStatus = 'error';
                }
            }

            if ($paymentStatus == 'paid') {
                /*  UPDATE SHIPMENT DATA */
                $shipment = Shipment::find($shipmentId);
                $shipment->toCountry = $request->personaldetails['shippingCountryId'];
                $shipment->toState = $request->personaldetails['shippingStateId'];
                $shipment->toCity = $request->personaldetails['shippingCityId'];
                $shipment->toName = $request->personaldetails['shippingFirstName'] . " " . $request->personaldetails['shippingLastName'];
                $shipment->toAddress = $request->personaldetails['shippingAddress'];
                $shipment->toZipCode = $request->personaldetails['shippingZipcode'];
                $shipment->toPhone = $request->personaldetails['shippingPhone'];
                $shipment->toEmail = $request->personaldetails['shippingEmail'];
                $shipment->totalShippingCost = $shippingCost;
                $shipment->totalClearingDuty = $totalClearingDuty;
                $shipment->isDutyCharged = $isDutyCharged;
                $shipment->totalInsurance = ($isInsuranceCharged == 'Y') ? $totalInsurance : 0;
                $shipment->totalTax = $totalTax;
                $shipment->totalOtherCharges = $totalOtherCharges;
                $shipment->totalDiscount = $request->totalDiscount;
                $shipment->couponcodeApplied = $couponCode;
                $shipment->totalCost = $totalCost;
                $shipment->storageCharge = $request->storageCharge;
                $shipment->storageChargeModifiedOn = Config::get('constants.CURRENTDATE');
                $shipment->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $shipment->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                $shipment->paymentReceivedOn = Config::get('constants.CURRENTDATE');
                $shipment->shippingMethod = 'Y';
                $shipment->wrongInventory = 'N';
                $shipment->isCurrencyChanged = $request->isCurrencyChanged;
                $shipment->defaultCurrencyCode = $request->defaultCurrencyCode;
                $shipment->paidCurrencyCode = ($request->isCurrencyChanged == 'Y') ? $request->currencyCode : $request->defaultCurrencyCode;
                $shipment->exchangeRate = $request->exchangeRate;
                $shipment->modifiedOn = Config::get('constants.CURRENTDATE');
                $shipment->save();

                /* Create order on successful payment */
                $orderObj = new \App\Model\Order;
                $orderObj->shipmentId = $shipment->id;
                $orderObj->userId = $shipment->userId;
                $orderObj->totalCost = $totalCost;
                $orderObj->status = '2';
                $orderObj->type = 'shipment';
                $orderObj->createdBy = $shipment->userId;
                $orderObj->save();
                $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
                $orderObj->orderNumber = $orderNumber;
                $orderObj->save();

                if (!empty($request->delivery)) {
                    $totalQuantity = $totalWeight = 0;

                    foreach ($request->delivery['deliveries'] as $key => $delivery) {
                        if($delivery['chargeableWeight']=="" || $delivery['chargeableWeight']=="0.00")
                            continue;
                        $totalQuantity += $delivery['totalQty'];

                        $shipmentDelivery = Shipmentdelivery::find($delivery['id']);
                        $shipmentDelivery->shippingCost = $delivery['shippingCost'];
                        $shipmentDelivery->clearingDutyCost = $delivery['clearingDutyCost'];
                        $shipmentDelivery->isDutyCharged = $delivery['isDutyCharged'];
                        $shipmentDelivery->inventoryCharge = $delivery['inventoryCharge'];
                        $shipmentDelivery->otherChargeCost = $delivery['otherChargeCost'];
                        $shipmentDelivery->totalCost = $delivery['totalDeliveryCost'];
                        $shipmentDelivery->snapshot = $delivery['snapshot'];
                        $shipmentDelivery->snapshotRequestedOn = $delivery['snapshotRequestedOn'];
                        $shipmentDelivery->recount = $delivery['snapshot'];
                        $shipmentDelivery->recountRequestedOn = $delivery['recountRequestedOn'];
                        $shipmentDelivery->reweigh = $delivery['reweigh'];
                        $shipmentDelivery->reweighRequestedOn = $delivery['reweighRequestedOn'];
                        $shipmentDelivery->reweighRequestedOn = $delivery['reweighRequestedOn'];
                        $shipmentDelivery->shippingMethodId = $delivery['shippingMethodId'];
                        $shipmentDelivery->save();


                        /* SET DATA FOR INVOICE PARTICULARS */
                        $deliveryDetails[$key] = array(
                            'id' => $delivery['id'],
                            'totalQuantity' => $delivery['totalQty'],
                            'shippingMethodId' => $delivery['shippingMethodId'],
                            'shippingMethod' => $delivery['shippingMethod'],
                            'totalItemCost' => $delivery['totalItemCost'],
                            'shippingCost' => $delivery['shippingCost'],
                            'clearingDutyCost' => $delivery['clearingDutyCost'],
                            'isDutyCharged' => $delivery['isDutyCharged'],
                            'inventoryCharge' => $delivery['inventoryCharge'],
                            'otherChargeCost' => $delivery['otherChargeCost'],
                            'totalCost' => $delivery['totalDeliveryCost'],
                        );

                        foreach ($delivery['packages'] as $count => $item) {
                            if(isset($item['itemReturn']) && $item['itemReturn']!='0')
                                continue;
                            $deliveryDetails[$key]['packages'][$count] = array(
                                'id' => $item['id'],
                                'itemName' => $item['itemName'],
                                'websiteUrl' => $item['websiteUrl'],
                                //'storeId' => $item['storeId'],
                                'siteCategoryId' => $item['siteCategoryId'],
                                'siteSubCategoryId' => $item['siteSubCategoryId'],
                                'siteProductId' => $item['siteProductId'],
                                'options' => $item['options'],
                                'itemPrice' => $item['itemPrice'],
                                'itemQuantity' => $item['itemQuantity'],
                                'itemShippingCost' => $item['itemShippingCost'],
                                'itemTotalCost' => $item['itemTotalCost'],
                            );
                        }
                    }
                }

                /*  PREPARE DATA FOR INVOICE PARTICULARS */
                $invoiceData = array(
                    'shipment' => array(
                        'totalItemCost' => $totalItemCost,
                        'totalQuantity' => $totalQuantity,
                        'totalTax' => $totalTax,
                        'isInsuranceCharged' => $isInsuranceCharged,
                        'totalInsurance' => $totalInsurance,
                        'totalWeight' => $request->totalWeight,
                        'inventoryCharge' => $inventoryCharge,
                        'otherChargeCost' => $totalOtherCharges,
                        'storageCharge' => $request->storageCharge,
                        'maxStorageDate' => $request->maxStorageDate,
                        'discount' => $request->discount,
                        'shippingCost' => $shippingCost,
                        'clearingDutyCost' => $totalClearingDuty,
                        'isDutyCharged' => $isDutyCharged,
                        'totalCost' => $totalCost,
                    ),
                    'warehouse' => array(
                        'fromAddress' => $warehouseData->address,
                        'fromZipCode' => $warehouseData->zipcode,
                        'fromCountry' => $warehouseData->countryId,
                        'fromState' => $warehouseData->stateId,
                        'fromCity' => $warehouseData->cityId,
                    ),
                    'shippingaddress' => array(
                        'toCountry' => $request->personaldetails['shippingCountryId'],
                        'toState' => $request->personaldetails['shippingStateId'],
                        'toCity' => $request->personaldetails['shippingCityId'],
                        'toAddress' => $request->personaldetails['shippingAddress'],
                        'toAlternateAddress' => $request->personaldetails['shippingAlternateAddress'],
                        'toZipCode' => $request->personaldetails['shippingZipcode'],
                        'toName' => $request->personaldetails['shippingFirstName'] . " " . $request->personaldetails['shippingLastName'],
                        'toEmail' => $request->personaldetails['shippingEmail'],
                        'toPhone' => $request->personaldetails['shippingPhone'],
                    ),
                    'deliveries' => $deliveryDetails,
                    'payment' => array(
                        'paymentMethodId' => $request->paymentMethod['paymentMethodId'],
                        'paymentMethodName' => $request->paymentMethod['paymentMethodName'],
                    ),
                );

                if (isset($request->coupondetails) && !empty($couponCode)) {
                    $invoiceData['shipment']['couponCode'] = $couponCode;
                    $invoiceData['shipment']['discountAmount'] = $discountAmount;
                    $invoiceData['shipment']['discountPoint'] = $discountPoint;
                    $invoiceData['shipment']['discountType'] = $discountType;
                }

                if ($request->paymentMethod['paymentMethodKey'] == 'wire_transfer') {
                    $invoiceData['payment']['poNumber'] = $request->paymentdetails['poNumber'];
                    $invoiceData['payment']['companyName'] = $request->paymentdetails['companyName'];
                    $invoiceData['payment']['buyerName'] = $request->paymentdetails['buyerName'];
                    $invoiceData['payment']['position'] = $request->paymentdetails['position'];
                }
                if ($request->paymentMethod['paymentMethodKey'] == 'ewallet') {
                    $invoiceData['payment']['ewalletId'] = $request->paymentdetails['ewalletId'];
                }

                if (!empty($request->data['shipmentMethodId'])) {
                    $invoiceData['shippingcharges']['shippingMethod'] = 'Y';
                    $invoiceData['shippingcharges']['isDutyCharged'] = $isDutyCharged;
                    $invoiceData['shippingcharges']['shippingCost'] = $shippingCost;
                    $invoiceData['shippingcharges']['totalClearingDuty'] = $totalClearingDuty;
                    $invoiceData['shippingcharges']['totalShippingCost'] = $totalShippingCost;
                }

                $invoiceUniqueId = 'REC' . $userData->unit . '-' . $shipmentId . '-' . date('Ymd');
                /*  PREPARE DATA FOR INVOICE PARTICULARS */

                /*  INSERT DATA INTO INVOICE TABLE */
                $invoice = new \App\Model\Invoice;
                $invoice->invoiceUniqueId = $invoiceUniqueId;
                $invoice->shipmentId = $shipmentId;
                $invoice->invoiceType = ($paymentMode == 'offline') ? 'invoice' : 'receipt';
                $invoice->type = $request->shipmentType;
                $invoice->userUnit = $userData->unit;
                $invoice->userFullName = $request->personaldetails['userTitle'] . " " . $request->personaldetails['firstName'] . " " . $request->personaldetails['lastName'];
                $invoice->userEmail = $request->personaldetails['userEmail'];
                $invoice->userContactNumber = $request->personaldetails['contactNumber'];
                $invoice->billingName = $request->personaldetails['billingFirstName'] . ' ' . $request->personaldetails['billingLastName'];
                $invoice->billingEmail = $request->personaldetails['billingEmail'];
                $invoice->billingAddress = $request->personaldetails['billingAddress'];
                $invoice->billingAlternateAddress = $request->personaldetails['billingAlternateAddress'];
                $invoice->billingCity = $request->personaldetails['billingCityId'];
                $invoice->billingState = $request->personaldetails['billingStateId'];
                $invoice->billingCountry = $request->personaldetails['billingCountryId'];
                $invoice->billingZipcode = $request->personaldetails['billingZipcode'];
                $invoice->billingPhone = $request->personaldetails['billingPhone'];
                $invoice->totalBillingAmount = $totalCost;
                $invoice->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $invoice->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                $invoice->invoiceParticulars = json_encode($invoiceData);
                $invoice->createdOn = Config::get('constants.CURRENTDATE');
                $invoice->save();
                $invoiceId = $invoice->id;
                /*  INSERT DATA INTO INVOICE TABLE */

                /*  INSERT DATA INTO PAYMENT TABLE */
                $paidfor = $shipment->shipmentType;
                if ($shipment->shipmentType == 'shopforme' || $shipment->shipmentType == 'autopart')
                    $paidfor .= 'shipment';

                $paymentTransaction = new \App\Model\Paymenttransaction;
                $paymentTransaction->userId = $userId;
                $paymentTransaction->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $paymentTransaction->paidFor = $paidfor;
                $paymentTransaction->paidForId = $shipmentId;
                $paymentTransaction->invoiceId = $invoice->id;
                $paymentTransaction->amountPaid = $totalCost;
                $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                ;
                if (!empty($transactionData))
                    $paymentTransaction->transactionData = $transactionData;
                if (!empty($transactionId))
                    $paymentTransaction->transactionId = $transactionId;
                $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $paymentTransaction->save();
                /*  INSERT DATA INTO PAYMENT TABLE */

                /*  COUPON DATA */
                if (isset($request->coupondetails) && !empty($couponCode)) {
                    /*  INSERT DATA INTO COUPON TABLE */
                    $data = array(
                        'couponId' => $couponId,
                        'code' => $couponCode,
                        'userId' => $userId,
                        'totalAmount' => $totalBDiscountCost,
                        'discountAmount' => $totalDiscount,
                        'discountPoint' => $discountPoint,
                    );
                    $couponLog = \App\Model\Couponlog::insertLog($data);

                    /*  UPDATE DATA IF COUPON POINTS */
                    if ($discountType == 'points') {
                        $fundpoint = \App\Model\Fundpoint::where('userId', $userId)->increment('point', $discountPoint);

                        $fundpointTransaction = new \App\Model\Fundpointtransaction;
                        $fundpointTransaction->userId = $userId;
                        $fundpointTransaction->point = $discountPoint;
                        $fundpointTransaction->date = Config::get('constants.CURRENTDATE');
                        $fundpointTransaction->save();
                    }
                    /*  UPDATE DATA IF COUPON POINTS */
                }

                /*  EMAIL INVOICE */
                if (!empty($invoiceId)) {
                    $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                    $data['orderNumber'] = $orderNumber;
                    $fileName = "Receipt_" . $invoiceUniqueId . ".pdf";
                    PDF::loadView('Administrator.shipments.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                    $to = $request->personaldetails['userEmail'];
                    
                    $content = "<p>Dear ".$request->personaldetails['firstName'] . " " . $request->personaldetails['lastName'].",</p>";
                    $content .= "<p>Attached is your invoice for Shipment # " . $shipmentId . " ( Order No : # " . $orderNumber . " ). You can use your order number ".$orderNumber." at any time to track your shipment online.<p>";
                    $content .= "<p>Thank you for choosing Shoptomydoor. We appreciate your business.</p>";
            
                    Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($invoiceUniqueId, $to, $fileName) {
                        $message->from(Config::get('constants.EmailSettings.FROMEMAIL'), Config::get('constants.EmailSettings.FROMNAME'));
                        $message->subject("$invoiceUniqueId - Shop for Me Invoice");
                        $message->to($to);
                        $message->attach(public_path('exports/invoice/' . $fileName));
                    });
                }

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);

            } else if ($paymentStatus == 'failed') {
                /*  INSERT DATA INTO PAYMENT TABLE */
                $paidfor = $request->shipmentType;
                if ($request->shipmentType == 'shopforme' || $request->shipmentType == 'autopart')
                    $paidfor .= 'shipment';

                $paymentTransaction = new \App\Model\Paymenttransaction;
                $paymentTransaction->userId = $userId;
                $paymentTransaction->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $paymentTransaction->paidFor = $paidfor;
                $paymentTransaction->paidForId = $shipmentId;
                $paymentTransaction->amountPaid = $totalCost;
                $paymentTransaction->status = $paymentStatus;
                if (!empty($transactionErrorMsg))
                    $paymentTransaction->errorMsg = $transactionErrorMsg;
                $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $paymentTransaction->save();
                /*  INSERT DATA INTO PAYMENT TABLE */

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => !empty($paymentErrorMessage) ? $paymentErrorMessage : 'There seems to be some issue with payment. Please contact site admin.'
                ], 200);


            } else {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'An error occured',
                ], 200);

            }
        }
    }

    public function uploadshipmentinvoice_mobile(Request $request) {

        if ($request->hasFile('fileItem') && $request->shipmentId) {
            $validator = Validator::make($request->all(), [
                        'fileItem' => 'required|mimes:pdf',
            ]);
            if ($validator->fails()) {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => $validator->errors()->first('fileItem'),
                ], 200);
            }
            $file = $request->file('fileItem');
            $name = time() . '_' . $file->getClientOriginalName();
            $destinationPath = public_path('/uploads/shipments/' . $request->shipmentId);
            
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777);
                chmod($destinationPath, 0777);
            }

            $file->move($destinationPath, $name);
            $shipmentFile = new \App\Model\Shipmentfile;
            $shipmentFile->shipmentId = $request->shipmentId;
            $shipmentFile->filename = $name;
            $shipmentFile->save();

            return response()->json([
            'status' => '200',
            'data' => url('uploads/shipments/' . $request->shipmentId . '/' . $name),
            'message' => 'success',
            ], 200);

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }

    }

    public function shipmentorderhistory_mobile(Request $request) {

        if (!empty($request->userId) && !empty($request->currentPage) && !empty($request->perPage)) {
            
            $page = $request->currentPage;
            $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
            $offset = ($page - 1) * $perPage;
            $shipmentData = array();
            $shipmentObj = new Shipment;
            $orderObj = new \App\Model\Order;
            $shipmentTable = $shipmentObj->table;
            $orderTable = $orderObj->table;

            /* Searching conditions */
            $where = $shipmentObj->prefix . "$shipmentTable.userId = $request->userId AND " . $orderObj->prefix . "$orderTable.userId = $request->userId";
            if ($request->orderDate == "FR") {
                if ($request->has('rangeDate')) {
                    $rangeDate = $request->rangeDate;
                    $str1 = $rangeDate[0];
                    $exStr1 = explode("-", $str1);
                    $exStr1_3 = explode("T", $exStr1[2]);

                    $str2 = $rangeDate[1];
                    $exStr2 = explode("-", $str2);
                    $exStr2_3 = explode("T", $exStr2[2]);

                    $string1 = $exStr1[0] . "-" . $exStr1[1] . "-" . $exStr1_3[0] . " 00:00:00";
                    $string2 = $exStr2[0] . "-" . $exStr2[1] . "-" . $exStr2_3[0] . " 23:59:59";

                    $where .= "  AND createdOn Between '" . $string1 . "' AND '" . $string2 . "'";
                }
            } else {
                if ($request->orderDate == "AD") {
                    $where .= "";
                } if ($request->orderDate == "TM") {
                    $where .= "  AND createdOn Like '%" . date("Y-m") . "%'";
                } if ($request->orderDate == "TW") {
                    $where .= "  AND createdOn BETWEEN " . $this->getCurrentWeek();
                } if ($request->orderDate == "TO") {
                    $where .= "  AND createdOn Like '%" . date("Y-m-d") . "%'";
                }
            }

            if ($request->shipmentId != "") {
                $where .=" AND " . $shipmentObj->prefix . "$shipmentTable.id=" . $request->shipmentId;
            }
            if ($request->orderId != "") {
                $where .=" AND " . $orderObj->prefix . "$orderTable.orderNumber='" . $request->orderId . "'";
            }
            if ($request->status != "") {
                $where .=" AND shipmentStatus='" . $request->status . "'";
            }

            $allShipmentStatus = Shipment::allStatus();
            $totalItems = DB::table($shipmentTable)->select("$shipmentTable.*", "$orderTable.id as orderId", "$orderTable.orderNumber")->leftJoin($orderTable, "$shipmentTable.id", "=", "$orderTable.shipmentId")->where('paymentMethodId', '!=', '0')->where('deleted', '0')->whereRaw($where)->count();
            //$shipmentList = Shipment::where('paymentMethodId', '!=', '0')->where('deleted', '0')->whereRaw($where)->orderby('id', 'desc')->take($perPage)->skip($offset)->get();
            $shipmentList = DB::table($shipmentTable)->select("$shipmentTable.*", "$orderTable.id as orderId", "$orderTable.orderNumber")->leftJoin($orderTable, "$shipmentTable.id", "=", "$orderTable.shipmentId")->where('paymentMethodId', '!=', '0')->where('deleted', '0')->whereRaw($where)->orderby('id', 'desc')->take($perPage)->skip($offset)->get();
            $shipmentListArr = $shipmentList->toArray();
            //dd($shipmentListArr);
            if (!empty($shipmentListArr)) {
                foreach ($shipmentListArr as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key == 'totalShippingCost')
                            $totalShippingCost = $value;
                        elseif ($key == 'totalClearingDuty')
                            $totalClearingDuty = $value;
                        elseif ($key == 'totalCost')
                            $totalCost = $value;
                        elseif ($key == 'paidCurrencyCode')
                            $currencyCode = $value;
                        elseif ($key == 'exchangeRate')
                            $exchangeRate = $value;

                        $shipmentData[$count][$key] = $value;

                        if ($key == 'shipmentStatus')
                            $shipmentData[$count]['status'] = $allShipmentStatus[$value];
                    }
                    $shipmentData[$count]['totalCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalCost, $currencyCode);
                    $shipmentData[$count]['totalShippingCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat(($totalClearingDuty + $totalShippingCost) * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalClearingDuty + $totalShippingCost, $currencyCode);
                }
            }

            if(!empty($shipmentData)){

                return response()->json([
                'status' => '200',
                'data' => (object)['shipmentRecords'=>$shipmentData,'totalItems' => $totalItems,'itemsPerPage'=>$perPage],
                'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);

            }

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }
    }

    public function getshipmentdetails_mobile(Request $request) {
       
        if(empty($request->shipmentid)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $deliveryData = array();
            $deliveryDetails = ViewShipmentDetails::where("shipmentId", $request->shipmentid)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->orderBy('deliveryId', 'asc')->get()->toarray();
            $deliveryData = Shipment::getDeliveryDetailsForApi($deliveryDetails);
            if(!empty($deliveryData)){

                return response()->json([
                    'status' => '200',
                    'data' => (object)['shipmentdetails' => $deliveryData],
                    'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }
            
        }
    }

    public function getallshipmentstatus_mobile() {

        $data = Shipment::allStatus();
        if(!empty($data)){ 

            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success',
            ], 200);

        } else {

            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);
        }

    }
    
    /*
    * print  shipment invoice
    */
    public function printshipmentinvoice_mobile(Request $request) {
        
        if(empty($request->shipmentid) || empty($request->shipmentType) || empty($request->paymentStatus)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $invoiceType = 'invoice';
            if ($request->paymentStatus == 'paid')
                $invoiceType = 'receipt';
            $data['invoice'] = \App\Model\Invoice::where('type', $request->shipmentType)->where('invoiceType', $invoiceType)->where('shipmentId', $request->shipmentid)->where('deleted', '0')->first();

            $data['pageTitle'] = "Print Invoice";
            $returnHTML = view('Administrator.shipments.printinvoice')->with($data)->render();
            ;
            if(count($data['invoice'])){
                
                return response()->json([
                'status' => '200',
                'data' => $returnHTML,
                'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);

            }
        }
    }

    public function getshipmentsnapshots_mobile(Request $request) {

        if(empty($request->shipmentId)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $result = array();
            $shipmentid = $request->shipmentId;
            if(!empty($request->deliveryId) && !empty($request->itemId))
            {
               $shipmentSnpashots = \App\Model\Shipmentsnapshot::where('shipmentId', $request->shipmentId)->where('deliveryId', $request->deliveryId)->where('packageId', $request->itemId)->get()->toArray();
            }else{
                $shipmentSnpashots = \App\Model\Shipmentsnapshot::where('shipmentId', $shipmentid)->get()->toArray();
            }
            if (!empty($shipmentSnpashots)) {
                foreach ($shipmentSnpashots as $index => $eachSnap) {
                    if(!empty($shipmentid))
                    {
                       $result[$index]['image'] = url('uploads/shipments/' . $shipmentid . '/' . $eachSnap['imageName']);
                    }else{
                       $result[$index]['image'] = url('uploads/shipments/' . $request->shipmentId . '/' . $eachSnap['imageName']);
                    }
                }
            }

            return response()->json([
            'status' => '200',
            'data' => $result,
            'message' => 'success',
            ], 200);
        }
    }

    /**
     * Method used to get the current week
     * @return string
     */
    public static function getCurrentWeek() {
        $monday = strtotime("last monday");
        $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

        $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");

        $this_week_sd = date("Y-m-d", $monday);
        $this_week_ed = date("Y-m-d", $sunday);
        return "'" . $this_week_sd . "%' And '" . $this_week_ed . "%'";
    }

    /*sendwronginventorynotification*/
    public function sendwronginventorynotification_mobile(Request $request) {
        
        if(empty($request->shipmentId) || empty($request->deliveryId) || empty($request->wrongInventoryMessage)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $replace = array();
            $shipmentDelivery = Shipmentdelivery::find($request->deliveryId);
            $shipmentDelivery->wrongInventory = "Y";
            $shipmentDelivery->wrongInventoryMessage = $request->wrongInventoryMessage;
            $shipmentDelivery->save();

            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'wronginventory_admin')->first();
            $to = 'contact@shoptomydoor.com';
            $replace['[MESSAGE]'] = $request->wrongInventoryMessage;
            $replace['[SHIPMENTID]'] = $request->shipmentId;
            $replace['[DELIVERYID]'] = $request->deliveryId;
            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);

            $shipmentCount = Shipmentdelivery::where("shipmentId", $request->shipmentId)->where("wrongInventory", "Y")->where("deleted", "0")->count();
            if (!empty($shipmentCount)) {
                Shipment::where("id", $request->shipmentId)->update(['wrongInventory' => 'Y']);
            } else {
                Shipment::where("id", $request->shipmentId)->update(['wrongInventory' => 'N']);
            }

            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
            ], 200);
            
        }
    }

    /*updateinventory*/
    public function updateinventory_mobile(Request $request) {

        if (!empty($request->shipmentId) && !empty($request->deliveryId)) {
            
            $shipmentDelivery = Shipmentdelivery::find($request->deliveryId);
            $shipmentDelivery->wrongInventory = "N";
            $shipmentDelivery->wrongInventoryMessage = "";
            $shipmentDelivery->save();

            $shipmentCount = Shipmentdelivery::where("shipmentId", $request->shipmentId)->where("wrongInventory", "Y")->where("deleted", "0")->count();
            if (!empty($shipmentCount)) {
                Shipment::where("id", $request->shipmentId)->update(['wrongInventory' => 'Y']);
            } else {
                Shipment::where("id", $request->shipmentId)->update(['wrongInventory' => 'N']);
            }

            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
            ], 200);

        } else {

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        }
    }

    /*updateitemprice*/
    public function updateitemprice_mobile(Request $request) {

        if(empty($request->packageId) || empty($request->price) || empty($request->userId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $packageId = $request->packageId;
            $itemPrice = $request->price;
            $userId = $request->userId;
            $shipmentPackage = Shipmentpackage::find($packageId);
            
            /* Log update */
            $shipmentPackageLog = new \App\Model\Shipmentitempriceupdatelog;
            $shipmentPackageLog->shipmentId = $shipmentPackage->shipmentId;
            $shipmentPackageLog->deliveryId = $shipmentPackage->deliveryId;
            $shipmentPackageLog->packageId = $packageId;
            $shipmentPackageLog->itemName = $shipmentPackage->itemName;
            $shipmentPackageLog->actionType = 'update';
            $shipmentPackageLog->oldPrice = $shipmentPackage->itemPrice;
            $shipmentPackageLog->newPrice = $itemPrice;
            $shipmentPackageLog->updatedByType = 'user';
            $shipmentPackageLog->updatedBy = $userId;
            $shipmentPackageLog->updatedOn = Config::get('constants.CURRENTDATE');
            $shipmentPackageLog->save();
            
            $shipmentPackageLog->updatedBy = $userId;
            $shipmentPackage->itemPrice = $itemPrice;
            $shipmentPackage->itemTotalCost = round(($itemPrice*$shipmentPackage->itemQuantity),2);
            $shipmentPackage->itemPriceEdited = '1';
            $shipmentPackage->save();
            
            Shipment::updateDeliveryFields($shipmentPackage->deliveryId);
            
            $result['itemPrice'] = customhelper::getCurrencySymbolFormat($itemPrice);
            $result['totalPrice'] = customhelper::getCurrencySymbolFormat($shipmentPackage->itemTotalCost);

            return response()->json([
            'status' => '200',
            'data' => $result,
            'message' => 'success'
            ], 200);

        }
    }

    /*upload dicounted invoice*/
    public function uploaddicountedinvoice_mobile(Request $request) {
        
        if(empty($request->hasFile('fileItem')) || empty($request->packageId) || empty($request->requestedItemprice)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $image = $request->file('fileItem');
            $name = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/discounted_invoice');
            $image->move($destinationPath, $name);
            
            $shipmentPackageInfo = Shipmentpackage::find($request->packageId);
            $shipmentPackageInfo->itemPriceEdited = '1';
            $shipmentPackageInfo->save();
            
            $shipmentInfo = Shipment::select('id','userId')->find($shipmentPackageInfo->shipmentId);
            $userInfo = User::select('firstName','lastName','unit','id')->find($shipmentInfo->userId);
            
            $shipmentPackageLog = new \App\Model\Shipmentitempriceupdatelog;
            $shipmentPackageLog->shipmentId = $shipmentPackageInfo->shipmentId;
            $shipmentPackageLog->deliveryId = $shipmentPackageInfo->deliveryId;
            $shipmentPackageLog->packageId = $request->packageId;
            $shipmentPackageLog->itemName = $shipmentPackageInfo->itemName;
            $shipmentPackageLog->actionType = 'request';
            $shipmentPackageLog->oldPrice = $shipmentPackageInfo->itemPrice;
            $shipmentPackageLog->newPrice = $request->requestedItemprice;
            $shipmentPackageLog->updatedByType = 'user';
            $shipmentPackageLog->updatedBy = $userInfo->id;
            $shipmentPackageLog->updatedOn = Config::get('constants.CURRENTDATE');
            $shipmentPackageLog->save();

            $deliveIdCount = 0;
            $shipmentDeliveryInfo = Shipmentdelivery::select('id')->where('shipmentId',$shipmentPackageInfo->shipmentId)->orderBy('id','asc')->get();
            foreach($shipmentDeliveryInfo as $eachDelivery)
            {
                $deliveIdCount++;
                if($eachDelivery->id == $shipmentPackageInfo->deliveryId)
                {
                    break;
                }
            }
            $replace = array();
            $replace['[NAME]'] = $userInfo->firstName . ' ' . $userInfo->lastName;
            $replace['[SHIPMENTID]'] = $shipmentInfo->id;
            $replace['[DELIVERYID]'] = $deliveIdCount;
            $replace['[ITEM]'] = $shipmentPackageInfo->itemName;
            $replace['[ITEMPRICE]'] = $request->requestedItemprice;
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'discounted_invoice')->first();
            $to = 'somnath.pahari@indusnet.co.in';
            
            $superAdminType = \App\Model\Adminusertype::where('userTypeSlag','superadmin')->first();
            //$to = \App\Model\UserAdmin::where('userType',$superAdminType->id)->where('status','1')->where('deleted','0')->first()->email;
            $isSend = customhelper::SendMailWithAttachment($emailTemplate, $replace, $to,public_path('uploads/discounted_invoice/' . $name));
            if($isSend)
            {
                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);
            }
            else
            {
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured while sending!'
                ], 200);
            }

            
        }
    }

    public function addpackagesnapshot_mobile(Request $request) {

        if (!empty($request->shipmentId) && !empty($request->packageId) && !empty($request->deliveryId)&& !empty($request->email)) {

            $packageSnapShot = \App\Model\Generalsettings::where('settingsKey', 'item_snap_shot')->first();
            $otherCharge = !empty($packageSnapShot->settingsValue) ? $packageSnapShot->settingsValue : 0;
            $otherChargeName = "Item Snap Shot Charge";

            /* Update Shipment Other Charges */
            $shipmentOtherCharge = new Shipmentothercharges;
            $shipmentOtherCharge->shipmentId = $request->shipmentId;
            $shipmentOtherCharge->deliveryId = $request->deliveryId;
            $shipmentOtherCharge->otherChargeName = $otherChargeName;
            $shipmentOtherCharge->otherChargeAmount = $otherCharge;
            $shipmentOtherCharge->createdBy = $request->email;
            $shipmentOtherCharge->createdOn = Config::get('constants.CURRENTDATE');
            $shipmentOtherCharge->save();

            /* Update Shipment Package */
            $shipmentPackage = Shipmentpackage::find($request->packageId);
            $shipmentPackage->snapshotOpt = 'Y';
            $shipmentPackage->save();

            /* Update Shipment Delivery */
            $shipmentDelivery = Shipmentdelivery::find($request->deliveryId);
            $shipmentDelivery->otherChargeCost = $shipmentDelivery->otherChargeCost + $otherCharge;
            $shipmentDelivery->totalCost = $shipmentDelivery->totalCost + $otherCharge;
            $shipmentDelivery->save();

            /* Update Shipment Table */
            Shipment::find($request->shipmentId)->increment('totalOtherCharges', $otherCharge);

            return response()->json([
                'status' => '200',
                'data' => (object)[
                    'otherChargeAmount' => customhelper::getCurrencySymbolFormat($shipmentDelivery->otherChargeCost),
                    'totalCost' => customhelper::getCurrencySymbolFormat($shipmentDelivery->totalCost)
                    ],
                'message' => 'success'
                ], 200);

        } else {

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);
        }
    }

    public function payreturn_mobile(Request $request)
    {
        if(empty($request->userId) || empty($request->returnData) || empty($request->data)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $paymentStatus = 'unpaid';
            $transactionId = $couponCode = $discountAmount = $discountPoint = $discountType = '';
            $transactionData = $transactionErrorMsg = array();
            $couponData = array();
            $paymentMode = 'offline';
            $paymentErrorMessage = '';

            /* Fetch User Details */
            $userId = $request->userId;
            $userData = User::find($userId); 
            $personaldetails = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->first();

            $shipmentWarehouse =Shipment::where('id', $request->returnData[0]['shipmentId'])->pluck('warehouseId');

            //print_r($shipmentWarehouse); die;

            $warehouseData = \App\Model\Warehouse::find($shipmentWarehouse[0]);

           
            if ($request->data['paymentMethodKey'] == 'wire_transfer') {
                $paymentStatus = 'paid';

                $trasactionData = json_encode(array(
                    'poNumber' => $request->paymentdetails['poNumber'],
                    'companyName' => $request->paymentdetails['companyName'],
                    'buyerName' => $request->paymentdetails['buyerName'],
                    'position' => $request->paymentdetails['position'],
                        )
                );
            }

            /*  PROCESS EWALLET PAYMENT */
            if ($request->data['paymentMethodKey'] == 'ewallet') {
                $eawalletid = (int) $request->paymentdetails['id'];

                $userEwallet = \App\Model\Ewallet::find($eawalletid)->decrement('amount', $totalCost);

                $ewalletTransaction = new \App\Model\Ewallettransaction;
                $ewalletTransaction->userId = $userId;
                $ewalletTransaction->ewalletId = $eawalletid;
                $ewalletTransaction->amount = $totalCost;
                $ewalletTransaction->transactionType = 'debit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');

                if ($ewalletTransaction->save())
                    $paymentStatus = 'paid';
                else
                    $paymentStatus = 'failed';

                $paymentMode = 'online';

                $transactionData = json_encode(array(
                    'ewalletId' => $request->paymentdetails['ewalletId'],
                        )
                );
            }

            /* PROCESS PAYSTACK DATA */
            if ($request->paymentMethod['paymentMethodKey'] == 'paystack_checkout') {
                if (!empty($request->paystackData)) {
                    $checkoutReturn = Paymenttransaction::paystack($request->paystackCreatedReference);
                    if ($checkoutReturn) {
                        $paymentMode = 'online';
                        $paymentStatus = 'paid';
                        $trasactionId = $request->paystackData['trans'];
                        $transactionData = json_encode($request->paystackData);

                    }
                } else{
                    $paymentStatus = 'error';
                }
            }

            /*  PROCESS CREDIT CARD PAYMENT */
            if ($request->data['paymentMethodKey'] == 'credit_debit_card') {
                $paymentMethod = \App\Model\Paymentmethod::where('paymentMethodKey', $request->paymentMethod['paymentMethodKey'])->first();
                if ($paymentMethod->paymentGatewayId == 1) {
                    $checkoutData = array();
                    $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                    $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                    $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                    $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                    $checkoutData['customerFirstName'] = $userData->firstName;
                    $checkoutData['customerLastName'] = $userData->lastName;
                    $checkoutData['customerAddress'] = $request->personaldetails['billingAddress'];
                    $checkoutData['customerCity'] = isset($request->personaldetails['billingCityName']) ? $request->personaldetails['billingCityName'] : "";
                    $checkoutData['customerState'] = isset($request->personaldetails['billingStateName']) ? $request->personaldetails['billingStateName'] : "";
                    $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                    $checkoutData['customerZip'] = $request->personaldetails['billingZipcode'];
                    $checkoutData['amount'] = round($totalCost, 2);
                    $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;

                    $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);
                    if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                        $paymentStatus = 'paid';
                        $paymentMode = 'online';
                        $transactionId = $checkoutReturn['TRANSACTIONID'];
                        $transactionData = json_encode($checkoutReturn);
                    } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                        $transactionErrorMsg = json_encode($checkoutReturn);
                    } else if ($checkoutReturn == 'error') {
                        $paymentStatus = 'failed';
                    }
                } else {
                    $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                    $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                    $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                    $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                    $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                    $checkoutData['customerFirstName'] = $request->personaldetails['billingFirstName'];
                    $checkoutData['customerLastName'] = $request->personaldetails['billingLastName'];
                    $checkoutData['customerAddress'] = $request->personaldetails['billingAddress'];
                    $checkoutData['customerCity'] = isset($request->personaldetails['billingCityName']) ? $request->personaldetails['billingCityName'] : "";
                    $checkoutData['customerState'] = isset($request->personaldetails['billingStateName']) ? $request->personaldetails['billingStateName'] : "";
                    $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                    $checkoutData['customerShippingAddress'] = $request->personaldetails['shippingAddress'];
                    $checkoutData['customerShippingCity'] = isset($request->personaldetails['shippingCityName']) ? $request->personaldetails['shippingCityName'] : "";
                    $checkoutData['customerShippingState'] = isset($request->personaldetails['shippingStateName']) ? $request->personaldetails['shippingStateName'] : "";
                    $checkoutData['customerShippingCountry'] = $request->personaldetails['shippingCountryName'];
                    $checkoutData['customerShippingZip'] = $request->personaldetails['shippingZipcode'];
                    $checkoutData['amount'] = round($totalCost, 2);
                    $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;
                    $checkoutData['shippingFirstName'] = $request->personaldetails['shippingFirstName'];
                    $checkoutData['shippingLastName'] = $request->personaldetails['shippingFirstName'];

                    $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);

                    if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                        $paymentStatus = 'paid';
                        $paymentMode = 'online';
                        $transactionId = $checkoutReturn['transactionResponse']['transId'];
                        $transactionData = json_encode($checkoutReturn['transactionResponse']);
                    } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                        $paymentStatus = 'failed';

                        if (!empty($checkoutReturn['transactionResponse'])) {

                            $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);

                            if (isset($checkoutReturn['transactionResponse']['errors']['error'][0])){
                                $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                            }
                            else{
                                $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                            }
                        }else {
                            if (!empty($checkoutReturn['messages']['message'])) {
                                $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['messages']['message']['text'];
                            }
                        }
                    }
                }
            }

            if ($request->data['paymentMethodKey'] == 'paypalstandard') {
                if (isset($request->paypalTransaction)) {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $request->paypalTransaction['orderID'];
                    $transactionData = json_encode($request->paypalTransaction);
                } else {
                    $paymentStatus = 'error';
                }
            }

            if ($paymentStatus == 'paid') {

                if(!empty($request->returnData))
                {
                    $i = 0;
                    $filePresent =array();
                    foreach($request->returnData as $package)
                    {
                        $i+=1;
                        //Update Pacakage table
                        $packageitem = \App\Model\Shipmentpackage::where('id', $package['itemId'])->update(['itemReturn'=>'1', 'returnLabel'=>$package['filename']]);

                        $filePresent[] = $package['filename'];

                        $packageItemDetails = \App\Model\Shipmentpackage::find($package['itemId']);

                        /* SET DATA FOR INVOICE PARTICULARS */
                            $packageDetails[$i] = array(
                                    'id' => $package['itemId'],
                                    'itemName' => $packageItemDetails['itemName'],
                                    'websiteUrl' => $packageItemDetails['websiteUrl'],
                                    'storeId' => $packageItemDetails['storeId'],
                                    'siteCategoryId' => $packageItemDetails['siteCategoryId'],
                                    'siteSubCategoryId' => $packageItemDetails['siteSubCategoryId'],
                                    'siteProductId' => $packageItemDetails['siteProductId'],
                                    'options' => $packageItemDetails['options'],
                                    'itemPrice' => $packageItemDetails['itemPrice'],
                                    'itemQuantity' => $packageItemDetails['itemQuantity'],
                                    'itemReturn' => ($packageItemDetails['itemReturn'] == '1') ? "Return requested" : "Item not returned",
                                    'returnLabel' => $packageItemDetails['returnLabel'],
                                    'itemTotalCost' => $packageItemDetails['itemTotalCost'],
                                );

                    }

                    ////Insert in Other Charges table
                    $shipmentothercharges = new \App\Model\Shipmentothercharges;

                    $shipmentothercharges->shipmentId = $request->returnData[0]['shipmentId'];
                    $shipmentothercharges->deliveryId = $request->returnData[0]['deliveryId'];
                    $shipmentothercharges->otherChargeName = "Return Item(s)";
                    $shipmentothercharges->otherChargeAmount = $request->returnData[0]['costOfReturn'];
                    $shipmentothercharges->createdBy = $userId;
                    $shipmentothercharges->createdOn = Config::get('constants.CURRENTDATE');
                    $shipmentothercharges->save(); 

                    /*  PREPARE DATA FOR INVOICE PARTICULARS */
                    $invoiceData = array(
                        'packages' => $packageDetails,
                        'shipment' => array(
                            'returnCost' => $request->returnData[0]['costOfReturn'], 
                            'totalTax' =>$request->data['taxAmount'],                       
                            'totalCost' => $request->data['paidAmount'],
                        ),
                        'warehouse' => array(
                            'fromAddress' => $warehouseData->address,
                            'fromZipCode' => $warehouseData->zipcode,
                            'fromCountry' => $warehouseData->countryId,
                            'fromState' => $warehouseData->stateId,
                            'fromCity' => $warehouseData->cityId,
                        ),
                        'payment' => array(
                            'paymentMethodId' => $request->data['paymentMethodId'],
                            'paymentMethodName' => $request->data['paymentMethodName'],
                            'paymentStatus' => $paymentStatus,
                            'totalBillingAmount'=>$request->data['paidAmount']
                        ),
                    );
                   
               


                    /*  INSERT DATA INTO INVOICE TABLE */
                    $invoiceUniqueId = 'RETURN - ' . $userData->unit . '-' .$request->returnData[0]['deliveryId'] . '-' . date('Ymd');
                    $invoice = new \App\Model\Invoice;
                    $invoice->invoiceUniqueId = $invoiceUniqueId;
                    $invoice->shipmentId = $request->returnData[0]['shipmentId'];
                    $invoice->invoiceType = 'receipt';
                    $invoice->type = 'itemReturn';
                    $invoice->userUnit = $userData->unit;
                    $invoice->userFullName = $userData->title . " " . $userData->firstName . " " . $userData->lastName;
                    $invoice->userEmail = $userData->email;
                    $invoice->userContactNumber = $userData->contactNumber;
                    $invoice->billingName = $personaldetails['firstName'] . ' ' . $personaldetails['lastName'];
                    $invoice->billingEmail = $personaldetails['email'];
                    $invoice->billingAddress = $personaldetails['address'];
                    $invoice->billingAlternateAddress = $personaldetails['alternateAddress'];
                    $invoice->billingCity = $personaldetails['cityId'];
                    $invoice->billingState = $personaldetails['stateId'];
                    $invoice->billingCountry = $personaldetails['countryId'];
                    $invoice->billingZipcode = $personaldetails['zipcode'];
                    $invoice->billingPhone = (isset($personaldetails['isdCode']) ? $personaldetails['isdCode'] . $personaldetails['phone'] : $personaldetails['phone']);
                    if(isset($request->data['paidAmount']) && $request->data['paidAmount']!="")
                    {
                      $invoice->totalBillingAmount = $request->data['paidAmount'];  
                    }
                    if(isset($request->returnData[0]['paidAmount'])){
                     $invoice->totalBillingAmount = $request->returnData[0]['paidAmount'];
                    }
                    $paymentMethodId = \App\Model\Paymentmethod::where('paymentMethodKey', $request->paymentMethod['paymentMethodKey'])->pluck('id');
                    $invoice->paymentMethodId = isset($request->data['paymentMethodId'])? $request->data['paymentMethodId']: $paymentMethodId;
                    $invoice->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                    ;
                    $invoice->invoiceParticulars = json_encode($invoiceData);
                    $invoice->createdOn = Config::get('constants.CURRENTDATE');
                    

                    if($invoice->save())
                    {
                          $invoiceId = $invoice->id;

                        /*  INSERT DATA INTO PAYMENT TABLE */
                        $paymentTransaction = new \App\Model\Paymenttransaction;
                        $paymentTransaction->userId = $userId;
                        $paymentTransaction->paymentMethodId = isset($request->data['paymentMethodId'])? $request->data['paymentMethodId']: $paymentMethodId;
                        $paymentTransaction->paidFor = 'itemReturn';
                        $paymentTransaction->paidForId = $request->returnData[0]['shipmentId'];
                        $paymentTransaction->invoiceId = $invoice->id;
                        if(isset($request->data['paidAmount']) && $request->data['paidAmount']!="")
                        {
                          $paymentTransaction->amountPaid = $request->data['paidAmount'];  
                        }
                        if(isset($request->returnData[0]['paidAmount'])){
                            $paymentTransaction->amountPaid = $request->returnData[0]['paidAmount'];
                        }
                        
                        $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                        if (!empty($transactionData))
                            $paymentTransaction->transactionData = $transactionData;
                        if (!empty($transactionId))
                        {
                            $paymentTransaction->transactionId = $transactionId;
                        }
                        $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                        $paymentTransaction->save();

                        $fileName = "Return_" . $invoiceUniqueId . ".pdf";

                        $data['invoice'] = $invoice;
                        $data['pageTitle'] = "Print Invoice";
                       
                        PDF::loadView('Administrator.shipments.returninvoice', $data)->save(public_path('exports/return/' . $fileName))->stream($fileName);

                        $to='contact@shoptomydoor.com';
                        //$to='anumita.banerjee@indusnet.co.in';
                        Mail::send(['html' => 'mail'], ['content' => " Return label for Shipment"], function ($message) use($invoiceUniqueId, $to, $filePresent, $request) {
                            $message->subject("Return Label for shipment - ".$request->returnData[0]['shipmentId']);
                            $message->to($to);
                            if(count($filePresent)>0)
                            {

                                foreach($filePresent as $item)
                                {
                                  $message->attach(public_path('uploads/shipments/' . $request->returnData[0]['shipmentId'] . '/'.$item));  
                                }

                            }
                            
                        });

                    }
                  
                    return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success'
                    ], 200);

                } else {

                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'An error occured on return data!'
                        ], 200);
                }

            }else if ($paymentStatus == 'failed') {
                /*  INSERT DATA INTO PAYMENT TABLE */
                $paymentTransaction = new \App\Model\Paymenttransaction;
                $paymentTransaction->userId = $userId;
                $paymentTransaction->paymentMethodId = $request->data['paymentMethodId'];
                $paymentTransaction->paidFor = 'itemReturn';
                $paymentTransaction->amountPaid = $totalCost;
                $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                if (!empty($transactionErrorMsg))
                    $paymentTransaction->errorMsg = $transactionErrorMsg;
                $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $paymentTransaction->save();
                /*  INSERT DATA INTO PAYMENT TABLE */
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => !empty($paymentErrorMessage) ? $paymentErrorMessage : 'There seems to be some issue with payment. Please contact site admin.'
                ], 200);

            } else {
                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Something is wrong in payment process! Please try again!'
                ], 200);
            }

        }
    }

    public function updateReturnData_mobile(Request $request)
    {
        if(empty($request->id) || empty($request->shipmentId) || empty($request->deliveryId) || empty($request->costOfReturn) || !$request->hasFile('image')){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $result = array();
            $result['itemId'] = $request->id;
            $result['shipmentId'] = $request->shipmentId;
            $result['deliveryId'] = $request->deliveryId;
            $result['costOfReturn'] = $request->costOfReturn;            
           
            $image = $request->file('image');
            $name = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/shipments/'.$request->shipmentId);

            if($image->move($destinationPath, $name))
            {
                $result['filename'] = $name;
            }
               
            
            return response()->json([
            'status' => '200',
            'data' => $result,
            'message' => 'success'
            ], 200);
        }

    }

    public function getDelieveryItems_mobile(Request $request)
    {
        if(empty($request->deliveryId) || empty($request->shipmentId) || empty($request->returnCost)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $result = array();

            $result = Shipmentpackage::where("deliveryId", $request->deliveryId)->where("shipmentId", $request->shipmentId)->where("itemReturn", "0")->get();
            $returnCost = explode("$", $request->returnCost);

            return response()->json([
            'status' => '200',
            'data' => (object)['items'=>$result,'returnCost'=>trim($returnCost[1])],
            'message' => 'success'
            ], 200);

        }
    }

}
