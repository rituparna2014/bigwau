<?php

namespace App\Http\Controllers\Api\mobile;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Usercart;
use App\Model\Emailtemplate;
use App\Model\Addressbook;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Stores;
use App\Model\Currency;
use App\Model\Defaultcurrency;
use App\Model\Sitecategory;
use App\Model\Siteproduct;
use App\Model\Usershipmentsettings;
use App\Model\Subscriber;
use Config;
use Illuminate\Support\Facades\Hash;
use Auth;
use customhelper;
use DB;
use App\Model\Authlog;
use App\Model\Smstemplate;
use PDF;
use Mail;

class UsersController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->_perPage = 10;
    }

    /**
     * Method for customer or user registration
     * @param Request $request
     * @return array
     */
    public function saveapiuser_mobile(Request $request) {

        $user = new User;
        $addressBook = new Addressbook;
        $error = '';
        $validator = Validator::make($request->all(), [
                    'firstName' => 'required|alpha',
                    'lastName' => 'required|alpha',
                    'password' => 'required',
                    'email' => 'required|unique:' . $user->table . ',email|email',
                    'phcode' => 'required',
                    'address' => 'required',
                    'contactNumber' => 'required',
                    'country' => 'required',
                    //'state' => 'required',
                    'city' => 'required'
        ]);
        if ($validator->fails()) {
            $dataErr = $validator->errors();
            //echo $dataErr;
            foreach ($dataErr->all() as $eacherror)
                $error .= $eacherror . ',';

            return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => substr($error, 0, -1),
                            ], 400);
        }

        if ($request->referralCode != '') {
            $referralUser = User::where('referralCode', $request->referralCode)->first();
            if (count($referralUser) > 0) {
                $user->referredBy = $referralUser->id;
            } else {
                return response()->json([
                            'status' => '400',
                            'data' => null,
                            'message' => 'Invalid referral code'
                                ], 400);
            }
        }

        /* GENERATE OTP */
        //$otp = $this->generateotp();
        $otp = customhelper::generateOTP();

        $user->status = '0';
        $user->password = Hash::make($request->password);
        $user->createdBy = 1;
        $user->createdOn = Config::get('constants.CURRENTDATE');
        $user->title = $request->title;
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->isdCode = $request->phcode;
        $user->contactNumber = $request->contactNumber;
        $user->company = empty($request->company) ? '' : $request->company;
        $user->referralCode = User::generateReferralCode();
        $user->profileImage = 'default.jpg';
        $user->registeredBy = 'app';
        $user->verifiedCode = $otp;
        $user->otpSentDate = Config::get('constants.CURRENTDATE');

        if ($user->save()) {
            $userId = $user->id;

            $addressBook->userId = $userId;
            $addressBook->title = $request->title;
            $addressBook->firstName = $request->firstName;
            $addressBook->lastName = $request->lastName;
            $addressBook->email = $request->email;
            $addressBook->address = $request->address;
            $addressBook->alternateAddress = $request->alternateAddress;
            $addressBook->cityId = $request->city;
            $addressBook->stateId = $request->state;
            $addressBook->countryId = $request->country;
            $addressBook->isdCode = $request->phcode;
            $addressBook->phone = $request->contactNumber;
            $addressBook->modifiedBy = '0';
            $addressBook->modifiedOn = Config::get('constants.CURRENTDATE');
            $addressBook->isDefaultBilling = '1';

            $addressBook->save();

            /* Update unit of user while adding */
            /*$maxUnitNumber = User::where('deleted', '0')->where('status', '1')->max('unitNumber');
            if (empty($maxUnitNumber))
                $maxUnitNumber = Config::get('constants.MAXUNITNUMBER');
            else
                $maxUnitNumber = $maxUnitNumber + 1;*/
            $maxUnitNumber = $userId;
            User::where('id', $userId)->update(['unitNumber' => $maxUnitNumber]);

            
            $activationToken = strtoupper(md5(uniqid(rand())));
            User::where('id', $userId)->update(['activationToken' => $activationToken]);
            $emailTemplate = Emailtemplate::where('templateKey', 'registration_active_for_mobile')->first();
            $to = $user->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $activationLink = Config::get('constants.frontendUrl').'join/'.$userId.'/'.$activationToken;
            $replace['[ACTIVATION_LINK]'] = '<a href="'.$activationLink.'">Activate</a>';
            $replace['[OTP]'] = $otp;
            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);

            //send otp to mobile
            $toMobile = trim($user->isdCode.$user->contactNumber);
            $smsTemplate = Smstemplate::where('templateKey', 'registration_active')->first();
            if($toMobile){
                $replaceVar['[NAME]'] = $user->firstName;
                $replaceVar['[OTP]'] = $otp;
                $isSendMsg = customhelper::sendMSG($toMobile,$replaceVar,$smsTemplate);
            }

            return response()->json([
                        'status' => '200',
                        'data' => $user,
                        //'message' => 'Registration successful. Please check you email for activation code'
                        'message' => 'OTP Sent'
                            ], 200);
        }


        return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'Something went wrong',
        ], 400);
    }

    /**
     * Method for resend OTP
     * @param Request $request
     * @return integer
     */
    public function resendotp_mobile(Request $request) {

        $userId = $request->userId;

        /* VERIFY WHETHER USER HAS ALREADY ACTIVATED OR NOT */
        $verify = User::where('status', '0')->where('id', $userId)->first();

        if(!empty($verify)){
            //$otp = $this->generateotp();
            $otp = customhelper::generateOTP();

            /* MODIFY USER TABLE WITH NEW OTP */
            $user = User::find($userId);
            $user->verifiedCode = $otp;
            $user->otpSentDate = Config::get('constants.CURRENTDATE');
            $user->save();

            $emailTemplate = Emailtemplate::where('templateKey', 'resend_otp')->first();
            $to = $user->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $replace['[OTP]'] = $otp;
            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
            
            //send otp to mobile
            $toMobile = trim($user->isdCode.$user->contactNumber);
            $smsTemplate = Smstemplate::where('templateKey', 'resend_registration_active')->first();
            if($toMobile){
                $replaceVar['[NAME]'] = $user->firstName;
                $replaceVar['[OTP]'] = $otp;
                $isSendMsg = customhelper::sendMSG($toMobile,$replaceVar,$smsTemplate);
            }

            return response()->json([
                'status' => '200',
                'data' => $user,
                'message' => 'OTP Resent'
            ], 200);

        } else {
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'Either user already activated or error in operation'
            ], 400);
        }

    }

    /**
     * Method for verify OTP
     * @param Request $request
     * @return array
     */
    public function getotp_mobile(Request $request) {
        $userId = $request->userId;
        $otp = $request->otp;

        $verify = User::where('verifiedCode', $otp)->where('status', '0')->where('id', $userId)->first();
        if(!empty($verify)){

            /* VERIFY THAT OTP SUBMITTED WITHIN 5 MINUTES */
            $toTime = strtotime(Config::get('constants.CURRENTDATE'));
            $fromTime=strtotime($verify->otpSentDate); 
            $convertInMinutes = round(abs($toTime - $fromTime) / 60,2);

            if($convertInMinutes > 5){
                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'OTP Expired'
                ], 400);
            }

            /* MODIFY DB RECORD */
            $user = User::find($userId);
            $user->verifiedCode = NULL;
            $user->status = '2';
            $user->activationToken = NULL;
            $user->verifiedOn = Config::get('constants.CURRENTDATE');
            $user->otpSentDate = NULL;
            $user->save();
            
            $emailTemplate = Emailtemplate::where('templateKey', 'successfull_activation')->first();
            $to = $user->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);

            return response()->json([
                'status' => '200',
                'data' => $user,
                'message' => 'Registration successful. Administrator will activate your account shortly!'
            ], 200);

        } else {
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'OTP not matched'
            ], 400);
        }
    }

    /**
     * Method for generate OTP
     * @return integer
     */
    public function generateotp() {
        $otp = rand(000000, 999999);
        return $otp;
    }

    /**
     * Method for update customer primary data
     * @param Request $request
     * @return array
     */
    public function updateuser_mobile(Request $request) {

        $userId = $request->userId;
        if(empty($userId))
            return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'No user id'
                            ], 400);

        $validator = Validator::make($request->all(), [
                    'firstName' => 'required|alpha',
                    'lastName' => 'required|alpha',
                    'title' => 'required',
                    'company' => 'required',
        ]);
        if ($validator->fails())
            return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => $validator->errors()->first()
                            ], 400);

        $userObj = User::find($userId);

        if(empty($userObj))
            return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'No user found'
                            ], 400);

        if (isset($request->newsletterSubscribe)) {

            if ($request->newsletterSubscribe)
                $newsSubStatus = '1';
            else
                $newsSubStatus = '0';

            $newsletterData = Subscriber::where('userId', $userId)->first();
            if (count($newsletterData) > 0)
                Subscriber::where('id', $newsletterData->id)->update(['status' => $newsSubStatus]);
            else if ($request->newsletterSubscribe) {
                $newsLetterObj = new Subscriber;
                $newsLetterObj->userId = $userId;
                $newsLetterObj->userEmail = $userObj->email;
                $newsLetterObj->status = '1';
                $newsLetterObj->save();
            }
        }



        $userObj->title = $request->title;
        $userObj->firstName = $request->firstName;
        $userObj->lastName = $request->lastName;
        $userObj->company = $request->company;


        /* SAVE PROFILE IMAGE IF ANY */
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/profile_image');
            $image->move($destinationPath, $name);
            User::where('id', $userId)->update(['profileImage' => $name]);
        }


        if ($request->dateOfBirth != '') {
            $userObj->dateOfBirth = date('Y-m-d', strtotime($request->dateOfBirth));
        }
        if ($request->oldPassword != '') {
            //return json_encode(array('old'=>Hash::make($request->oldPassword),'pass'=>$userObj->password));
            if (Hash::check($request->oldPassword, $userObj->password)) {
                if ($request->newPassword == $request->newRepeatPassword) {
                    $userObj->password = Hash::make($request->newPassword);
                } else {
                    return response()->json([
                                'status' => '400',
                                'data' => null,
                                'message' => 'New password and retype password did not match'
                                    ], 400);
                }
            } else {
                return response()->json([
                            'status' => '400',
                            'data' => null,
                            'message' => 'Old password did not match'
                                ], 400);
            }
        }
        if ($userObj->save()) {
            return response()->json([
                        'status' => '200',
                        'data' => $userObj,
                        'message' => 'Detail has been saved successfully'
                            ], 200);
        }
    }

    /**
     * Method for customer login
     * @param Request $request
     * @return array
     */
    public function userlogin_mobile(Request $request) {

        
        /* IF USER STATUS IS INACTIVE BUT WANY TO LOGIN */
        $checkStatus = User::select("status", "id")->where("email", $request->email)->first();
        if(!empty($checkStatus)){

            if($checkStatus->status == '0'){
                $userId = $checkStatus->id;
                $verify = array('verified' => 0, 'userId' => $userId);
                return response()->json([
                    'status' => '400',
                    'data' => (object)$verify,
                    'message' => 'Account is not activated',
                ], 200);
            }
        }

        $userdata = array(
            'email' => $request->email,
            'password' => $request->password,
            'status' => '1',
        );
        if (Auth::guard('user')->attempt($userdata)) {

            $user = auth()->guard('user')->user();
           
            $isLoggedIn = \App\Model\Userlog::checkstatus($user->id, $request->deviceType, $request->deviceId, '', '');
            if(!empty($isLoggedIn)){

                return response()->json([
                'status' => '401',
                'data' => (object)["userId" => $user->id],
                'message' =>  Config::get('constants.MessageForceLogin.Text')
                ], 200);

            } else {

               $insertLogRecord = \App\Model\Userlog::insertlogrecord($user->id, $request->deviceType, $request->deviceId, $request->deviceToken, '');

            }

            $tokenData = $user->createToken('login');
            $token = $tokenData->accessToken;
            $hasDeliveryAddress = "0";
            $userShippingAddress = Addressbook::where('userId', $user->id)->where('deleted', '0')->where('isDefaultShipping','1')->with('country', 'state', 'city')->get();
            if($userShippingAddress->count()>0)
                $hasDeliveryAddress = "1";
            /*$subscriptionData = Subscriber::where('userId', $user->id)->first();
            $subscriptionStatus = '0';
            if (!empty($subscriptionData))
                $subscriptionStatus = $subscriptionData->status;*/

            if ($user->profileImage)
                $user->profileImage = url('uploads/profile_image/' . $user->profileImage);                
            return response()->json([
                    "status" => 200,
                    "data" => array(
                        "token" => $token,
                        "userId" => $user->id,
                        "name" => $user->firstName." ".$user->lastName,
                        "profileImage" => $user->profileImage,
                        "hasDeliveryAddress" => $hasDeliveryAddress,
                    ),
                    "message" => "Login successful"
            ], 200);
            
            
        } else {
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'Username / Password not matched',
            ], 200);
        }
    }

    public function usershippingaddress_mobile(Request $request) {

        if(empty($request->input('id'))){
            
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $addressType = 'All';

            if(!empty($request->input('addressType'))){
                $addressType = $request->input('addressType');
            }

            $id = $request->input('id');
            $where = '1';
            if ($addressType == 'shipping')
                $where = 'isDefaultShipping = "1"';
            else if ($addressType == 'billing')
                $where = 'isDefaultBilling = "1"';
            if ($request->input('addressId') && $request->input('addressId') != '')
                $where .= ' AND id=' . $request->input('addressId');

            $userShippingAddress = Addressbook::where('userId', $id)->where('deleted', '0')->whereRaw($where)->with('country', 'state', 'city')->get();

            if (count($userShippingAddress) > 0) {

                return response()->json([
                'status' => '200',
                'data' => $userShippingAddress,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);
            }
        }
    }

    /**
     * Method for add/update address book records for a customer
     * @param Request $request
     * @return array
     */
    public function saveshippingaddress_mobile(Request $request) {
        
        if(empty($request->userId)){
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'No user id'
                    ], 400);
        } else {
            $userId = $request->userId;
            $id = (!empty($request->id) ? $request->id : '-1');
            $addressBook = new Addressbook;

            $validator = Validator::make($request->all(), [
                        'firstName' => 'required|alpha',
                        'lastName' => 'required|alpha',
                        'email' => 'required|email',
                        'country' => 'required',
                        'address' => 'required',
                        //'cityId' => 'required',
                        'phone' => 'required',
            ]);

            if ($validator->fails()) {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => $validator->errors()
                        ], 400);
            } else {

                if ($id != '-1' && $id != '0' && $request->postAction == 'modify') {
                    $addressBook = Addressbook::find($id);
                } else {
                    $addressBook = new Addressbook;
                    $addressBook->userId = $userId;
                }
                $addressBook->title = $request->title;
                $addressBook->firstName = $request->firstName;
                $addressBook->lastName = $request->lastName;
                $addressBook->email = $request->email;
                $addressBook->address = $request->address;
                $addressBook->alternateAddress = $request->alternateAddress;
                $addressBook->cityId = $request->city;
                $addressBook->stateId = $request->state;
                $addressBook->countryId = $request->country;
                $addressBook->zipcode = empty($request->zipcode) ? '' : $request->zipcode;
                $addressBook->phone = $request->phone;
                if(!empty($request->phCode)){
                    $addressBook->isdCode = $request->phCode;
                }
                        $addressBook->alternatePhone = $request->alternatePhone;
                if(!empty($request->altphCode)){
                    $addressBook->altIsdCode = $request->altphCode;
                }
                $addressBook->modifiedBy = '0';
                $addressBook->modifiedOn = Config::get('constants.CURRENTDATE');
                if (isset($request->locationId) && $request->locationId != '')
                    $addressBook->locationId = $request->locationId;

                $userBillingAddress = Addressbook::where('userId', $userId)->where('isDefaultBilling', '1')->first();
                if (empty($userBillingAddress)) {
                    $addressBook->isDefaultBilling = '1';
                }

                $userShippingAddress = Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->first();
                if (empty($userShippingAddress)) {
                    $addressBook->isDefaultShipping = '1';
                }

                if ($addressBook->save()) {
                    $addressBookId = $addressBook->id;
                    if (isset($addressBook->isDefaultShipping) && $addressBook->isDefaultShipping == '1') {

                        $user = User::find($userId);
                        $countryCode = Country::where('id', $request->country)->first()->code;
                        /*$maxUnitNumber = User::where('id', $userId)->max('unitNumber');
                        if (empty($maxUnitNumber))
                            $maxUnitNumber = Config::get('constants.MAXUNITNUMBER');
                        else
                            $maxUnitNumber = $maxUnitNumber + 1;*/

                        //$user->unitNumber = $maxUnitNumber;
                        $user->unit = $countryCode . $userId;
                        $user->save();
                    }

                    return response()->json([
                        'status' => '200',
                        'data' => $addressBook,
                        'message' => 'Delivery address has been saved successfully'
                            ], 200);

                } else {
                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'Error in operation'
                            ], 400);
                }
            }
        }
    }

    /**
     * Method for fetch all addressbook records for a customer
     * @param Request $request
     * @return array
     */
    public function getshippingaddress_mobile(Request $request){
        $userId = $request->userId;

        if(empty($userId))
            return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'No user id'
                            ], 400);

        $addressbookRecords = Addressbook::where("userId", $userId)->where("deleted", '0')->orderby("id", "desc")->get();

        if(!empty($addressbookRecords[0])){
            return response()->json([
                'status' => '200',
                'data' => $addressbookRecords,
                'message' => 'Successfully returns all addressbook records'
                    ], 200);
        } else {
            return response()->json([
                'status' => '200',
                'data' => null,
                'message' => 'No record found'
                    ], 200);
        }
    }

    /**
     * Method for set a billing or shipping address as default for a customer
     * @param Request $request
     * @return array
     */
    public function setdefaultshipping_mobile(Request $request) {

        $userId = $request->userId;
        $addressId = $request->addressId; 
        $type = $request->type;

        if(empty($userId))
            return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'No user id'
                            ], 400);

        if ($type == 'shipping')
            $updateAddressField = 'isDefaultShipping';
        else if ($type == 'billing')
            $updateAddressField = 'isDefaultBilling';
        Addressbook::where('userId', $userId)->update([$updateAddressField => '0']);
        $update = Addressbook::where('id', $addressId)->update([$updateAddressField => '1']);
        if ($type == 'shipping') {
            $address = Addressbook::find($addressId);
            $user = User::find($userId);
            $countryCode = Country::where('id', $address->countryId)->first()->code;
            $maxUnitNumber = User::where('id', $userId)->max('unitNumber');
            if (empty($maxUnitNumber))
                $maxUnitNumber = Config::get('constants.MAXUNITNUMBER');
            else
                $maxUnitNumber = $maxUnitNumber + 1;

            $user->unitNumber = $maxUnitNumber;
            $user->unit = $countryCode . $maxUnitNumber;
            $user->save();
        }
        if($update){
            return response()->json([
                    'status' => '200',
                    'data' => null,
                    'message' => 'Record successfully saved'
                        ], 200);
        } else {
            return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Record not found'
                        ], 400);
        }
        
    }

    /**
     * Method for get master data
     * @return array
     */
    public function getmasterdata_mobile(){
        $initial = array();
        $countries = Country::select('id', 'code', 'name','isdCode')->where("status", '1')->orderby('priority', 'desc')->get();
        $states = State::getAllStateList();
        $cities = City::getAllCityList();
            

        $unitList[] = array("id" => 1, "value" => 'lbs/in');
        $unitList[] = array("id" => 2, "value" => 'kg/cm');

        $stores = Stores::select("id", "storeName")->where("status", '1')->where("deleted", '0')->get();

        $currencies = Currency::getAllCurrencyList();

        $siteCategories = Sitecategory::getAllCategories();
        $siteSubcategories = Sitecategory::getAllSubcategories();
        $siteProducts = Siteproduct::select("id", "productName", "categoryId", "subCategoryId")->where('status', '1')->get();

        $autoCarMake = \App\Model\Automake::where('status', '0')->where('deleted', '0')->orderby('orders', 'asc')->get();
        $autoCarModel = \App\Model\Automodel::where('deleted', '0')->get();

        $initial = array(
            "CountryConst" => $countries,
            "stateList" => $states,
            "cityList"=> $cities,
            "unitList" => $unitList,
            "storeList" => $stores,
            "currencyList" => $currencies,
            "categoryList" => $siteCategories,
            "subcategoryList" => $siteSubcategories,
            "productList" => $siteProducts,
            "carMake" => $autoCarMake,
            "carModel" => $autoCarModel
        );

        return response()->json([
        'status' => '200',
        'data' => $initial,
        'message' => 'success'
            ], 200);

    }

    /**
     * Method for show user details
     * @param Request $request
     * @return array
     */
    public function getuserdetails_mobile(Request $request) {

        $data = array();
        $userId = $request->userId;

        if(!$userId){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'Please provide a valid user Id'
                ], 400);
        }

        /* GET ALL WAREHOUSE */
        $allWarehouse = \App\Model\Warehouse::where('status', '1')->where('deleted', '0')->with('country', 'state', 'city')->get();
        if (!empty($allWarehouse)) {
            $data['all_warehouses'] = $allWarehouse;
        }
        /*GET USER RELATED WAREHOUSE*/
        $userWarehouses = \App\Model\Warehouse::getUserWarehouseList($userId);

        if (!empty($userWarehouses)) {
            $data['user_warehouses'] = $userWarehouses;
        }

        /*GET DEFAULT CURRENCY & SYMBOL*/
        $defaultCurrencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode();
        $defaultcurrency = \App\Helpers\customhelper::getCurrencySymbolCode('',true);
        if(!empty($defaultCurrencySymbol)){
            $data['defaultcurrency_symbol'] = $defaultCurrencySymbol;
        }
        if(!empty($defaultcurrency)){
            $data['defaultcurrency'] = $defaultcurrency;
        }

        /* GET DEFAULT SHIPPING ADDRESS */
        $queryIsDefaultShipping = Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->where('deleted', '0')->get();
        if (!empty($queryIsDefaultShipping)) {
            $data['my_delivery_address'] = $queryIsDefaultShipping;
        }

        /* GET USER DETAILS */
        $queryUserDetails = User::where('id', $userId)->where('deleted', '0')->first();
        if (!empty($queryUserDetails)) {
            $data['userDetails'] = $queryUserDetails;
            $data['profileImage'] = (file_exists(public_path() . '/uploads/profile_image/'. $queryUserDetails->profileImage) ? url('uploads/profile_image/'.$queryUserDetails->profileImage) : url('administrator/img/default-no-img.jpg') );
        }
        
        /* GET BALANCE POINTS */
        $getQueryBalancePoints = \App\Model\Fundpoint::where('userId', $userId)->first();
        if(!empty($getQueryBalancePoints)){
            
            /* COUNT TOTAL POINT EARN */
            $getQueryEarnedPoints = \App\Model\Fundpointtransaction::where('userId', $userId)->where('type', 'A')->sum('point');
            $data['earnedPoints'] = $getQueryEarnedPoints;


            $data['balancePoint'] = $getQueryBalancePoints->point;

            /* GET TOTAL EARNED POINTS TRANSACTIONS */
            

            /* GET DATA FROM REWARD SETTINGS */
            $gainedPoint = ($getQueryBalancePoints->point == 0 ? 1 : $getQueryBalancePoints->point);

            $getQueryReward = \App\Model\Rewardsetting::where('deleted', '0')->whereRaw('points <= '.$gainedPoint)->orderby('id', 'desc')->first();
            if(!empty($getQueryReward)){
                $data['rewardName'] = $getQueryReward->name;
                $data['rewardImage'] = (file_exists(public_path() . '/uploads/rewardpoints/'. $getQueryReward->image) ? url('uploads/rewardpoints/'.$getQueryReward->image) : url('administrator/img/default-no-img.jpg') );

                /* POINTS REMAINING TO ACHIEVE NEXT LEVEL */
                $reminingPointsQuery = \App\Model\Rewardsetting::where('deleted', '0')->where("id", ">", $getQueryReward->id)->orderby('id', 'asc')->first();
                $data['remaining'] = ($gainedPoint == 1 ? $reminingPointsQuery->points : $reminingPointsQuery->points - $gainedPoint);
            } else {
                $data['rewardName'] = "No Reward";
                $data['rewardImage'] = '';
                $data['remaining'] = 'N/A';
            }
            
            

            /* POINTS NEAR TO EXPIRE */
            $splitExpDate = '';

            $nearToExpireQuery = \App\Model\Fundpointtransaction::where('expiredDate', ">", date('Y-m-d'))->orderby("expiredDate", 'asc')->first();
            if(!empty($nearToExpireQuery)){
                $splitExpDate = explode(" ", $nearToExpireQuery->expiredDate);
                $totalPointsExpired = \App\Model\Fundpointtransaction::select(DB::raw("SUM(point) as point"))->where('expiredDate', "LIKE", trim($splitExpDate[0])."%")->get();

                $dateExp=date_create(trim($splitExpDate[0]));
                $data['expString'] = $totalPointsExpired[0]->point . " points will expires on ". date_format($dateExp,"M dS, Y");
                $data['expStringShort'] = date_format($dateExp,"M dS, Y");
            } else {
                $data['expString'] = '';
                $data['expStringShort'] = '';
            }
            

        } else {

            /* NO POINT IN MAIN POINT TABLE */
            $data['balancePoint'] = 0;
            $data['earnedPoints'] = 0;

            $gainedPoint = 1;

            /* GET DATA FROM REWARD SETTINGS */
            $getQueryReward = \App\Model\Rewardsetting::where('deleted', '0')->orderby('points', 'asc')->first();
            if(!empty($getQueryReward)){
                $data['rewardName'] = $getQueryReward->name;
                $data['rewardImage'] = url('uploads/rewardpoints/'.$getQueryReward->image);

                /* POINTS REMAINING TO ACHIEVE NEXT LEVEL */
                $reminingPointsQuery = \App\Model\Rewardsetting::where('deleted', '0')->where("id", ">", $getQueryReward->id)->orderby('id', 'asc')->first();
                if(!empty($reminingPointsQuery)){
                    $data['remaining'] = $reminingPointsQuery->points - $gainedPoint + 1;
                } else {
                    $data['remaining'] = 0;
                }
                

            } else {
                $data['rewardName'] = "No Reward";
                $data['rewardImage'] = '';
                $data['remaining'] = 0;
            }
            
            $data['expString'] = '';
            $data['expStringShort'] = 'N/A';

        }
        
        //get user shipment count
        $shipmentCount = \App\Model\Shipment::getUserShipmentAndNotDeleverCount($userId);
        //get user shipment not delevered count
        $shipmentNotDeleveredCount = \App\Model\Shipment::getUserShipmentAndNotDeleverCount($userId,6);
        $data['shipmentCount'] = ($shipmentCount)?$shipmentCount:0;
        $data['shipmentNotDeleveredCount'] = ($shipmentNotDeleveredCount)?$shipmentNotDeleveredCount:0;

        return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success'
            ], 200);

    }

    public function is_valid_password($password) {
        return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$_!%*?&-]{8,20}$/', $password) ? true : false;
    }

    /**
     * Method for change user password
     * @param Request $request
     * @return array
     */
    public function changepassword_mobile(Request $request) {
        $data = array();
        $userId = $request->userId;

        if(!$userId){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'Please provide a valid user Id'
            ], 400);
        }

        /* GET USER DETAILS */
        $queryUserDetails = User::select("email", "id")->where('id', $userId)->where('deleted', '0')->first();
        if (!empty($queryUserDetails)) {
            $data['userEmail'] = $queryUserDetails;
        }

        $userObj = User::find($userId);

        if ($request->oldPassword != '') {
            //return json_encode(array('old'=>Hash::make($request->oldPassword),'pass'=>$userObj->password));
            if (Hash::check($request->oldPassword, $userObj->password)) {
                if ($request->newPassword == $request->newRepeatPassword) {
                    if($this->is_valid_password($request->newPassword) == false){
                        return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'Password must contains minimum 8 characters, atleast one digit, one special character, one uppercase letter'
                        ], 400);
                    }

                    $userObj->password = Hash::make($request->newPassword);
                    $userObj->save();

                    return response()->json([
                    'status' => '200',
                    'data' => null,
                    'message' => 'Password has been changed successfully'
                    ], 200);


                } else {
                    return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'New password and confirm password did not matched'
                    ], 400);
                }
            } else {
                return response()->json([
                 'status' => '200',
                    'data' => null,
                    'message' => 'Old password not matched'
                ], 200);
            }
        }

    }

    /**
     * Method for get a quote
     * @param Request $request
     * @return array
     */
    public function getaquote_mobile(Request $request){

        #echo "<pre>"; print_r($request->all()); die;

        $price = $chargeable_weight = $totalItemCost = $dim_weight = $packageCount = $heightWeightError = 0;
        $totalWeight = 0;
        $shippingMethodData = array();
        $message = "";

        $chargeableWeightFactorKg = \App\Model\Generalsettings::where('settingsKey', 'charge_weight_factor')->first();
        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

        if (!empty($chargeableWeightFactorKg))
            $chargeableWeightFactorKgVal = $chargeableWeightFactorKg->settingsValue;
        if (!empty($chargeableWeightFactor))
            $chargeableWeightFactorVal = $chargeableWeightFactor->settingsValue;



        if (!empty(json_decode($request->packages))) {

            foreach(json_decode($request->packages) as $eachPackage) {

                $packageCount ++;
                $length = $eachPackage->length;
                $width = $eachPackage->width;
                $height = $eachPackage->height;
                $weight = $eachPackage->weight;
                $unit = $eachPackage->unit;
                $declaredValue = $eachPackage->declaredValue;

                if ($length > 0 && $width > 0 && $height > 0
                ) {
                    $weight = max($weight, ($length * $width * $height / (($unit == 'K') ? $chargeableWeightFactorKgVal : $chargeableWeightFactorVal)));
                }
                else
                {
                    $message = "Please enter height/weight/length correctly";
                    $heightWeightError = 1;
                    break;
                }
                if ($unit == 'K') {
                    $weight *= 2.2;
                }
                $totalWeight += round($weight, 2);
                $totalItemCost += $declaredValue;
            }

            if($heightWeightError == 0)
            {
                $param = array(
                    'fromCountry' => $request->fromCountryId,
                    'fromState' => $request->fromStateId,
                    'fromCity' => $request->fromCityId,
                    'toCountry' => $request->toCountryId,
                    'toState' => $request->toStateId,
                    'toCity' => $request->toCityId,
                    'totalWeight' => $totalWeight,
                    'totalProcurementCost' => $totalItemCost,
                    'totalQuantity' => $packageCount,
                );

                $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);
                if (!empty($shippingMethodCharges)) {
                    $shippingMethodData = $this->getShippingmethods($shippingMethodCharges, $request['viewCurrency']);
                }
                else {
                    $message = "Sorry. An online quote is not available for the selected locations. Please contact us at contact@shoptomydoor.com for details of cost to this location.";
                }
            }
        }

        if (!empty($shippingMethodData)) {

            return response()->json([
            'status' => '200',
            'data' => $shippingMethodData,
            'message' => 'success'
            ], 200);

        } else {
            return response()->json([
            'status' => '200',
            'data' => null,
            'message' => 'success but no data found'
            ], 200);
        }


    }

    /**
     * Method for get auto quote
     * @param Request $request
     * @return array
     */
    public function fetchautoquote_mobile(Request $request) {
        $message = "";
        $shippingCost = '0.00';
        $pickupCost = '0.00';
        $totalCost = '0.00';
        $itemPrice = $request->price;
        $warehouseId = $request->warehouse;
        $viewCurrency = $request->viewCurrency;
        $retunQuote = array();
        if($itemPrice > 0)
        {
            $param = array(
                'fromCountry' => $request->countryId,
                'fromState' => $request->stateId,
                'fromCity' => $request->cityId,
                'toCountry' => $request->tocountryId,
                'toState' => $request->tostateId,
                'toCity' => $request->tocityId
            );
            $autoData = array('make' => $request->makeId, 'model' => $request->modelId);
            $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
            if (!empty($shippingCostsArr)) {
                $shippingCost = $shippingCostsArr['totalShippingCost'];
                $insuranceCost = $shippingCostsArr['insurance'];
            }
            $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $request->cityId);
            $totalCost = round(($pickupCost + $shippingCost), 2);

            if($shippingCost!='0.00' && $pickupCost!='0.00')
            {
                $defaultCurrencyCode = customhelper::getCurrencySymbolCode('', true);
                if ($viewCurrency == "") {
                    $exchangeRate = '1';
                    $currencyCode = $defaultCurrencyCode;
                } else {
                    $currencyCode = $viewCurrency;
                    $exchangeRate = \App\Model\Currency::currencyExchangeRate($defaultCurrencyCode, $currencyCode);
                }

                $retunQuote['shippingCost'] = customhelper::getCurrencySymbolFormat(round(($shippingCost * $exchangeRate), 2), $currencyCode);
                $retunQuote['pickupCost'] = customhelper::getCurrencySymbolFormat(round(($pickupCost * $exchangeRate), 2), $currencyCode);
                $retunQuote['totalCost'] = customhelper::getCurrencySymbolFormat(round(($totalCost * $exchangeRate), 2), $currencyCode);
            }
            else{
                $message = "Sorry. An online quote is not available for the selected locations. Please contact us at contact@shoptomydoor.com for details of cost to this location.";
            }
        }
        else {
            $message = "Enter price of car correctly";
        }

        if (!empty($retunQuote)) {
            return response()->json([
                'status' => '200',
                'data' => $retunQuote,
                'message' => 'success'
                ], 200);

        } else {
            return response()->json([
                'status' => '200',
                'data' => null,
                'message' => $message
                ], 200);

        }
    }

    /**
     * Method for get item quote
     * @param Request $request
     * @return array
     */
    public function fetchitemquote_mobile(Request $request) {

        $totalItemCost = $totalWeight = $itemCount = $qtyDeclareValError = 0;
        $shippingMethodCharges = array();
        $chargeableWeightFactorKg = \App\Model\Generalsettings::where('settingsKey', 'charge_weight_factor')->first();
        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

        if (!empty($chargeableWeightFactorKg))
            $chargeableWeightFactorKgVal = $chargeableWeightFactorKg->settingsValue;
        if (!empty($chargeableWeightFactor))
            $chargeableWeightFactorVal = $chargeableWeightFactor->settingsValue;

        if (!empty(json_decode($request->items))) {

            foreach(json_decode($request->items) as $eachItem) {
                if($eachItem->quantity > 0 && $eachItem->declaredValue>0)
                {
                    $itemCount +=$eachItem->quantity;
                    $productId = $eachItem->productId;
                    $productInfo = \App\Model\Siteproduct::find($productId);
                    if (!empty($productInfo)) {
                        $totalWeight += round(($productInfo->weight * $eachItem->quantity) * 2);
                    }
                    $totalItemCost += round(($eachItem->declaredValue * $eachItem->quantity), 2);
                }
                else
                {
                    $qtyDeclareValError = 1;
                    $message = "Please enter Quantity/DeclaredValue correctly";
                    break;
                }
            }
            if($qtyDeclareValError == 0)
            {
                $param = array(

                    'fromCountry' => $request->fromCountryId,
                    'fromState' => $request->fromStateId,
                    'fromCity' => $request->fromCityId,
                    'toCountry' => $request->toCountryId,
                    'toState' => $request->toStateId,
                    'toCity' => $request->toCityId,
                    'totalWeight' => $totalWeight,
                    'totalProcurementCost' => $totalItemCost,
                    'totalQuantity' => $itemCount,
                );
                //print_r($param);exit;
                $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);
                if (!empty($shippingMethodCharges)) {
                    $shippingMethodData = $this->getShippingmethods($shippingMethodCharges, $request->viewCurrency);
                }
            }

            if (!empty($shippingMethodData)) {

                return response()->json([
                'status' => '200',
                'data' => $shippingMethodData,
                'message' => 'success'
                ], 200);

            } else {
                return response()->json([
                'status' => '200',
                'data' => null,
                'message' => 'success but no data found'
                ], 200);
            }
        }
    }

    public function getShippingmethods($shippingMethodCharges, $viewCurrency = "") {
        $shippingMethodData = array();
        $defaultCurrencyCode = customhelper::getCurrencySymbolCode('', true);
        if ($viewCurrency == "") {
            $exchangeRate = '1';
            $currencyCode = $defaultCurrencyCode;
        } else {
            $currencyCode = $viewCurrency;
            $exchangeRate = \App\Model\Currency::currencyExchangeRate($defaultCurrencyCode, $currencyCode);
        }
        //echo $exchangeRate.' '.$currencyCode;exit;
        foreach ($shippingMethodCharges as $key => $row) {
            foreach ($row as $field => $value) {
                if ($field == 'companyLogo') {
                    $shippingMethodData[$key][$field] = asset('uploads/shipping/' . $value);
                } else if ($field == 'shippingCost' || $field == 'duty' || $field == 'clearing' || $field == 'totalShippingCost') {
                    if ($field == 'clearing') {
                        $clearingDuty = $value;
                    } else if ($field == 'duty') {
                        $shippingMethodData[$key]['clearingDuty'] = customhelper::getCurrencySymbolFormat(round((($value + $clearingDuty) * $exchangeRate), 2), $currencyCode);
                        $shippingMethodData[$key]['isDutyCharged'] = !empty($value) ? true : false;
                        $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat(round(($value * $exchangeRate), 2), $currencyCode);
                    } else
                        $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat(round(($value * $exchangeRate), 2), $currencyCode);
                } else if ($field == 'days') {
                    $shippingMethodData[$key][$field] = $value;
                    $shippingMethodData[$key]['estimatedDeliveryDate'] = \Carbon\Carbon::now()->addDays($value)->format('D, M dS Y');
                } else {
                    $shippingMethodData[$key][$field] = $value;
                }
            }
        }

        return $shippingMethodData;
    }

    /*added to bag*/
    public function updateusercart_mobile(Request $request) {
        if(empty($request->type)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);
        } else if((!empty($request->type) && in_array($request->type,['autoparts','shopforme'])) && (empty($request->userId) || empty($request->warehouseId) || empty($request->urgent) || empty($request->storeId) || empty($request->websiteUrl) || empty($request->itemName) || empty($request->itemPrice) || empty($request->itemQuantity))){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);
        } else if((!empty($request->type) && $request->type=='buy_a_car') && (empty($request->userId) || empty($request->warehouseId))){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);
        } else {
            $type = $request->type;
            $userId = $request->userId; 
            if(!in_array($type, ['autoparts','shopforme','buy_a_car','ship_my_car','fillship'])){
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);
            }
            if ($type == 'autoparts') {
                $cartContent = array(
                    'storeId' => $request->storeId,
                    'siteCategoryId' => $request->siteCategoryId,
                    'siteSubCategoryId' => $request->siteSubCategoryId,
                    'siteProductId' => $request->siteProductId,
                    'websiteUrl' => $request->websiteUrl,
                    'itemName' => $request->itemName,
                    'itemDescription' => $request->itemDescription,
                    'itemYear' => $request->itemYear,
                    'itemMake' => $request->itemMake,
                    'itemModel' => $request->itemModel,
                    'itemPrice' => abs($request->itemPrice),
                    'itemQuantity' => abs($request->itemQuantity),
                    'itemShippingCost' => abs($request->itemShippingCost),
                    'itemTotalCost' => (abs($request->itemPrice) * abs($request->itemQuantity)) + abs($request->itemShippingCost),
                    'siteProductImage' => $request->siteProductImage,
                    'type' => 'autopart'
                );
            } else if ($type == 'shopforme') {
                $cartContent = array(
                    'storeId' => $request->storeId,
                    'siteCategoryId' => $request->siteCategoryId,
                    'siteSubCategoryId' => $request->siteSubCategoryId,
                    'siteProductId' => $request->siteProductId,
                    'websiteUrl' => $request->websiteUrl,
                    'itemName' => $request->itemName,
                    'options' => $request->options,
                    'itemPrice' => abs($request->itemPrice),
                    'itemQuantity' => abs($request->itemQuantity),
                    'itemShippingCost' => abs($request->itemShippingCost),
                    'itemTotalCost' => (abs($request->itemPrice) * abs($request->itemQuantity)) + abs($request->itemShippingCost),
                    'siteProductImage' => $request->siteProductImage,
                    'type' => $type
                );
            } else if ($type == 'buy_a_car') {
                $cartContent = (array) $request->all();
                $cartContent['price'] = abs($cartContent['price']);
                $cartContent['websiteName'] = \App\Model\Autowebsite::find($cartContent['website'])->name;
                $cartContent['makeName'] = \App\Model\Automake::find($cartContent['make'])->name;
                $cartContent['modelName'] = \App\Model\Automodel::find($cartContent['model'])->name;
                $cartContent['countryName'] = Country::find($cartContent['fromCountry'])->name;
                $cartContent['stateName'] = State::find($cartContent['fromState'])->name;
                $cartContent['cityName'] = City::find($cartContent['fromCity'])->name;
                $cartContent['websiteName'] = \App\Model\Autowebsite::find($cartContent['website'])->name;
                $cartContent['type'] = $type;
            } else if ($type == 'ship_my_car') {
                $cartContent = (array) $request->all();
                $cartContent['makeName'] = \App\Model\Automake::find($cartContent['makeId'])->name;
                $cartContent['modelName'] = \App\Model\Automodel::find($cartContent['modelId'])->name;
                $cartContent['shippingCost'] = $cartContent['shipmentCost'];
                $cartContent['carUrl'] = $cartContent['websiteLink'];
                $cartContent['countryName'] = Country::find($cartContent['pickupCountry'])->name;
                $cartContent['stateName'] = State::find($cartContent['pickupState'])->name;
                $cartContent['cityName'] = City::find($cartContent['pickupCity'])->name;
                $cartContent['shippingCountryName'] = Country::find($cartContent['destinationCountry'])->name;
                $cartContent['shippingStateName'] = State::find($cartContent['destinationState'])->name;
                $cartContent['shippingCityName'] = City::find($cartContent['destinationCity'])->name;
                $cartContent['vinnumber'] = $cartContent['vinNumber'];
                $cartContent['price'] = abs($cartContent['itemPrice']);
                $cartContent['type'] = $type;
                //$cartContent['locationTypeName'] = \App\Model\Locationtype::find($cartContent['pickupLocationType'])->name;
                //$cartContent['receiverPhone'] = '+' . $cartContent['receiverPhoneCode'] . ' ' . $cartContent['receiverPhone'];
                //$cartContent['pickupPhone'] = '+' . $cartContent['pickupPhoneCode'] . ' ' . $cartContent['pickupPhone'];

                if(!empty($request->tempCarTitleId)){
                    
                    $tempCarTitleId = $request->tempCarTitleId;
                    
                    $tempUpLoadDataTitle = \App\Model\Tempfileupload::where('userId', $userId)->where('id', $tempCarTitleId);

                    $tempUpLoadDataTitleData = $tempUpLoadDataTitle->first();
                    $imageNameTitle = !empty($tempUpLoadDataTitleData->imageName)?$tempUpLoadDataTitleData->imageName:'';
                    $imageNameTitlePath = !empty($tempUpLoadDataTitleData->itemImagePath)?$tempUpLoadDataTitleData->itemImagePath:'';

                    $destinationPathT = public_path('/uploads/auto/carpickup/'.$userId);
                    if (!file_exists($destinationPathT)) {
                        mkdir($destinationPathT, 0777);
                        chmod($destinationPathT, 0777);
                    }
                    if (!empty($imageNameTitlePath) && !empty($imageNameTitle)){ 

                        $tFrm = public_path($imageNameTitlePath);
                        $tTo = $destinationPathT.'/'.$imageNameTitle;

                        if(\File::copy($tFrm,$tTo)){


                            $cartContent['carTitle'] = $imageNameTitle;
                            
                            if ($tempUpLoadDataTitle->delete()) {

                                $file = public_path($imageNameTitlePath);
                                if (file_exists($file)) {
                                    unlink($file);
                                }

                            }

                        }
                    }
                    
                }

                if(!empty($request->tempItemImageId)){

                    $tempItemImageId = $request->tempItemImageId;
                    $tempUpLoadDataImage = \App\Model\Tempfileupload::where('userId', $userId)->where('id', $tempItemImageId);

                    $destinationPathI = public_path('/uploads/auto/carpickup/'.$userId);
                    if (!file_exists($destinationPathI)) {
                        mkdir($destinationPathI);
                        chmod($destinationPathI, 0777);
                    }

                    $tempUpLoadDataImageData = $tempUpLoadDataImage->first();
                    $imageNameTemp = !empty($tempUpLoadDataImageData->imageName)?$tempUpLoadDataImageData->imageName:'';
                    $imageNamePathTemp = !empty($tempUpLoadDataImageData->itemImagePath)?$tempUpLoadDataImageData->itemImagePath:'';

                    if (!empty($imageNamePathTemp) && !empty($imageNameTemp)){ 

                        $iFrm = public_path($imageNamePathTemp);
                        $iTo = $destinationPathI.'/'.$imageNameTemp;
                        
                        if(\File::copy($iFrm,$iTo)){


                            $cartContent['itemImage'] = $imageNameTemp;
                            
                            if ($tempUpLoadDataImage->delete()) {

                                $fileI = public_path($imageNamePathTemp);
                                if (file_exists($fileI)) {
                                    unlink($fileI);
                                }

                            }

                        }
                    }

                }
            } else if ($type == 'fillship') {
                $cartContent = array(
                    'siteCategoryId' => $request->siteCategoryId,
                    'siteSubCategoryId' => $request->siteSubCategoryId,
                    'siteProductId' => $request->siteProductId,
                    'itemPrice' => abs($request->itemPrice),
                    'itemQuantity' => abs($request->itemQuantity),
                    'itemTotalCost' => abs($request->itemPrice) * abs($request->itemQuantity),
                    'type' => 'fillship'
                );
            }
            if ($request->hasFile('itemImage')) {
                $image = $request->file('itemImage');
                $name = time() . '_' . $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/procurement/shopforme/'.$userId);
                if ($type == 'buy_a_car') {
                    $destinationPath = public_path('/uploads/auto/carpickup/' . $userId);
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath);
                        chmod($destinationPath, 0777);
                    }
                } else if ($type == 'shopforme') {
                    $destinationPath = public_path('/uploads/procurement/shopforme/' . $userId);
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath, 777);
                        chmod($destinationPath, 0777);
                    }
                } else if ($type == 'autoparts') {
                    $destinationPath = public_path('/uploads/procurement/autoparts/' . $userId);
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath);
                        chmod($destinationPath, 0777);
                    }
                } else if ($type == 'fillship') {
                    $destinationPath = public_path('/uploads/fillship/shipments/' . $userId);
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath);
                        chmod($destinationPath, 0777);
                    }
                }
                $image->move($destinationPath, $name);
                $cartContent['itemImage'] = $name;
            }


            $results = 'success';
            if ($type == 'buy_a_car') {
                if (!empty($cartContent['itemImage']))
                    $cartContent['itemImagePath'] = url('/uploads/auto/carpickup/' . $userId . '/' . $cartContent['itemImage']);
                $results = $cartContent;
            }
            else if ($type == 'ship_my_car') {
                if (!empty($cartContent['itemImage']))
                    $cartContent['itemImagePath'] = url('/uploads/auto/carpickup/' . $userId . '/' . $cartContent['itemImage']);

                $results = $cartContent;
            } else if ($type == 'shopforme') {
                if (!empty($cartContent['itemImage']))
                    $cartContent['itemImagePath'] = url('/uploads/procurement/shopforme/' . $userId . '/' . $cartContent['itemImage']);
                $results = $cartContent;
            }else if ($type == 'autoparts') {
                if (!empty($cartContent['itemImage']))
                    $cartContent['itemImagePath'] = url('/uploads/procurement/autoparts/' . $userId . '/' . $cartContent['itemImage']);
                $results = $cartContent;
            }else if ($type == 'fillship') {
                if (!empty($cartContent['itemImage']))
                    $cartContent['itemImagePath'] = url('/uploads/fillship/shipments/' . $userId . '/' . $cartContent['itemImage']);
                $results = $cartContent;
            }

            $userCart = new Usercart;
            $userCart->userId = $userId;
            $userCart->type = $type;
            $userCart->cartContent = json_encode($cartContent);
            $userCart->savedForLater = 'N';
            $userCart->createdOn = Config::get('constants.CURRENTDATE');
            if (!empty($request->warehouseId))
                $userCart->warehouseId = $request->warehouseId;

            if ($userCart->save()) {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'Item successfully added to bag'
                ], 200);

            } else {
                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'An error occured'
                    ], 200);
            }
        }
    }

    /*get user cart list*/
    public function getusercartlist_mobile(Request $request) {
        
        if(empty($request->type) || empty($request->id) || empty($request->warehouseId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {
            
            $data = array();
            $totalItemCost = $totalProcessingFee = $totalUrgentCost = $totalCost = $urgentPurchaseCostMin = 0;
            $urgentCostCharged = false;
        
            if ($request->type != 'fillship')
                $userCartList = Usercart::where('userId', $request->id)
                    ->where('savedForLater', 'N')
                    ->where('type', $request->type)
                    ->get()->toArray();
            else {
                $userCartList = Usercart::where('userId', $request->id)
                    ->where('savedForLater', 'N')
                    ->where('warehouseId', $request->warehouseId)
                    ->where('type', $request->type)
                    ->get()->toArray();
            }
            if (!empty($userCartList)) {
                foreach ($userCartList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key != 'cartContent') {
                            $data['items'][$count][$key] = $value;
                        } else {
                            $cartContent = json_decode($value);
                            if (!empty($cartContent)) {
                                foreach ($cartContent as $key => $value) {
                                    if ($key == 'itemPrice' || $key == 'itemShippingCost') {
                                        $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'itemTotalCost') {
                                        $totalItemCost += $value;
                                        $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'storeId') {
                                        if (!empty($value)) {
                                            $store = \App\Model\Stores::find($value);
                                            $data['items'][$count]['storeName'] = $store->storeName;
                                            $data['items'][$count]['storeId'] = $value;
                                        }

                                        // $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'siteProductId') {
                                        if (!empty($value)) {
                                            $product = \App\Model\Siteproduct::find($value);
                                            $data['items'][$count]['siteProductName'] = $product->productName;
                                            $data['items'][$count]['siteProductId'] = $value;
                                            $data['items'][$count]['siteProductImage'] = asset('/uploads/site_products/' . $product->image);
                                        }
                                    } else {
                                        $data['items'][$count][$key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }

                $userCartData = Usercart::select('warehouseId')->where('userId', $request->id)->where('savedForLater', 'N')->where('type', $request->type)->first();
                $warehouse = \App\Model\Warehouse::find($userCartData->warehouseId);
                if ($request->type != 'fillship') {
                    /* Calculate procurement processing fee */
                    $param = array('warehouseId' => $userCartData->warehouseId, 'totalItemCost' => $totalItemCost);
                    $totalProcessingFee = \App\Model\Procurement::calculateProcessingFee($param);
                    $totalCost = $totalItemCost + $totalProcessingFee;

                    /* Calculate urgent fee */
                    $urgentPurchaseCostMin = $warehouse['urgentPurchaseCost'];
                } else {
                    $totalCost = 0;
                }

                /* Calculate procurement urgent fee */
                if ($request->urgent == 'Y') {
                    $urgentPurchaseCost = ($warehouse['urgentPurchase'] / 100) * $totalItemCost;

                    if ($totalItemCost >= $urgentPurchaseCostMin) {
                        $totalUrgentCost = $urgentPurchaseCost;
                        $urgentCostCharged = true;
                    }

                    $totalCost = $totalCost + $totalUrgentCost;
                }

                $data['details'] = array(
                    'totalItemCost' => customhelper::getCurrencySymbolFormat($totalItemCost),
                    'totalProcessingFee' => customhelper::getCurrencySymbolFormat($totalProcessingFee),
                    'totalUrgentCost' => customhelper::getCurrencySymbolFormat($totalUrgentCost),
                    'urgentCostCharged' => $urgentCostCharged,
                    'minUrgentCost' => customhelper::getCurrencySymbolFormat($urgentPurchaseCostMin),
                    'totalCost' => customhelper::getCurrencySymbolFormat($totalCost),
                );
                $data['warehouseId'] = $userCartData->warehouseId;
                $data['warehouseCode'] = $warehouse['warehouseId'];
            }
            if(!empty($data)){
                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
                ], 200);
            } else {
                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);
            }
        }
    }

    /*save items for later use*/
    public function savecartlist_mobile(Request $request) {
        if(empty($request->items) || empty($request->userId)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);
        } else {
            foreach ($request->items as $item) {
                $userCart = Usercart::find($item['id']);
                $userCart->savedForLater = 'Y';
                $userCart->save();
            }
            return response()->json([
            'status' => '200',
            'data' =>(object)[],
            'message' => 'Saved items for later use'
            ], 200);
        } 
    }

    /*remove cart item*/
    public function removecartitem_mobile(Request $request) {
        if(empty($request->id) || empty($request->userId) || empty($request->type)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);
        } else {
            $userId = $request->userId;
            $type = $request->type;
            if(!in_array($type, ['autoparts','shopforme','buy_a_car','ship_my_car','fillship'])){
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);
            }
            $userCart = Usercart::find($request->id);
            $cartContent = !empty($userCart->cartContent)?$userCart->cartContent:'';
            if ($userCart->delete()) {
                
                if(!empty($cartContent) && !empty(json_decode($cartContent)->itemImage)) {
                    $image = json_decode($cartContent)->itemImage;
                    if ($type == 'buy_a_car' || $type == 'ship_my_car') {
                        $destinationPath = public_path('/uploads/auto/carpickup/'.$userId.'/'.$image);
                        if (!empty($image) && file_exists($destinationPath)) {
                            @chmod($destinationPath, 0777);
                            @unlink($destinationPath);
                        }
                    } else if ($type == 'shopforme') {
                        $destinationPath = public_path('/uploads/procurement/shopforme/'.$userId.'/'.$image);
                        if (!empty($image) && file_exists($destinationPath)) {
                            @chmod($destinationPath, 0777);
                            @unlink($destinationPath);
                        }
                    } else if ($type == 'autoparts') {
                        $destinationPath = public_path('/uploads/procurement/autoparts/'.$userId.'/'.$image);
                        if (!empty($image) && file_exists($destinationPath)) {
                            @chmod($destinationPath, 0777);
                            @unlink($destinationPath);
                        }

                    } else if ($type == 'fillship') {
                        $destinationPath = public_path('/uploads/fillship/shipments/'.$userId.'/'.$image);
                        if (!empty($image) && file_exists($destinationPath)) {
                            @chmod($destinationPath, 0777);
                            @unlink($destinationPath);
                        }
                    }

                }

                return response()->json([
                'status' => '200',
                'data' =>(object)[],
                'message' => 'Selected Item has been removed from Bag'
                ], 200);

            } else {
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);
            }
        }
    }

    /*get save items list for later use*/
    public function getsaveforlaterlist_mobile(Request $request) {
        
        $data = array();
        if(empty($request->userId) || empty($request->currentPage)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            /*  FETCH TOTAL ITMES IN USER CART LIST */
            $totalItems = Usercart::where('userId', $request->userId)->where('type', 'shopforme')->where('savedForLater', 'Y')->count();

            /*  FETCH USER CART LIST */
            $userCartList = Usercart::where('userId', $request->userId)->where('type', 'shopforme')->where('savedForLater', 'Y')->take($this->_perPage)->skip($offset)->get();

            $userCartListArr = $userCartList->toArray();
            if (!empty($userCartListArr)) {
                foreach ($userCartListArr as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key != 'cartContent') {
                            $data['items'][$count][$key] = $value;
                        } else {
                            $cartContent = json_decode($value);
                            if (!empty($cartContent)) {
                                foreach ($cartContent as $key => $value) {
                                    if ($key == 'itemPrice' || $key == 'itemShippingCost' || $key == 'itemTotalCost') {
                                        $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'storeId') {
                                        if (!empty($value)) {
                                            $store = \App\Model\Stores::find($value);
                                            $data['items'][$count]['storeName'] = $store->storeName;
                                            $data['items'][$count]['storeId'] = $value;
                                        }

                                        //  $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'itemImage') {
                                        $itemImage = $value;
                                    } else if ($key == 'siteProductImage') {
                                        $siteProductImage = $value;
                                    } else {
                                        if (isset($itemImage) && !empty($itemImage))
                                            $data['items'][$count]['itemImage'] = $itemImage;
                                        elseif (isset($siteProductImage) && !empty($siteProductImage))
                                            $data['items'][$count]['itemImage'] = $siteProductImage;
                                        else
                                            $data['items'][$count]['itemImage'] = "";

                                        $data['items'][$count][$key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if(!empty($data)){
                $data['totalItems'] = $totalItems;
                $data['itemsPerPage'] = $this->_perPage;
                return response()->json([
                    'status' => '200',
                    'data' => $data,
                    'message' => 'success'
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success'
                ], 200);
            }
            
        }

    }

    // To fetch Auto Parts Save for Later records
    public function getautopartslist_mobile(Request $request) {
        
        if(empty($request->currentPage) || empty($request->userId)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $data = array();
            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            /*  FETCH TOTAL ITMES IN USER CART LIST */
            $totalItems = Usercart::where('userId', $request->userId)->where('type', 'autoparts')->where('savedForLater', 'Y')->count();

            /*  FETCH USER CART LIST */
            $userCartList = Usercart::where('userId', $request->userId)->where('type', 'autoparts')->where('savedForLater', 'Y')->take($this->_perPage)->skip($offset)->get();

            $userCartListArr = $userCartList->toArray();
            if (!empty($userCartListArr)) {
                foreach ($userCartListArr as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key != 'cartContent') {
                            $data['items'][$count][$key] = $value;
                        } else {
                            $cartContent = json_decode($value);
                            if (!empty($cartContent)) {
                                foreach ($cartContent as $key => $value) {
                                    if ($key == 'itemPrice' || $key == 'itemShippingCost' || $key == 'itemTotalCost') {

                                        $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'storeId') {

                                        if (!empty($value)) {
                                            $store = \App\Model\Stores::find($value);
                                            $data['items'][$count]['storeName'] = $store->storeName;
                                            $data['items'][$count]['storeId'] = $value;
                                        }

                                        $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'itemImage') {

                                        $itemImage = $value;
                                    } else if ($key == 'siteProductImage') {

                                        $siteProductImage = $value;
                                    } else {

                                        if (isset($itemImage) && !empty($itemImage))
                                            $data['items'][$count]['itemImage'] = $itemImage;
                                        elseif (isset($siteProductImage) && !empty($siteProductImage))
                                            $data['items'][$count]['itemImage'] = $siteProductImage;
                                        else
                                            $data['items'][$count]['itemImage'] = "";

                                        $data['items'][$count][$key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!empty($data)){
                $data['totalItems'] = $totalItems;
                $data['itemsPerPage'] = $this->_perPage;
                return response()->json([
                    'status' => '200',
                    'data' => $data,
                    'message' => 'success'
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success'
                ], 200);
            }
        }
    }

    /*move to cart*/
    public function movetousercart_mobile(Request $request) {

        if(empty($request->itemIds) || empty($request->userId)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            if (!empty($request->itemIds)) {
                foreach ($request->itemIds as $itemId) {
                    $userCart = Usercart::find($itemId);
                    $userCart->savedForLater = 'N';
                    $userCart->save();
                }
                return response()->json([
                'status' => '200',
                'data' =>(object)[],
                'message' => 'Saved items moved to Bag'
                ], 200);
            } else {
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);
            }

            
            
        }
    }

    /*clear cart*/
    public function clearusercart_mobile(Request $request) {
        if(empty($request->type) || empty($request->userId)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {
            $type = $request->type;
            $userId = $request->userId;
            if(!in_array($type, ['autoparts','shopforme','buy_a_car','ship_my_car'])){
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);
            }
            
            $userCartData = Usercart::where('userId', $userId)->where('savedForLater', 'N')->where('type',$type)->get()->toArray();
            /* DELETE USER CART ITEMS */
            $userCart = Usercart::where('userId', $userId)->where('savedForLater', 'N')->where('type', $type)->delete();
            if($userCart){
                if(!empty($userCartData)){
                    foreach ($userCartData as $key => $value) {
                        
                        $cartContent = !empty($value['cartContent'])?$value['cartContent']:'';
                        if(!empty($cartContent) && !empty(json_decode($cartContent)->itemImage)) {
                            $image = json_decode($cartContent)->itemImage;
                            if ($type == 'buy_a_car' || $type == 'ship_my_car') {
                                $destinationPath = public_path('/uploads/auto/carpickup/'.$userId.'/'.$image);
                                if (!empty($image) && file_exists($destinationPath)) {
                                    unlink($destinationPath);
                                }
                            } else if ($type == 'shopforme') {
                                $destinationPath = public_path('/uploads/procurement/shopforme/'.$userId.'/'.$image);
                                if (!empty($image) && file_exists($destinationPath)) {
                                    unlink($destinationPath);
                                }
                            } else if ($type == 'autoparts') {
                                $destinationPath = public_path('/uploads/procurement/autoparts/'.$userId.'/'.$image);
                                if (!empty($image) && file_exists($destinationPath)) {
                                    unlink($destinationPath);
                                }
                            }
                        }
                    }
                }

                return response()->json([
                'status' => '200',
                'data' =>(object)[],
                'message' => 'All items are removed from your cart'
                ], 200);

            } else {
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);
            }
        } 

    }

    public function updatecartcurrency_mobile(Request $request) {

        if(empty($request->toCurrency) || empty($request->fromCurrency) || empty($request->userId) || empty($request->usercart)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $currencyCode = $request->toCurrency;
            $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
            $exchangeRate = \App\Model\Currency::currencyExchangeRate($request->fromCurrency, $currencyCode);
            $userCartData = $request->usercart;
            $userId = $request->userId;
            $type = $userCartData['type'];
            $userCartData['currencyCode'] = $currencyCode;
            $userCartData['exchangeRate'] = round($exchangeRate, 2);
            $userCartData['currencySymbol'] = $currencySymbol;
            $userCartData['isCurrencyChanged'] = 'Y';
            $res = Usercart::where('userId', $userId)->where('type', $type)->update(['cartContent' => json_encode($userCartData)]);
            
            if($res){

                return response()->json([
                'status' => '200',
                'data' => $userCartData,
                'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured while update',
                ], 200);

            }
        }
    }

    public function updatecartcontent_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->usercart)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userCartData = $request->usercart;
            $userId = $request->userId;
            $type = $userCartData['type'];
            $res = Usercart::where('userId', $userId)->where('type', $type)->update(['cartContent' => json_encode($userCartData)]);
           
            if($res){

                return response()->json([
                'status' => '200',
                'data' => $userCartData,
                'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured while update',
                ], 200);

            }

        }
    }

    public function checkautocartdata_mobile(Request $request) {

        if(empty($request->userId) || empty($request->type)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId = $request->userId;
            $type = $request->type;

            $data = Usercart::where('userId', $userId)->where('type', $type)->first();

            if (count($data) > 0) {

                $userCartData = json_decode($data->cartContent);
                return response()->json([
                'status' => '200',
                'data' => $userCartData,
                'message' => 'success',
                ], 200);

            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }
        }
    }

    public function deleteautocartdata_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->type)){
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId = $request->userId;
            $type = $request->type;
            $cart = Usercart::where('userId', $userId)->where('type', $type);
            if($cart->count()){
                $cartContent = json_decode($cart->first()->cartContent);
                if ($cart->delete()) {
                    if (isset($cartContent->itemImage)) {
                        $cartFile = url('/uploads/auto/carpickup/' . $userId . '/' . $cartContent->itemImage);
                        if (file_exists($cartFile)) {

                            unlink($cartFile);
                        }
                    }
                    if ($type == 'ship_my_car') {
                        if (isset($cartContent->carTitle)) {
                            $carTitle = url('/uploads/auto/carpickup/' . $userId . '/' . $cartContent->carTitle);
                            if (file_exists($carTitle)) {
                                unlink($carTitle);
                            }
                        }
                    }
                    return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                    ], 200);

                } else {

                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'An error occured',
                    ], 200);

                }
            } else {
                return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'An error occured to find the data',
                    ], 200);
            }
        }
    }

    /*validate e-wallet*/
    public function validateewallet_mobile(Request $request) {
        if(empty($request->ewalletId) || empty($request->amountToBePaid) || empty($request->userId)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $userEwallet = \App\Model\Ewallet::where('recipient', $request->userId)->where('ewalletId', $request->ewalletId)->first();
            if (!empty($userEwallet)) {
                if ($userEwallet->amount >= $request->amountToBePaid) {

                    return response()->json([
                    'status' => '200',
                    'data' =>(object)['id'=>$userEwallet->id],
                    'message' => 'Amount will be deducted from your wallet'
                    ], 200);

                } else {

                    return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'E-Wallet ID does not have sufficent fund for this transaction.'
                    ], 200);
                }

            } else {
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'E-Wallet ID does not exist'
                ], 200);
               
            }
        }
    }

    /*payment gateway list*/
    public function paymentgetway_mobile(Request $request) {

        $paymentGatewayList = array();       
        $getPaymentgateway = \App\Model\Paymentgateway::get()->toArray();
        if(!empty($getPaymentgateway)) {
            foreach($getPaymentgateway as $key=>$value){
                if($value['id']){
                    
                    $getPaymentgatewaysettings = \App\Model\Paymentgatewaysettings::select(['configurationKeys','configurationValues'])->where('paymentGatewayId',$value['id'])->get()->toArray();
                    if(!empty($getPaymentgatewaysettings)){
                        $paymentGatewayList[$key]['paymentGatewayName'] = $value['paymentGatewayName'];
                        $paymentGatewayList[$key]['paymentGatewayKey'] = $value['paymentGatewayKey'];
                        $paymentGatewayList[$key]['paymentGatewayId'] = $value['id'];
                        $paymentgatewaysettingsList = array();
                        foreach ($getPaymentgatewaysettings as $keyS => $valueS) {
                            $paymentgatewaysettingsList[$valueS['configurationKeys']] = $valueS['configurationValues'];
                        }
                        $paymentGatewayList[$key]['configuration'] = $paymentgatewaysettingsList;
                    }
                }
            }
        }
        if(!empty($paymentGatewayList)){
            return response()->json([
            'status' => '200',
            'data' => $paymentGatewayList,
            'message' => 'success'
            ], 200);
        } else {
            return response()->json([
            'status' => '200',
            'data' =>(object)[],
            'message' => 'success'
            ], 200);
        }
    }

    /* File upload api for temporary
    * can upload single file at a time
    */
    public function uploadfiletem_mobile(Request $request){
        
        if(empty($request->userId) || empty($request->type) || empty($request->fieldName)){
            
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $type = $request->type;
            $userId = $request->userId;
            $fieldName = $request->fieldName;
            if ($request->hasFile('imageName')) {

                $image = $request->file('imageName');
                $imageNameTitle = time() . '_' . $image->getClientOriginalName();

                if($type=='ship_my_car'){
                    $destinationPath = public_path('/uploads/temp/auto/carpickup/'.$userId);
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath, 0777);
                        chmod($destinationPath, 0777);
                    }
                   
                    $image->move($destinationPath, $imageNameTitle);
                    
                    $tempFileUpload = new \App\Model\Tempfileupload;
                    $tempFileUpload->userId = $userId;
                    $tempFileUpload->type = $type;
                    $tempFileUpload->fieldName = $fieldName;
                    $tempFileUpload->imageName = $imageNameTitle;
                    $tempFileUpload->itemImagePath = '/uploads/temp/auto/carpickup/'.$userId.'/'.$imageNameTitle;
                    $tempFileUpload->createdOn = Config::get('constants.CURRENTDATE');


                    if ($tempFileUpload->save()) {

                        $tempData = \App\Model\Tempfileupload::select('id','userId','type','fieldName','imageName as tempImageName','itemImagePath as tempItemImagePath')->where('id',$tempFileUpload->id)->first();
                        return response()->json([
                        'status' => '200',
                        'data' => (object)$tempData,
                        'message' => 'success'
                        ], 200);

                    } else {

                        return response()->json([
                            'status' => '400',
                            'data' => null,
                            'message' => 'An error occured while saving'
                            ], 200);
                    }

                } else {

                    return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'An error occured on service of this type'
                    ], 200);

                }

            } else {
                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured on file send'
                ], 200);
            }

        }
    }

    /*
    * delete temporary table data by id and user id
    */
    public function deletetempuoloaddata_mobile(Request $request) {
        
        if(empty($request->tempId) || empty($request->userId)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId = $request->userId;
            $tempId = $request->tempId;
            $tempUpLoadData = \App\Model\Tempfileupload::where('userId', $userId)->where('id', $tempId);
            if($tempUpLoadData->count()){

                $getdata = $tempUpLoadData->first();
                $imageName = $getdata->imageName;
                $itemImagePath = $getdata->itemImagePath;
                if ($tempUpLoadData->delete()) {

                    if (!empty($imageName) && !empty($itemImagePath)) {
                        $file = public_path($itemImagePath);
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }

                    return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                    ], 200);

                } else {

                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'An error occured, item not deleted',
                    ], 200);

                }

            } else {

                return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'An error occured to find the data',
                    ], 200);

            }
        }
    }

    /*
    * forcelogin for user
    */
    public function forcelogin_mobile(Request $request){

        if(empty($request->userId) || empty($request->deviceType) || empty($request->deviceId) || empty($request->password)/* || empty($request->deviceToken)*/){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
            ], 200);

        } else {

            $userId = $request->userId;
            $deviceType = $request->deviceType;
            $deviceId = $request->deviceId;
            $deviceToken = $request->deviceToken;
            $updateUserLog = \App\Model\Userlog::logoutstatus($userId);
            $insertLogRecord = \App\Model\Userlog::insertlogrecord($userId, $deviceType, $deviceId, $deviceToken, '');
            
            $userdata = array(
                'id' => $userId,
                'password' => $request->password,
                'status' => '1',
            );

            if (Auth::guard('user')->attempt($userdata)) {
                $user = auth()->guard('user')->user();
                $tokenData = $user->createToken('login');
                $token = $tokenData->accessToken;
                $hasDeliveryAddress = "0";
                $userShippingAddress = Addressbook::where('userId', $user->id)->where('deleted', '0')->where('isDefaultShipping','1')->with('country', 'state', 'city')->get();
                if($userShippingAddress->count()>0)
                    $hasDeliveryAddress = "1";

                if ($user->profileImage)
                    $user->profileImage = url('uploads/profile_image/' . $user->profileImage);
                
                return response()->json([
                        "status" => 200,
                        "data" => array(
                            "token" => $token,
                            "userId" => $user->id,
                            "name" => $user->firstName." ".$user->lastName,
                            "profileImage" => $user->profileImage,
                            "hasDeliveryAddress" => $hasDeliveryAddress,
                        ),
                        "message" => "Login successful"
                ], 200);


            } else {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'An error occured, Invalid user!'
                ], 200);

            }

        }
        
    }

    /**
     * Method used to check user login status
     * @param Request $request
     * @return json array
     */
    public function checkuserloginstatus_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->deviceType) || empty($request->deviceId)/* || empty($request->deviceToken)*/){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
            ], 200);

        } else {

            $userId = $request->userId;
            $deviceType = $request->deviceType;
            $deviceId = $request->deviceId;
            $deviceToken = $request->deviceToken;

            $isLoggedIn = \App\Model\Userlog::status($userId, $deviceType, $deviceId, $deviceToken,'');

            if($isLoggedIn == NULL){

                return response()->json([
                    'status' => '401',
                    'data' => null,
                    'message' => "You have logged out!"
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => null,
                    'message' => 'success'
                ], 200);
                

            }

        }

    }

    /**
     * Method used to send otp for modifying default shipping address
     * @return object
     */
    public function sendotpforshippingaddress_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->adressbookId) || empty($request->billingAddress)){
            
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId = $request->userId;
            $adressbookId = $request->adressbookId;

            if($request->billingAddress == 'yes'){
                // CHECK WHETHER THE ADDRESS IS DEFAULT SHIPPING/DELIVERY ADDRESS
                $isDefaultShipping = \App\Model\Addressbook::select('isDefaultShipping')->where('userId', $userId)->where("id", $adressbookId)->count();

                $isBilling = \App\Model\Addressbook::select("isDefaultShipping")->where('userId', $userId)->where("id", $adressbookId)->first();
                if($isDefaultShipping == 0 || $isBilling->isDefaultShipping == 0){
                    return response()->json([
                        'status' => '201',
                        'data' => (object)[],
                        'message' => 'success',
                    ], 200);
                }
            }

            // GENERATE THE OTP 
            $createOTP = customhelper::generateOTP();

            // GET USER DETAILS
            $user = User::select(['email', 'firstName', 'lastName', 'isdCode', 'contactNumber'])->where('id', $userId)->first();

            // SENDING EMAIL WITH DEFINED TEMPLATE
            if (!empty($user)) {
                
                $isSendMsg = '';    
                $emailTemplate = Emailtemplate::where('templateKey', 'send_otp')->first();
                $to = $user->email;
                $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
                $replace['[OTP]'] = $createOTP;
                $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
                
                //send otp to mobile
                $toMobile = trim($user->isdCode.$user->contactNumber);
                $smsTemplate = Smstemplate::where('templateKey', 'otp_for_modify_shipping_address')->first();
                if($toMobile){
                    $replaceVar['[NAME]'] = $user->firstName;
                    $replaceVar['[OTP]'] = $createOTP;
                    $isSendMsg = customhelper::sendMSG($toMobile,$replaceVar,$smsTemplate);
                }

                if($isSend && $isSendMsg){

                    // DELETE RECORD FOR THE USER IF ANY
                    $res=\App\Model\OtpShippingAddress::where('userId',$userId)->delete();

                    // INSERT RECORD FOR NEW OTP
                    $otp = new \App\Model\OtpShippingAddress;
                    $otp->userId = $userId;
                    $otp->otpcode = $createOTP;
                    $otp->save();

                    return response()->json([
                        'status' => '200',
                        'data' => (object)[],
                        'message' => 'success',
                    ], 200);


                } else {

                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'An error occured while sending otp! please try again',
                    ], 200);

                }
                
            } else {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'An error occured',
                ], 200);

            }
            
        }
        
    }

    /**
     * Method used to resend otp for modifying default shipping address
     * @return object
     */
    public function resendotpforshippingaddress_mobile(Request $request) {
        
        if(empty($request->userId)){
            
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId = $request->userId;
            
            // GENERATE THE OTP 
            $createOTP = customhelper::generateOTP();

            // GET USER DETAILS
            $user = User::select(['email', 'firstName', 'lastName', 'isdCode', 'contactNumber'])->where('id', $userId)->first();

            // SENDING EMAIL WITH DEFINED TEMPLATE
            if (!empty($user)) {

                $isSendMsg = '';
                $emailTemplate = Emailtemplate::where('templateKey', 'send_otp')->first();
                $to = $user->email;
                $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
                $replace['[OTP]'] = $createOTP;
                $isSend = customhelper::SendMail($emailTemplate, $replace, $to);

                //send otp to mobile
                $toMobile = trim($user->isdCode.$user->contactNumber);
                $smsTemplate = Smstemplate::where('templateKey', 'resend_otp_for_modify_shipping_address')->first();

                if($toMobile){
                    $replaceVar['[NAME]'] = $user->firstName;
                    $replaceVar['[OTP]'] = $createOTP;
                    $isSendMsg = customhelper::sendMSG($toMobile,$replaceVar,$smsTemplate);
                }

                if($isSend && $isSendMsg){

                    // DELETE RECORD FOR THE USER IF ANY
                    $res=\App\Model\OtpShippingAddress::where('userId',$userId)->delete();

                    // INSERT RECORD FOR NEW OTP
                    $otp = new \App\Model\OtpShippingAddress;
                    $otp->userId = $userId;
                    $otp->otpcode = $createOTP;
                    $otp->save();

                    return response()->json([
                        'status' => '200',
                        'data' => (object)[],
                        'message' => 'success',
                    ], 200);

                } else {

                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'An error occured while sending otp! please try again',
                    ], 200);

                }
                
            } else {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'An error occured',
                ], 200);

            }
            
        }
        
    }

    /**
     * Method used to check otp for shipping address modification
     * @return object
     */
    public function checkotpforshippingaddress_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->otp)){
            
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $otp = $request->otp;
            $userId = $request->userId;

            $res = \App\Model\OtpShippingAddress::where('userId',$userId)->first();
            
            // CHECKING THE OTP
            //SUCCESS
            if($res->otpcode == $otp){
                \App\Model\OtpShippingAddress::where('userId',$userId)->delete();

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);

            } else {
                // FAILED
                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Invalid OTP',
                ], 200);
            }
        }
    }

    /*get payment method list*/
    public function getpaymentmethodlist_mobile(Request $request) {

        if (!empty($request->userId)) {
            $user = \App\Model\Addressbook::select('countryId')->where('isDefaultBilling', 1)->where('userId', $request->userId)->first();
            $paymentMethodList = \App\Model\Paymentmethod::getallpaymentmethods($user->countryId);

            if(!empty($paymentMethodList)){

                return response()->json([
                    'status' => '200',
                    'data' => $paymentMethodList,
                    'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);

            }
            return json_encode($paymentMethodList);

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }
    }

    /*get payment history*/
    public function getpaymenthistory_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->currentPage) || empty($request->perPage)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {   

            $data = array();
            $page = $request->currentPage;
            $perPage = isset($request->perPage) ? $request->perPage : $this->_perPage;
            $offset = ($page - 1) * $perPage;

            /*  FETCH TOTAL ITMES IN PAYMENT LOG */
            $totalItems = \App\Model\Paymenttransaction::where('userId', $request->userId)->count();

            /*  FETCH PAYMENT DATA */
            $param = array(
                'userId' => $request->userId,
                'perPage' => $perPage,
                'offset' => $offset,
            );
            $paymentData = \App\Model\Paymenttransaction::getPaymentHistory($param)->toArray();
            if (!empty($paymentData)) {
                foreach ($paymentData as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key == 'amountPaid') {
                            if ($row['paidFor'] == 'shipacar')
                                $data[$count][$key] = customhelper::getCurrencySymbolFormat($value * $row['autoExchangeRate'], $row['autoPaidCurrencyCode']);
                            else
                                $data[$count][$key] = customhelper::getCurrencySymbolFormat($value * $row['exchangeRate'], $row['paidCurrencyCode']);
                        } elseif ($key == 'transactionOn') {
                            $data[$count][$key] = \Carbon\Carbon::parse($value)->format('m/d/Y');
                        } elseif ($key == 'paidFor') {
                            if ($value == 'shopforme' || $value == 'shopformeshipment')
                                $paidForLabel = 'Shop For Me #' . $row['paidForId'];
                            else if ($value == 'autopart' || $value == 'autopartshipment')
                                $paidForLabel = 'Auto Parts #' . $row['paidForId'];
                            else if ($value == 'buyacar')
                                $paidForLabel = 'Buy A Car For Me #' . $row['paidForId'];
                            else if ($value == 'shipacar')
                                $paidForLabel = 'Ship A Car #' . $row['paidForId'];
                            else if ($value == 'shipacar')
                                $paidForLabel = 'Ship A Car #' . $row['paidForId'];
                            else if ($value == 'fillship')
                                $paidForLabel = 'Fill and Ship #' . $row['paidForId'];
                            else
                                $paidForLabel = 'Shipment #' . $row['paidForId'];

                            $data[$count][$key] = $value;
                            $data[$count]['paidForLabel'] = $paidForLabel;
                        } else {
                            $data[$count][$key] = $value;
                        }
                    }
                }
            }

            if(!empty($data)){
                
                return response()->json([
                    'status' => '200',
                    'data' => (object)['listItem' => $data,
                    'totalItems' => $totalItems,
                    'itemsPerPage' => $perPage],
                    'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);

            }
            
        }
    }

    /*get invoice list*/
    public function getinvoicelist_mobile(Request $request) {
       
        if(empty($request->userId) || empty($request->currentPage) || empty($request->perPage)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else { 

            $data = array();
            $page = $request->currentPage;
            $perPage = isset($request->perPage) ? $request->perPage : $this->_perPage;
            $offset = ($page - 1) * $perPage;

            $user = User::find($request->userId);

            if(count($user)){
                /*  FETCH TOTAL ITMES IN PAYMENT LOG */
                $totalItems = \App\Model\Invoice::where('userEmail', $user->email)
                        ->where('invoiceType', 'invoice')
                        ->where('paymentMethodId', NULL)
                        ->where('paymentStatus', 'unpaid')
                        ->where('extraCostCharged', 'Y')
                        ->whereNotNull('extraCostCharged')
                        ->count();

                /*  FETCH PAYMENT DATA */
                $param = array(
                    'userEmail' => $user->email,
                    'perPage' => $perPage,
                    'offset' => $offset,
                );
                $invoiceData = \App\Model\Invoice::getUnpaidInvoiceList($param)->toArray();
                if (!empty($invoiceData)) {
                    foreach ($invoiceData as $count => $row) {
                        foreach ($row as $key => $value) {
                            if ($key == 'transactionOn') {
                                $data[$count][$key] = \Carbon\Carbon::parse($value)->format('m/d/Y');
                            } elseif ($key == 'type') {
                                if ($value == 'shopforme') {
                                    if (!empty($row['procurementId']))
                                        $paidForLabel = 'Shop For Me #' . $row['procurementId'];
                                    else
                                        $paidForLabel = 'Shop For Me #' . $row['shipmentId'];
                                }else if ($value == 'autopart') {
                                    if (!empty($row['procurementId']))
                                        $paidForLabel = 'Auto Parts #' . $row['procurementId'];
                                    else
                                        $paidForLabel = 'Auto Parts #' . $row['shipmentId'];
                                }else if ($value == 'buyacar')
                                    $paidForLabel = 'Buy A Car For Me #' . $row['procurementId'];
                                else if ($value == 'othershipment')
                                    $paidForLabel = 'Shipment #' . $row['shipmentId'];
                                else
                                    $paidForLabel = 'Ship A Car #' . $row['shipmentId'];

                                $data[$count][$key] = $value;
                                $data[$count]['paidForLabel'] = $paidForLabel;
                            } else {
                                $data[$count][$key] = $value;
                            }
                        }
                    }
                }

                if(!empty($data)){
                
                    return response()->json([
                        'status' => '200',
                        'data' => (object)['listItem' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $perPage],
                        'message' => 'success',
                    ], 200);

                } else {

                    return response()->json([
                        'status' => '200',
                        'data' => (object)[],
                        'message' => 'success',
                    ], 200);

                }


            } else {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Invalid user!',
                ], 200);
            }   

        }
    }

    /*get payment invoice*/
    public function getpaymentinvoice_mobile(Request $request) {

        if (!empty($request->paidForId)) {

            $paidFor = $request->paidFor;


            if ($paidFor == 'shopforme' || $paidFor == 'autopart' || $paidFor == 'buyacar') {
                $invoiceData = \App\Model\Invoice::where('procurementId', $request->paidForId)
                        ->where('type', $paidFor)
                        ->where('extraCostCharged', 'N')
                        ->where('deleted', '0')
                        ->orderby('id', 'desc')
                        ->first();
            } else {

                 if($paidFor == 'shopformeshipment')
                        $paidFor = 'shopforme';
                    elseif($paidFor == '')
                        $paidFor = 'othershipment';

                $invoiceData = \App\Model\Invoice::where('shipmentId', $request->paidForId)
                        ->where('extraCostCharged', 'N')
                        ->where('type', $paidFor)
                        ->where('deleted', '0')
                        ->orderby('id', 'desc')
                        ->first();

                $invoiceType = ($invoiceData->type == 'autoshipment') ? $invoiceData->type : 'shipment';

                $orderData = \App\Model\Order::where('shipmentId', $request->paidForId)->where('type', $invoiceType)->first();
                if (!empty($orderData))
                    $data['orderNumber'] = $orderData->orderNumber;
            }

            if (!empty($invoiceData)) {

                $data['invoice'] = $invoiceData;

                if ($invoiceData->type == 'buycarforme' && $invoiceData->invoiceType == 'invoice') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarinvoice')->with($data)->render();
                } elseif ($invoiceData->type == 'buycarforme' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'invoice') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarinvoice')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autopart')
                    $returnHTML = view('Administrator.autoparts.invoice')->with($data)->render();
                elseif ($invoiceData->type == 'shopforme')
                    $returnHTML = view('Administrator.procurement.invoice')->with($data)->render();
                elseif ($invoiceData->type == 'fillship')
                    $returnHTML = view('Administrator.fillnship.invoice')->with($data)->render();
                else
                    $returnHTML = view('Administrator.shipments.invoice')->with($data)->render();

                return response()->json([
                    'status' => '200',
                    'data' => $returnHTML,
                    'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }
    }

    /*pay invoice*/
    public function payinvoice_mobile(Request $request) {

        if (!empty($request->data)) {

            $paymentErrorMessage = '';
            $paymentStatus = 'unpaid';
            $defaultCurrencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);

            $invoiceId = $request->data['invoiceId'];
            $userId = $request->userId;

            $userData = User::find($userId);
            $invoice = \App\Model\Invoice::find($invoiceId);
            $invoiceParticulars = json_decode($invoice->invoiceParticulars, true);
            $shippingAddress = (array) $invoiceParticulars['shippingaddress'];

            if ($request->data['paymentMethodKey'] == 'wire_transfer') {
                $trasactionData = json_encode(array(
                    'poNumber' => $request->data['poNumber'],
                    'companyName' => $request->data['companyName'],
                    'buyerName' => $request->data['buyerName'],
                    'position' => $request->data['position'],
                        )
                );
            } else if ($request->data['paymentMethodKey'] == 'ewallet') { /*  PROCESS EWALLET PAYMENT */
                $eawalletid = (int) $request->data['id'];
                $userEwallet = \App\Model\Ewallet::find($eawalletid)->decrement('amount', $totalCost);

                $ewalletTransaction = new \App\Model\Ewallettransaction;
                $ewalletTransaction->userId = $userId;
                $ewalletTransaction->ewalletId = $eawalletid;
                $ewalletTransaction->amount = $invoice->extraCostAmount;
                $ewalletTransaction->transactionType = 'debit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $ewalletTransaction->save();

                $paymentStatus = 'paid';

                $transactionData = json_encode(array(
                    'ewalletId' => $request->data['ewalletId'],
                        )
                );
            } else if ($request->data['paymentMethodKey'] == 'paystack_checkout') {

                /* PROCESS PAYSTACK DATA */

                if (!empty($request->paystackData)) {
                    $checkoutReturn = \App\Model\Paymenttransaction::paystack($request->paystackCreatedReference);
                    if ($checkoutReturn) {
                        $paymentStatus = 'paid';
                        $transactionId = $request->paystackData['trans'];
                        $transactionData = json_encode($request->paystackData);
                    } else {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($request->paystackData);
                        $paymentErrorMessage = "Payment Failed";
                    }
                }
            } elseif ($request->data['paymentMethodKey'] == 'credit_debit_card') { 
                
                /*  PROCESS CREDIT CARD PAYMENT */
                $paymentMethod = \App\Model\Paymentmethod::where('paymentMethodKey', $request->data['paymentMethodKey'])->first();
                if ($paymentMethod->paymentGatewayId == 1) {
                    $checkoutData = array();
                    $checkoutData['cardNumber'] = $request->data['ccardNumber'];
                    $checkoutData['expMonth'] = $request->data['expiryMonth'];
                    $checkoutData['expYear'] = $request->data['expiryYear'];
                    $checkoutData['cardCode'] = $request->data['cvvCode'];
                    $checkoutData['customerFirstName'] = $invoice->billingName;
                    $checkoutData['customerLastName'] = '';
                    $checkoutData['customerAddress'] = $invoice->billingAddress;
                    $checkoutData['customerCity'] = $invoice->billingCity;
                    $checkoutData['customerState'] = $invoice->billingState;
                    $checkoutData['customerCountry'] = $invoice->billingCountry;
                    $checkoutData['customerZip'] = $invoice->billingZipcode;
                    $checkoutData['amount'] = $invoice->extraCostAmount;
                    $checkoutData['defaultCurrency'] = $defaultCurrencyCode;


                    $checkoutReturn = \App\Model\Paymenttransaction::paypaypalpro($checkoutData);

                    if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                        $paymentStatus = 'paid';
                        $transactionId = $checkoutReturn['TRANSACTIONID'];
                        $transactionData = json_encode($checkoutReturn);
                    } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($checkoutReturn);
                        $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    } else if ($checkoutReturn == 'error') {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = "Payment Error";
                    }
                } else {

                    $checkoutData['cardNumber'] = $request->data['ccardNumber'];
                    $checkoutData['expMonth'] = $request->data['expiryMonth'];
                    $checkoutData['expYear'] = $request->data['expiryYear'];
                    $checkoutData['cardCode'] = $request->data['cvvCode'];
                    $checkoutData['customerFirstName'] = $invoice->billingName;
                    $checkoutData['customerLastName'] = "";
                    $checkoutData['customerAddress'] = $invoice->billingAddress;
                    $checkoutData['customerCity'] = $invoice->billingCity;
                    $checkoutData['customerState'] = $invoice->billingState;
                    $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                    $checkoutData['customerShippingAddress'] = $shippingAddress['toAddress'];
                    $checkoutData['customerShippingCity'] = $shippingAddress['toCity'];
                    $checkoutData['customerShippingState'] = $shippingAddress['toState'];
                    $checkoutData['customerShippingCountry'] = $shippingAddress['toCountry'];
                    $checkoutData['customerShippingZip'] = $shippingAddress['toZipCode'];
                    $checkoutData['amount'] = $invoice->extraCostAmount;
                    $checkoutData['defaultCurrency'] = $defaultCurrencyCode;
                    $checkoutData['shippingFirstName'] = $shippingAddress['toName'];
                    $checkoutData['shippingLastName'] = "";

                    $checkoutReturn = \App\Model\Paymenttransaction::payauthorizedotnet($checkoutData);

                    if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                        $paymentStatus = 'paid';
                        $transactionId = $checkoutReturn['transactionResponse']['transId'];
                        $transactionData = json_encode($checkoutReturn['transactionResponse']);
                    } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);
                        if (isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                        else
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                    }
                }
            } else if ($request->data['paymentMethodKey'] == 'paypalstandard') {
                if (!empty($request->paypalTransaction)) {
                    $paymentStatus = 'paid';
                    $transactionId = $request->paypalTransaction['orderID'];
                    $transactionData = json_encode($request->paypalTransaction);
                }
            } else if($request->data['paymentMethodKey'] == 'payeezy') {
                if(!empty($request->payeezyData)) {
                    $checkoutData['amount'] = $request->payeezyData["paidAmount"];
                    $checkoutData['method'] = $request->payeezyData["paymentType"];
                    $checkoutData['currency_code'] = "USD";
                    $checkoutData['type'] = $request->payeezyData["cardType"];
                    $checkoutData['cardholder_name'] = $request->payeezyData["cardHolderName"];
                    $checkoutData['card_number'] = $request->payeezyData['ccardNumber'];
                    $checkoutData['exp_date'] = $request->payeezyData['expiryMonth'].substr($request->payeezyData['expiryYear'],2);
                    $checkoutData['cvv'] = $request->payeezyData['cvvCode'];
                    $checkoutReturn = \App\Model\Paymenttransaction::processpayeezy($checkoutData);

                    if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved')
                    {
                        $paymentStatus = 'paid';
                        $paymentMode = 'online';
                        $transactionId = $checkoutReturn['transaction_id'];
                        $transactionData = json_encode($checkoutReturn);
                    }
                    else if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed')
                    {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = 'Payment failed. ' .$checkoutReturn['Error']['messages'][0]['description'];
                    }
                    else
                    {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                    }

                } else {
                    $paymentStatus = 'error';
                }
            }

            $invoiceParticulars['payment'] = array(
                'paymentMethodId' => $request->data['paymentMethodId'],
                'paymentMethodName' => $request->data['paymentMethodName'],
            );
            $invoiceParticulars['shipment']['totalTax'] = $request->data['taxAmount'];
            $invoiceParticulars['shipment']['totalCost'] = $request->data['paidAmount'];
            if ($paymentStatus == 'paid')
                $invoice->invoiceType = 'receipt';

            $invoice->invoiceParticulars = json_encode($invoiceParticulars);
            $invoice->paymentMethodId = $request->data['paymentMethodId'];
            $invoice->paymentStatus = $paymentStatus;
            $invoice->totalBillingAmount = $request->data['paidAmount'];


            if ($paymentStatus == 'paid') {

                if ($invoice->save()) {

                    $paymentTransaction = new \App\Model\Paymenttransaction;
                    $paymentTransaction->userId = $userId;
                    $paymentTransaction->paymentMethodId = $request->data['paymentMethodId'];
                    $paymentTransaction->paidFor = $invoice->type;
                    if ($invoice->type == 'othershipment')
                        $paymentTransaction->paidForId = $invoice->shipmentId;
                    else
                        $paymentTransaction->paidForId = $invoice->procurementId;
                    
                    $paymentTransaction->invoiceId = $invoice->id;

                    $paymentTransaction->amountPaid = $request->data['paidAmount'];
                    $paymentTransaction->status = $paymentStatus;
                    if (!empty($transactionData))
                        $paymentTransaction->transactionData = $transactionData;
                    if (!empty($transactionId))
                        $paymentTransaction->transactionId = $transactionId;
                    $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                    $paymentTransaction->save();

                    $invoiceUniqueId = $invoice->invoiceUniqueId;
                    $fileName = "Receipt_" . $invoiceUniqueId . ".pdf";

                    $data['invoice'] = $invoice;
                    $data['pageTitle'] = "Print Invoice";
                    if ($invoice->type == 'shopforme')
                        PDF::loadView('Administrator.procurement.extrainvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                    elseif ($invoice->type == 'autopart')
                        PDF::loadView('Administrator.autoparts.extrainvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                    else
                        PDF::loadView('Administrator.shipments.printinvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');

                    $to = $invoice->billingEmail;
                    Mail::send(['html' => 'mail'], ['content' => strtoupper($invoice->invoiceType) . ' for #' . $invoiceUniqueId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                        $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                        $message->subject("$invoiceUniqueId - Invoice");
                        $message->to($to);
                        $message->attach(public_path('exports/invoice/' . $fileName));
                    });

                    return response()->json([
                        'status' => '200',
                        'data' => (object)[],
                        'message' => 'success',
                    ], 200);


                } else {

                    return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' =>"An error occured in saving!",
                    ], 200);

                }

            } else {

                $paymentTransaction = new \App\Model\Paymenttransaction;
                $paymentTransaction->userId = $userId;
                $paymentTransaction->paymentMethodId = $request->data['paymentMethodId'];
                $paymentTransaction->paidFor = $invoice->type;
                $paymentTransaction->amountPaid = $request->data['paidAmount'];
                $paymentTransaction->status = $paymentStatus;
                if (!empty($transactionErrorMsg))
                    $paymentTransaction->errorMsg = $transactionErrorMsg;
                $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $paymentTransaction->save();

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => $paymentErrorMessage,
                ], 200);

            }

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }
    }

    /*get invoice*/
    public function getinvoice_mobile(Request $request) {
        
        if (!empty($request->id)) {

            $invoice = \App\Model\Invoice::where('id', $request->id)->where('extraCostCharged', 'Y')->first();

            if(!empty($invoice)){

                return response()->json([
                    'status' => '200',
                    'data' => $invoice,
                    'message' => 'success',
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }

        } else {

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        }
    }

}
