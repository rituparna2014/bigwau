<?php

namespace App\Http\Controllers\Api\mobile;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\customhelper;
use App\Model\Contactus;
use Config;

class GetquoteController extends Controller {

    public function fetchquote_mobile(Request $request) {

        $price = $chargeable_weight = $totalItemCost = $dim_weight = $packageCount = $heightWeightError = 0;
        $totalWeight = 0;
        $shippingMethodData = array();
        $message = "";

        $chargeableWeightFactorKg = \App\Model\Generalsettings::where('settingsKey', 'charge_weight_factor')->first();
        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

        if (!empty($chargeableWeightFactorKg))
            $chargeableWeightFactorKgVal = $chargeableWeightFactorKg->settingsValue;
        if (!empty($chargeableWeightFactor))
            $chargeableWeightFactorVal = $chargeableWeightFactor->settingsValue;

        if (!empty($request['packages'])) {
            
            foreach($request['packages'] as $eachPackage) {
                $packageCount ++;
                $length = abs($eachPackage['length']);
                $width = abs($eachPackage['width']);
                $height = abs($eachPackage['height']);
                $weight = abs($eachPackage['weight']);
                $unit = $eachPackage['unit'];
                $declaredValue = abs($eachPackage['declaredValue']);

                if ($length > 0 && $width > 0 && $height > 0
                ) {
                    $weight = max($weight, ($length * $width * $height / (($unit == 'K') ? $chargeableWeightFactorKgVal : $chargeableWeightFactorVal)));
                }
                else
                {
                    $message = "Please enter height/weight/length correctly";
                    $heightWeightError = 1;
                    break;
                }
                if ($unit == 'K') {
                    $weight *= 2.2;
                }
                $totalWeight += round($weight, 2);
                $totalItemCost += $declaredValue;
            }
            if($heightWeightError == 0)
            {
                $param = array(
                    'fromCountry' => $request['from']['countryId'],
                    'fromState' => $request['from']['stateId'],
                    'fromCity' => $request['from']['cityId'],
                    'toCountry' => $request['to']['countryId'],
                    'toState' => $request['to']['stateId'],
                    'toCity' => $request['to']['cityId'],
                    'totalWeight' => $totalWeight,
                    'totalProcurementCost' => $totalItemCost,
                    'totalQuantity' => $packageCount,
                );

                $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);
                if (!empty($shippingMethodCharges)) {
                    $shippingMethodData = $this->getShippingmethods($shippingMethodCharges, $request['viewCurrency']);
                }
                else {
                    $message = "Sorry. An online quote is not available for the selected locations. Please contact us at contact@shoptomydoor.com for details of cost to this location.";
                }
            }
                
        }

        if (!empty($shippingMethodData)) {

            return response()->json([
                'status' => '200',
                'data' => $shippingMethodData,
                'message' => 'success'
                    ], 200);

        } else {
            
            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => $message
                ], 200);
        }
    }

    public function fetchitemquote_mobile(Request $request) {
        
        $totalItemCost = $totalWeight = $itemCount = $qtyDeclareValError = 0;
        $shippingMethodCharges = array();
        $chargeableWeightFactorKg = \App\Model\Generalsettings::where('settingsKey', 'charge_weight_factor')->first();
        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

        if (!empty($chargeableWeightFactorKg))
            $chargeableWeightFactorKgVal = $chargeableWeightFactorKg->settingsValue;
        if (!empty($chargeableWeightFactor))
            $chargeableWeightFactorVal = $chargeableWeightFactor->settingsValue;

        if (!empty($request['items'])) {

            foreach($request['items'] as $eachItem) {
                $eachItem['quantity'] = abs($eachItem['quantity']);
                $eachItem['declaredValue'] = abs($eachItem['declaredValue']);
                
                if ($eachItem['quantity'] > 0 && $eachItem['declaredValue'] > 0) {
                    $itemCount +=$eachItem['quantity'];
                    $productId = $eachItem['productId'];
                    $productInfo = \App\Model\Siteproduct::find($productId);
                    if (!empty($productInfo)) {
                        $totalWeight += round(($productInfo->weight * $eachItem['quantity']), 2);
                    }
                    $totalItemCost += round(($eachItem['declaredValue'] * $eachItem['quantity']), 2);
                }
                else
                {
                    $qtyDeclareValError = 1;
                    $message = "Please enter Quantity/DeclaredValue correctly";
                    break;
                }
            }
            
            if($qtyDeclareValError == 0)
            {
                $param = array(
                    'fromCountry' => $request['from']['countryId'],
                    'fromState' => $request['from']['stateId'],
                    'fromCity' => $request['from']['cityId'],
                    'toCountry' => $request['to']['countryId'],
                    'toState' => $request['to']['stateId'],
                    'toCity' => $request['to']['cityId'],
                    'totalWeight' => $totalWeight,
                    'totalProcurementCost' => $totalItemCost,
                    'totalQuantity' => $itemCount,
                );
                $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);
                if (!empty($shippingMethodCharges)) {
                    $shippingMethodData = $this->getShippingmethods($shippingMethodCharges, $request['viewCurrency']);
                }
                else {
                    $message = "Sorry. An online quote is not available for the selected locations. Please contact us at contact@shoptomydoor.com for details of cost to this location.";
                }
            }

            if (!empty($shippingMethodData)) {

                return response()->json([
                'status' => '200',
                'data' => $shippingMethodData,
                'message' => 'success'
                    ], 200);

            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => $message
                ], 200);

            }

        } else {

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        }
    }

    public function fetchautoquote_mobile(Request $request) {
        
        if(empty($request->warehouse) || /*empty($request->viewCurrency) ||*/ empty($request->countryId) || empty($request->stateId) || empty($request->cityId) || empty($request->tocountryId) || empty($request->tostateId) || empty($request->tocityId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);


        } else {

            $message = "";
            $shippingCost = '0.00';
            $pickupCost = '0.00';
            $totalCost = '0.00';
            $itemPrice = '0.00';
            $warehouseId = $request->warehouse;
            $viewCurrency = $request->viewCurrency;
            $retunQuote = array();
            $param = array(
                'fromCountry' => $request->countryId,
                'fromState' => $request->stateId,
                'fromCity' => $request->cityId,
                'toCountry' => $request->tocountryId,
                'toState' => $request->tostateId,
                'toCity' => $request->tocityId
            );
            $autoData = array('make' => $request->makeId, 'model' => $request->modelId);
            $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
            if (!empty($shippingCostsArr)) {
                $shippingCost = $shippingCostsArr['totalShippingCost'];
                $insuranceCost = $shippingCostsArr['insurance'];
            }
            $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $request->cityId);
            $totalCost = round(($pickupCost + $shippingCost), 2);

            if($shippingCost!='0.00' && $pickupCost!='0.00')
            {
                $defaultCurrencyCode = customhelper::getCurrencySymbolCode('', true);
                if ($viewCurrency == "") {
                    $exchangeRate = '1';
                    $currencyCode = $defaultCurrencyCode;
                } else {
                    $currencyCode = $viewCurrency;
                    $exchangeRate = \App\Model\Currency::currencyExchangeRate($defaultCurrencyCode, $currencyCode);
                }

                $retunQuote['shippingCost'] = customhelper::getCurrencySymbolFormat(round(($shippingCost * $exchangeRate), 2), $currencyCode);
                $retunQuote['pickupCost'] = customhelper::getCurrencySymbolFormat(round(($pickupCost * $exchangeRate), 2), $currencyCode);
                $retunQuote['totalCost'] = customhelper::getCurrencySymbolFormat(round(($totalCost * $exchangeRate), 2), $currencyCode);
            }
            else{
                $message = "Sorry. An online quote is not available for the selected locations. Please contact us at contact@shoptomydoor.com for details of cost to this location.";
            }


            if (!empty($retunQuote)) {

                return response()->json([
                'status' => '200',
                'data' => $retunQuote,
                'message' => 'success'
                    ], 200);

            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => $message
                ], 200);
                
            }
        }
    }

    public function getShippingmethods($shippingMethodCharges, $viewCurrency = "") {
        $shippingMethodData = array();
        $defaultCurrencyCode = customhelper::getCurrencySymbolCode('', true);
        if ($viewCurrency == "") {
            $exchangeRate = '1';
            $currencyCode = $defaultCurrencyCode;
        } else {
            $currencyCode = $viewCurrency;
            $exchangeRate = \App\Model\Currency::currencyExchangeRate($defaultCurrencyCode, $currencyCode);
        }
        //echo $exchangeRate.' '.$currencyCode;exit;
        foreach ($shippingMethodCharges as $key => $row) {
            foreach ($row as $field => $value) {
                if ($field == 'companyLogo') {
                    $shippingMethodData[$key][$field] = asset('uploads/shipping/' . $value);
                } else if ($field == 'shippingCost' || $field == 'duty' || $field == 'clearing' || $field == 'totalShippingCost') {
                    if ($field == 'clearing') {
                        $clearingDuty = $value;
                    } else if ($field == 'duty') {
                        $shippingMethodData[$key]['clearingDuty'] = customhelper::getCurrencySymbolFormat(round((($value + $clearingDuty) * $exchangeRate), 2), $currencyCode);
                        $shippingMethodData[$key]['isDutyCharged'] = !empty($value) ? true : false;
                        $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat(round(($value * $exchangeRate), 2), $currencyCode);
                    } else
                        $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat(round(($value * $exchangeRate), 2), $currencyCode);
                } else if ($field == 'days') {
                    $shippingMethodData[$key][$field] = $value;
                    $shippingMethodData[$key]['estimatedDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($value)->format('D, M dS Y');
                } else {
                    if($field!='clearingDuty')
                        $shippingMethodData[$key][$field] = $value;
                }
            }
        }

        return $shippingMethodData;
    }

    public function submitContact_mobile(Request $request)
    {       
        
        if(empty($request->contactName) || empty($request->contactPhone) || empty($request->contactEmail)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);


        } else {

            $dataObj = new Contactus;
            $makeName = \App\Model\Automake::find($request['makeId'])->name;
            $modelName = \App\Model\Automodel::find($request['modelId'])->name;
            $message = "<p>User ".$request['contactName']." has requested a clearing quote for Auto of Make ".$makeName.", Model ".$modelName.", Year ".$request['year']."</p>";

            $sourceCountry = \App\Model\Country::find($request['countryId'])->name;
            $sourceState = \App\Model\State::find($request['stateId'])->name;
            $sourceCity = \App\Model\City::find($request['cityId'])->name;
            $destinationCountry = \App\Model\Country::find($request['tocountryId'])->name;
            $destinationState = \App\Model\State::find($request['tostateId'])->name;
            $destinationCity = \App\Model\City::find($request['tocityId'])->name;
            $message .= "<p>Form ".$sourceCountry.",".$sourceState.",".$sourceCity." To ".$destinationCountry.",".$destinationState.",".$destinationCity."</p>";
            $message .= "<p> Contact number of user is <strong>".$request['contactPhone']."</strong> and contact email is <strong>".$request['contactEmail']."</strong></p>";
            $dataObj->name = $request->contactName;
            $dataObj->phone = $request->contactPhone;
            $dataObj->email = $request->contactEmail;
            $dataObj->status = "1";
            $dataObj->reason_id = "1";
            $dataObj->postedOn = Config::get('constants.CURRENTDATE');
            $dataObj->subject = "Auto clearing quote request";
            $dataObj->message = $message;
            
            $to = Config::get('constants.EmailSettings.FROMEMAIL');
            $from = $request->contactEmail;
            Mail::send(['html' => 'mail'], ['content' => $message], function ($message) use($to,$from) {
                $message->from($from, $from);
                $message->subject("Auto clearing quote request");
                $message->to($to);
            });
             if ($dataObj->save()) {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'Data has saved successfully'
                    ], 200);

            } else {

                return response()->json([
                'status' => '400',
                'data' => (object)[],
                'message' => 'An error occured! Please try again'
                    ], 200);
            }
        }

    }

    public function saveitemquotecontact_mobile(Request $request) {

        if(empty($request->itemContactEmail) || empty($request->items)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);


        } else {

            $dataObj = new Contactus;
            $itemDescriptions = array();
            $itemurls = array();
            $message = "<p>User ".$request['itemContactName']." has requested a quote for following items:</p>";
            foreach($request['items'] as $eachItem) {
                $message .= "<p>Item Url : ".$eachItem['itemUrl'].", Item Description : ".$eachItem['itemDescription'].", Item Quantity : ".$eachItem['quantity']."</p>";
            }
            
            $sourceCountry = \App\Model\Country::find($request['from']['countryId'])->name;
            $sourceState = \App\Model\State::find($request['from']['stateId'])->name;
            $sourceCity = \App\Model\City::find($request['from']['cityId'])->name;
            
            $destinationCountry = \App\Model\Country::find($request['to']['countryId'])->name;
            $destinationState = \App\Model\State::find($request['to']['stateId'])->name;
            $destinationCity = \App\Model\City::find($request['to']['cityId'])->name;
            
            $message .= "Form ".$sourceCountry.",".$sourceState.",".$sourceCity." To ".$destinationCountry.",".$destinationState.",".$destinationCity;
            $message .= "<p> Contact number of user is <strong>".$request['itemContactPhone']."</strong> and contact email is <strong>".$request['itemContactEmail']."</strong></p>";
            
            $dataObj->name = $request['itemContactName'];
            $dataObj->phone = $request['itemContactPhone'];
            $dataObj->email = $request['itemContactEmail'];
            $dataObj->status = "1";
            $dataObj->reason_id = "1";
            $dataObj->postedOn = Config::get('constants.CURRENTDATE');
            $dataObj->subject = "Item quote request";
            $dataObj->message = $message;
            
            //$to = "contact@shoptomydoor.com";
            $to = Config::get('constants.EmailSettings.FROMEMAIL');
            $from = $request['itemContactEmail'];
            Mail::send(['html' => 'mail'], ['content' => $message], function ($message) use($to,$from) {
                $message->from($from, $from);
                $message->subject("Item quote request");
                $message->to($to);
            });
            
            if ($dataObj->save()) {
                
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'Data has saved successfully'
                        ], 200);

            } else {
                
                return response()->json([
                    'status' => '400',
                    'data' => (object)[],
                    'message' => 'An error occured! Please try again'
                        ], 200);
            }
        }
    }

}