<?php

namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Usercart;
use App\Model\Emailtemplate;
use App\Model\Addressbook;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Usershipmentsettings;
use App\Model\Subscriber;
use App\Model\Sitepage;
use App\Model\UserAdmin;
use Config;
use Illuminate\Support\Facades\Hash;
use Auth;
use customhelper;
use DB;
use PDF;
use Mail;
use Carbon\Carbon;
use App\Model\Smstemplate;

class UsersController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->_perPage = 10;
    }

    public function saveapiuser(Request $request) {

        $user = new User;
        $addressBook = new Addressbook;
        $error = '';
        $referralUser = array();
        //print_r($request->all()); die;

        $validator = Validator::make($request->all(), [
                    'firstName' => 'required|alpha',
                    'lastName' => 'required|alpha',
                    'password' => 'required',
                    'email' => 'required',
                    'phcode' => 'required',
                    'address' => 'required',
                    'number' => 'required',
                    'country' => 'required',
                    'city' => 'required'
        ]);

        if ($validator->fails()) {
            $dataErr = $validator->errors();
            //echo $dataErr;
            foreach ($dataErr->all() as $eacherror)
                $error .= $eacherror . ',';

            return response()->json([
                        'status' => '-2',
                        'results' => substr($error, 0, -1)
                            ], 200);
        }else{
            $validateEmail = User::where('email', $request->email)->where('deleted','0')->first();

            $validateNumber = User::where('contactNumber', ltrim($request->number, '0'))->where('deleted','0')->first();

            if(count($validateNumber)>0)
            {
                return response()->json([
                        'status' => '-2',
                        'results' => "This contact number already exist",
                            ], 200);

            }
            if(count($validateEmail)>0)
            {
                return response()->json([
                        'status' => '-2',
                        'results' => "This email address already has been taken",
                            ], 200);

            }
        }

        if ($request->referralCode != '') {
            $referralUser = User::where('referralCode', $request->referralCode)->first();
            if (count($referralUser) > 0) {
                $user->referredBy = $referralUser->id;
            } else {
                return response()->json([
                            'status' => '-3',
                            'results' => 'Invalid referral code'
                                ], 200);
            }
        }

        $user->status = '0';
        $user->password = Hash::make($request->password);
        $user->createdBy = 1;
        $user->createdOn = Config::get('constants.CURRENTDATE');
        $user->title = $request->title;
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->isdCode = $request->phcode;
        $user->contactNumber = ltrim($request->number, '0');
        $user->company = empty($request->company) ? '' : $request->company;
        $user->referralCode = User::generateReferralCode();
        $user->profileImage = 'default.jpg';
        $user->registeredBy = 'website';

        if ($user->save()) {
            $userId = $user->id;

            $addressBook->userId = $userId;
            $addressBook->title = $request->title;
            $addressBook->firstName = $request->firstName;
            $addressBook->lastName = $request->lastName;
            $addressBook->email = $request->email;
            $addressBook->address = $request->address;
            $addressBook->alternateAddress = $request->alternateAddress;
            $addressBook->cityId = $request->city;
            $addressBook->stateId = $request->state;
            $addressBook->countryId = $request->country;
            $addressBook->isdCode = $request->phcode;
            $addressBook->phone = ltrim($request->number, '0');
            $addressBook->modifiedBy = '0';
            $addressBook->modifiedOn = Config::get('constants.CURRENTDATE');
            $addressBook->isDefaultBilling = '1';

            $addressBook->save();

            /* Update unit of user while adding */
            /* $maxUnitNumber = User::where('deleted', '0')->where('status', '1')->max('unitNumber');
              if (empty($maxUnitNumber))
              $maxUnitNumber = Config::get('constants.MAXUNITNUMBER');
              else
              $maxUnitNumber = $maxUnitNumber + 1; */

            $maxUnitNumber = $userId;
            User::where('id', $userId)->update(['unitNumber' => $maxUnitNumber]);

            $activationToken = strtoupper(md5(uniqid(rand())));
            User::where('id', $userId)->update(['activationToken' => $activationToken]);
            
            if (!empty($referralUser)) {
                $emailTemplate = array();
                $to = $referralUser->email;
                $replace['[NAME]'] = $referralUser->firstName . ' ' . $referralUser->lastName;
                $replace['[CODEUSENAME]'] = $request->firstName . ' ' . $request->lastName;
                $emailTemplate = Emailtemplate::where('templateKey', 'successfull_referral')->first();
                customhelper::SendMail($emailTemplate, $replace, $to);
            }else{
                $emailTemplate = Emailtemplate::where('templateKey', 'registration_active')->first();
                $to = $user->email;
                $replace['[EMAIL]'] = $user->email;
                $replace['[PASSWORD]'] = $request->password;
                $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
                $activationLink = Config::get('constants.frontendUrl') . 'join/' . $userId . '/' . $activationToken;
                $replace['[ACTIVATION_LINK]'] = '<a href="' . $activationLink . '">Verify</a>';
                customhelper::SendMail($emailTemplate, $replace, $to);

            }
            return response()->json([
                        'status' => '1',
                        'results' => $user
                            ], 200);
        }


        return response()->json([
                    'user' => '-1',
                    'results' => $user
                        ], 200);
    }

    public function resendactivationmail(Request $request) {

        $userId = $request->userId;
        $user = User::find($userId);
        $activationToken = $user->activationToken;
        $emailTemplate = Emailtemplate::where('templateKey', 'registration_active')->first();
        $to = $user->email;
        $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
        $activationLink = Config::get('constants.frontendUrl') . 'join/' . $userId . '/' . $activationToken;
        $replace['[ACTIVATION_LINK]'] = '<a href="' . $activationLink . '">Verify</a>';
        $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
        if ($isSend) {
            return response()->json([
                        'status' => '1',
                        'results' => 'success'
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => 'error'
                            ], 200);
        }
    }

    public function updateuser($userId, Request $request) {

        $validator = Validator::make($request->all(), [
                    'firstName' => 'required|alpha',
                    'lastName' => 'required|alpha',
                    'title' => 'required',
        ]);
        if ($validator->fails())
            return response()->json([
                        'status' => '-1',
                        'results' => $validator->errors()
                            ], 200);

        $userObj = User::find($userId);

        if (isset($request->newsletterSubscribe)) {

            if ($request->newsletterSubscribe)
                $newsSubStatus = '1';
            else
                $newsSubStatus = '0';

            $newsletterData = Subscriber::where('userId', $userId)->first();
            if (count($newsletterData) > 0)
                Subscriber::where('id', $newsletterData->id)->update(['status' => $newsSubStatus]);
            else if ($request->newsletterSubscribe) {
                $newsLetterObj = new Subscriber;
                $newsLetterObj->userId = $userId;
                $newsLetterObj->userEmail = $userObj->email;
                $newsLetterObj->status = '1';
                $newsLetterObj->save();
            }
        }

        $userObj->title = $request->title;
        $userObj->firstName = $request->firstName;
        $userObj->lastName = $request->lastName;
        $userObj->company = $request->company;

        if ($request->dateOfBirth != '') {
            $userObj->dateOfBirth = date('Y-m-d', strtotime($request->dateOfBirth));
        }

        if ($request->oldPassword != '') {
            //return json_encode(array('old'=>Hash::make($request->oldPassword),'pass'=>$userObj->password));
            if (Hash::check($request->oldPassword, $userObj->password)) {
                if ($request->newPassword == $request->newRepeatPassword) {
                    $userObj->password = Hash::make($request->newPassword);
                } else {
                    return response()->json([
                                'status' => '-3',
                                'results' => 'New password and retype password did not match'
                                    ], 200);
                }
            } else {
                return response()->json([
                            'status' => '-2',
                            'results' => 'Old password did not match'
                                ], 200);
            }
        }
        if ($userObj->save()) {
            return response()->json([
                        'status' => '1',
                        'results' => $userObj
                            ], 200);
        }
    }

    public function userlogin(Request $request) {

        $userdata = array(
            'email' => $request->email,
            'password' => $request->password,
            'status' => '1',
        );
        if (Auth::guard('user')->attempt($userdata)) {

            $user = auth()->guard('user')->user();

            /* CHECK WHETHER USER IS LOGGED IN WITH ANOTHER DEVICE */

            $isLoggedIn = \App\Model\Userlog::checkstatus($user->id, 'W', '', '', $_SERVER['HTTP_USER_AGENT']);
            $msg = Config::get('constants.MessageForceLogin.Text');
            if (!empty($isLoggedIn)) {
                return response()->json([
                            'user' => '-2',
                            'token' => '-1',
                            'message' => $msg
                                ], 200);
            }


            /* INSERT LOG RECORD */
            $insertLogRecord = \App\Model\Userlog::insertlogrecord($user->id, 'W', '', '', $_SERVER['HTTP_USER_AGENT']);

            $tokenData = $user->createToken('login');
            $token = $tokenData->accessToken;

            $subscriptionData = Subscriber::where('userId', $user->id)->first();
            $subscriptionStatus = '0';
            if (!empty($subscriptionData))
                $subscriptionStatus = $subscriptionData->status;

            if ($user->profileImage)
                $user->profileImage = url('uploads/profile_image/' . $user->profileImage);
            /* $virtualTourReplay = '';
              $virtualTourTaken = \App\Model\Virtualtourlog::where('userId',$user->id)->max('logTime');
              if(!empty($virtualTourTaken))
              {
              $virtualTourReplay = date('Y-m-d h:i:s',strtotime($virtualTourTaken)+86400);
              } */

            return response()->json([
                        'user' => $user,
                        'token' => $token,
                        'tokenExpires' => Carbon::now()->addDays(1),
                        'subscription' => $subscriptionStatus,
                            ], 200);
        } else {
            return response()->json([
                        'user' => '-1',
                        'token' => '-1'
                            ], 200);
        }
    }

    /**
     * Method used to forced login
     * @param Request $request
     * @return json array
     */
    public function userloginforce(Request $request) {

        $userdata = array(
            'email' => $request->email,
            'password' => $request->password,
            'status' => '1',
        );
        if (Auth::guard('user')->attempt($userdata)) {

            $user = auth()->guard('user')->user();
            $tokenData = $user->createToken('login');
            $token = $tokenData->accessToken;

            /* UPDATE ALL LOG AS LOGGED OUT AND INSERT NEW ONE */
            $updateUserLog = \App\Model\Userlog::logoutstatus($user->id);
            $insertLogRecord = \App\Model\Userlog::insertlogrecord($user->id, 'W', '', '', $_SERVER['HTTP_USER_AGENT']);

            $subscriptionData = Subscriber::where('userId', $user->id)->first();
            $subscriptionStatus = '0';
            if (!empty($subscriptionData))
                $subscriptionStatus = $subscriptionData->status;

            if ($user->profileImage)
                $user->profileImage = url('uploads/profile_image/' . $user->profileImage);
            /* $virtualTourReplay = '';
              $virtualTourTaken = \App\Model\Virtualtourlog::where('userId',$user->id)->max('logTime');
              if(!empty($virtualTourTaken))
              {
              $virtualTourReplay = date('Y-m-d h:i:s',strtotime($virtualTourTaken)+86400);
              } */

            return response()->json([
                        'user' => $user,
                        'token' => $token,
                        'tokenExpires' => Carbon::now()->addDays(1),
                        'subscription' => $subscriptionStatus,
                            ], 200);
        } else {
            return response()->json([
                        'user' => '-1',
                        'token' => '-1'
                            ], 200);
        }
    }

    /**
     * Method used to check user login status
     * @param Request $request
     * @return json array
     */
    public function checkuserloginstatus(Request $request) {
        $userId = 0;
        $userId = $request->userId;
        $isLoggedIn = \App\Model\Userlog::status($userId, 'W', '', '', $_SERVER['HTTP_USER_AGENT']);

        if ($isLoggedIn == NULL) {
            return response()->json([
                        'user' => $userId,
                        'msg' => 'null',
                            ], 200);
        }

        return response()->json([
                    'user' => $userId,
                    'msg' => 'notnull',
                        ], 200);
    }

    public function activateuser(Request $request) {

        $id = $request->input('id');
        $token = $request->input('token');

        $user = User::where('id', $id)->where('activationToken', $token)->first();
        if (!empty($user)) {
            User::where('id', $id)->update(['status' => '2', 'activationToken' => NULL, 'verifiedCode' => NULL]);
            $emailTemplate = Emailtemplate::where('templateKey', 'successfull_activation')->first();
            $to = $user->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
            return response()->json([
                        'status' => '1',
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                            ], 200);
        }
    }

    public function userlogout(Request $request) {

        $id = $request->input('id');
        DB::table('oauth_access_tokens')->where('user_id', $id)->delete();
        $updateUserLog = \App\Model\Userlog::logoutstatus($id);
        return response()->json([
                    'status' => '1',
                        ], 200);
    }

    public function usershippingaddress($addressType = 'All', Request $request) {

        $id = $request->input('id');
        $where = '1';
        if ($addressType == 'shipping')
            $where = 'isDefaultShipping = "1"';
        else if ($addressType == 'billing')
            $where = 'isDefaultBilling = "1"';
        if ($request->input('addressId') && $request->input('addressId') != '')
            $where .= ' AND id=' . $request->input('addressId');

        $userShippingAddress = Addressbook::where('userId', $id)->where('deleted', '0')->whereRaw($where)->with('country', 'state', 'city')->get();

        if (count($userShippingAddress) > 0) {
            return response()->json([
                        'status' => '1',
                        'results' => $userShippingAddress
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => ''
                            ], 200);
        }
    }

    public function saveshippingaddress($userId, $id = '-1', Request $request) {

        $addressBook = new Addressbook;

        $validator = Validator::make($request->all(), [
                    'firstName' => 'required|alpha',
                    'lastName' => 'required|alpha',
                    'email' => 'required|email',
                    'country' => 'required',
                    'address' => 'required',
                    //'cityId' => 'required',
                    'phone' => 'required',
        ]);

        if ($validator->fails()) {

            return response()->json([
                        'status' => '-1',
                        'results' => $validator->errors()
                            ], 200);
        } else {

            if ($id != '-1' && $id != '0' && $request->postAction == 'modify') {
                $addressBook = Addressbook::find($id);
            } else {
                $addressBook = new Addressbook;
                $addressBook->userId = $userId;
            }
            $addressBook->title = $request->title;
            $addressBook->firstName = $request->firstName;
            $addressBook->lastName = $request->lastName;
            $addressBook->email = $request->email;
            $addressBook->address = $request->address;
            $addressBook->alternateAddress = $request->alternateAddress;
            $addressBook->cityId = $request->city;
            $addressBook->stateId = $request->state;
            $addressBook->countryId = $request->country;
            $addressBook->zipcode = empty($request->zipcode) ? NULL : $request->zipcode;
            $addressBook->phone = $request->phone;
            $addressBook->isdCode = $request->phCode;
            $addressBook->alternatePhone = $request->alternatePhone;
            $addressBook->altIsdCode = $request->altphCode;
            $addressBook->modifiedBy = '0';
            $addressBook->modifiedOn = Config::get('constants.CURRENTDATE');
            if (isset($request->locationId) && $request->locationId != '')
                $addressBook->locationId = $request->locationId;

            $userBillingAddress = Addressbook::where('userId', $userId)->where('isDefaultBilling', '1')->first();
            if (empty($userBillingAddress)) {
                $addressBook->isDefaultBilling = '1';
            }

            $userShippingAddress = Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->first();
            if (empty($userShippingAddress)) {
                $addressBook->isDefaultShipping = '1';
            }

            if ($addressBook->save()) {
                $addressBookId = $addressBook->id;
                if (isset($addressBook->isDefaultShipping) && $addressBook->isDefaultShipping == '1') {

                    $user = User::find($userId);
                    $countryCode = Country::where('id', $request->country)->first()->code;
                    /* $maxUnitNumber = User::where('id', $userId)->max('unitNumber');
                      if (empty($maxUnitNumber))
                      $maxUnitNumber = Config::get('constants.MAXUNITNUMBER');
                      else
                      $maxUnitNumber = $maxUnitNumber + 1; */

                    //$user->unitNumber = $maxUnitNumber;
                    $user->unit = $countryCode . $userId;
                    $user->setDeliveryAddress = "1";
                    $user->save();
                }
                $user = User::find($userId);
                return response()->json([
                            'status' => '1',
                            'results' => $user->unit
                                ], 200);
            } else {
                return response()->json([
                            'status' => '-2',
                            'results' => 'error'
                                ], 200);
            }
        }
    }

    public function setdefaultshipping($userId, $addressId, $type) {

        $addressDetails = Addressbook::find($addressId);
        if ($addressDetails->locationId != 0 && $type == 'billing') {
            return response()->json([
                        'status' => '-1',
                        'results' => 'You are not allowed to set it as default billing as it is a shoptomydoor location',
                            ], 200);
        }
        if ($type == 'shipping')
            $updateAddressField = 'isDefaultShipping';
        else if ($type == 'billing')
            $updateAddressField = 'isDefaultBilling';
        Addressbook::where('userId', $userId)->update([$updateAddressField => '0']);
        Addressbook::where('id', $addressId)->update([$updateAddressField => '1']);
        if ($type == 'shipping') {
            $address = Addressbook::find($addressId);
            $user = User::find($userId);
            $countryCode = Country::where('id', $address->countryId)->first()->code;
            /* $maxUnitNumber = User::where('id', $userId)->max('unitNumber');
              if (empty($maxUnitNumber))
              $maxUnitNumber = Config::get('constants.MAXUNITNUMBER');
              else
              $maxUnitNumber = $maxUnitNumber + 1;
             */
            //$user->unitNumber = $maxUnitNumber;
            $user->unit = $countryCode . $userId;
            $user->save();
        }

        return response()->json([
                    'status' => '1',
                    'results' => 'success'
                        ], 200);
    }

    public function updateprofileimage($userId, Request $request) {
        if ($request->hasFile('fileItem')) {
            $image = $request->file('fileItem');
            $name = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/profile_image');
            $image->move($destinationPath, $name);
            User::where('id', $userId)->update(['profileImage' => $name]);
            return response()->json([
                        'status' => '1',
                        'results' => url('uploads/profile_image/' . $name)
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => 'error'
                            ], 200);
        }
    }

    public function saveshipmentsettings($userId, Request $request) {
        //return json_encode($request->settings);

        //print_r($request->all()); die;

        if (!empty($request->settings)) {
            Usershipmentsettings::where('userId', $userId)->delete();
            foreach ($request->settings as $field => $value) {
                $userSettings = new Usershipmentsettings;
                $userSettings->userId = $userId;
                $userSettings->fieldType = $field;
                $userSettings->fieldValue = $value;
                $userSettings->save();
            }

            if($request->type == 'firstUser')
            {
               User::where('id', $userId)->update(['setShipmentSettings' => '1']);

            }

            $settingsHistory = new \App\Model\Usershipmentsettingshistory;
            $settingsHistory->modifiedBy = $userId;
            $settingsHistory->modifiedOn = Config::get('constants.CURRENTDATE');
            $settingsHistory->save();

            return response()->json([
                        'status' => '1',
                        'results' => 'success'
            ]);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => 'error'
            ]);
        }
    }

    public function getshipmentsettings($userId) {

        $results = array();
        $userSettingsData = Usershipmentsettings::where('userId', $userId)->get();
        $userSettingsHistory = \App\Model\Usershipmentsettingshistory::where('modifiedBy', $userId)->get();
        if (count($userSettingsData) > 0) {
            foreach ($userSettingsData as $eachData) {
                $results[$eachData->fieldType] = ($eachData->fieldValue=="0" || $eachData->fieldValue=="")?"0":$eachData->fieldValue;
            }
            return response()->json([
                        'status' => '1',
                        'userData' => $results,
                        'historyData' => $userSettingsHistory,
            ]);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => ''
            ]);
        }
    }

    public function sendreferralinviation($userId, Request $request) {
        $user = User::find($userId);

        $emailTemplate = Emailtemplate::where('templateKey', 'referral_invitation')->first();
        $emailTemplate->templateBody = $request->message . '<br/>' . $emailTemplate->templateBody;
        $referralLink = Config::get('constants.frontendUrl') . 'join/refer/' . $user->referralCode;
        $replace['[ACTIVATION_LINK]'] = '<a href="' . $referralLink . '">Join</a>';
        if (!empty($request->users)) {
            foreach ($request->users as $eachUser) {
                $to = $eachUser['value'];
                $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
            }
            return response()->json([
                        'status' => '1',
                        'results' => 'success'
            ]);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => ''
            ]);
        }
    }

    public function updateusercart($userId, $type, Request $request) {

        if ($type == 'autoparts') {
            $cartContent = array(
                'storeId' => $request->storeId,
                'siteCategoryId' => $request->siteCategoryId,
                'siteSubCategoryId' => $request->siteSubCategoryId,
                'siteProductId' => $request->siteProductId,
                'websiteUrl' => $request->websiteUrl,
                'itemName' => $request->itemName,
                'itemDescription' => $request->itemDescription,
                'itemYear' => $request->itemYear,
                'itemMake' => $request->itemMake,
                'itemModel' => $request->itemModel,
                'itemPrice' => abs($request->itemPrice),
                'itemQuantity' => abs($request->itemQuantity),
                'itemShippingCost' => abs($request->itemShippingCost),
                'itemTotalCost' => (abs($request->itemPrice) * abs($request->itemQuantity)) + abs($request->itemShippingCost),
                'siteProductImage' => $request->siteProductImage,
                'type' => 'autopart'
            );
        } else if ($type == 'shopforme') {
            $cartContent = array(
                'storeId' => $request->storeId,
                'siteCategoryId' => $request->siteCategoryId,
                'siteSubCategoryId' => $request->siteSubCategoryId,
                'siteProductId' => $request->siteProductId,
                'websiteUrl' => $request->websiteUrl,
                'itemName' => $request->itemName,
                'options' => $request->options,
                'itemPrice' => abs($request->itemPrice),
                'itemQuantity' => abs($request->itemQuantity),
                'itemShippingCost' => abs($request->itemShippingCost),
                'itemTotalCost' => (abs($request->itemPrice) * abs($request->itemQuantity)) + abs($request->itemShippingCost),
                'siteProductImage' => $request->siteProductImage,
                'type' => $type
            );
        } else if ($type == 'buy_a_car') {
            $cartContent = (array) $request->all();
            $cartContent['price'] = abs($cartContent['price']);
            $cartContent['websiteName'] = \App\Model\Autowebsite::find($cartContent['website'])->name;
            $cartContent['makeName'] = \App\Model\Automake::find($cartContent['make'])->name;
            $cartContent['modelName'] = \App\Model\Automodel::find($cartContent['model'])->name;
            $cartContent['countryName'] = Country::find($cartContent['fromCountry'])->name;
            $cartContent['stateName'] = State::find($cartContent['fromState'])->name;
            $cartContent['cityName'] = City::find($cartContent['fromCity'])->name;
            $cartContent['websiteName'] = \App\Model\Autowebsite::find($cartContent['website'])->name;
            $cartContent['type'] = $type;
        } else if ($type == 'ship_my_car') {
            $cartContent = (array) $request->all();
            $cartContent['makeName'] = \App\Model\Automake::find($cartContent['makeId'])->name;
            $cartContent['modelName'] = \App\Model\Automodel::find($cartContent['modelId'])->name;
            $cartContent['shippingCost'] = $cartContent['shipmentCost'];
            $cartContent['carUrl'] = $cartContent['websiteLink'];
            $cartContent['countryName'] = Country::find($cartContent['pickupCountry'])->name;
            $cartContent['stateName'] = State::find($cartContent['pickupState'])->name;
            $cartContent['cityName'] = City::find($cartContent['pickupCity'])->name;
            $cartContent['shippingCountryName'] = Country::find($cartContent['destinationCountry'])->name;
            $cartContent['shippingStateName'] = State::find($cartContent['destinationState'])->name;
            $cartContent['shippingCityName'] = City::find($cartContent['destinationCity'])->name;
            $cartContent['vinnumber'] = $cartContent['vinNumber'];
            $cartContent['price'] = abs($cartContent['itemPrice']);
            $cartContent['type'] = $type;
            $cartContent['locationTypeName'] = \App\Model\Locationtype::find($cartContent['pickupLocationType'])->name;
            $cartContent['receiverPhone'] = '+' . $cartContent['receiverPhoneCode'] . ' ' . $cartContent['receiverPhone'];
            $cartContent['pickupPhone'] = '+' . $cartContent['pickupPhoneCode'] . ' ' . $cartContent['pickupPhone'];

            if ($request->hasFile('carTitle')) {
                $image = $request->file('carTitle');
                $imageNameTitle = time() . '_' . $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auto/carpickup/' . $userId);
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 0777);
                    chmod($destinationPath, 0777);
                }
                $image->move($destinationPath, $imageNameTitle);
                $cartContent['carTitle'] = $imageNameTitle;
            }
        } else if ($type == 'fillship') {
            $cartContent = array(
                'siteCategoryId' => $request->siteCategoryId,
                'siteSubCategoryId' => $request->siteSubCategoryId,
                'siteProductId' => $request->siteProductId,
                'itemPrice' => abs($request->itemPrice),
                'itemQuantity' => abs($request->itemQuantity),
                'itemTotalCost' => abs($request->itemPrice) * abs($request->itemQuantity),
                'type' => 'fillship'
            );
        }

        if ($request->hasFile('itemImage')) {
            $image = $request->file('itemImage');
            $name = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/procurement/shopforme/' . $userId);
            if ($type == 'buy_a_car' || $type == 'ship_my_car') {
                $destinationPath = public_path('/uploads/auto/carpickup/' . $userId);
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath);
                    chmod($destinationPath, 0777);
                }
            } else if ($type == 'shopforme') {
                $destinationPath = public_path('/uploads/procurement/shopforme/' . $userId);
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 777);
                    chmod($destinationPath, 0777);
                }
            } else if ($type == 'autoparts') {
                $destinationPath = public_path('/uploads/procurement/autoparts/' . $userId);
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath);
                    chmod($destinationPath, 0777);
                }
            } else if ($type == 'fillship') {
                $destinationPath = public_path('/uploads/fillship/shipments/' . $userId);
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath);
                    chmod($destinationPath, 0777);
                }
            }
            $image->move($destinationPath, $name);
            $cartContent['itemImage'] = $name;
        }

        $results = 'success';
        if ($type == 'buy_a_car') {
            if (!empty($cartContent['itemImage']))
                $cartContent['itemImagePath'] = url('/uploads/auto/carpickup/' . $userId . '/' . $cartContent['itemImage']);
            $results = $cartContent;
        }
        else if ($type == 'ship_my_car') {
            if (!empty($cartContent['itemImage']))
                $cartContent['itemImagePath'] = url('/uploads/auto/carpickup/' . $userId . '/' . $cartContent['itemImage']);

            $results = $cartContent;
        } else if ($type == 'shopforme') {
            if (!empty($cartContent['itemImage']))
                $cartContent['itemImagePath'] = url('/uploads/procurement/shopforme/' . $userId . '/' . $cartContent['itemImage']);
            $results = $cartContent;
        }else if ($type == 'autoparts') {
            if (!empty($cartContent['itemImage']))
                $cartContent['itemImagePath'] = url('/uploads/procurement/autoparts/' . $userId . '/' . $cartContent['itemImage']);
            $results = $cartContent;
        }else if ($type == 'fillship') {
            if (!empty($cartContent['itemImage']))
                $cartContent['itemImagePath'] = url('/uploads/fillship/shipments/' . $userId . '/' . $cartContent['itemImage']);
            $results = $cartContent;
        }

        $userCart = new Usercart;
        $userCart->userId = $userId;
        $userCart->type = $type;
        $userCart->cartContent = json_encode($cartContent);
        $userCart->savedForLater = 'N';
        $userCart->createdOn = Config::get('constants.CURRENTDATE');
        if (!empty($request->warehouseId))
            $userCart->warehouseId = $request->warehouseId;


        if ($userCart->save()) {
            return response()->json([
                        'status' => '1',
                        'results' => $results
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    public function getusercartlist(Request $request) {
        $data = array();

        $totalItemCost = $totalProcessingFee = $totalUrgentCost = $totalCost = $urgentPurchaseCostMin = 0;
        $urgentCostCharged = false;

        if ($request->type != 'fillship')
            $userCartList = Usercart::where('userId', $request->id)
                            ->where('savedForLater', 'N')
                            ->where('type', $request->type)
                            ->get()->toArray();
        else {
            $userCartList = Usercart::where('userId', $request->id)
                            ->where('savedForLater', 'N')
                            ->where('warehouseId', $request->warehouseId)
                            ->where('type', $request->type)
                            ->get()->toArray();
        }

        if (!empty($userCartList)) {
            foreach ($userCartList as $count => $row) {
                foreach ($row as $key => $value) {
                    if ($key != 'cartContent') {
                        $data['items'][$count][$key] = $value;
                    } else {
                        $cartContent = json_decode($value);
                        if (!empty($cartContent)) {
                            foreach ($cartContent as $key => $value) {
                                if ($key == 'itemPrice' || $key == 'itemShippingCost') {
                                    $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                } else if ($key == 'itemTotalCost') {
                                    $totalItemCost += $value;
                                    $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                } else if ($key == 'storeId') {
                                    if (!empty($value)) {
                                        $store = \App\Model\Stores::find($value);
                                        $data['items'][$count]['storeName'] = $store->storeName;
                                        $data['items'][$count]['storeId'] = $value;
                                    }

                                    // $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                } else if ($key == 'siteProductId') {
                                    if (!empty($value)) {
                                        $product = \App\Model\Siteproduct::find($value);
                                        if(!empty($product))
                                        {
                                            $data['items'][$count]['siteProductName'] = $product->productName;
                                            $data['items'][$count]['siteProductId'] = $value;
                                            $data['items'][$count]['siteProductImage'] = asset('/uploads/site_products/' . $product->image);
                                        }
                                    }
                                } else {
                                    $data['items'][$count][$key] = $value;
                                }
                            }
                        }
                    }
                }
            }

            $userCartData = Usercart::select('warehouseId')->where('userId', $request->id)->where('savedForLater', 'N')->where('type', $request->type)->first();
            $warehouse = \App\Model\Warehouse::find($userCartData->warehouseId);

            if ($request->type != 'fillship') {
                /* Calculate procurement processing fee */
                $param = array('warehouseId' => $userCartData->warehouseId, 'totalItemCost' => $totalItemCost);
                $totalProcessingFee = \App\Model\Procurement::calculateProcessingFee($param);
                $totalCost = $totalItemCost + $totalProcessingFee;

                /* Calculate urgent fee */
                $urgentPurchaseCostMin = $warehouse['urgentPurchaseCost'];
            } else {
                $totalCost = 0;
            }

            /* Calculate procurement urgent fee */
            if ($request->urgent == 'Y') {
                $urgentPurchaseCost = ($warehouse['urgentPurchase'] / 100) * $totalItemCost;

                if ($totalItemCost >= $urgentPurchaseCostMin) {
                    $totalUrgentCost = $urgentPurchaseCost;
                    $urgentCostCharged = true;
                }

                $totalCost = $totalCost + $totalUrgentCost;
            }

            $data['details'] = array(
                'totalItemCost' => customhelper::getCurrencySymbolFormat($totalItemCost),
                'totalProcessingFee' => customhelper::getCurrencySymbolFormat($totalProcessingFee),
                'totalUrgentCost' => customhelper::getCurrencySymbolFormat($totalUrgentCost),
                'urgentCostCharged' => $urgentCostCharged,
                'minUrgentCost' => customhelper::getCurrencySymbolFormat($urgentPurchaseCostMin),
                'totalCost' => customhelper::getCurrencySymbolFormat($totalCost),
            );

            $data['warehouseId'] = $userCartData->warehouseId;
            $data['warehouseCode'] = $warehouse->warehouseId;
        }


        echo json_encode($data);
        exit;
    }

    public function savecartlist($userId, Request $request) {
        if (!empty($request->items)) {
            foreach ($request->items as $item) {
                $userCart = Usercart::find($item['id']);
                $userCart->savedForLater = 'Y';
                $userCart->save();
            }

            return response()->json([
                        'status' => '1',
                        'results' => 'success'
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    public function removecartitem(Request $request) {
        $userCart = Usercart::find($request->id);
        $cartContent = !empty($userCart->cartContent) ? $userCart->cartContent : '';
        $userId = $userCart->userId;
        if ($userCart->delete()) {
            if (!empty($cartContent) && !empty(json_decode($cartContent)->itemImage)) {
                $image = json_decode($cartContent)->itemImage;
                if ($userCart->type == 'shopforme') {
                    $destinationPath = public_path('/uploads/procurement/shopforme/' . $userId . '/' . $image);
                    if (!empty($image) && file_exists($destinationPath)) {
                        @chmod($destinationPath, 0777);
                        @unlink($destinationPath);
                    }
                } else if ($userCart->type == 'autoparts') {
                    $destinationPath = public_path('/uploads/procurement/autoparts/' . $userId . '/' . $image);
                    if (!empty($image) && file_exists($destinationPath)) {
                        @chmod($destinationPath, 0777);
                        @unlink($destinationPath);
                    }
                }
            }

            return response()->json([
                        'status' => '1',
                        'results' => 'success'
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    public function getsaveforlaterlist(Request $request) {
        $data = array();

        if ($request->userId) {
            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            /*  FETCH TOTAL ITMES IN USER CART LIST */
            $totalItems = Usercart::where('userId', $request->userId)->where('type', 'shopforme')->where('savedForLater', 'Y')->count();

            /*  FETCH USER CART LIST */
            $userCartList = Usercart::where('userId', $request->userId)->where('type', 'shopforme')->where('savedForLater', 'Y')->take($this->_perPage)->skip($offset)->get();

            $userCartListArr = $userCartList->toArray();
            if (!empty($userCartListArr)) {
                foreach ($userCartListArr as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key != 'cartContent') {
                            $data['items'][$count][$key] = $value;
                        } else {
                            $cartContent = json_decode($value);
                            if (!empty($cartContent)) {
                                foreach ($cartContent as $key => $value) {
                                    if ($key == 'itemPrice' || $key == 'itemShippingCost' || $key == 'itemTotalCost') {
                                        $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'storeId') {
                                        if (!empty($value)) {
                                            $store = \App\Model\Stores::find($value);
                                            $data['items'][$count]['storeName'] = $store->storeName;
                                            $data['items'][$count]['storeId'] = $value;
                                        }

                                        //  $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'itemImage') {
                                        $itemImage = $value;
                                    } else if ($key == 'siteProductImage') {
                                        $siteProductImage = $value;
                                    } else {
                                        if (isset($itemImage) && !empty($itemImage))
                                            $data['items'][$count]['itemImage'] = $itemImage;
                                        elseif (isset($siteProductImage) && !empty($siteProductImage))
                                            $data['items'][$count]['itemImage'] = $siteProductImage;
                                        else
                                            $data['items'][$count]['itemImage'] = "";

                                        $data['items'][$count][$key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'data' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $this->_perPage,
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    // To fetch Auto Parts Save for Later records

    public function getautopartslist(Request $request) {
        $data = array();

        if ($request->userId) {
            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            /*  FETCH TOTAL ITMES IN USER CART LIST */
            $totalItems = Usercart::where('userId', $request->userId)->where('type', 'autoparts')->where('savedForLater', 'Y')->count();

            /*  FETCH USER CART LIST */
            $userCartList = Usercart::where('userId', $request->userId)->where('type', 'autoparts')->where('savedForLater', 'Y')->take($this->_perPage)->skip($offset)->get();

            $userCartListArr = $userCartList->toArray();
            if (!empty($userCartListArr)) {
                foreach ($userCartListArr as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key != 'cartContent') {
                            $data['items'][$count][$key] = $value;
                        } else {
                            $cartContent = json_decode($value);
                            if (!empty($cartContent)) {
                                foreach ($cartContent as $key => $value) {
                                    if ($key == 'itemPrice' || $key == 'itemShippingCost' || $key == 'itemTotalCost') {

                                        $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'storeId') {

                                        if (!empty($value)) {
                                            $store = \App\Model\Stores::find($value);
                                            $data['items'][$count]['storeName'] = $store->storeName;
                                            $data['items'][$count]['storeId'] = $value;
                                        }

                                        $data['items'][$count][$key] = customhelper::getCurrencySymbolFormat($value);
                                    } else if ($key == 'itemImage') {

                                        $itemImage = $value;
                                    } else if ($key == 'siteProductImage') {

                                        $siteProductImage = $value;
                                    } else {

                                        if (isset($itemImage) && !empty($itemImage))
                                            $data['items'][$count]['itemImage'] = $itemImage;
                                        elseif (isset($siteProductImage) && !empty($siteProductImage))
                                            $data['items'][$count]['itemImage'] = $siteProductImage;
                                        else
                                            $data['items'][$count]['itemImage'] = "";

                                        $data['items'][$count][$key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'data' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $this->_perPage,
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    public function movetousercart(Request $request) {

        if ($request->userId) {
            if (!empty($request->itemIds)) {
                foreach ($request->itemIds as $itemId) {
                    $userCart = Usercart::find($itemId);
                    $userCart->savedForLater = 'N';
                    $userCart->save();
                }
            }

            return response()->json([
                        'status' => '1',
                        'results' => 'success'
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    public function clearusercart(Request $request) {
        if ($request->userId) {
            $userId = $request->userId;

            /*  FETCH USER CART LIST */
            $userCartList = Usercart::where('userId', $userId)->where('type', $request->type)->where('savedForLater', 'N')->get()->toArray();

            /* DELETE ITEM IMAGE FOR CART */
            if (!empty($userCartList)) {
                foreach ($userCartList as $value) {
                    $cartContent = json_decode($value['cartContent'], TRUE);
                    if (!empty($cartContent)) {
                        $image = !empty($cartContent['itemImage']) ? $cartContent['itemImage'] : '';
                        if ($value['type'] == 'shopforme') {
                            $destinationPath = public_path('/uploads/procurement/shopforme/' . $userId . '/' . $image);
                            if (!empty($image) && file_exists($destinationPath)) {
                                @chmod($destinationPath, 0777);
                                @unlink($destinationPath);
                            }
                        } else if ($value['type'] == 'autoparts') {
                            $destinationPath = public_path('/uploads/procurement/autoparts/' . $userId . '/' . $image);
                            if (!empty($image) && file_exists($destinationPath)) {
                                @chmod($destinationPath, 0777);
                                @unlink($destinationPath);
                            }
                        }
                    }
                }
            }

            /* DELETE USER CART ITEMS */
            $userCart = Usercart::where('userId', $request->userId)->where('type', $request->type)->where('savedForLater', 'N')->delete();

            return response()->json([
                        'status' => '1',
                        'results' => 'success'
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    public function updatecartcurrency(Request $request) {

        $currencyCode = $request->toCurrency;
        $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
        $exchangeRate = \App\Model\Currency::currencyExchangeRate($request->fromCurrency, $currencyCode);
        $userCartData = $request->usercart;
        $userId = $request->userId;
        $type = $userCartData['type'];
        $userCartData['currencyCode'] = $currencyCode;
        $userCartData['exchangeRate'] = round($exchangeRate, 2);
        $userCartData['currencySymbol'] = $currencySymbol;
        $userCartData['isCurrencyChanged'] = 'Y';
        Usercart::where('userId', $userId)->where('type', $type)->update(['cartContent' => json_encode($userCartData)]);
        return response()->json([
                    'status' => '1',
                    'results' => $userCartData,
        ]);
    }

    public function updatecartcontent(Request $request) {
        $userCartData = $request->usercart;
        $userId = $request->userId;
        $type = $userCartData['type'];
        Usercart::where('userId', $userId)->where('type', $type)->update(['cartContent' => json_encode($userCartData)]);
        return response()->json([
                    'status' => '1',
                    'results' => $userCartData,
        ]);
    }

    public function checkautocartdata(Request $request) {

        $userId = $request->userId;
        $type = $request->type;

        $data = Usercart::where('userId', $userId)->where('type', $type)->first();

        if (count($data) > 0) {
            $userCartData = json_decode($data->cartContent);
            return response()->json([
                        'status' => '1',
                        'results' => $userCartData,
            ]);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => array(),
            ]);
        }
    }

    public function deleteautocartdata(Request $request) {
        $userId = $request->userId;
        $type = $request->type;
        $cart = Usercart::where('userId', $userId)->where('type', $type);
        $cartContent = json_decode($cart->first()->cartContent);
        if ($cart->delete()) {
            if (isset($cartContent->itemImage)) {
                $cartFile = url('/uploads/auto/carpickup/' . $userId . '/' . $cartContent->itemImage);
                if (file_exists($cartFile)) {

                    unlink($cartFile);
                }
            }
            if ($type == 'ship_my_car') {
                if (isset($cartContent->carTitle)) {
                    $carTitle = url('/uploads/auto/carpickup/' . $userId . '/' . $cartContent->carTitle);
                    if (file_exists($carTitle)) {
                        unlink($carTitle);
                    }
                }
            }
            return response()->json([
                        'status' => '1',
                        'results' => 'success',
            ]);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => 'success',
            ]);
        }
    }

    public function validateewallet(Request $request) {
        if (!empty($request->ewalletId)) {
            $userEwallet = \App\Model\Ewallet::where('recipient', $request->userId)->where('ewalletId', $request->ewalletId)->first();
            if (!empty($userEwallet)) {
                if ($userEwallet->amount >= $request->amountToBePaid) {
                    return response()->json([
                                'status' => '1',
                                'results' => $userEwallet->id,
                    ]);
                } else {
                    return response()->json([
                                'status' => '0',
                                'results' => 'E-Wallet ID does not have sufficent fund for this transaction.',
                    ]);
                }
            } else {
                return response()->json([
                            'status' => '0',
                            'results' => 'E-Wallet ID does not exist',
                ]);
            }
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'E-Wallet ID required',
            ]);
        }
    }

    public function getpaymenthistory(Request $request) {
        $data = array();

        if (!empty($request->userId)) {
            $page = $request->currentPage;
            $perPage = isset($request->perPage) ? $request->perPage : $this->_perPage;
            $offset = ($page - 1) * $perPage;

            /*  FETCH TOTAL ITMES IN PAYMENT LOG */
            $totalItems = \App\Model\Paymenttransaction::where('userId', $request->userId)->count();

            /*  FETCH PAYMENT DATA */
            $param = array(
                'userId' => $request->userId,
                'perPage' => $perPage,
                'offset' => $offset,
            );
            $paymentData = \App\Model\Paymenttransaction::getPaymentHistory($param)->toArray();
            if (!empty($paymentData)) {
                foreach ($paymentData as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key == 'amountPaid') {
                            if ($row['paidFor'] == 'shipacar')
                                $data[$count][$key] = customhelper::getCurrencySymbolFormat($value * $row['autoExchangeRate'], $row['autoPaidCurrencyCode']);
                            else if($row['paidFor'] == 'shopformeshipment' || $row['paidFor'] == 'autopartshipment' || $row['paidFor'] == 'othershipment')
                                $data[$count][$key] = customhelper::getCurrencySymbolFormat($value * $row['shipmentExchangeRate'], $row['shipmentPaidCurrency']);
                            else if($row['paidFor'] == 'fillship')
                                $data[$count][$key] = customhelper::getCurrencySymbolFormat($value * $row['fillnshipExchangeRate'], $row['fillnshipPaidCurrency']);
                            else
                                $data[$count][$key] = customhelper::getCurrencySymbolFormat($value * $row['exchangeRate'], $row['paidCurrencyCode']);
                        } elseif ($key == 'transactionOn') {
                            $data[$count][$key] = \Carbon\Carbon::parse($value)->format('m/d/Y');
                        } elseif ($key == 'paidFor') {
                            if ($value == 'shopforme' || $value == 'shopformeshipment')
                                $paidForLabel = 'Shop For Me #' . $row['paidForId'];
                            else if ($value == 'autopart' || $value == 'autopartshipment')
                                $paidForLabel = 'Auto Parts #' . $row['paidForId'];
                            else if ($value == 'buyacar')
                                $paidForLabel = 'Buy A Car For Me #' . $row['paidForId'];
                            else if ($value == 'shipacar')
                                $paidForLabel = 'Ship A Car #' . $row['paidForId'];
                            else if ($value == 'shipacar')
                                $paidForLabel = 'Ship A Car #' . $row['paidForId'];
                            else if ($value == 'fillship')
                                $paidForLabel = 'Fill and Ship #' . $row['paidForId'];
                             else if ($value == 'itemReturn')
                                $paidForLabel = 'Shipment Return #' . $row['paidForId'];
                            else
                                $paidForLabel = 'Shipment #' . $row['paidForId'];

                            $data[$count][$key] = $value;
                            $data[$count]['paidForLabel'] = $paidForLabel;
                        } else {
                            $data[$count][$key] = $value;
                        }
                    }
                }
            }
            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'data' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $perPage,
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'Invalid user',
            ]);
        }
    }

    public function getinvoicelist(Request $request) {
        $data = array();

        if (!empty($request->userId)) {
            $page = $request->currentPage;
            $perPage = isset($request->perPage) ? $request->perPage : $this->_perPage;
            $offset = ($page - 1) * $perPage;

            $user = User::find($request->userId);

            /*  FETCH TOTAL ITMES IN PAYMENT LOG */
            $totalItems = \App\Model\Invoice::where('userEmail', $user->email)
                    ->where('invoiceType', 'invoice')
                    ->where('paymentMethodId', NULL)
                    ->where('paymentStatus', 'unpaid')
                    ->where('extraCostCharged', 'Y')
                    ->whereNotNull('extraCostCharged')
                    ->count();

            /*  FETCH PAYMENT DATA */
            $param = array(
                'userEmail' => $user->email,
                'perPage' => $perPage,
                'offset' => $offset,
            );
            $invoiceData = \App\Model\Invoice::getUnpaidInvoiceList($param)->toArray();
            if (!empty($invoiceData)) {
                foreach ($invoiceData as $count => $row) {
                    foreach ($row as $key => $value) {
                        if ($key == 'transactionOn') {
                            $data[$count][$key] = \Carbon\Carbon::parse($value)->format('m/d/Y');
                        } elseif ($key == 'type') {
                            if ($value == 'shopforme') {
                                if (!empty($row['procurementId']))
                                    $paidForLabel = 'Shop For Me #' . $row['procurementId'];
                                else
                                    $paidForLabel = 'Shop For Me #' . $row['shipmentId'];
                            }else if ($value == 'autopart') {
                                if (!empty($row['procurementId']))
                                    $paidForLabel = 'Auto Parts #' . $row['procurementId'];
                                else
                                    $paidForLabel = 'Auto Parts #' . $row['shipmentId'];
                            }else if ($value == 'buyacar')
                                $paidForLabel = 'Buy A Car For Me #' . $row['procurementId'];
                            else if ($value == 'othershipment')
                                $paidForLabel = 'Shipment #' . $row['shipmentId'];
                            else if ($value == 'itemReturn')
                                $paidForLabel = 'Shipment Return #' . $row['paidForId'];
                            else
                                $paidForLabel = 'Ship A Car #' . $row['shipmentId'];

                            $data[$count][$key] = $value;
                            $data[$count]['paidForLabel'] = $paidForLabel;
                        } else {
                            $data[$count][$key] = $value;
                        }
                    }
                }
            }
            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'data' => $data,
                        'totalItems' => $totalItems,
                        'itemsPerPage' => $perPage,
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'Invalid user',
            ]);
        }
    }

    public function getpaymentmethodlist(Request $request) {
        if (!empty($request->userId)) {
            $user = \App\Model\Addressbook::select('countryId')->where('isDefaultBilling', 1)->where('userId', $request->userId)->first();
            $paymentMethodList = \App\Model\Paymentmethod::getallpaymentmethods($user->countryId);
            //print_r($paymentMethodList);exit;
            return json_encode($paymentMethodList);
        }
    }

    public function getinvoice(Request $request) {
        if (!empty($request->id)) {
            $invoice = \App\Model\Invoice::where('id', $request->id)->where('extraCostCharged', 'Y')->first();
            return $invoice->toJson();
        }
    }

    public function payinvoice(Request $request) {
        //print_r($request->all());exit;
        if (!empty($request->data)) {
            $paymentErrorMessage = '';
            $paymentStatus = 'unpaid';
            $defaultCurrencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);

            $invoiceId = $request->data['invoiceId'];
            $userId = $request->userId;

            $userData = User::find($userId);
            $invoice = \App\Model\Invoice::find($invoiceId);
            $invoiceParticulars = json_decode($invoice->invoiceParticulars, true);
            $shippingAddress = (array) $invoiceParticulars['shippingaddress'];

            if ($request->data['paymentMethodKey'] == 'wire_transfer') {
                $trasactionData = json_encode(array(
                    'poNumber' => $request->data['poNumber'],
                    'companyName' => $request->data['companyName'],
                    'buyerName' => $request->data['buyerName'],
                    'position' => $request->data['position'],
                        )
                );
            } else if ($request->data['paymentMethodKey'] == 'ewallet') { /*  PROCESS EWALLET PAYMENT */
                $eawalletid = (int) $request->data['id'];
                $userEwallet = \App\Model\Ewallet::find($eawalletid)->decrement('amount', $totalCost);

                $ewalletTransaction = new \App\Model\Ewallettransaction;
                $ewalletTransaction->userId = $userId;
                $ewalletTransaction->ewalletId = $eawalletid;
                $ewalletTransaction->amount = $invoice->extraCostAmount;
                $ewalletTransaction->transactionType = 'debit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $ewalletTransaction->save();

                $paymentStatus = 'paid';

                $transactionData = json_encode(array(
                    'ewalletId' => $request->data['ewalletId'],
                        )
                );
            } else if ($request->data['paymentMethodKey'] == 'paystack_checkout') {/* PROCESS PAYSTACK DATA */
                /* Old code */
//                if (!empty($request->paystackData)) {
//                    $checkoutReturn = \App\Model\Paymenttransaction::paystack($request->paystackCreatedReference);
//                    if ($checkoutReturn) {
//                        $paymentStatus = 'paid';
//                        $trasactionId = $request->paystackData['trans'];
//                        $transactionData = json_encode($request->paystackData);
//                    }
//                } 
                if(!empty($request->paystackData['trans']))
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $request->paystackCreatedReference and amountPayable = '".$request->data['paidAmount']."' and processed='0'")->first();
                else
                    $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $request->paystackCreatedReference and amountPayable = '".$request->data['paidAmount']."'")->first();
                if(!empty($findRecord))
                {
                    $paymentTransacionData = \App\Model\Paymenttransaction::where("transactionData","like",'%"reference":"'.$request->paystackCreatedReference.'"%')->first();
                    if(empty($paymentTransacionData) || (!empty($paymentTransacionData) && $paymentTransacionData->status=='unpaid'))
                    {
                        \App\Model\Paystackreferencehistory::where("id",$findRecord->id)->update(["processed"=>"1"]);
                        if (!empty($request->paystackData)) {
                            $checkoutReturn = \App\Model\Paymenttransaction::paystack($request->paystackCreatedReference);
                            if ($checkoutReturn) {
                                $paymentStatus = 'paid';
                                if(!empty($request->paystackData['trans']))
                                    $trasactionId = $request->paystackData['trans'];
                                else if(!empty($request->paystackData['id']))
                                    $transactionId = $request->paystackData['id'];
                                else
                                    $transactionId = '123456789';
                                $transactionData = json_encode($request->paystackData);
                            } else {
                                $paymentStatus = 'failed';
                                $transactionErrorMsg = json_encode($request->paystackData);
                                $paymentErrorMessage = "Payment Failed";
                            }
                        }
                    }
                    else
                    {
                        $paymentStatus = 'pastackProcessed';
                    }
                }
                else
                {
                    $paymentStatus = 'pastackProcessed';
                }
            } elseif ($request->data['paymentMethodKey'] == 'credit_debit_card') { /*  PROCESS CREDIT CARD PAYMENT */
                $paymentMethod = \App\Model\Paymentmethod::where('paymentMethodKey', $request->data['paymentMethodKey'])->first();
                if ($paymentMethod->paymentGatewayId == 1) {
                    $checkoutData = array();
                    $checkoutData['cardNumber'] = $request->data['ccardNumber'];
                    $checkoutData['expMonth'] = $request->data['expiryMonth'];
                    $checkoutData['expYear'] = $request->data['expiryYear'];
                    $checkoutData['cardCode'] = $request->data['cvvCode'];
                    $checkoutData['customerFirstName'] = $invoice->billingName;
                    $checkoutData['customerLastName'] = '';
                    $checkoutData['customerAddress'] = $invoice->billingAddress;
                    $checkoutData['customerCity'] = $invoice->billingCity;
                    $checkoutData['customerState'] = $invoice->billingState;
                    $checkoutData['customerCountry'] = $invoice->billingCountry;
                    $checkoutData['customerZip'] = $invoice->billingZipcode;
                    $checkoutData['amount'] = $invoice->extraCostAmount;
                    $checkoutData['defaultCurrency'] = $defaultCurrencyCode;

                    /* Old code */
//                    $checkoutReturn = \App\Model\Paymenttransaction::paypaypalpro($checkoutData);
//                    if (isset($checkoutReturn["ACK"]) Paymenttransaction&& ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
//                        $paymentStatus = 'paid';
//                        $transactionId = $checkoutReturn['TRANSACTIONID'];
//                        $transactionData = json_encode($checkoutReturn);
//                    }

                    $checkoutReturn = \App\Model\Paymenttransaction::paypaypalpro($checkoutData);

                    if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                        $paymentStatus = 'paid';
                        $transactionId = $checkoutReturn['TRANSACTIONID'];
                        $transactionData = json_encode($checkoutReturn);
                    } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($checkoutReturn);
                        $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    } else if ($checkoutReturn == 'error') {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = "Payment Error";
                    }
                } else {
                    $checkoutData['cardNumber'] = $request->data['ccardNumber'];
                    $checkoutData['expMonth'] = $request->data['expiryMonth'];
                    $checkoutData['expYear'] = $request->data['expiryYear'];
                    $checkoutData['cardCode'] = $request->data['cvvCode'];
                    $checkoutData['customerFirstName'] = $invoice->billingName;
                    $checkoutData['customerLastName'] = "";
                    $checkoutData['customerAddress'] = $invoice->billingAddress;
                    $checkoutData['customerCity'] = $invoice->billingCity;
                    $checkoutData['customerState'] = $invoice->billingState;
                    $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                    $checkoutData['customerShippingAddress'] = $shippingAddress['toAddress'];
                    $checkoutData['customerShippingCity'] = $shippingAddress['toCity'];
                    $checkoutData['customerShippingState'] = $shippingAddress['toState'];
                    $checkoutData['customerShippingCountry'] = $shippingAddress['toCountry'];
                    $checkoutData['customerShippingZip'] = $shippingAddress['toZipCode'];
                    $checkoutData['amount'] = $invoice->extraCostAmount;
                    $checkoutData['defaultCurrency'] = $defaultCurrencyCode;
                    $checkoutData['shippingFirstName'] = $shippingAddress['toName'];
                    $checkoutData['shippingLastName'] = "";

                    /*
                      $checkoutReturn = \App\Model\Paymenttransaction::payauthorizedotnet($checkoutData);

                      if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                      $paymentStatus = 'paid';
                      $transactionId = $checkoutReturn['transactionResponse']['transId'];
                      $transactionData = json_encode($checkoutReturn['transactionResponse']);
                      } else {
                      $transactionData = json_encode($checkoutReturn['transactionResponse']);
                      } */

                    $checkoutReturn = \App\Model\Paymenttransaction::payauthorizedotnet($checkoutData);

                    if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                        $paymentStatus = 'paid';
                        $transactionId = $checkoutReturn['transactionResponse']['transId'];
                        $transactionData = json_encode($checkoutReturn['transactionResponse']);
                    } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);
                        if (isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                        else
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                    }
                }
            } else if ($request->data['paymentMethodKey'] == 'paypalstandard') {
                if (!empty($request->paypalTransaction)) {
                    $paymentStatus = 'paid';
                    $transactionId = $request->paypalTransaction['orderID'];
                    $transactionData = json_encode($request->paypalTransaction);
                }
            } else if($request->data['paymentMethodKey'] == 'payeezy') {
                if(!empty($request->payeezyData)) {
                    $checkoutData['amount'] = $request->payeezyData["paidAmount"];
                    $checkoutData['method'] = $request->payeezyData["paymentType"];
                    $checkoutData['currency_code'] = "USD";
                    $checkoutData['type'] = $request->payeezyData["cardType"];
                    $checkoutData['cardholder_name'] = $request->payeezyData["cardHolderName"];
                    $checkoutData['card_number'] = $request->payeezyData['ccardNumber'];
                    $checkoutData['exp_date'] = $request->payeezyData['expiryMonth'].substr($request->payeezyData['expiryYear'],2);
                    $checkoutData['cvv'] = $request->payeezyData['cvvCode'];
                    $checkoutData['userUnit'] = (!empty($userData->unit) ? $userData->unit : "");
                    $checkoutReturn = \App\Model\Paymenttransaction::processpayeezy($checkoutData);

                    if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved')
                    {
                        $paymentStatus = 'paid';
                        $paymentMode = 'online';
                        $transactionId = $checkoutReturn['transaction_id'];
                        $transactionData = json_encode($checkoutReturn);
                    }
                    else if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed')
                    {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = 'Payment failed. ' .$checkoutReturn['Error']['messages'][0]['description'];
                    }
                    else
                    {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                    }

                } else {
                    $paymentStatus = 'error';
                }
            }
            

            $invoiceParticulars['payment'] = array(
                'paymentMethodId' => $request->data['paymentMethodId'],
                'paymentMethodName' => $request->data['paymentMethodName'],
            );
            $invoiceParticulars['shipment']['totalTax'] = $request->data['taxAmount'];
            $invoiceParticulars['shipment']['totalCost'] = $request->data['paidAmount'];
            if ($paymentStatus == 'paid')
                $invoice->invoiceType = 'receipt';

            $invoice->invoiceParticulars = json_encode($invoiceParticulars);
            $invoice->paymentMethodId = $request->data['paymentMethodId'];
            $invoice->paymentStatus = $paymentStatus;
            $invoice->totalBillingAmount = $request->data['paidAmount'];


            if ($paymentStatus == 'paid') {
                if ($invoice->save()) {

                    $paymentTransaction = new \App\Model\Paymenttransaction;
                    $paymentTransaction->userId = $userId;
                    $paymentTransaction->paymentMethodId = $request->data['paymentMethodId'];
                    $paymentTransaction->paidFor = $invoice->type;
                    if ($invoice->type == 'othershipment')
                        $paymentTransaction->paidForId = $invoice->shipmentId;
                    else
                        $paymentTransaction->paidForId = $invoice->procurementId;
                    $paymentTransaction->invoiceId = $invoice->id;
                    $paymentTransaction->amountPaid = $request->data['paidAmount'];
                    $paymentTransaction->status = $paymentStatus;
                    if (!empty($transactionData))
                        $paymentTransaction->transactionData = $transactionData;
                    if (!empty($transactionId))
                        $paymentTransaction->transactionId = $transactionId;
                    $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                    $paymentTransaction->save();

                    $invoiceUniqueId = $invoice->invoiceUniqueId;
                    $fileName = "Receipt_" . $invoiceUniqueId . ".pdf";

                    $data['invoice'] = $invoice;
                    $data['pageTitle'] = "Print Invoice";
                    if ($invoice->type == 'shopforme')
                        PDF::loadView('Administrator.procurement.extrainvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                    elseif ($invoice->type == 'autopart')
                        PDF::loadView('Administrator.autoparts.extrainvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                    else
                        PDF::loadView('Administrator.shipments.printinvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');

                    $to = $invoice->billingEmail;
                    Mail::send(['html' => 'mail'], ['content' => strtoupper($invoice->invoiceType) . ' for #' . $invoiceUniqueId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                        $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                        $message->subject("$invoiceUniqueId - Invoice");
                        $message->to($to);
                        $message->attach(public_path('exports/invoice/' . $fileName));
                    });

                    return response()->json([
                                'status' => '1',
                                'results' => 'success',
                    ]);
                }
            } 
            else if($paymentStatus == 'pastackProcessed') {
                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                ]);
            }
            else {

                $paymentTransaction = new \App\Model\Paymenttransaction;
                $paymentTransaction->userId = $userId;
                $paymentTransaction->paymentMethodId = $request->data['paymentMethodId'];
                $paymentTransaction->paidFor = $invoice->type;
                $paymentTransaction->amountPaid = $request->data['paidAmount'];
                $paymentTransaction->status = $paymentStatus;
                if (!empty($transactionErrorMsg))
                    $paymentTransaction->errorMsg = $transactionErrorMsg;
                $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $paymentTransaction->save();

                return response()->json([
                            'status' => '-1',
                            'results' => $paymentErrorMessage,
                ]);
            }
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => '',
            ]);
        }
    }

    public function virtualtourfinished(Request $request) {
        $userId = $request->userId;
        if ($userId != '') {
            User::where('id', $userId)->update(['virtualTourVisited' => '1']);
            return response()->json([
                        'status' => '1',
                        'results' => 'updated',
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'error',
            ]);
        }
    }

    public function updatevirtualtourlog(Request $request) {
        $userId = $request->userId;
        $logType = $request->logType;
        $section = $request->section;
        $isFinished = $request->isFinished;

        $virtualtourLog = new \App\Model\Virtualtourlog;
        $virtualtourLog->userId = $userId;
        $virtualtourLog->logType = $logType;
        $virtualtourLog->section = $section;
        $virtualtourLog->isFinished = $isFinished;
        $virtualtourLog->logTime = Config::get('constants.CURRENTDATE');
        $virtualtourLog->save();

        return response()->json([
                    'status' => '1',
                    'results' => 'success',
        ]);
    }

    public function checkvirtualtourfinished(Request $request) {
        $userId = $request->userId;
        $section = $request->section;

        $userVisited = 0;

        $userVisitRecord = \App\Model\Virtualtourlog::where('userId', $userId)->where('section', $section)->where('isFinished', '1')->count();
        if ($userVisitRecord > 0) {
            $userVisited = 1;
        }

        return response()->json([
                    'status' => '1',
                    'results' => $userVisited,
        ]);
    }

    public function getpaymentinvoice(Request $request) {
        if (!empty($request->paidForId)) {
            $paidFor = $request->paidFor;


            if ($paidFor == 'shopforme' || $paidFor == 'autopart' || $paidFor == 'buyacar') {
                if(!empty($request->invoiceId))
                {
                    $invoiceData = \App\Model\Invoice::where('id', $request->invoiceId)->first();
                }
                else
                {
                    $invoiceData = \App\Model\Invoice::where('procurementId', $request->paidForId)
                        ->where('type', $paidFor)
                        ->where('extraCostCharged', 'N')
                        ->where('deleted', '0')
                        ->orderby('id', 'desc')
                        ->first();
                }
            } else {
                 if($paidFor == 'shopformeshipment')
                        $paidFor = 'shopforme';
                    elseif($paidFor == '')
                        $paidFor = 'othershipment';
                if(!empty($request->invoiceId))
                {
                    $invoiceData = \App\Model\Invoice::where('id', $request->invoiceId)->first();
                }
                else
                {
                $invoiceData = \App\Model\Invoice::where('shipmentId', $request->paidForId)
                        ->where('extraCostCharged', 'N')
                        ->where('type', $paidFor)
                        ->where('deleted', '0')
                        ->orderby('id', 'desc')
                        ->first();
                }

                $invoiceType = ($invoiceData->type == 'autoshipment') ? $invoiceData->type : 'shipment';

                $orderData = \App\Model\Order::where('shipmentId', $request->paidForId)->where('type', $invoiceType)->first();
                if (!empty($orderData))
                    $data['orderNumber'] = $orderData->orderNumber;
            }

            if (!empty($invoiceData)) {
                $data['invoice'] = $invoiceData;

                if ($invoiceData->type == 'buycarforme' && $invoiceData->invoiceType == 'invoice') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarinvoice')->with($data)->render();
                } elseif ($invoiceData->type == 'buycarforme' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'invoice') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarinvoice')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autopart')
                    $returnHTML = view('Administrator.autoparts.invoice')->with($data)->render();
                elseif ($invoiceData->type == 'shopforme')
                    $returnHTML = view('Administrator.procurement.invoice')->with($data)->render();
                elseif ($invoiceData->type == 'fillship')
                    $returnHTML = view('Administrator.fillnship.invoice')->with($data)->render();
                elseif ($invoiceData->type == 'itemReturn')
                    $returnHTML = view('Administrator.shipments.returninvoice')->with($data)->render();
                else
                    $returnHTML = view('Administrator.shipments.invoice')->with($data)->render();

                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                            'data' => $returnHTML,
                ]);
            } else {
                return response()->json([
                            'status' => '0',
                            'results' => 'failed',
                ]);
            }
        }
    }



    /**
     * Method used to send otp for modifying default shipping address
     * @return object
     */
    public function sendotpforshippingaddress(Request $request) {
        
        $userId = $request->userId;
        $adressbookId = $request->adressbookId;

        if($request->billingAddress == 'yes'){
            // CHECK WHETHER THE ADDRESS IS DEFAULT SHIPPING/DELIVERY ADDRESS
            $isDefaultShipping = \App\Model\Addressbook::select('isDefaultShipping')->where('userId', $userId)->where("id", $adressbookId)->count();

            $isBilling = \App\Model\Addressbook::select("isDefaultShipping")->where('userId', $userId)->where("id", $adressbookId)->first();

            //if(!empty($isDefaultShipping->isDefaultShipping) && $isDefaultShipping->isDefaultShipping == 0){
            if($isDefaultShipping == 0 || $isBilling->isDefaultShipping == 0){
                return response()->json([
                    'status' => '-1'
                ]);
            }
        }
        //dd($isDefaultShipping);
        // GENERATE THE OTP 
        $digits = 5;
        $createOTP = rand(pow(10, $digits-1), pow(10, $digits)-1);

        // GET USER DETAILS
        $user = User::select(['email', 'firstName', 'lastName', 'isdCode', 'contactNumber'])->where('id', $userId)->first();

        // SENDING EMAIL WITH DEFINED TEMPLATE
        if (!empty($user)) {
            $isSendMsg = '';
            $emailTemplate = Emailtemplate::where('templateKey', 'send_otp')->first();
            $to = $user->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $replace['[OTP]'] = $createOTP;
            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
            //send otp to mobile
            $toMobile = trim($user->isdCode.$user->contactNumber);
            $smsTemplate = Smstemplate::where('templateKey', 'otp_for_modify_shipping_address')->first();
            if($toMobile){
                $replaceVar['[NAME]'] = $user->firstName;
                $replaceVar['[OTP]'] = $createOTP;
                $isSendMsg = customhelper::sendMSG($toMobile,$replaceVar,$smsTemplate);
            }
            if($toMobile && $isSendMsg){

                // DELETE RECORD FOR THE USER IF ANY
                $res=\App\Model\OtpShippingAddress::where('userId',$userId)->delete();

                // INSERT RECORD FOR NEW OTP
                $otp = new \App\Model\OtpShippingAddress;
                $otp->userId = $userId;
                $otp->otpcode = $createOTP;
                $otp->save();
            }

            return response()->json([
                'status' => '1'
            ]);
            
        }
        return response()->json([
            'status' => '0'
        ]);
        
    }


    /**
     * Method used to check otp for shipping address modification
     * @return object
     */
    public function checkotpforshippingaddress(Request $request) {
        $otp = $request->otp;
        $userId = $request->userId;

        $res = \App\Model\OtpShippingAddress::where('userId',$userId)->first();
        
        // CHECKING THE OTP
        //SUCCESS
        if($res->otpcode == $otp){
            \App\Model\OtpShippingAddress::where('userId',$userId)->delete();
            return response()->json([
                'status' => '1'
            ]);
        } else {
            // FAILED
            return response()->json([
                'status' => '0'
            ]);
        }
    }


    /**
     * Method used to re-send otp for modifying default shipping address
     * @return object
     */
    public function resendotpforshippingaddress(Request $request) {
        
        $userId = $request->userId;

        // GENERATE THE OTP 
        $digits = 5;
        $createOTP = rand(pow(10, $digits-1), pow(10, $digits)-1);

        // GET USER DETAILS
        $user = User::select(['email', 'firstName', 'lastName', 'isdCode', 'contactNumber'])->where('id', $userId)->first();

        // SENDING EMAIL WITH DEFINED TEMPLATE
        if (!empty($user)) {
            $isSendMsg = '';
            $emailTemplate = Emailtemplate::where('templateKey', 'send_otp')->first();
            $to = $user->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $replace['[OTP]'] = $createOTP;
            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
            //if($isSend){
            $toMobile = trim($user->isdCode.$user->contactNumber);
            $smsTemplate = Smstemplate::where('templateKey', 'resend_otp_for_modify_shipping_address')->first();
            if($toMobile){
                $replaceVar['[NAME]'] = $user->firstName;
                $replaceVar['[OTP]'] = $createOTP;
                $isSendMsg = customhelper::sendMSG($toMobile,$replaceVar,$smsTemplate);
            }
            if($toMobile && $isSendMsg){

                // DELETE RECORD FOR THE USER IF ANY
                $res=\App\Model\OtpShippingAddress::where('userId',$userId)->delete();

                // INSERT RECORD FOR NEW OTP
                $otp = new \App\Model\OtpShippingAddress;
                $otp->userId = $userId;
                $otp->otpcode = $createOTP;
                $otp->save();
            }

            return response()->json([
                'status' => '1'
            ]);
            
        }
        return response()->json([
            'status' => '0'
        ]);
        
    }

    public function saveSignature($userId, Request $request)
    {
        $data = $pageContent = array();

        $slug = "terms_conditions_join";

        


        $user = User::select(['unit', 'email', 'firstName', 'lastName', 'isdCode', 'contactNumber'])->where('id', $userId)->first();
        $adminEmail = UserAdmin::where("userType", 1)->where('status', '1')->where('deleted', '0')->pluck('email')->first();

        

        $cc = "contact@shoptomydoor.com";

        

        //DB::enableQueryLog();
        $pageContent = Sitepage::select("pageContent")->where("slug", $slug)->get();

        //print_r($pageContent[0]->pageContent); die;

        $data['pageContent'] = $pageContent[0]->pageContent;
        $data['signature'] = $request->firstname." ".$request->lastname;
        //dd(DB::getQueryLog());

       // print_r($pageContent);  die;

        $fileName = "TermsConditions_" . $user->unit. ".pdf";

        PDF::loadView('Administrator.users.terms', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
        
        if (!empty($user) && $request->terms=="true") {

            User::where('id', $userId)->update(['aggreedToTerms' => '1']);//join_terms_condition


            $emailTemplate = Emailtemplate::where('templateKey', 'join_terms_condition')->first();

            $templateSubject = $emailTemplate['templateSubject'];

            $to = $user['email'];
            $content = "<p>Dear ".$user['firstName'] . " " . $user['lastName'].",</p>";
            $content .= $emailTemplate['templateBody']; // Need to create email templates for all
            
                    
            //print_r($subject); die;
            $content = str_replace('[NAME]', $user['firstName'] . ' ' . $user['lastName'], $content);
            $content = str_replace('[UNIT]', $user['unit'] , $content);

                  Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($userId, $to, $fileName, $templateSubject, $cc) {
                        $message->from("contact@shoptomydoor.com", "Shoptomydoor");
                        //$message->subject("Your New Shopping Address");
                        $message->subject($templateSubject);
                        $message->to($to)->cc($cc);
                        //$message->to($to)->cc("n.udeh@shoptomydoor.com");
                        $message->attach(public_path('exports/invoice/' . $fileName));
                    });

                 return response()->json([
                   'status' => '1'
                 ]);

        }


    }

    public function usershipmentsetting(Request $request)
    {

        $id = $request->input('id');
        

        $user = User::where('id', $id)->where('deleted', '0')->where('setShipmentSettings', '1')->first();

        if (count($user) > 0) {
            return response()->json([
                        'status' => '1',
                        'results' => $user
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => ''
                            ], 200);
        }
    }

    public function usertermssetting(Request $request)
    {
      $id = $request->input('id');
        

        $user = User::where('id', $id)->where('deleted', '0')->where('aggreedToTerms', '1')->first();

        if (count($user) > 0) {
            return response()->json([
                        'status' => '1',
                        'results' => $user
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => ''
                            ], 200);
        }  
    }

}
