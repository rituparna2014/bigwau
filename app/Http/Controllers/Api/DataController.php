<?php

namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouse;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use DB;
use Config;
use customhelper;
use Mail;

class DataController extends Controller {

    public function getallwarehouse() {
        $results = array();
        $allWarehouse = Warehouse::where('status', '1')->where('deleted', '0')->with('country', 'state', 'city')->get();
        $warehouseBanner = \App\Model\Banner::select('imagePath', 'pageLink')->where('type', '3')->where('bannerType', 'image')->where('status','1')->get()->toArray();

       // print_r($warehouseBanner); die;

        if (!empty($allWarehouse)) {
            $results = $allWarehouse;

            for ($i = 0; $i < count($results); $i++) {
                $results[$i]['imagepath'] = url('uploads/warehouse/' . $results[$i]['image']);
                $results[$i]['name'] = $results[$i]['country']['name'];
                $results[$i]['code'] = $results[$i]['country']['code'];
            }
        }
        if(!empty($warehouseBanner))
        {
            for($i = 0; $i < count($warehouseBanner); $i++)
            {
               $banner[$i]['imagePath'] = url('uploads/banner/'.$warehouseBanner[$i]['imagePath']); 
               $banner[$i]['pageLink'] = $warehouseBanner[$i]['pageLink'];
            }
            
        }else{
            $banner[] = array();
        }
        

        return response()->json([
                    'status' => '1',
                    'results' => $results,
                    'banner'=>$banner
                        ], 200);
    }

    public function getcountrylist($fromPage = "", $type = "") {
        $data = array();

        if ($fromPage == 'auto') {
            if ($type == "pickup") {
                $data = \App\Model\Country::where('status', '1')->where('code', 'US')->orderby('name', 'asc')->get();
            } else if ($type == 'destination') {
                $data = \App\Model\Country::where('status', '1')->where('code', 'NG')->orderby('name', 'asc')->get();
            }
        } else {
            $data = \App\Model\Country::where('status', '1')->orderby('name', 'asc')->get();
        }

        return $data->toJson();
    }

    public function getlocationcountrylist() {
        $data = array();

        $location = new \App\Model\Location;
        $country = new Country;
        $locationTable = $location->table;
        $countryTable = $country->table;
        $data = DB::table($countryTable)->select(DB::raw("DISTINCT " . $country->prefix . "$countryTable.id"), "$countryTable.*")
                        ->join("$locationTable", "$countryTable.id", "=", "$locationTable.countryId")
                        ->where("$locationTable.status", "1")
                        ->where("$locationTable.deleted", "0")->get();
        return $data->toJson();
    }

    public function getlocationstatelist($id) {
        $data = array();
        if (!empty($id)) {
            $location = new \App\Model\Location;
            $locationTable = $location->table;

            $country = \App\Model\Country::find($id);
            $countryTable = $country->table;

            $state = new State;
            $stateTable = $state->table;

            $city = new City;
            $cityTable = $city->table;

            $data['stateList'] = DB::table($stateTable)->select(DB::raw("DISTINCT " . $state->prefix . "$stateTable.id"), "$stateTable.*")
                            ->join("$locationTable", "$stateTable.id", "=", "$locationTable.stateId")
                            ->where("$stateTable.status", '1')
                            ->where("$stateTable.deleted", '0')
                            ->where("$locationTable.deleted", '0')
                            ->where("$stateTable.countryCode", $country['code'])
                            ->orderby("$stateTable.name", 'asc')
                            ->get()->toArray();

            $data['cityList'] = DB::table($cityTable)->select(DB::raw("DISTINCT " . $city->prefix . "$cityTable.id"), "$cityTable.*")
                            ->join("$locationTable", "$cityTable.id", "=", "$locationTable.cityId")
                            ->where("$cityTable.status", '1')
                            ->where("$cityTable.deleted", '0')
                            ->where("$locationTable.deleted", '0')
                            ->where("$cityTable.countryCode", $country['code'])
                            ->orderby("$cityTable.name", 'asc')
                            ->get()->toArray();
        }

        return json_encode($data);
    }

    public function getlocationcitylist($id = 0) {
        $data = array();
        if (!empty($id)) {
            $state = \App\Model\State::find($id);

            $location = new \App\Model\Location;
            $city = new City;
            $locationTable = $location->table;
            $cityTable = $city->table;
            $data['cityList'] = DB::table($cityTable)->select(DB::raw("DISTINCT " . $city->prefix . "$cityTable.id"), "$cityTable.*")
                            ->join("$locationTable", "$cityTable.id", "=", "$locationTable.cityId")
                            ->where("$cityTable.stateCode", $state->code)
                            ->where("$cityTable.countryCode", $state->countryCode)
                            ->where("$cityTable.status", "1")
                            ->where("$cityTable.deleted", "0")
                            ->where("$locationTable.deleted", '0')
                            ->get()->toArray();
        }
        return json_encode($data);
    }

    public function getisdcodelist() {
        $data = array();

        $data = \App\Model\Country::select('id', 'isdCode', 'code', 'name')->where('status', '1')->orderby('name', 'asc')->get();

        foreach ($data as $key => $value) {
            $data[$key]['image'] = "https://www.countryflags.io/" . $value['code'] . "/flat/64.png";
        }

        return $data->toJson();
    }

    public function getstatecitylist($id) {
        $data = array();

        $country = \App\Model\Country::find($id);

        $data['stateList'] = \App\Model\State::select('id', 'name')
                        ->where('status', '1')
                        ->where('deleted', '0')
                        ->where('countryCode', $country['code'])
                        ->groupby('id')
                        ->orderby('name', 'asc')
                        ->get()->toArray();


        $data['cityList'] = \App\Model\City::select('id', 'name')
                        ->where('status', '1')
                        ->where('deleted', '0')
                        ->where('countryCode', $country['code']) 
                        ->groupby('id')
                        ->orderby('name', 'asc')
                        ->get()->toArray();

        return json_encode($data);
    }

    public function getcitylist($id = 0) {
        $data = array();
        if (!empty($id)) {
            $state = \App\Model\State::find($id);
            #print_r($state); die;
            $data['cityList'] = \App\Model\City::select('id', 'name')
                            ->where('status', '1')
                            ->where('deleted', '0')
                            ->where('stateCode', $state->code)
                            ->where('countryCode', $state->countryCode)
                            ->groupby('id')
                            ->orderby('name', 'asc')
                            ->get()->toArray();
        }

        return json_encode($data);
    }

    public function getstorelist(Request $request) {
        $data = array();

        $type = $request->type;
        $warehouseId = $request->warehouseId;

        if (!empty($warehouseId)) {
            $data = \App\Model\Stores::getAllStores(array('type' => $type, 'warehouseId' => $warehouseId));
        } else {
            $data = \App\Model\Stores::select('id', 'storeName')
                    ->where('deleted', '0')
                    ->where('status', '1')
                    ->where('storeType', $type)
                    ->get();
        }


        return $data->toJson();
    }

    public function getcategorylist(Request $request) {
        $data = array();

        $type = $request->type;
        $storeId = $request->storeId;

        if (!empty($storeId)) {
            $data = \App\Model\Stores::getAllCategoriesByStore(array('type' => $type, 'storeId' => $storeId));
        } else {
            if (!empty($type)) {
                $data = \App\Model\Sitecategory::select('id', 'categoryName')
                        ->where('parentCategoryId', '-1')
                        ->where('status', '1')
                        ->where('type', $type)
                        ->get();
            } else {
                $data = \App\Model\Sitecategory::select('id', 'categoryName')
                        ->where('parentCategoryId', '-1')
                        ->where('status', '1')
                        ->get();
            }
        }

        return $data->toJson();
    }


    public function getsubcategorylist($id) {
        $data = array();

        $data = \App\Model\Sitecategory::select('id', 'categoryName')
                ->where('parentCategoryId', $id)
                ->where('status', '1')
                ->get();


        return $data->toJson();
    }

    public function getproductlist($id) {
        $results = array();

        $data = \App\Model\Siteproduct::select('id', 'productName', 'image')
                ->where('subCategoryId', $id)
                ->where('status', '1')
                ->get();


        if (!empty($data)) {
            foreach ($data as $key => $eachdata) {
                $results[$key]['id'] = $eachdata->id;
                $results[$key]['productName'] = $eachdata->productName;
                if ($eachdata->image != '')
                    $results[$key]['image'] = url('uploads/site_products/' . $eachdata->image);
                else
                    $results[$key]['image'] = "";
            }
        }

        return json_encode($results);
    }

    public function getmakelist() {
        $data = \App\Model\Automake::where('deleted', '0')->where('type', '0')->orderBy('name', 'ASC')->get();

        return $data->toJson();
    }

    public function getmodellist($makeid) {
        $data = \App\Model\Automodel::where('makeId', $makeid)->where('makeType', '0')->where('deleted', '0')->orderBy('name', 'ASC')->get();

        return $data->toJson();
    }

    public function getcurrencylist(Request $request) {
        $currecnyCode = '';

        if (isset($request->currencyCode) && !empty($request->currencyCode))
            $currecnyCode = $request->currencyCode;

        $data = \App\Model\Currency::getAllCurrencyList($currecnyCode);

        return $data->toJson();
    }

    public function getwarehouselist() {
        $data = \App\Model\Warehouse::warehouseCountryMap();

        return $data->toJson();
    }

    public function getuserwarehouselist($userId) {
        $data = \App\Model\Warehouse::getUserWarehouseList($userId);


        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['imagepath'] = url('uploads/warehouse/' . $data[$i]['image']);
        }



        return $data->toJson();
    }

    public function getshopfromwarehouselist($userId)
    {
         $data = array();
        $data = Warehouse::where('status', '1')->where('deleted', '0')->with('country', 'state', 'city')->groupBy('countryId')->get();
       

       for ($i = 0; $i < count($data); $i++) {
            $data[$i]['imagepath'] = url('uploads/warehouse/' . $data[$i]['image']);
        }

        return $data->toJson();

    }

    public function getallwarehouseshopfrom()
    {
        $data = array();
        $data = Warehouse::where('status', '1')->where('deleted', '0')->with('country', 'state', 'city')->groupBy('countryId')->get();

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['imagepath'] = url('uploads/warehouse/' . $data[$i]['image']);
        }

        return $data->toJson();
    }

    public function getprocessingfee($itemPrice, $warehouseId) {

        $data = \App\Model\Procurementfees::where('feeLeft', '<=', $itemPrice)->where('feeRight', '>=', $itemPrice)->where('warehouseId', $warehouseId)->where('deleted', '0')->first();
        if (count($data) > 0) {
            if ($data->feeType == 'P')
                $processingFee = $itemPrice * ($data->value / 100);
            else if ($data->feeType == 'A')
                $processingFee = $data->value;

            return response()->json([
                        'status' => '1',
                        'results' => $processingFee,
                            ], 200);
        }
        else {
            return response()->json([
                        'status' => '1',
                        'results' => 0,
                            ], 200);
        }
    }

    public function getautoallcharges($userId, $warehouseId, Request $request) {

        $results = array();
        $itemPrice = "0.00";
        $insuranceCost = "0.00";
        $fromCity = "";
        if (abs($request->price) != '')
            $itemPrice = abs($request->price);
        if ($request->fromCity != '')
            $fromCity = $request->fromCity;

        $userShippingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->first();

        $param = array(
            'fromCountry' => $request->fromCountry,
            'fromState' => $request->fromState,
            'fromCity' => $fromCity,
            'toCountry' => $userShippingAddress->countryId,
            'toState' => $userShippingAddress->stateId,
            'toCity' => $userShippingAddress->cityId
        );
        //$defaultCurrency = \App\Model\Currency::getDefaultCurrency();
        $currencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);
        $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
        $exchangeRate = 1;
        /* if($currencyCode == 'USD')
          $exchangeRate = 1;
          else
          $exchangeRate = \App\Model\Currency::currencyExchangeRate('USD',$currencyCode); */

        $autoData = array('make' => $request->make, 'model' => $request->model);
        $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
        if (!empty($shippingCostsArr)) {
            $shippingCost = $shippingCostsArr['totalShippingCost'];
            $insuranceCost = $shippingCostsArr['insurance'];
        }
        $processingFee = \App\Model\Procurementfees::getProcessingFee($warehouseId, $itemPrice);
        $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $fromCity);

        /* if($exchangeRate !=1)
          {
          $shippingCost = \App\Model\Currency::currencyConverter($shippingCost,$exchangeRate);
          $processingFee = \App\Model\Currency::currencyConverter($processingFee,$exchangeRate);
          $pickupCost = \App\Model\Currency::currencyConverter($pickupCost,$exchangeRate);
          } */

        $totalCost = round(($processingFee + $pickupCost + $shippingCost + $itemPrice + $insuranceCost), 2);
        $results['processingFee'] = $processingFee;
        $results['pickupCost'] = $pickupCost;
        $results['shippingCost'] = $shippingCost;
        $results['totalCost'] = $totalCost;
        $results['currencyCode'] = $currencyCode;
        $results['defaultCurrency'] = $currencyCode;
        $results['currencySymbol'] = $currencySymbol;
        $results['exchangeRate'] = $exchangeRate;
        $results['insuranceCost'] = $insuranceCost;
        $results['isInsuranceCharged'] = 'Y';

        return response()->json([
                    'status' => '1',
                    'results' => json_encode($results),
                        ], 200);
    }

    public function getshipmycarallcharges(Request $request) {

        $results = array();
        $formData = $request->formData;
        $userId = $request->userId;
        $warehouseId = $request->warehouseId;
        $itemPrice = "";
        $fromCity = "";
        $insuranceCost = "0.00";
        if (abs($formData['itemPrice']) != '')
            $itemPrice = abs($formData['itemPrice']);
        if ($formData['pickupCity'] != '')
            $fromCity = $formData['pickupCity'];

        $param = array(
            'fromCountry' => $formData['pickupCountry'],
            'fromState' => $formData['pickupState'],
            'fromCity' => $fromCity,
            'toCountry' => $formData['destinationCountry'],
            'toState' => $formData['destinationState'],
            'toCity' => $formData['destinationCity']
        );
        $autoData = array('make' => $formData['makeId'], 'model' => $formData['modelId']);
        $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
        if (!empty($shippingCostsArr)) {
            $shippingCost = $shippingCostsArr['totalShippingCost'];
            $insuranceCost = $shippingCostsArr['insurance'];
        }
        $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $fromCity);
        $totalCost = round(($pickupCost + $shippingCost + $insuranceCost), 2);

        $currencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);
        $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
        $exchangeRate = 1;
        $results['currencyCode'] = $currencyCode;
        $results['defaultCurrency'] = $currencyCode;
        $results['currencySymbol'] = $currencySymbol;
        $results['exchangeRate'] = $exchangeRate;
        $results['pickupCost'] = $pickupCost;
        $results['shippingCost'] = $shippingCost;
        $results['totalCost'] = $totalCost;
        $results['insuranceCost'] = $insuranceCost;
        $results['isInsuranceCharged'] = 'Y';
        return response()->json([
                    'status' => '1',
                    'results' => json_encode($results),
                        ], 200);
    }

    public function getpaymentmethodlist(Request $request) {
        $paymentMethodList = \App\Model\Paymentmethod::getallpaymentmethods($request->billingCountryId);
        return json_encode($paymentMethodList);
    }

    public function getdefaultcurrency() {
        $defaultcurrency = \App\Helpers\customhelper::getCurrencySymbol();

        return response()->json([
                    'defaultcurrency' => $defaultcurrency
                        ], 200);
    }
    
    public function getdefaultcurrencycode() {
        $defaultcurrency = \App\Helpers\customhelper::getCurrencySymbolCode('',true);

        return response()->json([
                    'defaultcurrencyCode' => $defaultcurrency
                        ], 200);
    }

    public function getamountforpaystack(Request $request) {
        $payAmount = round($request->payAmount, 2);
        $defaultCurrency = $request->defaultCurrency;
        
        if($defaultCurrency != 'NGN')
        {
           $exchangeRateToNaira = \App\Model\Currency::currencyExchangeRate($defaultCurrency, 'NGN');
           $convertedAmount = round(($payAmount * $exchangeRateToNaira), 2); 
        }else{
            $convertedAmount = $payAmount;
        }
        
        $amountForPaystack = round($convertedAmount * 100, 2);
        return response()->json([
                    'amountForPaystack' => $amountForPaystack
                        ], 200);
    }
    
    public function getamountforpayeezy(Request $request) {
        
        $payAmount = round($request->payAmount, 2);
        $defaultCurrency = $request->defaultCurrency;
        
        if($defaultCurrency != 'USD')
        {
           $exchangeRateToNaira = \App\Model\Currency::currencyExchangeRate($defaultCurrency, 'USD');
           $convertedAmount = ($payAmount * $exchangeRateToNaira); 
        }else{
            $convertedAmount = $payAmount;
        }
        
        $amountDisplay = round($convertedAmount,2);
        $amountForPayeezy = round($amountDisplay*100);
        //$amountForPayeezy = $amountDisplay*100;
        return response()->json([
                    'amountForPayeezy' => $amountForPayeezy,
                    'amountDisplay' => $amountDisplay,
                        ], 200);
    }

    public function itemarriveddetails_Old(Request $request) {

        $trackingNumber = $request->trackingNumber;
        $result = array();
        $arr = array();
        $isTrackinExist = \App\Model\Procurementitem::where('trackingNumber', $trackingNumber)->orWhere('tracking2', $trackingNumber)->get();
        if ($isTrackinExist->count() > 0) {
            $allProcurementStatus = \App\Model\Procurementitem::allStatus();
            $storeData = collect(\App\Model\Stores::where('deleted', '0')->where('status', '1')->get());
            $storeList = $storeData->mapWithKeys(function($item) {
                return [$item['id'] => $item['storeName']];
            });
            foreach ($isTrackinExist as $eachTracking) {
                $trackStatus = '';
                $trackStatus = \App\Model\Procurementitemstatus::where('procurementId', $eachTracking->procurementId)->where('procurementItemId', $eachTracking->id)->get();
                $newArr = array();
                if ($trackStatus->count() > 0) {
                    $itemArr = array('store' => $storeList[$eachTracking->storeId],
                        'itemName' => $eachTracking->itemName);
                    $statusdata = array();
                    foreach ($trackStatus as $eachStatus) {
                        $statusdata[] = array(
                            'status' => $allProcurementStatus[$eachStatus->status],
                            'date' => $eachStatus->updatedOn,
                            'completed' => true,
                            'visited' => true,
                        );
                    }
                    $itemArr['statusDetails'] = $statusdata;
                    array_push($result, $itemArr);
                }
            }
        }

        $isTrackinExist = \App\Model\Shipmentpackage::where('tracking', $trackingNumber)->orWhere('tracking2', $trackingNumber)->get();
        if ($isTrackinExist->count() > 0) {
            $allShipmentStatus = \App\Model\Shipmentstatuslog::allStatus();
            $storeData = collect(\App\Model\Stores::where('deleted', '0')->where('status', '1')->get());
            $storeList = $storeData->mapWithKeys(function($item) {
                return [$item['id'] => $item['storeName']];
            });
            foreach ($isTrackinExist as $eachTracking) {
                $trackStatus = '';
                $trackStatus = \App\Model\Shipmentstatuslog::where('shipmentId', $eachTracking->shipmentId)->where('deliveryId', $eachTracking->deliveryId)->get();
                $newArr = array();
                if ($trackStatus->count() > 0) {
                    $itemArr = array('store' => $storeList[$eachTracking->storeId],
                        'itemName' => $eachTracking->itemName);
                    $statusdata = array();
                    foreach ($trackStatus as $eachStatus) {
                        $statusdata[] = array(
                            'status' => $allShipmentStatus[$eachStatus->status],
                            'date' => $eachStatus->updatedOn,
                            'completed' => true,
                            'visited' => true,
                        );
                    }
                    $itemArr['statusDetails'] = $statusdata;
                    array_push($result, $itemArr);
                }
            }
        }


        if (!empty($result)) {
            return response()->json([
                        'status' => '1',
                        'results' => $result,
                            ], 200);
        } else
            return response()->json([
                        'status' => '-1',
                        'result' => '',
                            ], 200);
    }
    
    public function itemarriveddetails(Request $request) {

        $trackingNumber = $request->trackingNumber;
        $result = array();
        
        $shipmentArrivedDetails = \App\Model\Shipmenttrackingnumber::where('trackingNumber','LIKE',"%$trackingNumber%")->first();
        if (!empty($shipmentArrivedDetails)) {

            //Search for the item in quick ship out which are in shipment tracking

            $itemInQuickshipout = \App\Model\Commercialinvoice::where('invoiceDetails','LIKE',"%$trackingNumber%")->first();

            if(!empty($itemInQuickshipout))
            {
                //Find Quick shipout and get details of log

                $quickshipoutId = $itemInQuickshipout->id;

                $details = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $quickshipoutId)->groupBy('status')->orderBy('id')->get();

                $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatusQuickShipout();
                $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);

                 $statusCompletedIndex = 0;
                        if ($details->count() > 0) {
                            foreach ($details as $eachStatus) {
                                $statusdata[] = array(
                                    'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                    'date' => $eachStatus->updatedOn,
                                    'completed' => true,
                                    'visited' => true,
                                    'comment' => $eachStatus->comments,
                                );
                                $statusCompletedIndex++;
                            }
                        }
                        for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                            $statusdata[] = array(
                                'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                                'date' => '',
                                'completed' => FALSE,
                                'visited' => FALSE,
                            );
                        }

                        $itemArr['statusDetails'] = $statusdata;
                        array_push($result, $itemArr);

                return response()->json([
                        'status' => '2',
                        'results' => $result,
                            ], 200);
            }
            else{
                return response()->json([
                        'status' => '1',
                        'results' => \Carbon\Carbon::parse($shipmentArrivedDetails->createdOn)->format('m-d-Y h:i a'),
                            ], 200);
            }
            
        } else
        {
            //Search for the item in quick ship out which are not in shipment tracking
            
            $itemInQuickshipout = \App\Model\Commercialinvoice::where('invoiceDetails','LIKE',"%$trackingNumber%")->first();

            if(!empty($itemInQuickshipout))
            {
                //Find Quick shipout and get details of log

                $quickshipoutId = $itemInQuickshipout->id;

                $details = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $quickshipoutId)->groupBy('status')->orderBy('id')->get();

                $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatusQuickShipout();
                $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);

                 $statusCompletedIndex = 0;
                        if ($details->count() > 0) {
                            foreach ($details as $eachStatus) {
                                $statusdata[] = array(
                                    'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                    'date' => $eachStatus->updatedOn,
                                    'completed' => true,
                                    'visited' => true,
                                    'comment' => $eachStatus->comments,
                                );
                                $statusCompletedIndex++;
                            }
                        }
                        for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                            $statusdata[] = array(
                                'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                                'date' => '',
                                'completed' => FALSE,
                                'visited' => FALSE,
                            );
                        }

                        $itemArr['statusDetails'] = $statusdata;
                        array_push($result, $itemArr);

                return response()->json([
                        'status' => '2',
                        'results' => $result,
                            ], 200);
            }else{
                return response()->json([
                        'status' => '-1',
                        'result' => '',
                            ], 200);

            }
            
        }
    }
    
    public function verifyitemarrivedtime(Request $request) {
        
        $deliveryDate = date('Y-m-d',strtotime($request->deliveryDate));
        //$deliveryTime = date('H:i:s',strtotime($request->deliveryTime));

        $fullDate = strtotime($deliveryDate);
        $difference = floor((time()-$fullDate)/3600);
        if($difference>24)
        {
            return response()->json([
                        'status' => '1',
                        'results' => 'showform',
                            ], 200);
        }
        else
        {
            return response()->json([
                        'status' => '2',
                        'results' => 'wait',
                            ], 200);
        }
    }
    
    public function verifytrackingdetails(Request $request) {
        //print_r($request->all());
        $name = $request->customerName;
        $email = $request->customerEmail;
        $unit = $request->customerUnit;
        $store = $request->customerPurchaseStore;
        $tracking = $request->customerTracking;
        $photoFilePath = "";
        $invFilePath = "";
        
        if ($request->hasFile('invoiceFile')) {
            
            $invFile = $request->file('invoiceFile');
            $invFileName = time() . '_' . str_replace(" ", "_", $invFile->getClientOriginalName());
            $invDestinationPath = public_path('/uploads/verifytracking');
            $invFilePath = $invDestinationPath."/".$invFileName;
            $invFile->move($invDestinationPath, $invFileName);
        }
        
        if ($request->hasFile('itemPhotoFile')) {
            
            $photoFile = $request->file('itemPhotoFile');
            $photoFileName = time() . '_' . str_replace(" ", "_", $photoFile->getClientOriginalName());
            $photoDestinationPath = public_path('/uploads/verifytracking');
            $photoFilePath = $photoDestinationPath."/".$photoFileName;
            $photoFile->move($photoDestinationPath, $photoFileName);
        } 
        
        $content = "<p>Dear Admin,</p>";
        $content .= "<p>The following customers items have not been updated since 48 hours. Please update immediately.</p>";
        $content .= "<p>Name – $name</p>";
        $content .= "<p>Email – $email</p>";
        $content .= "<p>Unit Number – $unit</p>";
        $content .= "<p>Store of Purchase – $store</p>";
        $content .= "<p>Tracking Number – $tracking</p>";
        $from = $email;
        $to = "contact@shoptomydoor.com";
        $subject = "Update Delay From Customer";
        $mailSent = Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($subject,$from, $to,$photoFilePath,$invFilePath) {
            $message->from($from, $from);
            $message->subject($subject);
            $message->to($to);
            if($photoFilePath!="")
                $message->attach($photoFilePath);
            if($invFilePath != "")
                $message->attach($invFilePath);
        });
        
//        if($mailSent)
//        {
            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                            ], 200);
//        }
//        else
//        {
//            return response()->json([
//                        'status' => '0',
//                        'results' => 'error',
//                            ], 200);
//        }
    }

    public function shipmenttrackingdetails(Request $request) {

        $orderNumber = $request->orderNumber;
        $result = array();
        $orderData = \App\Model\Order::where('orderNumber', $orderNumber)->first();
        if (!empty($orderData)) {

            if ($orderData->type == 'shipment') {
                $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatus();
                $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);
                $deliveryData = \App\Model\Shipmentdelivery::where('shipmentId', $orderData->shipmentId)->where("deleted","0")->get();
                $deliveryNum = 1;
                if ($deliveryData->count() > 0) {
                    foreach ($deliveryData as $eachDeliveryData) {
                        $shippingMethodDetails = \App\Model\Shippingmethods::where('shippingid',$eachDeliveryData->shippingMethodId)->first();
                        if(!empty($shippingMethodDetails))
                            $itemArr = array('deliveryId' => $deliveryNum,'shippingMethod' => $shippingMethodDetails->shipping,'shippingMethodLogo'=>url('uploads/shipping/'.$shippingMethodDetails->companyLogo));
                        else
                            $itemArr = array('deliveryId' => $deliveryNum,'shippingMethod' => '','shippingMethodLogo'=>'');
                        $statusdata = array();
                        $shipmentStatusLog = \App\Model\Shipmentstatuslog::where('shipmentId', $orderData->shipmentId)->where('deliveryId', $eachDeliveryData->id)->get();
                        $statusCompletedIndex = 0;
                        if ($shipmentStatusLog->count() > 0) {
                            foreach ($shipmentStatusLog as $eachStatus) {
                                $statusdata[] = array(
                                    'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                    'date' => $eachStatus->updatedOn,
                                    'completed' => true,
                                    'visited' => true,
                                    'comment' => $eachStatus->comments,
                                );
                                $statusCompletedIndex++;
                            }
                        }
                        for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                            $statusdata[] = array(
                                'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                                'date' => '',
                                'completed' => FALSE,
                                'visited' => FALSE,
                            );
                        }
                        $itemArr['statusDetails'] = $statusdata;
                        array_push($result, $itemArr);
                        $deliveryNum++;
                    }
                }
            } else if ($orderData->type == 'autoshipment') {
                $shipmentDeliveryStatus = \App\Model\Autoshipment::allStatus();
                $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);
                $itemArr = array('deliveryId' => '','shippingMethod'=>'','shippingMethodLogo'=>'');
                $statusdata = array();
                $shipmentStatusLog = \App\Model\Autoshipmentstatuslog::where('autoshipmentId', $orderData->shipmentId)->get();
                $statusCompletedIndex = 0;
                if ($shipmentStatusLog->count() > 0) {
                    foreach ($shipmentStatusLog as $eachStatus) {
                        $statusdata[] = array(
                            'status' => $shipmentDeliveryStatus[$eachStatus->status],
                            'date' => $eachStatus->updatedOn,
                            'completed' => true,
                            'visited' => true,
                            
                        );
                        $statusCompletedIndex++;
                    }
                }
                for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                    $statusdata[] = array(
                        'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                        'date' => '',
                        'completed' => FALSE,
                        'visited' => FALSE,
                    );
                }
                $itemArr['statusDetails'] = $statusdata;
                array_push($result, $itemArr);
            }
            
            if ($orderData->type == 'fillship') {
                $itemArr['statusDetails'] = \App\Model\Fillshipshipmentstatuslog::getStatusLog($orderData->shipmentId);
                array_push($result, $itemArr);
            }
        }

        if (!empty($result)) {
            return response()->json([
                        'status' => '1',
                        'results' => $result,
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'result' => '',
                            ], 200);
        }
    }

    public function getlocationdetails(Request $request) {

        $countryId = $request->countryId;
        $stateId = $request->stateId;
        $cityId = $request->cityId;
        $locationDetails = array();
        $where = "countryId=" . $countryId . " AND cityId=" . $cityId;
        if ($stateId != "")
            $where .= " AND stateId=" . $stateId;
        $locationDetails = \App\Model\Location::whereRaw($where)
                        ->where('status', '1')->where('deleted', '0')->get();


        if ($locationDetails->count() > 0) {
            return response()->json([
                        'status' => '1',
                        'results' => $locationDetails,
                            ], 200);
        } else {
            return response()->json([
                        'status' => '-1',
                        'results' => 'No location found for selected country/state/city',
                            ], 200);
        }
    }

    public function getstoreByCatergory(Request $request) {
        $categoryId = $request->categoryId;
        $type = $request->type;

        $result = array();

        $store = new \App\Model\Stores;
        $category = new \App\Model\Sitecategory;
        $storemapping = new \App\Model\Storecountrymapping;

        $categoryTable = $category->table;
        $storemappingTable = $storemapping->table;
        $storeTable = $store->table;


        $result = \App\Model\Stores::select($storeTable . '.*', $categoryTable . ".categoryName", $categoryTable . ".id AS categoryId")->join($storemappingTable, $storemappingTable . ".storeId", "=", $storeTable . ".id")->join($categoryTable, $storemappingTable . ".categoryId", "=", $categoryTable . ".id")->where($storemappingTable . '.categoryId', $categoryId)->where($storeTable . '.storeType', 'shopforme')->where($storeTable . '.status', '1')->where($storeTable . '.deleted', '0')->groupby($storeTable . '.id')->get();

        if(count($result) == 0)
        {
            $category = \App\Model\Sitecategory::where('id', $categoryId)->pluck('categoryName')->toArray();  
        }

        

        for ($i = 0; $i < count($result); $i++) {
            if (!empty($result[$i]['storeIcon'])) {
                $result[$i]['iconpath'] = url('/uploads/stores/' . $result[$i]['storeIcon']);
            } else {
                $result[$i]['iconpath'] = url('assets/imgs/image-not-available.jpg');
            }
        }


        if (count($result)>0) {
            return response()->json([
                        'status' => '1',
                        'result' => $result,
                            ], 200);
            
        } else {            
            return response()->json([
                        'status' => '2',
                        'result' => $category[0],
                            ], 200);
        }
    }


    public function getamountforpaypalstandard(Request $request) {
        $payAmount = round($request->payAmount, 2);
        $defaultCurrency = $request->defaultCurrency;

        /* GET THE PAYPAL STANDARD CREDENTIALS */
        $data = array();

        $paymentMethodList = \App\Model\Paymentmethod::where("paymentMethodKey", 'paypalstandard')->first();
        if(!empty($paymentMethodList))
        $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", $paymentMethodList->id)->get();
        $data['setUrl'] = $getSettings[0]->configurationValues;
        $data['Merchantemail'] = $getSettings[1]->configurationValues;
        $data['NotificationUrl'] = $getSettings[2]->configurationValues;
        $data['CancelUrl'] = $getSettings[3]->configurationValues;
        $data['SuccessUrl'] = $getSettings[4]->configurationValues;

        if($defaultCurrency != 'USD'){
            $exchangeRateToUSD = \App\Model\Currency::currencyExchangeRate($defaultCurrency, 'USD');
            $convertedAmount = round(($payAmount * $exchangeRateToUSD), 2);
            return response()->json([
                        'amountForPaypalStandard' => $convertedAmount,
                        'credentials' => $data
                            ], 200);
        } else{
            return response()->json([
                        'amountForPaypalStandard' => $payAmount,
                        'credentials' => $data
                            ], 200);
        }
    }
    
    public function savepaystackreference(Request $request) {
        //print_r($request->all());exit;
        if(!empty($request->paystackReference))
        {
            
            $postData = $request->paymentData;
            if(is_array($postData))
            {
                $postData = json_encode($postData);
            }
            if(!is_array($request->paymentData))
                $request->paymentData = json_decode($request->paymentData,true);
            $amt = '0.00';
            $paidCurrency = 'USD';
            if($request->paymentUrl == 'shop-for-me' || $request->paymentUrl == 'fill-and-ship' || $request->paymentUrl == 'auto-parts')
            {
                if($request->paymentUrl == 'auto-parts')
                    $userData = \App\Model\User::where('email',$request->paymentData['personalDetails']['userEmail'])->first();
                else
                    $userData = \App\Model\User::where('email',$request->paymentData['personaldetails']['userEmail'])->first();
                $amt = $request->paymentData['data']['totalCost']+$request->paymentData['data']['totalTax'];
                $paidCurrency = $request->paymentData['currencyCode'];
                if(!empty($userData))
                {
                   $type = 'shopforme';
                   if($request->paymentUrl == 'fill-and-ship') 
                       $type = 'fillship';
                   elseif($request->paymentUrl == 'auto-parts')
                       $type = 'autoparts';
                   $userCartData = \App\Model\Usercart::where('type',$type)->where('userId',$userData->id)->where('warehouseId',$request->paymentData['data']['warehouseId'])->first();
                }
                
                $paidFor = !empty($userCartData) ? $userCartData->id : "-1";
            }
            else if($request->paymentUrl == 'my-warehouse')
            {
                $paidFor = $request->paymentData['id'];
                $amt = $request->paymentData['totalCost']+$request->paymentData['totalTax'];
                $paidCurrency = $request->paymentData['currencyCode'];
            }
            else if($request->paymentUrl == 'buy_a_car' || $request->paymentUrl == 'ship_my_car')
            {
                $userData = \App\Model\User::find($request->userId);
                if(!empty($userData))
                {
                    $userCartData = \App\Model\Usercart::where('type',$request->paymentUrl)->where('userId',$userData->id)->first();
                }
                $paidFor = !empty($userCartData) ? $userCartData->id : "-1";
                $amt = round($request->paymentData['totalCost'],2);
                $paidCurrency = $request->paymentData['currencyCode'];
            }
            else if($request->paymentUrl == 'my-warehouse-payment')
            {
                $paidFor = $request->paymentData['returnData'][0]['shipmentId'];
                $amt = $request->paymentData['returnData'][0]['paidAmount'];
                $paidCurrency = \App\Helpers\customhelper::getCurrencySymbolCode('',true);
            }
            else if($request->paymentUrl == 'payment')
            {
                $paidFor = $request->paymentData['data']['invoiceId'];
                $amt = $request->paymentData['data']['paidAmount'];
                $paidCurrency = \App\Helpers\customhelper::getCurrencySymbolCode('',true);
            }

            $convertedAmount = 0;
            $payAmount = round($amt, 2);
            $defaultCurrency = $paidCurrency;

            if($defaultCurrency != 'NGN')
            {
               $exchangeRateToNaira = \App\Model\Currency::currencyExchangeRate($defaultCurrency, 'NGN');
               $convertedAmount = round(($payAmount * $exchangeRateToNaira), 2); 
            }else{
                $convertedAmount = $payAmount;
            }
            
            $amountForPaystack = round($convertedAmount * 100, 2);
            
            $paystackReference = new \App\Model\Paystackreferencehistory;
            $paystackReference->userId = $request->userId;
            $paystackReference->warehouseId = $request->warehouseId;
            $paystackReference->paidType = $request->paymentUrl;
            $paystackReference->postData = $postData;
            $paystackReference->paidFor = $paidFor;
            $paystackReference->referenceNo = $request->paystackReference;
            $paystackReference->amountPayable = $amt;
            $paystackReference->amountForPaystack = $amountForPaystack;
            $paystackReference->paidCurrency = $paidCurrency;
            $paystackReference->updatedAt = Config::get('constants.CURRENTDATE');
            $paystackReference->save();
        }
        
        return response()->json([
            'status' => '1',
        ], 200);
    }
    
    public function processpayeezy() {
        
        $data = \App\Model\Paymenttransaction::processpayeezy();
        
        return response()->json([
            'status' => $data,
        ], 200); 
    }

}
