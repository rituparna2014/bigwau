<?php
namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Emailtemplate;
use customhelper;
use App\Model\Paymentmethod;
use App\Model\Currency;

class PayController extends Controller {

    /**
     * Method used to pay through Authorize.net payment gateway
     * @param request $request 
     * @return string
     */
    public function payauthorizedotnet(Request $request) {

        /* CALL A COMMON FUNCTION TO RETRIEVE THE SETTING VALUES */
        $getSettings = $this->getSettingsDetails(2);
        
        /* CALL FUNCTION TO GET THE DEFAULT CURRENCY */
        $defaultCurrency = $this->getdefaultcurrency();


        /* SET VARIABLES */
        $cardNumber     =   $request->formvalues['cardNumber'];
        $expMonth       =   $request->formvalues['expMonth'];
        $expYear        =   $request->formvalues['expYear'];
        $cardCode       =   $request->formvalues['cardCode'];
        $expMonthYear   =   $expYear."-".$expMonth;

        $url = $getSettings["setUrl"];
        $API_Key = $getSettings['API_Key'];
        $transactionKey = $getSettings['Transaction_Key'];

        /* SET THE AMOUNT IN USD FORMAT */
        $amount = 5.05 * $defaultCurrency;

        /* SET THE CUSTOMER NAME */
        $customerFirstName      = "Ellen";
        $customerLastName       = "Johnson";
        $customerAddress        = "14 Main Street";
        $customerCity           = "Pecan Springs";
        $customerState          = "TX";
        $customerZip            = "44628";
        $customerCountry        = "USA";

        /* SET OTHER VARIABLES */
        $invoiceNumber = "INV-".rand(00, 99)."-".time();
        $description = "From ShoptoMyDoor";


$query = <<<EOT
<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
  <merchantAuthentication>
    <name>$API_Key</name>
    <transactionKey>$transactionKey</transactionKey>
  </merchantAuthentication>
  <transactionRequest>
    <transactionType>authCaptureTransaction</transactionType>
    <amount>$amount</amount>
    <payment>
      <creditCard>
        <cardNumber>$cardNumber</cardNumber>
        <expirationDate>$expMonthYear</expirationDate>
        <cardCode>$cardCode</cardCode>
      </creditCard>
    </payment>
    <order>
     <invoiceNumber>$invoiceNumber</invoiceNumber>
     <description>$description</description>
    </order>
    <billTo>
      <firstName>$customerFirstName</firstName>
      <lastName>$customerLastName</lastName>
      <company></company>
      <address>$customerAddress</address>
      <city>$customerCity</city>
      <state>$customerState</state>
      <zip>$customerZip</zip>
      <country>$customerCountry</country>
    </billTo>
    <shipTo>
      <firstName>$customerFirstName</firstName>
      <lastName>$customerLastName</lastName>
      <company></company>
      <address>$customerAddress</address>
      <city>$customerCity</city>
      <state>$customerState</state>
      <zip>$customerZip</zip>
      <country>$customerCountry</country>
    </shipTo>
  </transactionRequest>
</createTransactionRequest>
EOT;

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
curl_setopt($ch, CURLOPT_POSTFIELDS, "$query");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);
curl_close($ch);

$output = $this->func_filter_output($output);

    preg_match("/<code>(.*?)<\/code>/", $output, $matches);
    if (count($matches) > 0) { 
        $message = $matches[1];
        if($message == "I00001"){
            $message = "Transaction successful";
        } else {
          $message;
        }
    } else {
        $message = "Transaction error!";
    }
    
    return response()->json([
    'results' => $message
        ], 200);

    }

    /**
     * Method used to extract field from xml data
     * @param integer $page 
     * @return html
     */

    function func_filter_output($ab_response) {
        $ab_response = explode("
        ", $ab_response);

        $_ab_response = '';
        foreach ($ab_response as $k => $v) {
            $elem = trim($v);
            if (strlen($elem) > 4 && $elem != '0fe8')
                $_ab_response .= $elem;
        }
        $ab_response = $_ab_response;
        return $ab_response;
    }

    /**
     * Method used to pay through PayPal Pro DoDirect payment gateway
     * @param request $request 
     * @return string
     */
    public function paypaypalpro(Request $request) {

    /* CALL A COMMON FUNCTION TO RETRIEVE THE SETTING VALUES */
    $getSettings = $this->getSettingsDetails(1);

    /* CALL FUNCTION TO GET THE DEFAULT CURRENCY */
    $defaultCurrency = $this->getdefaultcurrency();
     
    // SET API CREDENTIALS */
    $api_version    = '85.0';
    $api_endpoint   = $getSettings["setUrl"];
    $api_username   = $getSettings["API_Key"];
    $api_password   = $getSettings["API_Password"];
    $api_signature  = $getSettings["API_Signature"];

    /* SET VARIABLES */
    $cardNumber     =   $request->formvalues['cardNumber'];
    $expMonth       =   $request->formvalues['expMonth'];
    $expYear        =   $request->formvalues['expYear'];
    $cardCode       =   $request->formvalues['cardCode'];
    $expMonthYear   =   $expMonth."".$expYear;

    /* SET THE AMOUNT IN USD FORMAT */
    $amount = 5.05 * $defaultCurrency;

    /* SET THE CUSTOMER NAME */
    $customerFirstName      = "Ellen";
    $customerLastName       = "Johnson";
    $customerAddress        = "14 Main Street";
    $customerCity           = "Pecan Springs";
    $customerState          = "TX";
    $customerZip            = "44628";
    $customerCountry        = "USA";

    /* SET OTHER VARIABLES */
    $description = "From ShoptoMyDoor";

    // Store request params in an array
    $request_params = array
    (
    'METHOD' => 'DoDirectPayment', 
    'USER' => $api_username, 
    'PWD' => $api_password, 
    'SIGNATURE' => $api_signature, 
    'VERSION' => $api_version, 
    'PAYMENTACTION' => 'Sale',                   
    'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
    'ACCT' => $cardNumber,                        
    'EXPDATE' => $expMonthYear,           
    'CVV2' => $cardCode, 
    'FIRSTNAME' => $customerFirstName, 
    'LASTNAME' => $customerLastName, 
    'STREET' => $customerAddress, 
    'CITY' => $customerCity, 
    'STATE' => $customerState,                     
    'COUNTRYCODE' => $customerCountry, 
    'ZIP' => $customerZip, 
    'AMT' => $amount, 
    'CURRENCYCODE' => 'USD', 
    'DESC' => $description
    );

    // Loop through $request_params array to generate the NVP string.
    $nvp_string = '';
    foreach($request_params as $var=>$val)
    {
        $nvp_string .= '&'.$var.'='.urlencode($val);    
    }



    // Send NVP string to PayPal and store response
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_VERBOSE, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_URL, $api_endpoint);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
     
    $result = curl_exec($curl);     
    curl_close($curl);
    #print_r($result);
    // Parse the API response
    parse_str($result, $nvp_response_array);

    #print_r($nvp_response_array);

    if($nvp_response_array["ACK"] == 'Success' || $nvp_response_array["ACK"] == 'uccessWithWarning'){
      $message = "Transaction successful";
    } else {
      $message = "Transaction error!";
    }

    return response()->json([
    'results' => $message
        ], 200);

    }

    /**
     * Method used to get the method (Authorize dot net OR PayPal DoDirect)
     * @param request $request 
     * @return string
     */
    public function getpaymethod(Request $request) {

      $getMethod = \App\Model\Paymentmethod::where('paymentMethod', 'Credit Card')->get();

      return response()->json([
        'results' => $getMethod
        ], 200);

    }

    /**
     * Method used to get the settings of Authorize dot net or PayPal DoDirect payment
     * @param request $request 
     * @return string
     */
    public function getpaysettings($activeId) {
      $data = [];
      $data = Paymentmethod::configurationSettings($activeId)->toArray();

      return $data;

    }

    /**
     * Method used to get the default currency 
     * @return array
     */
    public function getdefaultcurrency(){
      $data = [];
      $data = Currency::where('isDefault', '1')->get();
      if($data[0]->currencyId != 71){
          $exchangeRate = $data[0]->exchangeRate;
      } else {
          $exchangeRate = 1;
      }
      return $exchangeRate;
    } 

    /**
     * Method used to get the default currency 
     * @param integer $id 
     * @return array
     */
    public function getSettingsDetails($id){

        /* CALL FUNCTION TO GET THE SETTINGS */
        $dataSettings = $this->getpaysettings($id);
        
        $data = array();

        if($id == 2){
          foreach($dataSettings as $key => $val){
            
            if($val['configurationKeys'] == 'API Login ID'){
              $API_Key = $val['configurationValues'];
            }

            if($val['configurationKeys'] == 'Transaction key'){ 
              $Transaction_Key = $val['configurationValues'];
            }

            if($val['configurationKeys'] == 'Test/Live mode'){ 
              $Url = $val['configurationValues'];
              
              if($Url == "Test"){
                $setUrl = "https://apitest.authorize.net/xml/v1/request.api";
              } else { 
                $setUrl = "https://api.authorize.net/xml/v1/request.api";
              }
            }
            
          }

            $data['setUrl'] = $setUrl;
            $data['API_Key'] = $API_Key;
            $data['Transaction_Key'] = $Transaction_Key;

        } else {
          foreach($dataSettings as $key => $val){
           
            if($val['configurationKeys'] == 'Test/Live mode'){ 
              $Url = $val['configurationValues'];
              if($Url == "Test"){
                $setUrl = "https://api-3t.sandbox.paypal.com/nvp";
              } else {
                $setUrl = "https://api-3t.paypal.com/nvp";
              }
            }

            if($val['configurationKeys'] == 'API access username'){ 
              $API_Key = $val['configurationValues'];
            }

            if($val['configurationKeys'] == 'API access password'){ 
              $API_Password = $val['configurationValues'];
            }

            if($val['configurationKeys'] == 'API signature'){ 
              $API_Signature = $val['configurationValues'];
            }
          }

          $data['setUrl'] = $setUrl;
          $data['API_Key'] = $API_Key;
          $data['API_Password'] = $API_Password;
          $data['API_Signature'] = $API_Signature;

        }

        return $data;

    }
    

}
