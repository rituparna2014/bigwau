<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;

class UserPermission extends Authenticatable {

    public $table;
    public $timestamps = false;
    public $prefix;

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.ADMINUSERROLEPERM');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Insert permission for admin user 
     * @param integer $adminMenuId
     * @param integer $userAdminId
     * @param string $permissionView
     * @param string $permissionAdd
     * @param string $permissionEdit
     * @param string $permissionDelete
     * @return object
     */
    public static function insertPermission($adminMenuId, $userAdminId, $permissionView, $permissionAdd, $permissionEdit, $permissionDelete) {

        $records = Adminmenuuserpermission::insert(
                ['adminMenuId' => $adminMenuId, 'adminUserId' => $userAdminId,
                    'permissionView' => $permissionView, 'permissionAdd' => $permissionAdd,
                    'permissionEdit' => $permissionEdit, 'permissionDelete' => $permissionDelete
        ]);
        return $records;
    }

    /**
     * Get permission data 
     * @param integer $id
     * @param integer $fieldType
     * @return object
     */
    public static function getPermissionRecords($id, $fieldType) {

        $records = Adminmenuuserpermission::select('adminMenuId', 'id', 'adminUserId', 'permissionView', 'permissionAdd', 'permissionEdit', 'permissionDelete')
                ->where($fieldType, '=', $id)
                ->orderBy('id', 'ASC')
                ->get();
        return $records;
    }

    /**
     * Get permission data by user Id
     * @param integer $adminMenuId
     * @param integer $adminUserId
     * @return object
     */
    public static function getPermissionRecordsByUserId($adminMenuId, $adminUserId) {

        $records = Adminmenuuserpermission::select('adminMenuId', 'id', 'adminUserId', 'permissionView', 'permissionAdd', 'permissionEdit', 'permissionDelete')
                ->where('adminMenuId', '=', $adminMenuId)
                ->where('adminUserId', '=', $adminUserId)
                ->get();
        return $records;
    }

    /**
     * Generate the left menu of admin panel
     * @param integer $startAt
     * @param integer $userId
     * @return object
     */
    public static function getLeftNavByUserId($startAt, $userId) { 

        $adminMenuMaster = new Adminmenumaster();
        $adminMenuMasterTable = $adminMenuMaster->table;

        $adminMenuPermission = new Adminmenuuserpermission();
        $adminMenuPermissionTable = $adminMenuPermission->table;

        $records = Adminmenumaster::select($adminMenuMasterTable.'.adminMenuName', $adminMenuMasterTable.'.adminMenuSlug', 
            $adminMenuMasterTable.'.adminMenuParentId', 
            $adminMenuMasterTable.'.adminMenuIcon', $adminMenuPermissionTable.'.adminMenuId', 
            $adminMenuPermissionTable.'.id', $adminMenuPermissionTable.'.adminUserId', 
            $adminMenuPermissionTable.'.permissionView', $adminMenuPermissionTable.'.permissionAdd', 
            $adminMenuPermissionTable.'.permissionEdit', 
            $adminMenuPermissionTable.'.permissionDelete')
                ->leftJoin($adminMenuPermissionTable, $adminMenuMasterTable.'.id', '=', $adminMenuPermissionTable.'.adminMenuId')
                ->where($adminMenuPermissionTable.'.adminUserId', '=', $userId)
                ->where($adminMenuMasterTable.'.adminMenuParentId', '=', $startAt)
                ->where($adminMenuMasterTable.'.status', '=', 1)
                ->orderByRaw('CAST(adminMenuOrder AS DECIMAL(10,2))')
                ->get();

        return $records;
    }

}
