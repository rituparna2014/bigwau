<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Adminmenumaster extends Model {

    public $table;
    public $timestamps = false;
    public $prefix;

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.ADMINMENUMASTER');
        $this->prefix = DB::getTablePrefix();
    }

}
