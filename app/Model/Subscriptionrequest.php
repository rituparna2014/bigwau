<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Subscriptionrequest extends Model
{
    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.SUBSCRIPTIONREQUESTS');
    }

   
}