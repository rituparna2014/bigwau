<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Service extends Model
{
    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.SERVICES');
    }

    public function getData($param) {

    	$where = "deleted='0'";

        if (!empty($param['searchData']))
            $where .= "  AND (serviceName like '%" . addslashes($param['searchData']) . "%') ";
        
       

        /* Print query ->toSql() dd($resultSet); */
        $resultSet = Service::whereRaw($where)
                ->orderBy($param['sortField'], $param['sortOrder'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    public static function changeStatus($id, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Service::where('id', $id)
                ->update(
                ['status' => $newStatus]
        );

        return $row;
    }

    public static function deleteStatus($id) {
        if (empty($id))
            return false;

        $row = false;

        $row = Service::where('id', $id)
                ->update(
                ['deleted' => 1]
        );

        return $row;
    }
}