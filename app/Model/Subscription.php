<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Subscription extends Model
{
    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.SUBSCRIPTIONS');
    }

    public function getData($param) {

    	$where = "status='1'";

        if (!empty($param['searchData']))
            $where .= "  AND (term like '%" . addslashes($param['searchData']) . "%') ";
        
              
        $resultSet = Subscription::whereRaw($where)
                ->orderBy($param['sortField'], $param['sortOrder'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }
}