<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;

class User extends Authenticatable {

  use Notifiable, HasApiTokens;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];
    protected $dates = ['dateOfBirth'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.USER');
        $this->prefix = DB::getTablePrefix();
    }

    public function addressbook() {
        return $this->hasMany('App\Model\Addressbook', 'userId', 'id');
    }

    /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getUserList($param, $type = '') {
        $user = new User;
        $city = new City;
        $country = new Country;
        $state = new State;

        $userTable = $user->prefix . $user->table;

        //DB::enableQueryLog();

        $where = "$userTable.deleted = '0'";

        if (!empty($param['searchByfirstName']))
            $where .= " AND $userTable.firstName LIKE '%" . addslashes($param['searchByfirstName']) . "%'";

        if (!empty($param['searchBylastName']))
            $where .= " AND $userTable.lastName LIKE '%" . addslashes($param['searchBylastName']) . "%'";

        if (!empty($param['searchByUnit']))
            $where .= " AND $userTable.unit = '". $param['searchByUnit']."'";
        
        if (!empty($param['searchByEmail']))
            $where .= " AND $userTable.email LIKE '%" . addslashes($param['searchByEmail']) . "%'";        

        if (!empty($param['searchByCompany']))
            $where .= " AND $userTable.company LIKE '%" . addslashes($param['searchByCompany']) . "%'";

        if ($param['searchByStatus'] != '')
            $where .= "  AND $userTable.status='" . addslashes($param['searchByStatus']) . "'";

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($userTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $userTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($userTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($userTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if (!empty($param['searchByUser'])) {
            $userArray = implode("','", $param['searchByUser']);
            $where .= " AND $userTable.id IN ('" . $userArray . "')";
        }

        if ($type == 'export') {
            $resultSet = User::whereRaw($where)
                    ->select(array("$user->table.id", "unit", "title", "firstName", "lastName", "email", "contactNumber", "company", "$user->table.createdOn", "dateOfBirth", "$user->table.status", "$city->table.name AS cityName", "$country->table.name AS countryName", "$state->table.name AS stateName",))
                    ->leftJoin($country->table, "$user->table.countryId", '=', "$country->table.id")
                    ->leftJoin($state->table, "$user->table.stateId", '=', "$state->table.id")
                    ->leftJoin($city->table, "$user->table.cityId", '=', "$city->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->get();
        } 

        else if($type == 'coupon'){
            $resultSet = User::whereRaw($where)
                    ->select(array("id", "unit", "firstName", "lastName", "email", "company", "createdOn", "status"))
                    ->orderBy($param['field'], $param['type'])
                    ->get();
        }

        else {

            $resultSet = User::whereRaw($where)
                    ->select(array("id", "unit", "firstName", "lastName", "email", "company", "createdOn", "status", "activationToken"))
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }

        //dd(DB::getQueryLog());

        return $resultSet;
    }

    /**
     * Method used to change State status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = User::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = User::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to user's unit record
     * @param integer $id
     * @param integer $addressBookId
     * @return boolean
     */
    public static function updateUserUnit($id, $addressBookId) {
        if (empty($id))
            return false;

        $addessBook = Addressbook::find($addressBookId)->country()->first();

        $user = new User;

        $user = User::find($id);
        $user->unit = $addessBook->code . $user->unitNumber;
        $user->modifiedBy = Auth::user()->id;
        $user->modifiedOn = Config::get('constants.CURRENTDATE');
        $user->save();

        return true;
    }

    public static function searchBasic($searchParam) {
        
        $data = User::where('unit','=',$searchParam)
                ->orWhere('firstName','LIKE',"%".$searchParam."%")
                ->orWhere('lastName','LIKE',"%".$searchParam."%")
                ->get();
        return $data;
    }

    public static function generateReferralCode() {

        $code = substr(strtoupper(md5(uniqid(rand()))),0,6);
        $CodeExist = User::where('referralCode',$code)->get();
        if(count($CodeExist) > 0)
            User::generateReferralCode();
        else
            return $code;
    }


    /**
     * Method used to get messages (notifications)
     * @param integer $id
     * @param integer $where
     * @param integer $offset1
     * @param integer $perPage
     * @return boolean
     */
    public static function getmessages($id, $where, $perPage, $offset1) {

        $auser = new UserAdmin;
        $auserTable = $auser->prefix . $auser->table;

        $usernotification = new Usernotification;
        $usernotificationTable = $usernotification->prefix . $usernotification->table;


        $queryTransctions = \App\Model\Usernotification::selectRaw("$auserTable.firstname, $auserTable.lastname, message, userId, $usernotificationTable.id, senderId, subject, $usernotificationTable.status, type, DATE_FORMAT(sentOn,'%m/%d/%Y') as date")
        ->leftJoin($auser->table, "$auser->table.id", '=', "$usernotification->table.senderId")
        ->where('userId', $id)->whereRaw($where)
        ->orderby($usernotification->table.'.id', 'desc')->take($perPage)->skip($offset1)->get();
        
        return $queryTransctions;

    }

    /**
     * Method used to delete document
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteDocument($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = \App\Model\Document::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }
    
    public static function getReferralReportData($param,$export = 0) {
        //print_r($param);exit;
        $userObj = new user;
        $paymentTransactionObj = new Paymenttransaction;
        $userTable = $userObj->prefix.$userObj->table;
        $paymentTransactionTable = $paymentTransactionObj->prefix.$paymentTransactionObj->table;
        
        $where="1";
        
        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($paymentTransactionTable.transactionOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $paymentTransactionTable.transactionOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($paymentTransactionTable.transactionOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($paymentTransactionTable.transactionOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        
        if ($param['searchByUnit'] != '') {
            
            $where .=" AND ".DB::raw("u.unit")."='".$param['searchByUnit']."'";
        }
        if($export ==0)
        {
        $data = DB::table(DB::raw("$userTable as u"))
                ->join("$paymentTransactionObj->table",DB::raw("u.id"),"=","$paymentTransactionObj->table.userId")
                ->select(array(
                    DB::raw("concat(firstName,' ',lastName) as customer"),
                    DB::raw("(select concat(firstName,' ',lastName) from $userTable where $userTable.id=u.referredBy) as refferedUser"),
                    'unit',DB::raw("sum(amountPaid) as paidAmount"),DB::raw("count(distinct paidForId) as numShipments")
                ))
                ->whereRaw($where)->whereNotNull('referredBy')->where("$paymentTransactionObj->table.status","paid")
                ->groupBy("$paymentTransactionObj->table.userId")->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        }
        else
        {
            $data = DB::table(DB::raw("$userTable as u"))
                ->join("$paymentTransactionObj->table",DB::raw("u.id"),"=","$paymentTransactionObj->table.userId")
                ->select(array(
                    DB::raw("concat(firstName,' ',lastName) as customer"),
                    DB::raw("(select concat(firstName,' ',lastName) from $userTable where $userTable.id=u.referredBy) as refferedUser"),
                    'unit',DB::raw("sum(amountPaid) as paidAmount"),DB::raw("count(distinct paidForId) as numShipments")
                ))
                ->whereRaw($where)->whereNotNull('referredBy')->where("$paymentTransactionObj->table.status","paid")
                ->groupBy("$paymentTransactionObj->table.userId")->orderBy($param['field'], $param['type'])->get();
        }
        
        return $data;
    }
    
    public static function referralExport($param) {
        
        $exportData = array();
        $dataList = User::getReferralReportData($param,'1');
        $fieldDisplayName = "";
        foreach($dataList as $index=>$data)
        {
            foreach($data as $fieldName=>$eachField)
            {
                if($fieldName == 'unit')
                    $fieldDisplayName = 'Unit Number';
                elseif($fieldName == 'customer')
                    $fieldDisplayName = 'Referred User';
                elseif($fieldName == 'refferedUser')
                    $fieldDisplayName = 'Referred By';
                elseif($fieldName == 'paidAmount')
                    $fieldDisplayName = 'Revenue Generated';
                elseif($fieldName == 'numShipments')
                    $fieldDisplayName = 'No. Of Shipments';
                else
                    $fieldDisplayName = $fieldName;
                
                $exportData[$index][$fieldDisplayName] = $eachField;
            }
        }
        
        return $exportData;
    }

        /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getCustomerRegistrationData($param,$export = 0) {
        $user = new User;


        $userTable = $user->prefix . $user->table;

        $where = "$userTable.deleted = '0'";

        if (isset($param['searchCustomer']['createdOn']) && !empty($param['searchCustomer']['createdOn'])) {
            if ($param['searchCustomer']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($userTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchCustomer']['createdOn'] == 'thisweek')
                $where .= "  AND $userTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'today')
                $where .= "  AND date($userTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchCustomer']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($userTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if($export == 0)
        {
            $resultSet = User::select(array('unit','email','company','registeredBy'))
                    ->selectRaw('CONCAT(firstName," ",lastName) as name, CONCAT(isdCode," ",contactNumber) as contactNumber, DATE(createdOn) as createdOn')
                    ->whereRaw($where)
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }
        else
        {
            $resultSet = User::select(array('unit','email','company','registeredBy'))
                    ->selectRaw('CONCAT(firstName," ",lastName) as name, CONCAT(isdCode," ",contactNumber) as contactNumber, DATE(createdOn) as createdOn')
                    ->whereRaw($where)
                    ->orderBy($param['field'], $param['type'])
                    ->get()->toArray();
        }

        return $resultSet;

    }

        /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getCustomerDestinationData($param,$export = 0) {
        $user = new User;
        $shipment = new Shipment;
        $city = new City;
        $country = new Country;
        $state = new State;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $cityTable = $shipment->prefix . $city->table;
        $countryTable = $user->prefix . $country->table;
        $stateTable = $user->prefix . $state->table;

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0'  AND $shipmentTable.paymentStatus = 'paid' AND $shipmentTable.shippingMethod = 'Y'";

         if (isset($param['searchCustomer']['toCountry']) && !empty($param['searchCustomer']['toCountry']))
            $where .= "  AND $shipmentTable.toCountry=" . $param['searchCustomer']['toCountry'];
         if (isset($param['searchCustomer']['toState']) && !empty($param['searchCustomer']['toState']))
            $where .= "  AND $shipmentTable.toState=". $param['searchCustomer']['toState'];
         if (isset($param['searchCustomer']['toCity']) && !empty($param['searchCustomer']['toCity']))
            $where .= "  AND $shipmentTable.toCity=". $param['searchCustomer']['toCity'];

        if (isset($param['searchCustomer']['createdOn']) && !empty($param['searchCustomer']['createdOn'])) {
            if ($param['searchCustomer']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchCustomer']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchCustomer']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        
        if($export == 0)
        {
            $resultSet = User::select(array('unit','email','company'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as toCity,
                            $countryTable.name as toCountry, $stateTable.name as toState, count($shipmentTable.id) as numberofshipment")
                        ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                         ->whereRaw($where)
                        ->groupBy("$shipment->table.userId")
                        ->distinct("$shipment->table.id")
                        ->orderBy($param['field'], $param['type'])
                        ->paginate($param['searchDisplay']);
        }
        else
        {
            $resultSet = User::select(array('unit','email','company'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as toCity,
                            $countryTable.name as toCountry, $stateTable.name as toState, count($shipmentTable.id) as numberofshipment")
                        ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                         ->whereRaw($where)
                        ->groupBy("$shipment->table.userId")
                        ->distinct("$shipment->table.id")
                        ->orderBy($param['field'], $param['type'])
                        ->get()->toArray();
        }

            return $resultSet;

    }

        /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getCustomerShippingMethodData($param,$export = 0) {
         $user = new User;
         $shipment = new Shipment;
         $shipmentDelivery = new Shipmentdelivery;
         $shippingMethod = new Shippingmethods;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $shipmentDeliveryTable = $user->prefix . $shipmentDelivery->table;
        $shippingMethodTable = $user->prefix . $shippingMethod->table;

         $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0' AND $shippingMethodTable.deleted = '0'  AND $shipmentDeliveryTable.deleted = '0' AND $shipmentTable.paymentStatus = 'paid' AND $shipmentTable.shippingMethod = 'Y'";

         if (isset($param['searchCustomer']['shippingMethodId']) && !empty($param['searchCustomer']['shippingMethodId']))
            $where .= " AND  $shipmentDeliveryTable.shippingMethodId=" . $param['searchCustomer']['shippingMethodId'];

        if (isset($param['searchCustomer']['createdOn']) && !empty($param['searchCustomer']['createdOn'])) {
            if ($param['searchCustomer']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchCustomer']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchCustomer']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        
        if($export == 0)
        {
        $resultSet = User::select(array('unit','email','company'))
                    ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $shippingMethodTable.shipping as shippingMethod, $shipmentTable.createdOn, count($shipmentTable.id) as numberofshipment")
                    ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                    ->join($shipmentDelivery->table, "$shipmentDelivery->table.shipmentId", '=', "$shipment->table.id")
                    ->join($shippingMethod->table, "$shipmentDelivery->table.shippingMethodId", '=', "$shippingMethod->table.shippingid")
                    ->whereRaw($where)
                    ->groupBy(array("$shipmentDelivery->table.shippingMethodId", "$shipment->table.userId"))
                   // ->distinct("$shipment->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }
        else
        {
            $resultSet = User::select(array('unit','email','company'))
                    ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $shippingMethodTable.shipping as shippingMethod, $shipmentTable.createdOn, count($shipmentTable.id) as numberofshipment")
                    ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                    ->join($shipmentDelivery->table, "$shipmentDelivery->table.shipmentId", '=', "$shipment->table.id")
                    ->join($shippingMethod->table, "$shipmentDelivery->table.shippingMethodId", '=', "$shippingMethod->table.shippingid")
                    ->whereRaw($where)
                    ->groupBy(array("$shipmentDelivery->table.shippingMethodId", "$shipment->table.userId"))
                   // ->distinct("$shipment->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->get()->toArray();
        }
        return $resultSet;
    }

            /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getCustomerShipmentWeightData($param,$export = 0) {
        $user = new User;
        $shipment = new Shipment;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0'  AND $shipmentTable.paymentStatus = 'paid' AND $shipmentTable.shippingMethod = 'Y'";

         if (isset($param['searchCustomer']['shipmentweight']) && !empty($param['searchCustomer']['shipmentweight']))
            $where .= "  AND $shipmentTable.totalWeight >= " . $param['searchCustomer']['shipmentweight'];

        if (isset($param['searchCustomer']['createdOn']) && !empty($param['searchCustomer']['createdOn'])) {
            if ($param['searchCustomer']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchCustomer']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchCustomer']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if($export == 0)
        {
        $resultSet = User::select(array('unit','email','company'))
                    ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, CONCAT($shipmentTable.totalWeight,' ',' lbs') as totalWeight, $shipmentTable.createdOn")
                    ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                     ->whereRaw($where)
                    //->groupBy("$shipment->table.userId")
                    ->distinct("$shipment->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }
        else
        {
            $resultSet = User::select(array('unit','email','company'))
                    ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, CONCAT($shipmentTable.totalWeight,' ',' lbs') as totalWeight, $shipmentTable.createdOn")
                    ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                     ->whereRaw($where)
                    //->groupBy("$shipment->table.userId")
                    ->distinct("$shipment->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->get()->toArray();
        }

        return $resultSet;

    }


    /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getCustomerOrderNumData($param,$export = 0) {
        $user = new User;
        $shipment = new Shipment;
        $order = new Order;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $orderTable = $user->prefix . $order->table;

        $having = "";

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0'  AND $shipmentTable.paymentStatus = 'paid' AND $orderTable.status = '5'";

        if (isset($param['searchCustomer']['numberoforder']) && !empty($param['searchCustomer']['numberoforder']))
            $having = "count($orderTable.id) >= " . $param['searchCustomer']['numberoforder'];

        if($export == 0)
        {
        $resultSet = User::select(array('unit','email','company'))
                    ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, count($orderTable.id) as numberoforder")
                    ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                    ->leftJoin($order->table, "$shipment->table.id", '=', "$order->table.shipmentId")
                    ->whereRaw($where)
                    ->groupBy("$order->table.userId")
                    ->havingRaw($having)
                    ->distinct("$order->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }
        else
        {
            $resultSet = User::select(array('unit','email','company'))
                    ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, count($orderTable.id) as numberoforder")
                    ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                    ->leftJoin($order->table, "$shipment->table.id", '=', "$order->table.shipmentId")
                    ->whereRaw($where)
                    ->groupBy("$order->table.userId")
                    ->havingRaw($having)
                    ->distinct("$order->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->get()->toArray();
        }

        return $resultSet;

    }

     /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getCustomerOrderDateData($param,$export = 0) {
        $user = new User;
        $shipment = new Shipment;
        $order = new Order;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $orderTable = $user->prefix . $order->table;

        $having = "";

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0'  AND $shipmentTable.paymentStatus = 'paid' AND $orderTable.status = '5'";

        if (isset($param['searchCustomer']['orderdate']) && !empty($param['searchCustomer']['orderdate']))
            $where .= " AND DATE($orderTable.createdDate)  <= '" . $param['searchCustomer']['orderdate']."'";

        if($export == 0)
        {
        $resultSet = User::select(array('unit','email','company'))
                    ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $shipmentTable.id as shipmentId, $orderTable.orderNumber, DATE($orderTable.createdDate) as createdDate")
                    ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                    ->leftJoin($order->table, "$shipment->table.id", '=', "$order->table.shipmentId")
                    ->whereRaw($where)
                    ->distinct("$order->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }
        else
        {
            $resultSet = User::select(array('unit','email','company'))
                    ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $shipmentTable.id as shipmentId, $orderTable.orderNumber, DATE($orderTable.createdDate) as createdDate")
                    ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                    ->leftJoin($order->table, "$shipment->table.id", '=', "$order->table.shipmentId")
                    ->whereRaw($where)
                    ->distinct("$order->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->get()->toArray();
        }

        return $resultSet;

    }
    
    public static function getCustomerSpendingData($param,$export = 0) {
        
        $userObj = new user;
        $paymentTransactionObj = new Paymenttransaction;
        $userTable = $userObj->prefix.$userObj->table;
        $paymentTransactionTable = $paymentTransactionObj->prefix.$paymentTransactionObj->table;
        
        $where="1";
        
        if (isset($param['searchCustomer']['createdOn']) && !empty($param['searchCustomer']['createdOn'])) {
            if ($param['searchCustomer']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($paymentTransactionTable.transactionOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchCustomer']['createdOn'] == 'thisweek')
                $where .= "  AND $paymentTransactionTable.transactionOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'today')
                $where .= "  AND date($paymentTransactionTable.transactionOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'custom' && !empty($param['searchCustomer']['searchByDate'])) {
                $searchDate = explode('-', $param['searchCustomer']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($paymentTransactionTable.transactionOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if($export ==0)
        {
            $resultSet = DB::table(DB::raw("$userTable"))
                ->join("$paymentTransactionObj->table","$userObj->table.id","=","$paymentTransactionObj->table.userId")
                ->select(array('unit','email','company',
                    DB::raw("concat(firstName,' ',lastName) as name"),
                    DB::raw("sum(amountPaid) as paidAmount"),
                    DB::raw("CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber")
                ))
                ->whereRaw($where)->where("$paymentTransactionObj->table.status","paid")
                ->groupBy("$paymentTransactionObj->table.userId")->havingRaw("sum(amountPaid)>=".$param['searchCustomer']['spentamount'])->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        }
        else
        {
            $resultSet = DB::table(DB::raw("$userTable"))
                ->join("$paymentTransactionObj->table","$userObj->table.id","=","$paymentTransactionObj->table.userId")
                ->select(array('unit','email','company',
                    DB::raw("concat(firstName,' ',lastName) as name"),
                    DB::raw("sum(amountPaid) as paidAmount"),
                    DB::raw("CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber")
                ))
                ->whereRaw($where)->where("$paymentTransactionObj->table.status","paid")
                ->groupBy("$paymentTransactionObj->table.userId")->havingRaw("sum(amountPaid)>=".$param['searchCustomer']['spentamount'])->orderBy($param['field'], $param['type'])->get()->toArray();
        }

        return $resultSet;
    }
    
    public static function getCustomerServiceType($param, $export = 0) {
        
        $user = new User;
        $shipment = new Shipment;
        $order = new Order;
        $procurement = new Procurement;
        $auto = new Autoshipment;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $orderTable = $user->prefix . $order->table;
        $procurementTable = $procurement->prefix. $procurement->table;
        $autoTable = $auto->prefix.$auto->table;
        
        if($param['searchCustomer']['servicetype'] == 'shipment')
        {
            $searchTable = $shipmentTable;
        }
        else if($param['searchCustomer']['servicetype'] == 'shop_for_me')
        {
            $searchTable = $procurementTable;
        }
        else if($param['searchCustomer']['servicetype'] == 'auto')
        {
            $searchTable = $autoTable;
        }
        
        $where="1";
        
        if (isset($param['searchCustomer']['createdOn']) && !empty($param['searchCustomer']['createdOn'])) {
            if ($param['searchCustomer']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($searchTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchCustomer']['createdOn'] == 'thisweek')
                $where .= "  AND $searchTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'today')
                $where .= "  AND date($searchTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchCustomer']['createdOn'] == 'custom' && !empty($param['searchCustomer']['searchByDate'])) {
                $searchDate = explode('-', $param['searchCustomer']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($searchTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        
        if($param['searchCustomer']['servicetype'] == 'shipment')
        {
            if($export == 0)
            {
                $resultSet = User::select(array('unit','email','company'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $shipmentTable.id as shipmentId, $orderTable.orderNumber, DATE($shipmentTable.createdOn) as createdDate")
                        ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                        ->leftJoin($order->table, "$shipment->table.id", '=', "$order->table.shipmentId")
                        ->where("$order->table.type","shipment")
                        ->whereRaw($where)
                        ->distinct("$order->table.id")
                        ->orderBy($param['field'], $param['type'])
                        ->paginate($param['searchDisplay']);
            }
            else
            {
                $resultSet = User::select(array('unit','email','company'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $shipmentTable.id as shipmentId, $orderTable.orderNumber, DATE($shipmentTable.createdOn) as createdDate")
                        ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                        ->leftJoin($order->table, "$shipment->table.id", '=', "$order->table.shipmentId")
                        ->where("$order->table.type","shipment")
                        ->whereRaw($where)
                        ->distinct("$order->table.id")
                        ->orderBy($param['field'], $param['type'])
                        ->get()->toArray();
            }
        }
        
        else if($param['searchCustomer']['servicetype'] == 'shop_for_me')
        {
            if($export == 0)
            {
                $resultSet = User::select(array('unit','email','company','procurementType'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $procurementTable.id as shipmentId, DATE($procurementTable.createdOn) as createdDate")
                        ->join($procurement->table, "$procurement->table.userId", '=', "$user->table.id")
                        ->whereRaw($where)
                        ->orderBy($param['field'], $param['type'])
                        ->paginate($param['searchDisplay']);
            }
            else
            {
                $resultSet = User::select(array('unit','email','company','procurementType'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $procurementTable.id as shipmentId, DATE($procurementTable.createdOn) as createdDate")
                        ->join($procurement->table, "$procurement->table.userId", '=', "$user->table.id")
                        ->whereRaw($where)
                        ->orderBy($param['field'], $param['type'])
                        ->get()->toArray();
            }
        }
        
        else if($param['searchCustomer']['servicetype'] == 'auto')
        {
            if($export == 0)
            {
                $resultSet = User::select(array('unit','email','company'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $autoTable.id as shipmentId, $orderTable.orderNumber, DATE($autoTable.createdOn) as createdDate")
                        ->join($auto->table, "$auto->table.userId", '=', "$user->table.id")
                        ->leftJoin($order->table, "$auto->table.id", '=', "$order->table.shipmentId")
                        ->where("$order->table.type","autoshipment")
                        ->whereRaw($where)
                        ->orderBy($param['field'], $param['type'])
                        ->paginate($param['searchDisplay']);
            }
            else
            {
                $resultSet = User::select(array('unit','email','company'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $autoTable.id as shipmentId, $orderTable.orderNumber, DATE($autoTable.createdOn) as createdDate")
                        ->join($auto->table, "$auto->table.userId", '=', "$user->table.id")
                        ->leftJoin($order->table, "$auto->table.id", '=', "$order->table.shipmentId")
                        ->where("$order->table.type","autoshipment")
                        ->whereRaw($where)
                        ->orderBy($param['field'], $param['type'])
                        ->get()->toArray();
            }
            
        }
        
        return $resultSet;
    }
    
    public static function getCustomerLifetimeData($param,$export = 0) {
        
        $user = new User;
        $shipment = new Shipment;
        $order = new Order;
        $procurement = new Procurement;
        $auto = new Autoshipment;
        $paymentTransactionObj = new Paymenttransaction;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $orderTable = $user->prefix . $order->table;
        $procurementTable = $procurement->prefix. $procurement->table;
        $autoTable = $auto->prefix.$auto->table;
        $paymentTransactionTable = $paymentTransactionObj->prefix.$paymentTransactionObj->table;
        
        $whereShipment = $whereProcurement = $whereAutoshipment = "($userTable.unit = '".$param['searchCustomer']['userdetails']."' OR $userTable.email='".$param['searchCustomer']['userdetails']."')";

        $whereShipment .= " AND $paymentTransactionTable.status='paid' AND $paymentTransactionTable.paidFor='othershipment' AND $paymentTransactionTable.paidForId=$shipmentTable.id";
        $whereProcurement .= " AND $paymentTransactionTable.status='paid' AND $paymentTransactionTable.paidFor IN ('shopforme','autopart','buyacar') AND $paymentTransactionTable.paidForId=$procurementTable.id";
        $whereAutoshipment .= " AND $paymentTransactionTable.status='paid' AND $paymentTransactionTable.paidFor = 'shipacar' AND $paymentTransactionTable.paidForId=$autoTable.id";
        
        if (isset($param['searchCustomer']['createdOn']) && !empty($param['searchCustomer']['createdOn'])) {
            if ($param['searchCustomer']['createdOn'] == 'thismonth')
            {
                $whereShipment .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                $whereProcurement .= "  AND MONTH($procurementTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                $whereAutoshipment .= "  AND MONTH($autoTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            }
            else if ($param['searchCustomer']['createdOn'] == 'thisweek')
            {
                $whereShipment .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                $whereProcurement .= "  AND $procurementTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                $whereAutoshipment .= "  AND $autoTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            }
            else if ($param['searchCustomer']['createdOn'] == 'today')
            {
                $whereShipment .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                $whereProcurement .= "  AND date($procurementTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                $whereAutoshipment .= "  AND date($autoTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            }
            else if ($param['searchCustomer']['createdOn'] == 'custom' && !empty($param['searchCustomer']['searchByDate'])) {
                $searchDate = explode('-', $param['searchCustomer']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $whereShipment .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                $whereProcurement .= "  AND date($procurementTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                $whereAutoshipment .= "  AND date($autoTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        
        $resultSetShipment = User::select(array('unit','email','company','amountPaid'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $shipmentTable.id as shipmentId, DATE($shipmentTable.createdOn) as createdOn,'Shipment' as type")
                        ->join($shipment->table, "$shipment->table.userId", '=', "$user->table.id")
                        ->join($paymentTransactionObj->table,"$paymentTransactionObj->table.userId","=","$user->table.id")
                        ->whereRaw($whereShipment);
        $resultProcurement = User::select(array('unit','email','company','amountPaid'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $procurementTable.id as shipmentId, DATE($procurementTable.createdOn) as createdOn,'Procurement' as type")
                        ->join($procurement->table, "$procurement->table.userId", '=', "$user->table.id")
                        ->join($paymentTransactionObj->table,"$paymentTransactionObj->table.userId","=","$user->table.id")
                        ->whereRaw($whereProcurement);
        $resultAuto = User::select(array('unit','email','company','amountPaid'))
                        ->selectRaw("CONCAT(firstName,' ',lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $autoTable.id as shipmentId, DATE($autoTable.createdOn) as createdOn,'Auto Shipment' as type")
                        ->join($auto->table, "$auto->table.userId", '=', "$user->table.id")
                        ->join($paymentTransactionObj->table,"$paymentTransactionObj->table.userId","=","$user->table.id")
                        ->whereRaw($whereAutoshipment);
        if($export == 0)
            $resultSet = $resultSetShipment->union($resultProcurement)->union($resultAuto)->get();
        else
            $resultSet = $resultSetShipment->union($resultProcurement)->union($resultAuto)->get()->toArray();
        
        return $resultSet;
        
    }

    public static function exportCustomerData($param) {
        

        if(isset($param['searchByCondition']) && !empty($param['searchByCondition'])) {

             if($param['searchByCondition'] == 'numberofshipment'){
                 $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'shippingMethod' => 'Shipping Method',
                    'createdOn' => 'Created On',
                    'numberofshipment'  => 'Number of Shipments',
                 );

                $records = User::getCustomerShippingMethodData($param,'1');
            }
            elseif($param['searchByCondition'] == 'shipmentweight'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'totalWeight' => 'Shipment Weight',
                    'createdOn' => 'Created On',
                 );
              
                 $records = User::getCustomerShipmentWeightData($param,1);
            }
            elseif($param['searchByCondition'] == 'destinationcity'){
                 $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'toCountry' => 'Destination Country',
                    'toState' => 'Destination State',
                    'toCity' => 'Destination City',
                    'numberofshipment'  => 'Number of Shipments',
                 );

                $records = User::getCustomerDestinationData($param,'1');
            }
           elseif($param['searchByCondition'] == 'registrationdate'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'createdOn' => 'Registered On',
                    'registeredBy' => 'Registered From',
                 );

                $records = User::getCustomerRegistrationData($param,'1');
            } elseif($param['searchByCondition'] == 'numberoforder'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'numberoforder' => 'Number Of Orders'
                 );

                $records = User::getCustomerOrderNumData($param,'1');
            } elseif($param['searchByCondition'] == 'orderdate'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'shipmentId' => 'Shipment ID',
                    'orderNumber' => 'Order Number',
                    'createdDate' => 'Last Order Date'
                 );

                $records = User::getCustomerOrderDateData($param,'1');
            } elseif($param['searchByCondition'] == 'spentamount'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'paidAmount' => 'Spent Amount',
                );
                
                $records = User::getCustomerSpendingData($param,1);
            } elseif($param['searchByCondition'] == 'procurementshipmenttype'){
                
                if($param['searchCustomer']['servicetype'] == 'shipment')
                {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company'  => 'Company',
                        'shipmentId' => 'Shipment ID',
                        'orderNumber' => 'Order Number',
                        'createdDate' => 'Created On'
                    );
                    
                }
                else if($param['searchCustomer']['servicetype'] == 'shop_for_me')
                {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company'  => 'Company',
                        'shipmentId' => 'Procurement ID',
                        'procurementType' => 'Type',
                        'createdDate' => 'Created On'
                    );
                }
                else if($param['searchCustomer']['servicetype'] == 'auto')
                {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company'  => 'Company',
                        'shipmentId' => 'Shipment ID',
                        'orderNumber' => 'Order Number',
                        'createdDate' => 'Created On'
                    );
                }

                $records = User::getCustomerServiceType($param,1);
            } elseif($param['searchByCondition'] == 'customertotalspent') {
                
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'shipmentId' => 'Shipment ID',
                    'type' => 'Type',
                    'totalCost' => 'Amount Spent',
                    'createdOn' => 'Created On'
                );
                
                $records = User::getCustomerLifetimeData($param,1);
            }
            //print_r($records);exit;
            if(!empty($records))
            {
                foreach($records as $index=>$data)
                {
                    //print_r($data);
                    foreach($data as $fieldName=>$eachField)
                    {
                        if(!empty($headers[$fieldName]))
                        {
                            $fieldDisplayName = $headers[$fieldName];
                        }
                        else
                        {
                            $fieldDisplayName = $fieldName;
                        }
                        
                        $exportData[$index][$fieldDisplayName] = $eachField;
                    }
                }

            }
            
            return $exportData;
        }
    }

}
