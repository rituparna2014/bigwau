<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable as Notifiable;
use Config;
use App\Model\Notification;

class UserAdmin extends Authenticatable {

    use Notifiable;

    public $table;
    public $prefix;
    public $timestamps = false;
    protected $fillable = ['firstName', 'lastName', 'email', 'company', 'website'];

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.ADMINUSER');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch Admin User list
     * @param array $param
     * @return object
     */
    public static function getAdminUserList($param) {

        $where = '1';
        $userAdmin = new userAdmin;
        $userAdminTable = $userAdmin->prefix . $userAdmin->table;
        $userAdminTableWithoutPrefix = $userAdmin->table;

               
        $adminusertype = new Adminusertype;
        $adminusertypetable = $adminusertype->prefix . $adminusertype->table;
        $adminusertypetableWithoutPrefix = $adminusertype->table;
        

        if (isset($param['search_firstName'])) {
            $where .= "  AND firstName Like '%" . addslashes($param['search_firstName']) . "%'";
        }
        if (isset($param['search_lastName'])) {
            $where .= "  AND lastName Like '%" . addslashes($param['search_lastName']) . "%'";
        }
        if (isset($param['search_email'])) {
            $where .= "  AND email Like '%" . addslashes($param['search_email']) . "%'";
        }
        if (isset($param['search_status'])) {
            if ($param['search_status'] == "All") {
            } else {
                $where .= "  AND " . $userAdminTable . ".status = " . $param['search_status'];
            }
        }
      
        if (isset($param['search_rangedate'])) {
            if ($param['search_rangedate'] == 'today') {
                $where .= "  AND createdOn Like '%" . date("Y-m-d") . "%'";
            } else if ($param['search_rangedate'] == 'thismonth') {
                $where .= "  AND createdOn Like '%" . date("Y-m") . "%'";
            } else if ($param['search_rangedate'] == 'thisweek') {
                $where .= "  AND createdOn BETWEEN " . UserAdmin::getCurrentWeek();
            } else if ($param['search_rangedate'] == 'custom') {
                // 
                $date_arr = explode('-', $param['search_reservation']);
                $date_arr1 = explode('/', $date_arr[0]);
                $set1_year = $date_arr1[2];
                $set1_month = $date_arr1[0];
                $set1_date = $date_arr1[1];
                $set1_all1 = trim($set1_year) . "-" . $set1_month . "-" . $set1_date;
                $date_arr2 = explode('/', $date_arr[1]);
                $set2_year = $date_arr2[2];
                $set2_month = $date_arr2[0];
                $set2_date = trim($date_arr2[1]);
                $set1_all2 = trim($set2_year) . "-" . trim($set2_month) . "-" . $set2_date;
                $where .= "  AND createdOn BETWEEN '" . $set1_all1 . "' And '" . $set1_all2 . "'";
            }
        }

        if (session()->get('admin_session_user_userType') == 3 || session()->get('admin_session_user_userType') == 4) {
            $where .= ' AND ' . $userAdminTable . '.userType = ' . session()->get('admin_session_user_userType');
        } else if (session()->get('admin_session_user_userType') == 2) {
            $where .= ' AND ' . $userAdminTable . '.userType IN (2, 3, 4, 5, 6, 7, 8, 9, 10)';
        } else {
            $where .= ' AND ' . $userAdminTable . '.userType IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)';
        }



        $resultSet = UserAdmin::whereRaw($where)
                
                ->leftJoin($adminusertypetableWithoutPrefix, $userAdmin->table.'.userType', '=', $adminusertypetableWithoutPrefix.'.id')
                ->addSelect(array($adminusertypetableWithoutPrefix.'.userTypeName', $userAdmin->table.'.id', $userAdmin->table.'.firstName', $userAdmin->table.'.lastName', $userAdmin->table.'.email', 
                    $userAdmin->table.'.status', $userAdmin->table.'.userType', $userAdmin->table.'.website', $userAdmin->table.'.company', $userAdmin->table.'.contactno', 
                    $userAdmin->table.'.status', $userAdmin->table.'.createdOn', $userAdmin->table.'.title'))
                ->groupBy($userAdmin->table.'.id')
                ->orderBy($userAdmin->table . ".".$param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to get the current week
     * @return string
     */
    public static function getCurrentWeek() {
        $monday = strtotime("last monday");
        $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

        $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");

        $this_week_sd = date("Y-m-d", $monday);
        $this_week_ed = date("Y-m-d", $sunday);
        return "'" . $this_week_sd . "%' And '" . $this_week_ed . "%'";
    }


    /**
     * Method used to change Admin User status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return object
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = UserAdmin::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }

    /**
     * Method used to change Admin User password
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newPassword
     * @return object
     */
    public static function changePassword($id, $createrModifierId, $newPassword) {
        if (empty($id))
            return false;

        $row = false;

        $row = User::where('id', $id)
                ->update(array('password' => $newPassword));

        return $row;
    }

    /**
     * Method used to notify user admin
     * @param integer $notify_userId
     * @param integer $createrModifierId
     * @param string $message
     * @return object
     */
    public static function sendNotification($notify_userId, $createrModifierId, $message) {
        if (empty($notify_userId))
            return false;

        $row = true;
        $row = Notification::insert(['userId' => $notify_userId, 'message' => $message, 'status' => 1, 'date' => date('Y-m-d H:i:s'), 'sendUserId' => $createrModifierId]
        );

        return $row;
    }

    /**
     * Method used to fetch the types of admin users
     * @param array $param
     * @return object
     */
    public static function fetchAdminUserType($param) {
        if (session()->get('user_type_id') == 3 || session()->get('user_type_id') == 4) {
            $where = ' id = ' . session()->get('user_type_id');
        } else if (session()->get('user_type_id') == 2) {
            $where = ' id IN (2, 3, 4, 5, 6, 7, 8, 9, 10)';
        } else {
            $where = ' id IN (2, 3, 4, 5, 6, 7, 8, 9, 10)';
        }
        $where .= 'AND status = 1';
        $resultSet = Adminusertype::whereRaw($where)->orderBy($param['field'], $param['type'])->get();

        return $resultSet;
    }

    /**
     * Method used to fetch the default types of admin users
     * @return object
     */
    public static function fetchAdminUserTypeDefault() {

        $resultSet = Adminusertype::where('id', '!=', '1')->where('status', '=', '1')->get();

        return $resultSet;
    }

    /**
     * Method used to change Admin Role status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatusUserType($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Adminusertype::where('id', $id)
                ->update(
                ['status' => $newStatus]
        );

        return $row;
    }

    public static function getAdminUserListExcel($param) {
        $where = '1';

        $userAdmin = new userAdmin;
        $userAdminTable = $userAdmin->prefix . $userAdmin->table;
        $userAdminTableWithoutPrefix = $userAdmin->table;

        $adminwarehousemapping = new Adminwarehousemapping;
        $adminwarehousemappingTable = $adminwarehousemapping->prefix . $adminwarehousemapping->table;
        $adminwarehousemappingTableWithoutPrefix = $adminwarehousemapping->table;

        $adminusertype = new Adminusertype;
        $adminusertypetable = $adminusertype->prefix . $adminusertype->table;
        $adminusertypetableWithoutPrefix = $adminusertype->table;

        if (\Session::get('ADMINUSERDATA.search_firstName')) {
            foreach (\Session::get('ADMINUSERDATA.search_firstName') as $val) {
                //echo $val;
            }
            if ($val != NULL) {
                $where .= "  AND firstName Like '%" . $val . "%'";
            }
        }

        if (\Session::get('ADMINUSERDATA.search_lastName')) {
            foreach (\Session::get('ADMINUSERDATA.search_lastName') as $val) {
                //echo $val;
            }
            if ($val != NULL) {
                $where .= "  AND lastName Like '%" . $val . "%'";
            }
        }


        if (\Session::get('ADMINUSERDATA.search_email')) {
            foreach (\Session::get('ADMINUSERDATA.search_email') as $val) {
                //echo $val;
            }
            if ($val != NULL) {
                $where .= "  AND email Like '%" . $val . "%'";
            }
        }


        if (\Session::get('ADMINUSERDATA.search_status')) {
            foreach (\Session::get('ADMINUSERDATA.search_status') as $val) {
                //echo $val;
            }
            if ($val == "All") {
                //$where .= "  AND status IN (0,1)";
            } else {
                $where .= "  AND " . $userAdminTable . ".status = " . $val;
            }
        }

        if (\Session::get('ADMINUSERDATA.search_warehouseId')) {
            if (count(\Session::get('ADMINUSERDATA.search_warehouseId')[0]) > 0) {
                $warehouseIds = '';
                foreach (\Session::get('ADMINUSERDATA.search_warehouseId')[0] as $val) {
                    $warehouseIds .= $val . ",";
                }

                $warehouseIds = rtrim($warehouseIds, ",");
                $where .= " AND " . $adminwarehousemappingTable . ".warehouseId IN (" . $warehouseIds . ") ";
            }
        }

        if (\Session::get('ADMINUSERDATA.search_rangedate')) {
            if (\Session::get('ADMINUSERDATA.search_rangedate')[0] == 'today') {
                $where .= "  AND createdOn Like '%" . date("Y-m-d") . "%'";
            } else if (\Session::get('ADMINUSERDATA.search_rangedate')[0] == 'thismonth') {
                $where .= "  AND createdOn Like '%" . date("Y-m") . "%'";
            } else if (\Session::get('ADMINUSERDATA.search_rangedate')[0] == 'thisweek') {
                $where .= "  AND createdOn BETWEEN " . UserAdmin::getCurrentWeek();
            } else if (\Session::get('ADMINUSERDATA.search_rangedate')[0] == 'custom') {
                // 
                $date_arr = explode('-', \Session::get('ADMINUSERDATA.search_reservation')[0]);
                $date_arr1 = explode('/', $date_arr[0]);
                $set1_year = $date_arr1[2];
                $set1_month = $date_arr1[0];
                $set1_date = $date_arr1[1];
                $set1_all1 = trim($set1_year) . "-" . $set1_month . "-" . $set1_date;
                $date_arr2 = explode('/', $date_arr[1]);
                $set2_year = $date_arr2[2];
                $set2_month = $date_arr2[0];
                $set2_date = trim($date_arr2[1]);
                $set1_all2 = trim($set2_year) . "-" . trim($set2_month) . "-" . $set2_date;
                $where .= "  AND createdOn BETWEEN '" . $set1_all1 . "' And '" . $set1_all2 . "'";
            }
        }
        $string = '';
        foreach ($param as $key => $val) {
            if ($val == 'userId') {
                $string .= $userAdminTable.'.id as ID, ';
            }
            if ($val == 'name') {
                $string .= "CONCAT(".$userAdminTable.".title, ' ', ".$userAdminTable.".firstName, ' ', ".$userAdminTable.".lastName) as Name, ";
            }
            if ($val == 'email') {
                $string .= $userAdminTable.".email as Email, ";
            }
            if ($val == 'company') {
                $string .= $userAdminTable.".company as Company, ";
            }
            if ($val == 'createdOn') {
                $string .= $userAdminTable.".createdOn as `Registered On`, ";
            }
            if ($val == 'website') {
                $string .= $userAdminTable.".website as Website, ";
            }
            if ($val == 'contactno') {
                $string .= $userAdminTable.".contactno as `Contact No`, ";
            }
            if ($val == 'userType') {
                $string .= $adminusertypetable.".userTypeName as Type, ";
            }
        }
        $string_trimmed = substr(trim("$string"), 0, -1);
        
        $resultSet = UserAdmin::whereRaw($where)
                ->leftJoin($adminwarehousemapping->table, $adminwarehousemapping->table.'.adminUserId', '=', $userAdmin->table.'.id')
                ->leftJoin($adminusertype->table, $userAdmin->table.'.userType', '=', $adminusertype->table.'.id')
                ->addSelect(array(DB::raw($string_trimmed)))
                ->groupBy($userAdmin->table.'.id');

        return $resultSet;
    }

    public static function getAdminUserListExcelSelected($param, $numRecords) {
        $where = '1';

        $userAdmin = new userAdmin;
        $userAdminTable = $userAdmin->prefix . $userAdmin->table;
        $userAdminTableWithoutPrefix = $userAdmin->table;

        $adminwarehousemapping = new Adminwarehousemapping;
        $adminwarehousemappingTable = $adminwarehousemapping->prefix . $adminwarehousemapping->table;
        $adminwarehousemappingTableWithoutPrefix = $adminwarehousemapping->table;

        $adminusertype = new Adminusertype;
        $adminusertypetable = $adminusertype->prefix . $adminusertype->table;
        $adminusertypetableWithoutPrefix = $adminusertype->table;

        $string = '';
        foreach ($param as $key => $val) {
            if ($val == 'userId') {
                $string .= $userAdminTable.'.id as ID, ';
            }
            if ($val == 'name') {
                $string .= "CONCAT(".$userAdminTable.".title, ' ', ".$userAdminTable.".firstName, ' ', ".$userAdminTable.".lastName) as Name, ";
            }
            if ($val == 'email') {
                $string .= $userAdminTable.".email as Email, ";
            }
            if ($val == 'company') {
                $string .= $userAdminTable.".company as Company, ";
            }
            if ($val == 'createdOn') {
                $string .= $userAdminTable.".createdOn as `Registered On`, ";
            }
            if ($val == 'website') {
                $string .= $userAdminTable.".website as Website, ";
            }
            if ($val == 'contactno') {
                $string .= $userAdminTable.".contactno as `Contact No`, ";
            }
            if ($val == 'userType') {
                $string .= $adminusertypetable.".userTypeName as Type, ";
            }
        }
        $string_trimmed = substr(trim("$string"), 0, -1);


        $rowfield = '';
        foreach ($numRecords as $val) {
            $rowfield .= $val . ",";
        }

        $where .= " AND ".$userAdminTable.".id IN (" . rtrim($rowfield, ',') . ") ";

        $resultSet = UserAdmin::whereRaw($where)
                ->leftJoin($adminwarehousemapping->table, $adminwarehousemapping->table.'.adminUserId', '=', $userAdmin->table.'.id')
                ->leftJoin($adminusertype->table, $userAdmin->table.'.userType', '=', $adminusertype->table.'.id')
                ->addSelect(array(DB::raw($string_trimmed)))
                ->groupBy($userAdmin->table.'.id');

        return $resultSet;
    }

    public static function getUserTypeNameById($id) {
        $usertypebyId = Adminusertype::select('id', 'userTypeName')->where('id', '=', $id)->get();

        return $usertypebyId;
    }

}
