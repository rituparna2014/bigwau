<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable as Notifiable;
use Config;
use App\Model\Notification;

class Sellers extends Authenticatable {

    use Notifiable;

    public $table;
    public $prefix;
    public $timestamps = false;
    protected $fillable = ['firstName', 'lastName', 'email', 'company', 'website'];

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.SELLERS');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch Admin User list
     * @param array $param
     * @return object
     */
    public static function getSellersList($param) {

        #dd($param);
        
        $where = '1';
        $seller = new Sellers;
        $sellerTable = $seller->prefix . $seller->table;
        $sellerTableWithoutPrefix = $seller->table;


        if (isset($param['search_firstName'])) {
            $where .= "  AND firstName Like '%" . addslashes($param['search_firstName']) . "%'";
        }
        if (isset($param['search_lastName'])) {
            $where .= "  AND lastName Like '%" . addslashes($param['search_lastName']) . "%'";
        }
        if (isset($param['search_companyName'])) {
            $where .= "  AND companyName Like '%" . addslashes($param['search_companyName']) . "%'";
        }
        if (isset($param['search_status'])) {
            if ($param['search_status'] == "All") {
            } else {
                $where .= "  AND " . $sellerTable . ".status = '" . $param['search_status']."'";
            }
        }
       
        if (isset($param['search_rangedate'])) {
            if ($param['search_rangedate'] == 'today') {
                $where .= "  AND createdOn Like '%" . date("Y-m-d") . "%'";
            } else if ($param['search_rangedate'] == 'thismonth') {
                $where .= "  AND createdOn Like '%" . date("Y-m") . "%'";
            } else if ($param['search_rangedate'] == 'thisweek') {
                $where .= "  AND createdOn BETWEEN " . UserAdmin::getCurrentWeek();
            } else if ($param['search_rangedate'] == 'custom') {
                // 
                $date_arr = explode('-', $param['search_reservation']);
                $date_arr1 = explode('/', $date_arr[0]);
                $set1_year = $date_arr1[2];
                $set1_month = $date_arr1[0];
                $set1_date = $date_arr1[1];
                $set1_all1 = trim($set1_year) . "-" . $set1_month . "-" . $set1_date;
                $date_arr2 = explode('/', $date_arr[1]);
                $set2_year = $date_arr2[2];
                $set2_month = $date_arr2[0];
                $set2_date = trim($date_arr2[1]);
                $set1_all2 = trim($set2_year) . "-" . trim($set2_month) . "-" . $set2_date;
                $where .= "  AND createdOn BETWEEN '" . $set1_all1 . "' And '" . $set1_all2 . "'";
            }
        }

        

        $resultSet = Sellers::whereRaw($where)              
                
                ->addSelect(array( $seller->table.'.id', $seller->table.'.firstName', $seller->table.'.lastName', $seller->table.'.email', 
                    $seller->table.'.status', $seller->table.'.sellerType', $seller->table.'.companyName', $seller->table.'.IBAN', $seller->table.'.phone', 
                    $seller->table.'.status', $seller->table.'.createdOn', $seller->table.'.BIC'))
                ->groupBy($seller->table.'.id')
                ->orderBy($seller->table . ".".$param['field'], $param['type'])
                ->paginate($param['searchDisplay']);
        #dd($resultSet);
        return $resultSet;
    }

    /**
     * Method used to get the current week
     * @return string
     */
    public static function getCurrentWeek() {
        $monday = strtotime("last monday");
        $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

        $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");

        $this_week_sd = date("Y-m-d", $monday);
        $this_week_ed = date("Y-m-d", $sunday);
        return "'" . $this_week_sd . "%' And '" . $this_week_ed . "%'";
    }


    /**
     * Method used to change Admin User status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return object
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Sellers::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }

    /**
     * Method used to change Admin User password
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newPassword
     * @return object
     */
    public static function changePassword($id, $createrModifierId, $newPassword) {
        if (empty($id))
            return false;

        $row = false;

        $row = User::where('id', $id)
                ->update(array('password' => $newPassword));

        return $row;
    }

    /**
     * Method used to notify user admin
     * @param integer $notify_userId
     * @param integer $createrModifierId
     * @param string $message
     * @return object
     */
    public static function sendNotification($notify_userId, $createrModifierId, $message) {
        if (empty($notify_userId))
            return false;

        $row = true;
        $row = Notification::insert(['userId' => $notify_userId, 'message' => $message, 'status' => 1, 'date' => date('Y-m-d H:i:s'), 'sendUserId' => $createrModifierId]
        );

        return $row;
    }

    /**
     * Method used to fetch the types of admin users
     * @param array $param
     * @return object
     */
    public static function fetchAdminUserType($param) {
        if (session()->get('user_type_id') == 3 || session()->get('user_type_id') == 4) {
            $where = ' id = ' . session()->get('user_type_id');
        } else if (session()->get('user_type_id') == 2) {
            $where = ' id IN (2, 3, 4, 5, 6, 7, 8, 9, 10)';
        } else {
            $where = ' id IN (2, 3, 4, 5, 6, 7, 8, 9, 10)';
        }

        $resultSet = Adminusertype::whereRaw($where)->orderBy($param['field'], $param['type'])->get();

        return $resultSet;
    }

    /**
     * Method used to fetch the default types of admin users
     * @return object
     */
    public static function fetchAdminUserTypeDefault() {

        $resultSet = Adminusertype::where('id', '!=', '1')->where(function($result) {
        $result->where('status', '!=', '0');
        })->get();

        return $resultSet;
    }

    /**
     * Method used to change Admin Role status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatusUserType($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Adminusertype::where('id', $id)
                ->update(
                ['status' => $newStatus]
        );

        return $row;
    }

    public static function getSellersListExcel($param) {
             

        $where = '1';
        $buyer = new Sellers;
        $sellerTable = $buyer->prefix . $buyer->table;
        $buyerTableWithoutPrefix = $buyer->table;


        if (\Session::get('SELLERDATA.search_firstName')) {
            foreach (\Session::get('SELLERDATA.search_firstName') as $val) {
                //echo $val;
            }
            if ($val != NULL) {
                $where .= "  AND firstName Like '%" . $val . "%'";
            }
        }

        if (\Session::get('SELLERDATA.search_lastName')) {
            foreach (\Session::get('SELLERDATA.search_lastName') as $val) {
                //echo $val;
            }
            if ($val != NULL) {
                $where .= "  AND lastName Like '%" . $val . "%'";
            }
        }


        if (\Session::get('SELLERDATA.search_companyName')) {
            foreach (\Session::get('SELLERDATA.search_companyName') as $val) {
                //echo $val;
            }
            if ($val != NULL) {
                $where .= "  AND companyName Like '%" . $val . "%'";
            }
        }


        if (\Session::get('SELLERDATA.search_status')) {
            foreach (\Session::get('SELLERDATA.search_status') as $val) {
                //echo $val;
            }
            if ($val == "All") {
                //$where .= "  AND status IN (0,1)";
            } else {
                $where .= "  AND " . $sellerTable . ".status = " . $val;
            }
        }

        if (\Session::get('SELLERDATA.search_rangedate')) {
            if (\Session::get('SELLERDATA.search_rangedate')[0] == 'today') {
                $where .= "  AND createdOn Like '%" . date("Y-m-d") . "%'";
            } else if (\Session::get('SELLERDATA.search_rangedate')[0] == 'thismonth') {
                $where .= "  AND createdOn Like '%" . date("Y-m") . "%'";
            } else if (\Session::get('SELLERDATA.search_rangedate')[0] == 'thisweek') {
                $where .= "  AND createdOn BETWEEN " . UserAdmin::getCurrentWeek();
            } else if (\Session::get('SELLERDATA.search_rangedate')[0] == 'custom') {
                // 
                $date_arr = explode('-', \Session::get('SELLERDATA.search_reservation')[0]);
                $date_arr1 = explode('/', $date_arr[0]);
                $set1_year = $date_arr1[2];
                $set1_month = $date_arr1[0];
                $set1_date = $date_arr1[1];
                $set1_all1 = trim($set1_year) . "-" . $set1_month . "-" . $set1_date;
                $date_arr2 = explode('/', $date_arr[1]);
                $set2_year = $date_arr2[2];
                $set2_month = $date_arr2[0];
                $set2_date = trim($date_arr2[1]);
                $set1_all2 = trim($set2_year) . "-" . trim($set2_month) . "-" . $set2_date;
                $where .= "  AND createdOn BETWEEN '" . $set1_all1 . "' And '" . $set1_all2 . "'";
            }
        }
        $string = '';
        foreach ($param as $key => $val) {
            if ($val == 'userId') {
                $string .= $sellerTable.'.id as ID, ';
            }
            if ($val == 'name') {
                $string .= "CONCAT(".$sellerTable.".firstName, ' ', ".$sellerTable.".lastName) as Name, ";
            }
            if ($val == 'buyerId') {
                $string .= $sellerTable.".sellerId as `Seller Id`, ";
            }
            if ($val == 'buyerType') {
                $string .= $sellerTable.".sellerType as `Seller Type`, ";
            }
            if ($val == 'email') {
                $string .= $sellerTable.".email as Email, ";
            }
            if ($val == 'companyName') {
                $string .= $sellerTable.".companyName as Company, ";
            }
            if ($val == 'profileDescription') {
                $string .= $sellerTable.".profileDescription as `Profile Description`, ";
            }
            if ($val == 'birthday') {
                $string .= $sellerTable.".birthday  as Birthday, ";
            }
            if ($val == 'phone') {
                $string .= $sellerTable.".phone as Phone, ";
            }
            if ($val == 'timezone') {
                $string .= $sellerTable.".timezone as Timezone, ";
            }
            if ($val == 'nationality') {
                $string .= $sellerTable.".nationality as Nationality, ";
            }
            if ($val == 'profession') {
                $string .= $sellerTable.".profession as Profession, ";
            }
            if ($val == 'IBAN') {
                $string .= $sellerTable.".IBAN as IBAN, ";
            }
            if ($val == 'BIC') {
                $string .= $sellerTable.".BIC as BIC, ";
            }
            if ($val == 'bankOwnerName') {
                $string .= $sellerTable.".bankOwnerName as `Bank Owner Name`, ";
            }
            if ($val == 'bankOwnerAddress') {
                $string .= $sellerTable.".bankOwnerAddress as `Bank Owner address`, ";
            }
            if ($val == 'annualIncome') {
                $string .= $sellerTable.".annualIncome as `Annual Income`, ";
            }
            if ($val == 'fee') {
                $string .= $sellerTable.".fee as `Subscription Fee`, ";
            }
            if ($val == 'address') {
                $string .= $sellerTable.".address as `Address`, ";
            }
            if ($val == 'createdOn') {
                $string .= $sellerTable.".createdOn as `Registered On`, ";
            }
            
        }
        $string_trimmed = substr(trim("$string"), 0, -1);
        
        $resultSet = Sellers::whereRaw($where)                
                ->addSelect(array(DB::raw($string_trimmed)))
                ->groupBy($buyer->table.'.id');

        return $resultSet;
    }

    public static function getSellerListExcelSelected($param, $numRecords) {
        $where = '1';

        $seller = new Sellers;
        $sellerTable = $seller->prefix . $seller->table;
        $sellerTableWithoutPrefix = $seller->table;

        $string = '';
        foreach ($param as $key => $val) {
            if ($val == 'userId') {
                $string .= $sellerTable.'.id as ID, ';
            }
            if ($val == 'name') {
                $string .= "CONCAT(".$sellerTable.".firstName, ' ', ".$sellerTable.".lastName) as Name, ";
            }
            if ($val == 'sellerId') {
                $string .= $sellerTable.".sellerId as `Seller Id`, ";
            }
            if ($val == 'sellerType') {
                $string .= $sellerTable.".sellerType as `Seller Type`, ";
            }
            if ($val == 'email') {
                $string .= $sellerTable.".email as Email, ";
            }
            if ($val == 'companyName') {
                $string .= $sellerTable.".companyName as Company, ";
            }
            if ($val == 'profileDescription') {
                $string .= $sellerTable.".profileDescription as `Profile Description`, ";
            }
            if ($val == 'birthday') {
                $string .= $sellerTable.".birthday  as Birthday, ";
            }
            if ($val == 'phone') {
                $string .= $sellerTable.".phone as Phone, ";
            }
            if ($val == 'timezone') {
                $string .= $sellerTable.".timezone as Timezone, ";
            }
            if ($val == 'nationality') {
                $string .= $sellerTable.".nationality as Nationality, ";
            }
            if ($val == 'profession') {
                $string .= $sellerTable.".profession as Profession, ";
            }
            if ($val == 'IBAN') {
                $string .= $sellerTable.".IBAN as IBAN, ";
            }
            if ($val == 'BIC') {
                $string .= $sellerTable.".BIC as BIC, ";
            }
            if ($val == 'bankOwnerName') {
                $string .= $sellerTable.".bankOwnerName as `Bank Owner Name`, ";
            }
            if ($val == 'bankOwnerAddress') {
                $string .= $sellerTable.".bankOwnerAddress as `Bank Owner address`, ";
            }
            if ($val == 'annualIncome') {
                $string .= $sellerTable.".annualIncome as `Annual Income`, ";
            }
            if ($val == 'fee') {
                $string .= $sellerTable.".fee as `Subscription Fee`, ";
            }
            if ($val == 'address') {
                $string .= $sellerTable.".address as `Address`, ";
            }
            if ($val == 'createdOn') {
                $string .= $sellerTable.".createdOn as `Registered On`, ";
            }
        }
        $string_trimmed = substr(trim("$string"), 0, -1);


        $rowfield = '';
        foreach ($numRecords as $val) {
            $rowfield .= $val . ",";
        }

        $where .= " AND ".$sellerTable.".id IN (" . rtrim($rowfield, ',') . ") ";

        $resultSet = Sellers::whereRaw($where)                
                ->addSelect(array(DB::raw($string_trimmed)))
                ->groupBy($seller->table.'.id');
            
        return $resultSet;
    }

    public static function getUserTypeNameById($id) {
        $usertypebyId = Adminusertype::select('id', 'userTypeName')->where('id', '=', $id)->get();

        return $usertypebyId;
    }

}
