<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
class Annualpurchase extends Model
{
   
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.ANNUALPURCHASINGVALUES');
        $this->prefix = DB::getTablePrefix();
    }

    public function getData($param) {

    	$where = "deleted='0'";

        if (!empty($param['searchData']))
            $where .= "  AND (pageHeader like '%" . addslashes($param['searchData']) . "%' OR pageTitle like '%" . addslashes($param['searchData']) . "%' OR metaKeyword like '%" . addslashes($param['searchData']) . "%') ";
        
       

        /* Print query ->toSql() dd($resultSet); */
        $resultSet = Annualpurchase::whereRaw($where)
                ->orderBy($param['sortField'], $param['sortOrder'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    
}
