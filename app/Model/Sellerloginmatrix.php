<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Model\User;
use App\Model\Sellers;
use Config;

class Sellerloginmatrix extends Model
{
    protected $table;
    public $timestamps = false;
    protected $fillable = ['userId', 'message', 'sendUserId', 'status', 'date'];

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.SELLERLOGINMATRICES');
    }

    /**
     * Method used to fetch notification list
     * @param array $param
     * @return object
     */
    public static function getList($param, $id) { 
        $where = '1';

        $seller = new Sellers();
        $sellerTable = $seller->table;

        $sellerloginmatrices = new Sellerloginmatrix();
        $sellerloginmatricesTable = $sellerloginmatrices->table;


        $resultSet = Sellerloginmatrix::select($sellerloginmatricesTable.'.*', $sellerTable.'.firstName', 
            $sellerTable.'.lastName',
            $sellerTable.'.sellerId',  
            $sellerTable.'.email')
                ->leftJoin($sellerTable, $sellerTable.'.id', '=', $sellerloginmatricesTable.'.sellerId')
                ->where($sellerloginmatricesTable.'.sellerId', '=', $id)                
                ->where($sellerloginmatricesTable.'.status', '=', 1)              
                ->paginate($param['searchDisplay']); 

       // $resultSet =  Buyerloginmatrix::where('buyerId', '=', $id)->paginate($param['searchDisplay']); 

        return $resultSet;
    }


    
}
