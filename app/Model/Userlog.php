<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;
use Carbon\Carbon;

class Userlog extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.USERLOG');
        $this->prefix = DB::getTablePrefix();
    }


    /**
     * Method used to check record
     * @param integer $userId
     * @param integer $deviceType
     * @param integer $deviceId
     * @param integer $deviceToken
     * @param integer $userAgent
     * @return array
     */
    public static function checkstatus($userId, $deviceType, $deviceId, $deviceToken, $userAgent) {

        $row = false;

        $row = Userlog::where('userId', $userId)->where('isLoggedIn', '1')->orderby('id', 'desc')->first();
        if(!empty($row)){
            if($deviceType == $row->deviceType && $userAgent == $row->userAgent && $deviceToken == $row->deviceToken
                && $deviceId ==  $row->deviceId)
            {
                $row = Userlog::where('userId', $userId)->where('isLoggedIn', '1')->update(array('isLoggedIn' => '0', 'loggedOutOn' => Config::get('constants.CURRENTDATE')));
                return null;
            }
        }

        return $row;
    }

    /**
     * Method used to insert record
     * @param integer $userId
     * @param integer $deviceType
     * @param integer $deviceId
     * @param integer $deviceToken
     * @param integer $userAgent
     * @return array
     */
    public static function insertlogrecord($userId, $deviceType, $deviceId, $deviceToken, $userAgent){

        $log = new Userlog;

        $log->userId                = $userId;
        $log->deviceType            = $deviceType;
        $log->deviceId              = $deviceId;
        $log->deviceToken           = $deviceToken;
        $log->userAgent             = $userAgent;
        $log->lastLoggedOn          = Carbon::now();
        $log->isLoggedIn            = '1';
        $log->save();

        return $log;
    }

    /**
     * Method used to logout and update record
     * @param integer $userId
     * @return array
     */
    public static function logoutstatus($userId) {

        $row = false;

        $row = Userlog::where('userId', $userId)->where('isLoggedIn', '1')->update(array('isLoggedIn' => '0', 'loggedOutOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to forced login
     * @param integer $userId
     * @return array
     */
    public static function forcedlogin($userId, $deviceType, $deviceId, $deviceToken, $userAgent) {

        $row = false;

        $row = Userlog::where('userId', $userId)->where('isLoggedIn', '1')->update(array('isLoggedIn' => '0', 'loggedOutOn' => Config::get('constants.CURRENTDATE')));

        $log = new Userlog;

        $log->userId                = $userId;
        $log->deviceType            = $deviceType;
        $log->userAgent             = $userAgent;
        $log->lastLoggedOn          = Carbon::now();
        $log->isLoggedIn            = '1';
        $log->save();

        return $log;
    }

    /**
     * Method used to check record
     * @param integer $userId
     * @param integer $deviceType
     * @param integer $deviceId
     * @param integer $deviceToken
     * @param integer $userAgent
     * @return array
     */
    public static function status($userId, $deviceType, $deviceId, $deviceToken, $userAgent) {

        $row = false;

        $res = Userlog::where('userId', $userId)->where('isLoggedIn', '1')->orderby('id', 'desc')->first();
        if(!empty($res)){
            if($deviceType == $res->deviceType && $userAgent == $res->userAgent && $deviceToken == $res->deviceToken
                && $deviceId ==  $res->deviceId)
            {
                $row = true;
            }

        }
        
        if(!empty($row)){
            return $row;
        }
    }
    
}
