<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Model\User;
use App\Model\Buyers;
use Config;

class Buyerloginmatrix extends Model
{
    protected $table;
    public $timestamps = false;
    protected $fillable = ['userId', 'message', 'sendUserId', 'status', 'date'];

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.LOGINMATRICES');
    }

    /**
     * Method used to fetch notification list
     * @param array $param
     * @return object
     */
    public static function getList($param, $id) { 
        $where = '1';

        $buyer = new Buyers();
        $buyerTable = $buyer->table;

        $buyerloginmatrices = new Buyerloginmatrix();
        $buyerloginmatricesTable = $buyerloginmatrices->table;


        $resultSet = Buyerloginmatrix::select($buyerloginmatricesTable.'.*', $buyerTable.'.firstName', 
            $buyerTable.'.lastName',
            $buyerTable.'.buyerId',  
            $buyerTable.'.email')
                ->leftJoin($buyerTable, $buyerTable.'.id', '=', $buyerloginmatricesTable.'.buyerId')
                ->where($buyerloginmatricesTable.'.buyerId', '=', $id)                
                ->where($buyerloginmatricesTable.'.status', '=', 1)              
                ->paginate($param['searchDisplay']); 

       // $resultSet =  Buyerloginmatrix::where('buyerId', '=', $id)->paginate($param['searchDisplay']); 

        return $resultSet;
    }


    
}
