<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Adminuserlog extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.ADMINUSERLOG');
    }

    /**
     * Insert admin user log associated with the user
     * @param integer $id
     * @param string $requirement
     */
    public function userLogInsert($id, $requirement) {
        $adminuserlogs = new Adminuserlog;
        
        if ($requirement == 'Insert') {
            $resultSet = Adminuserlog::insert(
                    ['adminUserId' => $id, 'adminUserInTime' => \Carbon\Carbon::now()]
            );
        } else {
            Adminuserlog::where('adminUserId', $id)
                    ->where('adminUserOutTime', NULL)->limit(1)
                    ->update(['adminUserOutTime' => \Carbon\Carbon::now()]);
        }
    }
}
