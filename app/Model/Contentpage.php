<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Contentpage extends Model
{
    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.CONTENTPAGES');
    }

    public function getData($param) {

    	$where = "status='1'";

        if (!empty($param['searchData']))
            $where .= "  AND (pageHeader like '%" . addslashes($param['searchData']) . "%' OR pageTitle like '%" . addslashes($param['searchData']) . "%' OR metaKeyword like '%" . addslashes($param['searchData']) . "%') ";
        
       

        /* Print query ->toSql() dd($resultSet); */
        $resultSet = Contentpage::whereRaw($where)
                ->orderBy($param['sortField'], $param['sortOrder'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }
}