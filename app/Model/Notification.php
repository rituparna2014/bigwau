<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Model\User;
use Config;

class Notification extends Model
{
    protected $table;
    public $timestamps = false;
    protected $fillable = ['userId', 'message', 'sendUserId', 'status', 'date'];

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.ADMINNOTIFICATION');
    }

    /**
     * Method used to fetch notification list
     * @param array $param
     * @return object
     */
    public static function getNotificationList($param, $id) { 
        $where = '1';

        $resultSet =  Notification::where('userId', '=', $id)->paginate($param['searchDisplay']); 

        return $resultSet;
    }


    
}
