<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use View;
use Auth;
use customhelper;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        //
        Validator::extend('recaptcha','App\\Validators\\ReCaptcha@validate');
        $auth = $this->app['auth'];
        view()->composer('*', function ($view) use($auth, $request) {  
        if(Auth::check() == true){
            #$request->session()->push('myUserId', $auth->user()->id);
            View::share('tree', customhelper::generateSiteTree(0, $auth->user()->id));
        }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
