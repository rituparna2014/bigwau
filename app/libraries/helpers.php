<?php

namespace App\libraries;

use Illuminate\Support\Facades\Mail;

Class helpers {

    /**
     * Method used print array value
     */
    public static function pr($arr, $e = 1) {
        if (is_array($arr)) {
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        } else {
            echo "<br>Not an array...<br>";
            echo "<pre>";
            var_dump($arr);
            echo "</pre>";
        }

        if ($e == 1) {
            exit();
        } else {
            echo "<br>";
        }
    }

    /**
     * Method used to format input value of array
     * @param array $arr
     * @param string $Type
     * @param boolean $htmlEntitiesEncode
     * @return string
     */
    public static function inputEscapeArray($arr, $Type = 'DB', $htmlEntitiesEncode = true) {
        if (count($arr) == 0)
            return $arr;

        $escapeArray = array();
        foreach ($arr as $key => $val) {
            $escapeArray[$key] = self::inputEscapeString($val, $Type, $htmlEntitiesEncode);
        }
        return $escapeArray;
    }

    /**
     * Method used to format output value
     * @param array $str
     * @param string $Type
     * @param boolean $htmlEntitiesEncode
     * @return string
     */
    public static function inputEscapeString($str, $Type = 'DB', $htmlEntitiesEncode = true) {
        if ($Type === 'DB') {
            $str = addslashes($str);
        } elseif ($Type === 'FILE') {
            if (get_magic_quotes_gpc() === 1) {
                $str = stripslashes($str);
            }
        } else {
            $str = $str;
        }
        if ($htmlEntitiesEncode === true) {
            $str = htmlentities($str);
        }

        $str = trim($str);

        return $str;
    }

    /**
     * Method used to format output value of array
     * @param array $arr
     * @param string $Type
     * @param boolean $htmlEntitiesEncode
     * @return array
     */
    public static function outputEscapeArray($arr, $Type = 'DB', $htmlEntitiesEncode = true) {
        if (count($arr) == 0)
            return $arr;

        $escapeArray = array();
        $count = 0;
        foreach ($arr as $key => $val) {
            foreach ($val as $key => $val) {
                $escapeArray[$count][$key] = self::outputEscapeString($val, $Type, $htmlEntitiesEncode);
            }
            $count++;
        }
        return $escapeArray;
    }

    /**
     * Method used to format output value of string
     * @param array $str
     * @param string $Type
     * @param boolean $htmlEntitiesDecode
     * @return array
     */
    public static function outputEscapeString($str, $Type = 'INPUT', $htmlEntitiesDecode = true) {
        if (get_magic_quotes_runtime() == 0) {
            $str = stripslashes($str);
        }
        $str = html_entity_decode($str);

        if ($Type == 'INPUT') {
            $str = $str;
        } elseif ($Type == 'TEXTAREA') {
            $str = $str;
        } elseif ($Type == 'HTML') {
            $str = nl2br($str);
        } else {
            $str = htmlspecialchars($str);
        }

        $str = trim($str);

        return $str;
    }

    /**
     * Method used to format output value of array
     * @param array $arr
     * @param string $Type
     * @param boolean $htmlEntitiesEncode
     * @return array
     */
    public static function outputEscapeFirstArray($arr, $Type = 'DB', $htmlEntitiesEncode = true) {
        if (count($arr) == 0)
            return $arr;
        $escapeArray = array();
        foreach ($arr as $key => $val) {
            $escapeArray[$key] = self::outputEscapeString($val, $Type, $htmlEntitiesEncode);
        }
        return $escapeArray;
    }

    /**
     * Method used to unset session value
     * @param array $session_var
     * @param array $search_array
     * @return boolean
     */
    public static function unsetSearchVariables($session_var, $search_array = array()) {
        if (!empty($session_var)) {
            $_SESSION[$session_var] = array();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Use for generate a password with lowercase letter,uppercase letter, digits  and special character.
     * @param int $length
     * @param string $available_sets
     * @return string
     */
    public static function generatePassword($length = 8, $available_sets = 'luds') {
        $sets = array();

        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
//        if (strpos($available_sets, 's') !== false)
//            $sets[] = '@#$%^&*()-_=+?/';

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);

        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }

        $password = str_shuffle($password);
        return $password;
    }

    /**
     * Method used to show error meesage
     * @param array $array
     * @param string $element
     * @param string $type
     */
    public static function setErrorMessage($array, $element, $type = 'SINGLE') {
        if (!is_array($array) || count($array) == 0 || empty($array[$element]))
            return '&nbsp;';
        $string = '';
        $arr = array_values($array[$element]);
        if ($type == 'MULTIPLE') {
            foreach ($arr as $arr) {
                $string .= $arr . '<br/>';
            }
            return '<span class="error">' . $string . '</span>';
        } else {
            return '<span class="error">' . $arr[0] . '</span>';
        }
    }

    /**
     * Create slug for content
     * @param string $title
     * @param string $table
     * @param integer $id
     * @return string/boolean
     */
    public static function genSlug($title, $tableArr) {
        $commonDb = new CommonDbHelper();
        $title = $this->url_slug($title, array('transliterate' => true));
        if (empty($title)) {
            return false;
        }

        $countSlug = $commonDb->cntSlug($title, $tableArr);
        $i = 0;
        if ($title != '' && !empty($countSlug)) {
            $isExit = true;
            while ($isExit) {
                $i++;
                $countNewSlug = $commonDb->cntSlug($title . '-' . $i, $tableArr);
                if (empty($countNewSlug))
                    $isExit = false;
            }
            return $title . '-' . $i;
        }else {
            return $title;
        }
    }

    /**
     * Method used to replace UTF-8 character to English Character or URL friendly Character
     * @param string $str
     * @param array $options
     * @return string
     */
    public static function url_slug($str, $options = array()) {
// Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());
        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => false,
        );
// Merge options
        $options = array_merge($defaults, $options);
        $char_map = array(
// Latin
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
            'ß' => 'ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
            'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
            'ÿ' => 'y',
            // Latin symbols
            '©' => '(c)',
            // Greek
            'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
            'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
            'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
            'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
            'Ϋ' => 'Y',
            'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
            'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
            'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
            'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
            'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
            // Turkish
            'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
            'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
            // Russian
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
            'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
            'Я' => 'Ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
            'я' => 'ya',
            // Ukrainian
            'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
            'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
            // Czech
            'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
            'Ž' => 'Z',
            'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
            'ž' => 'z',
            // Polish
            'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
            'Ż' => 'Z',
            'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
            'ż' => 'z',
            // Latvian
            'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
            'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
            'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
            'š' => 's', 'ū' => 'u', 'ž' => 'z'
        );
// Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
// Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }
// Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
// Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
// Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
// Remove delimiter from ends
        $str = trim($str, $options['delimiter']);
        return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }

    /**
     * Method used to wrap content
     * @param string $string
     * @param integer $limit
     * @param string $break
     * @param string $pad
     * @return string
     */
    public static function wordWrap($string, $limit, $break = ".", $pad = "...") {
// return with no change if string is shorter than $limit
        if (strlen($string) <= $limit)
            return $string;

// is $break present between $limit and the end of the string?
        if (false !== ($breakpoint = strpos($string, $break, $limit))) {
            if ($breakpoint < strlen($string) - 1) {
                $string = substr($string, 0, $breakpoint) . $pad;
            }
        }

        return $string;
    }

    /**
     * Method used to get email template
     * @param string $toEmail
     * @param array $templateArray
     * @param array $searchText
     * @param array $replaceText
     */
    public static function emailTemplate($templateCode = '', $searchText = array(), $replaceText = array()) {
        $emailBody = array();
        if (!empty($templateCode)) {
            $data = dbHelpers::templateData($templateCode);
            $emailSubject = (!empty($data['subject']) && $data['subject'] <> 'New Notification From eWallet') ? $data['subject'] : $templateArray[1];
            if (count($searchText) > 0) {
                $subject = str_replace($searchText, $replaceText, $emailSubject);
                $body = self::getMailTemplate(str_replace($searchText, $replaceText, $data['emailBody']));
            } else {
                $subject = $emailSubject;
                $body = self::getMailTemplate($data['emailBody']);
            }
            $emailBody = array('body' => $body, 'subject' => $subject);
        } else {
            return false;
        }
        return $emailBody;
    }

    /**
     * Method used to get custom date format
     * @param string $dateStr
     * @return string $date
     */
    public static function customDateFormat($dateStr, $modifyTo = false) {
        $dateArr = explode("-", $dateStr);

        $date = date("Y-m-d H:i:s", strtotime($dateArr[2] . '-' . $dateArr[1] . '-' . $dateArr[0]));

        if ($modifyTo)
            $date = date("Y-m-d H:i:s", strtotime($date . "+1 day"));

        return $date;
    }

    public static function formatDate($date, $format = '') {
        if (empty($format))
            $format = "dS M, Y H:i A";

        return date($format, strtotime($date));
    }

    /**
     * Method used to format DB field
     * @param string $field
     * @return string
     */
    public static function formatTableField($field) {
        if (empty($field))
            return '1';

        return "replace(" . $field . ",'\\\','')";
    }

    /**
     * Method used to generate captch hash code
     * @param type $value
     * @return type
     */
    public static function rpHash($value) {
        $hash = 5381;
        $value = strtoupper($value);
        for ($i = 0; $i < strlen($value); $i++) {
//$hash = (($hash << 5) + $hash) + ord(substr($value, $i));
            $hash = (self::leftShift32($hash, 5) + $hash) + ord(substr($value, $i));
        }
        return $hash;
    }

    /**
     * Method used for enryption
     * @param integer $number
     * @param integer $steps
     * @return string
     */
    public static function leftShift32($number, $steps) {
// convert to binary (string)
        $binary = decbin($number);
// left-pad with 0's if necessary
        $binary = str_pad($binary, 32, "0", STR_PAD_LEFT);
// left shift manually
        $binary = $binary . str_repeat("0", $steps);
// get the last 32 bits
        $binary = substr($binary, strlen($binary) - 32);
// if it's a positive number return it
// otherwise return the 2's complement
        return ($binary{0} == "0" ? bindec($binary) :
                -(pow(2, 31) - bindec(substr($binary, 1))));
    }

    public static function arrayFlatten($array, $return = array()) {

        if (empty($array))
            return false;

        foreach ($array as $key => $value) {
            if (@is_array($value)) {
                $return = self::arrayFlatten($value, $return);
            } elseif (@$value) {
                $return[$key] = $value;
            }
        }
        return $return;
    }

    public static function generateTimeRangeString($fromDate, $toDate) {
        if (empty($fromDate) && empty($toDate))
            return false;

        $timeStr = "";

        $diff = abs(strtotime($fromDate) - strtotime($toDate));
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

        if (!empty($years)) {
            if ($years > 1)
                $timeStr .= '&nbsp;' . $years . ' years';
            else
                $timeStr .= '&nbsp;' . $years . ' year';
        }

        if (!empty($months)) {
            if ($months > 1)
                $timeStr .= '&nbsp;' . $months . ' months';
            else
                $timeStr .= '&nbsp;' . $months . ' month';
        }

        if (empty($years) && empty($months)) {
            if ($days > 1)
                $timeStr .= '&nbsp;' . $days . ' days';
            else
                $timeStr .= '&nbsp;' . $days . ' day';
        }

        return $timeStr;
    }

    public static function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }

    public static function getMailTemplate($emailBody = '') {
        $string = '<table width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                        <tr>
                            <td style="padding:20px;background:#4795DB;text-align:center;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#ffffff;"></td>
                        </tr>
                        <tr>
                            <td style="padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px;background:#4795DB;text-align:center;font-family:Arial,Helvetica,sans-serif"><table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#ffffff;border-radius:10px;">
                                    <tbody>
                                        <tr>
                                            <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td  width="100%" align="center" style="padding:20px;"><img style="display:block;" width="110" src="' . asset('public/administrator/images/logo.png') . '"/></td>
                                                        </tr>
                                                    </tbody>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:10px;font-size:25px;font-weight:bold;line-height:20px;color:#a2a1a1;font-family:Arial,Helvetica,sans-serif;" align="center">' . SITETITLE . '</td>
                                        </tr>
                                        <tr>
                                            <td style="color:#333333;" align="center">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left" style="color:#6f6f6f;font-size:14px;border-left:1px solid #dfdfdf;border-right:1px solid #dfdfdf;line-height:150%; padding-top:20px; padding-bottom:20px; padding-left:20px; padding-right:20px;font-family:Arial,Helvetica,sans-serif;border-top:solid 1px #4795DB;"> ' . $emailBody . '</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" align="left" style="padding:10px;font-size:12px;line-height:20px;color:#a2a1a1;font-family:Arial,Helvetica,sans-serif; border-top:solid 1px #4795DB;">If you have any questions about your account, please send an email to <a href="mailto:' . ADMINEMAIL . '" style="color:#ffd16e; text-decoration:none;">' . ADMINEMAIL . '</a> </td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" align="center" style="padding:10px;font-size:14px;line-height:20px;font-weight:bold;color:#a2a1a1;font-family:Arial,Helvetica,sans-serif;">' . SITETITLE . ' &copy; copyright</td>
                                        </tr>
                                    </tbody>
                                </table></td>
                        </tr>
                    </tbody>
                    </table>';
        return $string;
    }
    
    public static function getTimeDifference($createdTime) {
        date_default_timezone_set('Asia/Calcutta');
        $str = strtotime($createdTime);
        $today = strtotime(date('Y-m-d H:m:i'));    /* It returns the time difference in Seconds... */
        $time_differnce = $today - $str;        /* To Calculate the time difference in Years... */
        $years = 60 * 60 * 24 * 365;        /* To Calculate the time difference in Months... */
        $months = 60 * 60 * 24 * 30;        /* To Calculate the time difference in Days... */
        $days = 60 * 60 * 24;        /* To Calculate the time difference in Hours... */
        $hours = 60 * 60;        /* To Calculate the time difference in Minutes... */
        $minutes = 60;
        if (intval($time_differnce / $years) > 1) {
            return intval($time_differnce / $years) . " years ago";
        } else if (intval($time_differnce / $years) > 0) {
            return intval($time_differnce / $years) . " year ago";
        } else if (intval($time_differnce / $months) > 1) {
            return intval($time_differnce / $months) . " months ago";
        } else if (intval(($time_differnce / $months)) > 0) {
            return intval(($time_differnce / $months)) . " month ago";
        } else if (intval(($time_differnce / $days)) > 1) {
            return intval(($time_differnce / $days)) . " days ago";
        } else if (intval(($time_differnce / $days)) > 0) {
            return intval(($time_differnce / $days)) . " day ago";
        } else if (intval(($time_differnce / $hours)) > 1) {
            return intval(($time_differnce / $hours)) . " hours ago";
        } else if (intval(($time_differnce / $hours)) > 0) {
            return intval(($time_differnce / $hours)) . " hour ago";
        } else if (intval(($time_differnce / $minutes)) > 1) {
            return intval(($time_differnce / $minutes)) . " minutes ago";
        } else if (intval(($time_differnce / $minutes)) > 0) {
            return intval(($time_differnce / $minutes)) . " minute ago";
        } else if (intval(($time_differnce)) > 1) {
            return intval(($time_differnce)) . " seconds ago";
        } else {
            return "few seconds ago";
        }
    } 
      
}
