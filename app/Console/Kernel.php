<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\SendCouponCode',
        '\App\Console\Commands\SetCurrencyExchangeRate',
        '\App\Console\Commands\ExpirePoints',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('SendCouponCode:sendcouponcode')->everyMinute();

        $selectGeneralSettingsSetCron = \App\Model\Generalsettings::where('groupName', 'CurrencySettings')->where('settingsKey', 'auto_update')->get();
        if($selectGeneralSettingsSetCron[0]->settingsValue == 1){
            $selectGeneralSettingsCromTime = \App\Model\Generalsettings::where('groupName', 'CurrencySettings')->where('settingsKey', 'update_scheduled_time')->get();
            $schedule->call('App\Http\Controllers\Administrator\CurrencyController@updatecurrencyDefault')->dailyAt($selectGeneralSettingsCromTime[0]->settingsValue);
        }

        $schedule->command('ExpirePoints:expirepoints')->dailyAt('00:01');
        $schedule->command('ExpirePoints:expirepoints')->dailyAt('00:30');
        $schedule->command('dbbackup:delete')->dailyAt('01:30');
        $schedule->command('ScheduleNotification:send')->dailyAt('06:30');
        $schedule->command('Shipmentemailnotification:send')->everyFiveMinutes();
        //$schedule->command('storageExpiry:notify')->dailyAt('05:30');
        //$schedule->command('quickshipout:notify')->everyFiveMinutes();	
        
        /* Backup DB schedule */
        $eventBy = \App\Model\Generalsettings::where('settingsKey', 'event_by')->first();
        $eventOn = \App\Model\Generalsettings::where('settingsKey', 'event_on')->first();
        if($eventBy->settingsValue == 'daily')
        {
            $eventTime = explode("_",$eventOn->settingsValue);
            $schedule->command('db:backup')->dailyAt($eventTime[0].":".$eventTime[1]);
        }
        else if($eventBy->settingsValue == 'weekly')
        {
            $schedule->command('db:backup')->weeklyOn($eventOn->settingsValue, '1:00');
        }
        else if($eventBy->settingsValue == 'monthly')
        {
            $schedule->command('db:backup')->monthlyOn($eventOn->settingsValue, '1:00');
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
