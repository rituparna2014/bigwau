<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Config;
use customhelper;
use Illuminate\Support\Facades\DB;

class QuickshipoutStatusNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quickshipout:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to notify customers for their quick shipout shipments status change';
    
    protected $process;
    public $fileName = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            
            $quickShipout = \App\Model\Quickshipoutstatusnotification::where("emailSentStatus","0")->orWhere("smsSentStatus","0")->get();
            $selectedIds = $quickShipout->pluck('id')->toArray();

            $quickShioutData = $quickShipout->get();


            if(count($quickShioutData)>0) {
                ///Update Statusnotification by 1 in emailsent and smssent
                \App\Model\Quickshipoutstatusnotification::whereIn("id",$selectedIds)->update(["emailSentStatus"=>"1","smsSentStatus"=>"1"]);
                foreach($quickShioutData as $eachShipout) {
                   
                     $userInfo = \App\Model\User::where('email', $eachShipout->useremail)->first();

                     $replace['[NAME]'] = $userInfo['firstName'] . " " . $userInfo['lastName'];

                     $replace['[SHIPMENTID]'] = $eachShipout->quickshipoutId;

                     $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatusQuickShipout();
                     $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);

                     $quickShipoutStatuslog = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $eachShipout['quickshipoutId'])->get();

                     ///////////////////////////////////////replace by html/////////////////////////////////////////////// 
                     $replace['[QUCIKSHIPOUTAIR_STATUS]'] = "";


                    \App\Model\Quickshipoutstatusnotification::where("id",$eachShipout->id)->update(["emailSentStatus"=>$emailSent,"emailsentOn"=>$emailSentOn,"smsSent"=>$smsSentStatus,"smssentOn"=>$smsSentOn]);
                }

                $this->info('Email sent');
            } else {
                $this->info('No upcomming notifications exist');
            }

        } catch (ProcessFailedException $exception) {
            $this->error('Email sent failed');
        }
    }
}
