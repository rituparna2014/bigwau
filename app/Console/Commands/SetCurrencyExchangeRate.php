<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class SetCurrencyExchangeRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SetCurrencyExchangeRate:setcurrencyexchangerate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set Currency Exchange Rate through CRON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Currency Exchange');
    }
}
