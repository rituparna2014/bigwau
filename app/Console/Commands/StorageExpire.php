<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Config;
use customhelper;
use Illuminate\Support\Facades\DB;

class StorageExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storageExpiry:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Storage expiry notification';
    
    protected $process;
    public $fileName = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            
            $shipment = new \App\Model\Shipment;
            $shipmentDelivery = new \App\Model\Shipmentdelivery;
            $shipmentDeliveryTable = $shipmentDelivery->prefix.$shipmentDelivery->table;
            
            $sentMessages = array("5"=>array(),"3"=>array(),"1"=>array());
            $where = "($shipmentDeliveryTable.maxStorageDate = (CURDATE() + INTERVAL 5 DAY) OR $shipmentDeliveryTable.maxStorageDate = (CURDATE() + INTERVAL 3 DAY) OR $shipmentDeliveryTable.maxStorageDate = (CURDATE() + INTERVAL 1 DAY))";
            
            $shipmentDelivery = \App\Model\Shipmentdelivery::select(
                    array("$shipmentDelivery->table.id","$shipmentDelivery->table.maxStorageDate","shipmentId","userId",
                        DB::raw("(case when ((CURDATE() + INTERVAL 5 DAY) = $shipmentDeliveryTable.maxStorageDate) then 5 when ((CURDATE() + INTERVAL 3 DAY) = $shipmentDeliveryTable.maxStorageDate) then 3 when ((CURDATE() + INTERVAL 1 DAY) = $shipmentDeliveryTable.maxStorageDate) then 1 end) as daydiff")))
                    ->join("$shipment->table","$shipment->table.id","=","$shipmentDelivery->table.shipmentId")
                    ->whereRaw($where)
                    ->where("$shipment->table.paymentStatus","unpaid")
                    ->get();
            if($shipmentDelivery->count() > 0) {
                foreach($shipmentDelivery as $eachDelivery) {
                    //$dayDifference = 
                    if(!in_array($eachDelivery->shipmentId,$sentMessages[$eachDelivery->daydiff])) {
                        $userInfo = \App\Model\User::find($eachDelivery->userId);
                        $to = $userInfo->email;
                        $cc = "rosy.chakraborty@indusnet.co.in";
                        $replace['[NAME]'] = $userInfo->firstName . " " . $userInfo->lastName;
                        $replace['[SHIPMENTID]'] = $eachDelivery->shipmentId;
                        $replace['[REMAINING_DAYS]'] = $eachDelivery->daydiff;
                        $emailTemplate = \App\Model\Emailtemplate::where('templateKey','shipment_storage_expiry')->first();
                        $sendMail = customhelper::SendMailWithCC($emailTemplate, $replace, $to,$cc);
                        if($sendMail)
                        {
                            $sentMessages[$eachDelivery->daydiff][] = $eachDelivery->shipmentId;
                        }
                    }
                }
                $this->info('Email sent');
            } else {
                $this->info('No upcomming notifications exist');
            }

        } catch (ProcessFailedException $exception) {
            $this->error('Email sent failed');
        }
    }
}
