<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use customhelper;

class SendCouponCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendCouponCode:sendcouponcode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Coupon Code Sending to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $Newuser = \App\Model\Sentlogcouponcode::where('sent', 'N')->limit(5)->get();

        foreach($Newuser as $key => $value){
            /* EMAIL FUNCTION */
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'send_couponcode')->first();
            $replace['[NAME]'] = $value->firstName . " " . $value->lastName;
            $replace['[COUPONCODE]'] = $value->couponCode;
            $replace['[EMAIL]'] = $value->email;
            $to = $value->email;
            customhelper::SendMail($emailTemplate, $replace, $to);
            /* EMAIL END */

            /* SET THE FLAG THAT EMAIL HAS BEEN SENT FOR THIS USER */
            $sentLogCoupon = \App\Model\Sentlogcouponcode::find($value->id);
            $sentLogCoupon->sent = 'Y';
            $sentLogCoupon->save();
        }
        
        
        $this->info('Coupon code sent to users');
    }
}
