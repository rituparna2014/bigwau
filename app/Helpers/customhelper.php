<?php

namespace App\Helpers;

use App\Model\Permission;
use App\Model\User;
use Auth;
use App\Model\UserPermission;
use App\Model\Usernotification;
use Config;
use Mail;
use App\Model\Currency;
use DB;

class customhelper {

    public static function seePermission($menuId, $userId) {
        $permissionRecord = UserPermission::getPermissionRecordsByUserId($menuId, $userId);
        $canArray = [];

        if ($userId == 1) {
            $canArray['canView'] = 1;
            $canArray['canAdd'] = 1;
            $canArray['canEdit'] = 1;
            $canArray['canDelete'] = 1;
        } else {
            $canArray['canView'] = $permissionRecord[0]->permissionView;
            $canArray['canAdd'] = $permissionRecord[0]->permissionAdd;
            $canArray['canEdit'] = $permissionRecord[0]->permissionEdit;
            $canArray['canDelete'] = $permissionRecord[0]->permissionDelete;
        }

        return $canArray;
    }

    public static function generateSiteTree($startAt, $userId) {
        if ($children = UserPermission::getLeftNavByUserId($startAt, Auth::user()->id)) {
            $thisLevel = array();
            foreach ($children as $child) {   #echo $child->id."<br>";
                $thisLevel[$child->id] = $child;
                $thisLevel[$child->id]->children = customhelper::generateSiteTree($child->adminMenuId, $userId);
            }
            return $thisLevel;
        }
    }

    public static function SendMail($emailTemplate, $replace, $to) {
        //$mailTemplatevars = array('[NAME]', '[EMAIL]', '[TOKEN]', '[PASSWORD]', '[NOTIFICATION]');
        $mailTemplatevars = Config::get('constants.emailTemplateVariables');

        $arrayvalues = [];
        foreach ($mailTemplatevars as $key => $val) {
            if (isset($replace[$val]) == $val) {
                $arrayvalues[] = $replace[$val];
            } else {
                $arrayvalues[] = '';
            }
        }

        if(empty($emailTemplate->fromEmail))
        {
            $emailTemplate->fromEmail = "rituparna.mitra@indusnet.co.in";
        }

        if(empty($emailTemplate->fromName))
        {
            $emailTemplate->fromName = "Bigwau";
        }

        $content = str_replace($mailTemplatevars, $arrayvalues, stripslashes($emailTemplate->templateBody));

        Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($emailTemplate, $to) {
            $message->from($emailTemplate->fromEmail, $emailTemplate->fromName);
            $message->subject($emailTemplate->templateSubject);
            $message->to($to);
        });

        return true;
    }
    
    public static function SendMailWithStaticContent($content,$subject,$to) {
        
        $fromEmail = "contact@shoptomydoor.com";
        $fromName = "Shoptomydoor";
        
        Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($fromEmail,$fromName,$subject, $to) {
            $message->from($fromEmail, $fromName);
            $message->subject($subject);
            $message->to($to);
        });
        
        return true;
    }

        public static function GetEmailContent($emailTemplate, $replace) {
        
        $mailTemplatevars = Config::get('constants.emailTemplateVariables');

        $arrayvalues = [];
        foreach ($mailTemplatevars as $key => $val) {
            if (isset($replace[$val]) == $val) {
                $arrayvalues[] = $replace[$val];
            } else {
                $arrayvalues[] = '';
            }
        }
        
        $content = str_replace($mailTemplatevars, $arrayvalues, stripslashes($emailTemplate->templateBody));
        
        return $content;
    }
    
    public static function GetSMSContent($replaceTo,$templateContent) {
        
        $smsTemplatevars = Config::get('constants.SmsTemplateVariables');

        $arrayvalues = [];
        foreach ($smsTemplatevars as $key => $val) {
            if (isset($replaceTo[$val]) == $val) {
                $arrayvalues[] = $replaceTo[$val];
            } else {
                $arrayvalues[] = '';
            }
        }

        $content = str_replace($smsTemplatevars, $arrayvalues, stripslashes($templateContent->templateBody));
        
        return $content;
    }

    public static function SendMailWithCC($emailTemplate, $replace, $to,$cc) {
        //$mailTemplatevars = array('[NAME]', '[EMAIL]', '[TOKEN]', '[PASSWORD]', '[NOTIFICATION]');
        $mailTemplatevars = Config::get('constants.emailTemplateVariables');

        $arrayvalues = [];
        foreach ($mailTemplatevars as $key => $val) {
            if (isset($replace[$val]) == $val) {
                $arrayvalues[] = $replace[$val];
            } else {
                $arrayvalues[] = '';
            }
        }

        $content = str_replace($mailTemplatevars, $arrayvalues, stripslashes($emailTemplate->templateBody));

        Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($emailTemplate, $to, $cc) {
            $message->from($emailTemplate->fromEmail, $emailTemplate->fromName);
            $message->subject($emailTemplate->templateSubject);
            $message->to($to)->cc($cc);
        });

        return true;
    }
    
    public static function SendMailWithAttachment($emailTemplate, $replace, $to,$filePath) {
        //$mailTemplatevars = array('[NAME]', '[EMAIL]', '[TOKEN]', '[PASSWORD]', '[NOTIFICATION]');
        $mailTemplatevars = Config::get('constants.emailTemplateVariables');

        $arrayvalues = [];
        foreach ($mailTemplatevars as $key => $val) {
            if (isset($replace[$val]) == $val) {
                $arrayvalues[] = $replace[$val];
            } else {
                $arrayvalues[] = '';
            }
        }

        $content = str_replace($mailTemplatevars, $arrayvalues, stripslashes($emailTemplate->templateBody));

        Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($emailTemplate, $to,$filePath) {
            $message->from($emailTemplate->fromEmail, $emailTemplate->fromName);
            $message->subject($emailTemplate->templateSubject);
            $message->to($to);
            $message->attach($filePath);
        });

        return true;
    }

    public static function getCurrencySymbol($string = '') {
        $getDefaultCurrency = Currency::getDefaultCurrency();
        
        if (isset($getDefaultCurrency[0])) {            
            if ($getDefaultCurrency[0]->symbol != '') {
                return $replcedText = str_replace("[CURRENCY]", $getDefaultCurrency[0]->symbol, $string);
            } else {
                return $replcedText = str_replace("[CURRENCY]", $getDefaultCurrency[0]->code, $string);
            }
            
        } else {
           
            return $string;
        }
        
        
    }

    public static function getCurrencySymbolCode($code = '', $returnCode = false) {
        if (empty($code)) {
            $getCurrency = Currency::getDefaultCurrency();
        } else {
            $getCurrency = Currency::getCurrencyData($code);
        }

        if ($returnCode == false) {
            if (isset($getCurrency[0])) {
                if ($getCurrency[0]->symbol != '') {
                    return $getCurrency[0]->symbol;
                } else {
                    return $getCurrency[0]->code;
                }
            } else {
                return "";
            }
        } else {
            if (isset($getCurrency[0])) {
                return $getCurrency[0]->code;
            } else {
                return "";
            }
        }
    }

    public static function getCurrencySymbolFormat($string, $code = '') {
        $string = number_format((float) $string, 2, '.', '');
        if (empty($code)) {
            $getDefaultCurrency = Currency::getDefaultCurrency();
        } else {
            $getDefaultCurrency = Currency::getCurrencyData($code);
        }

        if (isset($getDefaultCurrency[0])) {
            if ($getDefaultCurrency[0]->symbol != '') {
                $replcedText = str_replace("9.99", $string, Config::get('constants.currencyFormat.' . $getDefaultCurrency[0]->format));

                return $replcedText = str_replace("[Currency]", $getDefaultCurrency[0]->symbol, $replcedText);
            } else {
                $replcedText = str_replace("9.99", $string, Config::get('constants.currencyFormat.' . $getDefaultCurrency[0]->format));

                return $replcedText = str_replace("[Currency]", $getDefaultCurrency[0]->code, $replcedText);
            }
        } else {
            return $string;
        }
    }

    /**
     * Method to get tax value and type for tax settings
     * @param int
     * @param string
     * @return string
     */
    public static function gettaxSettingsData($type, $countryId, $paymentMethodId) {
        $returnItem = \App\Model\Paymenttaxsettings::select($type)->where('paymentMethodId', $paymentMethodId)->where('countryId', $countryId)->get()->toArray();

        if (!empty($returnItem)) {
            return $returnItem[0][$type];
        } else {
            return '';
        }
    }

    /**
     * Method to extract the currency values only excepting currency symbol
     * @param $string
     * @return string
     */
    public static function getExtractCurrency($string) {
        $getDefaultCurrency = Currency::getDefaultCurrency();
        $symbol = $getDefaultCurrency[0]->symbol;
        if ($getDefaultCurrency[0]->symbol != '') {
            $string = str_replace($symbol, '', $string);
        } else {
            $code = $getDefaultCurrency[0]->code;
            $string = str_replace($code, '', $string);
        }
        return $string;
    }

    /**
     * Method to get the countries
     * @param $string as string
     * @return array
     */
    public static function getCountries($string) {
        $cnt = explode(',', $string);
        $name = array();
        foreach($cnt as $key => $val){
            $getCountries = \App\Model\Country::where("id", $val)->get();
            $name[] = $getCountries[0]->name;
        }
        return $name;
    }

    /**
     * Method to fetch country / state / city name
     * @param int
     * @param string
     * @return string
     */
    public static function getCountryStateCityName($id, $type) {
        if ($type == 'country')
            $returnItem = \App\Model\Country::select('name')->where('id', $id)->first();
        elseif ($type == 'state')
            $returnItem = \App\Model\State::select('name')->where('id', $id)->first();
        else
            $returnItem = \App\Model\City::select('name')->where('id', $id)->first();

        if (!empty($returnItem)) {
            return $returnItem->name;
        } else {
            return '';
        }
    }


    /**
     * Method to insert data into users notification
     * @param int
     * @param string
     * @return string
     */
    public static function insertUserNotification($userId, $senderId, $message, $type, $subject) {
        /* TYPE CAN BE  1=Customer Notification, 2=Fund Point, 3=Fund Wallet, 4=Warehouse Message, 5=Item received in Procurement, 6=Order Created */

        $userNotification = new Usernotification;

        $userNotification->userId = $userId;
        $userNotification->message = $message;
        $userNotification->subject = $subject;
        $userNotification->senderId = $senderId;
        $userNotification->type = $type;
        $userNotification->sentOn = Config::get('constants.CURRENTDATE');
        $userNotification->save();

        return true;
    }


    /*
    * Method to send message/otp to mobile
    * 
    */
    public static function sendMSG($to,$replaceTo,$templateContent){
        
        $smsTemplatevars = Config::get('constants.SmsTemplateVariables');

        $arrayvalues = [];
        foreach ($smsTemplatevars as $key => $val) {
            if (isset($replaceTo[$val]) == $val) {
                $arrayvalues[] = $replaceTo[$val];
            } else {
                $arrayvalues[] = '';
            }
        }

        $content = str_replace($smsTemplatevars, $arrayvalues, stripslashes($templateContent->templateBody));

//        $curl = curl_init();
//        $postFields = array(
//            'from'=>Config::get('constants.SmsSettings.FROMNAME'),
//            'to'=>$to,
//            'text'=>$content
//        );
//        $postFieldsJson = json_encode($postFields);
//        curl_setopt_array($curl, array(
//
//            CURLOPT_URL => Config::get('constants.SmsSettings.BASEURL')."sms/2/text/single",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 30,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "POST",
//            CURLOPT_POSTFIELDS => $postFieldsJson,
//            CURLOPT_HTTPHEADER => array("accept: application/json","authorization: Basic ".Config::get('constants.SmsSettings.AUTHKEY'),"content-type: application/json"),
//            )
//        );
//
//        $response = curl_exec($curl);
//        $err = curl_error($curl);
//        
//        curl_close($curl);

        $smslog = new \App\Model\Sendsmslog;
        $smslog->number = $to;
        $smslog->smsContent = $content;
        $smslog->gatewayResponse = '';
        $smslog->createdOn = Config::get('constants.CURRENTDATE');
        $smslog->save();
        
        
//        if ($err) {
//          //echo "cURL Error #:" . $err;
//            return false;
//        } else {
//          //echo $response;
//            return true;
//        }
        
        return true;
    }
    
    public static function sendMSGWithStaticContent($to,$content){

        $curl = curl_init();
        $postFields = array(
            'from'=>Config::get('constants.SmsSettings.FROMNAME'),
            'to'=>$to,
            'text'=>$content
        );
//        $postFieldsJson = json_encode($postFields);
//        curl_setopt_array($curl, array(
//
//            CURLOPT_URL => Config::get('constants.SmsSettings.BASEURL')."sms/2/text/single",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 30,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "POST",
//            CURLOPT_POSTFIELDS => $postFieldsJson,
//            CURLOPT_HTTPHEADER => array("accept: application/json","authorization: Basic ".Config::get('constants.SmsSettings.AUTHKEY'),"content-type: application/json"),
//            )
//        );
//
//        $response = curl_exec($curl);
//        $err = curl_error($curl);
//
//        curl_close($curl);
//
//        if ($err) {
//          //echo "cURL Error #:" . $err;
//            return false;
//        } else {
//          //echo $response;
//            return true;
//        }
        return true;
    }

    /* Method to generate the otp
    * Bydefault generate 6 digits otp
    * Or user define digits
    * param-$digits(optional)
    */
    public static function generateOTP($digits=''){

        if(empty($digits)){
            $digits = 6;
        }
        $createOTP = rand(pow(10, $digits-1), pow(10, $digits)-1);

        return $createOTP;

    }

    /* Method to crop banner / image
     * @param type $sourcePath
     * @param type $destinationPath
     * @param type $imageName
     * @param type $width
     * @param type $height
     * @return string
    */
    public static function bannerCrop($sourcePath, $destinationPath, $imageName, $width, $height){
        if(mime_content_type(public_path($sourcePath.$imageName)) == "image/jpeg"){
            $im_php = imagecreatefromjpeg(public_path($sourcePath.$imageName));
            $new = imagescale($im_php, $width, $height);
            $upload = imagejpeg($new, public_path($destinationPath.$imageName));
        } else {
            $im_php = imagecreatefrompng(public_path($sourcePath.$imageName));
            $new = imagescale($im_php, $width, $height);
            $upload = imagepng($new, public_path($destinationPath.$imageName));
        }

        return $upload;

    }

}
