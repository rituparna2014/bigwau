<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
/* Create Image Url */
Route::get('/profileimage/{filename}', function ($filename) {
    $path = public_path() . '/uploads/profile/thumbnail/' . $filename;
    if (!File::exists($path))
        abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
})->name('profileimage');
Route::get('/blogimages/{filename}', function ($filename) {
    $path = public_path() . '/uploads/blog/thumbnail/' . $filename;
    if (!File::exists($path))
        abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
})->name('blogimages');

//, 'middleware' => ['validuser']
Route::get('/', 'HomeController@index');
Route::get('admin', array('before' => 'auth', 'uses' => 'Administrator\IndexController@index'));
Route::group(['prefix' => 'administrator'], function() {

    /* Authentication */
    Route::get('login', 'Administrator\IndexController@index')->name('login');
    Route::post('login', 'Auth\LoginController@doLogin');
    Route::get('logout', 'Auth\LoginController@logout');


    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');

    /* DASHBOARD */
    Route::get('dashboard', 'Administrator\DashboardController@getIndex');
    Route::get('dashboard/index', 'Administrator\DashboardController@getIndex');
    Route::post('dashboard/searchbyselection/{searchKey?}/{searchType?}', 'Administrator\DashboardController@searchbyselection');
    Route::post('dashboard/getSearchOption/{searchKey?}', 'Administrator\DashboardController@getSearchOption');
    Route::post('dashboard/getCountryName/{searchKey?}', 'Administrator\DashboardController@getCountryName');
    Route::post('dashboard/getWarehouseName/{searchKey?}', 'Administrator\DashboardController@getWarehouseName');
    Route::post('dashboard/getChartsRecords/{searchKey?}', 'Administrator\DashboardController@getChartsRecords');
    Route::post('dashboard/getOrderRecords', 'Administrator\DashboardController@getOrderRecords');
    Route::post('dashboard/getShipmentStatus', 'Administrator\DashboardController@getShipmentStatus');
    Route::post('dashboard/getCompletedOrder', 'Administrator\DashboardController@getCompletedOrder');
    Route::post('dashboard/getTotalShipment', 'Administrator\DashboardController@getTotalShipment');
    Route::post('dashboard/getTotalRevenue', 'Administrator\DashboardController@getTotalRevenue');
    Route::post('dashboard/gettopCustomers', 'Administrator\DashboardController@gettopCustomers');
    Route::post('dashboard/getshippingnotifications', 'Administrator\DashboardController@getshippingnotifications');
    Route::post('dashboard/getcontactnotifications', 'Administrator\DashboardController@getcontactnotifications');
    /* Reset Password */
    /* Route::get('password/reset', 'Auth\ResetPasswordController@resetauthenticatedView'); 
      Route::post('password/reset', 'Auth\ResetPasswordController@resetAuthenticated'); */

    /* Content */

    /* My Profile */
    Route::resource('myProfile', 'Administrator\MyProfileController');

    /* Admin Users Management */
    Route::get('adminusers', 'Administrator\AdminuserController@index');
    Route::get('adminusers/index', 'Administrator\AdminuserController@index');
    Route::post('adminusers/index', 'Administrator\AdminuserController@index');
    Route::get('adminusers/changestatus/{id?}/{page?}/{status?}', 'Administrator\AdminuserController@changestatus');
    Route::get('adminusers/addedit/{id?}/{page?}', 'Administrator\AdminuserController@addedit');
    Route::post('adminusers/save/{id?}/{page?}', 'Administrator\AdminuserController@savedata');
    Route::post('adminusers/add/{page?}', 'Administrator\AdminuserController@adddata');
    Route::get('adminusers/changepassword/{id?}/{page?}', 'Administrator\AdminuserController@changepassword');
    Route::post('adminusers/changepassword/{id?}/{page?}', 'Administrator\AdminuserController@changepassword');
    Route::get('adminusers/notify/{id?}/{page?}', 'Administrator\AdminuserController@notify');
    Route::post('adminusers/notify/{id?}/{page?}', 'Administrator\AdminuserController@notify');
    Route::post('adminusers/exportall/{page?}', 'Administrator\AdminuserController@exportall');
    Route::post('adminusers/exportselected/{page?}', 'Administrator\AdminuserController@exportSelected');
    Route::get('adminusers/editrole/{id?}', 'Administrator\AdminuserController@editRole');
    Route::post('adminusers/editrole/{id?}', 'Administrator\AdminuserController@saveRole');
    Route::get('adminusers/showall', 'Administrator\AdminuserController@showall');


    /* Notification History */
    Route::get('notificationhistory/{id?}', 'Administrator\NotificationhistoryController@index');
    Route::get('notificationhistory/index/{id?}', 'Administrator\NotificationhistoryController@index');
    Route::post('notificationhistory/index/{id?}', 'Administrator\NotificationhistoryController@index');

    /* Fund Wallet */
    Route::resource('fundwallet', 'Administrator\FundwalletController');
    /* Fund Point */
    Route::resource('fundpoint', 'Administrator\FunduserpointController');

    /* admin User Roles */
    Route::get('adminrole', 'Administrator\AdminroleController@index');
    Route::get('adminrole/index', 'Administrator\AdminroleController@index');
    Route::post('adminrole/index', 'Administrator\AdminroleController@index');
    Route::get('adminrole/changestatus/{id?}/{status?}', 'Administrator\AdminroleController@changestatus');

    /* Permissions for admin user (roles) */
    Route::resource('permission', 'Administrator\PermissionController');

    /* Authentication required */
    Route::resource('authentication', 'Administrator\AuthenticationrequiredController');

    /* Test Controller */
    Route::resource('test', 'Administrator\TestController');

    /* Banner */
    Route::get('homepagebanner', 'Administrator\BannerController@showHomepageBanner');
    Route::post('homepagebanner', 'Administrator\BannerController@showHomepageBanner');
    Route::get('registrationbanner', 'Administrator\BannerController@showRegistrationBanner');
    Route::post('registrationbanner', 'Administrator\BannerController@showRegistrationBanner');
    Route::get('warehousebanner', 'Administrator\BannerController@showWarehouseBanner');
    Route::post('warehousebanner', 'Administrator\BannerController@showWarehouseBanner');
    Route::get('adbanner', 'Administrator\BannerController@showadbanner');
    Route::post('adbanner', 'Administrator\BannerController@showadbanner');
    Route::post('addbanner/{type?}/{page?}', 'Administrator\BannerController@addBanner');
    Route::get('addedithomepagebanner/{id?}/{page?}', 'Administrator\BannerController@addedithomepagebanner');
    Route::post('editbanner/{type?}/{id?}/{page?}', 'Administrator\BannerController@editBanner');
    Route::get('editbannerstatus/{id?}/{status?}', 'Administrator\BannerController@editBannerStatus');
    Route::get('bannerdelete/{id?}/{type?}/{page?}', 'Administrator\BannerController@bannerDelete');
    Route::post('bannerdeletemultiple/{type?}', 'Administrator\BannerController@bannerdeletemultiple');
    Route::get('bannerinfo/{id?}', 'Administrator\BannerController@getBannerInfo');
    Route::get('autobanner', 'Administrator\BannerController@showAutoBanner');
    Route::post('autobanner', 'Administrator\BannerController@showAutoBanner');
    Route::get('shopformebanner', 'Administrator\BannerController@showShopformeBanner');
    Route::post('shopformebanner', 'Administrator\BannerController@showShopformeBanner');
    Route::get('fillnshipbanner', 'Administrator\BannerController@showFillnshipBanner');
    Route::post('fillnshipbanner', 'Administrator\BannerController@showFillnshipBanner');
    Route::get('specialofferbanner', 'Administrator\BannerController@showSpecialofferBanner');
    Route::post('specialofferbanner', 'Administrator\BannerController@showSpecialofferBanner');
    Route::get('editbannerdefaultstatus/{id?}/{page?}/{type?}', 'Administrator\BannerController@editbannerdefaultstatus')->name('editbannerdefaultstatus');

    /* Split Shipment */
    Route::get('splitshipmentrequests', 'Administrator\SplitShipmentController@index');
    Route::post('splitshipmentrequests', 'Administrator\SplitShipmentController@index');
    Route::get('addeditsplitshipment/{id?}/{page?}', 'Administrator\SplitShipmentController@addeditsplitshipment');
    Route::post('addeditsplitshipmentrecord/{id?}/{page?}', 'Administrator\SplitShipmentController@addeditsplitshipmentrecord');
    Route::get('deletesplitshipment/{id?}/{page?}', 'Administrator\SplitShipmentController@deletesplitshipment');
    Route::get('editsplitshipmentstatus/{id?}/{status?}', 'Administrator\SplitShipmentController@editstatus');

    /* Block Content */
    Route::get('blockcontent', 'Administrator\BlockcontentController@index');
    Route::post('blockcontent', 'Administrator\BlockcontentController@index');
    Route::get('blockcontentedit/{id?}/{slug?}', 'Administrator\BlockcontentController@geteditcontent');
    Route::post('blockcontentupdate/{id?}', 'Administrator\BlockcontentController@updtecontent');
    Route::get('editblockcontentstatus/{id?}/{status?}', 'Administrator\BlockcontentController@editstatus');
    Route::get('blockcontent/add', 'Administrator\BlockcontentController@showaddform')->name('addblockcontent');
    Route::post('blockcontent/save', 'Administrator\BlockcontentController@addData')->name('saveblockcontent');
    Route::get('blockcontent/delete/{id?}', 'Administrator\BlockcontentController@deleteData');

    /* Site categories */
    Route::get('settingscategories', 'Administrator\SitecategoryController@index');
    Route::post('settingscategories', 'Administrator\SitecategoryController@index');
    Route::get('addeditcategory/{section?}/{id?}/{page?}', 'Administrator\SitecategoryController@addeditcategory');
    Route::post('addeditcategoryrecord/{id?}/{page?}', 'Administrator\SitecategoryController@addeditrecord');
    Route::get('editsitecategorystatus/{id?}/{status?}', 'Administrator\SitecategoryController@editstatus');
    Route::get('deletecategory/{id?}/{parentId?}/{page?}', 'Administrator\SitecategoryController@deleterecord');
    Route::get('getsubcategory/{rootCategory?}', 'Administrator\SitecategoryController@getsubcategory');

    /* Site static & landing pages */
    Route::get('pagelist/{pagetype?}', 'Administrator\SitepageController@staticpage')->name('pagelist');
    Route::post('pagelist/{pagetype?}', 'Administrator\SitepageController@staticpage')->name('pagelist');
    Route::get('addeditpage/{id?}/{pagetype?}/{page?}', 'Administrator\SitepageController@addeditpage')->name('addeditpage');
    Route::post('addeditpagerecord/{id?}/{pagetype?}/{page?}', 'Administrator\SitepageController@addeditpagerecord');
    Route::get('editpagestatus/{id?}/{status?}', 'Administrator\SitepageController@editstatus');
    Route::get('deletepage/{id?}/{pagetype?}/{page?}', 'Administrator\SitepageController@deleterecord')->name('deletepage');

    /* Site products */
    Route::get('settingsproducts', 'Administrator\SiteproductController@index')->name('siteproductlist');
    Route::post('settingsproducts', 'Administrator\SiteproductController@index')->name('siteproductlist');
    Route::get('addeditsiteproduct/{id?}/{status?}', 'Administrator\SiteproductController@addeditsiteproduct')->name('addeditsiteproduct');
    Route::post('addeditproductrecord/{id?}/{page?}', 'Administrator\SiteproductController@addeditproductrecord');
    Route::get('editsiteproductstatus/{id?}/{page?}', 'Administrator\SiteproductController@editstatus');
    Route::get('deletesiteproduct/{id?}/{page?}', 'Administrator\SiteproductController@deletesiteproduct')->name('deletesiteproduct');
    Route::get('exportproduct', 'Administrator\SiteproductController@exportproduct')->name('exportproduct');
    Route::get('getsiteproduct/{subCategory?}', 'Administrator\SiteproductController@getsiteproduct');
    Route::get('getsiteproductweight/{productId?}', 'Administrator\SiteproductController@getsiteproductweight');

    /* Ewallet */
    Route::get('ewallet', 'Administrator\EwalletController@index')->name('ewallet');
    Route::post('ewallet', 'Administrator\EwalletController@index')->name('ewallet');
    Route::get('ewallet/add/{id?}/{page?}', 'Administrator\EwalletController@addData');
    Route::get('searchuser/{searchVal?}', 'Administrator\UsersController@searchuser');
    Route::post('ewallet/save/{page?}', 'Administrator\EwalletController@saveData')->name('saveEwallet');
    Route::get('ewallet/delete/{id?}/{page?}', 'Administrator\EwalletController@deleteData')->name('deleteEwallet');
    Route::post('ewallet/deleteselected', 'Administrator\EwalletController@deleteselected')->name('deleteSelected');
    Route::post('ewallet/updatestatus/{page?}/{id?}', 'Administrator\EwalletController@updatestatus')->name('updatestatus');
    Route::get('ewallet/details/{id?}/{page?}', 'Administrator\EwalletController@getDetails');
    Route::get('ewallet/settings', 'Administrator\EwalletController@getSettings')->name('ewalletSettings');
    Route::post('ewallet/settings/update', 'Administrator\EwalletController@editSettings')->name('updateEwalletSettings');

    /* Shipping Charges */
    Route::get('shippingcharges', 'Administrator\ShippingchargesController@index')->name('shippingcharges');
    Route::post('shippingcharges', 'Administrator\ShippingchargesController@index')->name('shippingcharges');
    Route::post('shippingcharges/add', 'Administrator\ShippingchargesController@addData')->name('addShippingCharge');
    Route::post('shippingcharges/update', 'Administrator\ShippingchargesController@updateData')->name('updateshippingcharge');

    /* Country */
    Route::get('country', 'Administrator\CountryController@index');
    Route::get('country/index', 'Administrator\CountryController@index');
    Route::post('country/index', 'Administrator\CountryController@index');
    Route::get('country/changestatus/{id?}/{page?}/{status?}', 'Administrator\CountryController@changestatus');
    Route::get('country/addedit/{id?}/{page?}', 'Administrator\CountryController@addedit');
    Route::post('country/save/{id?}/{page?}', 'Administrator\CountryController@savedata');
    Route::get('country/getcountryisocode/{countryid?}', 'Administrator\CountryController@getcountryisocode');


    /* State */
    Route::get('state', 'Administrator\StateController@index');
    Route::get('state/index', 'Administrator\StateController@index');
    Route::post('state/index', 'Administrator\StateController@index');
    Route::get('state/changestatus/{id?}/{page?}/{status?}', 'Administrator\StateController@changestatus');
    Route::get('state/addedit/{id?}/{page?}', 'Administrator\StateController@addedit');
    Route::post('state/save/{id?}/{page?}', 'Administrator\StateController@savedata');
    Route::get('state/delete/{id?}/{page?}', 'Administrator\StateController@delete');

    /* City */
    Route::get('city', 'Administrator\CityController@index');
    Route::get('city/index', 'Administrator\CityController@index');
    Route::post('city/index', 'Administrator\CityController@index');
    Route::get('city/changestatus/{id?}/{page?}/{status?}', 'Administrator\CityController@changestatus');
    Route::get('city/addedit/{id?}/{page?}', 'Administrator\CityController@addedit');
    Route::post('city/save/{id?}/{page?}', 'Administrator\CityController@savedata');
    Route::get('city/delete/{id?}/{page?}', 'Administrator\CityController@delete');
    Route::get('city/getstatelist/{countrycode?}', 'Administrator\CityController@getstatelist');
    Route::get('city/importdata', 'Administrator\CityController@importdata');
    Route::post('city/saveimport', 'Administrator\CityController@saveimport');

    /* County */
    Route::get('county', 'Administrator\CountyController@index');
    Route::get('county/{stateid?}', 'Administrator\CountyController@index');
    Route::get('county/index/{stateid?}', 'Administrator\CountyController@index');
    Route::post('county/index/{stateid?}', 'Administrator\CountyController@index');
    Route::get('county/changestatus/{stateid?}/{id?}/{page?}/{status?}', 'Administrator\CountyController@changestatus');
    Route::get('county/addedit/{stateid?}/{id?}/{page?}', 'Administrator\CountyController@addedit');
    Route::post('county/save/{id?}/{page?}', 'Administrator\CountyController@savedata');
    Route::get('county/delete/{stateid?}/{id?}/{page?}', 'Administrator\CountyController@delete');

    /* Location Types */
    Route::get('locationtype', 'Administrator\LocationtypeController@index');
    Route::get('locationtype/index', 'Administrator\LocationtypeController@index');
    Route::post('locationtype/index', 'Administrator\LocationtypeController@index');
    Route::get('locationtype/changestatus/{id?}/{page?}/{status?}', 'Administrator\LocationtypeController@changestatus');
    Route::get('locationtype/addedit/{id?}/{page?}', 'Administrator\LocationtypeController@addedit');
    Route::post('locationtype/save/{id?}/{page?}', 'Administrator\LocationtypeController@savedata');
    Route::get('locationtype/delete/{id?}/{page?}', 'Administrator\LocationtypeController@delete');

    /* Location */
    Route::get('location', 'Administrator\LocationController@index');
    Route::get('location/index', 'Administrator\LocationController@index');
    Route::post('location/index', 'Administrator\LocationController@index');
    Route::get('location/changestatus/{id?}/{page?}/{status?}', 'Administrator\LocationController@changestatus');
    Route::get('location/addedit/{id?}/{page?}', 'Administrator\LocationController@addedit');
    Route::post('location/save/{id?}/{page?}', 'Administrator\LocationController@savedata');
    Route::get('location/delete/{id?}/{page?}', 'Administrator\LocationController@delete');
    Route::get('location/getstatelist/{countryid?}', 'Administrator\LocationController@getstatelist');
    Route::get('location/getcitylist/{stateid?}', 'Administrator\LocationController@getcitylist');
    Route::get('location/getcitylistbycountry/{countryid?}', 'Administrator\LocationController@getcitylistbycountry');

    /* Zone */
    Route::get('zone', 'Administrator\ZoneController@index');
    Route::get('zone/index', 'Administrator\ZoneController@index');
    Route::post('zone/index', 'Administrator\ZoneController@index');
    Route::get('zone/changestatus/{id?}/{page?}/{status?}', 'Administrator\ZoneController@changestatus');
    Route::get('zone/add/{id?}/{page?}', 'Administrator\ZoneController@add');
    Route::get('zone/edit/{id?}/{page?}', 'Administrator\ZoneController@edit');
    Route::post('zone/update/{id?}/{page?}', 'Administrator\ZoneController@update');
    Route::post('zone/addeditzonerecord/{id?}/{page?}', 'Administrator\ZoneController@addeditzonerecord');
    Route::post('zone/save/{id?}/{page?}', 'Administrator\ZoneController@savedata');
    Route::get('zone/delete/{id?}/{page?}', 'Administrator\ZoneController@delete');
    Route::get('zone/getstatelist/{countryid?}', 'Administrator\ZoneController@getstatelist');
    Route::get('zone/getcitylist/{stateid?}', 'Administrator\ZoneController@getcitylist');
    Route::get('zone/getcitylistbycountry/{countryid?}', 'Administrator\ZoneController@getcitylistbycountry');


    /* Currency */
    Route::get('currency', 'Administrator\CurrencyController@index');
    Route::get('currency/index', 'Administrator\CurrencyController@index');
    Route::post('currency/index', 'Administrator\CurrencyController@index');
    Route::get('currency/setdefault/{id?}/{page?}', 'Administrator\CurrencyController@setdefault');
    Route::get('currency/changestatus/{id?}/{page?}/{status?}', 'Administrator\CurrencyController@changestatus');
    Route::get('currency/addedit/{id?}/{page?}', 'Administrator\CurrencyController@addedit');
    Route::get('currency/settings/{id?}/{page?}', 'Administrator\CurrencyController@settings');
    Route::post('currency/save/{id?}/{page?}', 'Administrator\CurrencyController@savedata');
    Route::get('currency/delete/{id?}/{page?}', 'Administrator\CurrencyController@delete');
    Route::post('currency/savesettings/{page?}', 'Administrator\CurrencyController@savesettings');
    Route::post('currency/updateCurrency', 'Administrator\CurrencyController@updatecurrency');
    Route::post('currency/updateCurrencyCronTime', 'Administrator\CurrencyController@updatecurrencycrontime');

    /* Email Template */
    Route::get('emailtemplate', 'Administrator\EmailtemplateController@index');
    Route::get('emailtemplate/index', 'Administrator\EmailtemplateController@index');
    Route::post('emailtemplate/index', 'Administrator\EmailtemplateController@index');
    Route::get('emailtemplate/addedit/{id?}/{page?}', 'Administrator\EmailtemplateController@addedit');
    Route::post('emailtemplate/save/{id?}/{page?}', 'Administrator\EmailtemplateController@savedata');
    Route::get('emailtemplate/clear', 'Administrator\EmailtemplateController@cleardata');
    Route::post('emailtemplate/generatekeyoptions', 'Administrator\EmailtemplateController@generateKeyOptions');

    /* User */
    Route::get('users', 'Administrator\UsersController@index');
    Route::get('users/index', 'Administrator\UsersController@index');
    Route::post('users/index', 'Administrator\UsersController@index');
    Route::get('users/addedit/{id?}/{page?}', 'Administrator\UsersController@addedit');
    Route::post('users/save/{id?}/{page?}', 'Administrator\UsersController@savedata');
    Route::get('users/changestatus/{id?}/{page?}/{status?}', 'Administrator\UsersController@changestatus');
    Route::get('users/delete/{id?}/{page?}', 'Administrator\UsersController@delete');
    Route::get('users/showall', 'Administrator\UsersController@showall');
    Route::get('users/getstatelist/{countryid?}', 'Administrator\UsersController@getstatelist');
    Route::get('users/getcitylist/{stateid?}', 'Administrator\UsersController@getcitylist');
    Route::get('users/getcitylistbycountry/{countryid?}', 'Administrator\UsersController@getcitylistbycountry');
    Route::get('users/addressdelete/{userid?}/{id?}/{page?}', 'Administrator\UsersController@deleteaddressbook');
    Route::post('users/saveaddressbook/{id?}/{page?}', 'Administrator\UsersController@saveaddressbook');
    Route::get('users/addressaddedit/{userid?}/{id?}/{page?}', 'Administrator\UsersController@addressaddedit');
    Route::post('users/saveaddress/{userid?}/{id?}/{page?}', 'Administrator\UsersController@saveaddressdata');
    Route::post('users/exportall/{page?}', 'Administrator\UsersController@exportall');
    Route::post('users/exportselected/{page?}', 'Administrator\UsersController@exportselected');
    Route::get('users/changepassword/{id?}/{page?}', 'Administrator\UsersController@changepassword');
    Route::post('users/changepassword/{id?}/{page?}', 'Administrator\UsersController@changepassword');
    Route::get('users/sendnotification/{id?}/{page?}', 'Administrator\UsersController@sendnotification');
    Route::post('users/sendnotification/{id?}/{page?}', 'Administrator\UsersController@sendnotification');
    Route::get('users/notificationhistory/{id?}/{userpage?}/{page?}', 'Administrator\UsersController@notificationhistory');
    Route::post('users/notificationhistory/{id?}/{userpage?}/{page?}', 'Administrator\UsersController@notificationhistory');
    Route::get('users/viewnotification/{id?}/{page?}', 'Administrator\UsersController@viewnotification');
    Route::get('users/getusershipmentsettings/{id?}/{page?}', 'Administrator\UsersController@getusershipmentsettings');
    Route::get('users/documentdelete/{id?}', 'Administrator\UsersController@documentdelete');
    Route::get('users/documentdelete/{id?}', 'Administrator\UsersController@documentdelete');

    Route::get('users/verifyuser/{id?}/{page?}', 'Administrator\UsersController@verifyuser');
    Route::post('users/resendActivationMail', 'Administrator\UsersController@resendActivationMail');

    /* Admin profile */
    Route::get('/profile', 'Administrator\ProfileController@getProfileView');

    /* General Settings */

    Route::get('/generalsettings', 'Administrator\SettingsController@index');
    Route::get('generalsettings/index', 'Administrator\SettingsController@index');
    Route::post('generalsettings/save', 'Administrator\SettingsController@savedata');

    /* Social Media Settings */

    Route::get('/socialmedia', 'Administrator\SocialMediaController@index');
    Route::get('socialmedia/index', 'Administrator\SocialMediaController@index');
    Route::post('socialmedia/save', 'Administrator\SocialMediaController@savedata');


    /* SMS Template */
    Route::get('smstemplate', 'Administrator\SmstemplateController@index');
    Route::get('smstemplate/index', 'Administrator\SmstemplateController@index');
    Route::post('smstemplate/index', 'Administrator\SmstemplateController@index');
    Route::get('smstemplate/addedit/{id?}/{page?}', 'Administrator\SmstemplateController@addedit');
    Route::post('smstemplate/save/{id?}/{page?}', 'Administrator\SmstemplateController@savedata');

    /* Shipment Settings */

    Route::get('/shippingsettings', 'Administrator\ShippingsettingsController@index');
    Route::get('shippingsettings/index', 'Administrator\ShippingsettingsController@index');
    Route::post('shippingsettings/save', 'Administrator\ShippingsettingsController@savedata');
    Route::get('/shippingmethods', 'Administrator\ShippingsettingsController@shippingmethods');
    Route::get('/shippingmethods', 'Administrator\ShippingsettingsController@shippingmethods');
    Route::post('/shippingmethods', 'Administrator\ShippingsettingsController@shippingmethods');
    Route::get('shippingsettings/shippingmethods', 'Administrator\ShippingsettingsController@shippingmethods');
    Route::get('shippingsettings/changestatus/{id?}/{page?}/{status?}', 'Administrator\ShippingsettingsController@changestatus');
    Route::get('shippingsettings/addmethod/{id?}/{page?}', 'Administrator\ShippingsettingsController@addmethod');
    Route::get('shippingsettings/editmethod/{id?}/{page?}', 'Administrator\ShippingsettingsController@editmethod');
    Route::post('shippingsettings/savemethod/{id?}/{page?}', 'Administrator\ShippingsettingsController@savemethod');
    Route::get('methodDelete/{id?}/{page?}', 'Administrator\ShippingsettingsController@methodDelete');
    Route::get('shippingsettings/editcomment/{id?}/{page?}', 'Administrator\ShippingsettingsController@editcomment');
    Route::post('shippingsettings/savecomment/{id?}/{page?}', 'Administrator\ShippingsettingsController@savecomment');

    /* Service Type */
    Route::get('servicetype', 'Administrator\ServicetypeController@index');
    Route::get('servicetype/index', 'Administrator\ServicetypeController@index');
    Route::post('servicetype/index', 'Administrator\ServicetypeController@index');
    Route::get('servicetype/changestatus/{id?}/{page?}/{status?}', 'Administrator\ServicetypeController@changestatus');
    Route::get('servicetype/addedit/{id?}/{page?}', 'Administrator\ServicetypeController@addedit');
    Route::post('servicetype/save/{id?}/{page?}', 'Administrator\ServicetypeController@savedata');
    Route::get('servicetype/delete/{id?}/{page?}', 'Administrator\ServicetypeController@deletedata');

    /* Update Type */
    Route::get('updatetype', 'Administrator\UpdatetypeController@index');
    Route::get('updatetype/index', 'Administrator\UpdatetypeController@index');
    Route::post('updatetype/index', 'Administrator\UpdatetypeController@index');
    Route::get('updatetype/changestatus/{id?}/{page?}/{status?}', 'Administrator\UpdatetypeController@changestatus');
    Route::get('updatetype/addedit/{id?}/{page?}', 'Administrator\UpdatetypeController@addedit');
    Route::post('updatetype/save/{id?}/{page?}', 'Administrator\UpdatetypeController@savedata');
    Route::get('updatetype/delete/{id?}/{page?}', 'Administrator\UpdatetypeController@deletedata');

    /* Destination Port */
    Route::get('destinationport', 'Administrator\DestinationportController@index');
    Route::get('destinationport/index', 'Administrator\DestinationportController@index');
    Route::post('destinationport/index', 'Administrator\DestinationportController@index');
    Route::get('destinationport/changestatus/{id?}/{page?}/{status?}', 'Administrator\DestinationportController@changestatus');
    Route::get('destinationport/addedit/{id?}/{page?}', 'Administrator\DestinationportController@addedit');
    Route::post('destinationport/save/{id?}/{page?}', 'Administrator\DestinationportController@savedata');
    Route::get('destinationport/delete/{id?}/{page?}', 'Administrator\DestinationportController@deletedata');

    /* Warehouse */
    Route::get('warehouse', 'Administrator\WarehouseController@index');
    Route::get('warehouse/index', 'Administrator\WarehouseController@index');
    Route::post('warehouse/index', 'Administrator\WarehouseController@index');
    Route::get('warehouse/changestatus/{id?}/{page?}/{status?}', 'Administrator\WarehouseController@changestatus');
    Route::get('warehouse/addwarehouse/{id?}/{page?}', 'Administrator\WarehouseController@addwarehouse');
    Route::get('warehouse/editwarehouse/{id?}/{page?}', 'Administrator\WarehouseController@editwarehouse');
    Route::post('warehouse/save/{id?}/{page?}', 'Administrator\WarehouseController@savedata');
    Route::get('warehouse/delete/{id?}/{page?}', 'Administrator\WarehouseController@deletedata');
    Route::get('warehouse/getstatelist/{countrycode?}', 'Administrator\WarehouseController@getstatelist');
    Route::get('warehouse/getcitylist/{statecode?}', 'Administrator\WarehouseController@getcitylist');

    /* Testimonials */
    Route::get('testimonial', 'Administrator\TestimonialController@index');
    Route::get('testimonial/index', 'Administrator\TestimonialController@index');
    Route::post('testimonial/index', 'Administrator\TestimonialController@index');
    Route::get('testimonial/changestatus/{id?}/{page?}/{status?}', 'Administrator\TestimonialController@changestatus');
    Route::get('testimonial/addedit/{id?}/{page?}', 'Administrator\TestimonialController@addedit');
    Route::post('testimonial/save/{id?}/{page?}', 'Administrator\TestimonialController@savedata');
    Route::get('testimonial/delete/{id?}/{page?}', 'Administrator\TestimonialController@deletedata');


    /* Warehouse Messages */
    Route::get('warehousemessage', 'Administrator\WarehousemessageController@index');
    Route::get('warehousemessage/index', 'Administrator\WarehousemessageController@index');
    Route::post('warehousemessage/index', 'Administrator\WarehousemessageController@index');
    Route::get('warehousemessage/changestatus/{id?}/{page?}/{status?}', 'Administrator\WarehousemessageController@changestatus');
    Route::get('warehousemessage/addedit/{id?}/{page?}', 'Administrator\WarehousemessageController@addedit');
    Route::post('warehousemessage/save/{id?}/{page?}', 'Administrator\WarehousemessageController@savedata');
    Route::get('warehousemessage/delete/{id?}/{page?}', 'Administrator\WarehousemessageController@deletedata');

    /* Warehouse Messages */
    Route::get('warehousemessage', 'Administrator\WarehousemessageController@index');
    Route::get('warehousemessage/index', 'Administrator\WarehousemessageController@index');
    Route::post('warehousemessage/index', 'Administrator\WarehousemessageController@index');
    Route::get('warehousemessage/reply/{id?}/{page?}', 'Administrator\WarehousemessageController@reply');
    Route::post('warehousemessage/save/{id?}/{page?}', 'Administrator\WarehousemessageController@savedata');

    /* Delivery Companies */
    Route::get('deliverycompany', 'Administrator\DeliverycompanyController@index');
    Route::get('dispatchcompany/index', 'Administrator\DispatchcompanyController@index');
    Route::post('deliverycompany/index', 'Administrator\DeliverycompanyController@index');
    Route::get('deliverycompany/changestatus/{id?}/{page?}/{status?}', 'Administrator\DeliverycompanyController@changestatus');
    Route::get('deliverycompany/addedit/{id?}/{page?}', 'Administrator\DeliverycompanyController@addedit');
    Route::post('deliverycompany/save/{id?}/{page?}', 'Administrator\DeliverycompanyController@savedata');

    /* Dispatch Companies */
    Route::get('dispatchcompany', 'Administrator\DispatchcompanyController@index');
    Route::get('dispatchcompany/index', 'Administrator\DispatchcompanyController@index');
    Route::post('dispatchcompany/index', 'Administrator\DispatchcompanyController@index');
    Route::get('dispatchcompany/changestatus/{id?}/{page?}/{status?}', 'Administrator\DispatchcompanyController@changestatus');
    Route::get('dispatchcompany/addedit/{id?}/{page?}', 'Administrator\DispatchcompanyController@addedit');
    Route::post('dispatchcompany/save/{id?}/{page?}', 'Administrator\DispatchcompanyController@savedata');

    /* Shipment Locations */
    Route::get('warehouselocation/rows', 'Administrator\WarehouselocationController@rows');
    Route::post('warehouselocation/rows', 'Administrator\WarehouselocationController@rows');
    Route::get('warehouselocation/zones', 'Administrator\WarehouselocationController@zones');
    Route::post('warehouselocation/zones', 'Administrator\WarehouselocationController@zones');
    Route::get('warehouselocation/changestatus/{id?}/{page?}/{status?}', 'Administrator\WarehouselocationController@changestatus');
    Route::get('warehouselocation/rows/addedit/{id?}/{page?}', 'Administrator\WarehouselocationController@rowsaddedit');
    Route::get('warehouselocation/zones/addedit/{id?}/{page?}', 'Administrator\WarehouselocationController@zonesaddedit');
    Route::post('warehouselocation/save/{id?}/{page?}', 'Administrator\WarehouselocationController@savedata');
    Route::get('warehouselocation/importdata', 'Administrator\WarehouselocationController@importdata');
    Route::post('warehouselocation/saveimport', 'Administrator\WarehouselocationController@saveimport');
    Route::get('warehouselocation/importzonedata', 'Administrator\WarehouselocationController@importzonedata');
    Route::post('warehouselocation/saveimportzone', 'Administrator\WarehouselocationController@saveimportzone');


    /* Driver Management */
    Route::get('driver', 'Administrator\DriverController@index');
    Route::get('driver/index', 'Administrator\DriverController@index');
    Route::post('driver/index', 'Administrator\DriverController@index');
    Route::get('driver/addedit/{id?}/{page?}', 'Administrator\DriverController@addedit');
    Route::post('driver/save/{id?}/{page?}', 'Administrator\DriverController@savedata');
    Route::get('driver/delete/{id?}/{page?}', 'Administrator\DriverController@deletedata');

    /* Shipments */
    Route::get('shipments', 'Administrator\ShipmentController@index');
    Route::get('shipments/index', 'Administrator\ShipmentController@index');
    Route::post('shipments/index', 'Administrator\ShipmentController@index');
    Route::get('shipments/addedit/{id?}/{page?}', 'Administrator\ShipmentController@addedit');
    Route::post('shipments/save/{id?}/{page?}', 'Administrator\ShipmentController@savedata');
    Route::get('shipments/showall', 'Administrator\ShipmentController@showall');
    Route::get('shipments/changestatus/{id?}/{page?}/{status?}', 'Administrator\ShipmentController@changestatus');
    Route::post('shipments/changestatus/{id?}/{page?}/{status?}', 'Administrator\ShipmentController@changestatus');
    Route::get('shipments/delete/{id?}/{page?}', 'Administrator\ShipmentController@delete');
    Route::get('shipments/verifyuser/{unit?}', 'Administrator\ShipmentController@verifyuser');
    Route::get('shipments/getwarehousezonelist/{rowid?}', 'Administrator\ShipmentController@getwarehousezonelist');
    Route::post('shipments/deleteall', 'Administrator\ShipmentController@deleteall');
    Route::get('shipments/bulkchangestatus/{id?}/{page?}', 'Administrator\ShipmentController@bulkchangestatus');
    Route::post('shipments/bulkchangestatus/{id?}/{page?}', 'Administrator\ShipmentController@bulkchangestatus');
    Route::get('shipments/schedulenotification/{id?}/{page?}', 'Administrator\ShipmentController@schedulenotification');
    Route::post('shipments/schedulenotification/{id?}/{page?}', 'Administrator\ShipmentController@schedulenotification');
    Route::get('shipments/changeaddress/{id?}/{userid?}', 'Administrator\ShipmentController@changeaddress');
    Route::post('shipments/changeaddress/{id?}/{userid?}', 'Administrator\ShipmentController@changeaddress');
    Route::get('shipments/getaddressbookdetails/{id?}', 'Administrator\ShipmentController@getaddressbookdetails');
    Route::get('shipments/addeditlocation/{id?}/{shipmentid?}', 'Administrator\ShipmentController@addeditlocation');
    Route::post('shipments/addeditlocation/{id?}/{shipmentid?}', 'Administrator\ShipmentController@addeditlocation');
    Route::get('shipments/deletelocation/{id?}/{shipmentid?}', 'Administrator\ShipmentController@deletelocation');
    Route::get('shipments/createorder/{id?}/{page?}', 'Administrator\ShipmentController@createorder')->name('shipmentcreateorder');
    Route::post('shipments/createorder/{id?}/{page?}', 'Administrator\ShipmentController@createorder')->name('shipmentcreateorder');
    Route::get('shipments/addeditdelivery/{shipmentid?}/{id?}', 'Administrator\ShipmentController@addeditdelivery');
    Route::post('shipments/addeditdelivery/{shipmentid?}/{id?}', 'Administrator\ShipmentController@addeditdelivery');
    Route::get('shipments/deletedelivery/{id?}', 'Administrator\ShipmentController@deletedelivery');
    Route::post('shipments/adddeliveryitem', 'Administrator\ShipmentController@addshipmentpackage');
    Route::get('shipments/deletedeliveryitem/{id?}', 'Administrator\ShipmentController@deletedeliveryitem');
    Route::post('shipments/editdeliveryitem', 'Administrator\ShipmentController@editshipmentpackage');
    Route::post('shipments/uploadimage', 'Administrator\ShipmentController@uploadimage');
    Route::post('shipments/startpackaging/{id?}/{page?}', 'Administrator\ShipmentController@startpackaging')->name('shipmentstartpackaging');
    Route::get('shipments/validatepackage/{id?}/{page?}', 'Administrator\ShipmentController@validatepackage');
    Route::post('shipments/addpackage/{id?}/{page?}', 'Administrator\ShipmentController@addpackage')->name('shipmentnewpackaging');
    Route::post('shipments/editpackage/{id?}/{page?}', 'Administrator\ShipmentController@editpackage')->name('shipmenteditpackaging');
    Route::post('shipments/addpackagedetails/{id?}/{page?}', 'Administrator\ShipmentController@addpackagedetails')->name('shipmentpackagedetails');
    Route::get('shipments/printmanifest/{id?}/{page?}', 'Administrator\ShipmentController@printmanifest')->name('shipmentmanifest');
    Route::post('shipments/addshipmentcharge/{id?}/{deliveryid}', 'Administrator\ShipmentController@addshipmentcharge');
    Route::get('shipments/getothercharges/{type?}/{id?}/{deliveryid?}', 'Administrator\ShipmentController@getothershipmentcharges');
    Route::get('shipments/notifycustomer/{messageId?}/{shipmentid?}', 'Administrator\ShipmentController@notifycustomer');
    Route::post('shipments/savenotification/{messageId?}/{shipmentid?}', 'Administrator\ShipmentController@savenotification');
    Route::get('shipments/deleteothercharge/{id?}', 'Administrator\ShipmentController@deleteothercharge');
    Route::get('shipments/redstarlabel/{id?}/{page?}', 'Administrator\ShipmentController@redstarlabel')->name('shipmentmanifest');
    Route::get('shipments/nationsdelivery/{id?}/{page?}', 'Administrator\ShipmentController@nationsdelivery')->name('shipmentmanifest');
    Route::get('shipments/dhlci/{id?}/{page?}', 'Administrator\ShipmentController@dhlci')->name('shipmentmanifest');
    Route::get('shipments/dhlcipost/{id?}/{page?}', 'Administrator\ShipmentController@dhlcipost')->name('shipmentmanifest');
    Route::post('shipments/dhlcipost/', 'Administrator\ShipmentController@dhlcipost')->name('shipmentmanifest');
    Route::get('shipments/dhl/{id?}/{page?}', 'Administrator\ShipmentController@dhl')->name('shipmentmanifest');
    Route::post('shipments/dhlpost/', 'Administrator\ShipmentController@dhlpost')->name('shipmentmanifest');
    Route::get('/shipments/getsnapshots/{shipmentId?}/{deliveryId?}/{packageId?}', 'Administrator\ShipmentController@getsnapshots');

    Route::post('shipments/addcomment/{shipmentid?}', 'Administrator\ShipmentController@addcomment');
    Route::get('shipments/sendcomment/{id?}/{shipmentid?}', 'Administrator\ShipmentController@sendcomment');
    Route::post('shipments/saveandnotify/{shipmentid?}', 'Administrator\ShipmentController@saveandnotify');
    Route::get('shipments/updatestoragecharge/{id?}', 'Administrator\ShipmentController@updatestoragecharge');
    Route::post('shipments/gettotalcharge/{id?}', 'Administrator\ShipmentController@gettotalshipmentcharge');
    Route::post('shipments/updateshipment/{id?}', 'Administrator\ShipmentController@updateshipment');
    Route::get('shipments/gettotalcharge/{id?}', 'Administrator\ShipmentController@gettotalshipmentcharge');
    Route::get('shipments/getshippinglabelfields/{id?}/{page?}', 'Administrator\ShipmentController@getshippinglabelfields')->name('shipmentshippinglabelfields');
    Route::post('shipments/printshippinglabel/{id?}/{page?}', 'Administrator\ShipmentController@printshippinglabel')->name('shipmentshippinglabel');
    Route::post('shipments/updatedelivery/{shipmentid?}/{deliveryid?}', 'Administrator\ShipmentController@updatedelivery');
    Route::post('shipments/updatepaymentstatus/{shipmentid?}/{shipmentType?}', 'Administrator\ShipmentController@updatepaymentstatus');
    Route::get('shipments/printlocationlabel/{id?}/{page?}', 'Administrator\ShipmentController@printlocationlabel');
    Route::get('shipments/shownotes/{id?}/{shipmentid?}', 'Administrator\ShipmentController@shownotes');
    Route::get('shipments/printdeliverylabel/{id?}/{deliveryid?}/{delivenum?}', 'Administrator\ShipmentController@printdeliverylabel');
    Route::get('shipments/printdropslip/{id?}/{page?}', 'Administrator\ShipmentController@printdropslip');
    Route::post('shipments/exportselected/{page?}', 'Administrator\ShipmentController@exportselected')->name('shipmentexportselected');
    Route::post('shipments/exportall/{page?}', 'Administrator\ShipmentController@exportall')->name('shipmentexportall');
    Route::get('shipments/getwarehouselocations/{id?}/{page?}', 'Administrator\ShipmentController@getwarehouselocations');
    Route::get('shipments/getpackagingdata/{id?}/{page?}', 'Administrator\ShipmentController@getpackagingdata');
    Route::get('shipments/getaddressdetails/{id?}', 'Administrator\ShipmentController@getaddressdetails');
    Route::get('shipments/getshippingmethods/{id?}/{page?}', 'Administrator\ShipmentController@getshippingmethods');
    Route::get('shipments/getpackagedetails/{id?}', 'Administrator\ShipmentController@getpackagedetails');
    Route::get('shipments/getshipmentdetails/{id?}/{page?}', 'Administrator\ShipmentController@getshipmentdetails');
    Route::get('shipments/getsubcategorylist/{id?}', 'Administrator\ShipmentController@getsubcategorylist');
    Route::get('shipments/getproductlist/{id?}', 'Administrator\ShipmentController@getproductlist');
    Route::get('shipments/printinvoice/{id?}/{invoiceid?}', 'Administrator\ShipmentController@printinvoice');
    Route::post('shipments/methodwiseshippingcharge', 'Administrator\ShipmentController@methodwiseshippingcharge');
    Route::get('shipments/deliveryshippingcost/{shipmentId}/{deliveryId}', 'Administrator\ShipmentController@deliveryshippingcost');
    Route::post('shipments/updatestage/{shipmentId}/{deliveryId}', 'Administrator\ShipmentController@updatestage')->name('updateshipmentstages');
    Route::post('shipments/updatetracking/{shipmentPackage?}', 'Administrator\ShipmentController@updatetracking');
    Route::get('shipments/getuserwarehouse/{userUnit}', 'Administrator\ShipmentController@getuserwarehouse');
    Route::post('shipments/createextrainvoice/{shipmentId}', 'Administrator\ShipmentController@createextrainvoice');
    Route::get('shipments/wronginvoice/download/{shipmentId}', 'Administrator\ShipmentController@wronginvoicedownload');
    Route::get('shipments/updatepackedstatus/{shipmentId}', 'Administrator\ShipmentController@updatepackedstatus');
    Route::get('shipments/getstorelist/{warehouseId}', 'Administrator\ShipmentController@getstorelist');
    Route::get('shipments/getcategorylist/{storeId}', 'Administrator\ShipmentController@getcategorylist');
    Route::get('shipments/assignDispatchCompany/{id?}/{page?}', 'Administrator\ShipmentController@assignDispatchCompany');
    Route::post('shipments/assigndispatchcompany/{id?}/{page?}/{method?}', 'Administrator\ShipmentController@assignDispatchCompany');


    /* Orders */
    Route::get('orders', 'Administrator\OrderController@index');
    Route::post('orders', 'Administrator\OrderController@index')->name('orders');
    Route::get('orders/changestatus/{id?}/{page?}', 'Administrator\OrderController@changestatus');
    Route::post('orders/changestatus/{id?}/{page?}', 'Administrator\OrderController@changestatus')->name('updateorderstatus');
    Route::get('orders/bulkchangestatus/{id?}/{page?}', 'Administrator\OrderController@bulkchangestatus')->name('updatebulkorderstatus');
    Route::post('orders/bulkchangestatus/{id?}/{page?}', 'Administrator\OrderController@bulkchangestatus')->name('updatebulkorderstatus');
    Route::get('orders/delete/{id?}/{page?}', 'Administrator\OrderController@delete')->name('deleteorder');
    Route::post('orders/exportselected/{page?}', 'Administrator\OrderController@exportselected')->name('orderexportselected');
    Route::post('orders/exportall/{page?}', 'Administrator\OrderController@exportall')->name('orderexportall');
    Route::get('orders/showaddress/{userId?}/{page?}', 'Administrator\OrderController@showaddressonmap');
    Route::get('orders/addedit/{orderId?}/{page?}', 'Administrator\OrderController@addedit')->name('orderdetails');
    Route::get('orders/showall', 'Administrator\OrderController@showall')->name('showallorderdata');
    Route::post('orders/savecustomernote/{orderId?}/{page?}', 'Administrator\OrderController@savecustomernote')->name('saveordercomments');
    Route::get('orders/view/{orderId?}/{page?}', 'Administrator\OrderController@vieworder');
    Route::get('/orders/getsnapshots/{shipmentId?}/{deliveryId?}/{packageId?}', 'Administrator\OrderController@getsnapshots');





    /* Procurement Fees */
    Route::get('procurementfees', 'Administrator\ProcurementfeesController@index');
    Route::get('procurementfees/index', 'Administrator\ProcurementfeesController@index');
    Route::post('procurementfees/index', 'Administrator\ProcurementfeesController@index');
    Route::get('procurementfees/addedit/{id?}/{page?}', 'Administrator\ProcurementfeesController@addedit');
    Route::post('procurementfees/save/{id?}/{page?}', 'Administrator\ProcurementfeesController@savedata');
    Route::get('procurementfees/delete/{id?}/{page?}', 'Administrator\ProcurementfeesController@deletedata');
    Route::post('procurementDelete', 'Administrator\ProcurementfeesController@procurementDelete');
    Route::post('procurementfees/deleteall', 'Administrator\ProcurementfeesController@deleteall');

    /* Store Management */
    Route::get('managestores', 'Administrator\StoresController@index');
    Route::get('managestores/index', 'Administrator\StoresController@index');
    Route::post('managestores/index', 'Administrator\StoresController@index');
    Route::get('managestores/addedit/{id?}/{page?}', 'Administrator\StoresController@addedit');
    Route::post('managestores/save/{id?}/{page?}', 'Administrator\StoresController@savedata');
    Route::get('managestores/delete/{id?}/{page?}', 'Administrator\StoresController@deletedata');
    Route::get('managestores/getcategory', 'Administrator\StoresController@getcategory');

    /* Procurement Services */
    Route::get('procurement', 'Administrator\ProcurementController@index');
    Route::get('procurement/index', 'Administrator\ProcurementController@index');
    Route::post('procurement/index', 'Administrator\ProcurementController@index');
    Route::get('procurement/changestatus/{id?}/{page?}', 'Administrator\ProcurementController@changestatus');
    Route::post('procurement/changestatus/{id?}/{page?}/{status?}', 'Administrator\ProcurementController@changestatus');
    Route::get('procurement/delete/{id?}/{page?}', 'Administrator\ProcurementController@delete');
    Route::get('procurement/getstatelist/{countrycode?}', 'Administrator\ProcurementController@getstatelist');
    Route::get('procurement/getcitylist/{statecode?}', 'Administrator\ProcurementController@getcitylist');
    Route::get('procurement/getcitylistbycountry/{countryid?}', 'Administrator\ProcurementController@getcitylistbycountry');
    Route::post('procurement/exportall/{page?}', 'Administrator\ProcurementController@exportall');
    Route::post('procurement/exportselected/{page?}', 'Administrator\ProcurementController@exportselected');
    Route::get('procurement/showall', 'Administrator\ProcurementController@showall');
    Route::get('procurement/addedit/{id?}/{page?}', 'Administrator\ProcurementController@addedit');
    Route::get('procurement/changeaddress/{id?}/{userid?}', 'Administrator\ProcurementController@changeaddress');
    Route::post('procurement/changeaddress/{id?}/{userid?}', 'Administrator\ProcurementController@changeaddress');
    Route::get('procurement/getaddressbookdetails/{id?}', 'Administrator\ProcurementController@getaddressbookdetails');
    Route::get('procurement/getaddressdetails/{id?}', 'Administrator\ProcurementController@getaddressdetails');
    Route::get('procurement/notifycustomer/{messageId?}/{shipmentid?}', 'Administrator\ProcurementController@notifycustomer');
    Route::post('procurement/notifycustomer/{messageId?}/{shipmentid?}', 'Administrator\ProcurementController@notifycustomer');
    Route::post('procurement/addcomment/{shipmentid?}', 'Administrator\ProcurementController@addcomment');
    Route::get('procurement/sendcomment/{id?}/{shipmentid?}', 'Administrator\ProcurementController@sendcomment');
    Route::post('procurement/saveandnotify/{shipmentid?}', 'Administrator\ProcurementController@saveandnotify');
    Route::post('procurement/adddeliveryitem', 'Administrator\ProcurementController@addprocurementitem');
    Route::post('procurement/editdeliveryitem', 'Administrator\ProcurementController@editprocurementitem');
    Route::get('procurement/deletedeliveryitem/{id?}', 'Administrator\ProcurementController@deleteprocurementitem');
    Route::get('procurement/receivepackage/{id?}/{page?}', 'Administrator\ProcurementController@receivepackage');
    Route::post('procurement/receivepackage/{id?}/{page?}', 'Administrator\ProcurementController@receivepackage');
    Route::get('procurement/getsubcategorylist/{id?}', 'Administrator\ProcurementController@getsubcategorylist');
    Route::get('procurement/getproductlist/{id?}', 'Administrator\ProcurementController@getproductlist');
    Route::get('procurement/getpackagedetails/{id?}', 'Administrator\ProcurementController@getpackagedetails');
    Route::get('procurement/viewtrackingdetails/{id?}/{page?}', 'Administrator\ProcurementController@viewtrackingdetails');
    Route::get('procurement/getprocurementdetails/{id?}/{page?}', 'Administrator\ProcurementController@getprocurementdetails');
    Route::post('procurement/updateprocurementitemstatus', 'Administrator\ProcurementController@updateprocurementitemstatus');
    Route::get('procurement/getprocurementcostdetails/{id?}', 'Administrator\ProcurementController@getprocurementcostdetails');
    Route::post('procurement/procurementweightdetails/{id?}', 'Administrator\ProcurementController@procurementweightdetails');
    Route::post('procurement/saveweightdetails/{id?}', 'Administrator\ProcurementController@saveweightdetails');
    Route::get('procurement/paymentdetails/{id?}', 'Administrator\ProcurementController@paymentdetails');
    Route::get('procurement/sendcustomerinvoice/{id?}', 'Administrator\ProcurementController@sendcustomerinvoice');
    Route::post('procurement/createshipment/{id?}', 'Administrator\ProcurementController@createshipment');
    Route::post('procurement/createinvoice/{id?}', 'Administrator\ProcurementController@createinvoice');
    Route::get('procurement/printinvoice/{id?}/{invoiceid?}', 'Administrator\ProcurementController@printinvoice');
    Route::get('procurement/getcreateshipmentoptions/{itemid?}/{id?}', 'Administrator\ProcurementController@getcreateshipmentoptions');
    Route::get('procurement/getunpaidshipmentlist/{userid?}', 'Administrator\ProcurementController@getunpaidshipmentlist');
    Route::get('procurement/updatepaymentstatus/{id}/{status}', 'Administrator\ProcurementController@updatepaymentstatus');
    Route::get('procurement/createprepaidshipment/{id?}', 'Administrator\ProcurementController@createprepaidshipment');
    Route::get('procurement/checkprocurementstatus/{id?}', 'Administrator\ProcurementController@checkprocurementstatus');
    Route::get('procurement/updateprocurementpayment/{id?}/{status?}', 'Administrator\ProcurementController@updateprocurementpayment');
    Route::post('procurement/updatetracking/{procurementItem?}', 'Administrator\ProcurementController@updatetracking');

    /* Schedule Pick up */
    Route::get('schedulepickup', 'Administrator\SchedulepickupController@index');
    Route::get('schedulepickup/index', 'Administrator\SchedulepickupController@index');
    Route::post('schedulepickup/index', 'Administrator\SchedulepickupController@index');
    Route::get('schedulepickup/changestatus/{id?}/{page?}/{status?}', 'Administrator\SchedulepickupController@changestatus');
    Route::post('schedulepickup/changestatus/{id?}/{page?}/{status?}', 'Administrator\SchedulepickupController@changestatus');
    Route::get('schedulepickup/delete/{id?}/{page?}', 'Administrator\SchedulepickupController@delete');
    Route::get('schedulepickup/sendnotification/{id?}/{page?}', 'Administrator\SchedulepickupController@sendnotification');
    Route::post('schedulepickup/sendnotification/{id?}/{page?}', 'Administrator\SchedulepickupController@sendnotification');
    Route::get('schedulepickup/getstatelist/{countrycode?}', 'Administrator\SchedulepickupController@getstatelist');
    Route::get('schedulepickup/getcitylist/{statecode?}', 'Administrator\SchedulepickupController@getcitylist');
    Route::get('schedulepickup/scheduledetail/{id?}/{page?}', 'Administrator\SchedulepickupController@scheduledetail');
    Route::get('schedulepickup/getpickupdetails/{scheduleid}', 'Administrator\SchedulepickupController@getpickupdetails');



    /* Tax Settings OR, Tax System */
    Route::resource('taxsettings', 'Administrator\TaxsettingsController');
    Route::get('taxsettings/changestatus/{id}/{status}/{page}', [
        'as' => 'changeStatus', 'uses' => 'Administrator\TaxsettingsController@changeStatus']);
    Route::get('taxsettings/delete/{id}/{page}', [
        'as' => 'delete', 'uses' => 'Administrator\TaxsettingsController@delete']);
    Route::post('taxsettings/exportall/{page}', [
        'as' => 'exportall', 'uses' => 'Administrator\TaxsettingsController@exportall']);
    Route::post('taxsettings/store/', [
        'as' => 'store', 'uses' => 'Administrator\TaxsettingsController@store']);

    /* Tax Rate */
    Route::get('taxrate/{id?}', 'Administrator\TaxrateController@index');
    Route::get('taxrate/{id?}/index', 'Administrator\TaxrateController@index');
    Route::post('taxrate/{id?}/index', 'Administrator\TaxrateController@index');
    Route::get('taxrate/addedit/{id?}/{taxId?}/{page?}', 'Administrator\TaxrateController@addedit');
    Route::post('taxrate/savedata/{id?}/{taxId?}/{page?}', 'Administrator\TaxrateController@savedata');
    Route::get('taxrate/delete/{id?}/{taxId?}/{page?}', 'Administrator\TaxrateController@delete');

    /* Auto Make */
    Route::get('automake', 'Administrator\AutomakeController@index');
    Route::get('automake/index', 'Administrator\AutomakeController@index');
    Route::post('automake/index', 'Administrator\AutomakeController@index');
    Route::get('automake/addedit/{id?}/{page?}', 'Administrator\AutomakeController@addedit');
    Route::post('automake/savedata/{id?}/{page?}', 'Administrator\AutomakeController@savedata');
    Route::get('automake/delete/{id?}/{page?}', 'Administrator\AutomakeController@delete');
    Route::get('automake/getmakelist/{type?}', 'Administrator\AutomakeController@getmakelist');

    /* Auto Model */
    Route::get('automodel', 'Administrator\AutomodelController@index');
    Route::get('automodel/index', 'Administrator\AutomodelController@index');
    Route::post('automodel/index', 'Administrator\AutomodelController@index');
    Route::get('automodel/addedit/{id?}/{page?}', 'Administrator\AutomodelController@addedit');
    Route::post('automodel/savedata/{id?}/{page?}', 'Administrator\AutomodelController@savedata');
    Route::get('automodel/delete/{id?}/{page?}', 'Administrator\AutomodelController@delete');

    /* Auto Pickup Cost */
    Route::get('autopickupcost', 'Administrator\AutopickupcostController@index');
    Route::post('autopickupcost', 'Administrator\AutopickupcostController@index')->name('autopickup');
    Route::post('autopickupcost/update', 'Administrator\AutopickupcostController@update')->name('updateautopickupcost');

    /* Other Shipment Charges */

    Route::get('othershipmentcharges', 'Administrator\OthershipmentchargesController@index');
    Route::get('othershipmentcharges/index', 'Administrator\OthershipmentchargesController@index');
    Route::post('othershipmentcharges/index', 'Administrator\OthershipmentchargesController@index');
    Route::get('othershipmentcharges/addedit/{id?}/{page?}', 'Administrator\OthershipmentchargesController@addedit');
    Route::post('othershipmentcharges/save/{id?}/{page?}', 'Administrator\OthershipmentchargesController@savedata');
    Route::get('othershipmentcharges/delete/{id?}/{page?}', 'Administrator\OthershipmentchargesController@deletedata');
    Route::get('othershipmentcharges/changestatus/{id?}/{page?}/{status?}', 'Administrator\OthershipmentchargesController@changestatus');

    /* Packaging Charges */
    Route::get('packagingcharges', 'Administrator\PackagingchargesController@index');
    Route::get('packagingcharges/index', 'Administrator\PackagingchargesController@index');
    Route::post('packagingcharges/index', 'Administrator\PackagingchargesController@index');
    Route::get('packagingcharges/addedit/{id?}/{page?}', 'Administrator\PackagingchargesController@addedit');
    Route::post('packagingcharges/save/{id?}/{page?}', 'Administrator\PackagingchargesController@savedata');
    Route::get('packagingcharges/delete/{id?}/{page?}', 'Administrator\PackagingchargesController@deletedata');
    Route::get('packagingcharges/changestatus/{id?}/{page?}/{status?}', 'Administrator\PackagingchargesController@changestatus');

    /* Menu */
    Route::get('menu', 'Administrator\MenuController@index');
    Route::get('menu/index', 'Administrator\MenuController@index');
    Route::post('menu/index', 'Administrator\MenuController@index');
    Route::get('menu/changestatus/{id?}/{page?}/{status?}', 'Administrator\MenuController@changestatus');
    Route::get('menu/addedit/{id?}/{page?}', 'Administrator\MenuController@addedit');
    Route::post('menu/save/{id?}/{page?}', 'Administrator\MenuController@savedata');
    Route::get('menu/delete/{id?}/{page?}', 'Administrator\MenuController@delete');

    /* Payment Settings */
    Route::get('paymentmethod', 'Administrator\PaymentmethodController@index');
    Route::get('paymentmethod/index', 'Administrator\PaymentmethodController@index');
    Route::post('paymentmethod/index', 'Administrator\PaymentmethodController@index');
    Route::get('paymentmethod/configuration/{id?}/{page?}', 'Administrator\PaymentmethodController@configurationsettings');
    Route::get('paymentmethod/addedit/{id?}/{page?}', 'Administrator\PaymentmethodController@addedit');
    Route::post('paymentmethod/save/{id?}/{page?}', 'Administrator\PaymentmethodController@savedata');
    Route::get('paymentmethod/delete/{id?}/{page?}', 'Administrator\PaymentmethodController@delete');
    Route::post('paymentmethod/saveconfiguration/{id?}', 'Administrator\PaymentmethodController@saveconfiguration');
    Route::get('paymentmethod/addtax/{id?}/{page?}', 'Administrator\PaymentmethodController@addtax');
    Route::post('paymentmethod/savetax/{id?}/{page?}', 'Administrator\PaymentmethodController@savetax');
    Route::get('paymentmethod/changestatus/{id?}/{page?}/{status?}', 'Administrator\PaymentmethodController@changestatus');

    /* Fill N Ship */
    Route::get('fillshipsettings', 'Administrator\FillshipsettingsController@index');
    Route::post('fillshipsettings/save', 'Administrator\FillshipsettingsController@savedata');
    Route::get('fillshipboxes', 'Administrator\FillshipboxesController@index');
    Route::post('fillshipboxes/index', 'Administrator\FillshipboxesController@index');
    Route::get('fillshipboxes/index', 'Administrator\FillshipboxesController@index');
    Route::get('fillshipboxes/addedit/{id?}/{page?}', 'Administrator\FillshipboxesController@addedit');
    Route::post('fillshipboxes/save/{id?}/{page?}', 'Administrator\FillshipboxesController@savedata');
    Route::get('fillshipboxes/show/{id?}', 'Administrator\FillshipboxesController@showData');
    Route::get('fillshipboxes/delete/{id?}/{page?}', 'Administrator\FillshipboxesController@delete');   
    Route::get('fillnship', 'Administrator\FillnshipController@index');
    Route::post('fillnship/index', 'Administrator\FillnshipController@index');
    Route::get('fillnship/index', 'Administrator\FillnshipController@index');
    Route::get('fillnship/getstatelist/{countrycode?}', 'Administrator\FillnshipController@getstatelist');
    Route::get('fillnship/getcitylist/{statecode?}', 'Administrator\FillnshipController@getcitylist');
    Route::get('fillnship/printinvoice/{fillshipId?}/{invoiceId?}', 'Administrator\FillnshipController@printinvoice');
    Route::get('fillnship/addedit/{shipmentid?}', 'Administrator\FillnshipController@addedit');
    Route::get('fillnship/notifycustomer/{messageId?}/{shipmentid?}', 'Administrator\FillnshipController@notifycustomer');
    Route::post('fillnship/savenotification/{messageId?}/{shipmentid?}', 'Administrator\FillnshipController@savenotification');
    Route::post('fillnship/addcomment/{shipmentid?}', 'Administrator\FillnshipController@addcomment');
    Route::get('fillnship/sendcomment/{id?}/{shipmentid?}', 'Administrator\FillnshipController@sendcomment');
    Route::get('fillnship/changeaddress/{id?}/{userid?}', 'Administrator\FillnshipController@changeaddress');
    Route::post('fillnship/changeaddress/{id?}/{userid?}', 'Administrator\FillnshipController@changeaddress');
    Route::get('fillnship/getsubcategorylist/{id?}', 'Administrator\FillnshipController@getsubcategorylist');
    Route::get('fillnship/getproductlist/{id?}', 'Administrator\FillnshipController@getproductlist');
    Route::post('fillnship/adddeliveryitem', 'Administrator\FillnshipController@addfillnshipitem');
    Route::get('fillnship/deletedeliveryitem/{id?}', 'Administrator\FillnshipController@deletefillnshipitem');
    Route::get('fillnship/getpackagedetails/{id?}', 'Administrator\FillnshipController@getpackagedetails');
    Route::get('fillnship/viewtrackingdetails/{id?}/{page?}', 'Administrator\FillnshipController@viewtrackingdetails');
    Route::get('fillnship/getshipmentdetails/{id?}/{page?}', 'Administrator\FillnshipController@getshipmentdetails');
    Route::post('fillnship/updateitemstatus', 'Administrator\FillnshipController@updateitemstatus');
    Route::get('fillnship/receivepackage/{id?}/{page?}', 'Administrator\FillnshipController@receivepackage');
    Route::post('fillnship/receivepackage/{id?}/{page?}', 'Administrator\FillnshipController@receivepackage');
    Route::post('fillnship/updatetracking/{procurementItem?}', 'Administrator\FillnshipController@updatetracking');
    Route::get('fillnship/viewtrackingdetails/{id?}/{page?}', 'Administrator\FillnshipController@viewtrackingdetails');
    Route::post('fillnship/updatestage/{shipmentId}', 'Administrator\FillnshipController@updatestage')->name('updatefillnshipstages');
    Route::get('fillnship/shownotes/{id?}/{shipmentid?}', 'Administrator\FillnshipController@shownotes');
    Route::post('fillnship/saveandnotify/{shipmentid?}', 'Administrator\FillnshipController@saveandnotify');
    Route::get('fillnship', 'Administrator\FillnshipController@index')->name('fillnship_requests');
    Route::get('fillnship/printlocationlabel/{id?}/{page?}', 'Administrator\FillnshipController@printlocationlabel');
    Route::get('fillnship/addeditlocation/{id?}/{shipmentid?}', 'Administrator\FillnshipController@addeditlocation');
    Route::post('fillnship/addeditlocation/{id?}/{shipmentid?}', 'Administrator\FillnshipController@addeditlocation');
    Route::get('fillnship/deletelocation/{id?}/{shipmentid?}', 'Administrator\FillnshipController@deletelocation');
    Route::get('fillnship/getwarehousezonelist/{rowid?}', 'Administrator\FillnshipController@getwarehousezonelist');
    Route::get('fillnship/getwarehouselocations/{id?}/{page?}', 'Administrator\FillnshipController@getwarehouselocations');
    Route::get('fillnship/printdeliverylabel/{id?}', 'Administrator\FillnshipController@printdeliverylabel');
    Route::get('fillnship/changestatus/{id?}/{page?}', 'Administrator\FillnshipController@changestatus');
    Route::post('fillnship/changestatus/{id?}/{page?}/{status?}', 'Administrator\FillnshipController@changestatus');
    Route::post('fillnship/updatetracking/{procurementItem?}', 'Administrator\FillnshipController@updatetracking');
    Route::post('fillnship/exportall/{page?}', 'Administrator\FillnshipController@exportall');
    Route::post('fillnship/exportselected/{page?}', 'Administrator\FillnshipController@exportselected');
    Route::get('fillnship/delete/{id?}/{page?}', 'Administrator\FillnshipController@delete');
    Route::post('fillnship/deleteall', 'Administrator\FillnshipController@deleteall');


    /* Offers */
    Route::get('offers', 'Administrator\OffersController@index');
    Route::post('offers/index', 'Administrator\OffersController@index');
    Route::get('offers/index', 'Administrator\OffersController@index');
    Route::get('offers/changestatus/{id?}/{page?}/{status?}', 'Administrator\OffersController@changestatus');
    Route::get('offers/addedit/{id?}/{page?}', 'Administrator\OffersController@addedit');
    Route::post('offers/savedata/{id?}/{page?}', 'Administrator\OffersController@savedata');
    Route::post('offers/getProductsByCategory', 'Administrator\OffersController@getProductsByCategory');
    Route::post('offers/getStates', 'Administrator\OffersController@getStates');
    Route::post('offers/getCities', 'Administrator\OffersController@getCities');
    Route::post('offers/deletedataLocation/{id?}/{page?}', 'Administrator\OffersController@deletedataLocation');
    Route::get('offers/addeditProductSet/{id?}/{page?}', 'Administrator\OffersController@addeditproductset');
    Route::get('offers/deleteProductSet/{setId?}/{id?}/{page?}', 'Administrator\OffersController@deleteproductset');
    Route::get('offers/deleteitemproduct/{setId?}/{valueId?}/{mainId?}/{id?}/{page?}', 'Administrator\OffersController@deleteitemproduct');
    Route::get('offers/deleteitemcategory/{setId?}/{valueId?}/{id?}/{page?}', 'Administrator\OffersController@deleteitemcategory');
    Route::get('offers/sendcoupon/{id?}', 'Administrator\OffersController@sendcoupon');
    Route::post('offers/sendcoupon/{id?}', 'Administrator\OffersController@sendcoupon');
    Route::post('offers/sendcouponcodeselected/{id?}', 'Administrator\OffersController@sendcouponcodeselected');
    Route::post('offers/deletesinglerowcondition', 'Administrator\OffersController@deletesinglerowcondition');
    Route::post('offers/checkNumberOfCondition', 'Administrator\OffersController@checknumberofcondition');

    /* Shipment Tracking */
    Route::get('shipmenttracking', 'Administrator\ShipmenttrackingController@index');
    Route::get('shipmenttracking/index', 'Administrator\ShipmenttrackingController@index');
    Route::post('shipmenttracking/index', 'Administrator\ShipmenttrackingController@index');
    Route::get('shipmenttracking/changestatus/{id?}/{page?}/{status?}', 'Administrator\ShipmenttrackingController@changestatus');
    Route::get('shipmenttracking/addedit/{id?}/{page?}', 'Administrator\ShipmenttrackingController@addedit');
    Route::post('shipmenttracking/save/{id?}/{page?}', 'Administrator\ShipmenttrackingController@savedata');
    Route::get('shipmenttracking/viewfiles/{id?}/{page?}', 'Administrator\ShipmenttrackingController@viewfiles');
    Route::get('shipmenttracking/viewtracking/{id?}/{page?}', 'Administrator\ShipmenttrackingController@viewtracking');
    Route::get('shipmenttracking/delete/{id?}/{page?}', 'Administrator\ShipmenttrackingController@delete');
    Route::post('shipmenttracking/deleteall', 'Administrator\ShipmenttrackingController@deleteall');
    Route::get('shipmenttracking/showall', 'Administrator\ShipmenttrackingController@showall');
    Route::post('shipmenttracking/exportall/{page?}', 'Administrator\ShipmenttrackingController@exportall');
    Route::post('shipmenttracking/exportselected/{page?}', 'Administrator\ShipmenttrackingController@exportSelected');
    Route::post('shipmenttracking/updatetracking/{trackingNumber}/{page?}', 'Administrator\ShipmenttrackingController@updatetracking');


    /* Consolidated Tracking */
    Route::get('consolidatedtracking', 'Administrator\ConsolidatedtrackingController@index');
    Route::get('consolidatedtracking/index', 'Administrator\ConsolidatedtrackingController@index');
    Route::post('consolidatedtracking/index', 'Administrator\ConsolidatedtrackingController@index');
    Route::get('consolidatedtracking/viewtracking/{id?}/{page?}', 'Administrator\ConsolidatedtrackingController@viewtracking');
    Route::get('consolidatedtracking/delete/{id?}/{page?}', 'Administrator\ConsolidatedtrackingController@delete');
    Route::post('consolidatedtracking/deleteall', 'Administrator\ConsolidatedtrackingController@deleteall');
    Route::get('consolidatedtracking/addedit/{id?}/{page?}', 'Administrator\ConsolidatedtrackingController@addedit');
    Route::post('consolidatedtracking/save/{id?}/{page?}', 'Administrator\ConsolidatedtrackingController@savedata');
    Route::post('consolidatedtracking/exportall/{page?}', 'Administrator\ConsolidatedtrackingController@exportall');
    Route::get('consolidatedtracking/viewfiles/{id?}/{page?}', 'Administrator\ConsolidatedtrackingController@viewfiles');
    Route::get('consolidated/commercial-invoice', 'Administrator\ConsolidatedtrackingController@commercialinvoice');
    Route::post('consolidated/commercial-invoice', 'Administrator\ConsolidatedtrackingController@commercialinvoice');
    Route::get('consolidated/commercial-invoice/create', 'Administrator\ConsolidatedtrackingController@createcommercialinvoice');
    Route::post('consolidated/commercial-invoice/create', 'Administrator\ConsolidatedtrackingController@createcommercialinvoice');
    Route::post('consolidated/commercial-invoice/save', 'Administrator\ConsolidatedtrackingController@savecommercialinvoice');
    Route::get('consolidated/commercial-invoice/printcommercialinvoice/{id}/{page?}', 'Administrator\ConsolidatedtrackingController@printcommercialinvoice');
    Route::get('consolidated/commercial-invoice/download/{id}', 'Administrator\ConsolidatedtrackingController@downloadcommercialinvoice');
    Route::get('consolidated/commercial-invoice/delete/{id}/{page?}', 'Administrator\ConsolidatedtrackingController@deletecommercialinvoice');
    
    Route::get('consolidated/shipment-manifest', 'Administrator\ConsolidatedtrackingController@shipmentmanifest');
    Route::post('consolidated/shipment-manifest', 'Administrator\ConsolidatedtrackingController@shipmentmanifest');
    Route::get('consolidated/shipment-manifest/create', 'Administrator\ConsolidatedtrackingController@createshipmentmanifest');
    Route::post('consolidated/shipment-manifest/create', 'Administrator\ConsolidatedtrackingController@createshipmentmanifest');
    Route::post('consolidated/shipment-manifest/save', 'Administrator\ConsolidatedtrackingController@saveshipmentmanifest');
    Route::get('consolidated/shipment-manifest/printcommercialinvoice/{id}/{page?}', 'Administrator\ConsolidatedtrackingController@printshipmentmanifest');
    Route::get('consolidated/shipment-manifest/download/{id}', 'Administrator\ConsolidatedtrackingController@downloadshipmentmanifest');
    Route::get('consolidated/shipment-manifest/delete/{id}/{page?}', 'Administrator\ConsolidatedtrackingController@deleteshipmentmanifest');

    /* Shipping Cost */
    Route::get('shippingcost', 'Administrator\ShippingcostController@index');
    Route::get('shippingcost/index', 'Administrator\ShippingcostController@index');
    Route::post('shippingcost/index', 'Administrator\ShippingcostController@index');
    Route::get('shippingcost/addedit/{id?}/{page?}', 'Administrator\ShippingcostController@addedit');
    Route::get('shippingcost/getmodellist/{id?}', 'Administrator\ShippingcostController@getmodellist');
    Route::post('shippingcost/savedata/{id?}/{page?}', 'Administrator\ShippingcostController@savedata');
    Route::get('shippingcost/delete/{id?}/{page?}', 'Administrator\ShippingcostController@delete');
    Route::post('shippingcost/editdata', 'Administrator\ShippingcostController@editdata')->name('updatedeleteshippingcost');


    /* Shipping Lines */
    Route::get('shippingline', 'Administrator\ShippinglineController@index');
    Route::get('shippingline/index', 'Administrator\ShippinglineController@index');
    Route::post('shippingline/index', 'Administrator\ShippinglineController@index');
    Route::get('shippingline/addedit/{id?}/{page?}', 'Administrator\ShippinglineController@addedit');
    Route::post('shippingline/savedata/{id?}/{page?}', 'Administrator\ShippinglineController@savedata');
    Route::get('shippingline/delete/{id?}/{page?}', 'Administrator\ShippinglineController@delete');

    /* Personal Item Charges */
    Route::get('personalcharge', 'Administrator\PersonalchargeController@index');
    Route::get('personalcharge/index', 'Administrator\PersonalchargeController@index');
    Route::post('personalcharge/index', 'Administrator\PersonalchargeController@index');
    Route::get('personalcharge/addedit/{id?}/{page?}', 'Administrator\PersonalchargeController@addedit');
    Route::get('personalcharge/getmodellist/{id?}', 'Administrator\PersonalchargeController@getmodellist');
    Route::post('personalcharge/savedata/{id?}/{page?}', 'Administrator\PersonalchargeController@savedata');
    Route::get('personalcharge/delete/{id?}/{page?}', 'Administrator\PersonalchargeController@delete');

    /* Cron */
    Route::get('cron/sendnotification', 'Administrator\Cron@sendnotification');

    /* Auto Pickup Requests */
    Route::get('autopickup', 'Administrator\AutopickupController@index');
    Route::get('autopickup/index', 'Administrator\AutopickupController@index');
    Route::post('autopickup/index', 'Administrator\AutopickupController@index');
    Route::get('autopickup/viewcar/{id?}/{page?}', 'Administrator\AutopickupController@viewcardetails');
    Route::get('autopickup/changestatus/{id?}/{page?}', 'Administrator\AutopickupController@changestatus');
    Route::post('autopickup/changestatus/{id?}/{page?}', 'Administrator\AutopickupController@changestatus');
    Route::get('autopickup/view/{id?}/{page?}', 'Administrator\AutopickupController@view');
    Route::get('autopickup/delete/{id?}/{page?}', 'Administrator\AutopickupController@delete');
    Route::get('autopickup/showall', 'Administrator\AutopickupController@showall');
    Route::get('autopickup/getmodellist/{id?}', 'Administrator\AutopickupController@getmodellist');
    Route::get('autopickup/getLocation/{q?}', 'Administrator\AutopickupController@getLocation');
    Route::post('autopickup/adddeliveryitem', 'Administrator\AutopickupController@adddeliveryitem');
    Route::get('autopickup/getitemdetails/{id?}', 'Administrator\AutopickupController@getitemdetails');
    Route::get('autopickup/getprocurementdetails/{id?}', 'Administrator\AutopickupController@getprocurementdetails');
    Route::get('autopickup/deletedeliveryitem/{id?}', 'Administrator\AutopickupController@deletedeliveryitem');
    Route::post('autopickup/editdeliveryitem', 'Administrator\AutopickupController@editdeliveryitem');
    Route::get('autopickup/uploadimage/{itemId?}/{page?}', 'Administrator\AutopickupController@uploadcarimage');
    Route::post('autopickup/saveimage/{itemId?}/{procurementId?}/{page?}', 'Administrator\AutopickupController@saveimage');
    Route::post('autopickup/updateprocurementitemstatus', 'Administrator\AutopickupController@updateprocurementitemstatus');
    Route::post('autopickup/updatepaymentstatus/{procurementId}/{userId}', 'Administrator\AutopickupController@updatepaymentstatus')->name('buycarupdatepaymentstatus');
    Route::get('autopickup/checkprocurementstatus/{id?}', 'Administrator\AutopickupController@checkprocurementstatus');
    Route::get('autopickup/receiveitem/{id?}/{page?}', 'Administrator\AutopickupController@receiveitem');
    Route::post('autopickup/receiveitem/{id?}/{page?}', 'Administrator\AutopickupController@receiveitem');
    Route::get('autopickup/viewtrackingdetails/{id?}/{page?}', 'Administrator\AutopickupController@viewtrackingdetails');
    Route::post('autopickup/notifycustomer/{procurementid?}', 'Administrator\AutopickupController@notifycustomer');
    Route::get('autopickup/createshipment/{procurementid?}', 'Administrator\AutopickupController@createshipment')->name('autopickupcreateshipment');
    Route::get('autopickup/printinvoice/{procurementId?}/{invoiceId?}', 'Administrator\AutopickupController@printinvoice')->name('printinvoice');

    /* Auto Website */
    Route::get('autowebsite', 'Administrator\AutowebsiteController@index');
    Route::get('autowebsite/index', 'Administrator\AutowebsiteController@index');
    Route::post('autowebsite/index', 'Administrator\AutowebsiteController@index');
    Route::get('autowebsite/addedit/{id?}/{page?}', 'Administrator\AutowebsiteController@addedit');
    Route::post('autowebsite/savedata/{id?}/{page?}', 'Administrator\AutowebsiteController@savedata');
    Route::get('autowebsite/delete/{id?}/{page?}', 'Administrator\AutowebsiteController@delete');
    Route::get('autowebsite/checkavailability/{id?}/{page?}', 'Administrator\AutowebsiteController@checkavailability');
    Route::post('autowebsite/saveavailability/{id?}/{page?}', 'Administrator\AutowebsiteController@saveavailability');
    Route::get('autowebsite/changestatus/{id?}/{page?}/{status?}', 'Administrator\AutowebsiteController@changestatus');


    /* Auto Shipment */
    Route::get('autoshipment', 'Administrator\AutoshipmentController@index');
    Route::get('autoshipment/index', 'Administrator\AutoshipmentController@index');
    Route::post('autoshipment/index', 'Administrator\AutoshipmentController@index');
    Route::get('autoshipment/addedit/{id?}/{page?}', 'Administrator\AutoshipmentController@addedit');
    Route::post('autoshipment/save/{id?}/{page?}', 'Administrator\AutoshipmentController@savedata');
    Route::get('autoshipment/delete/{id?}/{page?}', 'Administrator\AutoshipmentController@delete')->name('deleteautoshipment');
    Route::get('autoshipment/getstatelist/{countrycode?}', 'Administrator\AutoshipmentController@getstatelist');
    Route::get('autoshipment/getCities/{statecode?}', 'Administrator\AutoshipmentController@getCities');
    Route::get('autoshipment/getCarMake/{makeid?}', 'Administrator\AutoshipmentController@getCarMake');
    Route::get('autoshipment/verifyuser/{unit?}', 'Administrator\AutoshipmentController@verifyuser');
    Route::get('autoshipment/showall', 'Administrator\AutoshipmentController@showall');
    Route::post('autoshipment/deleteSelected', 'Administrator\AutoshipmentController@deleteSelected');
    Route::get('autoshipment/view/{id?}/{page?}', 'Administrator\AutoshipmentController@view');
    Route::post('autoshipment/updatestatus', 'Administrator\AutoshipmentController@updatestatus');
    Route::get('autoshipment/printinvoice/{procurementId?}/{invoiceId?}', 'Administrator\AutoshipmentController@printinvoice')->name('printinvoice');
    Route::get('autoshipment/downloadtitle/{titleId}/{userId}', 'Administrator\AutoshipmentController@downloadtitle')->name('autoshipmenttitledownload');
    Route::post('autoshipment/updatepaymentstatus/{shipmentId}/{userId}', 'Administrator\AutoshipmentController@updatepaymentstatus')->name('autoshipmentupdatepaymentstatus');
    Route::get('autoshipment/createorder/{id?}/{page?}', 'Administrator\AutoshipmentController@createorder')->name('autoshipmentcreateorder');
    Route::post('autoshipment/notifycustomer/{shipmentId?}', 'Administrator\AutoshipmentController@notifycustomer');

    /* Auto Parts Requests */
    Route::get('autoparts', 'Administrator\AutopartsController@index');
    Route::get('autoparts/index', 'Administrator\AutopartsController@index');
    Route::post('autoparts/index', 'Administrator\AutopartsController@index');
    Route::get('autoparts/changestatus/{id?}/{page?}', 'Administrator\AutopartsController@changestatus');
    Route::post('autoparts/changestatus/{id?}/{page?}', 'Administrator\AutopartsController@changestatus');
    Route::get('autoparts/getstatelist/{id?}', 'Administrator\AutopartsController@getstatelist');
    Route::get('autoparts/getcitylist/{id?}', 'Administrator\AutopartsController@getcitylist');
    Route::get('autoparts/view/{id?}/{page?}', 'Administrator\AutopartsController@view');
    Route::get('autoparts/delete/{id?}/{page?}', 'Administrator\AutopartsController@delete');
    Route::get('autoparts/showall', 'Administrator\AutopartsController@showall');
    Route::get('autoparts/getmodellist/{id?}', 'Administrator\AutopartsController@getmodellist');
    Route::post('autoparts/adddeliveryitem', 'Administrator\AutopartsController@adddeliveryitem');
    Route::get('autoparts/getitemdetails/{id?}', 'Administrator\AutopartsController@getitemdetails');
    Route::get('autoparts/getprocurementdetails/{id?}', 'Administrator\AutopartsController@getprocurementdetails');
    Route::get('autoparts/deletedeliveryitem/{id?}', 'Administrator\AutopartsController@deletedeliveryitem');
    Route::post('autoparts/editdeliveryitem', 'Administrator\AutopartsController@editdeliveryitem');
    Route::get('autoparts/uploadimage/{itemId?}/{page?}', 'Administrator\AutopartsController@uploadimage');
    Route::post('autoparts/saveimage/{itemId?}/{procurementId?}/{page?}', 'Administrator\AutopartsController@saveimage');
    Route::post('autoparts/updateprocurementitemstatus', 'Administrator\AutopartsController@updateprocurementitemstatus');
    Route::get('autoparts/checkprocurementstatus/{id?}', 'Administrator\AutopartsController@checkprocurementstatus');
    Route::get('autoparts/receiveitem/{id?}/{page?}', 'Administrator\AutopartsController@receiveitem');
    Route::post('autoparts/receiveitem/{id?}/{page?}', 'Administrator\AutopartsController@receiveitem');
    Route::get('autoparts/getprocurementcostdetails/{id?}', 'Administrator\AutopartsController@getprocurementcostdetails');
    Route::post('autoparts/procurementweightdetails/{id?}', 'Administrator\AutopartsController@procurementweightdetails');
    Route::post('autoparts/saveweightdetails/{id?}', 'Administrator\AutopartsController@saveweightdetails');
    Route::get('autoparts/paymentdetails/{id?}', 'Administrator\AutopartsController@paymentdetails');
    Route::get('autoparts/sendcustomerinvoice/{id?}', 'Administrator\AutopartsController@sendcustomerinvoice');
    Route::get('autoparts/notifycustomer/{messageId?}/{shipmentid?}', 'Administrator\AutopartsController@notifycustomer');
    Route::post('autoparts/notifycustomer/{messageId?}/{shipmentid?}', 'Administrator\AutopartsController@notifycustomer');
    Route::post('autoparts/addcomment/{shipmentid?}', 'Administrator\AutopartsController@addcomment');
    Route::get('autoparts/sendcomment/{id?}/{shipmentid?}', 'Administrator\AutopartsController@sendcomment');
    Route::post('autoparts/saveandnotify/{shipmentid?}', 'Administrator\AutopartsController@saveandnotify');
    Route::get('autoparts/changeaddress/{id?}/{userid?}', 'Administrator\AutopartsController@changeaddress');
    Route::post('autoparts/changeaddress/{id?}/{userid?}', 'Administrator\AutopartsController@changeaddress');
    Route::get('autoparts/getsubcategorylist/{id?}', 'Administrator\AutopartsController@getsubcategorylist');
    Route::get('autoparts/getproductlist/{id?}', 'Administrator\AutopartsController@getproductlist');
    Route::get('autoparts/createshipment/{userid?}/{id}', 'Administrator\AutopartsController@createshipment');
    Route::post('autoparts/createinvoice/{id?}', 'Administrator\AutopartsController@createinvoice');
    Route::get('autoparts/printinvoice/{id?}/{invoiceid?}', 'Administrator\AutopartsController@printinvoice');
    Route::get('autoparts/viewtrackingdetails/{id?}/{page?}', 'Administrator\AutopartsController@viewtrackingdetails');
    Route::post('autoparts/saveshipment/{id}', 'Administrator\AutopartsController@saveshipment');
    Route::get('autoparts/updatepaymentstatus/{shipmentid?}/{status?}', 'Administrator\AutopartsController@updatepaymentstatus');
    Route::get('autoparts/createprepaidshipment/{id?}', 'Administrator\AutopartsController@createprepaidshipment');
    Route::get('autoparts/getunpaidshipmentlist/{userid?}', 'Administrator\AutopartsController@getunpaidshipmentlist');
    Route::get('autoparts/updateprocurementpayment/{userid?}/{status?}', 'Administrator\AutopartsController@updateprocurementpayment');
    Route::post('autoparts/updatetracking/{procurementId?}', 'Administrator\ProcurementController@updateAutopartstracking');

    /* Media Library */
    Route::get('media', 'Administrator\MedialibraryController@index');
    Route::get('media/index', 'Administrator\MedialibraryController@index');
    Route::post('media/index', 'Administrator\MedialibraryController@index');
    Route::get('media/addedit/{id?}/{page?}', 'Administrator\MedialibraryController@addedit');
    Route::post('media/savedata/{id?}/{page?}', 'Administrator\MedialibraryController@savedata');
    Route::get('media/delete/{id?}/{page?}', 'Administrator\MedialibraryController@delete');

    /* Use Media Library */
    Route::get('media/media/{id?}/{page?}', 'Administrator\MedialibraryController@popup');
    Route::post('media/loaddata', 'Administrator\MedialibraryController@loaddataajax');
    Route::post('media/loaddataselected', 'Administrator\MedialibraryController@loaddataselected');

    /* Procurement Services */
    Route::get('othervehicle', 'Administrator\OthervehicleController@index');
    Route::get('othervehicle/index', 'Administrator\OthervehicleController@index');
    Route::post('othervehicle/index', 'Administrator\OthervehicleController@index');
    Route::get('othervehicle/changestatus/{id?}/{page?}', 'Administrator\OthervehicleController@changestatus');
    Route::post('othervehicle/changestatus/{id?}/{page?}/{status?}', 'Administrator\OthervehicleController@changestatus');
    Route::get('othervehicle/delete/{id?}/{page?}', 'Administrator\OthervehicleController@delete');
    Route::get('othervehicle/getstatelist/{countrycode?}', 'Administrator\OthervehicleController@getstatelist');
    Route::get('othervehicle/getcitylist/{statecode?}', 'Administrator\OthervehicleController@getcitylist');
    Route::get('othervehicle/getcitylistbycountry/{countryid?}', 'Administrator\OthervehicleController@getcitylistbycountry');
    Route::post('othervehicle/exportall/{page?}', 'Administrator\OthervehicleController@exportall');
    Route::post('othervehicle/exportselected/{page?}', 'Administrator\OthervehicleController@exportselected');
    Route::get('othervehicle/showall', 'Administrator\OthervehicleController@showall');
    Route::get('othervehicle/addedit/{id?}/{page?}', 'Administrator\OthervehicleController@addedit');
    Route::get('othervehicle/changeaddress/{id?}/{userid?}', 'Administrator\OthervehicleController@changeaddress');
    Route::post('othervehicle/changeaddress/{id?}/{userid?}', 'Administrator\OthervehicleController@changeaddress');
    Route::get('othervehicle/getaddressbookdetails/{id?}', 'Administrator\OthervehicleController@getaddressbookdetails');
    Route::get('othervehicle/getaddressdetails/{id?}', 'Administrator\OthervehicleController@getaddressdetails');
    Route::get('othervehicle/notifycustomer/{messageId?}/{shipmentid?}', 'Administrator\OthervehicleController@notifycustomer');
    Route::post('othervehicle/notifycustomer/{messageId?}/{shipmentid?}', 'Administrator\OthervehicleController@notifycustomer');
    Route::post('othervehicle/addcomment/{shipmentid?}', 'Administrator\OthervehicleController@addcomment');
    Route::get('othervehicle/sendcomment/{id?}/{shipmentid?}', 'Administrator\OthervehicleController@sendcomment');
    Route::post('othervehicle/saveandnotify/{shipmentid?}', 'Administrator\OthervehicleController@saveandnotify');
    Route::post('othervehicle/adddeliveryitem', 'Administrator\OthervehicleController@addothervehicleitem');
    Route::post('othervehicle/editdeliveryitem', 'Administrator\OthervehicleController@editothervehicleitem');
    Route::get('othervehicle/deletedeliveryitem/{id?}', 'Administrator\OthervehicleController@deleteothervehicleitem');
    Route::get('othervehicle/receivepackage/{id?}/{page?}', 'Administrator\OthervehicleController@receivepackage');
    Route::post('othervehicle/receivepackage/{id?}/{page?}', 'Administrator\OthervehicleController@receivepackage');
    Route::get('othervehicle/getmodellist/{id?}', 'Administrator\OthervehicleController@getmodellist');
    Route::get('othervehicle/getpackagedetails/{id?}', 'Administrator\OthervehicleController@getpackagedetails');
    Route::get('othervehicle/viewtrackingdetails/{id?}/{page?}', 'Administrator\OthervehicleController@viewtrackingdetails');
    Route::get('othervehicle/getothervehicledetails/{id?}/{page?}', 'Administrator\OthervehicleController@getothervehicledetails');
    Route::post('othervehicle/updateprocurementitemstatus', 'Administrator\OthervehicleController@updateprocurementitemstatus');
    Route::get('othervehicle/checkprocurementstatus/{id?}', 'Administrator\OthervehicleController@checkprocurementstatus');
    Route::get('othervehicle/getprocurementcostdetails/{id?}', 'Administrator\OthervehicleController@getprocurementcostdetails');
    Route::post('othervehicle/procurementweightdetails/{id?}', 'Administrator\OthervehicleController@procurementweightdetails');
    Route::post('othervehicle/saveweightdetails/{id?}', 'Administrator\OthervehicleController@saveweightdetails');
    Route::get('othervehicle/paymentdetails/{id?}', 'Administrator\OthervehicleController@paymentdetails');
    Route::get('othervehicle/sendcustomerinvoice/{id?}', 'Administrator\OthervehicleController@sendcustomerinvoice');
    Route::get('othervehicle/createshipment/{id?}', 'Administrator\OthervehicleController@createshipment');

    /* Get Country Phone List */
    Route::post('generalsettings/getphone', 'Administrator\SettingsController@getphonedata');

    /* Remove Contact from General Settings */
    Route::get('generalsettings/deletecontact/{val?}', 'Administrator\SettingsController@deletecontact');


    /* Newsletter Management */
    Route::get('newsletter', 'Administrator\NewsletterController@index');
    Route::get('newsletter/index', 'Administrator\NewsletterController@index');
    Route::post('newsletter/index', 'Administrator\NewsletterController@index');
    Route::get('newsletter/addedit/{id?}/{page?}', 'Administrator\NewsletterController@addedit');
    Route::post('newsletter/save/{id?}/{page?}', 'Administrator\NewsletterController@savedata');
    Route::get('newsletter/sendmessage/{id?}/{page?}', 'Administrator\NewsletterController@sendmessage');
    Route::post('newsletter/savesubscriber/{id?}/{page?}', 'Administrator\NewsletterController@savesubscriber');

    /* Contact us requests */
    Route::get('contactus', 'Administrator\ContactusController@index')->name('contactuslist');
    Route::post('contactus', 'Administrator\ContactusController@index')->name('contactus');
    Route::get('contactus/details/{id}/{page?}', 'Administrator\ContactusController@getdetails')->name('contactusdetails');
    Route::post('contactus/reply/{id}/{page?}', 'Administrator\ContactusController@reply')->name('contactusreply');
    Route::get('contactus/delete/{id}/{page?}', 'Administrator\ContactusController@deletedata')->name('contactusdelete');
    Route::post('contactus/deleteselected', 'Administrator\ContactusController@deleteselected')->name('contactusdeleteSelected');
    Route::get('contactus/resolved/{id}/{page?}', 'Administrator\ContactusController@resolved')->name('resolved');

    /* FAQ */
    Route::get('faq', 'Administrator\FaqController@index');
    Route::get('faq/index', 'Administrator\FaqController@index');
    Route::post('faq/index', 'Administrator\FaqController@index');
    Route::get('faq/addedit/{id?}/{page?}', 'Administrator\FaqController@addedit');
    Route::post('faq/savedata/{id?}/{page?}', 'Administrator\FaqController@savedata');
    Route::get('faq/changestatus/{id?}/{page?}/{status?}', 'Administrator\FaqController@changestatus');
    Route::get('faq/delete/{id?}/{page?}', 'Administrator\FaqController@delete');

    /* PARTNER */
    Route::get('partner', 'Administrator\PartnerController@index');
    Route::get('partner/index', 'Administrator\PartnerController@index');
    Route::post('partner/index', 'Administrator\PartnerController@index');
    Route::get('partner/addedit/{id?}/{page?}', 'Administrator\PartnerController@addedit');
    Route::post('partner/savedata/{id?}/{page?}', 'Administrator\PartnerController@savedata');
    Route::get('partner/changestatus/{id?}/{page?}/{status?}', 'Administrator\PartnerController@changestatus');
    Route::get('partner/delete/{id?}/{page?}', 'Administrator\PartnerController@delete');
    Route::get('partner/getcontent/{id?}/{page?}/{type?}', 'Administrator\PartnerController@getcontent');
    Route::post('partner/updatecontent/{id?}', 'Administrator\PartnerController@updatecontent');

    /* Contact reasons */
    Route::get('contactusreasons', 'Administrator\ContactreasonController@index');
    Route::get('contactusreasons/index', 'Administrator\ContactreasonController@index');
    Route::post('contactusreasons/index', 'Administrator\ContactreasonController@index');
    Route::get('contactusreasons/addedit/{id?}/{page?}', 'Administrator\ContactreasonController@addedit');
    Route::post('contactusreasons/savedata/{id?}/{page?}', 'Administrator\ContactreasonController@savedata');
    Route::get('contactusreasons/changestatus/{id?}/{page?}/{status?}', 'Administrator\ContactreasonController@changestatus');
    Route::get('contactusreasons/delete/{id?}/{page?}', 'Administrator\ContactreasonController@delete');

    /* REWARD POINT LEVELS */
    Route::get('rewardsettings', 'Administrator\RewardsettingsController@index');
    Route::get('rewardsettings/index', 'Administrator\RewardsettingsController@index');
    Route::post('rewardsettings/index', 'Administrator\RewardsettingsController@index');
    Route::get('rewardsettings/addedit/{id?}/{page?}', 'Administrator\RewardsettingsController@addedit');
    Route::post('rewardsettings/savedata/{id?}/{page?}', 'Administrator\RewardsettingsController@savedata');
    Route::get('rewardsettings/delete/{id?}/{page?}', 'Administrator\RewardsettingsController@delete');

    /* CUSTOMER HELP MANAGEMENT */
    Route::get('customerhelp', 'Administrator\CustomerhelpController@index');
    Route::get('customerhelp/index', 'Administrator\CustomerhelpController@index');
    Route::post('customerhelp/index', 'Administrator\CustomerhelpController@index');
    Route::get('customerhelp/addedit/{id?}/{page?}', 'Administrator\CustomerhelpController@addedit');
    Route::post('customerhelp/savedata/{id?}/{page?}', 'Administrator\CustomerhelpController@savedata');
    Route::get('customerhelp/changestatus/{id?}/{page?}/{status?}', 'Administrator\CustomerhelpController@changestatus');
    Route::get('customerhelp/delete/{id?}/{page?}', 'Administrator\CustomerhelpController@delete');

    Route::get('customerhelpsections', 'Administrator\CustomerhelpsectionController@index');
    Route::get('customerhelpsections/index', 'Administrator\CustomerhelpsectionController@index');
    Route::post('customerhelpsections/index', 'Administrator\CustomerhelpsectionController@index');
    Route::get('customerhelpsections/addedit/{id?}/{page?}', 'Administrator\CustomerhelpsectionController@addedit');
    Route::post('customerhelpsections/savedata/{id?}/{page?}', 'Administrator\CustomerhelpsectionController@savedata');
    Route::get('customerhelpsections/changestatus/{id?}/{page?}/{status?}', 'Administrator\CustomerhelpsectionController@changestatus');
    Route::get('customerhelpsections/delete/{id?}/{page?}', 'Administrator\CustomerhelpsectionController@delete');

    /* WAREHOUSE TRACKING (REPORT) */

    Route::get('report/procurement-tracking', 'Administrator\ReportController@procurementindex');
    Route::get('report/procurement-tracking/procurementindex', 'Administrator\ReportController@procurementindex');
    Route::post('report/procurement-tracking/procurementindex', 'Administrator\ReportController@procurementindex');

    Route::get('report/procurement-tracking/showall/{type?}', 'Administrator\ReportController@showall');
    Route::post('report/exportall/{page?}', 'Administrator\ReportController@exportall');
    Route::post('report/exportselected/{page?}', 'Administrator\ReportController@exportselected');

    Route::post('procurement/exportall/{page?}', 'Administrator\ProcurementController@exportall');
    Route::post('report/procurement-tracking/exportall/{page?}', 'Administrator\ReportController@exportall_procurement');
    Route::post('report/procurement-tracking/exportselected/{page?}', 'Administrator\ReportController@exportselected_procurement');

    Route::get('report/shipment-tracking', 'Administrator\ReportController@shipmenttracking')->name('warehouseshipmenttrackinglist');
    Route::post('report/shipment-tracking', 'Administrator\ReportController@shipmenttracking')->name('warehouseshipmenttrackinglist');
    Route::get('report/shipment-tracking/showall/{type?}', 'Administrator\ReportController@showall')->name('warehouseshipmenttrackingshowall');
    Route::post('report/shipment-tracking/exportall/{page?}', 'Administrator\ReportController@exportallshipment')->name('warehouseshipmenttrackingexportall');
    Route::post('report/shipment-tracking/exportselected/{page?}', 'Administrator\ReportController@exportselected');

    Route::get('report/special-offer-report', 'Administrator\ReportController@offerreport');
    Route::get('report/special-offer-report', 'Administrator\ReportController@offerreport')->name('offers');
    Route::post('report/special-offer-report', 'Administrator\ReportController@offerreport')->name('offers');
    Route::get('report/special-offer-report/{id?}', 'Administrator\ReportController@offerreport')->name('offers');

    Route::post('report/special-offer-report/exportallProcurementReportForSpecialOffer/{page}', 'Administrator\ReportController@exportallProcurementReportForSpecialOffer');
    Route::post('report/special-offer-report/exportallShipmentReportForSpecialOffer/{page}', 'Administrator\ReportController@exportallShipmentReportForSpecialOffer');
    Route::post('report/special-offer-report/exportallFillshipReportForSpecialOffer/{page}', 'Administrator\ReportController@exportallFillshipReportForSpecialOffer');
    Route::post('report/special-offer-report/exportallAutoReportForSpecialOffer/{page}', 'Administrator\ReportController@exportallAutoReportForSpecialOffer');

    Route::post('report/special-offer-report/exportselectedShipmentReportForSpecialOffer/{id}', 'Administrator\ReportController@exportselectedShipmentReportForSpecialOffer');
    Route::post('report/special-offer-report/exportselectedProcurementReportForSpecialOffer/{id}', 'Administrator\ReportController@exportselectedProcurementReportForSpecialOffer');
    Route::post('report/special-offer-report/exportselectedFillshipReportForSpecialOffer/{id}', 'Administrator\ReportController@exportselectedFillshipReportForSpecialOffer');
    Route::post('report/special-offer-report/exportselectedAutoReportForSpecialOffer/{id}', 'Administrator\ReportController@exportselectedAutoReportForSpecialOffer');

    

    Route::get('report/customer', 'Administrator\ReportController@customerreport');
    Route::get('report/customer', 'Administrator\ReportController@customerreport')->name('customers');
    Route::post('report/customer', 'Administrator\ReportController@customerreport')->name('customers');
    Route::get('report/getcustomersearchcondition/{type?}', 'Administrator\ReportController@getcustomersearchcondition');
    Route::get('report/customer/showall/{type?}', 'Administrator\ReportController@showall')->name('customershowall');
    Route::get('report/customer/export', 'Administrator\ReportController@exportcustomerreportdata')->name('customers');

    Route::get('report/warehouse-tracking', 'Administrator\ReportController@warehousetrackingreport');
    Route::get('report/warehouse-tracking', 'Administrator\ReportController@warehousetrackingreport')->name('warehousetrackinglist');
    Route::post('report/warehouse-tracking', 'Administrator\ReportController@warehousetrackingreport')->name('warehousetrackinglist');
    Route::get('report/getwarehousesearchcondition/{type?}', 'Administrator\ReportController@getwarehousesearchcondition');
    
    Route::get('report/warehouse-tracking/export', 'Administrator\ReportController@exportwarehousereportdata')->name('exportwarehousereportdata');

    
    /* FILL & SHIP */
    Route::get('fillnship', 'Administrator\FillnshipController@index')->name('fillnship_requests');
    Route::get('fillnship/printlocationlabel/{id?}/{page?}', 'Administrator\FillnshipController@printlocationlabel');
    Route::get('fillnship/addeditlocation/{id?}/{shipmentid?}', 'Administrator\FillnshipController@addeditlocation');
    Route::post('fillnship/addeditlocation/{id?}/{shipmentid?}', 'Administrator\FillnshipController@addeditlocation');
    Route::get('fillnship/deletelocation/{id?}/{shipmentid?}', 'Administrator\FillnshipController@deletelocation');
    Route::get('fillnship/getwarehousezonelist/{rowid?}', 'Administrator\FillnshipController@getwarehousezonelist');
    Route::get('fillnship/getwarehouselocations/{id?}/{page?}', 'Administrator\FillnshipController@getwarehouselocations');
    Route::get('fillnship/printdeliverylabel/{id?}', 'Administrator\FillnshipController@printdeliverylabel');
    Route::get('fillnship/validatepackage/{id?}/{page?}', 'Administrator\FillnshipController@validatepackage');
    Route::post('fillnship/startpackaging/{id?}/{page?}', 'Administrator\FillnshipController@startpackaging')->name('fillshipstartpackaging');
    Route::post('fillnship/addpackage/{id?}/{page?}', 'Administrator\FillnshipController@addpackage')->name('fillshipnewpackaging');
    Route::post('fillnship/editpackage/{id?}/{page?}', 'Administrator\FillnshipController@editpackage')->name('fillshipeditpackaging');
    Route::post('fillnship/addfillshippackagenotes/{id?}/{page?}', 'Administrator\FillnshipController@addfillshippackagenotes')->name('fillshippackagenotes');
    Route::get('fillnship/updatepackedstatus/{shipmentId}', 'Administrator\FillnshipController@updatepackedstatus');
    Route::get('fillnship/printmanifest/{id?}/{page?}', 'Administrator\FillnshipController@printmanifest');
    Route::get('fillnship/getshippinglabelfields/{id?}/{page?}', 'Administrator\FillnshipController@getshippinglabelfields');
    Route::post('fillnship/printshippinglabel/{id?}/{page?}', 'Administrator\FillnshipController@printshippinglabel')->name('fillshipshippinglabel');
    Route::get('fillnship/redstarlabel/{id?}/{page?}', 'Administrator\FillnshipController@redstarlabel');
    Route::get('fillnship/dhl/{id?}/{page?}', 'Administrator\FillnshipController@dhl');
    Route::post('fillnship/dhlpost', 'Administrator\FillnshipController@dhlpost');
    Route::get('fillnship/dhlci/{id?}/{page?}', 'Administrator\FillnshipController@dhlci');
    Route::get('fillnship/dhlcipost/{id?}/{page?}', 'Administrator\FillnshipController@dhlcipost');
    Route::post('fillnship/dhlcipost/', 'Administrator\FillnshipController@dhlcipost');
    Route::get('fillnship/nationsdelivery/{id?}/{page?}', 'Administrator\FillnshipController@nationsdelivery');
    
    Route::get('report/storehistory', 'Administrator\ReportController@storehistory');
    Route::post('report/storehistory', 'Administrator\ReportController@storehistory');
    Route::get('report/storehistory/showall/{type?}', 'Administrator\ReportController@showall');
    Route::post('report/storehistory/exportAllStores/{page?}', 'Administrator\ReportController@exportallstores');
    Route::post('report/storehistory/showall/{type?}', 'Administrator\ReportController@showall');
    Route::get('report/referral', 'Administrator\ReportController@referral');
    Route::post('report/referral', 'Administrator\ReportController@referral');
    Route::get('report/referral/export', 'Administrator\ReportController@exportallreferral');

});



//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
