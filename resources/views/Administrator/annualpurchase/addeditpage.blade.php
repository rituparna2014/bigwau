@extends ('Administrator.layouts.master')
@section('content')
<section class="content">
    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">

                @if($action == 'Add') 
                {{ Form::open(array('url' => 'administrator/addeditannualpurchaserecord/-1/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                @else  
                {{ Form::open(array('url' => 'administrator/addeditannualpurchaserecord/' . $record->id.'/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                @endif
                <div class="box-body">
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Title <span class="text-red">*</span></label>
                                <input id="title" name="titles" required="" class="form-control" placeholder="Enter Title" type="text" value="{{ ($record->title!='') ? $record->title : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id="description" name="description" class="form-control" placeholder="Enter Description" type="text" required>{{ ($record->description!='') ? $record->description : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->


                <div class="box-footer text-right">
                   
                    <a class="btn btn-info" href="{{ route('annualpurchase') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<div class="modal fade" id="modal-addEdit"></div>
<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>


@endsection

<script>
$(function () {
    $("#addeditFrm").validate();
});
</script>