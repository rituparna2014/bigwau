@extends('Administrator.layouts.master')
@section('content')

<!-- Main content -->
<section class="content">   
    <div class="row m-b-15">
        {{ Form::open(array('url' => route('subscription'), 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-md-12 text-right">
            <div class="pull-left"> <label>Entries per page</label>
                <div class="adddrop m-l-15 w-100">
                    <select class="form-control highLight" name="searchDisplay" onchange="$('#frmsearch').submit();">
                        @for($step = 10; $step<=50; $step+=10)
                        <option value="{{ $step }}" {{($param['searchDisplay'] == $step) ? "selected" : ""}}>{{ $step }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="text-right">
                @if($canAdd == 1)<a class="btn btn-info btn-sm" href="{{ route('addeditannualpurchase',['id' => '-1']) }}"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New {{substr($pageTitle,0,-1) }}</a>@endif
            </div>
            <input type="hidden" name="field" id="field" value="{{$param['sortField']}}" />
            <input type="hidden" name="type" id="type" value="{{$param['sortOrder']}}" />
            <input type="hidden" name="searchData" id="searchData" value="">
            <input type="hidden" name="resetData" id="resetData" value="0">
        </div>
        {{ Form::close() }}
    </div>                 
    <section class="col-lg-12 connectedSortable"> 
        <!-- Custom tabs (Charts with tabs)-->                          

        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">&nbsp;</div>
                    <div class="col-sm-6 text-right">
                        <!--<a href="javascript:void(0)" id="resetFilter">Reset Filter</a> -->
                        <div id="NY_filter" class="dataTables_filter rtSearchBox pull-right"><label>Search:</label>
                            <input class="form-control input-sm" id="table-search-text" placeholder="Enter search text ... " aria-controls="NY" type="search" value="{{ ($param['searchData']!='' ? $param['searchData'] : "" )}}">
                            <a id="table-search-button-pages" class="btn btn-success btn-sm"><span class="Cicon"><i class="fa fa-search"></i></span>Search</a>
                        </div>
                    </div>
                </div>                                    
                <table id="example2" class="table table-bordered table-hover countryName">
                    <thead>
                        <tr>                                                
                            <th> Title</th>
                            <th width="15%">Status</th>
                            <th width="15%">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($records)>0)
                        @foreach($records as $record)
                        <tr>
                            <td>{{ $record->title }}</td>
                            <td>@if($canEdit == 1)
                                @if($record->status=='1')
                                <a data-toggle="tooltip" title="" class="btn btn-success btnActive updateStatusCommon" data-edit="{{ $record->id }}" data-status="1" id="AL" data-original-title="Click to Change Staus">Active</a>
                                @else
                                <a data-toggle="tooltip" title="" class="btn btn-danger btnActive updateStatusCommon" data-edit="{{ $record->id }}" data-status="0"  id="AL" data-original-title="Click to Change Staus">Inactive</a>
                                @endif
                                @endif
                            </td>
                            <td>
                                @if($canEdit == 1)<a class="text-green actionIcons" href="{{ route('addeditannualpurchase',['id' => $record->id]) }}"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit"></i></a>@endif
                                @if($canDelete == 1)<a class="color-theme-2 actionIcons" href="{{ route('deleteannualpurchaserecord',['id' => $record->id]) }}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                            </td>
                        </tr>
                        @endforeach
                        @endif                                                              
                    </tbody>

                </table>
                <div class="row mt-20">
                    <div class="col-md-6 col-sm-6 col-xs-6 for12">
                        <p class="results">Showing {{ $records->firstItem() . ' - ' . $records->lastItem() . ' of  ' . $records->total() }}</p>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                            {{ $records->links() }}
                        </div>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.nav-tabs-custom --> 
    </section>
</div>
<input type="hidden" id="ajaxPage" value="editannualpurchasestatus">
<div class="modal fade" id="modal-addEdit"></div>
</section>

@endsection