@extends ('Administrator.layouts.master')

@section('content')

<!-- Main content -->
                
                <section class="content gapdashboard">
                    <div class="row">
                        <div class="tab-content no-padding"> 
                            <div class="nav-tabs-custom" style="margin-left: 15px; margin-right: 15px;"> 
                              <div class="chart tab-pane active">
                              <ul class="airShip">
                                  <li>
                                      <div class="airShipBox ltGreenCir"><span class="borairCir"> <a data-toggle="tooltip" href="{{url('/administrator/sellers/')}}" class="airCir" title="Seller Management"><img src="{{ asset('public/administrator/img/icon01.png') }}" width="58" height="58"></a> </span>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="airShipBox skyBlueCir"><span class="borairCir"> <a data-toggle="tooltip" href="{{url('/administrator/buyers/')}}" class="airCir" title="Buyer Management"><img src="{{ asset('public/administrator/img/icon01.png') }}" width="58" height="58"></a> </span>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="airShipBox drkGreenCir"><span class="borairCir"> <a data-toggle="tooltip" href="{{url('/administrator/settingscategories/')}}" class="airCir" title="Category Management"><img src="{{ asset('public/administrator/img/icon03.png') }}" width="58" height="58"></a> </span>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="airShipBox orangeCir"><span class="borairCir"> <a data-toggle="tooltip" href="{{url('/administrator/servicelist/')}}" class="airCir" title="Service Management"><img src="{{ asset('public/administrator/img/icon03.png') }}" width="58" height="58"></a> </span>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="airShipBox RedCir"><span class="borairCir"> <a data-toggle="tooltip" href="{{url('/administrator/subscription/')}}" class="airCir" title="Subscription Management"><img src="{{ asset('public/administrator/img/icon02.png') }}" width="58" height="58"></a> </span>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="airShipBox yellGreeCir"><span class="borairCir"> <a data-toggle="tooltip" href="javascript:;" class="airCir" title="Quotation Requests"><img src="{{ asset('public/administrator/img/icon01.png') }}" width="58" height="58"></a> </span>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="airShipBox ltRedCir"><span class="borairCir"> <a data-toggle="tooltip" href="javascript:;" class="airCir" title="Orders"><img src="{{ asset('public/administrator/img/icon04.png') }}" width="58" height="58"></a> </span>
                                      </div>
                                  </li>
                              </ul> 
                            </div>
                          </div>
                        </div>
                    </div>

                      <div class="row"> 
                        <!-- Left col -->
                        <section class="col-lg-6 col-md-12"> 
                            <!-- Custom tabs (Charts with tabs)-->
                            <div class="row currentActive">
                              <div class="col-md-6 col-sm-6 col-xs-12">
                               <a href="{{url('/administrator/buyers/')}}"> <div class="info-box skyBlue"> <span class="info-box-icon desktop"><span class="glyphicon glyphicon-user"></span></span>
                                  <div class="info-box-content"> <span class="info-box-number f-S-27">{{$activeBuyers}} / {{$inactiveBuyers}}</span> <span class="info-box-text">Active / Inactive Buyer</span> </div>
                                </div></a>
                              </div>

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <a href="{{url('/administrator/sellers/')}}"><div class="info-box skyBlue"> <span class="info-box-icon mobile"><span class="glyphicon glyphicon-user"></span></span>
                                  <div class="info-box-content"> <span class="info-box-number f-S-27">{{$activeSellers}} / {{$inactiveSellers}}<!--<small>%</small>--></span> <span class="info-box-text">Active / Inactive Seller</span> </div>                                  
                                </div></a>                              
                              </div>

                              

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <a href="{{url('/administrator/sellers/')}}"><div class="info-box skyBlue"> <span class="info-box-icon mobile"><span class="glyphicon glyphicon-tasks"></span></span>
                                  <div class="info-box-content"> <span class="info-box-number f-S-27">{{$activeSevices}}<!--<small>%</small>--></span> <span class="info-box-text">{{ ($activeSevices > 1) ? 'Active Services':'Active Service' }}</span> </div>
                                </div></a>                              
                              </div>

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <a href="{{url('/administrator/sellers/')}}"><div class="info-box skyBlue"> <span class="info-box-icon mobile"><span class="glyphicon glyphicon-tasks"></span></span>
                                  <div class="info-box-content"> <span class="info-box-number f-S-27">0</span> <span class="info-box-text">Total Quotes</span> </div>
                                </div></a>                              
                              </div>

                            </div>

                        </section>
                        
                        <section class="col-lg-6 col-md-12"> 
                            
                             <div class="row currentActive">
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <a href="{{url('/administrator/sellers/')}}"><div class="info-box skyBlue"> <span class="info-box-icon mobile"><span class="glyphicon glyphicon-usd"></span></span>
                                  <div class="info-box-content"> <span class="info-box-number f-S-27">{{$totalFee}} USD<!--<small>%</small>--></span> <span class="info-box-text">Subscription Charges</span> </div>                                  
                                </div></a>                              
                              </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <a href="{{url('/administrator/sellers/')}}"><div class="info-box skyBlue"> <span class="info-box-icon mobile"><span class="glyphicon glyphicon-list-alt"></span></span>
                                    <div class="info-box-content"> <span class="info-box-number f-S-27">{{$activeSellers}}<!--<small>%</small>--></span> <span class="info-box-text">Booking Requests</span> </div>
                                    </div></a>                              
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <a href="{{url('/administrator/sellers/')}}"><div class="info-box skyBlue"> <span class="info-box-icon mobile"><span class="glyphicon glyphicon-list-alt"></span></span>
                                    <div class="info-box-content"> <span class="info-box-number f-S-27">0</span> <span class="info-box-text">Total Orders</span> </div>
                                    </div></a>                              
                                </div>
                              </div>

                              

                            
                        </section>
                        <!-- right col --> 
                    </div>


                    <div class="row"> 
                        <!-- Left col -->
          <section class="col-lg-6 col-md-12"> 
            <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Recent Buyers</h3>
                <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Company</th>
                    <th>Registered On</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(count($recentBuyers) > 0) { 
                  foreach($recentBuyers as $recentBuyer){
                  ?> 
                  <tr>
                    <td>{{$recentBuyer['firstName'].' '.$recentBuyer['lastName']}}</td>
                    <td>{{$recentBuyer['buyerType']}}</td>
                    <td>{{$recentBuyer['companyName']}}</td>
                    <td>{{$recentBuyer['createdOn']}}</td>
                  </tr>
                  <?php }} else { ?>
                  <tr>No record found</tr>
                  <?php
                  }
                  ?>                  
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix" style="">
              <a href="{{url('administrator/buyers/addedit')}}" class="btn btn-sm btn-info btn-flat pull-left">Add Buyer</a>
              <a href="{{url('administrator/buyers/')}}" class="btn btn-sm btn-default btn-flat pull-right">View All Buyers</a>
            </div>
            <!-- /.box-footer -->
          </div>
        </section>
                        
          <section class="col-lg-6 col-md-12"> 
            <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Top 10 Sellers</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Company</th>
                    <th>Registered On</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(count($topTenSellers) > 0) { 
                  foreach($topTenSellers as $topTenSeller){
                  ?> 
                  <tr>
                    <td>{{$topTenSeller['firstName'].' '.$topTenSeller['lastName']}}</td>
                    <td>{{$topTenSeller['sellerType']}}</td>
                    <td>{{$topTenSeller['companyName']}}</td>
                    <td>{{$topTenSeller['createdOn']}}</td>
                  </tr>
                  <?php }} else { ?>
                  <tr>No record found</tr>
                  <?php
                  }
                  ?>    
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix" style="">
              <a href="{{url('administrator/sellers/addedit')}}" class="btn btn-sm btn-info btn-flat pull-left">Add Seller</a>
              <a href="{{url('administrator/sellers/')}}" class="btn btn-sm btn-default btn-flat pull-right">View All Sellers</a>
            </div>
            <!-- /.box-footer -->
          </div>
          </section>
                        <!-- right col --> 
          <section class="col-lg-6 col-md-12"> 
            <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Top 10 services / products</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Company</th>
                    <th>Registered On</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr><td>No record found</td></tr>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix" style="">
              <a href="javascript:;" class="btn btn-sm btn-info btn-flat pull-left">Add Services</a>
              <a href="javascript:;" class="btn btn-sm btn-default btn-flat pull-right">View All Services</a>
            </div>
            <!-- /.box-footer -->
          </div>
        </section>

        <section class="col-lg-6 col-md-12"> 
            <div class="box box-info">
              <div class="box-header with-border">
              <h3 class="box-title">Recent Orders / Quotes</h3>
              <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                 
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body" style="">
                <div class="table-responsive">
                  <table class="table no-margin">
                    <thead>
                    <tr>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Company</th>
                      <th>Registered On</th>
                    </tr>
                    </thead>
                    <tbody>
                      <tr><td>No record found</td></tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix" style="">
                <a href="javascript:;" class="btn btn-sm btn-info btn-flat pull-left">Add Orders</a>
                <a href="javascript:;" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
              </div>
            </div>
        </section>
                                 
  </div>
</section>
                <!-- /.content --> 

<!-- Sparkline --> 
<script src="{{ asset('public/global/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script> 

<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('public/global/bower_components/jvectormap/jquery-jvectormap.css') }}">
<!-- jvectormap --> 
<script src="{{ asset('public/global/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script> 
<script src="{{ asset('public/global/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> 
<!-- jQuery Knob Chart --> 
<script src="{{ asset('public/global/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script> 
<!-- chart js -->
<script src="{{ asset('public/administrator/js/chartjs/Chart.bundle.js') }}"></script>
<script src="{{ asset('public/administrator/js/chartjs/utils.js') }}"></script>

<script src="{{ asset('public/administrator/controller-css-js/dashboardGraph.js') }}"></script>
<style type="text/css">
  .popover{
    width: 5%;
  }
</style>
<script>
  $(function() {
    $('[data-toggle="tooltip"]').tooltip({'placement':'top', 'trigger' : 'hover'}) 
  });  
</script>

@endsection



