<!--modal open-->
                <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{{$action}} Driver</h4>
                                </div>
                                <div class="modal-body">
                                   {{ Form::open(array('url' => 'administrator/driver/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Driver Name <span class="text-red">*</span></label>
                                                    <input id="driverName" name="driverName" class="form-control" required placeholder="Enter Driver Name" type="text" value="{{empty($driver->driverName) ? '': $driver->driverName}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Phone Number <span class="text-red">*</span></label>
                                                    <input id="contactNumber" name="contactNumber" class="form-control" maxlength="10" required placeholder="Enter Phone Number" type="text" onkeypress="return isNumberKey(event, this);"  value="{{empty($driver->driverPhone) ? '': $driver->driverPhone}}">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Company Name <span class="text-red">*</span></label>
                                                    <input id="companyName" name="companyName" class="form-control" required placeholder="Enter Company Name" type="text"  value="{{empty($driver->companyName) ? '': $driver->companyName}}">
                                                </div>
                                            </div>
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Contact Email </label>
                                                    <input id="contactEmail" name="contactEmail" class="form-control" placeholder="Enter Email Id" type="email"  value="{{empty($driver->contactEmail) ? '': $driver->contactEmail}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                                            </div>
                                        </div>
                                    {{Form::close()}}
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
        <!-- /.modal-dialog -->




<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>