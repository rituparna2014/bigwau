@extends('Administrator.layouts.master')
@section('content')

<section class="content">   
                 <div class="row m-b-15">
                 {{ Form::open(array('url' => 'administrator/driver/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
                        <div class="col-md-4"> 
                        <label>Entries per page</label>
                        <div class="adddrop m-l-15 w-100">
                           <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                                <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                                <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                                <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                                <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                                <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-8">
                            @if($canAdd == 1)<a href="javascript:void(0);" class="btn btn-info btn-sm pull-right" onclick="showAddEdit(0, {{$page}}, 'driver/addedit');"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Driver</a>@endif
                        </div>
                        <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
                        <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
                        {{FORM::close()}}
                    </div>
                    <div class="row"> 
                        <section class="col-lg-12"> 
                            <!-- Custom tabs (Charts with tabs)-->

                            <div class="box">
                                <div class="box-body">
                                    <div class="table-responsive">
                                    <table id="example2" class="table table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th data-sort="driverName" class="{{$sort['driverName']['current']}} sortby">Name</th>
                                                <th data-sort="driverPhone" class="{{$sort['driverPhone']['current']}} sortby">Phone Number</th>
                                                <th data-sort="companyName" class="{{$sort['companyName']['current']}} sortby">Company Name</th>
                                                <th data-sort="contactEmail">Contact Email</th>
                                                <th width="15%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         @if($driverData)
                                        @foreach ($driverData as $driver)
                                        @php 
                                        $deleteLink = url('administrator/driver/delete/'.$driver->id.'/'.$page); 
                                        @endphp
                                            <tr>
                                                <td>{{$driver->driverName}}</td>
                                                <td>{{$driver->driverPhone}}</td>
                                                <td>{{$driver->companyName}}</td>
                                                <td>{{$driver->contactEmail}}</td>
                                                <td>
                                                    @if($canEdit == 1)<a class="actionIcons text-green edit" onclick="showAddEdit({{$driver->id}}, {{$page}}, 'driver/addedit');"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit"></i></a>@endif
                                                    @if($canDelete == 1)<a class="actionIcons color-theme-2" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                                </td>
                                            </tr>
                                         @endforeach
                                           @endif     
                                        </tbody>
                                        
                                    </table>
                                    <div class="row mt-20">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <p class="results">Showing {{ $driverData->firstItem() . ' - ' . $driverData->lastItem() . ' of  ' . $driverData->total() }}</p>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                                {{ $driverData->links() }}
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.nav-tabs-custom --> 
                        </section>
                    </div>
                  
                   
                    
                    <!--modal open-->
                    <div class="modal fade" id="modal-addEdit">
               
                    </div>
                    <!--modal close-->
                </section>
                <!-- /.content -->             
@endsection