<script src = "{{ asset('public/administrator/controller-css-js/user.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            @if(isset($name) && isset($userAddress))
                <h4>{{ $name }} - {{$userAddress}}</h4>
                <hr>
            @endif
            <h4 class="modal-title">Shipment Settings</h4>
        </div>
        <div class="modal-body">
        @if(isset($userId) && $userId == '-1')
            <div class="row">
                <div class="col-sm-6">User Not Found</div>
            </div>
        @else
            <div class="row">
                @foreach($userSettingsFields as $eachFieldName => $eachSettingsFields)
                <div class="col-md-12">
                    <div class="form-group">                        
                        @if(array_key_exists($eachFieldName,$userSettingsInfo) && $userSettingsInfo[$eachFieldName] == "1")
                        <label class="text-red">Yes</label>
                        @else
                         <label>No</label>
                        @endif
                        <label>{{ $eachSettingsFields }}</label>
                    </div>
                </div>
                @endforeach
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Special Handling Instructions : </label>
                        <span>{{ (isset($userSettingsInfo['special_instruction']) && $userSettingsInfo['special_instruction']!='')?$userSettingsInfo['special_instruction']:"N/A" }}</span>
                    </div>
                </div>
            </div>
            
            
            <div>
                    <div class="row">
                            <div class="col-sm-4"><h3>Change History</h3></div>
                            <div class="col-sm-8">&nbsp;</div>
                    </div>
                    @php if(count($userSettingsHistory)>0){ @endphp
                    @foreach($userSettingsHistory as $eachHistory)
                    <div class="row">
                            <div class="col-sm-6">Setting modified on {{date('m/d/Y',strtotime($eachHistory['modifiedOn']))}} at {{date('H:i',strtotime($eachHistory['modifiedOn']))}}</div>
                            <div class="col-sm-6">&nbsp;</div>
                    </div>
                    @endforeach
                    @php } else { @endphp
                    <div class="row">
                        <div class="col-sm-6">No Record</div>
                    </div>
                    @php } @endphp
            </div>
        @endif
      
        @if(!empty($userFillnshipInfo) && isset($userFillnshipInfo))
             @foreach($userFillnshipInfo as $key=>$warehouse)
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Fill n Ship in warehouse - {{$userWarehouse[$key]}}</label>
                            <div class="idShow">
                                    <div>
                                        @foreach($warehouse as $shipment)
                                            <a href="{{url('administrator/fillnship/addedit/'.$shipment)}}">#{{$shipment}}</a>
                                        @endforeach
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @endif
        </div>
   
        <div class="modal-footer">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-info" name="submit" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>