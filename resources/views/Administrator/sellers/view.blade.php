@extends('Administrator.layouts.master')
@section('content')
<section class="content">

    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="container">
                                
                                <ul class="nav nav-pills">
                                    <li class="active"><a data-toggle="pill" href="#personal">Personal</a></li>
                                    <li><a data-toggle="pill" href="#banking">Banking</a></li>
                                    <li><a data-toggle="pill" href="#subscription">Subscription</a></li>
                                </ul>
                                  
                                <div class="tab-content">
                                    <div id="personal" class="tab-pane fade in active">
                                      <h4>Name</h4>
                                      <p>{{ $sellerDetails->firstName." ".$sellerDetails->lastName}}</p>
                                      <h4>Seller Type</h4>
                                      <p>{{ $sellerDetails->sellerType }}</p>
                                      <h4>Email</h4>
                                      <p>{{ $sellerDetails->email }}</p>
                                      <h4>Profile Description</h4>
                                      <p>{{ $sellerDetails->profileDescription }}</p>
                                      <h4>Birthday</h4>
                                      <p>{{ $sellerDetails->birthday }}</p>
                                      <h4>Phone</h4>
                                      <p>{{ $sellerDetails->phone }}</p>
                                      <h4>Timezone</h4>
                                      <p>{{ $sellerDetails->timezone }}</p>
                                      <h4>Nationality</h4>
                                      <p>{{ $sellerDetails->nationality }}</p>
                                      <h4>Profession</h4>
                                      <p>{{ $sellerDetails->profession }}</p>
                                      <h4>Annual Income</h4>
                                      <p>{{ $sellerDetails->annualIncome }}</p>
                                      <h4>Address</h4>
                                      <p>{{ $sellerDetails->address }}</p>
                                    </div>
                                    <div id="banking" class="tab-pane fade">
                                      <h4>IBAN</h4>
                                      <p>{{ $sellerDetails->IBAN }}</p>
                                      <h4>BIC</h4>
                                      <p>{{ $sellerDetails->BIC }}</p>
                                      <h4>Bank Owner Name</h4>
                                      <p>{{ $sellerDetails->bankOwnerName }}</p>
                                      <h4>Bank Owner Address</h4>
                                      <p>{{ $sellerDetails->bankOwnerAddress }}</p>
                                    </div>
                                    <div id="subscription" class="tab-pane fade">
                                      <h4>Subscription Amount</h4>
                                      <p>{{ $sellerDetails->fee }}</p>
                                      <h4>Payment Status</h4>
                                      <p>{{ $sellerDetails->paymentStatus }}</p>
                                      <h4>Subscription Start Date</h4>
                                      <p>{{ $sellerDetails->subscription_start_date }}</p>
                                      <h4>Subscription End Date</h4>
                                      <p>{{ $sellerDetails->subscription_end_date }}</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                        
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<script src="{{ asset('public/administrator/controller-css-js/buyers.js') }}"></script>

@endsection