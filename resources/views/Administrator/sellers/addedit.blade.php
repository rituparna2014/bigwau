@extends('Administrator.layouts.master')
@section('content')
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                <!-- <form id="addEditFrm" role="form" method="post" action="admin-roles.html"> -->
                @if(isset($seller))
                {{ Form::open(array('url' => 'administrator/sellers/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
                    <div class="box-body">
                        <div class="row">
                            
                            
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>First Name <span class="text-red">*</span></label>
                                    <input id="firstName" name="firstName" class="form-control" value="{{$seller->firstName}}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Last Name <span class="text-red">*</span></label>
                                    <input id="lastName" name="lastName" class="form-control" value="{{$seller->lastName}}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Seller Id <span class="text-red">*</span></label>
                                    <input  id="buyerId" name="buyerId" class="form-control" value="{{ $seller->sellerId }}" required placeholder="Enter Seller Id" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Seller Type <span class="text-red">*</span></label>
                                    <select name="buyerType" id="buyerType" class="form-control customSelect" required>                                         
                                    <option value="Person" {{ ($seller->sellerType == 'Person') ? "selected":"" }}>Person</option>
                                    <option value="Company" {{ ($seller->sellerType == 'Company') ? "selected":"" }} >Company</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Company <span class="text-red">*</span></label>
                                    <input  id="companyName" name="companyName" class="form-control" value="{{ $seller->companyName }}" required placeholder="Enter Company" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email Address <span class="text-red">*</span></label>
                                    <input  id="email" name="email" class="form-control" value="{{ $seller->email }}" required placeholder="Enter Email" type="email">
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password <span class="text-red">*</span></label>
                                    <input  id="password" name="password" class="form-control" value="" required placeholder="Enter Password" type="password">
                                </div>
                            </div>
                        </div> -->
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Profile Description </label>
                                    <textarea id="profileDescription" class="form-control" name="profileDescription" placeholder="Enter Profile Description">{{ $seller->profileDescription }}</textarea>
                                </div>
                            </div>
                        </div>
                         <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Birthday </label>
                                    <input id="birthday" name="birthday" class="form-control datepicker" placeholder="Enter birthday" type="text"  value="{{ !empty($seller->birthday)?\Carbon\Carbon::parse($seller->birthday)->format('m/d/Y'):''}} ">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Phone <span class="text-red">*</span></label>
                                    <input  id="phone" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="14" name="phone" class="form-control" value="{{ $seller->phone }}" required placeholder="Enter Phone" type="text">
                                </div>
                            </div>
                        </div>



                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Timezone </label>
                                    <input  id="timezone" name="timezone" class="form-control" value="{{ $seller->timezone  }}"  placeholder="Enter Timezone" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nationality </label>
                                    <input  id="nationality" name="nationality" class="form-control" value="{{ $seller->nationality }}"  placeholder="Enter Nationality" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Profession </label>
                                    <input  id="profession" name="profession" class="form-control" value="{{ $seller->profession }}"  placeholder="Enter Profession" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>IBAN <span class="text-red">*</span></label>
                                    <input  id="IBAN" name="IBAN" class="form-control" value="{{ $seller->IBAN }}" required placeholder="Enter IBAN" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>BIC <span class="text-red">*</span></label>
                                    <input  id="BIC" name="BIC" class="form-control" value="{{ $seller->BIC }}" required placeholder="Enter BIC" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Bank Owner Name <span class="text-red">*</span></label>
                                    <input  id="bankOwnerName" name="bankOwnerName" class="form-control" value="{{ $seller->bankOwnerName }}" required placeholder="Enter Bank Owner Name" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Bank Owner Address <span class="text-red">*</span></label>
                                    <input  id="bankOwnerAddress" name="bankOwnerAddress" class="form-control" value="{{ $seller->bankOwnerAddress }}" required placeholder="Enter Bank Owner Address" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Annual Income </label>
                                    <input  id="annualIncome" name="annualIncome" class="form-control" value="{{ $seller->annualIncome }}"  placeholder="Enter Annual Income" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Subscription <span class="text-red">*</span></label>
                                    <select class="customSelect form-control" name="fee" required="">
                                        @foreach($subscriptionList as $subscription)
                                        <option value="{{ $subscription->amount }}" {{ (($seller->fee) == $subscription->amount ? "selected" : "" )}}>{{ $subscription->term }}</option>
                                        @endforeach    
                                    </select>
                                </div>
                               <!--  <div class="form-group">
                                    <label>Fee </label>
                                    <input  id="fee" name="fee" class="form-control" value="{{ $seller->annualIncome }}"  placeholder="Enter Fee" type="text">
                                </div> -->
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address </label>
                                    <textarea id="address" class="form-control" name="address" placeholder="Enter Address">{{ $seller->address }}</textarea>
                                </div>
                            </div>
                        </div>
                        
                 
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <a href="{{ url('administrator/sellers') }}" class="btn btn-primary"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>&nbsp;
                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                    </div>
                <!-- </form> -->
                {{ Form::close() }}
                @endif

                @if(!isset($seller))
                <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                {{ Form::open(array('url' => 'administrator/sellers/add/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}

                    <div class="box-body">
                        <div class="row">
                          
                            
                            
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>First Name <span class="text-red">*</span></label>
                                    <input id="firstName" name="firstName" class="form-control" value="{{ old('firstName') }}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Last Name <span class="text-red">*</span></label>
                                    <input id="lastName" name="lastName" class="form-control" value="{{ old('lastName') }}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Seller Id <span class="text-red">*</span></label>
                                    <input  id="buyerId" name="buyerId" class="form-control" value="{{ old('buyerId') }}" required placeholder="Enter Seller Id" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Seller Type <span class="text-red">*</span></label>
                                    <select name="buyerType" id="buyerType" class="form-control customSelect" required>                                         
                                    <option value="Person" {{ (Input::old("buyerType") == 'Person' ? "selected":"") }}>Person</option>
                                    <option value="Company" {{ (Input::old("buyerType") == 'Company' ? "selected":"") }}>Company</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Company <span class="text-red">*</span></label>
                                    <input  id="companyName" name="companyName" class="form-control" value="{{ old('companyName') }}" required placeholder="Enter Company" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email Address <span class="text-red">*</span></label>
                                    <input  id="email" name="email" class="form-control" value="{{ old('email') }}" required placeholder="Enter Email" type="email">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password <span class="text-red">*</span></label>
                                    <input  id="password" name="password" class="form-control" value="{{ old('password') }}" required placeholder="Enter Password" type="password">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Profile Description </label>
                                    <textarea id="profileDescription" class="form-control" name="profileDescription" placeholder="Enter Profile Description">{{ old('profileDescription') }}</textarea>
                                </div>
                            </div>
                        </div>
                         <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Birthday </label>
                                     <input id="birthday" name="birthday" class="form-control datepicker" placeholder="Enter birthday" type="text"  value="{{ !empty(old('birthday')) ? old('birthday') : ''}}">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Phone <span class="text-red">*</span></label>
                                    <input  id="phone" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="14" name="phone" class="form-control" value="{{ old('phone') }}" required placeholder="Enter Phone" type="text">
                                </div>
                            </div>
                        </div>



                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Timezone </label>
                                    <input  id="timezone" name="timezone" class="form-control" value="{{ old('timezone') }}"  placeholder="Enter Timezone" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nationality </label>
                                    <input  id="nationality" name="nationality" class="form-control" value="{{ old('nationality') }}"  placeholder="Enter Nationality" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Profession </label>
                                    <input  id="profession" name="profession" class="form-control" value="{{ old('profession') }}"  placeholder="Enter Profession" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>IBAN <span class="text-red">*</span></label>
                                    <input  id="IBAN" name="IBAN" class="form-control" value="{{ old('IBAN') }}" required placeholder="Enter IBAN" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>BIC <span class="text-red">*</span></label>
                                    <input  id="BIC" name="BIC" class="form-control" value="{{ old('BIC') }}" required placeholder="Enter BIC" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Bank Owner Name <span class="text-red">*</span></label>
                                    <input  id="bankOwnerName" name="bankOwnerName" class="form-control" value="{{ old('bankOwnerName') }}" required placeholder="Enter Bank Owner Name" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Bank Owner Address <span class="text-red">*</span></label>
                                    <input  id="bankOwnerAddress" name="bankOwnerAddress" class="form-control" value="{{ old('bankOwnerAddress') }}" required placeholder="Enter Bank Owner Address" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Annual Income </label>
                                    <input  id="annualIncome" name="annualIncome" class="form-control" value="{{ old('annualIncome') }}"  placeholder="Enter Annual Income" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-15">
                            <div class="col-md-12">
                                
                                <div class="form-group">
                                    <label>Select Subscription <span class="text-red">*</span></label>
                                    <select class="customSelect form-control" name="fee" required="">
                                        @foreach($subscriptionList as $subscription)
                                        <option value="{{ $subscription->amount }}">{{ $subscription->term }}</option>
                                        @endforeach    
                                    </select>
                                </div>
                                <!-- <div class="form-group">
                                    <label>Fee </label>
                                    <input  id="fee" name="fee" class="form-control" value="{{ old('fee') }}"  placeholder="Enter Fee" type="text">
                                </div> -->
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address </label>
                                    <textarea id="address" class="form-control" name="address" placeholder="Enter Address">{{ old('address') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <a href="{{ url('administrator/sellers') }}" class="btn btn-primary"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>&nbsp;
                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                    </div>
                {{ Form::close() }}
                <!-- /.box-body -->
            </div>
            @endif


                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<script src="{{ asset('public/administrator/controller-css-js/buyers.js') }}"></script>
<script>
        $(function () {
            $("#addeditFrm").validate();
            $('.datepicker').datepicker({
                autoclose: true
             });
        });
        CKEDITOR.replace('editor2', {
        allowedContent: true,
//        toolbar: [
//            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
//            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
//            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
//        ]
    });
</script>
@endsection