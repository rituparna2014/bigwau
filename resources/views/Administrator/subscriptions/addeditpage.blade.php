@extends ('Administrator.layouts.master')
@section('content')
<script src="{{ asset('public/administrator/controller-css-js/emailtemplate.js')}}"></script>
<section class="content">
    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">

                @if($action == 'Add') 
                {{ Form::open(array('url' => 'administrator/addeditsubscriptionrecord/-1/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                @else  
                {{ Form::open(array('url' => 'administrator/addeditsubscriptionrecord/' . $record->id.'/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                @endif
                <div class="box-body">
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Subscription Term <span class="text-red">*</span></label>
                                <input id="term" name="term" required="" class="form-control" placeholder="Enter Subscription Term" type="text" value="{{ ($record->term!='') ? $record->term : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Amount (in USD)</label>
                                <input id="amount" name="amount" class="form-control" placeholder="Enter Amount" type="text" value="{{ ($record->amount!='') ? $record->amount : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->


                <div class="box-footer text-right">
                   
                    <a class="btn btn-info" href="{{ route('subscription') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<div class="modal fade" id="modal-addEdit"></div>
<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>


@endsection

<script>
$(function () {
    $("#addeditFrm").validate();
});
</script>