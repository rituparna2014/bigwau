<script src = "{{ asset('public/administrator/controller-css-js/menu.js') }}" ></script>
<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body"> 
            {{ Form::open(array('url' => 'administrator/menu/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Menu Position</label>
                        <select id="position" name="position" class="customSelect" onchange="displayPanel(this.value);" required>
                            <option value="0">Header</option>
                            <option {{(!empty($menu->position) && $menu->position=='1')?'selected="selected"':''}} value="1">Footer</option>
                        </select>                    
                    </div>
                </div>
            </div>
            <div id="menuPannelDiv" style="{{(!empty($menu->position) && $menu->position=='1')?'':'display: none;'}}" class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Menu Panel <span class="text-red">*</span></label>
                        <select id="pannel" name="pannel" class="customSelect" required>
                            <option value="">Select</option>
                            <option {{(!empty($menu->pannel) && $menu->pannel=='1')?'selected="selected"':''}}  value="1">First</option>
                            <option {{(!empty($menu->pannel) && $menu->pannel=='2')?'selected="selected"':''}}  value="2">Second</option>
                            <option {{(!empty($menu->pannel) && $menu->pannel=='3')?'selected="selected"':''}}  value="3">Third</option>
                            <!-- <option {{(!empty($menu->pannel) && $menu->pannel=='4')?'selected="selected"':''}}  value="4">Fourth</option> -->
                        </select>                         
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Menu Label <span class="text-red">*</span></label>
                        <input id="label" name="label" class="form-control" required value="@if(!empty($menu->label)){{$menu->label}}@endif" placeholder="Enter Label" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Link Type <span class="text-red">*</span></label>
                        <select id="linkType" name="linkType" class="customSelect" onchange="pageType(this.value);" required>
                            <option value="">Select</option>
                            <option {{(!empty($menu->linkType) && $menu->linkType=='1')?'selected="selected"':''}} value="1">External</option>
                            <option {{(!empty($menu->linkType) && $menu->linkType=='2')?'selected="selected"':''}} value="2">Internal</option>'
                            <option {{(!empty($menu->linkType) && $menu->linkType=='3')?'selected="selected"':''}} value="3">Block Content</option>
                        </select>                         
                    </div>
                </div>
            </div>
            <div id="menuLinkDev" style="{{(!empty($menu->linkType) && $menu->linkType=='1')?'':'display: none;'}}" class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Menu Link</label>
                        <input id="link" name="link" class="form-control" value="{{!empty($menu->link)?$menu->link:''}}" placeholder="Enter Link" type="text">
                    </div>
                </div>
            </div>
            <div id="pageListDiv" style="{{(!empty($menu->linkType) && $menu->linkType=='2')?'':'display: none;'}}" class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Page List <span class="text-red">*</span></label>
                        <select id="cmsPageLink" name="cmsPageLink" class="customSelect" required>
                            <option value="">Select</option>
                            <optgroup label="Static Pages">
                                @foreach($cmsPageList as $page) 
                                @if($page->pageType == 1)
                                <option {{(!empty($menu->cmsPageLink) && $menu->cmsPageLink==$page->slug)?'selected="selected"':''}} value="{{$page->slug}}">{{$page->pageHeader}}</option>
                                @endif
                                @endforeach
                            </optgroup>
                            <optgroup label="Landing Pages">
                                @foreach($cmsPageList as $page) 
                                @if($page->pageType == 2)
                                <option {{(!empty($menu->cmsPageLink) && $menu->cmsPageLink== $page->slug)?'selected="selected"':''}} value="{{$page->slug}}">{{$page->pageHeader}}</option>
                                @endif
                                @endforeach
                            </optgroup>

                        </select>                         
                    </div>
                </div>
            </div>
            <div id="blockListDiv" style="{{(!empty($menu->linkType) && $menu->linkType=='3')?'':'display: none;'}}" class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Block List <span class="text-red">*</span></label>
                        <select id="blockPageLink" name="blockPageLink" class="customSelect" required>
                            <option value="">Select</option>
                            @foreach($blockList as $block) 
                            <option {{(!empty($menu->cmsPageLink) && $menu->cmsPageLink==$block->slug)?'selected="selected"':''}} value="{{$block->slug}}">{{$block->blockTitle}}</option>
                            @endforeach
                            <option {{(!empty($menu->cmsPageLink) && $menu->cmsPageLink=='shipping-cost')?'selected="selected"':''}} value="shipping-cost">Get Shipping Cost</option>
                        </select>                         
                    </div>
                </div>
            </div>
            @if(!empty($menu->sort))
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Menu Order <span class="text-red">*</span></label>
                        <input id="sort" name="sort" required onkeypress="return isNumberKey(event, this);" class="form-control" value="@if(!empty($menu->sort)){{$menu->sort}}@endif" placeholder="Enter Meu Order" type="text">
                    </div>
                </div>
            </div>
            @else
            <input name="sort" value="0" type="hidden">
            @endif
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>