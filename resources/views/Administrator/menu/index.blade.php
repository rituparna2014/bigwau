@extends('Administrator.layouts.master')
@section('content')

<!-- Main content -->
<section class="content"> 
    <div class="row m-b-15">
        <div class="col-lg-9 col-md-9">
            {{ Form::open(array('url' => 'administrator/menu/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
            {{ Form::close() }}
        </div>
        <div class="col-lg-3 col-md-9 text-right">    
            @if($canAdd == 1)<a href="javascript:void(0)" class="btn btn-sm btn-info" onclick="showAddEdit(0, {{$page}}, 'menu/addedit');"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Menu</a>@endif
        </div>
    </div>                     
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <table id="example2"  class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-sort="label" class="{{$sort['label']['current']}} sortby">Menu Label</th>
                                <th data-sort="position" class="{{$sort['position']['current']}} sortby" >Menu Position</th>
                                <th>Menu Link</th>
                                <th data-sort="sort" class="{{$sort['sort']['current']}} sortby">Menu Order</th>
                                <th width="10%">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($menuData)
                            @foreach ($menuData as $menu)
                            @php 
                            $changeStatusLink = url('administrator/menu/changestatus/'.$menu->id.'/'.$page); 
                            $deleteLink = url('administrator/menu/delete/'.$menu->id.'/'.$page); 
                            @endphp
                            <tr>
                                <td>{{$menu->label}}</td>
                                <td>{{$menu->position=='1'?'Footer':'Header'}}</td>
                                <td>{{$menu->linkType=='1'?$menu->link:$menu->cmsPageLink}}</td>
                                <td>{{$menu->sort}}</td>
                                @if($canEdit == 1)
                                @if($menu->status == '1')
                                
                                <td><a data-toggle="confirmation" href="{{$changeStatusLink. '/0'}}" class="btn btn-success btnActive">Active</a></td>
                                @else 
                                <td><a data-toggle="confirmation" href="{{$changeStatusLink. '/1'}}" class="btn btn-danger btnActive">Inactive</a></td>
                                @endif
                                @endif
                                <td>
                                    @if($canEdit == 1)<a class="text-green edit actionIcons" data-toggle="tooltip" title="Click to Edit" onclick="showAddEdit({{$menu->id}}, {{$page}}, 'menu/addedit');"><i class="fa fa-fw fa-edit"></i></a>@endif
                                    @if($canDelete == 1)<a class="actionIcons color-theme-2" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="6">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $menuData->firstItem() . ' - ' . $menuData->lastItem() . ' of  ' . $menuData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $menuData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
</section>
<!-- /.content -->                    
@endsection