@extends('Administrator.layouts.master')
@section('content')

<section class="content">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/emailtemplate/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-md-12">
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
                <input type="hidden" name="field" id="field" />
                <input type="hidden" name="type" id="type" />
            </div>
            @if($canAdd == 1)<a href="{{url('administrator/emailtemplate/addedit')}}" class="btn btn-info pull-right btn-sm"><span class="Cicon"><i class="fa fa-plus"></i></span> Add Email Template</a>@endif
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            
        </div>
        {{ Form::close() }} 
    </div>

    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">                                   
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-sort="createdOn" class="{{$sort['createdOn']['current']}} sortby">Created On</th>
                                <th data-sort="templateKey" class="{{$sort['templateKey']['current']}} sortby">Template name</th>
                                <th width="15%">Subject</th>
                                <th width="15%">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($emailData)
                            @foreach ($emailData as $emailTemplate) 
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($emailTemplate->createdOn)->format('m/d/Y') }}</td>
                                <td>{{ !empty($emailTemplateKeysList[$emailTemplate->templateKey]) ? $emailTemplateKeysList[$emailTemplate->templateKey] : $emailTemplate->templateKey }}</td>
                                <td>{{ $emailTemplate->templateSubject }}</td>
                                <td>@if($canEdit == 1)
                                @if($emailTemplate->status=='1')
                                    <a data-toggle="tooltip" title="" class="btn btn-success btnActive updateStatusCommon" data-edit="{{ $emailTemplate->id }}" data-status="1" id="AL" data-original-title="Click to Change Staus">Active</a>
                                    @else
                                    <a data-toggle="tooltip" title="" class="btn btn-danger btnActive updateStatusCommon" data-edit="{{ $emailTemplate->id }}" data-status="0"  id="AL" data-original-title="Click to Change Staus">Inactive</a>
                                    @endif
                                    @endif
                                </td>
                                <td>
                                    @if($canEdit == 1)<a class="actionIcons text-green edit" href="{{url('administrator/emailtemplate/addedit/'.$emailTemplate->id.'/'.$page)}}"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit"></i></a>@endif
                                    @if($canDelete == 1)<a class="color-theme-2 actionIcons" href="{{ route('deleteEmailtemplate',['id' => $emailTemplate->id]) }}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                </td>
                            </tr> 
                            @endforeach 
                            @else 
                            <tr><td colspan="4">No record present</td></tr>
                            @endif
                        </tbody>

                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $emailData->firstItem() . ' - ' . $emailData->lastItem() . ' of  ' . $emailData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $emailData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->

</section>

@endsection