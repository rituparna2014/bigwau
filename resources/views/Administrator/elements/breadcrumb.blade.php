<section class="content-header">
    <h1> {{ $pageTitle }} 
    @if (!empty($contentTop['pageInfo']))
    	<small>{{ $contentTop['pageInfo'] }}</small>
   	@endif

    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('administrator/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        @if(is_array($contentTop['breadcrumbText']))
        	@for($i=0; $i< count($contentTop['breadcrumbText']); $i++)
        	<li class="{{ ($i==(count($contentTop['breadcrumbText'])-1)) ? 'active' : '' }}">{{ $contentTop['breadcrumbText'][$i] }}</li>
        	@endfor
        @else
        	<li class="active">{{ $contentTop['breadcrumbText'] }}</li>
        @endif
    </ol>
</section>