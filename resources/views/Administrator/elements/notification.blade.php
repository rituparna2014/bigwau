@if(session()->has('successMessage'))
<script type="text/javascript">
    $(function () {
        $('.notiification-holder').hide();
        setTimeout(function () {
            $(".notiification-holder").slideDown('slow');
            $('.notiification-holder').show();
        }, 500);
        setTimeout(function () {
            $(".notiification-holder").slideUp('slow');
        }, 3000);
    });
</script>

<div class="alert alert-success alert-dismissible notiification-holder">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      {{ session()->get('successMessage') }}
</div>
@endif
@if (session('errorMessage'))
<script type="text/javascript">
    $(function () {
        $('.notiification-holder').hide();
        setTimeout(function () {
            $(".notiification-holder").slideDown('slow');
            $('.notiification-holder').show();
        }, 500);
        setTimeout(function () {
            $(".notiification-holder").slideUp('slow');
        }, 3000);
    });
</script>
<div class="alert alert-danger notiification-holder">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    {{ session('errorMessage') }}
</div>

@endif
