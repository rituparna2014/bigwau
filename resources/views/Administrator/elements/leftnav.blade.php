<?php
    $end = '';
    $url = url()->current();
    $url_explode = explode('/', $url);
    $end = end($url_explode);
    if($end != ""){
        $activeClassMain = 'active';
    }
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <div id="menu_content">
        <section class="sidebar"> 
                        <ul class="sidebar-menu" data-widget="tree">
                            <!--<li class="header">MAIN NAVIGATION</li>-->
                            <li class="@if($end =='dashboard') {{$activeClassMain}} @endif"><a href="{{url('administrator/dashboard/')}}"> <i class="fa fa-dashboard"></i> <span>Dashboard</span>  </a></li>
                            
                            <li class="treeview {{ $end == 'adminusers' || $end == 'adminrole' ? 'active' : null }}">
                                <a href="#">
                                    <i class="fa fa-user"></i> <span>User Management </span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="treeview {{ $end == 'adminusers' ? 'active' : null }}">
                                         <li><a href="{{url('administrator/adminusers/')}}"><i class="fa fa-circle-o"></i> Admin Users</a></li>                                    
                                    </li>
                                    <li class="treeview {{ $end == 'adminrole' ? 'active' : null }}">
                                         <li><a href="{{url('administrator/adminrole/')}}"><i class="fa fa-circle-o"></i> Admin Roles</a></li>                                    
                                    </li>
                                                                          
                                </ul>
                            </li>
                            <li class="@if($end =='servicelist') {{$activeClassMain}} @endif"><a href="{{url('administrator/servicelist/')}}"> <i class="fa fa-asterisk"></i> <span>Service Management</span></a></li>

                            <li class="{{ $end == 'subscription' ? 'active' : null }}"><a href="{{url('administrator/subscription/')}}"> <i class="fa fa-money"></i> <span>Subscription Management</span>  </a></li>


                            <li class="treeview {{ $end == 'buyers' ? 'active' : null }}">
                                <a href="#">
                                    <i class="fa fa-user-circle"></i> <span>Manage Buyers</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/buyers/')}}"><i class="fa fa-circle-o"></i> Buyers Management</a></li>                                    
                                    </li>                                    
                                </ul>
                            </li>
                                <li class="treeview {{ $end == 'sellers' ? 'active' : null }}">
                                <a href="#">
                                    <i class="fa fa-user-circle"></i> <span>Manage Sellers</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/sellers/')}}"><i class="fa fa-circle-o"></i> Seller Management</a></li>                                    
                                    </li>  
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/sourcingpurposes/')}}"><i class="fa fa-circle-o"></i> Sourcing Purpose</a></li>                                    
                                    </li> 
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/resellitems/')}}"><i class="fa fa-circle-o"></i> Resell Items</a></li>
                                    </li> 
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/annualpurchase/')}}"><i class="fa fa-circle-o"></i> Annual Purchasing Value</a></li>                                    
                                    </li>
                                     <li class="treeview">
                                         <li><a href="{{url('administrator/sourceduration/')}}"><i class="fa fa-circle-o"></i> Source Duration</a></li>
                                    </li> 
                            </ul>
                            </li>
                            <li class="treeview {{ $end == 'pagelist' ? 'active' : null }}">
                                <a href="#">
                                    <i class="fa fa-sticky-note"></i> <span>Manage Pages</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/pagelist/')}}"><i class="fa fa-circle-o"></i> Page Management</a></li>                                    
                                    </li>                                    
                                </ul>
                            </li>
                            <li class="treeview {{ ($end == 'contentlist' || $end == 'settingscategories' || $end == 'settingscategories' || $end == 'emailtemplate' || $end == 'newsletter' ) ? 'active' : null }}">
                                <a href="#">
                                    <i class="fa fa-sticky-note-o"></i> <span>Manage Contents</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/contentlist/')}}"><i class="fa fa-circle-o"></i> Content Management</a></li>                                    
                                    </li>
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/settingscategories/')}}"><i class="fa fa-circle-o"></i> Service Category Management</a></li>                                    
                                    </li>
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/emailtemplate/')}}"><i class="fa fa-circle-o"></i> Email Template Management</a></li>                                    
                                    </li>
                                    <li class="treeview">
                                         <li><a href="{{url('administrator/newsletter/')}}"><i class="fa fa-circle-o"></i> Newsletter Management</a></li>                                    
                                    </li>                                      
                                </ul>
                            </li>
                            
                        
                            
                            
                        </ul>
                    </section>
    </div>
    <!-- /.sidebar --> 
</aside>
