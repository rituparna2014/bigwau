<?php
$firstName      = session()->get('admin_session_user_firstname');
$lastName       = session()->get('admin_session_user_lastname');
$userTypeName   = session()->get('admin_session_user_userTypeName');
$name = $firstName.' '.$lastName;
?>
<header class="main-header"> 
                <!-- Logo --> 
                <a href="javascript:void(0);" class="logo"> 
                    <!-- mini logo for sidebar mini 50x50 pixels --> 
                    <span class="logo-mini"><img src="{{ asset('public/administrator/img/logoBigwau.jpg') }}" height="24" width="24"/></span> 
                    <!-- logo for regular state and mobile devices --> 
                    <span class="logo-lg"><img src="{{ asset('public/administrator/img/logoBigwau.jpg') }}" height="45"/><b>Admin</b>LTE</span> </a> 
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top"> 
                    <!-- Sidebar toggle button--> 
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"> <span class="sr-only">Toggle navigation</span> </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{ asset('public/administrator/img/user2-160x160.jpg') }}" class="user-image" alt="User Image"> <span class="hidden-xs">{{$name}}</span> </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header"> <img src="{{ asset('public/administrator/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                                        <p> {{$name}} <small>{{$userTypeName}}</small> </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left"> <a href="{{ url('administrator/myProfile') }}" class="btn btn-default btn-flat">Profile</a> </div>
                                        <div class="pull-right"> <a href="{{ url('administrator/logout') }}" class="btn btn-default btn-flat">Sign out</a> </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                    <!-- search form -->
                 <!--    <div class="navSearch">
                        <form action="shipment.html" method="post" class="top-pan-search">
                            <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Please provide tracking ID">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn btn-flat noBg"><i class="fa fa-search"></i> </button>
                                </span> </div>
                        </form>
                    </div> -->
                    <!-- /.search form -->
                    <!-- <div class="trackID">
                        <div class="dropdown topDropDn">
                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Tracking ID<span class="caret skybg m-l-10"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Shipment ID</a></li>
                                <li><a href="#">Order ID</a></li>
                                <li><a href="#">Customer Name</a></li>
                            </ul>
                        </div>
                    </div> -->
                </nav>
            </header>