<!--modal open-->
<!-- <div class="modal fade" id="modal-changepwd"> -->
    
    <form>          
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <input type="hidden" name="base_path" value="{{ url('/').'/administrator/buyers/changepassword/'.$id.'/'.$page }}">
            <div class="modal-body">
                <!-- <p>A newly generated password will be sent to user. Continue ?</p> -->
                <div class="alert alert-danger print-error-msg" style="display:none"><ul></ul></div>
                <div class="alert alert-success print-success-msg" style="display:none"><ul></ul></div>
                <input type="hidden" name="userId" value="" id="userId">
                
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>New password <span class="text-red">*</span><span data-toggle="tooltip" title="Your password must be at least 6 characters long" class="toolTip"><i class="fa fa-question-circle"></i></span></label>
                      <input class="form-control" required placeholder="Enter new password" type="password" name="new_password">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>Confirm password <span class="text-red">*</span><span data-toggle="tooltip" title="Your password must be at least 6 characters long" class="toolTip"><i class="fa fa-question-circle"></i></span></label>
                      <input class="form-control" required placeholder="Confirm password" type="password" name="confirm_password">
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="password_yes" id="password_yes">      
                <button type="button" onclick="" id="password_submit"  class="btn btn-success" >Yes</button>
                
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</form>
    
    <!-- /.modal-dialog -->
<!-- </div> -->
<!--modal close-->
<script src="{{ asset('public/administrator/controller-css-js/adminusers.js') }}"></script> 