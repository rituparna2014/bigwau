@extends('Administrator.layouts.master')
@section('content')

<script src="{{ asset('public/administrator/controller-css-js/buyers.js') }}"></script>                
<!-- Main content -->
<div id="wait" style="display:none;" class="loaderMiddle"></div>

<style>

</style>
<div class="m-t-20" id="userroleApply" style="display:none">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>                            
            <p>Permissions applied successfully</p>
        </div>
    </div>
</div>
<section class="content" id="mainContentNotLoad"> 
    <div class="row m-b-15">
        <div class="col-lg-3 col-md-9">
            {{ Form::open(array('url' => 'administrator/buyers/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
            {{ Form::close() }}
        </div>
        <div class="col-lg-9 col-md-9 text-right">     
            <div class="btn-group actionGroup">
                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actions <span class="CiconR"><i class="fa fa-caret-down"></i></span>
                </button>
                <ul class="dropdown-menu">                    
                    <li role="separator" class="divider"></li>                    
                    <li><a href="javascript:void(0);" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modal-export">Export All</a> </li>
                    <li><a href="javascript:void(0);" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modal-export-selected">Export Selected</a> </li>
                </ul>
            </div>
            <div class="btn-group actionGroup">
                @if($canAdd == 1)<a class="btn btn-info btn-sm" href="{{ url('administrator/buyers/addedit') }}"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New {{substr($pageTitle,0,-1) }}</a>@endif
            </div>
            <a class="accordion-toggle btn-sm btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="Cicon"> <i class="fa fa-plus"></i></span> Advanced Search </a> 
        </div>
    </div>     
    <div class="col-md-12 m-t-15">
        <div class="row">
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="box">
                    <div class="box-body">
                        {{ Form::open(array('url' => 'administrator/buyers/index/', 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post','autocomplete'=>'off')) }}
                        <div class="form-row">
                            <div class="col-md-12">
                                <label>Date Period</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"> 
                                <!-- <label for="inputEmail4">Email</label>-->
                                <div class="withRdaioButtons" style="padding:15px 0 0 0;"> 
                                    <label>
                                        <input @isset($searchData['search_rangedate']) {{$searchData['search_rangedate']=='all'?'checked':''}} @endisset type="radio" name="search_rangedate" value="all" class="flat-red" onclick="show1();" checked>
                                                <span class="radioSpan">All dates</span> </label>
                                    <label>
                                        <input @isset($searchData['search_rangedate']) {{$searchData['search_rangedate']=='thismonth'?'checked':''}}  @endisset type="radio" name="search_rangedate" value="thismonth" class="flat-red" onclick="show1();">
                                                <span class="radioSpan">This month</span> </label>
                                    <label>
                                        <input @isset($searchData['search_rangedate']) {{$searchData['search_rangedate']=='thisweek'?'checked':''}} @endisset type="radio" name="search_rangedate" value="thisweek" class="flat-red" onclick="show1();">
                                                <span class="radioSpan">This week</span> </label>
                                    <label>
                                        <input @isset($searchData['search_rangedate']) {{$searchData['search_rangedate']=='today'?'checked':''}} @endisset type="radio" name="search_rangedate" value="today" class="flat-red" onclick="show1();">
                                                <span class="radioSpan">Today</span> </label>
                                    <label>
                                        <input @isset($searchData['search_rangedate']) {{$searchData['search_rangedate']=='custom'?'checked':''}} @endisset type="radio" name="search_rangedate" value="custom" class="flat-red customRange" onclick="show2();">
                                                <span class="radioSpan">Custom</span> </label>
                                </div>
                            </div>
                            @isset($searchData['search_rangedate']) 
                            @if($searchData['search_rangedate']=='custom')
                            <input type="hidden" value="custom" name="isCustomSelected" id="isCustomSelected">
                            @endif
                            @endisset
                            <div class="form-group col-md-6">
                                <div id="customDaterange" class="form-group dateRange">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon input-daterange"> <i class="fa fa-calendar"></i> </div>
                                            <input type="text" class="form-control pull-right" name="search_reservation" id="reservation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="sel1">First Name</label> 
                                <input class="form-control" placeholder="" value="@isset($searchData['search_firstName']) {{$searchData['search_firstName']}} @endisset" type="text" name="search_firstName">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Last Name</label> 
                                <input class="form-control" placeholder="" value="@isset($searchData['search_lastName']) {{$searchData['search_lastName']}} @endisset" type="text" name="search_lastName">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Company</label> 
                                <input class="form-control" placeholder="" type="text" value="@isset($searchData['search_companyName']) {{$searchData['search_companyName']}} @endisset" name="search_companyName">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Status</label>
                                <select class="form-control customSelect2" id="sel1" name="search_status">
                                    <option>All</option>
                                    <option @isset($searchData['search_status']) {{$searchData['search_status']=='1'?'selected':''}} @endisset value="1">Active</option>
                                    <option @isset($searchData['search_status']) {{$searchData['search_status']=='0'?'selected':''}} @endisset value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                       
                        <div class="form-row">
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-search"></i></span>Search</button>
                                <button type="button" onclick="location.href ='{{url('administrator/buyers/showall')}}'" class="btn btn-danger"><span class="Cicon"><i class="fa fa-refresh"></i></span>Show All</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <table id="example2"  class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="withCheck"><label><input type="checkbox" class="flat-red chk_all"></label></th>
                                <th>#</th>
                                <th data-sort="firstName" class="{{$sort['firstName']['current']}} sortby">Name</th>
                                <th data-sort="email" class="{{$sort['email']['current']}} sortby">Email Address</th>
                                <th data-sort="buyerType" class="{{$sort['buyerType']['current']}} sortby">Type</th>
                                <th data-sort="companyName" class="{{$sort['companyName']['current']}} sortby">Company</th>
                                <th>Registered On</th>
                                <th width="15%" data-sort="status" class="{{$sort['status']['current']}} sortby">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="checkboxes">
                            @if($buyersData)
                            @foreach ($buyersData as $buyer)
                            @php $url = url('administrator/buyers/changestatus/'.$buyer->id.'/'.$page);
                            @endphp
                            <tr>
                                <td class="withCheck"><label><input type="checkbox" name="checkboxselected" value="{{$buyer->id}}" class="flat-red checkbox"></label></td>
                                <td>{{$buyer->id}}</td>
                                <td>{{$buyer->firstName}} {{$buyer->lastName}}</td>
                                <td>{{$buyer->email}}</td>
                                <td>{{$buyer->buyerType}}</td>
                                <td>{{$buyer->companyName}}</td>
                                <td>{{$buyer->createdOn}}</td>
                                @if(Auth::user()->id != $buyer->id)
                                @if($buyer->status == 1)
                                <td><a data-toggle="confirmation" href="{{$url. '/0'}}" class="btn btn-success btnActive">Active</a></td>
                                @else 
                                <td><a data-toggle="confirmation" href="{{$url. '/1'}}" class="btn btn-danger btnActive">Inactive</a></td>
                                @endif
                                @else
                                <td><a class="btn btn-success btnActive">@if($buyer->status == 1) Active @else Inactive @endif</a></td>
                                @endif
                                <td>
                                    
                                   
                                    <a class="actionIcons text-warning" data-id="{{$buyer->id}}" onclick="showAddEdit({{$buyer->id}}, {{$page}}, 'buyers/changepassword');">
                                        <i data-toggle="tooltip" title="Click to Change Password" class="fa fa-key"></i>
                                    </a>

                                    <a  class="actionIcons emailNotify color-theme" data-userid = "{{$buyer->id}}" data-id="{{$buyer->email}}" onclick="showAddEditCustom({{$buyer->id}}, {{$page}}, 'buyers/notify');">
                                        <i data-toggle="tooltip" title="Click to Send Notification" class="fa fa-envelope-o"></i>
                                    </a>

                                    <a href="{{url('administrator/buyers/addedit')}}/{{$buyer->id}}" class="actionIcons text-green">
                                        <i data-toggle="tooltip" title="Click to Edit" class="fa fa-edit"></i>
                                    </a>

                                    <a href="{{url('administrator/buyerloginmatrices')}}/{{$buyer->id}}" class="actionIcons text-green">
                                        <i data-toggle="tooltip" title="Click to Edit" class="fa fa-address-book"></i>
                                    </a>

                                    
                                   
                                    
                                </td>

                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="8">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $buyersData->firstItem() . ' - ' . $buyersData->lastItem() . ' of  ' . $buyersData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $buyersData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <div class="modal fade" id="modal-addEditCustom"></div>

    <!--modal close-->
</section>
<!-- /.content -->  

<!--modal open-->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-red"><span class="text-red"><i class="fa fa-exclamation-triangle"></i></span> Are you sure?</h4>
            </div>
            <div class="modal-body">
                <p>The items will be deleted permanently. The action cannot be reversed.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--modal close-->

<!--modal open-->
<div class="modal fade" id="modal-export">
    {{ Form::open(array('url' => 'administrator/buyers/exportall/'.$page.'/', 'name' => 'exportAlls', 'id' => 'exportAlls', 'method' => 'post')) }}
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Export Buyers Data </h4>
            </div>
            <div class="modal-body">
                <div id="errorTxt" class="alert alert-danger" style="display:none;">

                </div>
                <h5 style="margin-bottom: 20px;">Choose fields to export</h5>

                <div class="row">                                        
                    <div class="col-md-6">
                        <!-- <div class="form-group">
                        <label><input type="checkbox" name="selectall[]" value="userId" class="flat-red checkbox"><span style="margin-left: 10px;">User ID</span></label>
                        </div>  -->
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="name" class="flat-red checkbox"><span style="margin-left: 10px;">Name</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="buyerId" class="flat-red checkbox"><span style="margin-left: 10px;">Buyer Id</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="buyerType" class="flat-red checkbox"><span style="margin-left: 10px;">Buyer Type</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="email" class="flat-red checkbox"><span style="margin-left: 10px;">Email Address</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="phone" class="flat-red checkbox"><span style="margin-left: 10px;">Phone</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="profileDescription" class="flat-red checkbox"><span style="margin-left: 10px;">Profile Description</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="birthday" class="flat-red checkbox"><span style="margin-left: 10px;">Birthday</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="timezone" class="flat-red checkbox"><span style="margin-left: 10px;">Timezone</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="nationality" class="flat-red checkbox"><span style="margin-left: 10px;">Nationality</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="profession" class="flat-red checkbox"><span style="margin-left: 10px;">Profession</span></label>
                        </div>  
                    </div>
                    <div class="col-md-6">                                        

                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="companyName" class="flat-red checkbox"><span style="margin-left: 10px;">Company</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="address" class="flat-red checkbox"><span style="margin-left: 10px;">Address</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="userType" class="flat-red checkbox"><span style="margin-left: 10px;">IBAN</span></label>
                        </div>                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="bankOwnerName" class="flat-red checkbox"><span style="margin-left: 10px;">Bank Owner Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="bankOwnerAddress" class="flat-red checkbox"><span style="margin-left: 10px;">Bank Owner Address</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="annualIncome" class="flat-red checkbox"><span style="margin-left: 10px;">Annual Income</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="fee" class="flat-red checkbox"><span style="margin-left: 10px;">Fee</span></label>
                        </div>                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="createdOn" class="flat-red checkbox"><span style="margin-left: 10px;">Registered On</span></label>
                        </div>                                                 
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" onclick="exportall_submit();" class="btn btn-success" data-dismiss="modal">Export</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>{{ Form::close() }}
    <!-- /.modal-dialog -->
</div>
<!--modal close--> 


<!--modal open-->
<div class="modal fade" id="modal-export-selected">
    {{ Form::open(array('url' => 'administrator/buyers/exportselected/'.$page.'/', 'name' => 'exportAllse', 'id' => 'exportAllse', 'method' => 'post')) }}
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Export Buyer </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="base_path_selected_export" value="{{ url('/').'/administrator/buyers/exportselected/1' }}">
                <div id="errorTxtse" class="alert alert-danger" style="display:none;">

                </div>
                <h5 style="margin-bottom: 20px;">Choose fields to export</h5>

                <div class="row">                                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="name" class="flat-red checkbox"><span style="margin-left: 10px;">Name</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="buyerId" class="flat-red checkbox"><span style="margin-left: 10px;">Buyer Id</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="buyerType" class="flat-red checkbox"><span style="margin-left: 10px;">Buyer Type</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="email" class="flat-red checkbox"><span style="margin-left: 10px;">Email Address</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="phone" class="flat-red checkbox"><span style="margin-left: 10px;">Phone</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="profileDescription" class="flat-red checkbox"><span style="margin-left: 10px;">Profile Description</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="birthday" class="flat-red checkbox"><span style="margin-left: 10px;">Birthday</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="timezone" class="flat-red checkbox"><span style="margin-left: 10px;">Timezone</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="nationality" class="flat-red checkbox"><span style="margin-left: 10px;">Nationality</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="profession" class="flat-red checkbox"><span style="margin-left: 10px;">Profession</span></label>
                        </div> 
                    </div>
                    <div class="col-md-6">                                        

                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="companyName" class="flat-red checkbox"><span style="margin-left: 10px;">Company</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="address" class="flat-red checkbox"><span style="margin-left: 10px;">Address</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="userType" class="flat-red checkbox"><span style="margin-left: 10px;">IBAN</span></label>
                        </div>                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="bankOwnerName" class="flat-red checkbox"><span style="margin-left: 10px;">Bank Owner Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="bankOwnerAddress" class="flat-red checkbox"><span style="margin-left: 10px;">Bank Owner Address</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="annualIncome" class="flat-red checkbox"><span style="margin-left: 10px;">Annual Income</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="fee" class="flat-red checkbox"><span style="margin-left: 10px;">Fee</span></label>
                        </div>                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="createdOn" class="flat-red checkbox"><span style="margin-left: 10px;">Registered On</span></label>
                        </div>                                                 
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" onclick="exportallse_submit();" class="btn btn-success" data-dismiss="modal">Export</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>{{ Form::close() }}
    <!-- /.modal-dialog -->
</div>
@endsection
