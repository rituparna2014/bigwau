<!--font-family: 'Roboto', sans-serif;-->
<style type="text/css">
    body {
        width: 100% !important;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        margin: 0;
        padding: 0;
        font-size: 12px;
        line-height: 1.4;
    }

    table td {
        border-collapse: collapse;
    }

    img {
        display: block;
        border: 0;
        outline: none;
    }

    a {
        outline: none;
        text-decoration: none;
    }
</style>
@php 
$asseturl = asset('/administrator/img/logo.png');
if (strpos($asseturl,'public') !== false) {
$asseturl = asset('/administrator/img/logo.png');
} else {
$asseturl = asset('/public/administrator/img/logo.png');
}
@endphp  
<div style="padding:15px; margin:0; font-size: 15px; border-bottom: 1px solid #e5e5e5">
<div>
  <img src="{{ $asseturl }}" style="padding:0; margin:0;" />
</div>
<div>{!! $pageContent !!}</div>
<div style="line-height: 1;"><i class="fa fa-check" style="display: block; float: left; margin-right: 10px; color: #43A047; line-height: 1;" aria-hidden="true"></i> I agree to terms and conditions of Shoptomydoor.com.</div>
<div align="right" valign="top" width="100%" style="padding:15px 15px 5px 15px; margin:0; color: #1b1b1b;">Signature</div>
<div align="right" valign="top" width="30%" style="padding:15px 15px 5px 15px; margin:0; font-size: 12px; color: #1b1b1b;">{{$signature}}</div>
</div>