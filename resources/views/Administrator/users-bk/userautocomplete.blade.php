<ul class="userData">
@if(count($userList)>0)
    @foreach($userList as $eachData)
        <li id="{{ $eachData->id }}">{{ $eachData->firstName.' '.$eachData->lastName }}</li>
    @endforeach
@else
    <li>No record found</li>
@endif
</ul>