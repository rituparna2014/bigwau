@extends('Administrator.layouts.master')
@section('content')


                <section class="content">
                    <div class="row">
                        <section class="col-lg-8 connectedSortable"> 
                            <!-- Custom tabs (Charts with tabs)-->

                            <div class="box"> 
                                <!--<div class="box-header">
                                                      <h3 class="box-title">Hover Data Table</h3>
                                                    </div>--> 
                                <!-- /.box-header -->
                                <form id="addEditFrm" role="form" method="post" action="admin-roles.html">
                                    <div class="box-body">
                                        <div class="row m-t-15">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>User Role <span class="text-red">*</span></label>
                                                    <input  id="company" name="company" class="form-control" placeholder="Enter ..." type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-header">
                                        <h3 class="box-title">Permission</h3>
                                    </div>
                                    <div class="box-body">
                                        <table class="table permissionTable">
                                            <thead>
                                                <tr>
                                                    <th>Menus</th>
                                                    <th colspan="4" width="70%">Actions</th>
                                                </tr>
                                                <tr>
                                                    <th width="30%">&nbsp;</th>
                                                    <th>view</th>
                                                    <th>add</th>
                                                    <th>edit</th>
                                                    <th>delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="firstRow">
                                                    <!--Dashboard-->
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Dashboard</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>
                                                    </td>
                                                </tr>
                                                <!--Content-->
                                                <tr class="firstRow">
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Content</span></label>
                                                    </td>

                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <!--Home Page Content-->
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Home Page Content</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Banners</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Block Content</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>

                                                <tr>
                                                    <!--Site Content-->
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Site Content </span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Static Pages</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Landing Pages</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Registration Banners</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Email Templates</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> SMS Templates</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Split Shipment Requests</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Warehouse Messages</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>

                                                <tr>
                                                    <!--Testimonials-->
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Testimonials</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Settings-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <!--Site Settings-->
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Site Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> General Configuration</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Countries</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> States</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Cities</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Destination Zones</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Locations</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Currencies</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Social Media</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Shipping Settings-->
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Shipping Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">General Configuration</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipping Methods</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Service Types</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Update Type</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Warehouse</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Site Categories</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Site Products</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Destination Ports</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>

                                                <!--Shipping Charges Settings-->
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipping Charges Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipping Charges</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipping Markups</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Other Shipment Charges</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Packaging Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Clearing Charges</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Insurance Charges</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Duty Charges</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Tax System</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Payment Settings-->
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Payment Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Rewards Settings-->
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Rewards Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Discounts</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Coupons</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Offers</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Users-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Users</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Admin</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Admin Roles</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:40px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Admin Users</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Users</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Fund User E-Wallet</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Shipments-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Shipments</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Manage Shipments</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Manage Drivers</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Schedule A Pick-up</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Track Shipment</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Consolidated Tracking</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Manage Delivery Companies</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Manage Dispatch Companies</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipment Locations - Rows</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipment Locations - Zones</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Orders-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Orders</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Manage Orders</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> E-Wallet</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Procurement-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Procurement</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Procurement Services</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Procurement Fees</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Fill & Ship-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Fill &amp; Ship</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Fill &amp; Ship Request Shipments</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Fill &amp; Ship Boxes</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Fill &amp; Ship Options</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>

                                                <!--Auto-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Auto</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Make Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Model Settings</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipping Cost</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipping Lines</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Shipping &amp; Clearing Quote</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">  Car Pickup Quote</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Auto Shipments</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Sailing Schedule</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Personal Items Clearing Charge</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Reports-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Reports</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Accounting Info</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Store History</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Warehouse Tracking</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Insurance</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Contact Us-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Contact Us</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Contact Us Reasons</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Contact Us Requests</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                                <!--Tools-->
                                                <tr class="firstRow">
                                                    <td style="padding-left:10px;">
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan">Tools</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left:10px;"><img src="assets/img/enter_row.png"/>
                                                        <label><input type="checkbox" name="r3" value="all" class="flat-red">
                                                            <span class="radioSpan"> Database Backup / Restore</span></label>
                                                    </td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                    <td><label><input type="checkbox" name="r3" value="all" class="flat-red"></label></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="box-footer text-right">
                                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                                    </div>
                                </form>
                                <!-- /.box-body --> 
                            </div>

                            <!-- /.nav-tabs-custom --> 
                        </section>
                    </div>
                    <!--Block 01--> 
                </section>
              
<script src="{{ asset('public/administrator/controller-css-js/adminrole.js') }}"></script>                
@endsection