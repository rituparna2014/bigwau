@extends('Administrator.layouts.master')
@section('content')


<section class="content">
                    <div class="row">
                        {{ Form::open(array('url' => 'administrator/adminrole/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
                        <div class="col-md-4"> 
                            <label>Entries per page</label>
                            <div class="adddrop m-l-15 w-100">
                                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
                        <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
                        {{ Form::close() }}
                        <!-- <div class="col-md-8 text-right"> 
                            <a href="#" class="btn btn-sm btn-info"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Role</a> 
                        </div> -->
                    </div>
                    <div class="row m-t-15">
                        <section class="col-lg-12"> 
                            <!-- Custom tabs (Charts with tabs)-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box">
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="table-responsive">
                                                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                                                <thead>
                                                                    <tr>
                                                                        <!-- <th width="2%" class="withCheck">
                                                                            <label>
                                                                                <input type="checkbox" class="flat-red chk_all">
                                                                            </label>
                                                                        </th> -->
                                                                        <th data-sort="userTypeName" class="{{$sort['userTypeName']['current']}} sortby">Name</th>
                                                                        <th width="15%">Status</th>
                                                                        <th width="15%">Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @if($adminRoleData)
                                                                    @foreach ($adminRoleData as $admintype)
                                                                    @php $url = url('administrator/adminrole/changestatus/'.$admintype->id);
                                                                    @endphp
                                                                    <tr>
                                                                        <!-- <td class="withCheck">
                                                                            <label>
                                                                                <input type="checkbox" class="flat-red checkbox">
                                                                            </label>
                                                                        </td> -->
                                                                        <td>{{$admintype->userTypeName}}</td>
                                                                        @if($canEdit == 1)
                                                                        @if($admintype->status == 1)
                                                                        <td><a data-toggle="confirmation" href="{{$url. '/0'}}" class="btn btn-success btnActive">Active</a></td>
                                                                        @else 
                                                                        <td><a data-toggle="confirmation" href="{{$url. '/1'}}" class="btn btn-danger btnActive">Inactive</a></td>
                                                                        @endif
                                                                        @endif
                                                                        <td>@if($canEdit == 1)
                                                                            <a href="{{url('administrator/permission/')}}/{{$admintype->id}}" class="actionIcons text-green">
                                                                                <i data-toggle="tooltip" title="Click to Edit" class="fa fa-edit"></i>
                                                                            </a>@endif
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach
                                                                    @else 
                                                                        <tr>
                                                                            <td colspan="3">No Record Found</td>
                                                                        </tr>
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-body --> 
                                        </div>
                                        <!-- /.box --> 
                                        <!-- /.box --> 
                                    </div>
                                    <!-- /.col --> 
                                </div>
                                <!-- /.nav-tabs-custom --> 
                        </section>
                    </div>
                    <!--Block 01--> 
                    

                </section>

<script src="{{ asset('public/administrator/controller-css-js/adminrole.js') }}"></script>                
@endsection