<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            @if(!isset($faqData))
            {{ Form::open(array('url' => 'administrator/faq/savedata/0/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            @else
            {{ Form::open(array('url' => 'administrator/faq/savedata/'.$faqId. '/' .$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            @endif            
            
            <div class="row">
                <div class="col-md-12" >
                    <div class="form-group">
                        <label>Question <span class="text-red">*</span></label>
                        <input id="question" name="question" required class="form-control" placeholder="Enter question" type="text" @if(isset($faqData)) value="{{$faqData->question}}" @endif>
                    </div>
                </div>
            </div>
                
            <div class="row">
                <div class="col-md-12" >
                    <div class="form-group">
                        <label>Answer <span class="text-red">*</span></label>
                        <textarea name="answer" rows="10" cols="80" id="editor1">@php if(isset($faqData->answer)) { @endphp {{$faqData->answer}} @php } else { @endphp Enter answer @php } @endphp</textarea>
                    </div>
                </div>
            </div>
            @if(isset($faqData))
            <div class="row">
                <div class="col-md-12" >
                    <div class="form-group">
                        <label>Display Order <span class="text-red">*</span></label>
                        <input id="displayOrder" name="displayOrder" required class="form-control" placeholder="Enter display order" type="text" @if(isset($faqData)) value="{{$faqData->displayOrder}}" @endif>
                    </div>
                </div>
            </div>
            @endif  
            

            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<script src="{{ asset('public/administrator/controller-css-js/faq.js') }}"></script>
<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>