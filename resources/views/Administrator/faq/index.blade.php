@extends('Administrator.layouts.master')
@section('content')

<section class="content">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/faq/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-lg-3 col-md-3"> 
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
        </div>
        {{ Form::close() }}
        @if($canAdd == 1)
        <div class="col-lg-9 col-md-9 text-right">
            <a href="javascript:void(0);" onclick="showAddEdit(0, {{$page}}, 'faq/addedit');" class="btn btn-info btn-sm"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New FAQ</a>
        </div>
        @endif
    </div>
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="30%" data-sort="question" class="{{$sort['question']['current']}} sortby">Question</th>
                                <th width="40%" data-sort="answer" class="{{$sort['answer']['current']}} sortby">Answer</th>
                                <th width="15%" data-sort="status" class="{{$sort['status']['current']}} sortby">Status</th>
                                <th class="text-center" width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($faqData)
                            @foreach ($faqData as $faq)
                            @php
                            $deleteLink = url('administrator/faq/delete/' . $faq->id . '/' . $page);
                            $url = url('administrator/faq/changestatus/'.$faq->id.'/'.$page);
                            @endphp
                            <tr>
                                <td>{{$faq->question}}</td>
                                <td>@php echo html_entity_decode($faq->answer); @endphp</td>
                                
                                @if($faq->status == 'Active')
                                <td><a data-toggle="confirmation" href="{{$url. '/Inactive'}}" class="btn btn-success btnActive">Active</a></td>
                                @else 
                                <td><a data-toggle="confirmation" href="{{$url. '/Active'}}" class="btn btn-danger btnActive">Inactive</a></td>
                                @endif
                                
                                <td class="text-center">
                                    @if($canEdit == 1)
                                    <a class="actionIcons text-green edit" data-toggle="modal" onclick="showAddEdit({{$faq->id}}, {{$page}}, 'faq/addedit');"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit"></i></a>
                                    @endif
                                    @if($canDelete == 1)
                                    <a class="actionIcons color-theme-2" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="3">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $faqData->firstItem() . ' - ' . $faqData->lastItem() . ' of  ' . $faqData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $faqData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->


    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit">
    </div>
    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-edit">
    </div>
    <!--modal close-->
</section>       
<!-- /.content -->             
@endsection