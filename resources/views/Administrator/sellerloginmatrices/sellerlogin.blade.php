@extends('Administrator.layouts.master')
@section('content')

<!-- Main content -->

<section class="content">
                    <div class="row m-b-15">
                        {{ Form::open(array('url' => 'administrator/notificationhistory/index/'.$getId, 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
                        <div class="col-md-4"> 
                            <label>Entries per page</label>
                            <div class="adddrop m-l-15 w-100">
                                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
                        <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
                        {{ Form::close() }}
                        <div class="col-md-8 text-right"> 
                            <a href="{{ url('administrator/sellers') }}" class="btn btn-sm btn-primary"><span class="Cicon"><i class="fa fa-arrow-left"></i></span> Back to Sellers</a> 
                        </div>
                    </div>
                    <div class="row"> 
                        <section class="col-lg-12 connectedSortable"> 
                            <!-- Custom tabs (Charts with tabs)-->

                            <div class="box">
                                <div class="box-body">
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Buyer ID</th>
                                                <th>Buyer Name</th>
                                                <th>Email</th>
                                                <th>IP</th>
                                                <th>Login Time</th>
                                                <th>Browser</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($notifyRecord)
                                            @foreach ($notifyRecord as $notify)
                                            <tr>
                                                <td>{{$notify->sellerId}}</td>
                                                <td>{{$notify->firstName.''.$notify->lastName}}</td>
                                                <td>{{$notify->email}}</td>
                                                <td>{{$notify->ip}}</td>
                                                <td>{{$notify->loginDatetime}}</td>
                                                <td>{{$notify->browser}}</td>
                                            </tr>

                                            <!--modal open-->
                                            <div class="modal fade" id="modal-view-{{$notify->notifyId}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">Message Sent</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p >@php echo html_entity_decode($notify->message); @endphp</p>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!--modal close-->


                                            @endforeach
                                            @else 
                                            <tr>
                                                <td colspan="2">No Record Found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->

                            </div>
                            <!-- /.nav-tabs-custom --> 
                            <div class="row mt-20">
                                                                <div class="col-md-6 col-sm-6 col-xs-6 for12">
                                                                    <p class="results">Showing {{ $notifyRecord->firstItem() . ' - ' . $notifyRecord->lastItem() . ' of  ' . $notifyRecord->total() }}</p>
                                                                </div>

                                                                <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                                                                    <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                                                        {{ $notifyRecord->links() }}
                                                                    </div>
                                                                </div>
                                                            </div>

                        </section>
                    </div>
                    <!--Block 01-->

                    
                </section>

<form name="formCheckAction" id="formCheckAction" action="<?php echo URL::to('administrator/adminUser'); ?>" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="checkedItemID" id="checkedItemID" value="" />
</form>  

<script src="{{ asset('/administrator/controller-css-js/notificationHistory.js') }}"></script>                
@endsection