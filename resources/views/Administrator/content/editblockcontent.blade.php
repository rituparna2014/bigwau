@extends ('Administrator.layouts.master')

@section('content')
<script src="{{ asset('public/administrator/controller-css-js/blockcontent.js')}}"></script>
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <input type="hidden" id="slug" value="{{ $slug }}" >
            <div class="box">
                {{ Form::open(array('url' => 'administrator/blockcontentupdate/' . $record->id, 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files' => true)) }}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Block Title</label>
                                <input id="blockTitle" name="blockTitle" class="form-control" placeholder="Enter ..." type="text" disabled="" value="{{ ($record->blockTitle!='') ? $record->blockTitle : '' }}">
                            </div>
                        </div>
                    </div>
                    @if($slug!='shop_for_me')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Content Title <span class="text-red">*</span></label>
                                <input id="blockTitle" name="contentTitle" class="form-control" placeholder="Enter ..." type="text" required="" value="{{ ($record->contentTitle!='') ? $record->contentTitle : '' }}">
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($slug=='how_it_works')
                    <div class="row" id="subTitle">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Content Sub Title <span class="text-red">*</span></label>
                                <input id="blockTitle" name="contentSubTitle" class="form-control" placeholder="Enter ..." type="text" required="" value="{{ ($record->contentSubTitle!='') ? $record->contentSubTitle : '' }}">
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($slug == 'why_shoptomydoor' || $slug == 'shop_for_me')
                    <div class="row" id="image">
                        <div class="col-md-6">
                            <label>Content Image</label>
                            <input id="contentImage" name="contentImage" class="form-control" type="file">
                        </div>
                        <div class="col-md-6">
                            @if($record->contentImage!='')
                            <img src="{{ url('public/uploads/blockcontent/'.$record->contentImage) }}" height="100px" width="100px">
                            @endif
                        </div>
                    </div>
                    @endif
                    @if($slug != 'value_added_service')
                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Block Content <span class="text-red">*</span></label>
                                <textarea id="editor2" class="ckeditor" name="content" rows="10" cols="80" required="">{!!html_entity_decode(stripslashes($record->content))!!}</textarea>
                            </div>
                        </div>
                    </div> 
                    @endif
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <a class="btn btn-info" href="{{ url('/administrator/blockcontent') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>
<scrip src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>
@endsection
<script>
$(function () {
    $("#addeditFrm").validate();
});
</script>