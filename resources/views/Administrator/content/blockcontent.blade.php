@extends('Administrator.layouts.master')
@section('content')

 <section class="content">
    <div class="row m-b-15">
        <div class="col-lg-6 col-md-6"> 
            {{ Form::open(array('url' => 'administrator/blockcontent', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option value="">All</option>
                    @foreach($allTypes as $eachTypeSlug => $eachTypeTitle)
                    <option value="{{ $eachTypeSlug }}" {{ ($search == $eachTypeSlug) ? "selected" : "" }}>{{ $eachTypeTitle }}</option>
                    @endforeach                   
                </select>
            </div>
            {{ Form::close() }}
        </div>
        <div class="col-lg-6 col-md-6 text-right">
            @if($canAdd == 1)<a class="btn btn-sm btn btn-info" href="{{ Route('addblockcontent') }}"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Block Content</a>@endif
        </div>
    </div>
   
    <div class="row"> 
        <section class="col-lg-12"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">                                   
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-sort="templateName" class="">Block Title</th>
                                <th>Content Title</th>
                                <th>Status</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                       @if (count($records) > 0 )
                             @foreach ($records as $record) 
                            <tr>
                                <td>{{ $record->blockTitle }}</td>
                                <td>{{ $record->contentTitle }}</td>               
                                <td>@if($canEdit == 1)
                                    @if($record->status=='1')
                                        <a data-toggle="tooltip" title="" class="btn btn-success btnActive updateStatus" data-edit="{{ $record->id }}" data-status="1" id="AL" data-original-title="Click to Change Staus">Active</a>
                                    @else
                                        <a data-toggle="tooltip" title="" class="btn btn-danger btnActive updateStatus" data-edit="{{ $record->id }}" data-status="0"  id="AL" data-original-title="Click to Change Staus">Inactive</a>
                                    @endif
                                @endif
                                </td>                                                
                                <td>
                                    @if($canEdit == 1)<a class="actionIcons text-green edit" href="{{ url('/administrator/blockcontentedit/'.$record->id.'/'.$record->slug) }}"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit"></i></a>@endif
                                    @if($record->slug == 'why_shoptomydoor')
                                        @if($canDelete == 1)<a class="actionIcons color-theme-2" href="{{ url('/administrator/blockcontent/delete/'.$record->id) }}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                    @endif    
                                </td>
                                
                            </tr> 
                          @endforeach 
                           @else 
                           <tr><td colspan="4">No record present</td></tr>
                          @endif
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->

</section>
<!--modal open-->
  <div class="modal fade" id="modal-addEdit"></div>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.updateStatus').on('click',function(e){
        e.preventDefault();
        var linkElem = $(this);
        var recordEditId = $(this).attr('data-edit');
        var baseUrl = $('#baseUrl').val();
        var recordStatus = $(this).attr('data-status');
        var postStatus = 1;
        if(recordStatus == 1)
            postStatus = 0;

        $.ajax({
            type: "get",
            dataType:'json',
            url: baseUrl+'/editblockcontentstatus/'+recordEditId+'/'+postStatus,
            data: {},
            success: function( msg ) {
                //console.log(msg.updated);
                if(msg.updated)
                {
                    //console.log('i am here');
                    if(recordStatus == 1)
                    {
                        linkElem.removeClass('btn-success');
                        linkElem.addClass('btn-danger');
                        linkElem.attr('data-status',0);
                        linkElem.text('Inactive');
                    }
                    else
                    {
                        linkElem.removeClass('btn-danger');
                        linkElem.addClass('btn-success');
                        linkElem.attr('data-status',1);
                        linkElem.text('Active');
                    }
                }
            }
        });
        

    });
  });
  </script>
  <!--modal close--> 
@endsection