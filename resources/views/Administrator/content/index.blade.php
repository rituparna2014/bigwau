@extends('Administrator.layouts.master')
@section('content')

<div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                  <div class="page-title">
                     <div class="title_left">
                         <h3>Users </h3>
                     </div>    
                  </div>
                    <div class="clearfix"></div>
                    <section class="advanced-search-sec newPanel row">
                        <div class="advanced-search-cont">
                             <?php echo Form::open(array('url' => 'administrator/content/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')); ?>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row mt-20">
                                    
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group advn-elemnt">
                                            <label class="control-label advn-elemnt">Status</label>
                                            <select name="searchByStatus" class="form-control">
                                                <option value="" <?php echo ($searchData['searchByStatus'] == '') ? 'selected' : ''; ?>>All</option>
                                            <option value="Active" <?php echo ($searchData['searchByStatus'] == 'Active') ? 'selected' : ''; ?>>Active</option>
                                            <option value="Inactive" <?php echo ($searchData['searchByStatus'] == 'Inactive') ? 'selected' : ''; ?>>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                          <div class="form-group advn-elemnt">
                                              <label class="control-label advn-elemnt">Search By</label>
                                              <select name="searchSelect" class="form-control">
                                                   <option value="name" <?php echo ($searchData['searchSelect'] == 'name') ? 'selected' : ''; ?>>Name</option>
                                                   <option value="link" <?php echo ($searchData['searchSelect'] == 'link') ? 'selected' : ''; ?>>Link</option>
                                              </select>
      
                                          </div>
                                      </div>
                                      <div class="col-md-4 col-xs-12 widthOutLabel">
                                          <input name="search" id="search" type="text" class="form-control" value=""  />
                                      </div>
                                </div>
                                  
                                     <div class="search-row row mt-20">
                                        <div class="col-md-4 col-md-6  col-xs-12">
                                            <div class="button-sec">
                                                <button type="submit" class="btn btn-success">Search</button>
                                                <a href="<?php echo URL::to('administrator/content/clear/'); ?>" class="btn btn-default" data-toggle = 'confirmation-search' title = 'Click to clear search parameters'>Show All</a>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                             </form>
                        </div>
                    </section>
                </div>    
            </div>
        </div>                  
                          

      <div class="clearfix"></div>
      <div class="x_title">
          <section class="newPanel row">
              <div class="row">
                  <div class="col-sm-4 col-xs-12">
                      <div class="col-sm-6  col-xs-12">
                          <div class="form-group advn-elemnt">
                              <label class="control-label advn-elemnt">Display</label>
                              <select name="searchDisplay" class="form-control" onchange="javascript:paginateData(this.value);">
                                        <option value="5"  <?php echo ($searchData['searchDisplay'] == '5') ? 'selected' : ''; ?>>5</option>
                                        <option value="10" <?php echo ($searchData['searchDisplay'] == '10') ? 'selected' : ''; ?>>10</option>
                                        <option value="15" <?php echo ($searchData['searchDisplay'] == '15') ? 'selected' : ''; ?>>15</option>
                                        <option value="20" <?php echo ($searchData['searchDisplay'] == '20') ? 'selected' : ''; ?>>20</option>
                                        <option value="25" <?php echo ($searchData['searchDisplay'] == '25') ? 'selected' : ''; ?>>25</option>
                                        <option value="30" <?php echo ($searchData['searchDisplay'] == '30') ? 'selected' : ''; ?>>30</option>
                              </select>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-8 col-xs-12">
                      <div class="pull-right">
                          <a onclick="confirmStatusAll('<?php echo URL::to('administrator/content/activeall/' . $page ); ?>')" class="btn btn-success btn"><i class="fa fa-unlock"></i> Active All</a>
                          <a onclick="confirmStatusAll('<?php echo URL::to('administrator/content/inactiveall/' . $page); ?>')" class="btn btn-warning btn"><i class=" fa fa-lock"></i> Inactive All</a>
                          <a href="<?php echo URL::to('administrator/content?page='); ?>" class="btn btn-info"><i class="fa fa-refresh"></i> Refresh</a>
                          <a href="<?php echo URL::to('administrator/content/addedit/'); ?>" class="btn btn-info"><i class="fa fa-plus"></i> Add</a> 
                      </div>
                  </div>
              </div>
          </section>
      </div>
        
      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">                                    
                  <div class="x_content">                                                                               
                      <table class="table table-striped table-hover table-bordered dt-responsive dataTable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" name="checkAll" id="checkAll" />
                                </th>
                                <th>
                                    <span class="sort-icons">
                                        <a href="javascript:void(0);">
                                            Name
                                        </a>
                                    </span>
                                </th>
                                <th>
                                    <span class="sort-icons">
                                        <a href="javascript:void(0);">
                                            Link
                                        </a>
                                    </span>
                                </th>
                                <th>
                                    <span class="sort-icons">
                                        <a href="javascript:void(0);">
                                            Modified On
                                        </a>
                                    </span>
                                </th>
                                <th>
                                    <span class="sort-icons">
                                        <a href="javascript:void(0);">
                                            Status
                                        </a>
                                    </span>
                                </th>

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($contentData as $content) { ?>
                                <tr>
                                    <td><input type="checkbox" id="c2<?php echo $content['id']; ?>" name="checkbox" class="ChkSelect" value="<?php echo $content['id']; ?>" /></td>
                                    <td><?php echo!empty($content['name']) ? $content['name'] : 'N/A'; ?></td>
                                    <td><?php echo!empty($content['link']) ? $content['link'] : 'N/A'; ?></td>
                                    <td><?php echo!empty($content['createdOn']) ? $content['createdOn'] : 'N/A'; ?></td>
                                    <td>
                                        <?php if ($content['status'] == 'Active') { ?>
                                            <a href="<?php echo URL::to('administrator/content/changestatus/' . $content['id'] . '/' . $page); ?>" class="btn btn-xs btn-success" data-toggle = 'confirmation' title = 'Click to make Inactive'><i class="fa fa-unlock"></i> Active</a>
                                        <?php } else { ?>
                                            <a href="<?php echo URL::to('administrator/content/changestatus/' . $content['id'] . '/' . $page); ?>" class="btn btn-xs btn-danger" data-toggle = 'confirmation' title = 'Click to make Active'><i class="fa fa-lock"></i> Inactive</a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo URL::to('administrator/content/addedit/' . $content['id'] . '/' . $page); ?>" class="btn btn-info btn-xs" data-toggle = 'confirmation' title = 'Click to update information'><i class="fa fa-edit"></i> Edit</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                  </div>
				  
				  <div class="row mt-20">
                    <div class="col-md-6 col-sm-6 col-xs-6 for12">
                        <div class="row">
                            <p class="results">Showing <?php echo $contentData->firstItem() . ' - ' . $contentData->lastItem() . ' of  ' . $contentData->total(); ?></p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 for12">
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                            {{ $contentData->links() }}
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>             
                          
</div>
 <form name="formCheckAction" id="formCheckAction" action="<?php echo URL::to('administrator/content'); ?>" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="checkedItemID" id="checkedItemID" value="" />
</form>                    
                        
 
@endsection