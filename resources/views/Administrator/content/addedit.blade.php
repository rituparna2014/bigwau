@extends ('Administrator.layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo!empty($pageTitle) ? $pageTitle : ''; ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">



                <?php echo Form::open(array('url' => 'administrator/content/save/' . $id . '/' . $page, 'id' => 'addEditForm', 'method' => 'post', 'class' => 'form-horizontal form-label-left', 'novalidate' => 'novalidate')); ?>

                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control col-md-7 col-xs-12"
                               name="name" value="<?php echo Input::old('name', !empty($content['name']) ? $content['name'] : ''); ?>"  id="name" required="required" placeholder="Enter Content Title" >
                    </div>
                </div>

                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Short Description <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="textarea"  name="shortDescription" rows="5" cols="20" id="shortDescription" required="required" class="form-control col-md-7 col-xs-12"
                                  placeholder="Enter Short Description"><?php echo Input::old('shortDescription', !empty($content['shortDescription']) ? $content['shortDescription'] : ''); ?></textarea>
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Description <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="description" name="description" class="ckeditor"><?php echo Input::old('description', !empty($content['description']) ? $content['description'] : ''); ?></textarea>
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Meta Title <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control col-md-7 col-xs-12" name="metaTitle" required="required"
                               value="<?php echo Input::old('metaTitle', !empty($content['metaTitle']) ? $content['metaTitle'] : ''); ?>" id="metaTitle"  placeholder="Enter Meta Title">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Meta Keyword <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control col-md-7 col-xs-12" name="metaKeywords" required="required"  id="metaKeywords"
                               value="<?php echo Input::old('metaKeywords', !empty($content['metaKeywords']) ? $content['metaKeywords'] : ''); ?>"   placeholder="Enter Meta Keywords" >
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Meta Description <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="textarea"  name="metaDescription" rows="5" cols="20" id="metaDescription" required="required"
                                  class="form-control col-md-7 col-xs-12" placeholder="Enter Meta Description"><?php echo Input::old('metaDescription', !empty($content['metaDescription']) ? $content['metaDescription'] : ''); ?></textarea>
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button id="send" type="submit" class="btn btn-success">Save</button>
                        <a href="{{@url('administrator/content?page='.$page)}}" class="btn btn-danger">Back</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('public/global/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>
@endsection