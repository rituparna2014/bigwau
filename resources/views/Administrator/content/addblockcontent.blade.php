@extends ('Administrator.layouts.master')

@section('content')
<script src="{{ asset('public/administrator/controller-css-js/blockcontent.js')}}"></script>
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                {{ Form::open(array('url' => route('saveblockcontent'), 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files' => true)) }}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Block</label>
                                <input id="slug" name="slug" class="form-control" placeholder="Enter ..." type="text" disabled="" value="Why Shoptomydoor">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Content Title <span class="text-red">*</span></label>
                                <input id="contentTitle" name="contentTitle" class="form-control" placeholder="Enter ..." type="text" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <label>Content Image <span class="text-red">*</span></label>
                            <input id="contentImage" name="contentImage" class="form-control" type="file" required="">

                        </div>
                    </div>
                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Content <span class="text-red">*</span></label>
                                <textarea id="editor2" name="content" class="ckeditor" rows="10" cols="80" required=""></textarea>
                            </div>
                        </div>
                    </div>                        
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <a class="btn btn-info" href="{{ url('/administrator/blockcontent') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>


<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>


@endsection

<script>
$(function () {
    $("#addeditFrm").validate();
});
</script>