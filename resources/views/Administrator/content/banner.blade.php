@extends('Administrator.layouts.master')
@section('content')
               
<section class="content">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/homepagebanner', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-lg-6 col-md-6"> 
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchDisplay=='10'?'selected':''}}>10</option>
                    <option {{$searchDisplay=='15'?'selected':''}}>15</option>
                    <option {{$searchDisplay=='20'?'selected':''}}>20</option>
                    <option {{$searchDisplay=='30'?'selected':''}}>30</option>
                    <option {{$searchDisplay=='40'?'selected':''}}>40</option>
                    <option {{$searchDisplay=='50'?'selected':''}}>50</option>
                </select>
            </div>
        </div>
        <input type="hidden" name="sort_order" id="sort_order" value="{{$sort_order}}" />
        
        {{ Form::close() }}
        <div class="col-lg-6 col-md-6 text-right">
            @if($canAdd == 1)<a class="btn btn-sm btn btn-info" onclick="showAddEdit('-1', {{$page}}, 'addedithomepagebanner');"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Banner</a>@endif
            @if($canDelete == 1)<a href="javascript:void(0)" id="delete-selected" onclick="deleteSelected();" class="btn btn-sm btn-danger"><span class="Cicon"><i class="fa fa-trash"></i></span> Delete Selected</a>@endif
        </div>
    </div>
    <div class="row"> 
        {{ Form::open(array('url' => 'administrator/bannerdeletemultiple/1', 'name' => 'frmsearch', 'id' => 'deleteAll', 'method' => 'post')) }}
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="2%" class="withCheck">
                                    <label>
                                        <input type="checkbox" class="chk_all flat-red">
                                    </label>
                                </th>
                                <th>Image / Video Preview</th>
                                <th>Embed Video URL</th>
                                <th class="sorting_{{ $sort_order }}"><span onclick="$('#frmsearch').submit();">Display Order</span></th>
                                <th>Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($bannerRecords)>0)
                        
                            @foreach($bannerRecords as $eachBanner)
                            <tr>
                                <td class="withCheck">
                                    <label>
                                        <input type="checkbox" name="deleteData[]" value="{{ $eachBanner->id }}" class="checkbox flat-red">
                                    </label>
                                </td>
                                <td><img 
                                    @php if (file_exists(public_path() . '/uploads/banner/'. $eachBanner->imagePath)) { @endphp 
                                    src="{{ asset('public/uploads/banner/'.$eachBanner->imagePath) }}" @php } 
                                    else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php } @endphp
                                    height="100px" width="150px"></td>
                                <td>
                                    @if($eachBanner->videoPath!='')
                                        {{ $eachBanner->videoPath }}
                                    @else
                                        N/A
                                    @endif

                                </td>
                                <td>{{ $eachBanner->displayOrder }}</td>
                                <td>
                                    @if($eachBanner->status=='1')
                                        @if($canEdit == 1)<a data-toggle="tooltip" title="" class="btn btn-success btnActive updateStatus" data-edit="{{ $eachBanner->id }}" data-status="1" id="AL" data-original-title="Click to Change Staus">Active</a>@endif
                                    @else
                                        @if($canEdit == 1)<a data-toggle="tooltip" title="" class="btn btn-danger btnActive updateStatus" data-edit="{{ $eachBanner->id }}" data-status="0"  id="AL" data-original-title="Click to Change Staus">Inactive</a>@endif
                                    @endif
                                </td>
                                
                                <td>
                                    @if($canEdit == 1)<a class="text-green edit actionIcons" data-toggle="tooltip" title="Click to Edit" onclick="showAddEdit({{$eachBanner->id}}, {{$page}}, 'addedithomepagebanner');"><i class="fa fa-fw fa-edit"></i></a>@endif
                                    @if($canDelete == 1)<a href="{{ url('/administrator/bannerdelete/'.$eachBanner->id.'/1/'.$page) }}" class="color-theme-2 actionIcons" data-delete="{{ $eachBanner->id }}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                </td>
                            </tr>
                            @endforeach
                            
                            @else
                            
                                <tr class="odd"><td valign="top" colspan="5" class="dataTables_empty" style="text-align: center;">No records found</td></tr>
                            @endif
                            
                            
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $bannerRecords->firstItem() . ' - ' . $bannerRecords->lastItem() . ' of  ' . $bannerRecords->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $bannerRecords->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
        {{ Form::close() }}
    </div>
    <!--Block 01-->

    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-red"><span class="text-red"><i class="fa fa-exclamation-triangle"></i></span> Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p>The items will be deleted permanently. The action cannot be reversed.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#deleteAll').submit();">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--modal close-->
</section>   

<script type="text/javascript">
    
    $(document).ready(function() {
        
        $('[data-toggle=confirmation]').confirmation({
              rootSelector: '[data-toggle=confirmation]',
              // other options
        });
        $('.updateStatus').on('click',function(e){
                e.preventDefault();
                var linkElem = $(this);
                var recordEditId = $(this).attr('data-edit');
                var baseUrl = $('#baseUrl').val();
                var recordStatus = $(this).attr('data-status');
                var postStatus = 1;
                if(recordStatus == 1)
                    postStatus = 0;

                $.ajax({
                    type: "get",
                    dataType:'json',
                    url: baseUrl+'/editbannerstatus/'+recordEditId+'/'+postStatus,
                    data: {},
                    success: function( msg ) {
                        //console.log(msg.updated);
                        if(msg.updated)
                        {
                            //console.log('i am here');
                            if(recordStatus == 1)
                            {
                                linkElem.removeClass('btn-success');
                                linkElem.addClass('btn-danger');
                                linkElem.attr('data-status',0);
                                linkElem.text('Inactive');
                            }
                            else
                            {
                                linkElem.removeClass('btn-danger');
                                linkElem.addClass('btn-success');
                                linkElem.attr('data-status',1);
                                linkElem.text('Active');
                            }
                        }
                    }
                });
                

            });
    });

    function deleteSelected() {
        if ($('.checkbox:checked').length == 0) {
            alert('Please select atleast one item!!');
        } else {
            $('#modal-default').modal('show');
        }
    }
</script>               
 
@endsection