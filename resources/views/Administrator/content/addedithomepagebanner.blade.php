<!--modal open-->
<script src="{{ asset('public/administrator/controller-css-js/emailtemplate.js')}}"></script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$action}} Banner</h4>
        </div>
        <div class="modal-body">
        @if($action == 'Add')
                    {{ Form::open(array('id'=>'data-form','url' => 'administrator/addbanner/1/'.$page,'files'=>true)) }}
        @else
                    {{ Form::open(array('id'=>'data-form','url' => 'administrator/editbanner/1/'.$bannerRecord->id.'/'.$page,'files'=>true)) }}
        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Type <span class="text-red">*</span></label>
                                    <select id="bannerType" required="" class="form-control customSelect" name="bannerType" onchange="showSelected(this.value)">
                                        <option value='image' {{ $bannerRecord->bannerType== 'video' ? '' : 'selected' }}>Image</option>
                                        <option value='video' {{ $bannerRecord->bannerType== 'video' ? 'selected' : '' }}>Video</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--<div id="imageData">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{ $bannerRecord->bannerType== 'video' ? 'Upload Video Preview Image' : 'Upload Banner Image' }} <span class="text-red">*</span></label>
                                        <input id="bannerImage" name="bannerImage" {{ ($action == 'Add') ? 'required' : ''}} type="file">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if($bannerRecord->imagePath!='')
                                        <img src="{{ asset('public/uploads/banner/'.$bannerRecord->imagePath) }}" height="100px" width="150px">
                                    @endif
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group" id="imageData">
                            <label>{{ $bannerRecord->bannerType== 'video' ? 'Upload Video Preview Image' : 'Upload Banner Image' }} </label>
                            <div class="main-img-preview">
                                @if($bannerRecord->imagePath =='')
                                <img id="storeIcon" class="thumbnail img-preview" src="{{ asset('public/administrator/img/default-no-img.jpg') }}" title="Preview Logo">
                                @else
                                <img id="storeIcon" class="thumbnail img-preview"
                                @php if (file_exists(public_path() . '/uploads/banner/'. $bannerRecord->imagePath)) {
                                @endphp 
                                src="{{ asset('public/uploads/banner/') }}/{{$bannerRecord->imagePath}}" @php } 
                                else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php } @endphp 
                                title="Preview Logo">
                                @endif
                                     <img id="loaded" src="#" alt="" style="display:none;">
                                <span class="closeImg"><i class="fa fa-times-circle"></i></span>
                            </div>
                            <div class="input-group">

                                <div class="input-group-btn">
                                    <div class="fileUpload btn btn-custom-theme fake-shadow fixedWidth111">
                                        <span>Change</span>
                                        <input id="logo-id" name="bannerImage" {{ ($action == 'Add') ? 'required' : ''}} type="file" class="attachment_upload">
                                    </div>
                                </div>
                                <!--<input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">-->
                            </div>
                            <p>Supported file types are: jpg, jpeg, png, gif<p>
                                        <p>Max upload limit: 4MB</p>
                                        <p>Min width: 1600px, Min height: 626px </p>
                        </div>

                        <div id="videoData" style="{{ $bannerRecord->bannerType== 'video' ? 'display: block;' : 'display : none' }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Video Embed URL <span class="text-red">*</span></label>
                                        <input id="vidURL" class="form-control" name="videoPath" type="text" value="{{ $bannerRecord->videoPath!='' ? $bannerRecord->videoPath : '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Display Order <span class="text-red">*</span></label>
                                    <input id="displayOrder" name="displayOrder" onkeypress="return isNumberKey(event, this);" maxlength="6" class="form-control" required placeholder="Enter display Order" type="text" value="{{ $bannerRecord->displayOrder!='' ? $bannerRecord->displayOrder : '' }}">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Banner Label</label>
                                    <textarea id="editor2" name="bannerContent" rows="10" cols="80">{{ ($bannerRecord->bannerContent!='') ? stripslashes($bannerRecord->bannerContent) : "" }}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <div class="text-right" id="homeBannerSubmit">
                                <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#vidURL").removeAttr('required');
        //$("#addeditFrm").validate();
    });

    function showSelected(selectedVal)
    {
        if(selectedVal == 'image')
        {
            //$("#imageData").show();
            $("#videoData").hide();
            $("#vidURL").removeAttr('required');
            $("#imageData label").html('Upload Banner Image <span class="text-red">*</span>');
            
        }
        else if(selectedVal == 'video')
        {
            //$("#imageData").hide();
            $("#videoData").show();
            $("#vidURL").attr('required','required');
            $("#imageData label").html('Upload Video Preview Image <span class="text-red">*</span>');
        }

    }

    //Promo Image upload validation
(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {},
            error: function () {}
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

$(function () {
    $('#logo-id').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
        success: function () {
            //alert('Success');
            $("#homeBannerSubmit").css('display', 'block');


        },
        error: function () {
            alert('Error! File type not supported');
            readURL('error');
            $('#loaded').css('display', 'none');
            $('#storeIcon').css('display', 'block');
            $("#homeBannerSubmit").css('display', 'none');
        }
    });



});

function readURL(input) {
    if (input != 'error') {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#loaded').css('display', 'block');
                $('#loaded').attr('src', e.target.result);
                $('#storeIcon').css('display', 'none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $('#loaded').css('display', 'none');
        $('#storeIcon').css('display', 'block');
    }
}


$("#logo-id").change(function () {
    //alert(this.files[0]);
    readURL(this);
});
</script>