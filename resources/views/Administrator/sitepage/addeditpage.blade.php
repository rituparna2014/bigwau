@extends ('Administrator.layouts.master')
@section('content')
<script src="{{ asset('public/administrator/controller-css-js/emailtemplate.js')}}"></script>
<section class="content">
    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">

                @if($action == 'Add') 
                {{ Form::open(array('url' => 'administrator/addeditpagerecord/-1/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                @else  
                {{ Form::open(array('url' => 'administrator/addeditpagerecord/' . $record->id.'/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                @endif
                <div class="box-body">
                    
                    @if($action == 'Edit')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Page Slug</label>
                                <input id="slug" name="slug" class="form-control" placeholder="Enter ..." type="text" disabled="" value="{{ ($record->slug!='') ? $record->slug : '' }}">
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Page Name <span class="text-red">*</span></label>
                                <input id="slug" name="pageHeader" required="" class="form-control" placeholder="Enter ..." type="text" value="{{ ($record->pageHeader!='') ? $record->pageHeader : '' }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label>Image</label>
                                <div class="main-img-preview">
                                    <img id="defaultLoaded" class="thumbnail img-preview" @php if(isset($record->pageBanner)) { 
                                         if (file_exists(public_path() . '/uploads/site_page/'. $record->pageBanner)) {
                                         @endphp 
                                         src="{{ asset('public/uploads/site_page/') }}/{{$record->pageBanner}}" @php } 
                                         else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php }

                                         } else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php } @endphp title="Preview Logo">
                                         <img id="loaded" src="#" alt="" style="display:none;">
                                    <span class="closeImg"><i class="fa fa-times-circle"></i></span>
                                </div>
                                <div class="input-group">

                                    <div class="input-group-btn">
                                        <div class="fileUpload btn btn-custom-theme fake-shadow fixedWidth111">
                                            <span>Upload Image</span>
                                            <input id="logo-id" name="logo" type="file" class="attachment_upload">
                                        </div>
                                    </div>
                                    <!--<input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">-->
                                </div>
                                <p>Supported file types are: jpg, jpeg, png, gif<p>
                                <p>Max upload limit: 4MB</p>
                                <p>Min width: 1600px, Min height: 626px </p>
                            </div>

                        </div>
                    </div>

                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Page Content <span class="text-red">*</span></label>
                                <textarea id="editor2" class="ckeditor" name="pageContent" rows="10" cols="80" required="">{{ ($record->pageContent!='') ? ($record->pageContent) : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Page Title</label>
                                <input id="blockTitle" name="pageTitle" class="form-control" placeholder="Enter ..." type="text" value="{{ ($record->pageTitle!='') ? $record->pageTitle : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Meta Keyword</label>
                                <textarea class="form-control" name="metaKeyword" rows="3" cols="80">{{ ($record->metaKeyword!='') ? stripslashes($record->metaKeyword) : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Meta Description</label>
                                <textarea class="form-control" name="metaDescription" rows="3" cols="80">{{ ($record->metaDescription!='') ? stripslashes($record->metaDescription) : '' }}</textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->


                <div class="box-footer text-right">
                   
                    <a class="btn btn-info" href="{{ route('pagelist') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<div class="modal fade" id="modal-addEdit"></div>
<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>


@endsection

<script>
$(function () {
    $("#addeditFrm").validate();
});
</script>