@extends('Administrator.layouts.master')
@section('content') 

<!-- Main content -->
<section class="content">
    <div class="row m-b-15"> {{ Form::open(array('url' => 'administrator/settingscategories', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-md-12 text-right">
            <div class="pull-left">
                <label>Entries per page</label>
                <div class="adddrop m-l-15 w-100">
                    <select class="form-control highLight" name="searchDisplay" onchange="$('#frmsearch').submit();">

                        @for($step = 10; $step<=50; $step+=10)

                        <option value="{{ $step }}" {{($param['searchDisplay'] == $step)?"selected" :""}}>{{ $step }}</option>

                        @endfor

                    </select>
                </div>
            </div>
            <div class="pull-left">
             <label>Search by Category</label>
            <div class="adddrop m-l-15 w-100">                             
                <select id="searchCategory" name="searchCategory" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option value="" {{$param['searchCategory']==''?'selected':''}}>All</option>
                    @foreach($categoryList as $category)
                    <option value="{{$category['id']}}" {{$param['searchCategory']==$category['id']?'selected':''}}>{{$category['categoryName']}}</option>
                    @endforeach
                </select>
            </div>
            </div>
            @if($canAdd == 1)<div class="text-right"> <a href="javascript:void(0)" class="btn btn-info btn-sm" onclick="showAddEdit('0', {{$page}}, 'addeditcategory/root-category');"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Categories</a> <a href="javascript:void(0)" class="btn btn-info btn-sm" onclick="showAddEdit('0', {{$page}}, 'addeditcategory/sub-category');"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Sub Categories</a> </div>@endif
            <input type="hidden" name="field" id="field" value="{{$param['sortField']}}" />
            <input type="hidden" name="type" id="type" value="{{$param['sortOrder']}}" />
            <input type="hidden" name="searchData" id="searchData" value="">
        </div>
        {{ Form::close() }} </div>
    <div class="row">
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">&nbsp;</div>
                        <div class="col-sm-6 text-right">
                            <div id="NY_filter" class="dataTables_filter form-inline">
                                <div class="form-group">
                                    <label class="m-">Search:</label>
                                    <input class="form-control input-sm" id="table-search" placeholder="Enter keyword to search" aria-controls="NY" type="search" value="{{ ($param['searchData']!='' ? $param['searchData'] : "" )}}">
                                    <a class="searchRtIcon" href="javascript:void(0)" onclick="$('#frmsearch').submit();"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="example2" class="table table-bordered table-hover countryName">
                        <thead>
                            <tr>
                                <th class="{{ ($param['sortField'] == 'categoryName')? 'sorting_'.$param['sortOrder'] :'' }} sorting sortby" data-sort="categoryName"> Category</th>
                                <th class="{{ ($param['sortField'] == 'parentCategoryId')? 'sorting_'.$param['sortOrder'] :'' }} sorting sortby" data-sort="parentCategoryId">Parent Category</th>
                                <th width="15%">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($records as $record)
                            <tr>
                                <td><a class="" style="cursor: pointer;" onclick="showAddEdit({{ $record->id }}, 'category', 'viewcategory/view-data');">{{ $record->categoryName }}</a></td>
                                @if(!empty($rootCategory[$record->parentCategoryId]))
                                    <td><a class="" style="cursor: pointer;" onclick="showAddEdit({{ $record->id }}, 'sub-category', 'viewcategory/view-data');">{{ (($record->parentCategoryId=='0') ? ' - ' : (!empty($rootCategory[$record->parentCategoryId]) ? $rootCategory[$record->parentCategoryId] : ' - ') )}}</a></td>
                                @else
                                    <td>{{ (($record->parentCategoryId=='0') ? ' - ' : (!empty($rootCategory[$record->parentCategoryId]) ? $rootCategory[$record->parentCategoryId] : ' - ') )}}</td>
                                @endif
                                <td>@if($canEdit == 1) @if($record->status=='1') <a data-toggle="tooltip" title="" class="btn btn-success btnActive updateStatusCommon" data-edit="{{ $record->id }}" data-status="1" id="AL" data-original-title="Click to Change Staus">Active</a> @else <a data-toggle="tooltip" title="" class="btn btn-danger btnActive updateStatusCommon" data-edit="{{ $record->id }}" data-status="0"  id="AL" data-original-title="Click to Change Staus">Inactive</a> @endif @endif</td>
                                <td>@if($canEdit == 1)<a class="text-green actionIcons" onclick="showAddEdit({{ $record->id }}, {{$page}}, 'addeditcategory/edit-data');"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit"></i></a> @endif
                                    @if(!in_array($record->id, $categoryIdExists))
                                    @if($canDelete == 1)<a class="color-theme-2 actionIcons" href="{{ url('/administrator/deletecategory/'.$record->id).'/'.$record->parentCategoryId.'/'.$page }}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif</td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $records->firstItem() . ' - ' . $records->lastItem() . ' of  ' . $records->total() }}</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate"> {{ $records->links() }} </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <input type="hidden" id="ajaxPage" value="editsitecategorystatus">
    <div class="modal fade" id="modal-addEdit"></div>
</section>
@endsection