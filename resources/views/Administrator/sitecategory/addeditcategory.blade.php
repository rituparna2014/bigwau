<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$action}} {{ ($section=='root-category') ? 'New Category' : 'New Sub Category' }}</h4>
        </div>
        <div class="modal-body">
            @if($action == 'Add')
            {{ Form::open(array('id'=>'addeditFrm','url' => 'administrator/addeditcategoryrecord/0/'.$page,'files'=>true)) }}
            @else
            {{ Form::open(array('id'=>'addeditFrm','url' => 'administrator/addeditcategoryrecord/'.$record->id.'/'.$page,'files'=>true)) }}
            @endif
            @if($section == 'root-category')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Category Name <span class="text-red">*</span></label>
                        <input id="catName" class="form-control" required="" name="categoryName" type="text" value="{{ $record->categoryName !='' ? $record->categoryName : '' }}" placeholder="Enter Category Name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Category Description <span class="text-red">*</span></label>
                        <textarea id="catDesc" class="form-control" required="" name="catDesc" placeholder="Enter Category Description...">{{ $record->categoryDesc !='' ? $record->categoryDesc : '' }}</textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" name="parentCategoryId" value="0">
            @elseif($section == 'sub-category') 
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Select Category <span class="text-red">*</span></label>
                        <!-- <select class="customSelect form-control" name="parentCategoryId" required="">
                            @foreach($rootCategory as $categoryId => $categoryName)
                            <option value="{{ $categoryId }}" {{ (($record->parentCategoryId) == $categoryId ? "selected" : "" )}}>{{ $categoryName }}</option>
                            @endforeach    
                        </select> -->
                        <select class="customSelect form-control" name="parentCategoryId" required="">                       
                        {!! $treeView !!}
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Sub-Category Name <span class="text-red">*</span></label>
                        <input id="name" name="categoryName" class="form-control" required placeholder="Enter Name..." value="{{ $record->categoryName !='' ? $record->categoryName : '' }}" type="text" placeholder="Enter Category Description">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Sub-Category Description <span class="text-red">*</span></label>
                        <textarea id="description" name="categoryDesc" class="form-control" required placeholder="Enter Sub-Category Description..." type="text">{{ $record->categoryName !='' ? $record->categoryName : '' }}</textarea>
                    </div>
                </div>
            </div>
            @endif                       
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>
