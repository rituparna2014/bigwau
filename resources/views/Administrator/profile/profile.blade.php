@extends('Administrator.layouts.master')
@section('content')

      
<section class="content">

                    <div class="row"> 
                        <section class="col-lg-8"> 
                            <!-- Custom tabs (Charts with tabs)-->
                            <!-- <form id="addEditFrm" class="form-horizontal" method="post" action="javascript:void(0);"> -->
                                @foreach($userRecord as $eachUserRecord)
                                {{ Form::open(array('url' => 'administrator/myProfile/'.$eachUserRecord->id, 'method' => 'PUT', 'class'=>'form-horizontal')) }}
                                

                                <!-- <div class="alert alert-info alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <strong></strong>
                                </div> -->
                                
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-dismissible" id="msgBox" >
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i>{{ $message }}</h4>
                                </div>
                                <!-- /.nav-tabs-custom -->
                                @endif
                                <ul>

                                @if(count($errors))
                                <div class="alert alert-warning alert-dismissible" id="msgBoxerror" >
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    @foreach($errors->all() as $error)<h4><i class="icon fa fa-warning"></i>{{ $error }}</h4>@endforeach
                                </div>
                                @endif

                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Details</h3> <a data-toggle="modal" data-target="#modal-add" class="btn btn-info m-l-15 pull-right btn-sm"><span class="Cicon"><i class="fa fa-lock"></i></span> Change Password</a>
                                    </div>
                                    <!-- /.box-header -->
                                    <input type="hidden" name="type" value="content">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6">First name </label>
                                                    
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input id="firstName" type="text" class="form-control" name="firstName"  placeholder="" value="{{$eachUserRecord->firstName}}">
                                                            
                                                       </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-6">Last name </label>
                                                    
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input id="lastName" type="text" class="form-control" name="lastName"  placeholder="" value="{{$eachUserRecord->lastName}}">
                                                            
                                                       </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label col-sm-6">Email <span data-toggle="tooltip" title="Make sure you enter a valid email address because the store will send you notifications to this address" class="toolTip"><i class="fa fa-question-circle"></i></span></label>
                                                    <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <input id="order_prefix" type="text" disabled style="background-color:#dddddd" class="form-control" name="email"  placeholder="" value="{{$eachUserRecord->email}}">
                                                        
                                                       </div>
                                                        
                                                    </div>
                                                </div>

                                                <div id="disabled_by_default">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6">Company </label>
                                                    <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <input id="paypal_api_username" type="text" class="form-control" name="company"  placeholder="" value="{{$eachUserRecord->company}}" >
                                                        
                                                       </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-6">Web site </label>
                                                    <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <input id="paypal_api_password" type="text" class="form-control" name="website"  placeholder="" value="{{$eachUserRecord->website}}" >
                                                        
                                                       </div>
                                                    </div>
                                                </div>
                                            </div>

                                                
                                        </div>
                                    </div>

                                    <!-- /.box-body -->
                                    <div class="box-footer text-right">
                                        <button type="submit" class="btn btn-success btn-sm"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Update</button>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                 
                            <!-- </form> -->
                            {{ Form::close() }}
                             @endforeach 

                        </section>
                    </div>
                    <!--Block 01-->
                </section>  

        <div class="modal fade" id="modal-add">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Change Password </h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger print-error-msg" style="display:none"><ul></ul></div>
                <div class="alert alert-success print-success-msg" style="display:none"><ul></ul></div>
              <!-- <form id="addFrm" role="form" method="post" action="profile.html"> -->
              <!-- @foreach($userRecord as $eachUserRecord)
              {{ Form::open(array('url' => 'administrator/myprofile/'.$eachUserRecord->id, 'method' => 'PUT', 'id' => 'addFrm')) }}
              @endforeach  -->
              <form>
                {{ csrf_field() }}
                <input type="hidden" name="type" value="pwd">
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>Old password <span class="text-red">*</span></label>
                      <input class="form-control" name="old_password" required placeholder="Enter old password" type="password">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>New password <span class="text-red">*</span><span data-toggle="tooltip" title="Your password must be at least 6 characters long" class="toolTip"><i class="fa fa-question-circle"></i></span></label>
                      <input class="form-control" required placeholder="Enter new password" type="password" name="new_password">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>Confirm password <span class="text-red">*</span><span data-toggle="tooltip" title="Your password must be at least 6 characters long" class="toolTip"><i class="fa fa-question-circle"></i></span></label>
                      <input class="form-control" required placeholder="Confirm password" type="password" name="confirm_password">
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="text-right">
                    <button type="submit" id="password_submit" class="btn btn-success btn-sm"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Update</button>
                  </div>
                </div>
              <!-- </form> -->
              {{ Form::close() }}
            </div>
          </div>
          <!-- /.modal-content --> 
        </div>
        <!-- /.modal-dialog --> 
      </div>   



      <script src="{{ asset('public/administrator/controller-css-js/profile.js') }}"></script>
@endsection
