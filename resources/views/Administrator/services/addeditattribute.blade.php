@extends ('Administrator.layouts.master')
@section('content')
<script src="{{ asset('public/administrator/controller-css-js/emailtemplate.js')}}"></script>
<section class="content">
    
    
    <div class="row m-b-15">
    <div class="col-lg-8">        
     <ul class="nav nav-pills">
      <li ><a href="{{ route('addeditservice',['id' => $id]) }}">Service Description</a></li>
      <li class="active"><a href="{{ route('addeditserviceattribute',['id' => $id]) }}">Attribute Details</a></li>        
     </ul>
    </div>
    </div>   
    <div class="row m-b-15">
        <div class="col-lg-8">
        <input type="button" class="btn btn-primary" value='Add' id='addButton'>
        <!-- <input type="button" class="btn btn-danger" value="Remove" id="removeButton"> -->
        </div>
    </div>
    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">

                
                  
                {{ Form::open(array('url' => 'administrator/addeditserviceattributerecord/' .$id.'/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                
                @if(count($records)>0)
                    @php
                     $counter = count($records);
                    @endphp

                @else
                    @php
                     $counter = 2;
                    @endphp
                @endif

                <input type="hidden" id="counter" name="counter" value="{{$counter}}">
                 
                <div class="box-body">
                    
                    <div id="TextBoxesGroup">

                    @if(count($records)>0)
                        @php
                        $count = 0;                       
                        @endphp
                        @foreach($records as $record)
                            @php                            
                            $divId = "TextBoxDiv".($count+1);
                            @endphp
                           <div class="row" id="{{$divId}}">                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Attribute Name <span class="text-red">*</span></label>
                                    <input  name="attributeName[]" required="" class="form-control" placeholder="Enter Attribute Name" type="text" value="{{$record->attributeName}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Attribute Values <span class="text-red">*</span></label>
                                    <input  name="attributeVal[]" required="" class="form-control" placeholder="Enter Values (for multiple use comma to separate)" type="text" value="{{$record->attributeVal}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                              <label>&nbsp;</label>
                              <div id="remove_div">
                                <a onclick="removeDiv({{($count+1)}});" >
                                  <span class="glyphicon glyphicon-trash"></span>
                                </a>
                              </div>
                              </div>
                            </div>
                            </div> 
                            @php                            
                            $count ++;
                            @endphp 
                        @endforeach
                    
                    @else
                         <div class="row" id="TextBoxDiv1">                        
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Attribute Name <span class="text-red">*</span></label>
                                    <input id="attributeName_1" name="attributeName[]" required="" class="form-control" placeholder="Enter Attribute Name" type="text" value="">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Attribute Values <span class="text-red">*</span></label>
                                    <input id="attributeVal_1" name="attributeVal[]" required="" class="form-control" placeholder="Enter Values (for multiple use comma to separate)" type="text" value="">
                                </div>
                            </div>
                            <div class="col-md-2">
                              <a href="void(0)" onclick="" >
                                  <span class="glyphicon glyphicon-trash"></span>
                              </a>
                            </div>
                        </div>  
                    @endif     
                   
                    </div>

                <div class="box-footer text-right">
                   
                    <a class="btn btn-info" href="{{ route('servicelist') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<div class="modal fade" id="modal-addEdit"></div>
<script src="{{ asset('public/administrator/controller-css-js/service.js')}}"></script>
<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>

<script>

function removeDiv(counterVal){
     if(counterVal==1){
          alert("No more textbox to remove");
          return false;
       }   
     
     var counter = $("#counter").val();   
     counter--;
            
        $("#TextBoxDiv" + counterVal).remove();
        $("#counter").val(counter);

    }  

$(document).ready(function(){

  counter = $('#counter').val();

  $("#addButton").click(function () {                
      
      counter++;  
      var newTextBoxDiv = $(document.createElement('div'))
           .attr("id", 'TextBoxDiv'+counter).attr("class", "row");
                  
      newTextBoxDiv.after().html('<div class="col-md-4"><div class="form-group"><label>Attribute Name <span class="text-red">*</span></label><input id="attributeName_'+counter+'" name="attributeName[]" required="" class="form-control" placeholder="Enter Attribute Name" type="text" value=""></div></div><div class="col-md-4"><div class="form-group"><label>Attribute Values <span class="text-red">*</span></label><input id="attributeVal_'+counter+'" name="attributeVal[]"required="" class="form-control" placeholder="Enter Values (for multiple use comma to separate)" type="text" value=""></div></div><div class="col-md-4"><div class="form-group"><label>&nbsp;</label><div id="remove_div"><a onclick="removeDiv('+counter+');" ><span class="glyphicon glyphicon-trash"></span></a></div></div></div>');
              
      newTextBoxDiv.appendTo("#TextBoxesGroup");
      $("#counter").val(counter);
        
  });

  $("#removeButton").click(function () {
      if(counter==1){
          alert("No more textbox to remove");
          return false;
      }   
      counter--;
      $("#TextBoxDiv" + counter).remove();
      $("#counter").val(counter);
            
  });
        
  $("#getButtonValue").click(function () {
      var msg = '';
      for(i=1; i<counter; i++){
        msg += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
      }
      alert(msg);
  });
});

</script>


@endsection
