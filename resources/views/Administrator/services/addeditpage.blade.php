@extends ('Administrator.layouts.master')
@section('content')
<script src="{{ asset('public/administrator/controller-css-js/emailtemplate.js')}}"></script>
<section class="content">

    
    @if($action != 'Add') 
        <div class="row m-b-15">
        <div class="col-lg-8">        
         <ul class="nav nav-pills">
          <li class="active"><a href="#">Service Description</a></li>
          <li><a href="{{ route('addeditserviceattribute',['id' => $record->id]) }}">Attribute Details</a></li>        
         </ul>
        </div>
        </div>
    @endif
    <div class="row">
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">

                @if($action == 'Add') 
                {{ Form::open(array('url' => 'administrator/addeditservicerecord/-1/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                @else  
                {{ Form::open(array('url' => 'administrator/addeditservicerecord/' . $record->id.'/', 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'files'=>'true')) }}
                @endif
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Service Name <span class="text-red">*</span></label>
                                <input id="serviceName" name="serviceName" required="" class="form-control" placeholder="Enter Service Name" type="text" value="{{ ($record->serviceName !='') ? $record->serviceName : '' }}">
                            </div>
                        </div>
                    </div>

                    <div  class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Seller</label>                               
                                <select name="sellerId" id="sellerId" class="customSelect form-control" required>
                                    <option value="0">Select Seller</option>
                                    @foreach($sellerList as $seller) 
                                    <option value="{{$seller['id']}}" {{ ($seller['id'] == $record->sellerId ) ? "selected":"" }} >{{$seller['firstName']." ".$seller['lastName']}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Service Category</label>                               
                                <select name="serviceCatId" id="serviceCatId" class="customSelect form-control" required >
                                    <option value="0">Select Category</option>
                                    {!! $treeView !!}
                                </select>
                            </div>
                        </div>
                    </div>
                    

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label>Image</label>
                                <div class="main-img-preview">
                                    <img id="defaultLoaded" class="thumbnail img-preview" @php if(isset($record->image)) { 
                                         if (file_exists(public_path() . '/uploads/service/'. $record->image)) {
                                         @endphp 
                                         src="{{ asset('public/uploads/service/') }}/{{$record->image}}" @php } 
                                         else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php }

                                         } else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php } @endphp title="Preview Logo">
                                         <img id="loaded" src="#" alt="" style="display:none;">
                                    <span class="closeImg"><i class="fa fa-times-circle"></i></span>
                                </div>
                                <div class="input-group">

                                    <div class="input-group-btn">
                                        <div class="fileUpload btn btn-custom-theme fake-shadow fixedWidth111">
                                            <span>Upload Image</span>
                                            <input id="logo-id" name="logo" type="file" class="attachment_upload">
                                        </div>
                                    </div>
                                    <!--<input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">-->
                                </div>
                                <p>Supported file types are: jpg, jpeg, png, gif<p>
                                <p>Max upload limit: 4MB</p>
                                <p>Min width: 1600px, Min height: 626px </p>
                            </div>

                        </div>
                    </div>

                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description <span class="text-red">*</span></label>
                                <textarea id="description" class="ckeditor" name="description" rows="10" cols="80" required="">{{ ($record->description!='') ? ($record->description) : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Created On <span class="text-red">*</span></label>
                                <input id="createdon" name="createdon" required="" class="form-control datepicker" placeholder="Enter Date" type="text" value="{{ !empty($record->createdOn)?\Carbon\Carbon::parse($record->createdOn)->format('m/d/Y'):''}}">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->


                <div class="box-footer text-right">
                   
                    <a class="btn btn-info" href="{{ route('servicelist') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<div class="modal fade" id="modal-addEdit"></div>
<script src="{{ asset('public/administrator/controller-css-js/service.js')}}"></script>
<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>


@endsection
