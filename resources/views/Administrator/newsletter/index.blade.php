@extends('Administrator.layouts.master')
@section('content')

<section class="content">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/newsletter/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-md-12">
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
                <input type="hidden" name="field" id="field" />
                <input type="hidden" name="type" id="type" />
            </div>
            @if($canAdd == 1)<a href="{{url('administrator/newsletter/addedit')}}" class="btn btn-info pull-right btn-sm"><span class="Cicon"><i class="fa fa-plus"></i></span> Add Newsletter</a>@endif
            <label class="m-l-15">Type</label>
            
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
        </div>
        {{ Form::close() }} 
    </div>

    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">                                   
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th data-sort="createdOn" class="{{$sort['createdOn']['current']}} sortby">Created On</th>
                                <th width="15%">Schedule Date</th>
                                <th width="15%">Schedule Status</th>
                                <th width="15%">Status</th>
                                <!-- <th width="15%">Send Message</th> -->
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @if ($newsletterData)
                            @foreach ($newsletterData as $newsletter)
                            @php
                                if($newsletter->scheduleStatus == 'Q')
                                    $scheduleStatus = "Queued";
                                else if($newsletter->scheduleStatus == 'S')
                                    $scheduleStatus = "Sent";
                                else
                                    $scheduleStatus = "Repeat";
                                
                            @endphp
                            @php 
                                $url = url('administrator/newsletter/editnewsletterstatus/'.$newsletter->id);
                                $delurl = url('administrator/newsletter/deletenewsletter/'.$newsletter->id);
                            @endphp
                            <tr>
                                <td>{{ $newsletter->newsletterName }}</td>
                                <td>{{ \Carbon\Carbon::parse($newsletter->createdOn)->format('m-Y-d H:i') }}</td>
                                <td>{{ $newsletter->scheduleDate }}</td>
                                <td>{{ $scheduleStatus }}</td>   
                                @if($canEdit == 1)
                                @if($newsletter->status=='1')
                                 <td><a data-toggle="confirmation" href="{{$url. '/0'}}" class="btn btn-success btnActive">Active</a></td>
                                @else 
                                <td><a data-toggle="confirmation" href="{{$url. '/1'}}" class="btn btn-danger btnActive">Inactive</a></td>
                                @endif
                                @endif                                            
                                <!-- <td>
                                    //@if($canEdit == 1)<a class="btn btn-success btn-sm" href="javascript:void(0);" onclick="showAddEdit({{$newsletter->id}}, 0, 'newsletter/sendmessage')"><i data-toggle="tooltip" title="Click to Send" class="fa fa-paper-plane"></i>Send</a>@endif
                                </td> -->
                                 <td>
                                    @if($canEdit == 1)<a class="actionIcons text-green edit" href="{{url('administrator/newsletter/addedit/'.$newsletter->id.'/'.$page)}}"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit"></i></a>@endif

                                     @if($canDelete == 1)<a class="color-theme-2 actionIcons" href="{{ $delurl }}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                    
                                </td>
                            </tr> 
                            @endforeach 
                            @else 
                            <tr><td colspan="4">No record present</td></tr>
                            @endif
                        </tbody>

                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $newsletterData->firstItem() . ' - ' . $newsletterData->lastItem() . ' of  ' . $newsletterData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $newsletterData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
</section>

@endsection