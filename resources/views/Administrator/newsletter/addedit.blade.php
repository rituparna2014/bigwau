@extends ('Administrator.layouts.master')

@section('content')
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="{{ asset('public/administrator/controller-css-js/newsletter.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                             
                {{ Form::open(array('url' => 'administrator/newsletter/save/' . $id . '/' . $page, 'name' => 'frmsearch', 'id' => 'addeditFrm', 'method' => 'post', 'novalidate' => 'novalidate')) }}
                <div class="box-body">
                   
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Email Template Key</label>                               
                                <select name="emailtemplate" id="emailtemplate" class="customSelect form-control" required>
                                    <option value="0">Select Template</option>
                                    @foreach($emailTemplate as $template) 
                                    <option value="{{$template['id']}}" {{ (!empty($id) && $content->emailTemplateKey == $template['id']) ? 'selected' : '' }} >{{$template['templateKey']}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Newsletter Name</label>
                                <input id="newsletterName" name="newsletterName" class="form-control" placeholder="Enter newsletter name" type="text" required="" value="{{ !empty($content['newsletterName']) ? $content['newsletterName'] : ''}}">
                            </div>
                        </div>
                    </div>
                    <div  class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Schedule Newsletter Type</label>                               
                                <select name="scheduleNewsletterType" id="scheduleNewsletterType" class="customSelect form-control" required>
                                    <option value="0">Select Newsletter Type</option>
                                    <option value="1" {{ (!empty($id) && $content->scheduleNewsletterType == 1) ? 'selected' : '' }}>Yearly</option>
                                    <option value="2" {{ (!empty($id) && $content->scheduleNewsletterType == 2) ? 'selected' : '' }}>Monthly</option>
                                    <option value="3" {{ (!empty($id) && $content->scheduleNewsletterType == 3) ? 'selected' : '' }}>Weekly</option>
                                    <option value="4" {{ (!empty($id) && $content->scheduleNewsletterType == 4) ? 'selected' : '' }}>Daily</option>
                                    <option value="5" {{ (!empty($id) && $content->scheduleNewsletterType == 5) ? 'selected' : '' }}>Specific Date</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="{{(!empty($id) && $content->scheduleNewsletterType != 4)?'display:block':'display:none'}}" id="scheduleDatediv">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Schedule Date</label>
                                <input id="scheduleDate" name="scheduleDate" class="form-control datepicker" placeholder="Enter schedule date" type="text" value="{{ !empty($content['scheduleDate']) ? $content['scheduleDate'] : ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="{{(!empty($id) && $content->scheduleNewsletterType == 4)?'display:block':'display:none'}}" id="scheduleTimediv">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Schedule Time</label>
                                <input id="scheduleTime" name="scheduleTime" class="form-control datepicker" placeholder="Enter schedule time" type="text" value="{{ !empty($content['scheduleTime']) ? $content['scheduleTime'] : ''}}">
                            </div>
                        </div>
                    </div>
                        
                    <div class="row" id="scheduleRepeatDiv" style="{{(!empty($id) && $content->scheduleNewsletterType != 4)?'display:block':'display:none'}}"> 
                        <div class="col-md-6 col-xs-12">                            
                            <div class="form-group">
                                <label>Schedule Repeat : </label>
                                <label> Yes <input id="scheduleRepeat" name="scheduleRepeat" class="flat-red" style="position: absolute; opacity: 0;" type="radio" required="" value="1"
                                    {{ (!empty($id) && $content->scheduleRepeat == 1) ? 'checked' : '' }} ></label>
                                <label> No <input id="scheduleRepeat" name="scheduleRepeat" class="flat-red" style="position: absolute; opacity: 0;" type="radio" required="" value="0" {{ (!empty($id) && $content->scheduleRepeat == 0) ? 'checked' : '' }}></label>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row" id="scheduleRepeatDiv"> 
                        <div class="col-md-6 col-xs-12">                            
                            <div class="form-group">
                                <label>Send Newsletter To : </label><br>
                                <label>  <input id="" name="sendNewsletter" class="flat-red clicktoSend" style="position: absolute; opacity: 0;" type="radio" required="" value="subscriber">&nbsp;&nbsp;Subscribers</label>
                                <label>  <input id="" name="sendNewsletter" class="flat-red clicktoSend" style="position: absolute; opacity: 0;" type="radio" required="" value="seller">&nbsp;&nbsp;Sellers</label>
                                <label>  <input id="" name="sendNewsletter" class="flat-red clicktoSend" style="position: absolute; opacity: 0;" type="radio" required="" value="buyer">&nbsp;&nbsp;Buyers</label>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="scheduleRepeatDiv"> 
                        <div class="col-md-12 col-xs-12">  
                            <ul class="scheduleRepeatList">
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 1 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 2 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 3 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 4 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 5 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 6 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 7 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 8 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 9 </label></li>
                                <li><input id="" name="" class="flat-red" style="position: absolute; opacity: 0;" type="checkbox" required="">&nbsp;&nbsp;<label> subscriber 10 </label></li>
                            </ul>                          
                        </div>
                    </div> -->
                    <!-- <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Subscribers : </label>
                            <input id="searchSubs" name="searchSubs" size="50" class="form-control"/>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Sellers : </label>
                            <input id="searchSellers" name="searchSellers" size="50" class="form-control"/>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Buyers : </label>
                            <input id="searchBuyers" name="searchBuyers" size="50" class="form-control"/>
                        </div>
                      </div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select Subscribers :</label>   <br>                            
                                <select name="searchSubs[]" id="example-enableCaseInsensitiveFiltering-subs" multiple="multiple" class="customSelect form-control" required>
                                    @foreach ($subscribers as $sub)
                                        <option value="{{$sub }}" checked>{{$sub}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select Sellers :</label>   <br>                            
                                <select name="searchSellers[]" id="example-enableCaseInsensitiveFiltering-sellers" multiple="multiple" class="customSelect form-control" required>
                                    @foreach ($sellers as $sel)
                                        <option value="{{$sel}}" >{{$sel}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select Buyers :</label>   <br>                            
                                <select name="searchBuyers[]" id="example-enableCaseInsensitiveFiltering-buyers" multiple="multiple" class="customSelect form-control" required>
                                    @foreach ($buyers as $buy)
                                        <option value="{{$buy}}">{{$buy}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                 </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <input type="hidden" name="baseUrl" id="baseUrl" value="{{ url('/') }}">
                <input type="hidden" name="scubscriberData" id="scubscriberData" value="{{ ($subscribers) }}">
                <input type="hidden" name="sellerData" id="sellerData" value="{{ ($sellers) }}">
                <input type="hidden" name="buyerData" id="buyerData" value="{{ ($buyers) }}">

                <input type="hidden" name="subscriberEmailselect" id="subscriberEmailselect" value="{{ ($subscriberEmailselect) }}">
                <input type="hidden" name="sellerEmailselect" id="sellerEmailselect" value="{{ ($sellerEmailselect) }}">
                <input type="hidden" name="buyerEmailselect" id="buyerEmailselect" value="{{ ($buyerEmailselect) }}">
                <input type="hidden" name="newsleterId" id="newsleterId" value="{{ ($id) }}">
                

                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
        
    </div>
    <!--Block 01-->
</section>

<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>
<script type="text/javascript">
    /*$(document).ready(function() {
        $('#example-getting-started').multiselect();
    });*/
    /*$(document).ready(function() {
        $('#example-enableCaseInsensitiveFiltering-subs').multiselect({
            enableCaseInsensitiveFiltering: true
        });

        $('#example-enableCaseInsensitiveFiltering-sellers').multiselect({
            enableCaseInsensitiveFiltering: true
        });

       $('#example-enableCaseInsensitiveFiltering-buyers').multiselect({
            enableCaseInsensitiveFiltering: true
        });
    });*/
</script>
@endsection
