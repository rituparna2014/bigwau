<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo!empty($pageTitle) ? $pageTitle : ''; ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               
                <form class="form-horizontal form-label-left" role="form" method="POST" action="{{ url('/administrator/password/reset') }}" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Old Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" type="password" name="old_password" id="old_pwd" placeholder="Please enter your old password"
                                   data-original-title="Please enter your old password" data-placement="top" required="required"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">New Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" type="password" name="new_password" id="new_pwd" placeholder="Please enter your new password"
                                   data-original-title="Please enter your new password" data-placement="top" required="required"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" type="password" name="confirm_password" id="confirm_pwd" placeholder="Please confirm your password"
                                   data-original-title="Please confirm your password" data-placement="top" required="required"/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button class="btn btn-danger" type="button" onclick="window.location ='{{url('administrator/dashboard')}}'">Back</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>