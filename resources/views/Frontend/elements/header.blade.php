<header class="mainHeader"><!--mainHeader-->
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="logo"><!--logo-->
						<figure><img src="{{ asset('public/frontend/images/logo.png') }}" alt=""></figure>
					</div><!--logo-->

					<nav class="mainNav"><!--mainNav-->
						<ul>
							<li><a class="active" href="javascript:void(0)">HOME</a></li>
							<li><a href="javascript:void(0)">SERVICES</a></li>
							<li><a href="javascript:void(0)">BLOG</a></li>
							<li><a href="javascript:void(0)">ABOUT</a></li>
							<li><a href="javascript:void(0)">CONTACT US</a></li>
							
						</ul>
					</nav><!--mainNav-->
					@if(Auth::guard('seller')->check())
					<div class="header_right"><!--header_right-->
						<ul>
							<li><a href="{{ route('seller.auth.logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">LOGOUT</a></li>
                            <form id="logout-form" action="{{ route('seller.auth.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
						</ul>
					</div>
					@elseif(Auth::guard('buyer')->check())
					<div class="header_right"><!--header_right-->
						<ul>
							<li><a href="{{ route('buyer.auth.logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">LOGOUT</a></li>
                            <form id="logout-form" action="{{ route('buyer.auth.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
						</ul>
					</div>
					@else
					<!--header_right-->
					<div class="header_right"><!--header_right-->
						<ul>
							<li>
								<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">Login</a>
							</li>

							<li>
								<a class="regs" href="javascript:void(0)">Register
									<i class="fa fa-angle-down" aria-hidden="true"></i></a>
							</li>
						</ul>
						<div class="reg_drop"><!--reg_drop-->
							<a href="{{ route('buyer.register') }}">
								Buyer’s registration
							</a>
							<a href="{{ route('seller.register') }}">
								Seller’s Registration
							</a>
						</div><!--reg_drop-->
					</div><!--header_right-->
					@endif
					
				</div>
			</div>
		</div>
		@if(Session::has('successMessage'))
			<div class="d-flex p-2 justify-content-center">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<p class="alert alert-success">{{ Session::get('successMessage') }}</p>
			</div>
		@endif

		@if(Session::has('errorMessage'))
			<div class="d-flex p-2 justify-content-center">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<p class="alert alert-danger">{{ Session::get('errorMessage') }}</p>
			</div>
		@endif
	</header><!--mainHeader-->

<!-- Modal -->
<div class="modal fade loginPopup" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <a href="{{route('buyer.auth.login')}}">Buyer</a>
        <a href="{{route('seller.auth.login')}}">Seller</a>
      </div>
      
    </div>
  </div>
</div>

