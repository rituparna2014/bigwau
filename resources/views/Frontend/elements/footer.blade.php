<footer class="footerHolder"><!--footerHolder-->
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h3>Main Links</h3>
					<ul>
						<li>
							<a href="javascript:void(0)">HOME</a>
						</li>
						<li>
							<a href="javascript:void(0)">SERVICES</a>
						</li>
						<li>
							<a href="javascript:void(0)">BLOG</a>
						</li>
						<li>
							<a href="javascript:void(0)">ABOUT</a>
						</li>
						<li>
							<a href="javascript:void(0)">CONTACT US</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3">
					<h3>Contact Us</h3>
					<span>Big Wau LLC</span>
					<span>1750 Prairie City Rd, Suite 130, PMB166</span>
					<span>Folsom, CA 95630 USA</span>
					<br><br>
					<span>Phone: 1-888-393-5570</span>
					<span>Email: info@bigwau.com</span>
					<span>Web: www.bigwau.com</span>
				</div>
				<div class="col-md-3">
					<h3>Keep in Touch</h3>
					<div class="footer_touch">
						<input type="text" name="" class="form-control" placeholder="Enter Your Email Address">
						<button class="btn">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</button>
					</div>
				</div>
				<div class="col-md-3">
					<figure>
						<img src="{{ asset('public/frontend/images/footer-logo.png') }}" alt="">
					</figure>
				</div>
			</div>

			<div class="copyRight"><!--copyRight-->
				<strong>© COPYRIGHT 2019 - BIG WAU LLC - PRIVACY POLICY</strong>
			</div><!--copyRight-->
		</div>
	</footer><!--footerHolder-->