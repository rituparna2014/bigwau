@extends ('Frontend.layouts.home')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="registerHolder"><!--registerHolder-->
        <h2>Seller register</h2>
        <div class="reg_steps"><!--reg_steps-->
            <ul>
                <li class="active">
                    <strong>01</strong>
                    <span>Verification</span>
                </li>

                <li>
                    <strong>02</strong>
                    <span>Information</span>
                </li>

                <li>
                    <strong>03</strong>
                    <span>Complete</span>
                </li>
            </ul>
        </div><!--reg_steps-->

        <div class="regFstStep"><!--regFstStep-->
            <div class="form-group">
                <label>Email</label>
                <input type="email" id="email" class="form-control" placeholder="Enter your email address" required>
                <span id="email_error" style="color:red;font-weight: bold;"></span>
            </div>

            <div class="form-group">
                <label>Verify your email</label>
                <!-- <figure>
                    <img src="{{ asset('public/frontend/images/caption.png') }}" alt="">
                </figure> -->
                <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
            </div>
            
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Upon creating my account, I agree the privacy policy and terms</label>
            </div>
            <span id="show_err">Please check the checkbox and recaptcha to move forward</span>
            <input type="hidden" id="baseUrl" value="{{ url('/') }}">
            <div class="btn_area">
                <button class="btn" id="first_next">next</button>
            </div>
            
        </div><!--regFstStep-->
    </section><!--registerHolder-->
<script>
    function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };
    $(document).ready(function(){
        $submit = $("#first_next").hide();
        var baseUrl = $('#baseUrl').val();
        $('#exampleCheck1').click(function(){
            $cbs = $('#exampleCheck1').click(function() {
                if(grecaptcha.getResponse().length != 0 ){
                    $submit.toggle( $cbs.is(":checked") );
                }
                
            });
        });
        $('#first_next').click(function(){
            var emailVal = $("#email").val();
            if (!ValidateEmail(emailVal)) {
                //alert("Invalid email address.");
                $("#email_error").text('Invalid email address');

            }
            else {
                $("#email_error").text('');
                $.ajax({
                    url: baseUrl + "/seller/checkcaptcha",
                    type: 'POST',
                    data: {'email':emailVal, '_token': $('meta[name="csrf-token"]').attr('content')},
                    success: function (response) {
                        if(response === 'exist'){
                            $("#email_error").text('Email already exist');
                        }
                    }
                });
            }
            
        });
        

        
    });
</script>
@endsection

