@extends ('Frontend.layouts.home')

@section('content')

<section class="loginHolder"><!--loginHolder-->
        <div class="wrapper">
            <h2>Seller's Login</h2>
            <form class="form-horizontal" method="POST" action="{{ route('seller.auth.doLogin') }}">
                        {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label>Email</label>
                    <!-- <input type="email" name="" class="form-control" placeholder="Please Enter Your Email"> -->
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus autocomplete="off" placeholder="Please enter your email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label>Password</label>
                    <!-- <input type="Password" class="form-control" placeholder="Please enter your password"> -->
                    <input id="password" type="password" class="form-control" name="password" required autocomplete="off" placeholder="Please enter your password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="forgPass">
                    <a href="javascript:void(0)">
                        Forgot Your Password?
                    </a>
                </div>

                <div class="btn_area">
                    <button type="submit" class="btn">Login</button>
                    <button class="btn">Register</button>
                </div>
            </form>
        </div>
    </section>
@endsection
