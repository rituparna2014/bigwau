@extends ('Frontend.layouts.home')

@section('content')
<section class="homeBanner"><!--homeBanner-->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="homeBanner_text"><!--homeBanner_text-->
                        <span>Your gateway to contact electronics sourcing services</span>
                        <h2>IN SOUTHEAST <strong>ASIA</strong></h2>
                        <span>No need to wait find your best service</span>
                    </div><!--homeBanner_text-->

                    <div class="homeBanner_search"><!--homeBanner_search-->
                        <div class="search"><!--search-->
                            <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <button class="btn">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                              </div>
                            </div>
                            <input type="text" name="" class="form-control" placeholder="Enter Keyword">
                            </div>
                        </div><!--search-->

                        <div class="category"><!--category-->
                            <select>
                                <option>Category</option>
                                <option>Category2</option>
                            </select>
                        </div><!--category-->

                        <div class="location"><!--location-->
                            <div class="input-group">
                                <input type="text" name="" class="form-control" placeholder="Location">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">
                                    <button class="btn">
                                    Go
                                </button>
                                  </div>
                                </div>
                                
                            </div>
                        </div><!--location-->
                    </div><!--homeBanner_search-->
                </div>
            </div>
        </div>
    </section><!--homeBanner-->

    <section class="homeCounter"><!--homeCounter-->
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <strong>20+</strong>
                    <p>
                        Contract manufacturers represented
                    </p>
                </div>
                <div class="col-md-3">
                    <strong>40%</strong>
                    <p>
                        Of contract manufacturers are public listed companies
                    </p>
                </div>
                <div class="col-md-3">
                    <strong>15</strong>
                    <p>
                        Average years of contract manufacturing experience serving start-ups to Fortune Global 500 companies
                    </p>
                </div>
                <div class="col-md-3">
                    <strong>5</strong>
                    <p>
                        Government and non-profit agencies consulted with
                    </p>
                </div>
            </div>
        </div>
    </section><!--homeCounter-->

    <section class="homeBrand"><!--homeBrand-->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>
                        We promote and facilitate Electronics outsourcing and contract manufacturing services to Malaysia, Singapore and eventually rest of <strong>Southeast Asia.</strong>
                    </p>
                    <h2>Brands</h2>
                    <ul>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon2.png') }}" alt="">
                            
                        </li>

                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon3.png') }}" alt="">
                            
                        </li>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon4.png') }}" alt="">
                            
                        </li>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon5.png') }}" alt="">
                            
                        </li>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon6.png') }}" alt="">
                            
                        </li>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon7.png') }}" alt="">
                            
                        </li>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon8.png') }}" alt="">
                            
                        </li>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon9.png') }}" alt="">
                            
                        </li>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon10.png') }}" alt="">
                            
                        </li>
                        <li>
                            
                                <img src="{{ asset('public/frontend/images/brand-icon11.png') }}" alt="">
                            
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!--homeBrand-->

    <section class="homeServices"><!--homeServices-->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Services</h2>
                    <ul>
                        <li>
                            <figure>
                                <img src="{{ asset('public/frontend/images/service-img1.jpg') }}" alt="">
                                <strong>
                                    <img src="{{ asset('public/frontend/images/service-icon1.png') }}" alt="">
                                </strong>
                            </figure>
                            <span>Semiconductors</span>
                        </li>
                        <li>
                            <figure>
                                <img src="{{ asset('public/frontend/images/service-img2.jpg') }}" alt="">
                                <strong>
                                    <img src="{{ asset('public/frontend/images/service-icon2.png') }}" alt="">
                                </strong>
                            </figure>
                            <span>
                                Electronic Manufacturing Services
                            </span>
                        </li>
                        <li>
                            <figure>
                                <img src="{{ asset('public/frontend/images/service-img3.jpg') }}" alt="">
                                <strong>
                                    <img src="{{ asset('public/frontend/images/service-icon3.png') }}" alt="">
                                </strong>
                            </figure>
                            <span>Mechanical Component & Assembly</span>
                        </li>
                        <li>
                            <figure>
                                <img src="{{ asset('public/frontend/images/service-img4.jpg') }}" alt="">
                                <strong>
                                    <img src="{{ asset('public/frontend/images/service-icon4.png') }}" alt="">
                                </strong>
                            </figure>
                            <span>Machine Fabrication & Industrial Automation</span>
                        </li>
                        <li>
                            <figure>
                                <img src="{{ asset('public/frontend/images/service-img5.jpg') }}" alt="">
                                <strong>
                                    <img src="{{ asset('public/frontend/images/service-icon5.png') }}" alt="">
                                </strong>
                            </figure>
                            <span>Supply Chain Services</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!--homeServices-->

    <section class="homeBlog"><!--homeBlog-->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Latest from the Blog</h2>
                    <ul>
                        <li>
                            <strong>Introduction to ASEAN</strong>
                            <span>January 8, 2019</span>
                            <p>
                                What is ASEAN? The Association of Southeast Asian Nations (ASEAN) is a 10-member regional bloc with a combined GDP of $2.4 trillion, a population of 630 million, and a land mass covering…
                            </p>
                            <div class="more">
                                <a href="javascript:void(0)">Read More</a>
                            </div>
                        </li>

                        <li>
                            <strong>Introduction to ASEAN</strong>
                            <span>January 8, 2019</span>
                            <p>
                                What is ASEAN? The Association of Southeast Asian Nations (ASEAN) is a 10-member regional bloc with a combined GDP of $2.4 trillion, a population of 630 million, and a land mass covering…
                            </p>
                            <div class="more">
                                <a href="javascript:void(0)">Read More</a>
                            </div>
                        </li>

                        <li>
                            <strong>Introduction to ASEAN</strong>
                            <span>January 8, 2019</span>
                            <p>
                                What is ASEAN? The Association of Southeast Asian Nations (ASEAN) is a 10-member regional bloc with a combined GDP of $2.4 trillion, a population of 630 million, and a land mass covering…
                            </p>
                            <div class="more">
                                <a href="javascript:void(0)">Read More</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!--homeBlog-->

    <section class="homeTestimonial"><!--homeTestimonial-->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>What People Say</h2>

                    <div class="homeTestimonial_box"><!--homeTestimonial_box-->
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec molestie magna quis tempus placerat. Sed fringilla id diam quis vehicula. Pellentesque id erat quis turpis volutpat convallis. Nunc et viverra nibh. Pellentesque porttitor diam eu massa rhoncus finibus.
                        </p>
                        <figure>
                            <img src="{{ asset('public/frontend/images/service-img2.jpg') }}" alt="">
                        </figure>
                        <strong>Alex D</strong>
                    </div><!--homeTestimonial_box-->
                </div>
            </div>
        </div>
    </section><!--homeTestimonial-->