<!DOCTYPE html>
<html lang="en">
    <head>
        <title>HomePage</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800,900&display=swap" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap" rel="stylesheet"><link rel="stylesheet" href="{{ asset('public/frontend/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/frontend/css/font-awesome.min.css') }}" >
        <link rel="stylesheet" href="{{ asset('public/frontend/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/owl.carousel.min.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/js/owl.carousel.min.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js' async defer ></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    </head>
    <body>
    <!-- Header Home -->
        @include('Frontend.elements.header')    
    <!-- Header Home -->
    
    <!-- /page content -->
        @yield('content')
    <!-- /.content --> 

    <!-- Footer Home -->
        @include('Frontend.elements.footer')    
    <!-- Footer Home -->
    
    <script>
        $(document).ready(function(){
            $('.regs').click(function(){
                $('.reg_drop').slideToggle()
            });


            $('.custom1').owlCarousel({
                loop:true,
                margin:10,
                responsiveClass:true,
                dots: false,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:2,
                        nav:false
                    },
                    1000:{
                        items:3,
                        nav:true,
                        loop:false
                    }
                }
            });

            $('.homeTest').owlCarousel({
                loop:true,
                margin:10,
                responsiveClass:true,
                dots: false,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:1,
                        nav:false
                    },
                    1000:{
                        items:1,
                        nav:true,
                        loop:false
                    }
                }
            });
        })
    </script>
</body>

</html>